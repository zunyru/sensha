<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><a href="dashboard.php"> Dashboard</a><span>Order history</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
		    <div class="box-content">
				<div class="layout-contain">
					<div class="topic">
						<p class="title-page">Order history</p>
					</div>
					<div class="clr box-detail-history">
						<div class="clr">
							<div class="left">
								<div class="col1">
									<span>Shipment ID:</span>
									<p>D892832TH</p>
									<div style="margin-top: 10px;">
										<span>Data of order:</span>
										<p>12/12/2018</p>
									</div>
								</div>
								<div class="col2">
									<div>
										<span>Delivery address</span>
										<p>Nobuaki Oka</p>
									</div>
									<div class="address">
										126/2 Soi Ratcgawithi2, Samsennai, Phayathai, Bangkok 10400 Thailand <br>Tel :  +81 87975 1234
									</div>
								</div>
							</div>
							<div class="clr right">
								<div class="col1">
									<span>Total Amount:</span>
									<p class="txt-red">16,360 Yen</p>
								</div>
								<div class="col2">
									<span>Delivery method:</span>
									<p>Domestic post</p>
									<span class="b-gray">Domestic</span>
								</div>
							</div>
						</div>
					    <div class="status-delivery">
							<h2>Status : <span>Delivery (#391849)</span></h2>
							<div class="c-left">
								<ul class="ul">
									<li>
										<figure><img src="images/p5.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p6.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p4.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p5.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p6.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="c-right">
								<ul>
									<li>
										<a href="" class="b-blue">Invoice download</a>
									</li>
									<li>
										<a href="" class="b-blue">Receipt download</a>
									</li>
									<li>
										<a href="claim.php" class="b-blue">Claim this shipment</a>
									</li>
								</ul>
						    </div>
						</div>
					</div>
					<div class="clr box-detail-history">
						<div class="clr">
							<div class="left">
								<div class="col1">
									<span>Shipment ID:</span>
									<p>D892832TH</p>
									<div style="margin-top: 10px;">
										<span>Data of order:</span>
										<p>12/12/2018</p>
									</div>
								</div>
								<div class="col2">
									<div>
										<span>Delivery address</span>
										<p>Nobuaki Oka</p>
									</div>
									<div class="address">
										126/2 Soi Ratcgawithi2, Samsennai, Phayathai, Bangkok 10400 Thailand <br>Tel :  +81 87975 1234
									</div>
								</div>
							</div>
							<div class="clr right">
								<div class="col1">
									<span>Total Amount:</span>
									<p class="txt-red">16,360 Yen</p>
								</div>
								<div class="col2">
									<span>Delivery method:</span>
									<p>Domestic post</p>
									<span class="b-gray">Domestic</span>
								</div>
							</div>
						</div>
					    <div class="status-delivery">
							<h2>Status : <span>Delivery (#391849)</span></h2>
							<div class="c-left">
								<ul class="ul2">
									<li>
										<figure><img src="images/p5.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p6.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p4.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p4.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
									<li>
										<figure><img src="images/p4.jpg"></figure>
										<div class="detail">
											<p class="name-p">Product Name</p>
											<div class="amount-status">
												<span>Amount: 3</span>
												<p class="txt-red">Subtotal: 5,180 Yen</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="c-right">
								<ul>
									<li>
										<a href="" class="b-blue">Invoice download</a>
									</li>
									<li>
										<a href="" class="b-blue">Receipt download</a>
									</li>
									<li>
										<a href="claim.php" class="b-blue">Claim this shipment</a>
									</li>
								</ul>
						    </div>
						</div>
					</div>
					<section id="pagination" class="row">
					  <ul>
						<li class="previous"><a href=""><img src="images/arrow-p.png"></a></li>
						<li class="active"><span>1</span></li>
						<li class="number"><a href="">2</a></li>
						<li class="number"><a href="">3</a></li>
						<li class="number"><a href="">4</a></li>
						<li class="number"><a href="">5</a></li>
						<li class="next"><a href=""><img src="images/arrow-next.png"></a></li>
					 </ul>
				   </section>
				</div><!--layout-contain-->
			</div><!--box-content-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
	<script src="js/jquery.showmore.js"></script> 
	<script>
		jQuery(document).ready(function(){
			jQuery(".ul").showmore({visible:3});
			jQuery(".ul2").showmore({visible:3});
			
		});
	</script>
</div>
<!-- .wrapper -->

