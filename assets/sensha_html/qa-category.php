<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
     <?php include('include/header.php')?>
	 <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a><span>Q & A</span></span>
		</div>
	 </div>
     <div class="clr page-qa">
		<div class="clr inner">
		  <div class="topic2">
			<p class="title-page">Question aout SENSHA</p>
		  </div>
		  <p class="title-detail">We offer high quality products and reliable services<br>At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.</p>
		  <div class="clr inner-qa">
			<aside class="col-left">
				<div class="box_menu">
					<div class="a_menu">
						<div class="topic">
						  <p>Q&A category</p>
		  			    </div>
						<ul>
							<li><a href="">SENSHA</a></li>
							<li><a href="">Products</a></li>
							<li><a href="">Order / Payment</a></li>
                            <li><a href="">SENSHA</a></li>
							<li><a href="">Products</a></li>
							<li><a href="">Order / Payment</a></li>
						</ul>
					</div>
				</div>
			</aside>
			<div class="content">
				<div class="question-list">
					<ul>
						<li class="active">
							<a href="single-qa.php">
								<figure><img src="images/icon-qa.png"></figure>
								<div class="detail">
									<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
									<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
	we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
									<div class="see-more">
										See more <span>>></span>
									</div>
								</div>
							</a>
						</li>
						<li>
							<a href="single-qa.php">
							<figure><img src="images/icon-qa.png"></figure>
							<div class="detail">
								<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
								<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
								<div class="see-more">
									See more <span>>></span>
								</div>
							</div>
							</a>
						</li>
						<li class="active">
							<a href="single-qa.php">
								<figure><img src="images/icon-qa.png"></figure>
								<div class="detail">
									<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
									<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
	we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
									<div class="see-more">
										See more <span>>></span>
									</div>
								</div>
							</a>
						</li>
						<li>
							<a href="single-qa.php">
								<figure><img src="images/icon-qa.png"></figure>
								<div class="detail">
									<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
									<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
	we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
									<div class="see-more">
										See more <span>>></span>
									</div>
								</div>
							</a>
						</li>
						<li class="active">
							<a href="single-qa.php">
								<figure><img src="images/icon-qa.png"></figure>
								<div class="detail">
									<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
									<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
	we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
									<div class="see-more">
										See more <span>>></span>
									</div>
								</div>
							</a>
						</li>
						<li>
							<a href="single-qa.php">
								<figure><img src="images/icon-qa.png"></figure>
								<div class="detail">
									<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
									<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why
	we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car.</p>
									<div class="see-more">
										See more <span>>></span>
									</div>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		  </div>
	   </div>
	 </div>
	 
   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

