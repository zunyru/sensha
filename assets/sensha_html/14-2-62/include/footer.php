<footer id="footer">
        <div class="footer-wrap">
          <div class="inner">
            <nav class="footer-nav">
              <section>
                <h4>SENSHA Products</h4>
                <ul>
                  <li><a href="">PPF</a></li>
                  <li><a href="">Body</a></li>
                  <li><a href="">Window</a></li>
                  <li><a href="">Wheel / Tire</a></li>
                  <li><a href="">Mall/Bumper</a></li>
                  <li><a href="">Enine room</a></li>
                  <li><a href="">Interior</a></li>
                  <li><a href="">The other</a></li>
                </ul>
              </section>
              <section>
                <h4>SYNCSHIELD CUT Films<br>SENSHA Content</h4>
                <ul>
                  <li><a href="">Content 01</a></li>
                  <li><a href="">Content 02</a></li>
                  <li><a href="">Content 03</a></li>
                  <li><a href="">Content 04</a></li>
                  <li><a href="">Content 05</a></li>
                  <li><a href="">Content 06</a></li>
                </ul>
              </section>
              <section>
                <h4>About SENSHA</h4>
                <ul>
                  <li><a href="">About 01</a></li>
                  <li><a href="">About 02</a></li>
                  <li><a href="">About 03</a></li>
                  <li><a href="">About 04</a></li>
                  <li><a href="">About 05</a></li>
                  <li><a href="">About 06</a></li>
                </ul>
              </section>
              <section>
                <h4>Q&A</h4>
                <ul>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                </ul>
              </section>
            </nav>

            <section class="footer-sns">
              <ul>
                <li class="facebook">
                  <a href="">
                    <span class="icon-facebook"></span>
                    <span>Facebook</span>
                  </a>
                </li>
                <li class="line">
                  <a href="">
                    <span class="icon-line"></span>
                    <span>Line id</span>
                  </a>
                </li>
                <li class="wechat">
                  <a href="">
                    <span class="icon-wechat"></span>
                    <span>WeChat</span>
                  </a>
                </li>
                <li class="whatsapp">
                  <a href="">
                    <span class="icon-whats-app"></span>
                    <span>What’s App</span>
                  </a>
                </li>
              </ul>
            </section>
          </div>
        </div>
        <div class="copyright">
          <small>Copyright © 2018 Sensha. All Rights Reserved.</small>
        </div>
      </footer>
<!--------------------------modal--------------------------------------->
<div class="header-form" style="display: none;" id="modal01">
    <ul class="cl-group">
      <li class="cl active"><a href="#login"><span class="icon-lock"></span><span>LOGIN</span></a></li>
      <li class="cl"><a href="#signup"><span class="icon-person"></span><span>SIGNUP</span></a></li>
    </ul>
    <div class="form-content">
      <div id="login" class="form-content-inner">
        <p class="lead">Please fill the form below to login</p>
        <form>
          <div class="form-block">
            <label for="CustomerEmail" class="hidden-label">Email</label>
            <input type="email" id="CustomerEmail" class="user-input user-email" name="customer" placeholder="">
            <label for="CustomerPassword" class="hidden-label">Password</label>
            <input type="password" id="CustomerPassword" class="user-input user-password" name="customer" placeholder="">
            <input type="button" class="sign-in-btn btn" onclick="location.href='dashboard.php';" value="LOGIN" />
          </div>
        </form>
      </div>
      <div id="signup" class="form-content-inner">
        <p class="lead">Please fill the form below to login2</p>
        <form>
          <div class="form-block">
            <label for="CustomerEmail" class="hidden-label">Email</label>
            <input type="email" id="CustomerEmail" class="user-input user-email" name="customer" placeholder="">
            <label for="CustomerPassword" class="hidden-label">Password</label>
            <input type="password" id="CustomerPassword" class="user-input user-password" name="customer" placeholder="">
            <input type="submit" class="sign-in-btn btn" value="LOGIN">
          </div>
        </form>
      </div>
    </div>
    <p class="note">*If you do not have an account, <a href="">Please signup from here</a></p>
    <p class="note">*If you forget the password, <a href="">Please reset he password from here</a></p>
</div>
<!----------------------------------------------------------------->
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script> 
<script src="js/slick.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/home.js"></script> 
<script src="js/action.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".search-button-m").fancybox();
        $(".sign-up").fancybox();
        $(".login").fancybox();
        $(".language").fancybox();
        $(".sp-login-btn").fancybox();       
    });
</script>

	<script>

		if ($('#gallery-thumbs').length > 0) {

		// Cache the thumb selector for speed
		var thumb = $('#gallery-thumbs').find('.thumb');

		// How many thumbs do you want to show & scroll by
		var visibleThumbs = 5;

		// Put slider into variable to use public functions
		var gallerySlider = $('#gallery').bxSlider({
			controls: false,
			pager: false,
			easing: 'easeInOutQuint',
			infiniteLoop: true,
			speed: 500,
			adaptiveHeight:true,
			auto: true,
			onSlideAfter: function (currentSlideNumber) {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				thumb.removeClass('pager-active');
				thumb.eq(currentSlideNumber).addClass('pager-active');
			},

			onSlideNext: function () {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				slideThumbs(currentSlideNumber, visibleThumbs);
			},

			onSlidePrev: function () {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				slideThumbs(currentSlideNumber, visibleThumbs);
			}
		});

		// When clicking a thumb
		thumb.click(function (e) {

			// -6 as BX slider clones a bunch of elements
			gallerySlider.goToSlide($(this).closest('.thumb-item').index());

			// Prevent default click behaviour
			e.preventDefault();
		});

		// Thumbnail slider
		var thumbsSlider = $('#gallery-thumbs').bxSlider({
			controls:true,
			pager:false,
			adaptiveHeight:true,
			easing: 'easeInOutQuint',
			infiniteLoop: false,
			minSlides: 5,
			maxSlides: 2,
			slideWidth: 360,
			slideMargin: 10
		});

		// Function to calculate which slide to move the thumbs to
		function slideThumbs(currentSlideNumber, visibleThumbs) {

			// Calculate the first number and ignore the remainder
			var m = Math.floor(currentSlideNumber / visibleThumbs);

			// Multiply by the number of visible slides to calculate the exact slide we need to move to
			var slideTo = m * visibleThumbs;

			// Tell the slider to move
			thumbsSlider.goToSlide(m);
		}

		// When you click on a thumb
		$('#gallery-thumbs').find('.thumb').click(function () {

			// Remove the active class from all thumbs
			$('#gallery-thumbs').find('.thumb').removeClass('pager-active');

			// Add the active class to the clicked thumb
			$(this).addClass('pager-active');

		});
	}
	</script>
</body>
</html>