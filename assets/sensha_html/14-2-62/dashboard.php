<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="index.php">HOME</a><span> Dashboard</span></span>
		</div>
	</div><!--inner-->				
	<div class="container-page">
		<div class="clr inner">
			<div class="topic">
				<p class="title-page">Credit point</p>
			</div>
			<p class="txt-green">To specify to use credit point (USD)</p>
			<p>Currently you have 2,345 USD points available for shopping. You can add the number not over than your total points.
Or you can refund from the shop admin page for sole agent.</p>
		    <div class="box-shop-admin">
				<div class="line"><div class="b-yellow">Shop admin</div></div>
			      <ul class="clr">
					  <li>
						  <figure><img src="images/icon-user.png"></figure>
						  <p class="title">Edit personal infomation</p>
						  <p>Edit personal Infomationsuch as account, delivery</p>
					  </li>
					  <li>
						  <figure><img src="images/icon-history.png"></figure>
						  <p class="title">Shopping history</p>
						  <p>Review Shoppinf history to download invoices.</p>
					  </li>
					  <li>
						  <figure><img src="images/icon-cart2.png"></figure>
						  <p class="title">Cart</p>
						  <p>Review current cart to buy online or edit shopping cart content</p>
					  </li>
					  <li>
						  <figure><img src="images/icon-wishlist.png"></figure>
						  <p class="title">Wishlist</p>
						  <p>Review current cart to buy online or edit shopping cart content</p>
					  </li>
			      </ul>
			</div>
			<div class="clr box-question">
				<div class="topic">
					<p class="title-page">Frequent question</p>
			    </div>
				<div class="clr inner-q">
					<div class="col">
						<h3>SENSHA</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Products</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Order / Payment</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Delivery</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Return</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Discount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>Avgount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col">
						<h3>The others</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
				</div>
			</div><!--box-question-->
			<div class="clr box_recent_purchased">
			<div class="inner">
				<div class="topic">
					<p>Recent purchased items</p>
					<h2>Recommended items bassed on your recent shopping history.</h2>
				</div>
				<div class="clr list-recent">
					<ul>
						<li>
							<figure>
						    	<img src="images/p5.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p1.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p2.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p3.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p4.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div><!--box_recent_purchased-->
		<div class="clr box_recent_view">
			<div class="inner">
				<div class="topic">
					<p>Recent viewed items</p>
					<h2>Recommended items bassed on your recent web browser.</h2>
				</div>
				<div class="clr list-recent">
					<ul>
						<li>
							<figure>
						    	<img src="images/p5.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p1.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p2.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p3.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<img src="images/p4.jpg">
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		</div>
	</div><!--container-->
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
	
</div>
<!-- .wrapper -->

