<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">HOME</a></span><span>Body</span>
			</div>
			<!-----------------menu-category-product-mobile------------------------>
			<div id="stick-here"></div>
            <div id="stickThis">
			<div class="menu-mobile">
          	  <header id="toggle">
            	<h3><img src="images/list.png"> Product Menu </h3><span class="arrow"><img src="images/chev.png"></span>
			  </header>
			  <div id="content">
				   <aside class="col-left">
				<div class="search">
					<form>
						<input type="text" placeholder="Keyword Search">
						<button type="button"><img src="images/icon-search.png" width="25"></button>
					</form>
				</div>
				<div class="box_menu">
					<div class="a_menu">
						<h3>BODY</h3>
						<ul>
							<li><a href="">Sub-cat 1</a></li>
							<li><a href="">Sub-cat 2</a></li>
							<li><a href="">Sub-cat 3</a></li>
							<li><a href="">Sub-cat 4</a></li>
							<li><a href="">Sub-cat 5</a></li>
							<li><a href="">Sub-cat 6</a></li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Item Type</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" checked="checked"> Paint protection film
								</label>
						    </li>
							<li>
								<label>
									<input type="checkbox"> Coating
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Basic wash
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Special wash
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Tools
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Bottles
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Delivery Type</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" > Oversea
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Domestic
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Feature</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" > Normal item
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" >Free Simple
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Usage</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" checked="checked"> Scratch/Damage
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Stain
								</label>
							</li>
						</ul>
					</div>
				</div>
			</aside>
			  </div>
            </div><!--menu-mobile-->
			</div>
			<!-----------------end-menu-category-product-mobile------------------------>
			
		  <aside class="col-left">
				<div class="search">
					<form>
						<input type="text" placeholder="Keyword Search">
						<button type="button"><img src="images/icon-search.png" width="25"></button>
					</form>
				</div>
				<div class="box_menu">
					<div class="a_menu">
						<h3>BODY</h3>
						<ul>
							<li><a href="">Sub-cat 1</a></li>
							<li><a href="">Sub-cat 2</a></li>
							<li><a href="">Sub-cat 3</a></li>
							<li><a href="">Sub-cat 4</a></li>
							<li><a href="">Sub-cat 5</a></li>
							<li><a href="">Sub-cat 6</a></li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Item Type</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" checked="checked"> Paint protection film
								</label>
						    </li>
							<li>
								<label>
									<input type="checkbox"> Coating
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Basic wash
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Special wash
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox"> Tools
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Bottles
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Delivery Type</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" > Oversea
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Domestic
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Feature</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" > Normal item
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" >Free Simple
								</label>
							</li>
						</ul>
					</div>
					<div class="a_menu m-checkbox">
						<h3>Usage</h3>
						<ul>
							<li>
								<label>
									<input type="checkbox" checked="checked"> Scratch/Damage
								</label>
							</li>
							<li>
								<label>
									<input type="checkbox" > Stain
								</label>
							</li>
						</ul>
					</div>
				</div>
			</aside>
			<div class="content">
				<div class="headline">
                	<p class="title">Search for SENSHA Products</p>
                	<h2>We offer high quality products and reliable services</h2>
					<div class="clr product-list">
						<ul>
							<li>
								<a href="product-detail.php">
									<figure><img src="images/p1.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p2.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p3.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p4.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
						    <li>
								<a href="">
									<figure><img src="images/p5.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p6.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p7.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p1.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p5.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p6.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p7.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p1.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p5.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p6.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p7.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p1.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p1.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p2.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p3.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
							<li>
								<a href="">
									<figure><img src="images/p4.jpg" alt=""/></figure>
									<div class="detail">
										<p class="name-p">Product Name Product Name Product Name</p>
										<p class="quantity">50ml Boxed + accessories</p>
										<div class="clr p-amount">
											<span class="b-gray">Domestic</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
										<div class="clr p-amount">
											<span class="b-gray">Oversea</span>
											<div class="price">
												1,990 Yen
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<section id="pagination" class="row">
					  <ul>
						<li class="previous"><a href=""><img src="images/arrow-p.png"></a></li>
						<li class="active"><span>1</span></li>
						<li class="number"><a href="">2</a></li>
						<li class="number"><a href="">3</a></li>
						<li class="number"><a href="">4</a></li>
						<li class="number"><a href="">5</a></li>
						<li class="next"><a href=""><img src="images/arrow-next.png"></a></li>
					 </ul>
				   </section>
                </div>
				<div>
				</div>
			</div>
		</div><!--inner-->
	</div>
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
	<script>
		function sticktothetop() {
			var window_top = $(window).scrollTop();
			var top = $('#stick-here').offset().top;
			if (window_top > top) {
				$('#stickThis').addClass('stick');
				$('#stick-here').height($('#stickThis').outerHeight());
			} else {
				$('#stickThis').removeClass('stick');
				$('#stick-here').height(0);
			}
			}
			$(function() {
				$(window).scroll(sticktothetop);
				sticktothetop();
			});
	</script>
</div>
<!-- .wrapper -->

