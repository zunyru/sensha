<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="css/jquery.bxslider.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span><a href="product-category.php">Body</a></span><span> Item name</span>
			</div>
			<!-----------------menu-category-product-mobile------------------------>
			<div id="stick-here"></div>
            <div id="stickThis">
			<div class="menu-mobile">
				<div class="clr m-inner">
          			<h3><i class="fas fa-clipboard-list" style="font-size:17px;margin-right: 5px;"></i> 5 Items In Cart</h3>
					<div class="right">
						<a href="cart.php"><i class="fas fa-shopping-cart"></i> &nbsp;Add to cart</a>
						<a href="payment.php" style="background:#f9bd00;"><i class="fas fa-money-bill-alt"></i>&nbsp; Buy</a>
					</div>
				</div>
            </div><!--menu-mobile-->
			</div>
			<!-----------------end-menu-category-product-mobile-------------------->
		    <div class="box-content">
				 <div class="clr box-detail-product">
					 <div class="left">
						 <div class="gallery-container"> 
						  	<div id="gallery" class="gallery-images"> 
								<img src="images/p01.jpg" alt="" /> 
								<img src="images/p02.jpg" alt="" /> 
								<img src="images/p03.jpg" alt="" /> 
								<img src="images/p04.jpg" alt="" />
						  	</div> 
							<div class="gallery-thumbs-container"> 
								 <ul id="gallery-thumbs" class="gallery-thumbs-list"> 
									<li class="thumb-item"> 
										<div class="thumb pager-active"> 
											<a href=""><img src="images/p01.jpg" alt="" /></a> 
										</div> 
									</li> 
									<li class="thumb-item"> 
										<div class="thumb"> 
											<a href=""><img src="images/p02.jpg" alt="" /></a> 
										</div>
									</li> 
									<li class="thumb-item"> 
										<div class="thumb"> 
											<a href=""><img src="images/p03.jpg" alt="" /></a> 
										</div>
									</li>
									<li class="thumb-item"> 
										<div class="thumb"> 
											<a href=""><img src="images/p04.jpg" alt="" /></a> 
										</div>
									</li>
								 </ul> 
							</div> 
					 	</div> 
						<div class="clr vdo">
							<div class="videoWrapper">
   				 				<iframe width="560" height="315" src="https://www.youtube.com/embed/rBZ3OOtRZgI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					 </div><!--left-->
					 <div class="right">
						 <div class="detail-product">
					     	 <h1>CRYSTAL GLOW 8 Year Nano Glass Coating 50ml Japan's finest Coating </h1>
							 <p class="id">Product ID : b-912B-005</p>
							 <p>＊＊ Exclusive Agent Partnership Enrollment＊＊ Interested to join our Exclusive Partnership send your proposal to info@cleanyourcar.jp</p>
							 <ul>
								 <li>
									 Wide acceptance from many pro-shops. Strong protective layer for the paint.
								 </li>
								 <li>
									 High durability and protection performance.
								 </li>
								 <li>
									 The desired finished appearance with hi-class gloss.
								 </li>
								 <li>
									 Keeping your car in a good condition for a long term.
								 </li>
							 </ul>
							 <div class="box-detail02">
								 <p class="head">Content</p>
								 <p>CRYSTAL GLOW 8 Year Nano Glass Coating 50ml </p>
							 </div>
						 </div>
						 <div class="clr price01">
							 <ul>
								 <li>
									<div class="clr p-amount">
										<span class="b-gray">Domestic</span>
										<div class="price">
											1,990 Yen
										</div>
									</div>
									<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
									<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
									<div class="amount01">
										<span>
											No.
											<select>
												<option value="0">1</option>
												<option value="1">2</option>
												<option value="2">3</option>
												<option value="3">4</option>
												<option value="4">5</option>
											</select>
										</span>
										<div class="col-cart">
											<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
										</div>
									</div>
								 </li>
								 <li>
									<div class="clr p-amount">
										<span class="b-gray">Oversea</span>
										<div class="price">
											1,990 Yen
										</div>
									</div>
									<div class="amount01">
										<span>
											No.
											<select>
												<option value="0">1</option>
												<option value="1">2</option>
												<option value="2">3</option>
												<option value="3">4</option>
												<option value="4">5</option>
											</select>
										</span>
										<div class="col-cart">
											<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
										</div>
									</div>
								 </li>
							 </ul>
						 </div>
						 <div class="addtowishlist">
							 Add your products to <a href="" class="wishlist"> &nbsp;<i class="far fa-heart"></i>  wishlist</a>
						 </div>
						 <div class="product-information">
							 <div class="table-responsive">
								<table class="table">
								<tbody>
									<tr>
										<td> Origin </td>
										<td>
											Japan
										</td>
									</tr>
									<tr>
										<td> General price </td>
										<td>
											5000 Yen
										</td>
									</tr>
									<tr>
										<td> Cost per use </td>
										<td>
											500 Yen
										</td>
									</tr>
									<tr>
										<td> Stock </td>
										<td>
											Available
										</td>
									</tr>
									<tr>
										<td> Minim order </td>
										<td>
											1
										</td>
									</tr>
									<tr>
										<td> Shipping </td>
										<td>
											Dommestic, Oversea
										</td>
									</tr>
									<tr>
										<td> Payment </td>
										<td>
											Paypal, Amazon, Alipay
										</td>
									</tr>
								</tbody>
							   </table>
							</div>
						 </div>
					 </div><!--right-->
				 </div><!--box-detail-product-->
			    <div class="clr box-about-product">
			        <h2>ABOUT CRYSTAL GLOW</h2>
					<figure class="pic-intro">
					  <img src="images/pic1.jpg" width="673" height="341" alt=""/>
					</figure>
					  <div class="inner-about-product">
						<div class="h-title">
							<h3>Feature and benefits</h3>
						</div>
						<p class="txt-bule">From beginners to experts, everyone gets the same finishing and qualities.</p>
						<figure><img src="images/pic2.jpg"></figure>
						<p>Fine Crystal contains professional used Glass Coating Agent as the principal component, and enables anyone to execute and form the glass coating layer on a car body. Fine Crystal has been specifically developed to form the glass coating which produces the finest glossy finish like your car is wet without coating unevenness and apply easily as car wash.</p>
						<p>Car owners who have tried polymeric agents and car wax however did not obtain satisfactory results, Fine Crystal certainly brings the realization of differences.</p>
						<p>Products such as "3 Years Durability Professional Coating" utilized by professionals could make coating film unevenness and this is an irretrievable mistake because Glass Coating Film is high hardness and difficult to remove. Hence, those "professional used coating agents" could not be applied simply.</p>
					  </div>
					  <div class="inner-about-product">
						<div class="h-title">
							<h3>Why so superior the glass coating?</h3>
						</div>
						<p class="txt-bule">Superior Coating</p>
						<figure><img src="images/pic3.jpg"></figure>
						<p>Usually polymeric agents for both commercial and business use contain petroleum solvent and this is easily oxidized which cannot be avoided. The coating surface will be, like front glass, oil films after the oxidization. As a consequence of this, conditions of a car are getting worse resulting in collecting dusts, scales and a lack of gloss on the car body. It can be said to car wax as well since it uses "oil content".</p>
						<p>Fine Crystal is glass based coating agent and its glass coating is never oxidized and deteriorated.</p>
					  </div>
					  <div class="inner-about-product">
						<div class="h-title">
							<h3>For reasons of low price</h3>
						</div>
						<p class="txt-bule">We manufacture and distribute products. Manufacturer direct sales guarantees Cost Performance and Quality Control.</p>
						<figure><img src="images/pic4.jpg"></figure>
					  </div>
					  <div class="inner-about-product">
						<div class="h-title">
							<h3>How to use</h3>
						</div>
						<div class="clr how-use">
							<ul>
								<li>
							    	<img src="images/h1.jpg">
									<p>
										<span>1) -</span> If you have on the surface, dust, sand, mud and dirt, rinse with a strong water current.
									</p>
								</li>
								<li>
							    	<img src="images/h2.jpg">
									<p>
										<span>2) -</span> Wash softly with a sponge using car shampoo. I recommend "BODY CLEAN" which is optimal detergent before coating.
									</p>
								</li>
								<li>
							    	<img src="images/h3.jpg">
									<p>
										<span>3) -</span> Wash away by water before dry.
									</p>
								</li>
								<li>
							    	<img src="images/h4.jpg">
									<p>
										<span>4) -</span> Spray "FINE CRYSTAL" on the wet body without wiping the water. Rub and extend it to be well applied vertically and horizontally with the soft sponge.(Pls splay 2 or 3 pushes on each panel)
									</p>
								</li>
								<li>
							    	<img src="images/h5.jpg">
									<p>
										<span>5) -</span> Pour water lightly before the coating becomes dry
									</p>
								</li>
								<li>
							    	<img src="images/h6.jpg">
									<p>
										<span>6) -</span> Be completed to finish wiping the remaining water on the surface using a cloth.
									</p>
								</li>
							</ul>
						</div>
					  </div>
			    </div>
			</div><!--box-content-->
            <aside class="col-right">
			  <div class="p-shipping">
			   <div class="total-domestic">
				   <figure><img src="images/icon-cart.png"></figure>
			     <div class="d2">
					   <span class="b-gray">Domestic</span>
					   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
					   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
				   </div>
				   <div style="background:#fff;">
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
						   <li>
							  <p>Minimum Shipping</p>
							  <span>900  Yen</span>
						   </li>
					   </ul>
				     <div class="total clr">
							<p>Total</p>
							<span class="txt-red">8,180 Yen</span>
					   </div>
				   </div>
		       </div>
		       <div class="total-oversea">
				   <div class="d2">
					   <span class="b-gray">Oversea</span>
					   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
					   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
				   </div>
				   <div style="background:#fff;">
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
						   <li>
							  <p>Minimum Shipping</p>
							  <span>900  Yen</span>
						   </li>
					   </ul>
					   <div class="total clr">
							<p>Total</p>
							<span class="txt-red">8,180 Yen</span>
					   </div>
				   </div>
		       </div>
			   <!--<div class="b-payment">
		     		<a href="payment.php">Proceed Payment <img src="images/i-cart.png"></a>
			   </div>-->
			   <div class="total clr" style="background:#004ea1;text-align: center;color:#fff;">
					<a href="" data-src="#modal01" class="login">Click to login <img src="images/login.png" style="width:20px;margin-left:7px;"></a>
			   </div>
			  </div>
			  <div class="box-recent-items">
				  <h3>Recent viewed items</h3>
				   <ul>
					   <li>
						   <a href="">
							   <figure><img src="images/p1.jpg"></figure>
							   <p class="name-p">
								   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
							   </p>
						   </a>
					   </li>
					   <li>
						   <a href="">
							   <figure><img src="images/p2.jpg"></figure>
							   <p class="name-p">
								   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
							   </p>
						   </a>
					   </li>
					   <li>
						   <a href="">
							   <figure><img src="images/p3.jpg"></figure>
							   <p class="name-p">
								   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
							   </p>
						   </a>
					   </li>
					   <li>
						   <a href="">
							   <figure><img src="images/p4.jpg"></figure>
							   <p class="name-p">
								   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
							   </p>
						   </a>
					   </li>
					   <li>
						   <a href="">
							   <figure><img src="images/p5.jpg"></figure>
							   <p class="name-p">
								   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
							   </p>
						   </a>
					   </li>
				   </ul>
		     </div>
			 <div class="product-cart-list">
			   <div class="p-shipping">
			       <p class="name-p">
					   Glass Coating Water-repellent Fine Crystal800ml by SENSHA
				   </p>
				   <div class="price-shipping">
					   <div class="clr p-amount">
							<span class="b-gray">Domestic</span>
							<div class="price">1,990 Yen</div>
					   </div>
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
					   </ul>
				   </div>
				   <div class="line-btn">
					     <a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
				   </div>
				   <div class="price-shipping">
					   <div class="clr p-amount">
							<span class="b-gray">Domestic</span>
							<div class="price">1,990 Yen</div>
					   </div>
				   </div>
				   <div class="line-btn" style="border-radius:0px 0px 15px 15px;">
					    <a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
				   </div>
		       </div>
			 </div>
	      </aside>
		</div><!--inner-->
		
		
	</div><!--container-->
    <script src="js/main.js"></script>
	<?php include('include/footer.php')?>
<script>
	$(function() { // document ready
		if ($('.product-cart-list').length) { // make sure "#sticky" element exists
			var el = $('.product-cart-list');
			var stickyTop = $('.product-cart-list').offset().top; // returns number
			var stickyHeight = $('.product-cart-list').height();

			$(window).scroll(function() { // scroll event
				var limit = $('#footer').offset().top - stickyHeight - 20;

				var windowTop = $(window).scrollTop(); // returns number

				if (stickyTop < windowTop) {
					el.css({
						position: 'fixed',
						top: 20
					});
				} else {
					el.css('position', 'static');
				}

				if (limit < windowTop) {
					var diff = limit - windowTop;
					el.css({
						top:20
					});
				}
			});
		}
	});
</script>
	<script>
		function sticktothetop() {
			var window_top = $(window).scrollTop();
			var top = $('#stick-here').offset().top;
			if (window_top > top) {
				$('#stickThis').addClass('stick');
				$('#stick-here').height($('#stickThis').outerHeight());
			} else {
				$('#stickThis').removeClass('stick');
				$('#stick-here').height(0);
			}
			}
			$(function() {
				$(window).scroll(sticktothetop);
				sticktothetop();
			});
	</script>
	
</div>
<!-- .wrapper -->