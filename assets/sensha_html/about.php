<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
      <?php include('include/header.php')?>
	  <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a><span> About</span></span>
		</div>
	  </div>
      <div class="top-banner b-about">
        <div class="caption-b">
			<p class="head-banner">CAPTIVATE YOU <br>BY REAL CAR BEAUTY</p>
			<h2>WITH PAINT PROTECTION FILM</h2>
			<p>Syncshield is a special paint protection film <br>
developed to protect vehicle body surface.</p>
		</div>
      </div>
	  <div class="box_section2">
		  <div class="clr wrap">
		  	  <h2>We offer high quality products and reliable services</h2>
			  <p>At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.<br>
We can help you manage and advance your car condition that can be measured through visible results.</p>
			  <ul class="clr">
				  <li>
					  <figure><img src="images/a1.jpg"></figure>
				  </li>
				  <li>
					  <figure><img src="images/a2.jpg"></figure>
				  </li>
				  <li>
					  <figure><img src="images/a3.jpg"></figure>
				  </li>
				  <li>
					  <figure><img src="images/a4.jpg"></figure>
				  </li>
			  </ul>
			  <div class="car">
		      	<img src="images/car.png">
			  </div>
		  </div>
	  </div>
      <div class="box_section3">
		  <div class="clr wrap">
			  <div class="topic">
				<p class="title-page">COMMITMENT IN QUALITY AND RELIABILITY</p>
			  </div>
			  <h2>Pride in professional quality products</h2>
		      <div class="clr b-service">
				  <div class="left">
					  <figure><img src="images/about01.png"></figure>
			      </div>
				  <div class="right">
					  <p>Our products are engaged in 800 our directly-managed shops and franchise networks in 18 countries. We continuously satisfy customer requiqmrents by focusing on Quality and Reliability. This is the main reason why we have Research and Development, and Manufacturing in house in Japan.</p>
					  <p>Feedback is gathered daily basis and utlized to advance our existing procuts and even for new product developments. In fact, our products are designed to be able to performe their capabilities, although a temperature is extreme: -30oC to + 40oC</p>
					  <p>At SENSHA, "Quality and Reliability" are necessary not only for the products and services offered to customers whereas also for SENSHA's attitude to society and how each SENSHA staff member carries out their work. SENSHA specifically understands quality as desctibed below:</p>
					  <ul>
						  <li>-  Quality of products</li>
						  <li>-  Quality of sales and service</li>
						  <li>-  Quality in terms of variety and delivery</li>
						  <li>-  Quality of business</li>
					  </ul>
					  <p>SENSHA family around the world will support your business. Track record of over 800 stores is the proof.</p>
				  </div>
			  </div>
		  </div>
      </div>
	  <div class="clr box-product ">
		  <div class="left">
			  <div class="topic">
				<p class="title-page">WHAT WE PROVIDE</p>
			  </div>
			  <p class="title">Full Product Line Up</p>
			  <p>SENSHA offers a full product line up to respond all our customers' needs in the car coating and washing. This avoids customers to use products from multiple suppliers which could have compatibility issues. The compatibility among our products was throughly checked so that you will not experience any compatibility issues</p>
			  <p class="title">Part and Problem Specific Approach</p>
			  <p>We develope and manufactur products by understanding which materials are used to which parts. Therefore, all of our products are "Part and/or Problem Specific". For instance, in terms of coating, you cannot use a coating agent for paint surfaces to wheels. Also, you should not use car shampoo to remove iron powders.</p>
			  <p>All our products were designed to undestand which materials and parts the products to be applied in order to maximize their performances and finishing.</p>
		  </div>
	      <div class="right">
			  <div class="product-category">
				  <ul>
					  <li>
						  <figure>
					      	<img src="images/c-p-1.jpg">
						  </figure>
						  <figcaption>
							   <h3>BODY</h3>
							   <p>Shiny and paint protection, cover minor scratches and stone chips, slowing down the corrosion</p>
						  </figcaption>
					  </li>
					  <li>
						  <figure>
					      	<img src="images/c-p-2.jpg">
						  </figure>
						  <figcaption>
							   <h3>WINDOW</h3>
							   <p>Sparkling clean windows and mirrors – without leaving behind streaks, scratches, residue or lint</p>
						  </figcaption>
					  </li>
					  <li>
						  <figure>
					      	<img src="images/c-p-3.jpg">
						  </figure>
						  <figcaption>
							   <h3>WHEEL/TIRE</h3>
							   <p>Stay black and shiniest shine, no more etching or spotting</p>
						  </figcaption>
					  </li>
					  <li>
						  <figure>
					      	<img src="images/c-p-4.jpg">
						  </figure>
						  <figcaption>
							   <h3>MALL/BUMPER</h3>
							   <p>Restore and refresh plastic surface with stuck dirt and wax into as new one</p>
						  </figcaption>
					  </li>
					  <li>
						  <figure>
					      	<img src="images/c-p-5.jpg">
						  </figure>
						  <figcaption>
							   <h3>ENGINE ROOM</h3>
							   <p>Protect and coat important components of engine room, restore the luster and rust proofing</p>
						  </figcaption>
					  </li>
					  <li>
						  <figure>
					      	<img src="images/c-p-6.jpg">
						  </figure>
						  <figcaption>
							   <h3>INTERIOR</h3>
							   <p>Polish, protect the interior work and restore the natural luster with very long-lasting effect</p>
						  </figcaption>
					  </li>
				  </ul>
		    </div>
		  </div>
	  </div>
      <div class="clr box-contact">
		  <div class="clr wrap">
			  <div class="left">
				  <h2>GLOBAL EXPANSION <br>OF SENSHA</h2>
				  <p>“We offer our products and service in
16 countries and regions all over the world!
Visit your local web site for further information.”</p>
			  </div>
		    <div class="right">
			  <div class="topic">
					<p class="title-page">CONTACT US</p>
			    </div>
			    <ul>
				    <li>
					    <figure><img src="images/i1.png"></figure>
						<div class="detail">
							<p class="title">HEAD OFFICE</p>
							<p>SENSHA CO., LTD</p>
						</div>
				    </li>
					<li>
					    <figure><img src="images/i2.png"></figure>
						<div class="detail">
							<p class="title">OFFICE ADDRESS</p>
							<p>1007-3, Kami-kasuya, Isehara-shi, Japan</p>
						</div>
				    </li>
					<li>
					    <figure><img src="images/i3.png"></figure>
						<div class="detail">
							<p class="title">PHONE</p>
							<p>(+81) 463-94-5106</p>
						</div>
				    </li>
			    </ul>
			  </div>
		  </div>
	  </div>






   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

