<footer id="footer">
	<div class="footer-wrap">
          <div class="inner">
            <nav class="footer-nav">
              <section>
                <h4>SENSHA Products</h4>
                <ul>
                  <li><a href="">PPF</a></li>
                  <li><a href="">Body</a></li>
                  <li><a href="">Window</a></li>
                  <li><a href="">Wheel / Tire</a></li>
                  <li><a href="">Mall/Bumper</a></li>
                  <li><a href="">Enine room</a></li>
                  <li><a href="">Interior</a></li>
                  <li><a href="">The other</a></li>
                </ul>
              </section>
              <section>
                <h4>SYNCSHIELD CUT Films<br>SENSHA Content</h4>
                <ul>
                  <li><a href="">Content 01</a></li>
                  <li><a href="">Content 02</a></li>
                  <li><a href="">Content 03</a></li>
                  <li><a href="">Content 04</a></li>
                  <li><a href="">Content 05</a></li>
                  <li><a href="">Content 06</a></li>
                </ul>
              </section>
              <section>
                <h4>About SENSHA</h4>
                <ul>
                  <li><a href="about.php">About</a></li>
                  <li><a href="terms.php">Terms of use </a></li>
                  <li><a href="privacy.php">Privacy policy </a></li>
                  <li><a href="contact.php">Contact</a></li>
                </ul>
              </section>
              <section>
                <h4>Q&A</h4>
                <ul>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                  <li><a href="">Q&A 01</a></li>
                </ul>
              </section>
            </nav>
            <section class="footer-sns">
              <ul>
                <li class="facebook">
                  <a href="">
                    <span class="icon-facebook"></span>
                    <span>Facebook</span>
                  </a>
                </li>
                <li class="line">
                  <a href="">
                    <span class="icon-line"></span>
                    <span>Line id</span>
                  </a>
                </li>
                <li class="wechat">
                  <a href="">
                    <span class="icon-wechat"></span>
                    <span>WeChat</span>
                  </a>
                </li>
                <li class="whatsapp">
                  <a href="">
                    <span class="icon-whats-app"></span>
                    <span>What’s App</span>
                  </a>
                </li>
              </ul>
            </section>
          </div>
      </div>
      <div class="copyright">
         <small>Copyright © 2018 Sensha. All Rights Reserved.</small>
      </div>
 </footer>
<!--------------------------modal--------------------------------------->
<div class="header-form" style="display: none;"id="modal01">
    <ul class="cl-group">
		<li class="cl active"><span class="icon-lock"></span><span>LOGIN</span></li>
    </ul>
    <div class="form-content">
      <div id="login" class="form-content-inner">
        <p class="lead">Please fill the form below to login</p>
        <form>
          <div class="form-block">
            <label for="CustomerEmail" class="hidden-label">Email</label>
            <input type="email" id="CustomerEmail" class="user-input user-email" name="customer" placeholder="">
            <label for="CustomerPassword" class="hidden-label">Password</label>
            <input type="password" id="CustomerPassword" class="user-input user-password" name="customer" placeholder="">
            <input type="button" class="sign-in-btn btn" onclick="location.href='dashboard.php';" value="LOGIN" />
          </div>
        </form>
      </div>
     
    </div>
    <p class="note">*If you do not have an account, <a href="register.php">Please signup from here</a></p>
    <p class="note">*If you forgot the password, <a href="forgot-password.php">Please reset he password from here</a></p>
</div>
<!----------------------------------------------------------------->
<script src="http://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/home.js"></script> 
<script src="js/action.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".search-button-m").fancybox();
        $(".sign-up").fancybox();
        $(".login").fancybox();
        $(".language").fancybox();
        $(".sp-login-btn").fancybox();       
    });
</script>

	
    <script src="่js/jquery.showmore.js"></script>
</body>
</html>