<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span><a href="dashboard.php">Dashboard</a><span>Confirm the claim</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="layout-contain">
				    <div class="clr box_form">
						<div class="topic">
							<p class="title-page">Confirm the claim for shipment id : D892832TH</p>
					    </div>
						<div class="box-inner">
							<div class="r-inline">
							  <label class="label"><img src="images/02.png">Shipment ID</label>
							  <div class="r-input">
							      <p>D892832TH</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="far fa-calendar-alt"></i> Date of order</label>
							  <div class="r-input">
								<p>12/12/2018</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="far fa-address-card"></i> Deliver address</label>
							  <div class="r-input">
								 <p>Nobuaki Oka</p>
								 <p class="f500">126/2 Soi Ratchawithi2, Samsennai, Phayathai, Bangkok 10400 Thailand <br>Tel : +81 879 75 1234</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="fas fa-file-alt"></i> Ordered items</label>
							  <div class="r-input">
								  <div class="confirm">
									<p class="f500">Item Name Item Name Item Name Item Name Item Name ItemName</p>
									<p style="margin-top:10px;">Amount: 3</p>
									<p class="txt-red">Subtotal: 5,180 Yen</p>
								  </div>
								  <div class="confirm">
									<p class="f500">Item Name Item Name Item Name Item Name Item Name ItemName</p>
									<p style="margin-top:10px;">Amount: 3</p>
									<p class="txt-red">Subtotal: 5,180 Yen</p>
								  </div>
								  <div class="confirm">
									<p class="f500">Item Name Item Name Item Name Item Name Item Name ItemName</p>
									<p style="margin-top:10px;">Amount: 3</p>
									<p class="txt-red">Subtotal: 5,180 Yen</p>
								  </div>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="far fa-clipboard"></i> Total amount</label>
							  <div class="r-input">
								  <p>16,360 Yen</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="fas fa-truck"></i> Deliver type</label>
							  <div class="r-input">
								  <p>Domestic</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/10.png"> Deliver method</label>
							  <div class="r-input">
								  <p>Domestic post</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/04.png">Email</label>
							  <div class="r-input">
								  <p>info@sensha-world.com</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="fas fa-cubes"></i> Reason</label>
							  <div class="r-input">
								  <p>The items are not delivered at all</p>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><i class="fas fa-exclamation-triangle"></i> Claim</label>
							  <div class="r-input">
								 <p class="f500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							  </div>
						    </div>
							<div class="line-checkbox">
								<input type="checkbox" id="01" name="selector">
							    <label style="font-weight:600;color:#000;">
									 Currently you have available for shopping. You can add the number not over than your total points.
							    </label>
							</div>
							<div class="row-btn">
							  <a href="claim-completed.php" class="b-blue"><img src="images/icon-check.png" style="width:16px;margin-right:5px;">Confirm</a>
							</div>
					   </div>
					</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

