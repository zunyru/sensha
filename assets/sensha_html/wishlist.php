<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container bg">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span><a href="dashboard.php">Dashboard</a></span><span>Wishlist</span>
			</div>
		    <div class="layout-contain">
				<div class="title-wish">
					 <ul>
						 <li>
							 <p>Your Wishlist</p>
						 </li>
						 <li><span class="txt-b2">Amount</span></li>
						 <li><span class="txt-b2">Domestic</span></li>
						 <li><span class="txt-b2">Oversea</span></li>
					 </ul>
				</div>
				<div class="box-wish">
					<div class="clr list-cart">
						<ul>
							<li>
								<div class="clr c1">
									<div class="pic-product">
										<img src="images/p2.jpg">
									</div>
								</div>
								<div class="clr c2">
									<div class="product-name">
										<p class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</p>
										<div class="available">
											<a href="" class="b-delete">Delete <img src="images/i-delete.png"></a>
										</div>
									</div>
									<div class="amount">
										<select>
											<option value="0">1</option>
											<option value="1">2</option>
											<option value="2">3</option>
											<option value="3">4</option>
											<option value="4">5</option>
										</select>
									</div>
									<div class="box-price">
										<div class="inner-p">
											<span class="txt-b2">Domestic</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
									<div class="clr box-type">
										 <div class="inner-p">
											<span class="txt-b2">Oversea</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
								</div>	
							</li>
							<li class="bg-gray">
								<div class="clr c1">
									<div class="pic-product">
										<img src="images/p1.jpg">
									</div>
								</div>
								<div class="clr c2">
									<div class="product-name">
										<p class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</p>
										<div class="available">
											<a href="" class="b-delete">Delete <img src="images/i-delete.png"></a>
										</div>
									</div>
									<div class="amount">
										<select>
											<option value="0">1</option>
											<option value="1">2</option>
											<option value="2">3</option>
											<option value="3">4</option>
											<option value="4">5</option>
										</select>
									</div>
									<div class="box-price">
										<div class="inner-p">
											<span class="txt-b2">Domestic</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
									<div class="clr box-type">
										 <div class="inner-p">
											<span class="txt-b2">Oversea</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
								</div>	
							</li>
							<li>
								<div class="clr c1">
									<div class="pic-product">
										<img src="images/p2.jpg">
									</div>
								</div>
								<div class="clr c2">
									<div class="product-name">
										<p class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</p>
										<div class="available">
											<a href="" class="b-delete">Delete <img src="images/i-delete.png"></a>
										</div>
									</div>
									<div class="amount">
										<select>
											<option value="0">1</option>
											<option value="1">2</option>
											<option value="2">3</option>
											<option value="3">4</option>
											<option value="4">5</option>
										</select>
									</div>
									<div class="box-price">
										<div class="inner-p">
											<span class="txt-b2">Domestic</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
									<div class="clr box-type">
										 <div class="inner-p">
											<span class="txt-b2">Oversea</span>
											<p class="txt-red">1,990 Yen</p>
											<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
											<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
											<div class="btn">
												<a href="cart.php" class="b-cart">Add to Cart &nbsp;<img src="images/i-cart.png"></a>
											</div>
										</div>
									</div>
								</div>	
							</li>
						</ul>
					</div>
                </div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->
