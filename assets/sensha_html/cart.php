<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span>Body</span>
			</div>
			<!-----------------menu-category-product-mobile------------------------>
			<div id="stick-here"></div>
            <div id="stickThis">
			<div class="menu-mobile">
				<div class="clr m-inner">
          			<h3><i class="fas fa-clipboard-list" style="font-size:17px;margin-right: 5px;"></i> 5 Items In Cart</h3>
					<div class="right">
						<a href="payment.php" style="background:#f9bd00;"><i class="fas fa-money-bill-alt"></i> &nbsp;Buy</a>
					</div>
				</div>
            </div><!--menu-mobile-->
			</div>
			<!-----------------end-menu-category-product-mobile-------------------->
		    <div class="box-content">
				<div class="box-domestic">
				 <div class="title-cart">
					 <ul>
						 <li>
							 <span class="b-gray">Domestic</span>
						 </li>
						 <li><span class="txt-b1">Shopping Cart</span></li>
						 <li><span class="txt-b2">Amount</span></li>
						 <li><span class="txt-b2">Price</span></li>
					 </ul>
				 </div>
				<div class="clr list-cart">
					<ul>
						<li>
							<div class="clr c1">
								<div class="pic-product">
									<a href="product-detail.php"><img src="images/p2.jpg"></a>
								</div>
							</div>
							<div class="clr c2">
								<div class="product-name">
									<h1 class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</h1>
									<div class="available">
										<p>Available</p>
										<span><a href="">Buy later</a></span> | <span><a href="">Delete</a></span>
									</div>
								</div>
								<div class="amount">
									<select>
										<option value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
									</select>
								</div>
								<div class="box-price">
									<p class="txt-red">1,990 Yen</p>
									<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
									<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
								</div>
							</div>	
						</li>
						<li class="bg-gray">
							<div class="clr c1">
								<div class="pic-product">
									<a href="product-detail.php"><img src="images/p1.jpg"></a>
								</div>
							</div>
							<div class="clr c2">
								<div class="product-name">
									<h1 class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</h1>
									<div class="available">
										<p>Available</p>
										<span><a href="">Buy later</a></span> | <span><a href="">Delete</a></span>
									</div>
								</div>
								<div class="amount">
									<select>
										<option value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
									</select>
								</div>
								<div class="box-price">
									<p class="txt-red">1,990 Yen</p>
									<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
									<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
								</div>
							</div>	
						</li>
					</ul>
					<div class="clr subtotal">
						<span>5 items Subtotal</span>
						<p class="txt-red">5,180 Yen</p>
					</div>
				</div>
				<div class="clr box-select-delivery">
					<div class="d1">
						<span class="b-domestic">Domestic</span>
						<span class="txt-delivery">Delivery</span>
					</div>
					<div class="clr select-delivery">
						<ul>
							<li>
								<input type="radio" id="r-one" name="selector">
                                <label for="r-one">
									<p>Domestic post</p>
									<span>Appomixately 3-4 days</span>
									<p class="txt-red">5,180 Yen</p>
								</label>
							</li>
							<li>
								<input type="radio" id="r-two" name="selector">
                                <label for="r-two">
									<p>DHL</p>
									<span>Appomixately 3-4 days</span>
									<p class="txt-red">5,180 Yen</p>
								</label>
							</li>
						</ul>
					</div>
					<div class="clr subtotal">
						<span>Total</span>
						<p class="txt-red">8,180 Yen</p>
					</div>
				</div>
				</div><!--box-domestic-->
				<div class="box-oversea">
				 <div class="title-cart">
					 <ul>
						 <li>
							 <span class="b-gray">Oversea</span>
						 </li>
						 <li><span class="txt-b1">Shopping Cart</span></li>
						 <li><span class="txt-b2">Amount</span></li>
						 <li><span class="txt-b2">Price</span></li>
					 </ul>
				 </div>
				<div class="clr list-cart">
					<ul>
						<li>
							<div class="clr c1">
								<div class="pic-product">
									<img src="images/p2.jpg">
								</div>
							</div>
							<div class="clr c2">
								<div class="product-name">
									<p class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</p>
									<div class="available">
										<p>Available</p>
										<span><a href="">Buy later</a></span> | <span><a href="">Delete</a></span>
									</div>
								</div>
								<div class="amount">
									<select>
										<option value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
									</select>
								</div>
								<div class="box-price">
									<p class="txt-red">1,990 Yen</p>
									<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
									<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
								</div>
							</div>	
						</li>
						<li class="bg-gray">
							<div class="clr c1">
								<div class="pic-product">
									<img src="images/p1.jpg">
								</div>
							</div>
							<div class="clr c2">
								<div class="product-name">
									<p class="name-p">Glass Coating Water-repellent Fine Crystal 800ml by SENSHA</p>
									<div class="available">
										<p>Available</p>
										<span><a href="">Buy later</a></span> | <span><a href="">Delete</a></span>
									</div>
								</div>
								<div class="amount">
									<select>
										<option value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
									</select>
								</div>
								<div class="box-price">
									<p class="txt-red">1,990 Yen</p>
									<div class="row"><span class="import">Import shipping</span><span class="tax">1,200 Yen</span></div>
									<div class="row"><span class="import">Import Tax</span><span class="tax">1,200 Yen</span></div>
								</div>
							</div>	
						</li>
					</ul>
					<div class="clr subtotal">
						<span>5 items Subtotal</span>
						<p class="txt-red">5,180 Yen</p>
					</div>
				</div>
				<div class="clr box-select-delivery">
					<div class="d1">
						<span class="b-domestic">Oversea</span>
						<span class="txt-delivery">Delivery</span>
					</div>
					<div class="clr select-delivery">
						<ul>
							<li>
								<input type="radio" id="01" name="selector">
                                <label for="01">
									<p>Domestic post</p>
									<span>Appomixately 3-4 days</span>
									<p class="txt-red">5,180 Yen</p>
								</label>
							</li>
							<li>
								<input type="radio" id="02" name="selector">
                                <label for="02">
									<p>DHL</p>
									<span>Appomixately 3-4 days</span>
									<p class="txt-red">5,180 Yen</p>
								</label>
							</li>
							<li>
								<input type="radio" id="03" name="selector">
                                <label for="03">
									<p>DHL</p>
									<span>Appomixately 3-4 days</span>
									<p class="txt-red">5,180 Yen</p>
								</label>
							</li>
						</ul>
					</div>
					<div class="clr subtotal">
						<span>Total</span>
						<p class="txt-red">8,180 Yen</p>
					</div>
				</div>
				</div><!--box-domestic-->
			</div><!--box-content-->
          <aside class="col-right">
			  <div class="p-shipping">
			   <div class="total-domestic">
				   <figure><img src="images/icon-cart.png"></figure>
			     <div class="d2">
					   <span class="b-gray">Domestic</span>
					   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
					   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
				   </div>
				   <div style="background:#fff;">
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
						   <li>
							  <p>Minimum Shipping</p>
							  <span>900  Yen</span>
						   </li>
					   </ul>
				     <div class="total clr">
							<p>Total</p>
							<span class="txt-red">8,180 Yen</span>
					   </div>
				   </div>
		       </div>
		    <div class="total-oversea">
				   <div class="d2">
					   <span class="b-gray">Oversea</span>
					   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
					   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
				   </div>
				   <div style="background:#fff;">
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
						   <li>
							  <p>Minimum Shipping</p>
							  <span>900  Yen</span>
						   </li>
					   </ul>
					   <div class="total clr">
							<p>Total</p>
							<span class="txt-red">8,180 Yen</span>
					   </div>
				   </div>
		       </div>
			   <div class="b-payment">
		     		<a href="payment.php">Proceed Payment <img src="images/i-cart.png"></a>
			   </div>
			  <!-- <div class="total clr" style="background:#004ea1;text-align: center;color:#fff;">
					<a href="" data-src="#modal01" class="login">Click to login <img src="images/login.png" style="width:20px;margin-left:7px;"></a>
			   </div>-->
			</div>
	      </aside>
		</div><!--inner-->
		<div class="clr box_recent_purchased">
			<div class="inner">
				<div class="topic">
					<p>Recent purchased items</p>
					<h2>Recommended items bassed on your recent shopping history.</h2>
				</div>
				<div class="clr list-recent">
					<ul class="owl-carousel owl-theme">
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p5.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="clr line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="clr line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p1.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p2.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p3.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<a href="product-detail.php"><img src="images/p4.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr box_recent_view">
			<div class="inner">
				<div class="topic">
					<p>Recent viewed items</p>
					<h2>Recommended items bassed on your recent web browser.</h2>
				</div>
				<div class="clr list-recent">
					<ul class="owl-carousel owl-theme">
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p5.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="clr line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="clr line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p1.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p2.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li class="item">
							<figure>
						    	<a href="product-detail.php"><img src="images/p3.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
						<li>
							<figure>
						    	<a href="product-detail.php"><img src="images/p4.jpg"></a>
							</figure>
							<div class="detail">
								<p class="name-p">Product Name Product Name Product Name</p>
								<p class="quantity">50ml Boxed + accessories</p>
								<div class="clr p-amount">
									<span class="b-gray">Domestic</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
								<div class="clr p-amount">
									<span class="b-gray">Oversea</span>
									<div class="price">1,990 Yen</div>	
									<div class="line-btn">
										<a href="" class="b-cart">Add to Cart <img src="images/i-cart.png"></a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
	<script src="js/owl.carousel.min.js"></script>
    <script>
       $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop:false,
                margin: 15,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 2,
                    nav: false
                  },
                  600: {
                    items: 4,
                    nav: false
                  },
                  1000: {
                    items: 5,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
       })
    </script>
	<script>
		function sticktothetop() {
			var window_top = $(window).scrollTop();
			var top = $('#stick-here').offset().top;
			if (window_top > top) {
				$('#stickThis').addClass('stick');
				$('#stick-here').height($('#stickThis').outerHeight());
			} else {
				$('#stickThis').removeClass('stick');
				$('#stick-here').height(0);
			}
			}
			$(function() {
				$(window).scroll(sticktothetop);
				sticktothetop();
			});
	</script>
</div>
<!-- .wrapper -->
