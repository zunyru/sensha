<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span>News</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="clr page-news">
			<div class="layout-contain">
		    	<div class="news-content-in">
				  <div class="headline">
					<p class="title">SENSHA News</p>
					<h2 align="center">We offer high quality products and reliable services</h2>
				  </div>
				  <ul class="news-content-list">
					
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry.lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry.lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
					<li>
					  <time datetime="">01.04.2019</time>
					  <a href="detail-news.php">lorem Ipsum is simply dummy text of the printing and typesetting industry. lorem Ipsum is simply</a>
					</li>
				  </ul>
			      <section id="pagination">
					  <ul>
						<li class="active"><span>1</span></li>
						<li class="number"><a href="">2</a></li>
						<li class="number"><a href="">3</a></li>
						<li class="number"><a href="">4</a></li>
						<li class="number"><a href="">5</a></li>
					 </ul>
			     </section>
            </div>
			</div><!--layout-contain-->
		</div><!--inner-->
	  </div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

