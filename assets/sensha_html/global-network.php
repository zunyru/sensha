<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper" class="page-network">
     <?php include('include/header.php')?>
	 <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a><span> World network</span></span>
		</div>
	 </div>
     <div class="clr box-network">
		  <div class="inner">
			  <div class="topic2">
				<p class="title-page">World network</p>
			  </div>
			  <p align="center">We offer high quality products and reliable services<br>At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.</p>
			  <div class="box-map">
		      	<img src="images/map.jpg">
				<div class="popup">
					<div class="inner-popup">
						<h3>Japan</h3>
						<p class="company-name">SENSHA CO., LTD</p>
						<p>OFFICE ADDRESS <br>
1007-3, Kami-kasuya, Isehara-shi, Japan<br>
PHONE : (+81) 463-94-5106</p><br>
					</div>
				</div>
			  </div>
			  <div class="clr box_location">
				  <ul>
					  <li>
						  <figure><img src="images/n1.jpg"></figure>
						  <div class="clr name-location">
							  <div class="l-inner">
							  	  <h3>Japan</h3>
								  <p class="company-name">SENSHA CO., LTD</p>
								  <p>OFFICE ADDRESS <br>
1007-3, Kami-kasuya, Isehara-shi, Japan<br>
PHONE : (+81) 463-94-5106</p><br>
							  </div>
							  <div class="btn">
								  <a href="" class="b-map">See google map</a>
							  </div>
						  </div>
					  </li>
					   <li>
						  <figure><img src="images/n2.jpg"></figure>
						  <div class="clr name-location">
							  <div class="l-inner">
							  	  <h3>Japan</h3>
								  <p class="company-name">SENSHA CO., LTD</p>
								  <p>OFFICE ADDRESS <br>
1007-3, Kami-kasuya, Isehara-shi, Japan<br>
PHONE : (+81) 463-94-5106</p><br>
							  </div>
							  <div class="btn">
								  <a href="" class="b-map">See google map</a>
							  </div>
						  </div>
					  </li>
				  </ul>
			  </div>
		  </div>
	  </div>
	 
	  
      




   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

