// JavaScript Document
/*===================================
  view port
===================================*/

$(function(){
  var ua = window.navigator.userAgent;
  if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
    $('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">');
  }else{
    $('head').prepend('<meta name="viewport" content="width=1300">');
  }
});


/*===================================
  hamburger menu , accordion
===================================*/
$(function() {
  function slideMenu() {
    var activeState = $(".header-nav").hasClass("active");
    $(".header-nav").animate(
      {
        left: activeState ? "0%" : "-100%"
      },
      400
    );
  }
  $("#hamburger").click(function(event) {
    event.stopPropagation();
    $("#hamburger-menu").toggleClass("open");
    $(".header-nav").toggleClass("active");
    slideMenu();

    $("body").toggleClass("open-menu");
  });

  $(".accordion-toggle").click(function() {
    var index = $('.accordion-toggle').index(this);
    $('.accordion-content').eq(index).toggleClass("open");
    $(this).toggleClass("active-tab").find(".menu-link").toggleClass("active");
  });

  $(".accordion-ttl").click(function() {
    $(this).next('.accordion-content').slideToggle(500);
  });

});



/*===================================
  menu toggle
===================================*/

$(function(){
  $("#menu_btn").click(function(){
    $(this).toggleClass("active");
    $("#global_navi").toggleClass("active");
	});
});

/*===================================
  anchor margin
===================================*/

$(function(){
  $('a[href^="#"]:not(.cl a[href="#"])').click(function(){

    if( window.matchMedia('(max-width:768px)').matches ){
      //SP
      var speed  = 1000;
      var header = 0;
    }else{
      //PC
      var speed  = 1000;
      var header = 0;
    }

    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top - header;
    $('html,body').animate({scrollTop:position},speed,'swing');
    return false;
  });
});


/*===================================
  accordion
===================================*/








