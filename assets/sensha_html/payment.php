<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span><a href="">Cart</a></span><span>Payment</span>
			</div>
		    <div class="box-content">
				<div class="layout-contain">
					<div class="box-progress">
						<div class="progress-row">
							<div class="progress-step">
								<button type="button" class="btn-circle">1</button>
								<p>Cart item&amp; Delivery</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle">2</button>
								<p>Shipping Address</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle step-active">3</button>
								<p>Confirmation&amp; Payment</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle c-blue">4</button>
								<p>Complete Order</p>
							</div> 
						</div>
					</div>
					<div class="topic">
						<p class="title-page">Choose payment method</p>
					</div>
					<div class="clr payment">
						<ul>
							<li>
								<input type="radio" id="r-one" name="selector">
                                <label for="r-one">
									<p>Amazon <span><img src="images/logo-amazon.png"></span></p>
									<span>Pay with you amazon account</span>
								</label>
							</li>
							<li>
								<input type="radio" id="r-two" name="selector">
                                <label for="r-two">
									<p>Paypal <span><img src="images/logo-paypal.png"></span></p>
									<span>Pay with you amazon account</span>
								</label>
							</li>
							<li>
								<input type="radio" id="r-two" name="selector">
                                <label for="r-two">
									<p>Alipay <span><img src="images/logo-alibaba.png"></span></p>
									<span>Pay with you amazon account</span>
								</label>
							</li>
						</ul>
					</div>
					<div class="clr choose-discount">
						<div class="topic">
							<p class="title-page">Choose discount</p>
					    </div>
						<div class="row-discount">
							<ul>
								<li>
									<div>
										<input type="radio" id="01" name="selector">
										<label for="01">
											<p>To Special to use credit point (USD)</p>
											<span>Currently you have 2,345 USD point available
	for shopping, You can add the number not over than your total points.</span>
										</label>
									</div>
								</li>
								<li>
									<input type="text" placeholder="Please input amount" class="form-control">
								</li>
								<li>
									<a href="" class="b-blue">Apply</a>
								</li>
							</ul>
						</div>
						<div class="row-discount">
							<ul>
								<li>
									<div>
										<input type="radio" id="01" name="selector">
										<label for="01">
											<p>To use all credit point (USD)</p>
											<span>Apply all available credit point for this payment</span>
										</label>
									</div>
								</li>
								<li>
									<input type="text" placeholder="S-3.2-PC" class="form-control">
								</li>
								<li>
									<a href="" class="b-blue">Apply</a>
								</li>
							</ul>
						</div>
						<div class="row-discount">
							<ul>
								<li>
									<div>
										<input type="radio" id="01" name="selector">
										<label for="01">
											<p>Promotion Code</p>
											<span>Apply promotion code</span>
										</label>
									</div>
								</li>
								<li>
									<input type="text" placeholder="Please input Coupon" class="form-control">
								</li>
								<li>
									<a href="" class="b-blue">Apply</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="line-p">
						<a href="complete.php" class="b-yellow">Confirm order
							<img src="images/icon-check.png" style="width:20px;margin-left:5px;">
						</a>
				    </div>
					<div class="clr box_order_confirm">
						<div class="topic">
							<p class="title-page">Confirm order</p>
					    </div>
						<div class="total-domestic">
							 <div class="d2">
								   <span class="b-gray">Domestic</span>
								   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
								   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
							   </div>
							   <div style="background:#fff;">
								   <ul class="list-items">
									   <li>
										  <p>Import shipping</p>
										  <span>1,200 Yen</span>
									   </li>
									   <li>
										  <p>Import Tax</p>
										  <span>820  Yen</span>
									   </li>
									   <li>
										  <p>Minimum Shipping</p>
										  <span>900  Yen</span>
									   </li>
								   </ul>
								 <div class="total clr">
										<p>Total</p>
										<span class="txt-red">8,180 Yen</span>
								   </div>
							   </div>
							</div>
							<div class="total-oversea">
								 <div class="d2">
									 <span class="b-gray">Oversea</span>
									 <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
									 <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
								 </div>
								 <div style="background:#fff;">
									 <ul class="list-items">
										 <li>
											<p>Import shipping</p>
											<span>1,200 Yen</span>
										 </li>
										 <li>
											<p>Import Tax</p>
											<span>820  Yen</span>
										 </li>
										 <li>
											<p>Minimum Shipping</p>
											<span>900  Yen</span>
										 </li>
									 </ul>
									 <div class="total-price">
										 <div class="total clr">
											<p>Total</p>
											<span class="txt-red">8,180 Yen</span>
										 </div>
										 <div class="total clr">
											<p style="float: none;">Grand Total</p>
											<span class="txt-red" style="display: block;float: none;text-align:right;font-size:16px;font-weight: 700;margin-top: 5px;">16,360 Yen</span>
										 </div>
										 <div class="total clr">
											<p>Discount</p>
											<span class="txt-red">6,360 Yen</span>
										 </div>
									 </div>
									 <div class="total">
										<p>Net to pay</p>
										<span class="txt-red" style="display: block;float: none;text-align:right;font-size:16px;font-weight: 700;margin-top: 5px;">10,000 Yen</span>
									 </div>
								 </div>
							 </div>
				            <div class="line-p">
								<a href="complete.php" class="b-yellow">Confirm order
									<img src="images/icon-check.png" style="width:20px;margin-left:5px;">
								</a>
							</div>

					</div>
				</div>
			</div><!--box-content-->
          	<aside class="col-right">
			  <div class="p-shipping">
			   <div class="total-domestic">
				   <figure><img src="images/icon-cart.png"></figure>
			     <div class="d2">
					   <span class="b-gray">Domestic</span>
					   <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
					   <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
				   </div>
				   <div style="background:#fff;">
					   <ul class="list-items">
						   <li>
						      <p>Import shipping</p>
							  <span>1,200 Yen</span>
						   </li>
						   <li>
							  <p>Import Tax</p>
							  <span>820  Yen</span>
						   </li>
						   <li>
							  <p>Minimum Shipping</p>
							  <span>900  Yen</span>
						   </li>
					   </ul>
				     <div class="total clr">
							<p>Total</p>
							<span class="txt-red">8,180 Yen</span>
					   </div>
				   </div>
		        </div>
				<div class="total-oversea">
					 <div class="d2">
						 <span class="b-gray">Oversea</span>
						 <p><span style="font-size:18px;font-weight:600;">5</span> items in cart</p>
						 <p>Subtotal <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
					 </div>
					 <div style="background:#fff;">
						 <ul class="list-items">
							 <li>
								<p>Import shipping</p>
								<span>1,200 Yen</span>
							 </li>
							 <li>
								<p>Import Tax</p>
								<span>820  Yen</span>
							 </li>
							 <li>
								<p>Minimum Shipping</p>
								<span>900  Yen</span>
							 </li>
						 </ul>
						 <div class="total-price">
							 <div class="total clr">
								<p>Total</p>
								<span class="txt-red">8,180 Yen</span>
							 </div>
							 <div class="total clr">
								<p style="float: none;">Grand Total</p>
								<span class="txt-red" style="display: block;float: none;text-align:right;font-size:16px;font-weight: 700;margin-top: 5px;">16,360 Yen</span>
							 </div>
							 <div class="total clr">
								<p>Discount</p>
								<span class="txt-red">6,360 Yen</span>
							 </div>
						 </div>
						 <div class="total-02">
							<p>Net to pay</p>
							<span class="txt-red">10,000 Yen</span>
						 </div>
					 </div>
				 </div>
				 <div class="b-payment">
				 	<a href="complete.php">Confirm and pay <img src="images/icon-check.png"></a>
				 </div>
			</div>
	      </aside>
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

