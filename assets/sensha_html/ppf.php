<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.bxslider.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
      <?php include('include/header.php')?>
	  <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a></span><span>PPF</span>
		</div>
	  </div>
      <div class="top-banner b-ppf">
         <div class="caption-b">
			<p class="head-banner">CAPTIVATE YOU <br>BY REAL CAR BEAUTY</p>
			<h2>WITH PAINT PROTECTION FILM</h2>
			<p>Syncshield is a special paint protection film <br>developed to protect vehicle body surface.</p>
		 </div>
		 <div class="img-ppf">
			  <ul>
				  <li><img src="images/ppf1.png"></li>
				  <li><img src="images/ppf2.png"></li>
				  <li><img src="images/ppf3.png"></li>
			  </ul>
    	 </div>
      </div>
	  <div class="box_about_ppf">
		  <div class="clr wrap">
			  <div class="topic">
				<p class="title-page">WHAT IS PAINT PROTECTION FILM?</p>
			  </div>
			  <div class="inner-ppf">
			  <p align="center">It is popularly called PPF taken from the initial letters of Paint Protection Film. <br>The purpose of PPF is to protect the body surface, prevention of fading colors and scratch protection, etc.</p>
			  <div class="img-ppt">
				<figure><img src="images/protection.svg"></figure>
				<div class="detail-img-ppt">
				  <div class="inner-detail-img-ppt">
					<div class="inner-img">
					  <div class="box-line">
						<div class="line1"></div>
						<div class="line-detail">Exfoliate Layer</div>
					  </div>
					</div><!--inner-img-->
					<div class="inner-img1">
					  <div class="box-line">
						<div class="line1"></div>
						<div class="line-detail">Self Healing Layer</div>
					  </div>
					</div><!--inner-img1-->
					<div class="inner-img2">
					  <div class="box-line">
						<div class="line1"></div>
						<div class="line-detail">Non-yellowing type polyurethane film</div>
					  </div>
					</div><!--inner-img2-->
					<div class="inner-img3">
					  <div class="box-line">
						<div class="line1"></div>
						<div class="line-detail">Adhesive Layer</div>
					  </div>
					</div><!--inner-img3-->
					<div class="inner-img4">
					  <div class="box-line">
						<div class="line1"></div>
						<div class="line-detail">Exfoliate Layer</div>
					  </div>
					</div><!--inner-img4-->
				  </div><!--inner-detail-img-ppt-->
				</div><!--detail-img-ppt-->
			  </div>
			  <p>Most PPF products are from the U.S. and the manufacturers are limted. Within such industry, Syncshield was developed in Japan by SENSHA in cooperation with several Japanese film related manufacturers and will be the new flagship product of the SENSHA Brand.</p>
			  <p>Compared to other popular products in the market, Syncshield has many unique performance enhanced features added. Its on a performance level of its own.</p>
			</div>
			<div class="row type-car">
				<ul>
				  <li> 
					<img src="images/q01.png">
					<p>Self-recovery</p>
				  </li>
				  <li> 
					<img src="images/q02.png">
					<p>Anti-fouling</p>
				  </li>
				  <li>
					<img src="images/q03.png">
					<p>Water repellent</p>
				  </li>
				  <li> 
					<img src="images/q04.png">
					<p>Oil repellent</p>
				  </li>
				  <li> 
					<img src="images/q05.png">
					<p>Anti-solvent</p>
				  </li>
				</ul>
      		</div>
			<div class="row w-search">
				<div class="topic">
					<p class="title-page">SERIAL SEARCH</p>
			    </div>
				
				<div class="row serial">
				  <form id="formSerial2">
					<input id="input-serial-2" class="long" placeholder="Serial No.">
					<a class="btn-serial btn-serial-2" data-fancybox="" data-src="#hidden-content" href="javascript:;">Search <i class="fa fa-search" aria-hidden="true"></i></a>
				  </form>
				</div>
				<p>Each Syncshield has a serial number assigned. Customers may receive the serial number information from the SENSHA family shop that served you. Enter the number in the Serial Search box and it will show results containing the shipped date from Japan, the arrival date in your country, service shop name, your car model and body color. If the serial number you enter shows result of "Not Found", then the PPF you purchaesed is not an authenticated one. By confirming the authenticity through this system, we guarantee it is 100% made by SENSHA.</p>
		  </div>
		  </div>
	  </div><!--box_about_ppf-->
	  <div class="clr box_vdo">
		  <div class="wrap">
			  <div class="topic2">
				   <p class="title-page">QUALITY TEST</p>
			  </div>
			  <div class="vdo">
	      	  <img src="images/vdo-img.png">
			  </div>
		  </div>
	  </div>
      <div class="clr box_how_ppf">
		 <div class="wrap">
			 <div class="topic2">
				 <p class="title-page">HOW PPF WORKS</p>
			 </div>
			 <p style="margin:30px 0;" align="center">This video shows the performance of our Syncshield compared against other products.</p>
			 <div class="list-ppf">
				 <ul>
				   <li>
						 <div class="img-vdo">
       				        <img src="images/works01.jpg">
					     </div>
						 <div class="detail">
							 <h3>LUSTROUS</h3>
							 <p>This picture shows Syncshield pasted on a dark blue car body panel. As you see, an old and dull looking body panel (right half side of picture) is revived with luster and looking like a high-class brand new body (left half side of picture). By applying Syncshield, you can restore the luster and shine of the body surface on your car just like brand new. You can experience extra-shine if your car is new, a deeper luster if it has condensed color.</p>
						 </div>
					 </li>
					 <li>
						 <div class="img-vdo">
              				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/RDGvFgI_Gp4" frameborder="0" allowfullscreen=""></iframe>
            			 </div>
						 <div class="detail">
							 <h3>SCRATCH RESISTANCE, THE "SELF HEALING EFFECT"</h3>
							 <p>The most unique character of Syncshield is its scratch resistancy. When you get fine scratches, our PPF carries a "Self Healing Effect" to prevent fine scratches from getting on your car. This is called Soft Coating which is a brand new technology born from the completely opposite idea of Hard Coating used for protection films for smart phones and windshields and fixes the scratches immediately (Please see video, the scratches caused by wire brush is fixed immediately). Other brand products require heat to fix the scratches, whereas Syncshield heals under normal temperature. No hot air or hot water necessary!</p>
						 </div>
					 </li>
					  <li>
						 <div class="img-vdo">
              				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/9wy5r2dliuM" frameborder="0" allowfullscreen=""></iframe>
            			 </div>
						 <div class="detail">
							 <h3>WATER REPELLENCY</h3>
							 <p>Syncshield has excellent water repellency. The result of water contact angle test was 110℃, which is the level equivalent to water repellent glass coating. No need for glass coating or waxing.</p>
						 </div>
					 </li>
					 <li>
						 <div class="img-vdo">
              				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/SBNK67sYAA4" frameborder="0" allowfullscreen=""></iframe>
            			 </div>
						 <div class="detail">
							 <h3>OIL REPELLENCY</h3>
							 <p>Tars and pitches from asphalt roads stick on the vehicle body. Oily stains from exhaust gas also is a reason for the vehicle to get dirty. To prevent these stubborn stains, we strengthened oil repellency also. Contact angle test using hexadecane resulted in 65℃. This video shows what happens when aerozole engine parts cleaner is sprayed on our PPF.</p>
						 </div>
					 </li>
					 <li>
						 <div class="img-vdo">
              				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/n0SmqCsOKcI?start=36" frameborder="0" allowfullscreen=""></iframe>
            			 </div>
						 <div class="detail">
							 <h3>SOLVENT RESISTANCE</h3>
							 <p>Vehicles are exposed not only to tars and pitches, but also to other liquid like maintenance cleaners. Gasoline, etc. Many of these liquid contain organic solvent which cause severe blemishes to PPF. Syncshield strengthened solvent resistancy in addition to the oil repellency. This video features the tests conducted to showcase the different performances level of Syncshield.</p>
						 </div>
					 </li>
				 </ul>
			 </div>
		 </div>  
	  </div>
	  <div class="clr box_about_ppf2">
		  <div class="wrap">
			  <div class="topic2">
				 <p class="title-page">ABOUT PPF</p>
			  </div>
			  <p style="margin:30px 0 40px;" align="center">This video shows the performance of our Syncshield compared against other products.</p>
			  <div class="clr box-1">
				 <figure>
			      	 <img src="images/a-ppf1.jpg">
				 </figure>
				 <div class="detail">
					 <h3>MATERIAL</h3>
					 <p>Generally, PVC or TPU was used as the base material (film) for PPF. The positive for PVC is the low cost of production. On the other hand, the negative is the weak weatherability, not much fit as the material for PPF.</p>
				 </div>
			  </div>
			  <div class="clr box-2">
				 <div class="col-3">
					 <div class="inner-box">
					 	<h3>TRANSPARANCY</h3>
					 	<p>High transparancy is a must for basic feature. We strongly focused on high transparency upon development in order to keep the texture of body colors different by each model. No one will notice that PPF is applied and being unnoticed is a very important basic feature for a PPF.</p>
				     </div>
				 </div>
				 <div class="col-3">
				     <img src="images/a-ppf2.jpg">
				 </div>
				 <div class="col-3">
					 <div class="inner-box">
					 	<h3>SMOOTHNESS</h3>
					 	<p>The ultimate smoothness Syncshield pursued is one of a mirror surface. We realized such smoothness.The ultimate smoothness, so no one will notice that PPF is applied, is a very important feature for a PPF.</p>
				     </div>
				 </div>
			  </div>
	      </div>
	  </div>
      <div class="clr box_protection">
	     <div class="wrap">
		   <div class="left">
			   <h2>PAINT<span>PROTECTION</span><span>FILM</span></h2>
		   </div>
		   <div class="right">
		      <div class="clr box-p">
				  <figure>
			      	 <img src="images/a-ppf3.jpg">
				  </figure>
				  <div class="detail">
					 <div class="inner-box">
					 	<h3>FLEXIBILITY</h3>
					 	<p>Syncshield's urethane film was developed focusing on the suppleness to follow the sophisticated 
tertiary curved surface of a car body. This allowed for application without excessive pulling or
additional heat and realized higher durability and anti-chipping performance.</p>
				     </div>
				  </div>
		      </div>
			  <div class="clr box-p">
				  <figure>
			      	 <img src="images/a-ppf4.jpg">
				  </figure>
				  <div class="detail">
					 <div class="inner-box">
					 	<h3>NON-YELLOWING</h3>
					 	<p>TPU used for Syncshield is a non-yellowing type material with one of the highest quality.</p>
				     </div>
				  </div>
		      </div>
			  <div class="clr box_adhesive">
				 <div class="topic2">
				 	<p class="title-page" style="color:#f7bb00;">ADHESIVE</p>
			     </div>
				  <p>The adhesive used for Syncshield was selected from 200 different types and adjusted to match a flexible urethane film. The adhesive is made to reduce the adhesion when applying, but keep strong adhesion after application to prevent from peeling off, which is one of the many unique characteristics of Syncshiled.</p>
			  </div>
		   </div>
		 </div>
	  </div>
      <div class="clr box_caution">
		  <div class="wrap">
			  <div class="topic2" style="text-align:left;">
				 <p class="title-page" style="color:#f7bb00;">CAUTION!</p>
			  </div>
		      <div class="clr inner-caution">
				  <div class="left">
					  <ul>
						  <li> - Use dedicated mounting liquid upon application of Syncshield.</li>
						  <li> - Please use with SENSHA Brand products upon maintenance, daily car care and car wash.</li>
						  <li> * We cannot assure quality when used with other Brand products and will not be part of the warranty.</li>
					  </ul>
				  </div>
				  <div class="right">
			    	  <img src="images/car3.png">
				  </div>
			  </div>
		  </div>
	  </div>





   	<?php include('include/footer.php')?> 
	<script>

		if ($('#gallery-thumbs').length > 0) {

		// Cache the thumb selector for speed
		var thumb = $('#gallery-thumbs').find('.thumb');

		// How many thumbs do you want to show & scroll by
		var visibleThumbs = 5;

		// Put slider into variable to use public functions
		var gallerySlider = $('#vdo').bxSlider({
			controls: false,
			pager: false,
			easing: 'easeInOutQuint',
			infiniteLoop: false,
			speed: 500,
			adaptiveHeight:true,
			auto: true,
			onSlideAfter: function (currentSlideNumber) {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				thumb.removeClass('pager-active');
				thumb.eq(currentSlideNumber).addClass('pager-active');
			},

			onSlideNext: function () {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				slideThumbs(currentSlideNumber, visibleThumbs);
			},

			onSlidePrev: function () {
				var currentSlideNumber = gallerySlider.getCurrentSlide();
				slideThumbs(currentSlideNumber, visibleThumbs);
			}
		});

		// When clicking a thumb
		thumb.click(function (e) {

			// -6 as BX slider clones a bunch of elements
			gallerySlider.goToSlide($(this).closest('.thumb-item').index());

			// Prevent default click behaviour
			e.preventDefault();
		});

		// Thumbnail slider
		var thumbsSlider = $('#gallery-thumbs').bxSlider({
			controls:true,
			pager:false,
			adaptiveHeight:true,
			easing: 'easeInOutQuint',
			infiniteLoop: false,
			minSlides: 5,
			maxSlides: 2,
			slideWidth: 360,
			slideMargin: 10
		});

		// Function to calculate which slide to move the thumbs to
		function slideThumbs(currentSlideNumber, visibleThumbs) {

			// Calculate the first number and ignore the remainder
			var m = Math.floor(currentSlideNumber / visibleThumbs);

			// Multiply by the number of visible slides to calculate the exact slide we need to move to
			var slideTo = m * visibleThumbs;

			// Tell the slider to move
			thumbsSlider.goToSlide(m);
		}

		// When you click on a thumb
		$('#gallery-thumbs').find('.thumb').click(function () {

			// Remove the active class from all thumbs
			$('#gallery-thumbs').find('.thumb').removeClass('pager-active');

			// Add the active class to the clicked thumb
			$(this).addClass('pager-active');

		});
	}
	</script>
  </div>  
<!-- .wrapper -->

