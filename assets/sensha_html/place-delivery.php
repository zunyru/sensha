<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span>Address</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
		    <div class="box-content">
				<div class="layout-contain">
					<div class="box-progress">
						<div class="progress-row">
							<div class="progress-step">
								<button type="button" class="btn-circle">1</button>
								<p>Cart item& Delivery</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle step-active">2</button>
								<p>Shipping Address</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle c-blue">3</button>
								<p>Confirmation& Payment</p>
							</div>
							<div class="progress-step">
								<button type="button" class="btn-circle c-blue">4</button>
								<p>Complete Order</p>
							</div> 
						</div>
					</div>
					<div class="topic">
						<p class="title-page">Use registered shipping  address</p>
					</div>
					<div class="clr box-shipping-address">
						<div class="left">
							<h3>Nobuaki Oka</h3>
							<p>126/2 Soi Ratcgawithi2, Samsennai, Phayathai, Bangkok 10400 Thailand</p>
							<p>Tel :  +81 87975 1234</p>
						</div>
					    <div class="right">
						    <div class="row">
							  <a href="" class="b-blue"><img src="images/icon-check.png" width="31" style="width: 20px;margin-right: 10px;">Use this address</a>
							</div>
						    <div class="row">
								<a href="" class="btn-gray"><img src="images/i-edit.png" width="31" style="width: 20px;margin-right: 10px;">
									Edit the address
								</a>
							</div>
						</div>
					</div>
					<div class="clr box_form">
						<div class="topic">
							<p class="title-page">Change registered address and use new address <span>( If in japan, this does not appear )</span></p>
					    </div>
						<div class="box-inner">
							<div class="r-inline">
							  <label class="label"><img src="images/01.png">Country</label>
							  <div class="r-input">
									<input type="text" placeholder="Thailand" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/02.png">Name</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/03.png">Company</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/04.png">Zipcode</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/05.png">Address</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/06.png">State</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/07.png">Telephone</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control">
							  </div>
						    </div>
							<div style="text-align: center;margin:40px 0 0 0">
							  <a href="" class="b-blue"><img src="images/icon-check.png" width="31" style="width: 20px;margin-right: 10px;">Over write and use new address</a>
							</div>
					   </div>
					</div>
				</div><!--layout-contain-->
			    
			</div><!--box-content-->
            
		</div><!--inner-->
		
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

