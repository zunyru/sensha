<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span>Register</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="layout-contain">
				 <div class="clr box_form">
					<div class="topic">
						<p class="title-page">Registration form</p>
					</div>
					<div class="box-paragraph">
						<p>Please choose the authentication method to prove you are not robot. <br>And fill the below registration form to proceed to shopping with SENSHA</p>
					</div>
					<div class="box-inner">
						    <div class="clr inline-radio">
							  <div class="left">
								  <input type="radio" id="01" name="selector">
								  <label class="">
									  Authenticate via email
								  </label>
							  </div>
							  <div class="left">
								  <input type="radio" id="01" name="selector">
								  <label class="">
									  Authenticate via SMS
								  </label>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/04.png">Email</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your Email" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/07.png">Telephone</label>
							  <div class="r-input">
								 <input type="text" placeholder="Please input your Telephone" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/08.png">Password</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your Password" class="form-control">
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/01.png">Country</label>
							  <div class="r-input">
								<select class="form-control">
									<option value="0">Country select</option>
									<option value="1">2</option>
									<option value="2">3</option>
									<option value="3">4</option>
									<option value="4">5</option>
								</select>
							  </div>
						    </div>
							<div class="r-inline">
							  <label class="label"><img src="images/06.png">State</label>
							  <div class="r-input">
								<select class="form-control">
									<option value="0">If Japan</option>
									<option value="1">2</option>
									<option value="2">3</option>
									<option value="3">4</option>
									<option value="4">5</option>
								</select>
							  </div>
						    </div>
							<div class="row-btn">
							  <a href="register-confrimation.php" class="b-blue"><img src="images/icon-save.png" style="width:16px;margin-right:5px;">Save</a>
							</div>
					 </div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

