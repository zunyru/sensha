<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a></span><span>Contact</span>
			</div>
		</div>
		<div class="inner">
		<div class="clr page-contact">
			<div class="topic">
				<p class="title-page">CONTACT</p>
			</div>
			<div class="clr box-contact">
					   <div class="right">
						<ul>
							<li>
								<figure><img src="images/i1.png"></figure>
								<div class="detail">
									<p class="title">HEAD OFFICE</p>
									<p>SENSHA CO., LTD</p>
								</div>
							</li>
							<li>
								<figure><img src="images/i2.png"></figure>
								<div class="detail">
									<p class="title">OFFICE ADDRESS</p>
									<p>1007-3, Kami-kasuya, Isehara-shi, Japan</p>
								</div>
							</li>
							<li>
								<figure><img src="images/i3.png"></figure>
								<div class="detail">
									<p class="title">PHONE</p>
									<p>(+81) 463-94-5106</p>
								</div>
							</li>
						</ul>
					  </div>
					  <div class="left">
						  <div class="r-inline">
							  <label class="label"><img src="images/02.png">Name</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your Name" class="form-control">
							  </div>
						  </div>
						  <div class="r-inline">
							  <label class="label"><img src="images/04.png">Email</label>
							  <div class="r-input">
								<input type="text" placeholder="Please input your Email" class="form-control">
							  </div>
						  </div>
						  <div class="r-inline">
							  <label class="label"><img src="images/11.png">Message</label>
							  <div class="r-input">
								<textarea rows="5" placeholder="Please input your Message" class="form-control"></textarea>

							  </div>
						  </div>
			  			   <div class="row-btn">
							  <a href="register-confrimation.php" class="b-blue">Submit</a>
							</div>
					  </div>
			</div>
	  </div>
	  </div>
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

