<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
     <?php include('include/header.php')?>
	 <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a><span>Q & A</span></span>
		</div>
	 </div>
     <div class="page-qa">
		<div class="inner">
		  <div class="topic2">
		     <p class="title-page">Q&A: Support for frequent questions</p>
		  </div>
		  <p class="title-detail">We offer high quality products and reliable services<br>At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.</p>
		  <div class="clr box_category_qa">
				<ul>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q1.jpg"></figure>
							<div class="detail">
								<p class="name-category">SENSHA</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q2.jpg"></figure>
							<div class="detail">
								<p class="name-category">Products</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q3.jpg"></figure>
							<div class="detail">
								<p class="name-category">Order / Payment</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q4.jpg"></figure>
							<div class="detail">
								<p class="name-category">Delivery</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q5.jpg"></figure>
							<div class="detail">
								<p class="name-category">Return product</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q6.jpg"></figure>
							<div class="detail">
								<p class="name-category">Discount</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q7.jpg"></figure>
							<div class="detail">
								<p class="name-category">Account</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
					<li>
						<a href="qa-category.php">
							<figure><img src="images/icon-q8.jpg"></figure>
							<div class="detail">
								<p class="name-category">The others</p>
								<p>Pay with your amazon account</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
			<div class="clr box-frequent">
				<div class="topic">
					<p class="title-page">Frequent question</p>
			    </div>
				<div class="clr inner-frequent">
					<div class="col-4">
						<h3>SENSHA</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Products</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Order / Payment</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							
						</ul>
					</div>
					<div class="col-4">
						<h3>Delivery</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Return</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Discount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Avgount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>The others</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
				</div>
			</div>
	   </div>
	  </div>
	 
	  
      




   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

