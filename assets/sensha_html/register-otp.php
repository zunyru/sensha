<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/menu-mobile.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
<div id="wrapper">
	<?php include('include/header.php')?>
    <div class="container">
		<div class="clr inner">
			<div id="breadcrumbs">
				<span><a href="index.php">Home</a><span>Registration confirmation</span>
			</div>
		</div><!--inner-->
		<div class="clr inner">
			<div class="layout-contain">
				 <div class="clr box_form">
					<div class="topic">
						<p class="title-page">Registration confirmation</p>
					</div>
					<div class="box-paragraph">
						<p>Please confirm the subscription by entering the OTP number from your phone number.</p>
					</div>
					<div class="box-inner">
						<div class="r-inline">
							 <label class="label"><img src="images/08.png">OTP</label>
							 <div class="r-input">
								<input type="text" placeholder="Please enter your OTP Number" class="form-control">
							 </div>
					    </div>
						
						<div class="row-btn">
							 <a href="#" class="c-gray"><img src="images/mail.png" style="width:18px;margin-right:8px;">Send again</a>
							 <a href="password-successfully.php" class="b-blue"><img src="images/icon-check.png" style="width:16px;margin-right:5px;">Confirm</a>
						</div>
				   </div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
    <script src="js/main.js"></script> 
	<?php include('include/footer.php')?>
</div>
<!-- .wrapper -->

