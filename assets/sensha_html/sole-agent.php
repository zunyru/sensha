<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper" class="page-agent">
      <?php include('include/header.php')?>
	  <div class="clr inner">
		<div id="breadcrumbs" style="margin:15px 0;">
			<span><a href="index.php">HOME</a><span> Sole Agent Wanted</span></span>
		</div>
	  </div>
      <div class="top-banner b-sole-agent">
         <div class="caption-b">
			<h2>Sole Agent Wanted</h2>
			<p>We offer high quality products and reliable services<br>At SENSHA, we believe that the superior we do our jobs,<br>the superior you are satisfied.</p>
		 </div>
      </div>
	  <div class="clr box-sole">
		  <div class="wrap">
			  <div class="topic2">
				<p class="title-page">Sole Agent Wanted</p>
			  </div>
			  <p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car. With itsability to produce over 700 horsepower, and unleash a roar that literally makes the earth quake, this purpose-built race car really gets our minds racing. Our teams at Calty Design and Toyota Racing Development</p>
		  </div>
	  </div>
	  <div class="page-s-agent">
		  <div class="wrap">
			  <div class="clr box-benefits">
				  <div class="topic2">
					<p class="title-page">Benefits for our sole agent</p>
				  </div>
				  <ul>
					  <li>
						  <figure><img src="images/b1.jpg"></figure>
						  <div class="detail">
							<p class="b-title">Benefit Title</p>
							<p>The racetrack is where we live. It’s where
we prove our commitment to making everbetter
cars. That’s why we’re proud to
announce our latest creation for the
racetrack: the 2019 Supra Xfinity</p>
						  </div>
					  </li>
					   <li>
						  <figure><img src="images/b2.jpg"></figure>
						  <div class="detail">
							<p class="b-title">Benefit Title</p>
							<p>The racetrack is where we live. It’s where
we prove our commitment to making everbetter
cars. That’s why we’re proud to
announce our latest creation for the
racetrack: the 2019 Supra Xfinity</p>
						  </div>
					  </li>
					   <li>
						  <figure><img src="images/b3.jpg"></figure>
						  <div class="detail">
							<p class="b-title">Benefit Title</p>
							<p>The racetrack is where we live. It’s where
we prove our commitment to making everbetter
cars. That’s why we’re proud to
announce our latest creation for the
racetrack: the 2019 Supra Xfinity</p>
						  </div>
					  </li>
				  </ul>
			  </div>
		  </div>
		  <div class="clr box-flow">
			  <div class="wrap">
				  <div class="topic2">
					<p class="title-page">The flow to be a SENSHA agent</p>
				  </div>
				  <ul>
					  <li>
						  <figure><img src="images/s1.jpg"></figure>
						  <div class="detail">
							  <p class="step">STEP 01</p>
							  <h2>The 2019 Supra Xfinity Series Race Car</h2>
							  <p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity. The racetrack is where we live. It’s where we prove our commitment to making ever-better cars.</p>
						  </div>
					  </li>
					  <li>
						  <figure><img src="images/s2.jpg"></figure>
						  <div class="detail">
							  <p class="step">STEP 02</p>
							  <h2>The 2019 Supra Xfinity Series Race Car</h2>
							  <p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity. The racetrack is where we live. It’s where we prove our commitment to making ever-better cars.</p>
						  </div>
					  </li>
					   <li>
						  <figure><img src="images/s3.jpg"></figure>
						  <div class="detail">
							  <p class="step">STEP 03</p>
							  <h2>The 2019 Supra Xfinity Series Race Car</h2>
							  <p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity. The racetrack is where we live. It’s where we prove our commitment to making ever-better cars.</p>
						  </div>
					  </li>
					   <li>
						  <figure><img src="images/s4.jpg"></figure>
						  <div class="detail">
							  <p class="step">STEP 04</p>
							  <h2>The 2019 Supra Xfinity Series Race Car</h2>
							  <p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity. The racetrack is where we live. It’s where we prove our commitment to making ever-better cars.</p>
						  </div>
					  </li>
				  </ul>
			  </div>
		  </div>
	  </div>
      <div class="clr box_apply">
		  <div class="wrap">
			  <div class="topic2">
				<p class="title-page">Apply for a SENSHA agent</p>
			  </div>
			  <p style="font-weight:600;text-align:center;">We offer high quality products and reliable services</p>
			  <div class="inner-apply">
				  <div class="r-inline">
					<label class="label"><img src="images/02.png">Name</label>
					<div class="r-input">
					  <input type="text" placeholder="Please input your Name" class="form-control">
					</div>
				  </div>
				  <div class="r-inline">
					<label class="label"><img src="images/04.png">E-mail</label>
					<div class="r-input">
					  <input type="text" placeholder="Please input your E-mail" class="form-control">
					</div>
				  </div>
				  <div class="r-inline">
					<label class="label"><img src="images/07.png">Telephone</label>
					<div class="r-input">
					  <input type="text" placeholder="Please input your Telephone" class="form-control">
					</div>
				  </div>
				  <div class="r-inline">
					<label class="label"><img src="images/01.png">Country</label>
					<div class="r-input">
					  <select class="form-control">
						<option value="0">Country select</option>
						<option value="1">2</option>
						<option value="2">3</option>
						<option value="3">4</option>
						<option value="4">5</option>
					  </select>
					</div>
				  </div>
				  <div class="r-inline">
					<label class="label"><img src="images/11.png">Message</label>
					<div class="r-input">
					  <textarea class="form-control" rows="3" placeholder="Comment"></textarea>
					</div>
				  </div>
				  <div class="row-btn"> <a href="register-confrimation.php" class="b-blue">Send</a> </div>
			</div>
		  </div>
	  </div>




   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

