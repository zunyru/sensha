<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
     <?php include('include/header.php')?>
	 <div class="clr inner">
		<div id="breadcrumbs"style="margin:15px 0;">
			<span><a href="index.php">HOME</a></span><span><a href="qa-category.php">QA</a></span><span>...............</span>
		</div>
	 </div>
     <div class="clr page-qa">
		<div class="clr inner">
		  <div class="topic2">
			<p class="title-page">Question aout SENSHA</p>
		  </div>
		  <div class="clr inner-qa">
			<aside class="col-left">
				<div class="box_menu">
					<div class="a_menu">
						<div class="topic">
						  <p>Q&A category</p>
		  			    </div>
						<ul>
							<li><a href="">SENSHA</a></li>
							<li><a href="">Products</a></li>
							<li><a href="">Order / Payment</a></li>
                            <li><a href="">SENSHA</a></li>
							<li><a href="">Products</a></li>
							<li><a href="">Order / Payment</a></li>
						</ul>
					</div>
				</div>
			</aside>
			<div class="content">
				<div class="answer-list">
					<div class="topic-qa">
						<figure><img src="images/icon-qa.png"></figure>
						<h3>What is SENSHA CO., LTD.? What kinda products SENSHA deliver over the world?</h3>
					</div>
					<div class="inner-answer">
						<p>The racetrack is where we live. It’s where we prove our commitment to making ever-better cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity Series Race Car. With its ability to produce over 700 horsepower, and unleash a roar that literally makes the earth quake, this purpose-built race car really gets our minds racing. Our teams at Calty Design and Toyota Racing Development worked hand-in-hand with NASCAR 24 to ensure the iconic race car shape was safe, powerful, and ready to take this car to speeds in excess of 200 mph. It’s set to shake up racing when it makes its ontrack debut at the NASCAR Xfinity Series on February 16, 2019.</p>
					</div>
				</div>
			</div>
		  </div>
		  <div class="clr box-frequent">
				<div class="topic">
					<p class="title-page">Frequent question</p>
			    </div>
				<div class="clr inner-frequent">
					<div class="col-4">
						<h3>SENSHA</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Products</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Order / Payment</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							
						</ul>
					</div>
					<div class="col-4">
						<h3>Delivery</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Return</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Discount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>Avgount</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
					<div class="col-4">
						<h3>The others</h3>
						<ul>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
							<li><a href="">This is a sample of frequent question.</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			
	   </div>
	 </div>
	 
   	<?php include('include/footer.php')?>  
  </div>  
<!-- .wrapper -->

