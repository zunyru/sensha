<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title>SENSHA</title>
<meta name="description" content="">
<meta name="keywords"    content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="css/animation.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/common.css">
<link rel="stylesheet" href="css/page.css">
<link rel="stylesheet" href="css/jquery.fancybox.css">
<link rel="stylesheet" href="css/jquery.bxslider.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
</head>
<body class>
  <div id="wrapper">
    <div id="content">
      <?php include('include/header.php')?>
      <div class="top-slide">
        <section id="video" class="clr">
	      <div class="youtube">
			<iframe width="560" height="315" src="https://www.youtube.com/embed/50ymfER43ZA?controls=0&amp;start=170&autoplay=1&mute=1&playlist=50ymfER43ZA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	      </div>
		  <div class="caption-banner">
			<p class="copy_big">The Sensha <br>world </p>
			<h2>We offer high quality products <br>and reliable services </h2>
		  </div>
	    </section>
		<div class="clr box-s-product">
			<div class="inner">
				<ul class="bxslider">
					<li>
						<img src="images/product.png">
						<div class="product-slider">
							<h2>Product Name Product</h2>
							<p>50ml Boxed + accessories</p>
							<div class="clr p-amount">
								<span class="b-gray" style="background:#004e9f;color:#fff;">Domestic</span>
								<div class="price" style="color:#000;">
									1,990 Yen
								</div>
							</div>
							<div class="clr p-amount">
								<span class="b-gray" style="background:#004e9f;color:#fff;">Oversea</span>
								<div class="price" style="color:#000;">
									1,990 Yen
								</div>
							</div>
						</div>
					</li>
					<li>
						<img src="images/product.png">
						<div class="product-slider">
							<h2>Product Name Product</h2>
							<p>50ml Boxed + accessories</p>
							<div class="clr p-amount">
								<span class="b-gray" style="background:#004e9f;color:#fff;">Domestic</span>
								<div class="price" style="color:#000;">
									1,990 Yen
								</div>
							</div>
							<div class="clr p-amount">
								<span class="b-gray" style="background:#004e9f;color:#fff;">Oversea</span>
								<div class="price" style="color:#000;">
									1,990 Yen
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
      </div>
<!-- .top-slide -->
      <div class="outline">
        <section class="brand-list">
          <div class="inner">
            <div class="brand-list-in">
              <div class="headline">
                <p class="title">THE SENSHA WORLD</p>
                <h1>We offer high quality products and reliable services</h1>
              </div>
              <ul class="brand-card">
                <li>
                  <a href="about.php"></a>
                    <img src="images/top/img_brand01.jpg" alt="">
                    <div class="des">
                      <p class="ttl">About SENSHA</p>
                      <p class="txt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                    </div>
                </li>
                <li>
                  <a href="ppf.php"></a>
                    <img src="images/top/img_brand02.jpg" alt="">
                    <div class="des">
                      <p class="ttl">About PPF</p>
                      <p class="txt">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    </div>
                </li>
              </ul>
            </div>
          </div>
        </section>
<!-- .brand-list -->
        <section class="products-lineup">
          <div class="inner">
            <div class="products-lineup-in">
              <div class="headline">
                <p class="title">Search for SENSHA Products</p>
                <h2>We offer high quality products and reliable services</h2>
              </div>
              
              <div class="product-list-wrap">
                <ul class="products-list">
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products01.png" alt="PPF  (18)"></figure><span>PPF  (18)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products02.png" alt="Body  (16)" class="hvr-bob"></figure><span>Body  (16)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products03.png" alt="Window  (10)" class="hvr-bob"></figure><span>Window  (10)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products04.png" alt="Wheel / Tire  (12)" class="hvr-bob"></figure><span>Wheel / Tire  (12)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products05.png" alt="Mall/Bumper  (8)" class="hvr-bob"></figure><span>Mall/Bumper  (8)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products06.png" alt="Engine room  (6)" class="hvr-bob"></figure><span>Engine room  (6)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products07.png" alt="Interior  (10)" class="hvr-bob"></figure><span>Interior  (10)</span></a></li>
                  <li><a href="">
                    <figure class="hvr-float-shadow"><img src="images/top/ico-products08.png" alt="The other  (20)" class="hvr-bob"></figure><span>The other  (20)</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </section>
<!-- .products-lineup -->
        <section class="select-films">
          <div class="inner">
            <div class="select-films-in">
              <div class="headline">
                <p class="title">Search for Syncshield cut films</p>
                <h2>We offer high quality products and reliable services</h2>
              </div>

              <div class="select-list-wrap cf">

                <ul class="select-list">
                  <li class="maker">
                    <span class="icon-maker"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Maker">Select Maker</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="model">
                    <span class="icon-model"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Model">Select Model</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="part">
                    <span class="icon-part"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Part">Select Part</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                  <li class="year">
                    <span class="icon-year"></span>
                    <form>
                      <select name="Select Maker">
                        <option value="Select Year">Select Year</option>
                        <option value="bbbbbbbbbb">bbbbbbbbbb</option>
                        <option value="cccccccccc">cccccccccc</option>
                        <option value="dddddddddd">dddddddddd</option>
                      </select>
                    </form>
                  </li>
                </ul>
                <div class="serch-btn">
                  <button type="button" id="sbtn2">
                    <span>Search</span>
                    <span class="icon-search02 ico-glass"></span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
<!-- .select-films -->
        <section class="news-content">
          <div class="inner">
            <div class="news-content-in">
              <div class="headline">
                <p class="title">SENSHA News</p>
                <h2>We offer high quality products and reliable services</h2>
              </div>

              <ul class="news-content-list">
                <li>
                  <time datetime="">01.04.2019</time>
                  <a href="">lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                </li>
                <li>
                  <time datetime="">01.04.2019</time>
                  <a href="">lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                </li>
                <li>
                  <time datetime="">01.04.2019</time>
                  <a href="">lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                </li>
                <li>
                  <time datetime="">01.04.2019</time>
                  <a href="">lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
                </li>
              </ul>
              <div class="news-btn">
                <a href="news.php" class="btn btn-seeall">See All</a>
              </div>
            </div>
          </div>
        </section>
<!-- .news-content -->
        <section class="content-list">
          <div class="inner">
            <div class="content-list-in">
              <ul class="content-card">
                <li>
                  <a href="global-network.php"></a>
                    <img src="images/top/img_contents01.jpg" alt="">
                    <div class="des">
                      <p class="ttl">GLOBAL NETWORK</p>
                      <p class="txt">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                    </div>
                </li>
                <li>
                  <a href="sole-agent.php"></a>
                    <img src="images/top/img_contents02.jpg" alt="">
                    <div class="des">
                      <p class="ttl">SOLE AGENT</p>
                      <p class="txt">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                    </div>
                </li>
                <li>
                  <a href="qa.php"></a>
                    <img src="images/top/img_contents03.jpg" alt="">
                    <div class="des">
                      <p class="ttl">SENSHA Q&A</p>
                      <p class="txt">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                    </div>
                </li>
              </ul>
            </div>
          </div>
        </section>
<!-- .content-list -->
      </div>
<!-- .outline -->
      <section class="magazine-content">
        <div class="inner">
          <div class="magazin-content-in cf">
            <div class="headline">
              <span class="icon-mail"></span>
              <p class="title">SENSHA Mail Magazine</p>
              <h2 class="txt">We offer high quality products and reliable services</h2>
            </div>
            <div class="magazine-input">
              <input type="text" name="mailaddress" class="magazine-input_input" placeholder="">
             <button type="submit">Apply</button>
           </div>
          </div>
        </div>
      </section>
<!-- .mail-content -->
  </div>
<!-- .content -->
   	<?php include('include/footer.php')?>
	<script src="js/jquery.bxslider.js"></script>
	<script>
		
		$('.bxslider').bxSlider({
	  	auto:false,
	  	autoControls:false,
	  	pager:false,
		slideWidth: 600
		});
	</script>
  </div>  
<!-- .wrapper -->

