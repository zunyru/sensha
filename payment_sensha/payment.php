<?php 

	namespace AmazonPay;

	session_start(); 
	
	define('WEB_ACCESS', 1);
	define('WEB_PAGE', 1);
	define('PRIVATE_DIR', 'private_html');
	define('PATH_LEVEL', '');
	define('DEFAULT_LANG', 'th');
	define('PATH_TYPE', 1); 
	
	require_once(PATH_LEVEL.(PRIVATE_DIR ? PRIVATE_DIR.'/' : '').'core/web.php');
	require_once('AmazonPay/Client.php');
	require_once('AmazonPay/ResponseParser.php');
	//require_once('Signature.php');

	$config = array(
	'merchant_id'   => AZ_MERCHANT_ID,
	'access_key'   	=> AZ_ACCESS_KEY_ID,
	'secret_key'   	=> AZ_SECRET_ACCESS_KEY,
	'client_id'   	=> AZ_CLIENT_ID,
	'region'   		=> 'jp',
	'sandbox'   	=> AZ_SANDBOX 
	);

	$client = new Client($config);
    $client->setSandbox(AZ_SANDBOX);
    $payment = Model('payment','frontend');

    $order_id   = GetSession('order_id');
    $data       = $payment->get_payment_order_data($order_id);

    if($data['order_id']!='') {

        switch($payment->filter_post('action')) {

            ######################################################
            case 'get_billing' : 
            ######################################################

                /*
                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id'] = $payment->filter_post('amazon_billing_agreement_id');
                //$requestParameters['address_consent_token'] = GetSession('enfetf_access_token');
                //$requestParameters['access_token'] = GetSession('enfetf_access_token');

                $response = $client->getBillingAgreementDetails($requestParameters);

                $responseToJson             = $response->toJson();
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('getBillingAgreementDetails',$requestParametersToJson,$responseToJson);

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                print $responseToJson;
                */
                
                break;

            ######################################################
            case 'set_billing' : 
            ######################################################

                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id']   = $data['billingAgreementId'];
                $requestParameters['seller_billing_agreement_id']   = $data['order_invoice'];
                $requestParameters['billing_agreement_type']        = 'MerchantInitiatedTransaction';
                $requestParameters['subscription_amount']           = $data['amount'];
                $requestParameters['currency_code']                 = 'JPY';
                $requestParameters['seller_note']                   = 'Test Seller Note';
                $requestParameters['store_name']                    = 'Test Store Name';
                $requestParameters['custom_information']            = 'Test Customer';

                $response = $client->setBillingAgreementDetails($requestParameters);

                $responseToJson             = $response->toJson();
                $responseToArray            = json_decode($responseToJson,true);
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('setBillingAgreementDetails',$requestParametersToJson,$responseToJson);

                $return = array();
                $return['status'] = $responseToArray['ResponseStatus']==200 ? 1 : 0;
                
                break;

            ######################################################
            case 'pay_billing' : 
            ######################################################

                // Confirm 

                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id'] = $data['billingAgreementId'];

                $response = $client->confirmBillingAgreement($requestParameters);

                $responseToJson             = $response->toJson();
                $responseToArray            = json_decode($responseToJson,true);
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('confirmBillingAgreement',$requestParametersToJson,$responseToJson);

                if($responseToArray['ResponseStatus']==200) {

                    $authorization_reference_id = time();

                    $requestParameters = array();
                    $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                    $requestParameters['amazon_billing_agreement_id']   = $data['billingAgreementId'];
                    $requestParameters['authorization_reference_id']    = $authorization_reference_id;
                    $requestParameters['authorization_amount']          = $data['amount'];
                    $requestParameters['currency_code']                 = 'JPY';
                    $requestParameters['capture_now']                   = true;

                    $response = $client->authorizeOnBillingAgreement($requestParameters);

                    $responseToJson             = $response->toJson();
                    $responseToArray2           = json_decode($responseToJson,true);
                    $requestParametersToJson    = json_encode($requestParameters);
                    $payment->add_payment_log('authorizeOnBillingAgreement',$requestParametersToJson,$responseToJson);

                    if($responseToArray2['ResponseStatus']==200) {

                        $amazonOrderReferenceId = $responseToArray2['AuthorizeOnBillingAgreementResult']['AmazonOrderReferenceId'];

                        $payment->update_authorizationReferenceId($authorization_reference_id,$amazonOrderReferenceId);

                        $url                                = $data['url_callback'];
                        $data['invoice']                    = $data['order_invoice'];
                        $data['status']                     = 1;
                        $data['amazon_ref_id']              = $amazonOrderReferenceId;
                        $payment->curl_exec_data($url,$data);

                        $return = array();
                        $return['status']   = 1; // Success
                        $return['url']      = BASE_URL.'/message/success';

                    }
                    else {

                        ########## Post Back URL Cancel

                        $payment->update_order_cancel();

                        $url                                = $data['url_callback'];
                        $data['invoice']                    = $data['order_invoice'];
                        $data['status']                     = 0;
                        $data['amazon_ref_id']              = $amazonOrderReferenceId;
                        $payment->curl_exec_data($url,$data);

                        $return = array();
                        $return['status']   = 2; // Cancel
                        $return['url']      = BASE_URL.'/message/fail';

                    }

                }
                else {

                    $return = array();
                    $return['status'] = 0;

                }
                
                break;

            ######################################################
            case 'confirm_billing' : 
            ######################################################

                /*
                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id'] = $payment->filter_post('amazon_billing_agreement_id');

                $response = $client->confirmBillingAgreement($requestParameters);

                $responseToJson             = $response->toJson();
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('confirmBillingAgreement',$requestParametersToJson,$responseToJson);

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                print $responseToJson;
                */
                
                break;

            ######################################################
            case 'validate_billing' : 
            ######################################################

                /*
                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id'] = $payment->filter_post('amazon_billing_agreement_id');

                $response = $client->validateBillingAgreement($requestParameters);

                $responseToJson             = $response->toJson();
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('validateBillingAgreement',$requestParametersToJson,$responseToJson);

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                print $responseToJson;
                */
                
                break;

            ######################################################
            case 'authorize_billing' : 
            ######################################################

                /*
                $requestParameters = array();
                $requestParameters['merchant_id'] = AZ_MERCHANT_ID;
                $requestParameters['amazon_billing_agreement_id'] = $payment->filter_post('amazon_billing_agreement_id');
                $requestParameters['authorization_reference_id'] = $payment->filter_post('authorization_reference_id');
                $requestParameters['authorization_amount'] = $payment->filter_post('authorization_amount');
                $requestParameters['currency_code'] = 'JPY';
                $requestParameters['capture_now'] = true;

                $response = $client->authorizeOnBillingAgreement($requestParameters);

                $responseToJson             = $response->toJson();
                $requestParametersToJson    = json_encode($requestParameters);
                $payment->add_payment_log('authorizeOnBillingAgreement',$requestParametersToJson,$responseToJson);

                header("Access-Control-Allow-Origin: *");
                header('Content-Type: application/json');
                print $responseToJson;
                */
                
                break;

        }


    }
    else {

        $return = array();
        $return['status'] = 0;

    }

    header("Access-Control-Allow-Origin: *");
    header('Content-Type: application/json');
    print json_encode($return);
    


?>