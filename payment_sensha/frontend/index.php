<?php

namespace AmazonPay;

if($site_flag!=1) {
	exit;
}

session_start();
ob_start();

ini_set('display_errors', 0);
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE|E_DEPRECATED));

define('WEB_ACCESS', 1);
define('WEB_PAGE', 1);

define('PRIVATE_DIR', 'private_html');
define('PUBLIC_DIR', '');
define('SITE_DIR', 'frontend');
define('PATH_LEVEL', '');
define('DEFAULT_LANG', 'th');

/**
 * PATH_TYPE
 * 1 = private dir อยู่นอก doc root
 * 2 = private dir อยู่ใน doc root
 */
define('PATH_TYPE', 1); 

require_once(PATH_LEVEL.(PRIVATE_DIR ? PRIVATE_DIR.'/' : '').'core/web.php');

$tpl_site_app->assign('SITE_PRODUCTION', SITE_PRODUCTION );
$tpl_site_app->assign('PATH_LINK', PATH_LINK);
$tpl_site_app->assign('MAIN_URL', MAIN_URL);

$site_lang 		= param('lang')=='en' ? 'en' : '';
$site_lang_path = param('lang')=='en' ? '/en' : '';
$tpl_site_app->assign('SITE_LANG', $site_lang);
$tpl_site_app->assign('SITE_LANG_PATH', $site_lang_path);


$app_name = param('app') ? param('app') : 'index'; 
if(is_file($app_site->load($app_name))){ 

	require_once( $app_site->load($app_name)); 
	$app_name = $app_name;
	$_app_name = explode('_', $app_name);
	if( is_array($_app_name) && count($_app_name) > 0 ){
		$arr_app_name = array();
		foreach($_app_name as $_an){
			$_an = strtolower($_an);
			$arr_app_name[] = ucfirst($_an);
		}
		$app_name = implode('_', $arr_app_name);
	} 

	if( $app_name ){
		$oApp = new $app_name();
		$oApp->run(); 
	}
	
}
?>
