<?php 

	namespace AmazonPay;

	session_start();
	
	define('WEB_ACCESS', 1);
	define('WEB_PAGE', 1);
	define('PRIVATE_DIR', 'private_html');
	define('PATH_LEVEL', '');
	define('DEFAULT_LANG', 'th');
	define('PATH_TYPE', 1); 
	
	require_once(PATH_LEVEL.(PRIVATE_DIR ? PRIVATE_DIR.'/' : '').'core/web.php');
	require_once('AmazonPay/Client.php');
	require_once('AmazonPay/ResponseParser.php');
	//require_once('Signature.php');

	$config = array(
	'merchant_id'   => AZ_MERCHANT_ID,
	'access_key'   	=> AZ_ACCESS_KEY_ID,
	'secret_key'   	=> AZ_SECRET_ACCESS_KEY,
	'client_id'   	=> AZ_CLIENT_ID,
	'region'   		=> 'jp',
	'sandbox'   	=> AZ_SANDBOX 
	);

	$client = new Client($config);
	$client->setSandbox(AZ_SANDBOX);


	if(filter_input(INPUT_GET, 'access_token', FILTER_SANITIZE_STRING)!='') {
		$access_token = filter_input(INPUT_GET, 'access_token', FILTER_SANITIZE_STRING);
		SetSession('enfetf_access_token', $access_token);
	}
	else if(GetSession('enfetf_access_token')!='') {
		$access_token = GetSession('enfetf_access_token');
	}
	//print $access_token; exit;


	if($access_token!='') {

		/*
		$client->setClientId($client_id);
		$userInfo = $client->getUserInfo($access_token);

		print '<pre>';
		print_r($userInfo);
		print '</pre>';
		*/
		header("location: ".BASE_URL."/checkout");

	}




?>