<?php
class Core extends Application {

	function __construct(){
		parent::Application();		
	}

	function default_index(){		
	}

	function load_js(){

		$site = param('_site');
		$app_name = param('_app');
		$js_file = param('js');

		$base_path = SITE_PATH;

		if( $site ){		
			$base_path = str_replace(SITE_DIR, $site, $base_path);
		}
		$js_path = $base_path.'/apps/'.$app_name.'/js/'.$js_file.'.js';

		if( is_file($js_path) ){
			ob_start();
			include($js_path);
			$js = ob_get_clean();
			header("Content-type: application/x-javascript");
			echo $js;
		}

	}

	function load_css(){

		$site = param('_site');
		$app_name = param('_app');
		$css_file = param('css');

		$base_path = SITE_PATH;
		if( $site ){		
			$base_path = str_replace(SITE_DIR, $site, $base_path);
		}
		$css_path = $base_path.'/apps/'.$app_name.'/css/'.$css_file.'.css';

		if( is_file($css_path) ){
			ob_start();
			include($css_path);
			$css = ob_get_clean();
			header("Content-type: text/css");
			echo $css;
		}
	}
	
	function view_securimage(){
		$namespace = param('namespace');
		
		$img = new Securimage();
		$img->image_height = 60;
		$img->image_width = $img->image_height * M_E;      
		$img->perturbation = 0.1;
		$img->num_lines = 0;
		$img->image_bg_color = new Securimage_Color('#eee');
		$img->text_color = new Securimage_Color('#2a3542');
		$img->case_sensitive = true;
		$img->charset = 'ABCDEFGHKMNPRSTUVWYZabcdefghkmnprstuvwyz23456789';
		$img->setNamespace(WEB_SECRET_CODE.$namespace);
		$img->show();
	}
}
?>
