<?php

class Index extends Application {

	function Index(){ 

		header("Access-Control-Allow-Origin: *");
		
		parent::Application();

		$this->payment_model = Model('payment','frontend');
        $this->admin_model = Model('admin','frontend');

		$this->set('BASE_URL', BASE_URL);
		$this->set('CALLBACK_URL', CALLBACK_URL);

		$this->set('AZ_MERCHANT_ID', AZ_MERCHANT_ID);
		$this->set('AZ_CLIENT_ID', AZ_CLIENT_ID);
		$this->set('AZ_CLIENT_SECRET', AZ_CLIENT_SECRET);
		$this->set('AZ_ACCESS_KEY_ID', AZ_ACCESS_KEY_ID);
		$this->set('AZ_SECRET_ACCESS_KEY', AZ_SECRET_ACCESS_KEY);
		$this->set('AZ_URL_CALLBACK', AZ_URL_CALLBACK);
		$this->set('AZ_URL_WIDGETS', AZ_URL_WIDGETS);
		$this->set('AZ_SANDBOX', AZ_SANDBOX);
		
		$this->access_token = GetSession('enfetf_access_token') ? GetSession('enfetf_access_token') : '';
		
	}

	function default_index(){
	}

	############## Start Test ##############

	function form_test() {

		$this->set('invoice','INV-'.date('ymdHis'));
		$this->view('form_test.html');

	}

	function form_test_complete() {

		print 'Payment Complete';

	}

	function form_test_cancel() {

		print 'Payment Cancel';

	}

	function form_test_callback() {
		
		$this->payment_model->add_test_callback();

	}

	############## End Test ##############

	function payment_log(){

		$list = $this->payment_model->get_payment_log_list();
		$data = $list[0];
		print '<pre>';
		print_r($data);
		print '</pre>';
		print '<hr>';
		print '<pre>';
		print_r(json_decode($data['log_request'],true));
		print '</pre>';
		print '<hr>';
		print '<pre>';
		print_r(json_decode($data['log_response'],true));
		print '</pre>';

	}

	function payment() {

		$queryString 	= file_get_contents("php://input");
		$data 			= array();
		parse_str($queryString, $data);

        $payment_total_amount =  $this->admin_model->get_sales_history_total($data);

        $arr = array();
        $amount = 0;
        for($i=1;$i<=20;$i++) {
            if($data['item_name_'.$i]!='' && $data['item_number_'.$i]!='' && $data['amount_'.$i]!='' && $data['quantity_'.$i]!='') {
                $arr['amount'] = (is_float(base64_decode($data['amount_' . $i])) || is_numeric(base64_decode($data['amount_' . $i]))) ? base64_decode($data['amount_' . $i]) : 0;
                $arr['quantity'] = (is_float($data['quantity_' . $i]) || is_numeric($data['quantity_' . $i])) ? $data['quantity_' . $i] : 0;

                if($arr['quantity'] > 0) {

                    $arr['total'] = $arr['amount']*$arr['quantity'];
                    $amount = $amount+$arr['total'];
                    $item[] = $arr;

                }
            }
        }
        if($amount != $payment_total_amount[0]){
            $base_url = str_replace("/payment_sensha","",BASE_URL);
            URL::redirect($base_url.'/page/payment_cancle');
        }

		/*print '<pre>';
		print_r($data);
		print '</pre>';
		exit;*/


		if($data['invoice']!='') {

			if($data['currency_code']=='JPY') {

			$result = $this->payment_model->create_payment_order($data);

				if($result['status']==1) {

					SetSession('order_id',$result['order_id']);
					URL::redirect(BASE_URL.'/checkout');

				}
				else {

					$this->clear_session();
					$this->set('message',$result['message']);
					$this->set('cbt',$data['cbt']);
					$this->set('url_cancel_return',$data['url_cancel_return']);
					$this->view('form_payment_message.html');

				}

			}
			else {

				$this->clear_session();
				$this->set('message','処理ができません。 (Currency Code は JPY のみです。)');
				$this->set('cbt',$data['cbt']);
				$this->set('url_cancel_return',$data['url_cancel_return']);
				$this->view('form_payment_message.html');

			}

		}
		else {

			$this->clear_session();
			$this->set('message','処理ができません (Invoiceが生成できませんでした。)');
			$this->set('cbt',$data['cbt']);
			$this->set('url_cancel_return',$data['url_cancel_return']);
			$this->view('form_payment_message.html');

		}

	}

	function checkout() {

		$order_id = GetSession('order_id');

		if($order_id) {

			$data = $this->payment_model->get_payment_order_data($order_id);

			if($data['order_id'] && $data['order_status']=='P') {

				$item = $this->payment_model->get_payment_order_detail($data['order_id']);

				$this->set('data',$data);
				$this->set('item',$item);
				$this->set('item_total',count($item));

				if($this->access_token!='') {
					$this->view('form_payment_checkout.html');
				}
				else {
					$this->view('form_payment.html');
				}

			}
			else {

				$this->clear_session();
				$this->set('message','処理ができません。');
				$this->set('cbt',$data['cbt']);
				$this->set('url_cancel_return',$data['url_cancel_return']);
				$this->view('form_payment_message.html');

			}

		}
		else {

			$this->clear_session();
			$this->set('message','セッションの有効期限が切れました。もう一度ウェブサイトに戻り、再度処理をしてください。');
			$this->set('cbt',$data['cbt']);
			$this->set('url_cancel_return',$data['url_cancel_return']);
			$this->view('form_payment_message.html');

		}

	}

	function message() {

		$order_id 	= GetSession('order_id');
		$data 		= $this->payment_model->get_payment_order_data($order_id);

		switch(param('code')) {
			case 'session_expired' 	:	$message = 'セッションの有効期限が切れました。もう一度ウェブサイトに戻り、再度処理をしてください。';
										$url 	 = $data['url_cancel_return'];
										break;
			case 'success' 			:	$message = 'ありがとうございます。無事発注が完了いたしました。';
										$url 	 = $data['url_complete_return'];
										$url    .= '/?invoice='.$data['order_invoice'].'&status=1&amazon_ref_id='.$data['amazonOrderReferenceId'];
										break;
			case 'fail' 			:	$message = '購入がキャンセルされました。';
										$url 	 = $data['url_complete_return'];
										$url    .= '/?invoice='.$data['order_invoice'].'&status=0';
										break;
			default 				: 	$message = '購入が失敗しました。';
										$url 	 = $data['url_cancel_return'];
										break;
		}

		$this->clear_session();
		$this->set('message',$message);
		$this->set('cbt',$data['cbt']);
		$this->set('url_cancel_return',$url);
		$this->view('form_payment_message.html');

	}

	function clear_session() {
		UnsetSession('order_id');
		UnsetSession('enfetf_access_token');
	}

	function do_update() {

		$order_id 	= GetSession('order_id');

		if($order_id!='') {

			$data = $this->payment_model->get_payment_order_data($order_id);

			switch(param('code')) {
				case 'billingAgreementId' 	:	$result = $this->payment_model->update_billingAgreementId();
												break;
			}

			header("Access-Control-Allow-Origin: *");
			header('Content-Type: application/json');
			$data['status'] = $result['status'];
            print json_encode($data);

		}
		else {

			header("Access-Control-Allow-Origin: *");
			header('Content-Type: application/json');
			$data['status'] = 'ERROR';
            print json_encode($data);

		}

	}

}

?>