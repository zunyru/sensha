<?php
class Model_Payment extends Model{

	function Model_Payment(){
		
		$this->db = Database::getInstance();
		
	}

	function filter_post($name) {
		return filter_input(INPUT_POST, $name, FILTER_SANITIZE_STRING);
	}

	function get_payment_log_list() {

		$sql_param = array();

		$sql = "
			SELECT a.* 
			FROM ".dbTable('payment_log')." AS a 
			ORDER BY a.log_id DESC 
			LIMIT 0,1 
		";
		$list = $this->db->GetArray($sql);
		return $list;

	}

	function add_payment_log($log_function,$log_request,$log_response) {

		$sql_param = array();
		array_push($sql_param,GetSession('order_id'));
		array_push($sql_param,$log_function);
		array_push($sql_param,$log_request);
		array_push($sql_param,$log_response);
		array_push($sql_param,date('Y-m-d H:i:s'));

		$sql = "
			INSERT INTO ".dbTable('payment_log')." SET 
			order_id = ?, log_function = ?, log_request = ?, log_response = ?, created = ? 
		";
		$this->db->Execute($sql,$sql_param);

	}

	function get_payment_order_data($order_id) {

		$sql_param = array();
		array_push($sql_param,$order_id);

		$sql 	= " SELECT * FROM ".dbTable('payment_order')." WHERE order_id = ? AND publish = 1 AND active = 1 ";
		$rs 	= $this->db->GetArray($sql,$sql_param);
		$arr 	= $rs[0];

		$arr['shipping_show'] 	= number_format($arr['shipping'],2,'.',',');
		$arr['amount_show'] 	= number_format($arr['amount'],2,'.',',');

		return $arr;

	}

	function get_payment_order_detail($order_id) {

		$sql_param = array();
		array_push($sql_param,$order_id);

		$sql = " 
			SELECT * 
			FROM ".dbTable('payment_order_detail')." 
			WHERE order_id = ? AND publish = 1 AND active = 1 
			ORDER BY order_detail_id
		";
		$list 	= $this->db->GetArray($sql,$sql_param);
		$total 	= count($list);

		for($i=0;$i<$total;$i++) {

			$list[$i]['amount_show'] 	= number_format($list[$i]['amount'],2,'.',',');
			$list[$i]['total_show'] 	= number_format($list[$i]['total'],2,'.',',');

		}

		return $list;		

	}

	function create_payment_order($data) {


		$sql_param = array();
		array_push($sql_param,$data['invoice']);

		$sql = "
			SELECT order_id 
			FROM ".dbTable('payment_order')." 
			WHERE order_invoice = ? AND order_status IN ('S','C') AND publish = 1 AND active = 1 
		";
		$rs = $this->db->GetArray($sql,$sql_param);

		if($rs[0]['order_id']!='') {

			$result['status'] 	= 0;
			$result['message'] 	= 'เลขอ้างอิง '.$data['invoice'].' ไม่สามารถใช้งานซ้ำได้';
			return $result;
			exit;

		}

		$amount = 0;
		$item 	= array();

		for($i=1;$i<=20;$i++) {

			if($data['item_name_'.$i]!='' && $data['item_number_'.$i]!='' && $data['amount_'.$i]!='' && $data['quantity_'.$i]!='') {

				

				$arr = array();
				$arr['item_name'] 	= $data['item_name_'.$i];
				$arr['item_number'] = $data['item_number_'.$i];
				$arr['amount'] 		= (is_float(base64_decode($data['amount_'.$i])) || is_numeric(base64_decode($data['amount_'.$i]))) ? base64_decode($data['amount_'.$i]) : 0;
				$arr['quantity'] 	= (is_float($data['quantity_'.$i]) || is_numeric($data['quantity_'.$i])) ? $data['quantity_'.$i] : 0;

				//if($arr['amount'] > 0 && $arr['quantity'] > 0) {
				if($arr['quantity'] > 0) {

					$arr['total'] = $arr['amount']*$arr['quantity'];
					$amount = $amount+$arr['total'];
					$item[] = $arr;

				}

			}

		}

		if($amount > 0 && count($item) > 0) {

			$sql_param = array();
			array_push($sql_param,$data['invoice']);
			array_push($sql_param,$data['first_name']);
			array_push($sql_param,$data['last_name']);
			array_push($sql_param,$data['email']);
			array_push($sql_param,$data['address1']);
			array_push($sql_param,$data['address2']);
			array_push($sql_param,$data['city']);
			array_push($sql_param,$data['zip']);
			array_push($sql_param,$data['phone']);
			array_push($sql_param,$data['cbt']);
			array_push($sql_param,$data['currency_code']);
			array_push($sql_param,$data['url_complete_return']);
			array_push($sql_param,$data['url_cancel_return']);
			array_push($sql_param,$data['url_callback']);
			array_push($sql_param,$data['shipping']);
			array_push($sql_param,date('Y-m-d H:i:s'));

			$sql = "
				INSERT INTO ".dbTable('payment_order')." SET 
				order_invoice			= ?, 
				order_status 			= 'P', 
				first_name				= ?, 
				last_name				= ?, 
				email					= ?, 
				address1				= ?, 
				address2				= ?, 
				city					= ?, 
				zip						= ?, 
				phone					= ?, 
				cbt						= ?, 
				currency_code			= ?, 
				url_complete_return		= ?, 
				url_cancel_return		= ?, 
				url_callback			= ?, 
				shipping				= ?, 
				created					= ? 
			";
			$this->db->Execute($sql,$sql_param);

			$order_id = $this->db->Insert_ID();

			if($order_id!='' && $amount > 0 && count($item) > 0) {

				$item_total = count($item);

				for($i=0;$i<$item_total;$i++) {

					$sql_param = array();
					array_push($sql_param,$order_id);
					array_push($sql_param,$item[$i]['item_name']);
					array_push($sql_param,$item[$i]['item_number']);
					array_push($sql_param,$item[$i]['amount']);
					array_push($sql_param,$item[$i]['quantity']);
					array_push($sql_param,$item[$i]['total']);
					array_push($sql_param,date('Y-m-d H:i:s'));

					$sql = "
						INSERT INTO ".dbTable('payment_order_detail')." SET 
						order_id		= ?, 
						item_name		= ?, 
						item_number		= ?, 
						amount			= ?, 
						quantity		= ?, 
						total			= ?, 
						created			= ? 
					";
					$this->db->Execute($sql,$sql_param);


				}

			}

			$amount = $amount + $data['shipping'];

			$sql_param = array();
			array_push($sql_param,$amount);
			array_push($sql_param,date('Y-m-d H:i:s'));
			array_push($sql_param,$order_id);

			$sql = " UPDATE ".dbTable('payment_order')." SET amount = ?, updated = ? WHERE order_id = ? ";
			$this->db->Execute($sql,$sql_param);

			$result['status'] 	= 1;
			$result['order_id']	= $order_id;
			return $result;

		}
		else {

			$result['status'] 	= 0;
			$result['message'] 	= 'ตรวจพบปัญหารายการสินค้าไม่ถูกต้อง';
			return $result;

		}

	}

	function update_billingAgreementId() {

		$billingAgreementId = $this->filter_post('billingAgreementId');
		$order_id 			= GetSession('order_id');

		if($billingAgreementId!='' && $order_id!='') {

			$sql_param = array();
			array_push($sql_param,$billingAgreementId);
			array_push($sql_param,date('Y-m-d H:i:s'));
			array_push($sql_param,$order_id);

			$sql = " UPDATE ".dbTable('payment_order')." SET billingAgreementId = ?, updated = ? WHERE order_id = ? ";
			$this->db->Execute($sql,$sql_param);

			$result = array();
			$result['status'] = 'SUCCESS';


		}
		else {

			$result = array();
			$result['status'] = 'ERROR';

		}

		return $result;

	}

	function update_authorizationReferenceId($id,$amazon_id) {

		$order_id = GetSession('order_id');

		if($id!='' && $order_id!='') {

			$sql_param = array();
			array_push($sql_param,$id);
			array_push($sql_param,$amazon_id);
			array_push($sql_param,date('Y-m-d H:i:s'));
			array_push($sql_param,$order_id);

			$sql = " 
				UPDATE ".dbTable('payment_order')." SET 
				order_status 				= 'S', 
				authorizationReferenceId 	= ?, 
				amazonOrderReferenceId		= ?, 
				updated 					= ? WHERE order_id = ? 
			";
			$this->db->Execute($sql,$sql_param);

		}

	}

	function update_order_cancel() {

		$order_id = GetSession('order_id');

		if($order_id!='') {

			$sql_param = array();
			array_push($sql_param,date('Y-m-d H:i:s'));
			array_push($sql_param,$order_id);

			$sql = " UPDATE ".dbTable('payment_order')." SET order_status = 'C', updated = ? WHERE order_id = ? ";
			$this->db->Execute($sql,$sql_param);

		}

	}

	function curl_exec_data($url,$postfields) { 

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$result = curl_exec($ch);

	}

	function add_test_callback() {

		$sql_param = array();
		array_push($sql_param,$_POST['invoice']);
		array_push($sql_param,$_POST['status']);
		array_push($sql_param,$_POST['amazon_ref_id']);
		array_push($sql_param,date('Y-m-d H:i:s'));

		$sql = "
			INSERT INTO ".dbTable('test')." SET 
			invoice			= ?, 
			status			= ?, 
			amazon_ref_id	= ?,
			created			= ? 
		";
		$this->db->Execute($sql,$sql_param);

	}
	
}
?>
