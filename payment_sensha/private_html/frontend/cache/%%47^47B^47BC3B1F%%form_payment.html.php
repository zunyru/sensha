<?php /* Smarty version 2.6.14, created on 2019-11-20 17:18:29
         compiled from index/views/form_payment.html */ ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<title>SENSHA Amazon Payment</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<style>
		html, body{
			margin: 0;
			padding: 0;
			font-size:15px;
			font-weight:400;
			font-family:"Montserrat", 'Hiragino Kaku Gothic ProN', "Yu Gothic",sans-serif;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}
		h1{
			background: #004ea1;
			background: #eaad03;
			color:#fff;
			margin: 0 0 40px;
			padding: 5px 0;
			font-size: 16px;
		}
		h2{
			margin: 0 0 30px;			
		}
		p {
			margin: 0 0 40px;
		}
		p span{
			display: block;
			font-size: 0.9em;
		}
		table{
			width: 100%;
			max-width: 800px;
			background: #fefefe;
			border: none;
			border-spacing: 0px;
		}
		table thead{
			background: #004ea1;
			color: #fff;
			text-align: center;
		}
		table tr th, table tr td{
			padding: 5px;
		}
		hr{
			max-width: 800px;			
		}
		#Logout {
			display: block;
			margin: 0 auto;
			padding: 5px 20px;
			color: #ffffff;
			font-size:14px;
			background: #b0bdc6;
			border: none;
			box-shadow: 0px 0px 4px 2px #dfdfdf;
		}
		#Logout, #return{
			display: block;
			margin: 0 auto;
			padding: 5px 20px;
			color: #ffffff;
			font-size:14px;
			background: #b0bdc6;
			border: none;
			box-shadow: 0px 0px 4px 2px #dfdfdf;
		}
	</style>
	<script type='text/javascript'>
	  window.onAmazonLoginReady = function() {
	    amazon.Login.setClientId('<?php echo $this->_tpl_vars['AZ_CLIENT_ID']; ?>
');
	  };
	  window.onAmazonPaymentsReady = function() {
	    showButton();
	  };
	</script>
	<script async='async' src='<?php echo $this->_tpl_vars['AZ_URL_WIDGETS']; ?>
'></script>
</head>
<body class>
    
<div style="text-align: center;">

    <h1>SENSHA: Amazon Pay</h1>
    <h2>取引番号: <?php echo $this->_tpl_vars['data']['order_invoice']; ?>
</h2>
	<p>下記のご注文内容に問題がないかご確認の上、「Amazonアカウントでのお支払い」ボタンをクリックしてお支払いを進めてください。<span>*尚、その他の取引内容については商品代金以外の合計金額となっております。</span></p>
	
    <table border="1" align="center">
     <thead>
      <tr>
        <!-- th style="text-align: center;">หมายเลขสินค้า</th-->
        <th style="text-align: center;">ご注文商品</th>
        <th style="text-align: center;">数量</th>
        <th style="text-align: center">値段</th>
        <th style="text-align: center;">小計</th>
      </tr>
     </thead>
      <tbody>
      <?php unset($this->_sections['index']);
$this->_sections['index']['name'] = 'index';
$this->_sections['index']['loop'] = is_array($_loop=$this->_tpl_vars['item_total']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['index']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['index']['show'] = true;
$this->_sections['index']['max'] = $this->_sections['index']['loop'];
$this->_sections['index']['start'] = $this->_sections['index']['step'] > 0 ? 0 : $this->_sections['index']['loop']-1;
if ($this->_sections['index']['show']) {
    $this->_sections['index']['total'] = min(ceil(($this->_sections['index']['step'] > 0 ? $this->_sections['index']['loop'] - $this->_sections['index']['start'] : $this->_sections['index']['start']+1)/abs($this->_sections['index']['step'])), $this->_sections['index']['max']);
    if ($this->_sections['index']['total'] == 0)
        $this->_sections['index']['show'] = false;
} else
    $this->_sections['index']['total'] = 0;
if ($this->_sections['index']['show']):

            for ($this->_sections['index']['index'] = $this->_sections['index']['start'], $this->_sections['index']['iteration'] = 1;
                 $this->_sections['index']['iteration'] <= $this->_sections['index']['total'];
                 $this->_sections['index']['index'] += $this->_sections['index']['step'], $this->_sections['index']['iteration']++):
$this->_sections['index']['rownum'] = $this->_sections['index']['iteration'];
$this->_sections['index']['index_prev'] = $this->_sections['index']['index'] - $this->_sections['index']['step'];
$this->_sections['index']['index_next'] = $this->_sections['index']['index'] + $this->_sections['index']['step'];
$this->_sections['index']['first']      = ($this->_sections['index']['iteration'] == 1);
$this->_sections['index']['last']       = ($this->_sections['index']['iteration'] == $this->_sections['index']['total']);
?>
      <tr>
        <!--td style="text-align: center;"><?php echo $this->_tpl_vars['item'][$this->_sections['index']['index']]['item_number']; ?>
</td-->
        <td style="text-align: left;"><?php echo $this->_tpl_vars['item'][$this->_sections['index']['index']]['item_name']; ?>
</td>
        <td style="text-align: center;"><?php echo $this->_tpl_vars['item'][$this->_sections['index']['index']]['quantity']; ?>
</td>
        <td style="text-align: right;"><?php echo $this->_tpl_vars['item'][$this->_sections['index']['index']]['amount_show']; ?>
</td>
        <td style="text-align: right;"><?php echo $this->_tpl_vars['item'][$this->_sections['index']['index']]['total_show']; ?>
</td>
      </tr>
      <?php endfor; endif; ?>
      <?php if ($this->_tpl_vars['data']['shipping'] > 0): ?>
      <tr>
        <!--td style="text-align: center;">&nbsp;</td-->
        <td style="text-align: right;" colspan="3">ค่าจัดส่ง</td>
        <td style="text-align: right;"><?php echo $this->_tpl_vars['data']['shipping_show']; ?>
</td>
      </tr>
      <?php endif; ?>
      <tr>
        <!--td style="text-align: center;">&nbsp;</td-->
        <td style="text-align: right;" colspan="3"><b>合計</b></td>
        <td style="text-align: right;"><b><?php echo $this->_tpl_vars['data']['amount_show']; ?>
</b></td>
      </tr>
      </tbody>
    </table>
    
    <br><br>
    <div id="AmazonPayButton"></div>
    <br><br>

    <hr>
    <br><br>
    <button id="Logout" >Amazon からログアウトする</button>

</div>
  
<script type="text/javascript">

  function showButton() {
      var authRequest;
      OffAmazonPayments.Button("AmazonPayButton", "<?php echo $this->_tpl_vars['AZ_MERCHANT_ID']; ?>
", {
          type: "PwA",
          color: "Gold",
          size: "large",
          language: "en-UK",
          authorization: function() {
              loginOptions = {
                  scope: "profile postal_code payments:widget payments:shipping_address",
                  popup: true
              };
              authRequest = amazon.Login.authorize(loginOptions, "<?php echo $this->_tpl_vars['AZ_URL_CALLBACK']; ?>
");
          },
          onError: function(error) {
              // your error handling code.
              // alert("The following error occurred: " 
              //        + error.getErrorCode() 
              //        + ' - ' + error.getErrorMessage());
          }
      });
  };

</script>

<script type="text/javascript">
   document.getElementById('Logout').onclick = function() {
     alert('Amazon Logout');
     amazon.Login.logout();
   };
</script>
  
</body>
</html>