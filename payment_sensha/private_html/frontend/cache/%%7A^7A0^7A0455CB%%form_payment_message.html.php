<?php /* Smarty version 2.6.14, created on 2019-11-10 11:57:38
         compiled from index/views/form_payment_message.html */ ?>
<html>
<head>
	<meta charset="UTF-8">
	<title>SENSHA Amazon Payment</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">	
	<style>
		html, body{
			margin: 0;
			padding: 0;
			font-size:15px;
			font-weight:400;
			font-family:"Montserrat", 'Hiragino Kaku Gothic ProN', "Yu Gothic",sans-serif;
			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}
		h1{
			background: #004ea1;
			background: #eaad03;
			color:#fff;
			margin: 0 0 40px;
			padding: 5px 0;
			font-size: 16px;
		}
		h2{
			margin: 0 0 30px;			
		}
		p {
			margin: 0 0 40px;
		}
		p span{
			display: block;
			font-size: 0.9em;
		}
		table{
			width: 100%;
			max-width: 800px;
			background: #fefefe;
			border: none;
			border-spacing: 0px;
		}
		table thead{
			background: #004ea1;
			color: #fff;
			text-align: center;
		}
		table tr th, table tr td{
			padding: 5px;
		}
		hr{
			max-width: 800px;			
		}
		#Logout, #return{
			display: block;
			margin: 0 auto;
			padding: 5px 20px;
			color: #ffffff;
			font-size:14px;
			background: #b0bdc6;
			border: none;
			box-shadow: 0px 0px 4px 2px #dfdfdf;
		}
	</style>	
</head>
  
  <body>
  
  
    <div style="text-align: center;">
    <h1>SENSHA: Amazon Pay Completion</h1>
	<p>お支払いが確定しました。「SENSHAに戻る」ボタンをクリックしてSENSHAサイトにお戻りください。</span></p>
        
        <div><?php echo $this->_tpl_vars['message']; ?>
</div>
        <br><br>
        <hr>
        <br><br>

        <?php if ($this->_tpl_vars['cbt'] && $this->_tpl_vars['url_cancel_return']): ?>
        <button id="return" onclick="window.location='<?php echo $this->_tpl_vars['url_cancel_return']; ?>
';"><?php echo $this->_tpl_vars['cbt']; ?>
</button>
        <?php endif; ?>

    </div>
  
  </body>
</html>