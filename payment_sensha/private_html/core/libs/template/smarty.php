<?php
	class Web_Smarty_Application extends Smarty {

		function Web_Smarty_Application( $site = '' ) {

			$path = SITE_PATH.($site ? '/'.$site : '');

			$this->Smarty();

			$this->template_dir	= $path.'/apps/';
			$this->compile_dir  = $path.'/cache/';

			$this->left_delimiter = "{{";
			$this->right_delimiter= "}}";

	   }

	}

	class Web_Smarty_Template extends Smarty {

		function Web_Smarty_Template( $site = '' ) {

			$path = SITE_PATH_PUBLIC.($site ? '/'.$site : '');

			$this->Smarty();

			$this->template_dir	= $path.'/';
			$this->compile_dir  = $path.'/cache/';

			$this->left_delimiter = "{{";
			$this->right_delimiter= "}}";

	   }

	}

?>
