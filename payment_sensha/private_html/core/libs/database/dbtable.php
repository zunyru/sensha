<?php
	class DBTable {
		var $db;
		var $table;
		var $primary;
		var $debug;
		var $exe;
		var $sqlscript;
		var $htmlspchars_decode = array();

		function DBTable( $table, $primary ) {
			$db = Database::getInstance();
			$this->db 		= $db;
			$this->table	= $this->getTableName($table);
			$this->primary	= $primary;
		}
		
		function getTableName( $tablename ){
			return  DB_PREFIX.$tablename;
		}
		
		function debug(){
			$this->debug = true;
		}
		
		function do_delete( $itemID, $fk = '' ){
			$sql = "DELETE FROM ". $this->table ." WHERE ". ($fk ? $fk : $this->primary) ." = " . $itemID;
			$this->db->Execute( $sql  );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
	
		function do_remove( $itemID, $fk = '' ){
			$sql = " UPDATE ". $this->table ." SET active = 0 WHERE ". ($fk ? $fk : $this->primary) ."=". $itemID;
			$this->db->Execute( $sql );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
	
		function do_publish( $itemID, $fk = '' ){
			$sql = " UPDATE ". $this->table ." SET publish = 1 WHERE ". ($fk ? $fk : $this->primary) ."=". $itemID;
			$this->db->Execute( $sql );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
		
		function do_unpublish( $itemID, $fk = '' ){
			$sql = " UPDATE ". $this->table ." SET publish = 0 WHERE ". ($fk ? $fk : $this->primary) ."=". $itemID;
			$this->db->Execute( $sql );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
	
		function do_active( $itemID, $fk = '' ){
			$sql = " UPDATE ". $this->table ." SET active = 1 WHERE ". ($fk ? $fk : $this->primary) ."=". $itemID;
			$this->db->Execute( $sql );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
		
		function do_inactive( $itemID, $fk = '' ){
			$sql = " UPDATE ". $this->table ." SET active = 0 WHERE ". ($fk ? $fk : $this->primary) ."=". $itemID;
			$this->db->Execute( $sql );
			if( $this->debug == true ){
				echo $sql;
				echo "<br>";
			}
			return true;
		}
		
		function get_new_ordering($where = ''){
		
			$rs = $this->db->GetArray( "SELECT ordering FROM ". $this->table ." ". $where ." ORDER BY ordering DESC LIMIT 1" );

			if( count($rs) == 0){
				return 1;
			}
			else{
				return ($rs[0]['ordering'] + 1);
			}
			
		}
		
		function do_ordering_up( $id, $fk = '', $lang_id = '' ){
			// GET CURRENT ORDERING
			$strSQL1 = "SELECT ordering, ".($fk ? $fk : $this->primary)."
						FROM ".$this->table."
						WHERE active = '1'
						AND ".($fk ? $fk : $this->primary)." = '". $id ."'
						".($lang_id ? "AND lang_id = '".$lang_id."'" : "")."
						ORDER BY ordering ASC
						LIMIT 1
					";
	
			$rs					= $this->db->GetArray( $strSQL1 );
			$oldID				= $rs[0][($fk ? $fk : $this->primary)];
			$oldOrdering	= $rs[0]['ordering'];
			
			if( $this->debug == true ){			
				echo $strSQL1;
				echo "<br>";
				echo "oldID = ".$oldID;
				echo "<br>";
				echo "oldOrdering = ".$oldOrdering;
				echo "<hr>";
			}
						
			// GET NEW ORDEING BY THIS > CURRENT ON THE CURENT PARENTID 
			$strSQL2 = "SELECT ordering, ".($fk ? $fk : $this->primary)."
						FROM ".$this->table."
						WHERE active = '1'
						AND ordering < '".$oldOrdering."'
						".($lang_id ? "AND lang_id = '".$lang_id."'" : "")."
						ORDER BY ordering DESC
						LIMIT 1
					";
					
			$rs2					= $this->db->GetArray( $strSQL2 );
			$newID				= $rs2[0][($fk ? $fk : $this->primary)];
			$newOrdering	= $rs2[0]['ordering'];
			
			if( $this->debug == true ){
				echo $strSQL2;
				echo "<br>";
				echo "newID = ".$newID;
				echo "<br>";
				echo "newOrdering = ".$newOrdering;
				echo "<br>";
			}
			
			if($oldID && $newID){
				// UPDATE CURRENT ORDERING BY NEW ORDERING
				$strSQLUpdate1 = " UPDATE ".$this->table."
								   SET Ordering = '".$newOrdering."'
								   WHERE ".($fk ? $fk : $this->primary)." = '".$oldID."'
								 ";
				$resultUpdate1  = $this->db->Execute( $strSQLUpdate1 );
				
				// UPDATE NEW ORDERING BY CURRENT ORDERING
				$strSQLUpdate2 = " UPDATE ".$this->table."
								   SET ordering = '".$oldOrdering."'
								   WHERE ".($fk ? $fk : $this->primary)." = '".$newID."'
								 ";
				$resultUpdate2  = $this->db->Execute( $strSQLUpdate2 );
			}
		}
		
		function do_ordering_down( $id, $fk = '', $lang_id = '' ){
			// GET CURRENT ORDERING
			$strSQL1 = "SELECT ordering, ".($fk ? $fk : $this->primary)."
						FROM ".$this->table."
						WHERE active = '1'
						AND ".($fk ? $fk : $this->primary)." = '".$id."'
						".($lang_id ? "AND lang_id = '".$lang_id."'" : "")."
						ORDER BY ordering ASC
						LIMIT 1
					";
			
			$rs					= $this->db->GetArray( $strSQL1 );
			$oldID				= $rs[0][($fk ? $fk : $this->primary)];
			$oldOrdering		= $rs[0]['ordering'];
		
			if( $this->debug == true ){
				echo $strSQL1;
				echo "<br>";
				echo "oldID = ".$oldID;
				echo "<br>";
				echo "oldOrdering = ".$oldOrdering;
				echo "<hr>";
			}
	
			// GET NEW ORDEING BY THIS > CURRENT ON THE CURENT PARENTID 
			$strSQL2 = "SELECT ordering, ".($fk ? $fk : $this->primary)."
						FROM ".$this->table."
						WHERE active = '1'
						AND ordering > '".$oldOrdering."'
						".($lang_id ? "AND lang_id = '".$lang_id."'" : "")."
						ORDER BY ordering ASC
						LIMIT 1
					";		
		
			$rs2					= $this->db->GetArray( $strSQL2 );
			$newID				= $rs2[0][($fk ? $fk : $this->primary)];
			$newOrdering	= $rs2[0]['ordering'];
			
			if( $this->debug == true ){
				echo $strSQL2;
				echo "<br>";
				echo "newID = ".$newID;
				echo "<br>";
				echo "newOrdering = ".$newOrdering;
				echo "<br>";
			}
			
			if($oldID && $newID){
				// UPDATE CURRENT ORDERING BY NEW ORDERING
				$strSQLUpdate1 = " UPDATE ".$this->table." 
								   SET ordering = '".$newOrdering."'
								   WHERE ".($fk ? $fk : $this->primary)." = '".$oldID."'
								 ";
				$resultUpdate1  = $this->db->Execute( $strSQLUpdate1 );
				// UPDATE NEW ORDERING BY CURRENT ORDERING
				$strSQLUpdate2 = " UPDATE ".$this->table."  
								   SET ordering = '".$oldOrdering."'
								   WHERE ".($fk ? $fk : $this->primary)." = '".$newID."'
								 ";
				$resultUpdate2  = $this->db->Execute( $strSQLUpdate2 );
				
				if( $this->debug == true ){
					echo "<hr>";
					echo $strSQLUpdate1."<br><br>";
					echo $strSQLUpdate2;
				}
			}
		}
		
		function do_add( $record, $fk = '' ){
			if( is_array($record) && count($record) > 0 ) {
				$rs =$this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = -1" ); 

				$record['created'] 	= $record['created'] ? $record['created'] : date('Y-m-d H:i:s');
				$record['ordering'] = $record['ordering'] ? $record['ordering'] : $this->get_new_ordering();
				$record['publish'] 	= $record['publish'] ? $record['publish'] : "1";
				$record['active'] 	= $record['active'] ? $record['active'] : "1";

				$insertSQL = $this->db->GetInsertSQL($rs,$record); 
				$this->db->Execute($insertSQL); 
				$this->sqlscript = $insertSQL;
			}
			else{
				return false;
			}
		}
		
		function do_edit( $record, $id, $fk = '', $lang_id = ''){
			if( is_array($record) && count($record) > 0 && $id  ) {
				$rs = $this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = '" .$id. "' ".($lang_id ? "AND lang_id = '".$lang_id."' " : "")." " ); 
				
				$record['updated'] = date('Y-m-d H:i:s');
				
				$updateSQL = $this->db->GetUpdateSQL($rs, $record); 

				$this->db->Execute($updateSQL); 
				if( $this->debug == true ){
					echo $updateSQL;
				}
	
			}
			else{
				return false;
			}
		}
		
		function do_edit_translation( $record, $id, $field_name, $lang_id){
			if( is_array($record) && count($record) > 0 && $id  ) {
				$rs = $this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".$field_name." = '".$id."' AND lang_id = '".$lang_id."' " ); 
				
				$record['updated'] = date('Y-m-d H:i:s');
				
				$updateSQL = $this->db->GetUpdateSQL($rs, $record); 

				$this->db->Execute($updateSQL); 
				if( $this->debug == true ){
					echo $updateSQL;
				}
	
			}
			else{
				return false;
			}
		}
		
		function get( $id, $fk = '', $lang_id = ''  ){
			if( !$id && $_POST['itemID'] ) {
				$arrItemID = $_POST['itemID'];
				$id = $arrItemID[0];
			}
			
			if( $id  ) {
				$sql = "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = '" .$id. "' ".($lang_id ? "AND lang_id = '".$lang_id."' " : "")." LIMIT 1";

				$rs = $this->db->Execute( $sql ); 
				if ($rs) {
					$array_ds = array();
					$obj = $rs->FetchNextObject();
					if( is_object($obj) ){
						foreach( $obj as $key => $val ){
							$key = strtolower($key);
							$array_ds[$key] = $val;
						}
					}
					return $array_ds;
				} 
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		
		function select( $id, $fk = '', $htmlspchars_decode = ''  ){
			if( $table ){
				$this->table = $table;
			}
			if( $primary ){
				$this->primary = $primary;
			}
			if( !$id && $_POST['itemID'] ) {
				$arrItemID = $_POST['itemID'];
				$id = $arrItemID[0];
			}
		
			if( $id  ) {
			
				$sql_param = array();
				array_push($sql_param, $id);
			
				$sql = "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = ? LIMIT 1";

				$rs = $this->db->Execute($sql,$sql_param); 
				if ($rs) {
					$array_ds = array();
					$obj = $rs->FetchNextObject();
					if( is_object($obj) ){
						foreach( $obj as $key => $val ){
							$key = strtolower($key);
							if( is_array($htmlspchars_decode) && in_array($key, $htmlspchars_decode) ){
								$array_ds[$key] = 	htmlspecialchars_decode(str_replace('"', '&quot;', $val));
							}
							else{
								$array_ds[$key] = 	str_replace('"', '&quot;', $val);
							}
						}
					}
					return $array_ds;
				} 
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		
		function select_unpublish( $id, $table = '', $primary = ''  ){
			if( $table ){
				$this->table = $table;
			}
			if( $primary ){
				$this->primary = $primary;
			}
			if( !$id && $_POST['itemID'] ) {
				$arrItemID = $_POST['itemID'];
				$id = $arrItemID[0];
			}
			
			if( $id  ) {
				$rs = $this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".$this->primary." = '" .$id. "' AND Active = '1' LIMIT 1" ); 
				if ($rs) { 
					$obj = $rs->FetchNextObject();
					return $obj;
				} 
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		
		function select_by($field, $value){
			$rs = $this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".$field." = '" .$value. "' AND Active = '1' AND Publish = '1' LIMIT 1" ); 			
			if ($rs) { 
				$obj = $rs->FetchNextObject();
				return $obj;
			} 
			else{
				return false;
			}
		}
		
		function select_translation( $id, $field_name, $lang_id ){
			if( !$id && $_POST['itemID'] ) {
				$arrItemID = $_POST['itemID'];
				$id = $arrItemID[0];
			}
			
			if( $id  ) {
				$sql = "SELECT * FROM ".$this->table." WHERE ".$field_name." = '" .$id. "' AND lang_id = '".$lang_id."' LIMIT 1"; 

				$rs = $this->db->Execute( $sql ); 
				if ($rs) {
					$array_ds = array();
					$obj = $rs->FetchNextObject();
					if( is_object($obj) ){
						foreach( $obj as $key => $val ){
							$key = strtolower($key);
							$array_ds[$key] = $val;
						}
					}
					return $array_ds;
				} 
				else{
					return false;
				}
			}
			else{
				return false;
			}
		}
		
		function get_last_id(){
			//return mysql_insert_id();
			return $this->db->insert_id();
		}
		
		function insert_id(){
			//return mysql_insert_id();
			return $this->db->insert_id();
		}
		
		function getSqlScript(){
			return $this->sqlscript;
		}
		
		/** 
		 * check by message returning version 
		 */
		function save_insert( $record, $fk = ''){
			if( is_array($record) && count($record) > 0 ) {
				
				$rs =$this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = NULL" ); 
				
				if( !in_array(DB_DRIVER, array('mssql', 'odbc_mssql', 'sqlsrv', 'mssqlnative')) ){	
					$record['created'] 	= $record['created'] ? $record['created'] : date('Y-m-d H:i:s');
					$record['updated'] 	= $record['updated'] ? $record['updated'] : date('Y-m-d H:i:s');
					$record['ordering'] 	= $record['ordering'] ? $record['ordering'] : $this->get_new_ordering();
					$record['publish'] 	= $record['publish'] ? $record['publish'] : "1";
					$record['active']	= $record['active'] ? $record['active'] : "1";
				}
								
				$insertSQL = $this->db->GetInsertSQL($rs, $record); 
				
				$exe_result = array();
				$exe_result['sql'] = $insertSQL;				
									
				if ($this->db->Execute($insertSQL) == false) {
					$exe_result['result'] = false;
					$exe_result['error_message'] = $this->db->ErrorMsg();						
				}
				else{						
					$exe_result['result'] = true;
				}
				
				return $exe_result;

			}
			else{
				return false;
			}
		}
		
		function save_update( $record, $id, $fk = '', $lang_id = ''){
			if( is_array($record) && count($record) > 0 && $id  ) {
				$rs = $this->db->Execute( "SELECT * FROM ".$this->table." WHERE ".($fk ? $fk : $this->primary)." = '" .$id. "' ".($lang_id ? "AND lang_id = '".$lang_id."' " : "")." " ); 
				
				if( !in_array(DB_DRIVER, array('mssql', 'odbc_mssql', 'sqlsrv', 'mssqlnative')) ){	
					$record['updated'] = date('Y-m-d H:i:s');
				}
				
				$updateSQL = $this->db->GetUpdateSQL($rs, $record);
				
				$exe_result = array();
				$exe_result['sql'] = $updateSQL;					
									
				if ($this->db->Execute($updateSQL) == false) {
					$exe_result['result'] = false;
					$exe_result['error_message'] = $this->db->ErrorMsg();						
				}
				else{						
					$exe_result['result'] = true;
				}
				
				return $exe_result;
			}
			else{
				return false;
			}
		}
		
		function delete_row( $itemID, $fk = '' ){
			$sql = "DELETE FROM ". $this->table ." WHERE ". ($fk ? $fk : $this->primary) ." = '" . $itemID."'";
			
				
			$exe_result = array();
			$exe_result['sql'] = $sql;					
								
			if ($this->db->Execute($sql) == false) {
				$exe_result['result'] = false;
				$exe_result['error_message'] = $this->db->ErrorMsg();
			}
			else{						
				$exe_result['result'] = true;
			}
			
			return $exe_result;
		}
	}
?>