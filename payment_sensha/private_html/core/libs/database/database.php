<?php
class Database{
	
	// Store the single instance of Database 
	private static $instance; 
	
	private function __construct(){}
	
	public static function getInstance(){
		if (!self::$instance){ 
			$db = ADONewConnection('mysqli'); 
			$db->debug = false;
			$db->Connect( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
			$db->Execute('SET NAMES UTF8');
		   
			ADODB_Active_Record::SetDatabaseAdapter($db);

			self::$instance = $db;
		} 
		return self::$instance;
	}
	
	private function __destruct(){
		if (self::$instance){
			self::$instance->Close();	
		}
	}
	
	function get_pk_name($table){
		$db = Database::getInstance();
		$rs = $this->db->GetArray("SHOW KEYS FROM ".dbTable($table)." WHERE Key_name = 'PRIMARY'");
		return $rs[0]['Column_name'];
	}
	
	public static function ExecuteItem($sql, $param, $mode = ''){
		$db = Database::getInstance();	
		
        $record_set = $db->Execute($sql, $param); 
        
        if (!$record_set){
        	echo $db->ErrorMsg();
        }
        else{
        	$rs = array();
        	$i = 0;
        	while (!$record_set->EOF){
        		/** จัดรูปแบบ Array ใหม่ */
        		foreach($record_set->fields as $key => $val){
					if( $mode == 'list' ){
						$rs[$i][$key] = $val;
					}
					else{
						$rs[$key] = $val;	
					}
        			        		}        
        		$record_set->MoveNext();        
        	$i++;
        	}
        	$record_set->Close();
        }
		
		return $rs;
	}
	
	public static function ExecuteList($sql, $param){
		$rs = Database::ExecuteItem($sql, $param, 'list');
		
		return $rs;
	}
}
?>