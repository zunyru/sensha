<?php
class CategoriesTypeInfo {
	var $db;
	function CategoriesTypeInfo(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function load($field, $type){
		$sql = "
			SELECT ".$field."
			FROM ".DB_PREFIX."categories_type
			WHERE Active = '1' 
			".(SYS_Page == 2 ? "AND Publish = '1'" : "")."
			AND Code  = '".$type."'
			LIMIT 1
		";   
		
		$rs = $this->db->GetArray( $sql );
		return $rs[0][$field];
	}
	
	function GetID($type){
		return $this->load('CateTID', $type);
	}
	
	function GetCode($type){
		return $this->load('Code', $type);
	}
	
	function GetTitle($type){
		return $this->load('Title', $type);
	}
	
}
?>