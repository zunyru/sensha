<?php
class Contents {
	var $db;
	var $code;
	var $condition;
	var $langcode;
	function Contents(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function load($field, $code){
		$sql = "
			SELECT ".$field."
			FROM ".DB_PREFIX."contents
			WHERE Active = '1' 
			AND Code = '".$code."'
			AND LangID = '".GetLangID()."'
			LIMIT 1
		";   
		
		$rs = $this->db->GetArray( $sql );
		return $rs[0][$field];
	}
	
	function set($field, $value, $code){
		$sql = "
			UPDATE
			".DB_PREFIX."contents
			SET ".$field." = '".$value."'
			WHERE Active = '1' 
			AND Code = '".$code."'
			AND LangID = '".GetLangID()."'
		";  
		
		if( $this->db->Execute($sql) ){	
			return true;
		}
		else{
			return false;
		}
	}
}
?>