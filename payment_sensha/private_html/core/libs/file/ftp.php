<?php
class FTP{
	function connect(){
	// set up basic connection
		$conn_id = ftp_connect(FTP_HOST); 
		// login with username and password
		$login_result = ftp_login($conn_id, FTP_USERNAME, FTP_PASSWORD); 
		// check connection
		if ((!$conn_id) || (!$login_result)) { 
		//	 echo "FTP connection has failed!";
			return false;
			} else {
			return $conn_id;
		}
	}
	
	function close($conn_id){
		ftp_close($conn_id); 
	}
}
?>