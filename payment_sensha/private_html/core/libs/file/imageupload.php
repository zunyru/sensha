<?php
class uploadFile {
	/**
 * Project:     Upload File
 * File:        class.upload.php
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * @link http://www.ssaii.com/
 * @copyright 2006-2007 ssaii.com
 * @author prasai phattaramano <webmaster@ssaii.com>
 * @version 0.1
 */
	var $path_upload	= "./";
	var $folder_upload	= "uploads";
	var $name_input	= "";
	var $name_file		= "";
	var $name_old		= "";
	var $file_type		= array(	"image/jpg",
									"image/jpeg",
									"image/pjpeg",
									"image/gif",
									"image/png",
									"image/x-jpg",
									"image/x-jpeg",
									"image/x-pjpeg",
									"image/x-gif",
									"image/x-png");
	/* PNG not resize */
	var $image_type		= array(	"image/jpg",
									"image/jpeg",
									"image/pjpeg",
									"image/x-jpg",
									"image/x-jpeg",
									"image/x-pjpeg");

	var $image_resize		= "auto";//fix
	var $image_height		= 0;
	var $image_width		= 0;

	var $mask_logo			= "";
	var $mask_position		= "fix";
	var $mask_width			= "10%";
	var $transparency		= 50;

	var $max_file_upload	= 0;
	var $max_file_post		= 0;
	var $type_upload		= "";
	var $size_upload		= "";
	var $msg_error			= "";
	var $txt_err			= array(	"err_size"	=> "Error Size",
										"err_type"	=> "Error Type",
										"err_copy"	=> "Error Copy"	);
	function uploadFile(){
		$this->max_file_upload		= $this->return_bytes(ini_get("upload_max_filesize"));
		$this->max_file_post		= $this->return_bytes(ini_get("post_max_size"));
	}
	function saveFile() {
		$file			= $_FILES[$this->name_input];
		$this->type_upload	= $file["type"];
		$this->size_upload	= $file["size"];
		$this->name_upload	= $file["name"];

		$path_file	= $this->path_upload.$this->folder_upload;
		$DirOK 		= 0;
		$DIR		= opendir($path_file);
		while ($text = readdir($DIR)) {
			if($text == $this->folder_upload) $DirOK = 1;
		}

		if($file["size"]>0 && $file["size"]<=$this->max_file_upload) {
			if(count($this->file_type)) {
				if(@in_array($this->type_upload,$this->file_type)){
					$type_file	= ".".end(@explode(".",$this->name_upload));
				}else{
					$type_file	= false;
				}
			}else{
				$type_file	= ".".end(@explode(".",$this->name_upload));
			}
			if($type_file) {
				if($this->name_old)	$this->delImage();
				$FileName	= $this->name_file.$type_file;
				$PathFile	= $path_file."/".$FileName;
				if(copy($file["tmp_name"],$PathFile)) {
					if(@in_array($this->type_upload,$this->image_type)){
						if(is_file($this->mask_logo)){
							$this->maskLogo($PathFile);
						}
						$this->resizeImage($PathFile);
					}
					return $FileName;
				}else{
					$this->msg_error	= $this->txt_err["err_copy"];
					return false;
				}
			}else{
				$this->msg_error	= $this->txt_err["err_type"]." : ".$this->type_upload;
				return false;
			}
		}else{
			$this->msg_error	= $this->txt_err["err_size"]." : ".$this->size_upload;
			return false;
		}
	}
	function delImage(){
		$path_file	= $this->path_upload.$this->folder_upload;
		$fileDel	= $path_file."/".$this->name_old;
		if(is_file($fileDel)) @unlink($fileDel);
	}
	function maskLogo($path_file){
		$check_mask	= true;
		$type_mask	= end(@explode("/",$this->mask_logo));
		$type_mask	= end(@explode(".",$type_mask));
		if($this->mask_width != ""){
			$size			= getimagesize($path_file);
			$size_mask		= getimagesize($this->mask_logo);
			if(eregi("%",$this->mask_width)){
				$resize_mask	= @current(@explode("%",strtolower($this->mask_width)));
				$width			= @floor(($size[0]*$resize_mask)/100);
				$height 		= @round($width*$size_mask[1]/$size_mask[0]);
			}elseif(eregi("px",$this->mask_width)){
				$resize_mask	= @current(@explode("px",strtolower($this->mask_width)));
				$width			= @floor($resize_mask);
				$height 		= @round($width*$size_mask[1]/$size_mask[0]);
			}else{
				$check_mask	= false;
			}
		}else{
			$check_mask	= false;
		}
		switch($type_mask){
			case "jpg"	:
			case "jpeg" :
			case "pjpeg" :
				$maskImg	= imagecreatefromjpeg($this->mask_logo);
				break;
			case "gif" :
				$maskImg	= imagecreatefromgif($this->mask_logo);
				break;
			case "png" :
				$maskImg	= imagecreatefrompng($this->mask_logo);
				break;
			default :
				$check_mask	= false;
				break;
		}
		if($check_mask && $width && $height){

			$margin		= 10;
			switch($this->type_upload){
				case "image/jpg" :
				case "image/jpeg" :
				case "image/pjpeg" :
				case "image/x-jpg" :
				case "image/x-jpeg" :
				case "image/x-pjpeg" :
					$images_orig = imagecreatefromjpeg($path_file);
					break;
				case "image/gif" :
				case "image/x-gif" :
					$images_orig = imagecreatefromgif($path_file);
					break;
				case "image/png" :
				case "image/x-png" :
					$images_orig = imagecreatefrompng($path_file);
					break;
			}
			$images_temp	= imagecreate($width, $height);
			$images_black	= imagecolorallocate($images_temp, 0, 0, 0);
			imagecopyresampled($images_temp, $maskImg, 0, 0, 0, 0, $width, $height, imagesx($maskImg), imagesy($maskImg));
			$images_trans	= imagecolortransparent($images_temp,$images_black);
			if($this->mask_position == "rand"){
				$maskX		= (bool)rand(0,1) ? $margin : (imagesx($images_orig) - $width) - $margin;
				$maskY		= (bool)rand(0,1) ? $margin : (imagesy($images_orig) - $height) - $margin;
			}else{
				$maskX		= $margin;
				$maskY		= (imagesy($images_orig) - $height) - $margin;
			}
			if($this->transparency){
				imagecopymerge($images_orig, $images_temp, $maskX, $maskY, 0, 0, $width, $height, $this->transparency);
			}else{
				imagecopy($images_orig, $images_temp, $maskX, $maskY, 0, 0, $width, $height);
			}

			switch($this->type_upload){
				case "image/jpg" :
				case "image/jpeg" :
				case "image/pjpeg" :
				case "image/x-jpg" :
				case "image/x-jpeg" :
				case "image/x-pjpeg" :
					imagejpeg($images_orig,$path_file);
					break;
				case "image/gif" :
				case "image/x-gif" :
					imagegif($images_orig,$path_file);
					break;
				case "image/png" :
				case "image/x-png" :
					imagepng($images_orig,$path_file);
					break;
			}
			imagedestroy($images_orig);
			imagedestroy($images_temp);
		}
		return;
	}
	function resizeImage($path_file){
		if($this->image_height>0 || $this->image_width>0){

			$size	= getimagesize($path_file);
			$width	= $size[0];
			$height	= $size[1];
			$resize	= false;

			if($this->image_resize == "fix"){
				if($this->image_height > 0 && $this->image_height > 0){
					$resize	= true;
					$height	= $this->image_height;
				}
				if($this->image_width > 0 && $this->image_width > 0){
					$resize	= true;
					$width	= $this->image_width;
				}
			}else{
				if($width > $this->image_width && $this->image_width > 0){
					$resize	= true;
					$width	= $this->image_width;
					$height = round($width*$size[1]/$size[0]);
				}
				if($height > $this->image_height && $this->image_height > 0){
					$resize	= true;
					$height	= $this->image_height;
					$width	= round($height*$size[0]/$size[1]);
				}
			}

			if($resize){
				switch($this->type_upload){
					case "image/jpg" :
					case "image/jpeg" :
					case "image/pjpeg" :
					case "image/x-jpg" :
					case "image/x-jpeg" :
					case "image/x-pjpeg" :
						$images_orig = imagecreatefromjpeg($path_file);
						break;
					case "image/gif" :
					case "image/x-gif" :
						$images_orig = imagecreatefromgif($path_file);
						break;
					case "image/png" :
					case "image/x-png" :
						$images_orig = imagecreatefrompng($path_file);
						break;
				}
				$photoX 	= imagesx($images_orig);
				$photoY 	= imagesy($images_orig);
				$images_fin = imagecreatetruecolor($width, $height);
				imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);

				switch($this->type_upload){
					case "image/jpg" :
					case "image/jpeg" :
					case "image/pjpeg" :
					case "image/x-jpg" :
					case "image/x-jpeg" :
					case "image/x-pjpeg" :
						imagejpeg($images_fin,$path_file);
						break;
					case "image/gif" :
					case "image/x-gif" :
						imagegif($images_fin,$path_file);
						break;
					case "image/png" :
					case "image/x-png" :
						imagepng($images_fin,$path_file);
						break;
				}
				imagedestroy($images_orig);
				imagedestroy($images_fin);
			}
		}
		return true;
	}
	function errMsg(){
		return $this->msg_error;
	}
	function return_bytes($val) {
		$val 	= trim($val);
		$last 	= strtolower($val{strlen($val)-1});
		switch($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}
}
?>