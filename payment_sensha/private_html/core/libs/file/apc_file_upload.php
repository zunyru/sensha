<?php
class fManFileUpload{
	var $_file;
	var $_fileType;
	var $_filePath;
	var $_arrFileFix = array();
	
	
	function fManFileUpload(){		
	}
	
	function setFile($file){
		$this->_file 	= $file;	
		$this->_fileType= $this->getFileType();	
		$this->setFileName(ucfirst($this->_fileType)."File_".$this->randomMsg(5).".".$this->_fileType);
	}
	
	function setFilePath($filePath){
		$this->_filePath = $filePath;
	}
	
	function setFileFix($fileFix){
		$this->_arrFileFix = $fileFix;
	}
	
	function getFileSize($type){
		if($type == 'b'){
			return $_FILES[$this->_file]['size'];
		}
		elseif($type == 'kb'){
			return byteToKByte($_FILES[$this->_file]['size']);
		}
		elseif($type == 'mb'){
			return byteToMByte($_FILES[$this->_file]['size']);
		}
		else{
			return false;
		}
	}
	
	function getFileType(){	
		$arrFName = explode('.', $_FILES[$this->_file]['name']);
		$fileType = end($arrFName);
		if($fileType == $this->_arrFileFix[0]){
			return $fileType;
		}
		else{		
			for($i = 1; $i < count($this->_arrFileFix); $i++){
				if($fileType == $this->_arrFileFix[$i]){
					return $fileType;
				}
			}
		}
	}
	
	function isFileType(){
		if($this->getFileType() == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function setFileName($name){
		$this->_fileName = $name;		
	}
	
	function getFileName(){
		return $this->_fileName;
	}
	
     function copyFile(){          	
		if($this->isFileType() == true){
			if(move_uploaded_file($_FILES[$this->_file]['tmp_name'], $this->_filePath."/".$this->_fileName)){
				return true;
			}
			else{
				return false;
			}	
		}	
     }

	function randomMsg($len){
		srand((double)microtime()*10000000);
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$ret_msg = "";
		$num = strlen($chars);
		for($i = 0; $i < $len; $i++){
			$ret_msg.= $chars[rand()%$num];
			$ret_msg.=""; 
		}
		return $ret_msg; 
	}
}
?>