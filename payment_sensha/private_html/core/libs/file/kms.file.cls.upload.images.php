<?php
class fManUpload{
	var $_imageFile;
	var $_fileType;
	var $_originalPath;
	var $_thumbnailPath;		
	var $_thumbnailWidth;			
	
	function fManUpload(){
		
	}
	
	function setImageFile($imgFile){
		$this->_imageFile 	= $imgFile;	
		$this->_fileType 	= $this->getFileType();	
		$this->setFileName(ucfirst($this->_fileType)."Img_".$this->randomMsg(5).".".$this->_fileType);
	}
	
	function setThumbnailWidth($width){
		$this->_thumbnailWidth	= $width;
	}
	
	function setImagePath($imgPath = array()){
		$this->_originalPath 	= $imgPath['original'];
		$this->_thumbnailPath	= $imgPath['thumbnail'];
	}
	
	function getFileType(){		
		if($_FILES[$this->_imageFile]['type'] == "image/pjpeg"){
			return "jpg";
		}elseif($_FILES[$this->_imageFile]['type'] == "image/gif"){
			return "gif";
		}elseif($_FILES[$this->_imageFile]['type'] == "image/x-png"){
			return "png";
		}else{
			return false;
		}
	}
	
	function isFileType(){
		if($this->getFileType() == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function setFileName($name){
		$this->_fileName = $name;		
	}
	
	function getFileName(){
		return $this->_fileName;
	}
	
     function copyImage(){          	
		if($this->isFileType() == true){
			if(move_uploaded_file($_FILES[$this->_imageFile]['tmp_name'], $this->_originalPath.$this->_fileName)){
				chmod($this->_originalPath.$this->_fileName,0644);			
				return true;
			}
			else{
				return false;
			}	
		}	
     }
	
	function createThumbnail(){
		if($_FILES[$this->_imageFile]['type'] == "image/pjpeg"){			
			$image_original = imagecreatefromjpeg($this->_originalPath.$this->_fileName);
		}elseif($_FILES[$this->_imageFile]['type'] == "image/gif"){			
			$image_original = imagecreatefromgif($this->_originalPath.$this->_fileName); 
		}elseif($_FILES[$this->_imageFile]['type'] == "image/x-png"){			
			$image_original = imagecreatefrompng($this->_originalPath.$this->_fileName); 
		}else{
			return false;
		}
		$size=getImageSize($this->_originalPath.$this->_fileName); // get width and height 
		$height=round($this->_thumbnailWidth*$size[1]/$size[0]);  // size[0] = width, size[1] = height
		$imageX = imagesx($image_original);
		$imageY = imagesy($image_original);
		$image_new = imagecreatetruecolor($this->_thumbnailWidth, $height); 
		imagecopyresampled($image_new, $image_original, 0, 0, 0, 0, $this->_thumbnailWidth, $height, $imageX, $imageY); 
					
		imagejpeg($image_new,$this->_thumbnailPath.$this->_fileName);
		chmod($this->_thumbnailPath.$this->_fileName,0644);
		imagedestroy($image_original);
		imagedestroy($image_new);
	}

	function deleteOriginal($file){
		if(is_file($this->_originalPath.$file)){
			if(unlink($this->_originalPath.$file)){
				return true;
			}
			else{
				return false;
			}
		}		
	}
	
	function deleteThumbnail($file){
		if(is_file($this->_thumbnailPath.$file)){
			if(unlink($this->_thumbnailPath.$file)){
				return true;
			}
			else{
				return false;
			}
		}		
	}

	function randomMsg($len){
		srand((double)microtime()*10000000);
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$ret_msg = "";
		$num = strlen($chars);
		for($i = 0; $i < $len; $i++){
			$ret_msg.= $chars[rand()%$num];
			$ret_msg.=""; 
		}
		return $ret_msg; 
	}
}
?>