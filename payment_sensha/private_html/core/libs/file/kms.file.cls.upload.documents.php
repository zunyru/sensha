<?php
class fManDocumentUpload{
	var $_documentFile;
	var $_fileType;
	var $_filePath;
	
	function fManDocumentUpload(){		
	}
	
	function setDocumentFile($documentFile){
		$this->_documentFile 	= $documentFile;	
		$this->_fileType 	= $this->getFileType();	
		$this->setFileName(ucfirst($this->_fileType)."Document_".$this->randomMsg(5).".".$this->_fileType);
	}
	
	function setFilePath($filePath){
		$this->_filePath 	= $filePath;
	}
	
	function getFileType(){		
		if($_FILES[$this->_documentFile]['type'] == "application/msword"){
			return "doc";
		}elseif($_FILES[$this->_documentFile]['type'] == "text/plain"){
			return "txt";
		}elseif($_FILES[$this->_documentFile]['type'] == "application/pdf"){
			return "pdf";
		}elseif($_FILES[$this->_documentFile]['type'] == "application/vnd.ms-excel"){
			return "xls";
		}else{
			return false;
		}
	}
	
	function isFileType(){
		if($this->getFileType() == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function setFileName($name){
		$this->_fileName = $name;		
	}
	
	function getFileName(){
		return $this->_fileName;
	}
	
     function copyDocument(){          	
		if($this->isFileType() == true){
			if(move_uploaded_file($_FILES[$this->_documentFile]['tmp_name'], $this->_filePath.$this->_fileName)){
				return true;
			}
			else{
				return false;
			}	
		}	
     }

	function randomMsg($len){
		srand((double)microtime()*10000000);
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$ret_msg = "";
		$num = strlen($chars);
		for($i = 0; $i < $len; $i++){
			$ret_msg.= $chars[rand()%$num];
			$ret_msg.=""; 
		}
		return $ret_msg; 
	}
}
/*
class fManDocumentUpload{
	var $_documentFile;
	var $_fileType;
	var $_filePath;
	
	function fManDocumentUpload(){		
	}
	
	function setDocumentFile($documentFile){
		$this->_documentFile 	= $documentFile;	
		$this->_fileType 		= $this->getFileType();	
		$this->setFileName(ucfirst($this->_fileType)."document_".$this->randomMsg(5).".".$this->_fileType);
	}
	
	function setFilePath($filePath){
		$this->_filePath 	= $filePath;
	}
	
	function getFileType(){		
		if($_FILES[$this->_documentFile]['type'] == "application/msword "){
			return "doc";
		}elseif($_FILES[$this->_documentFile]['type'] == "application/pdf "){
			return "pdf";
		}elseif($_FILES[$this->_documentFile]['type'] == "text/plain "){
			return "txt";
		}elseif($_FILES[$this->_documentFile]['type'] == "application/vnd.ms-excel"){
			return "xls";
		}else{
			return false;
		}
	}
	
	function isFileType(){
		if($this->getFileType() == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function setFileName($name){
		$this->_fileName = $name;		
	}
	
	function getFileName(){
		return $this->_fileName;
	}
	
     function copyDocument(){          	
		if($this->isFileType() == true){
			if(move_uploaded_file($_FILES[$this->_documentFile]['tmp_name'], $this->_filePath.$this->_fileName)){
				return true;
			}
			else{
				return false;
			}	
		}	
     }

	function randomMsg($len){
		srand((double)microtime()*10000000);
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$ret_msg = "";
		$num = strlen($chars);
		for($i = 0; $i < $len; $i++){
			$ret_msg.= $chars[rand()%$num];
			$ret_msg.=""; 
		}
		return $ret_msg; 
	}
}
*/
?>