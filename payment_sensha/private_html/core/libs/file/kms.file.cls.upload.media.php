<?php
class fManMediaUpload{
	var $_mediaFile;
	var $_fileType;
	var $_filePath;
	
	function fManMediaUpload(){		
	}
	
	function setMediaFile($mediaFile){
		$this->_mediaFile 	= $mediaFile;	
		$this->_fileType 	= $this->getFileType();	
		$this->setFileName(ucfirst($this->_fileType)."media_".$this->randomMsg(5).".".$this->_fileType);
	}
	
	function setFilePath($filePath){
		$this->_filePath 	= $filePath;
	}
	
	function getFileType(){		
		if($_FILES[$this->_mediaFile]['type'] == "audio/x-ms-wma"){
			return "wma";
		}elseif($_FILES[$this->_mediaFile]['type'] == "video/x-ms-wmv"){
			return "wmv";
		}elseif($_FILES[$this->_mediaFile]['type'] == "video/mpeg"){
			return "mpg";
		}elseif($_FILES[$this->_mediaFile]['type'] == "audio/mpeg"){
			return "mp3";
		}else{
			return false;
		}
	}
	
	function isFileType(){
		if($this->getFileType() == true){
			return true;
		}
		else{
			return false;
		}
	}
	
	function setFileName($name){
		$this->_fileName = $name;		
	}
	
	function getFileName(){
		return $this->_fileName;
	}
	
     function copyMedia(){          	
		if($this->isFileType() == true){
			if(move_uploaded_file($_FILES[$this->_mediaFile]['tmp_name'], $this->_filePath.$this->_fileName)){
				return true;
			}
			else{
				return false;
			}	
		}	
     }

	function randomMsg($len){
		srand((double)microtime()*10000000);
		$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$ret_msg = "";
		$num = strlen($chars);
		for($i = 0; $i < $len; $i++){
			$ret_msg.= $chars[rand()%$num];
			$ret_msg.=""; 
		}
		return $ret_msg; 
	}
}
?>