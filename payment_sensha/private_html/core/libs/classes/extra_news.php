<?php
class Extra_News_INI{
	function Extra_News_INI(){
		$db = Database::getInstance();
		$this->db = $db;
		$this->dbObj_News = new DBTable ("extra_news", "NewsID" );
		$this->dbObj_Cate = new DBTable ("categories", "CateID" );
	}
	
	function GetTitle($id){		
		$rs = $this->dbObj_News->select($id);
		return $rs->TITLE;
	}
	
	function GetCateID($id){		
		$rs = $this->dbObj_News->select($id);
		return $rs->CATEID;
	}
	
	function GetCateTitle($id){
		$rs = $this->dbObj_Cate->select($id);
		return $rs->TITLE;
	}
}
?>