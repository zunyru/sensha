<?php
 function getContentTitle( $code ){
 	$db = Database::getInstance();
 	$sql = "
	SELECT Title 
	FROM ".DB_PREFIX.'contents'."
	 WHERE Active = '1' 
	 ".(SYS_PAGE == 2 ? "Publish = '1'" : "")." 
	 AND Code = '".$code."'
	 AND LangID = '".GetLangID()."'
	 LIMIT 1
	";
	$rs= $db->GetArray( $sql );
	
	return $rs[0]['Title'];
 }
 function getContentDetail( $code ){
 	$db = Database::getInstance();
 	$sql = "
	SELECT Detail 
	FROM ".DB_PREFIX.'contents'."
	 WHERE Active = '1' 
	 ".(SYS_PAGE == 2 ? "Publish = '1'" : "")." 
	 AND Code = '".$code."'
	 AND LangID = '".GetLangID()."'
	 LIMIT 1
	";
	$rs= $db->GetArray( $sql );
	
	return $rs[0]['Detail'];
 }
 function saveContentTitle( $code, $value ){
 	$db = Database::getInstance();
 	$db->Execute("UPDATE ".DB_PREFIX.'contents'." SET Title='".$value."' WHERE Code = '".$code."' AND LangID = '".GetLangID()."'");
 }
 function saveContentDetail( $code, $value ){
 	$db = Database::getInstance();
 	$db->Execute("UPDATE ".DB_PREFIX.'contents'." SET Detail='".$value."' WHERE Code = '".$code."' AND LangID = '".GetLangID()."'");
 }
?>