<?php
class ClassifiedsType_INI{
	function ClassifiedsType_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function dropdown($id = ''){
		$sql = "
			SELECT CsfTypeID, Title 
			FROM ".DB_PREFIX."classifieds_type 
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."	
			ORDER BY Ordering ASC
		";		
		$rs = $this->db->GetArray($sql);
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){		
			$selected = $id == $rs[$i]['CsfTypeID'] ? 'selected="selected"' : '';
			$html .= '<option value="'.$rs[$i]['CsfTypeID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $html;
	}
}
?>