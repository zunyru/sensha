<?php
class Member_Point{
	function Member_Point(){
		$db = Database::getInstance();
		$this->db = $db;
	
		switch(getParam('app')):
			case 'member':
				$this->login_point();
				break;
			default:
				
				break;
		endswitch;
	}
	
	function login_point(){
		if($_SESSION['session_member_id']){
			$sql = "SELECT * FROM ".DB_PREFIX."member_point_log 
			WHERE Active = '1' AND Publish = '1' AND MemberID = '".$_SESSION['session_member_id']."'
			AND App = '".getParam('app')."' AND LogDate = '".date('Y-m-d')."' LIMIT 1 ";
			$rs = $this->db->GetArray($sql);
			
			if(count($rs) <= 0){
				$obj = new DBTable('member_point_log', 'PointLogID');
				$ds['MemberID'] = $_SESSION['session_member_id'];
				$ds['App'] = getParam('app');
				$ds['Point'] = 5;
				$ds['LogDate'] = date('Y-m-d');
				$obj->do_add($ds);
				
				$this->updatePoint($ds['Point']);
				$this->clearLastLog();
			}	
		}
	}
	
	function getCurrentPoint(){
		$obj = new DBTable('user', 'UsrID');
		if($_SESSION['session_member_id']){
			$rs = $obj->select($_SESSION['session_member_id']);
			return $rs->POINT;
		}
	}
	
	function updatePoint($point){
		$obj = new DBTable('user', 'UsrID');
		if($_SESSION['session_member_id']){
			$ds['Point'] = $this->getCurrentPoint() + $point;
			$obj->do_edit($ds, $_SESSION['session_member_id']);
		}
	}
	
	function clearLastLog(){
		if($_SESSION['session_member_id']){
			$sql = "DELETE FROM ".DB_PREFIX."member_point_log WHERE Active = '1' AND Publish = '1'
			AND LogDate < '".date('Y-m-d')."' AND MemberID = '".$_SESSION['session_member_id']."' AND App = '".getParam('app')."'";
			$this->db->Execute($sql);
		}
	}
	
}
?>