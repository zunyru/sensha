<?php
class Banner_Client_INI{
	function Banner_Client_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function getOptions($id = ''){
		$sql = "
			SELECT ClientID, Name 
			FROM ".DB_PREFIX."banner_client 
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."	
			ORDER BY Ordering ASC
		";		
		$rs = $this->db->GetArray($sql);
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){		
			$selected = $id == $rs[$i]['ClientID'] ? 'selected="selected"' : '';
			$html .= '<option value="'.$rs[$i]['ClientID'].'" '.$selected.'>'.$rs[$i]['Name'].'</option>';
		}
		
		return $html;
	}
	
	function GetDropdownWithForm($selected = '', $action = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<script language="javascript">';
			$html .= 'function Type2Go(){';
				$html .= 'document.forms[\'adminForm\'].action = \''.$action.'\';';
				$html .= 'document.forms[\'adminForm\'].submit();';
			$html .= '}';
		$html .= '</script>';
		$html .= '<select name="ClientID" id="ClientID" class="txt_input"  '.($onchange_form ? 'onChange="Type2Go(); '.$onchange_form.'"' : 'onchange="Type2Go();"').'>';
		if($params['first_label']){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Display_All.'</option>';
		}
		$html .= $this->getOptions($selected);
		$html .= '</select>';
		
		return $html;
	}
}
?>