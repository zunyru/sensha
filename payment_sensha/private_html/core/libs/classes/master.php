<?php
class Master_INI{
	function Master_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function options($type, $id = ''){
		$sql = "
			SELECT DataID, Title, Created, Publish
			FROM ".DB_PREFIX.'master_data'."
			WHERE Active = '1' AND Type = '".$type."'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			ORDER BY Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['DataID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['DataID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
	
	function check_box($type, $ids = array()){
		$sql = "
			SELECT DataID, Title, Created, Publish
			FROM ".DB_PREFIX.'master_data'."
			WHERE Active = '1' AND Type = '".$type."'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			ORDER BY Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$html = "";
		for($i = 0; $i < count($rs); $i++){
			$checked[$i] = in_array($rs[$i]['DataID'], $ids) ? 'checked="checked"' : '';
			$html .= '<input name="'.$type.'[]" type="checkbox" id="'.$type.'_'.$i.'" value="'.$rs[$i]['DataID'].'" '.$checked[$i].' /> '.$rs[$i]['Title'];
		}
		
		return $html;
	}
	
	function radio($type, $id = ''){
		$sql = "
			SELECT DataID, Title, Created, Publish
			FROM ".DB_PREFIX.'master_data'."
			WHERE Active = '1' AND Type = '".$type."'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			ORDER BY Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$html = "";
		for($i = 0; $i < count($rs); $i++){
			$checked = $id == $rs[$i]['DataID'] ? 'checked="checked"' : '';
			$html .= '<input name="'.$type.'" type="radio" id="'.$type.'_'.$i.'" value="'.$rs[$i]['DataID'].'" '.$checked.' /> '.$rs[$i]['Title'];
		}
		
		return $html;
	}
}
?>