<?php
class Banner_Position_INI{
	function Banner_Position_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function getOptions($id = ''){
		$sql = "
			SELECT PositionID, Name 
			FROM ".DB_PREFIX."banner_position 
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."	
			ORDER BY Ordering ASC
		";		
		$rs = $this->db->GetArray($sql);
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){		
			$selected = $id == $rs[$i]['PositionID'] ? 'selected="selected"' : '';
			$html .= '<option value="'.$rs[$i]['PositionID'].'" '.$selected.'>'.$rs[$i]['Name'].'</option>';
		}
		
		return $html;
	}
	
	function GetDropdownWithForm($selected = '', $action = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<script language="javascript">';
			$html .= 'function Type2Go(){';
				$html .= 'document.forms[\'adminForm\'].action = \''.$action.'\';';
				$html .= 'document.forms[\'adminForm\'].submit();';
			$html .= '}';
		$html .= '</script>';
		$html .= '<select name="PositionID" id="PositionID" class="txt_input"  '.($onchange_form ? 'onChange="Type2Go(); '.$onchange_form.'"' : 'onchange="Type2Go();"').'>';
		if($params['first_label']){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Display_All.'</option>';
		}
		$html .= $this->getOptions($selected);
		$html .= '</select>';
		
		return $html;
	}
}
?>