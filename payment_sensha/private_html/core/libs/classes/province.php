<?php
class Province_INI{
	function Province_INI(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function options($id = ''){
		$sql = "
			SELECT ProvID, Title, Created, Publish
			FROM ".DB_PREFIX.'province'."
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."
			AND LangID = '".GetLangID()."'
			ORDER BY Title ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['ProvID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['ProvID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
}
?>