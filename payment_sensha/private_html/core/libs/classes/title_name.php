<?php
class TitleName{
	function TitleName(){
		$db = Database::getInstance();
		$this->db = $db;
	}
	
	function getOptions($id = ''){
		$sql = "
			SELECT TitleNameID, Title 
			FROM ".DB_PREFIX."title_name 
			WHERE Active = '1'
			".(SYS_PAGE == 2 ? "AND Publish = '1'" : "")."	
			ORDER BY Ordering ASC
		";		
		$rs = $this->db->GetArray($sql);
		
		$html = "";
		for($i = 0; $i < count($rs); $i++){		
			$selected = $id == $rs[$i]['TitleNameID'] ? 'selected="selected"' : '';
			$html .= '<option value="'.$rs[$i]['TitleNameID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $html;
	}
	
	function GetDropdownWithForm($selected = '', $action = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<script language="javascript">';
			$html .= 'function Type2Go(){';
				$html .= 'document.forms[\'adminForm\'].action = \''.$action.'\';';
				$html .= 'document.forms[\'adminForm\'].submit();';
			$html .= '}';
		$html .= '</script>';
		$html .= '<select name="title_name_id" id="title_name_id" class="txt_input"  '.($onchange_form ? 'onChange="Type2Go(); '.$onchange_form.'"' : 'onchange="Type2Go();"').'>';
		if($params['first_label']){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Display_All.'</option>';
		}
		$html .= $this->getOptions($selected);
		$html .= '</select>';
		
		return $html;
	}
	
	function GetDropdown($selected = '', $condition = '', $onchange_form = '', $first_label = ''){
		$html .= '<select name="title_name_id" id="title_name_id" style="width:350px;" class="txt_input"  '.($onchange_form ? $onchange_form : '').'>';
		if($first_label){
			$html .= '<option value="">'.$first_label.'</option>';
		}
		else{
			$html .= '<option value="">'._Please_Select.'</option>';
		}
		$html .= $this->getOptions($selected, $condition);
		$html .= '</select>';
		
		return $html;
	}
	
}
?>