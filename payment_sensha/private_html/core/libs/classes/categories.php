<?php
class Categories_INI{
	function Categories_INI(){
		$db = Database::getInstance();
		$this->db = $db;
		$this->split_tbl = array('video'); 
	}
	
	function parent_options($type, $id = ''){
		$tbl_ex = $type && in_array($type, $this->split_tbl) ? $type."_" : "";
		$sql = "
			SELECT c.CateID, c.Title, c.Created, c.Publish
			FROM ".DB_PREFIX.$tbl_ex.'categories'." c
			WHERE c.Active = '1' 
			AND c.LangID = '".GetLangID()."'
			AND c.Type = '".$type."'
			AND c.Parent = '0'
			ORDER BY c.Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['CateID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['CateID'].'" '.$selected.' title="'.$rs[$i]['Title'].'">'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
	
	function options($type, $parent, $id = ''){
		$tbl_ex = $type && in_array($type, $this->split_tbl) ? $type."_" : "";
		$sql = "
			SELECT c.CateID, c.Title, c.Created, c.Publish
			FROM ".DB_PREFIX.$tbl_ex.'categories'." c
			WHERE c.Active = '1' 
			AND c.LangID = '".GetLangID()."'
			AND c.Type = '".$type."'
			AND c.Parent = '".$parent."'
			ORDER BY c.Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$options = "";
		for($i = 0; $i < count($rs); $i++){
			$selected = $id == $rs[$i]['CateID'] ? 'selected="selected"' : '';
			$options .= '<option value="'.$rs[$i]['CateID'].'" '.$selected.'>'.$rs[$i]['Title'].'</option>';
		}
		
		return $options;
	}
	
	function getCateIDSetFromParent($type, $id){
		$tbl_ex = $type && in_array($type, $this->split_tbl) ? $type."_" : "";
		$sql = "
			SELECT c.CateID
			FROM ".DB_PREFIX.$tbl_ex.'categories'." c
			WHERE c.Active = '1' 
			AND c.LangID = '".GetLangID()."'
			AND c.Type = '".$type."'
			".($id ? "AND c.Parent = '".$id."'" : "")."
			".(SYS_PAGE == 2 ? "Publish = '1'" : "")."
			ORDER BY c.Ordering ASC
		";
		$rs = $this->db->GetArray($sql);
		$cateids = array();
		for($i = 0; $i < count($rs); $i++){
			$cateids[] = $rs[$i]['CateID'];
		}
		
		$cateid_set = implode(',', $cateids);
		
		return $cateid_set;
	}
}
?>