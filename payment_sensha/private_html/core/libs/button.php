<?php
class Button{
	
	private $buttons;
	
	public function __construct(){
	}
	
	public function load($params){
		if( is_array($params) && count($params) > 0 ){
			
			foreach($params as $bk => $bv){
				
				if( is_array($bv) ){					
					$b_param = array(
						'type' => $bk,
						'title' => $bv['title'] ? $bv['title'] : '',
						'link' => $bv['link'] ? $bv['link'] : ''
					);
					$this->buttons .= $this->get_button($b_param);
				}
				else{
					$b_param = array(
						'type' => $bv
					);
					$this->buttons .= $this->get_button($b_param);
				}
			}
			
		}
		
		return $this->buttons;
	}
	
	private function get_button($params){
		
		$html = '';
		$link = $params['link'];
		$text = $params['title'];
		
		switch($params['type']){
			
			############################################
			case 'add':
			############################################
			
				$text = $text=="" ? LANG_TOOL_NEW : $text;
				$html.= '&nbsp;';
				$html.= '<button class="btn btn-success btn-tools" onclick="backend_form_new(\''.$link.'\');"><span class="fa fa-plus"></span> '.$text.'</button>';
				break;
				
			############################################
			case 'delete':
			############################################
			
				$text = $text=="" ? LANG_TOOL_DELETE : $text;
				$html.= '&nbsp;';
				$html.= '<button class="btn btn-warning btn-tools" onclick="backend_form_delete_multi(\''.$link.'\');"><span class="fa fa-times"></span> '.$text.'</button>';
				break;
				
			/*
			case 'publish':
				if( $text == "" ){
					$text = "Enable";	
				}
				$html = '<button class="btn btn-large btn-blue" onclick="setPublish(\''.$link.'\');"><i class="icon-eye-open"></i> '.$text.'</button> ';
				break;
			case 'unpublish':
				if( $text == "" ){
					$text = "Disable";	
				}
				$html = '<button class="btn btn-large btn-grey" onclick="setUnPublish(\''.$link.'\');"><i class="icon-eye-close"></i> '.$text.'</button> ';
				break;
			*/
				
			############################################
			case 'save':
			############################################
			
				$text = $text=="" ? LANG_TOOL_SAVE : $text;
				$html.= '&nbsp;';
				//$html.= '<button class="btn btn-success btn-tools" id="button_form_save"><span class="fa fa-save"></span> '.$text.'</button>';
				$html.= '<button class="btn btn-success btn-tools" onclick="button_form_save();"><span class="fa fa-save"></span> '.$text.'</button>';
				break;
				
			############################################
			case 'save_and_new':
			############################################
			
				$text = $text=="" ? LANG_TOOL_SAVE_NEW : $text;
				$html.= '&nbsp;';
				//$html.= '<button class="btn btn-primary btn-tools" id="button_form_save_new"><span class="fa fa-save"></span> '.$text.'</button>';
				$html.= '<button class="btn btn-primary btn-tools" onclick="button_form_save_new();"><span class="fa fa-save"></span> '.$text.'</button>';
				break;
				
			############################################
			case 'save_and_exit':
			############################################
			
				$text = $text=="" ? LANG_TOOL_SAVE_EXIT : $text;
				$html.= '&nbsp;';
				//$html.= '<button class="btn btn-info btn-tools" id="button_form_save_exit"><span class="fa fa-save"></span> '.$text.'</button>';
				$html.= '<button class="btn btn-info btn-tools" onclick="button_form_save_exit();"><span class="fa fa-save"></span> '.$text.'</button>';
				break;
				
			############################################
			case 'exit':
			############################################
			
				$text = $text=="" ? LANG_TOOL_EXIT : $text;
				$html.= '&nbsp;';
				$html.= '<button class="btn btn-default btn-tools" onclick="window.location=\''.$link.'\'"><span class="fa fa-close"></span> '.$text.'</button>';
				break;
				
		}
		
		return $html;
		
	}
	
	public function __destruct(){
	}
	
}
?>