<?php
# Original PHP code by Chirp Internet: www.chirp.com.au
  # Please acknowledge use of this code by including this header.

  class myAtomParser
  {
    # keeps track of current and preceding elements
    var $tags = array();

    # array containing all feed data
    var $output = array();

    # return value for display functions
    var $retval = &quot;&quot;;

    var $encoding = array();

    # constructor for new object
    function myAtomParser($file)
    {
      # instantiate xml-parser and assign event handlers
      $xml_parser = xml_parser_create(&quot;&quot;);
      xml_set_object($xml_parser, $this);
      xml_set_element_handler($xml_parser, &quot;startElement&quot;, &quot;endElement&quot;);
      xml_set_character_data_handler($xml_parser, &quot;parseData&quot;);

      # open file for reading and send data to xml-parser
      $fp = @fopen($file, &quot;r&quot;) or die(&quot;&lt;b&gt;myAtomParser Error:&lt;/b&gt; Could not open URL $file for input&quot;);
      while($data = fread($fp, 4096)) {
        xml_parse($xml_parser, $data, feof($fp)) or die(
          sprintf(&quot;myAtomParser: Error &lt;b&gt;%s&lt;/b&gt; at line &lt;b&gt;%d&lt;/b&gt;&lt;br&gt;&quot;,
          xml_error_string(xml_get_error_code($xml_parser)),
          xml_get_current_line_number($xml_parser))
        );
      }
      fclose($fp);

      # dismiss xml parser
      xml_parser_free($xml_parser);
    }

    function startElement($parser, $tagname, $attrs)
    {
      if($this-&gt;encoding) {
        # content is encoded - so keep elements intact
        $tmpdata = &quot;&lt;$tagname&quot;;
        if($attrs) foreach($attrs as $key =&gt; $val) $tmpdata .= &quot; $key=\&quot;$val\&quot;&quot;;
        $tmpdata .= &quot;&gt;&quot;;
        $this-&gt;parseData($parser, $tmpdata);
      } else {
        if($attrs['HREF'] &amp;&amp; $attrs['REL'] &amp;&amp; $attrs['REL'] == 'alternate') {
          $this-&gt;startElement($parser, 'LINK', array());
          $this-&gt;parseData($parser, $attrs['HREF']);
          $this-&gt;endElement($parser, 'LINK');
        }
        if($attrs['TYPE']) $this-&gt;encoding[$tagname] = $attrs['TYPE'];

        # check if this element can contain others - list may be edited
        if(preg_match(&quot;/^(FEED|ENTRY)$/&quot;, $tagname)) {
          if($this-&gt;tags) {
            $depth = count($this-&gt;tags);
            list($parent, $num) = each($tmp = end($this-&gt;tags));
            if($parent) $this-&gt;tags[$depth-1][$parent][$tagname]++;
          }
          array_push($this-&gt;tags, array($tagname =&gt; array()));
        } else {
          # add tag to tags array
          array_push($this-&gt;tags, $tagname);
        }
      }
    }

    function endElement($parser, $tagname)
    {
      # remove tag from tags array
      if($this-&gt;encoding) {
        if(isset($this-&gt;encoding[$tagname])) {
          unset($this-&gt;encoding[$tagname]);
          array_pop($this-&gt;tags);
        } else {
          if(!preg_match(&quot;/(BR|IMG)/&quot;, $tagname)) $this-&gt;parseData($parser, &quot;&lt;/$tagname&gt;&quot;);
        }
      } else {
        array_pop($this-&gt;tags);
      }
    }

    function parseData($parser, $data)
    {
      # return if data contains no text
      if(!trim($data)) return;
      $evalcode = &quot;\$this-&gt;output&quot;;
      foreach($this-&gt;tags as $tag) {
        if(is_array($tag)) {
          list($tagname, $indexes) = each($tag);
          $evalcode .= &quot;[\&quot;$tagname\&quot;]&quot;;
          if(${$tagname}) $evalcode .= &quot;[&quot; . (${$tagname} - 1) . &quot;]&quot;;
          if($indexes) extract($indexes);
        } else {
          if(preg_match(&quot;/^([A-Z]+):([A-Z]+)$/&quot;, $tag, $matches)) {
            $evalcode .= &quot;[\&quot;$matches[1]\&quot;][\&quot;$matches[2]\&quot;]&quot;;
          } else {
            $evalcode .= &quot;[\&quot;$tag\&quot;]&quot;;
          }
        }
      }

      if(isset($this-&gt;encoding['CONTENT']) &amp;&amp; $this-&gt;encoding['CONTENT'] == &quot;text/plain&quot;) {
        $data = &quot;&lt;pre&gt;$data&lt;/pre&gt;&quot;;
      }

      eval(&quot;$evalcode .= '&quot; . addslashes($data) . &quot;';&quot;);
    }

    # display a single feed as HTML
    function display_feed($data, $limit)
    {
      extract($data);
      if($TITLE) {
        # display feed information
        $this-&gt;retval .= &quot;&lt;h1&gt;&quot;;
        if($LINK) $this-&gt;retval .= &quot;&lt;a href=\&quot;$LINK\&quot; target=\&quot;_blank\&quot;&gt;&quot;;
        $this-&gt;retval .= stripslashes($TITLE);
        if($LINK) $this-&gt;retval .= &quot;&lt;/a&gt;&quot;;
        $this-&gt;retval .= &quot;&lt;/h1&gt;\n&quot;;
        if($TAGLINE) $this-&gt;retval .= &quot;&lt;P&gt;&quot; . stripslashes($TAGLINE) . &quot;&lt;/P&gt;\n\n&quot;;
        $this-&gt;retval .= &quot;&lt;div class=\&quot;divider\&quot;&gt;&lt;!-- --&gt;&lt;/div&gt;\n\n&quot;;
      }
      if($ENTRY) {
        # display feed entry(s)
        foreach($ENTRY as $item) {
          $this-&gt;display_entry($item, &quot;FEED&quot;);
          if(is_int($limit) &amp;&amp; --$limit &lt;= 0) break;
        }
      }
    }

    # display a single entry as HTML
    function display_entry($data, $parent)
    {
      extract($data);
      if(!$TITLE) return;

      $this-&gt;retval .=  &quot;&lt;p&gt;&lt;b&gt;&quot;;
      if($LINK) $this-&gt;retval .=  &quot;&lt;a href=\&quot;$LINK\&quot; target=\&quot;_blank\&quot;&gt;&quot;;
      $this-&gt;retval .= stripslashes($TITLE);
      if($LINK) $this-&gt;retval .= &quot;&lt;/a&gt;&quot;;
      $this-&gt;retval .=  &quot;&lt;/b&gt;&quot;;
      if($ISSUED) $this-&gt;retval .= &quot; &lt;small&gt;($ISSUED)&lt;/small&gt;&quot;;
      $this-&gt;retval .=  &quot;&lt;/p&gt;\n&quot;;

      if($AUTHOR) {
        $this-&gt;retval .=  &quot;&lt;P&gt;&lt;b&gt;Author:&lt;/b&gt; &quot; . stripslashes($AUTHOR['NAME']) . &quot;&lt;/P&gt;\n\n&quot;;
      }
      if($CONTENT) {
        $this-&gt;retval .=  &quot;&lt;P&gt;&quot; . stripslashes($CONTENT) . &quot;&lt;/P&gt;\n\n&quot;;
      } elseif($SUMMARY) {
        $this-&gt;retval .=  &quot;&lt;P&gt;&quot; . stripslashes($SUMMARY) . &quot;&lt;/P&gt;\n\n&quot;;
      }
    }

    function fixEncoding($input, $output_encoding)
    {
      $encoding = mb_detect_encoding($input);
      switch($encoding) {
        case 'ASCII':
        case $output_encoding:
          return $input;
        case '':
          return mb_convert_encoding($input, $output_encoding);
        default:
          return mb_convert_encoding($input, $output_encoding, $encoding);
      }
    }

    # display entire feed as HTML
    function getOutput($limit=false, $output_encoding='UTF-8')
    {
      $this-&gt;retval = &quot;&quot;;
      $start_tag = key($this-&gt;output);

      switch($start_tag) {
        case &quot;FEED&quot;:
          foreach($this-&gt;output as $feed) $this-&gt;display_feed($feed, $limit);
          break;
        default:
          die(&quot;Error: unrecognized start tag '$start_tag' in getOutput()&quot;);
      }

      return $this-&gt;fixEncoding($this-&gt;retval, $output_encoding);
    }

    # return raw data as array
    function getRawOutput($output_encoding='UTF-8')
    {
      return $this-&gt;fixEncoding($this-&gt;output, $output_encoding);
    }
  }

?>