CREATE TABLE `items_ratings` (
  `ID` int(11) NOT NULL auto_increment,
  `item_id` int(11) NOT NULL,
  `rating` tinyint(2) NOT NULL,
  `rater_ip` varchar(20) NOT NULL,
  `rater_id` int(11) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM;