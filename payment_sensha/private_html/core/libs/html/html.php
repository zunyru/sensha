<?php
class HTML{
	function _($type, $value){
		switch($type):
			case 'filter':
				$html = BaseHtml::htmlFilter($value);	
				break;
			default:
				break;
		endswitch;
		
		return $html;
	}
	
	function set_check_selected($type, $value){
		return $type == $value ? 'selected="selected"' : '';
	}
	
	function set_check_checked($type, $value){
		return $type == $value ? 'checked="checked"' : '';
	}
	
	function show_picture($file_path, $title = '', $attribute = ''){
		return '<img src="'.$file_path.'" alt="'.$title.'" title="'.$title.'" border="0" '.$attribute.'>';
	}
	
	function get_select_option($rs, $id, $title, $current_id = '', $txt_prefix = ''){

		$html = '';
		for($i = 0; $i < count($rs); $i++){			
			$selected = $current_id == $rs[$i][$id] ? 'selected="selected"' : '';
			$html .= '<option value="'.$rs[$i][$id].'" '.$selected.'>'.$txt_prefix.$rs[$i][$title].'</option>';
		}
		
		return $html;
	}
	
	function image_copy($source_image, $my_new_file_path, $new_file_name = ''){
		
		if( $source_image && $my_new_file_path ){
			
			$source = BASE_PATH.'/'.$source_image;
			$basename = basename($source);
			$ext = explode('.', $basename);
			$file_ext = end($ext);
		
			$source_info = list($source_width, $source_height, $source_type, $source_attr) = getimagesize($source);
		
			$new_file_name = $new_file_name ? $new_file_name : date('YmdHis').'.'.$file_ext;
			$new_file_url = 'files/'.$my_new_file_path.$new_file_name;
			$new_images = BASE_PATH.'/'.$new_file_url;
			
			$data = array();
			$data['file_name'] = $new_file_name;
			$data['file_path'] = $new_file_url;
			
			copy($source , $new_images);	
			
			return $data;		
		}
	}
	
	function image_resize($source_image, $my_new_file_path, $my_resize_width = '', $my_resize_height = '', $new_file_name = ''){
		
		if( $source_image && $my_new_file_path ){
			
			$source = BASE_PATH.'/'.$source_image;
			$basename = basename($source);
			$ext = explode('.', $basename);
			$file_ext = end($ext);
		
			$source_info = list($source_width, $source_height, $source_type, $source_attr) = getimagesize($source);
		
			$images = $source;
			$new_file_name = $new_file_name ? $new_file_name : date('YmdHis');//.'.'.$file_ext;
			$new_file_url = 'files/'.$my_new_file_path.$new_file_name;
			$new_images = BASE_PATH.'/'.$new_file_url;
			$width = $my_resize_width;// ? $my_resize_width : $source_width;
			$height = $my_resize_height;
			
			$data = array();
						
			$handle = new Upload($source);
			if ($handle->uploaded) {
				$handle->file_src_name_body = $new_file_name;		
					
				if( ($width == $source_width && $height == $source_height) ){
					$handle->image_resize = false;
				}
				elseif($width > $source_width || $height > $source_height ){
					$handle->image_resize = false;
				}
				else{
					$handle->image_resize = true;	
				}
				/* fix width แล้ว height จะถูกคำนวณตาม width อีกที */
				$handle->image_ratio_y = ($my_resize_width != "" && $my_resize_height != "" ) ? false : true;	
				
				if( $my_resize_width ){
					$handle->image_x = $my_resize_width;
				}
				else{
					$handle->image_x = $source_width;	
				}
				
				if( $my_resize_height ){
					$handle->image_y = $my_resize_height;
				}
				else{
					$handle->image_y = $source_height;
				}
				
            	$handle->Process(BASE_PATH.'/files/'.$my_new_file_path);            
				if ($handle->processed) {
					$new_file_url = 'files/'.$my_new_file_path.$handle->file_dst_name;		
				}
			}
			
			$data['file_name'] = $new_file_name;
			$data['file_path'] = $new_file_url;
			/*
			if( $width == $source_width ){
				copy($source , $new_images);	
			}
			else{
				HTML::resize($source, $width, $height, $new_images);
			}
			*/
			
			return $data;	
		}
	}
	
	function resize($img, $w, $h, $newfilename) {
 
		//Check if GD extension is loaded
		if (!extension_loaded('gd') && !extension_loaded('gd2')) {
			trigger_error("GD is not loaded", E_USER_WARNING);
			return false;
		}
		 
		 //Get Image size info
		 $imgInfo = getimagesize($img);
		 switch ($imgInfo[2]) {
		  	case 1: $im = imagecreatefromgif($img); break;
		  	case 2: $im = imagecreatefromjpeg($img);  break;
		  	case 3: $im = imagecreatefrompng($img); break;
		  	default:  trigger_error('Unsupported filetype!', E_USER_WARNING);  break;
		 }
		 
		 if( $h && $w == "" ){
			$nWidth = $imgInfo[0]*($h/$imgInfo[1]);
			$nHeight = $h;
		 }
		 else{
			 //If image dimension is smaller, do not resize
			 if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
				$nHeight = $imgInfo[1];
				$nWidth = $imgInfo[0];
			 }
			 else{
				//yeah, resize it, but keep it proportional
				if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
					$nWidth = $w;
					$nHeight = $imgInfo[1]*($w/$imgInfo[0]);
				}
				else{
					$nWidth = $imgInfo[0]*($h/$imgInfo[1]);
					$nHeight = $h;
				}
			 }
		 }
		 $nWidth = round($nWidth);
		 $nHeight = round($nHeight);
		 
		 $newImg = imagecreatetruecolor($nWidth, $nHeight);
		 
		 /* Check if this image is PNG or GIF, then set if Transparent*/  
		 if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
		  	imagealphablending($newImg, false);
		  	imagesavealpha($newImg,true);
		  	$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
		  	imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
		 }
		 imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
		 
		 //Generate the file, and rename it to $newfilename
		 switch ($imgInfo[2]) {
		  	case 1: imagegif($newImg,$newfilename); break;
		  	case 2: imagejpeg($newImg,$newfilename);  break;
		  	case 3: imagepng($newImg,$newfilename); break;
		  	default:  trigger_error('Failed resize image!', E_USER_WARNING);  break;
		 }
	   
	   	return $newfilename;
	}

	function upload_file($source, $dest_path){
		
		$ext = explode('.', $source['name']);
		$file_ext = end($ext);
		$new_file_name = date('YmdHis').'.'.$file_ext;
		$dest_path = 'files/'.$dest_path.$new_file_name;
		
		$dest = BASE_PATH.'/'.$dest_path;
		move_uploaded_file($source['tmp_name'], $dest);		
		
		return $dest_path;
	}
	
	function is_image($img_path){
		$data = explode('.', $img_path);
		$type = end($data);
		
		$img_type = array('bmp', 'gif', 'jpeg', 'jpg', 'png');
		if( in_array($type, $img_type) ){
			return true;
		}
		else{
			return false;	
		}		
	}
	
	function clean_idcard_number_format($value){
		return str_replace('-', '', $value);
	}
	
	function set_idcard_number_format($value){
		$d1 = substr($value, 0, 1);
		$d2 = substr($value, 1, 4);
		$d3 = substr($value, 5, 5);
		$d4 = substr($value, 10, 2);
		$d5 = substr($value, 12, 1);
		return $d1.'-'.$d2.'-'.$d3.'-'.$d4.'-'.$d5;
	}
	
	function get_lang_icon($lang_id){
		$o_lang = new DBTable('languages', 'lang_id');
		$lang = $o_lang->select($lang_id);
		return '<img src="'.BASE_URL.'/images/lang/'.$lang['code'].'.png" align="absmiddle" alt="'.$lang['title'].'" title="'.$lang['title'].'" />';	
	}
	
	function sub_string($text, $lenght){
		$text = trim($text);	
		$text_subfix = mb_strlen($text, DETAULT_CHARSET) > $lenght ? '...' : '';
		$text = mb_substr($text, 0, $lenght, DETAULT_CHARSET);	
		$text = $text.$text_subfix; 
		
		return $text;
	}
	
	function html2db_date($date, $mode = ''){
		if( $date != "" ){
			list($d, $m, $y) = explode("/" , $date);
			if( $mode == '' || $mode == 'th' ){
				$y -= 543;
			}
			$db_date = $y.'-'.$m.'-'.$d;
			return $db_date;
		}
	}
	
}

class BaseHtml{

	function BaseHtml(){}
	
	function getDropDownHour($select = ''){
		$html = '';
		for($i = 0; $i < 24; $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			$html .= '<option value="'.(strlen($i) < 2 ? '0'.$i : $i).'" '.$selected.'>'.(strlen($i) < 2 ? '0'.$i : $i).'</option>';
		}
		
		return $html;
	}
	
	function getDropDownMinute($select = ''){
		$html = '';
		for($i = 0; $i < 60; $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			$html .= '<option value="'.(strlen($i) < 2 ? '0'.$i : $i).'" '.$selected.'>'.(strlen($i) < 2 ? '0'.$i : $i).'</option>';
		}
		
		return $html;
	}
	
	function getDropDownDay($select = ''){
		$html = '';
		
		for($i = 1; $i <= 31; $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			$html .= '<option value="'.(strlen($i) < 2 ? '0'.$i : $i).'" '.$selected.'>'.(strlen($i) < 2 ? '0'.$i : $i).'</option>';
		}
		
		return $html;
	}
	
	function getDropDownMonth($select = ''){
		$_Month_Name_List = array(JANUARY,FEBRUARY,MARCH,APRIL,MAY,JUNE,JULY,AUGUST,SEPTEMBER,OCTOBER,NOVEMBER,DECEMBER);
		
		$html = '';
		for($i = 1; $i <= 12; $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			$html .= '<option value="'.(strlen($i) < 2 ? '0'.$i : $i).'" '.$selected.'>'.$_Month_Name_List[($i-1)].'</option>';
		}
		
		return $html;
	}
	
	function getDropDownCurrentYear($select = ''){
		$html = '';
		for($i = (date('Y')); $i <= (date('Y')+2); $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			//$selected2 = $selected == '' ? 'selected="selected"' : '';
			$html .= '<option value="'.$i.'" '.$selected.' '.$selected2.'>'.(GetLangCode() == 'th' ? ($i+543) : $i).'</option>';
		}
		
		return $html;
	}
	
	function getDropDownYear($select = ''){
		$html = '';
		for($i = (date('Y')-10); $i <= (date('Y')+10); $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			//$selected2 = $selected == '' ? 'selected="selected"' : '';
			$html .= '<option value="'.$i.'" '.$selected.' '.$selected2.'>'.(GetLangCode() == 'th' ? ($i+543) : $i).'</option>';
		}
		
		return $html;
	}
	
	function getDropDownBirthYear($select = ''){
		$html = '';
		for($i = (date('Y')-80); $i <= (date('Y')-10); $i++){
			$selected = $select == $i ? 'selected="selected"' : '';
			$html .= '<option value="'.$i.'" '.$selected.'>'.(GetLangCode() == 'th' ? ($i+543) : $i).'</option>';
		}
		
		return $html;
	}
	
	function showPicture($picture, $title = '', $attribute = ''){
		return '<img src="'.$picture.'" alt="'.$title.'" title="'.$title.'" border="0" '.$attribute.'>';
	}
	
	function showNoPicture($title = "", $attribute = ""){
		$picture = BASE_URL.'/images/noimage.png';
		return '<img src="'.$picture.'" alt="'.$title.'" title="'.$title.'" border="0" '.$attribute.'>';
	}
	
	function showPictureLink($picture, $title, $link, $attribute_link = '', $attribute_picture = ''){
		$html  = '';
		$html .= '<a href="'.$link.'" title="'.$title.'" '.$attribute_link.'>';
		$html .= '<img src="'.$picture.'" alt="'.$title.'" title="'.$title.'" border="0" '.$attribute_picture.'>';
		$html .= '</a>';
		return $html;
	}
	
	function showLink($link, $title, $attribute = '', $title2 = ''){
		$title2 = str_replace('"', '&quot;', $title2);
		return '<a href="'.$link.'" title="'.$title2.'" '.$attribute.'>'.$title.'</a>';
	}
	
	function showArray($array){
		echo '<div style="background-color:#fff;">';
		echo '<pre>';
		print_r($array);
		echo '</pre>';
		echo '</div>';
	}
	
	function showError($message){
		$html  = '<div id="error_panel">';
		$html .= $message;
		$html .= '</div>';
		return $html;
	}
	
	function showFlash($flash, $width = '', $height = ''){
		$fname = substr($flash, 0, -4);
		$html = '';
		$html .= '<script type="text/javascript">';
		$html .= "AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','".$width."','height','".$height."','src','".$fname."','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','".$fname."'  ); //end AC code";
		$html .= '</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" width="178" height="148" wmode="transparent">';
		  	$html .= '<param name="movie" value="'.$flash.'" />';
		  	$html .= '<param name="quality" value="high" />';
		  	$html .= '<param name="wmode" value="transparent" />';
		  	$html .= '<embed src="'.$flash.'" quality="high" wmode="transparent" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="'.$width.'" height="'.height.'"></embed>';
		$html .= '</object>';
		$html .= '</noscript>';
		return $html;
	}
	
	function stripTag($html){
		$html = strip_tags($html, "<BR>");
		$html = strip_tags($html, "<B>");
		$html = strip_tags($html, "<br>");
		$html = strip_tags($html, "<b>");
		$html = strip_tags($html, "\n");
		return $html;
	}
	
	function htmlFilter($html){
		$html = strip_tags($html, "<BR>");
		$html = strip_tags($html, "<B>");
		$html = strip_tags($html, "<br>");
		$html = strip_tags($html, "<b>");
		$html = strip_tags($html, "\n");
		$html = nl2br($html);
		$html = BadWordFilter($html);
		return $html;
	}	

	function getBrowser(){
		$useragent = $_SERVER['HTTP_USER_AGENT'];		
		if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'IE';
		} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			$browser_version=$matched[1];
			$browser = 'Opera';
		} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
				$browser_version=$matched[1];
				$browser = 'Firefox';
		} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
				$browser_version=$matched[1];
				$browser = 'Safari';
		} else {
				// browser not recognized!
			$browser_version = 0;
			$browser= 'other';
		}
		return $browser;
	}
}

?>