<?php
	class App_Validator {
		function App_Validator(){
		
		}
		
		function isEMail( $mail ){
			if( !(preg_match('!@.*@|\.\.|\,|\;!', $mail) || !preg_match('!^.+\@(\[?)[a-zA-Z0-9\.\-]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$!', $mail))  ){
				return true;
			}
			else{
				return false;
			}
		}
		
		function isWebsite( $value ){
			if( (strstr($value, "http://") or strstr($value, "https://")) and (substr($value, 0, 7) == "http://"  or substr($value, 0, 8) == "https://") ){
				return true;
			}
			else{
				return false;
			}
		}
		
		function isUsername( $value ){
			if( preg_match("/^[a-zA-Z0-9-_]+$/i", $value) ) {
				return true;
			}
			else{
				return false;
			}
		}

		function isNumber( $value ){
			if( preg_match("/^[0-9]+$/i", $value) || is_numeric($value) ) { 
				return true;
			}
			else{
				return false;
			}
		}

		function isURL( $value ){
			return (@parse_url($value)) ? true : false;
		}

		function isIDCardNumber( $value ){
			//9-9999-99999-99-9
			if( preg_match("/^[0-9]{1}-[0-9]{4}-[0-9]{5}-[0-9]{2}-[0-9]{1}+$/i", $value) ) { 
				return true;
			}
			else{
				return false;
			}
		}

	}
?>