<?php
class GetSession{
	function _($type){
		switch($type):
			case 'id':
				$value = $_SESSION['session_member_id'];
				break;
			case 'name':
				$value = $_SESSION['session_member_name'];
				break;
			case 'type':
				$value = $_SESSION['session_member_type'];
				break;
			default:
				$value = $_SESSION[$type];
				break;
		endswitch;
		
		return $value;
	}
}

class GetSeekerSession{
	function _($type){
		switch($type):
			case 'id':
				$value = $_SESSION['session_seeker_id'];
				break;
			case 'name':
				$value = $_SESSION['session_seeker_name'];
				break;
			default:
				$value = $_SESSION[$type];
				break;
		endswitch;
		
		return $value;
	}
}

class GetEmployerSession{
	function _($type){
		switch($type):
			case 'id':
				$value = $_SESSION['session_employer_id'];
				break;
			case 'name':
				$value = $_SESSION['session_employer_name'];
				break;
			default:
				$value = $_SESSION[$type];
				break;
		endswitch;
		
		return $value;
	}
}

class GetAdminSession{
	function _($type){
		switch($type):
			case 'id':
				$value = get_access_id();
				break;
			case 'name':
				$value = $_SESSION['session_admin_name'];
				break;
			default:
				$value = $_SESSION[$type];
				break;
		endswitch;
		
		return $value;
	}
}
?>