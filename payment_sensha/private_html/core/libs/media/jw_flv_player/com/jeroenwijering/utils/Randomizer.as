class com.jeroenwijering.utils.Randomizer
{
    var originalArray, bufferArray;
    function Randomizer(arr)
    {
        originalArray = arr;
        bufferArray = new Array();
    } // End of the function
    function pick()
    {
        if (bufferArray.length == 0)
        {
            for (var _loc2 = 0; _loc2 < originalArray.length; ++_loc2)
            {
                if (originalArray[_loc2].category != "preroll" && originalArray[_loc2].category != "postroll")
                {
                    bufferArray.push(_loc2);
                } // end if
            } // end of for
        } // end if
        var _loc3 = random(bufferArray.length);
        var _loc4 = bufferArray[_loc3];
        bufferArray.splice(_loc3, 1);
        return (_loc4);
    } // End of the function
} // End of Class
