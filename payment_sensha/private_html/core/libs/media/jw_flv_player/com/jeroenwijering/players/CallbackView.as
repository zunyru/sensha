class com.jeroenwijering.players.CallbackView extends com.jeroenwijering.players.AbstractView
{
    var config, varsObject, startStamp, playSentInt, currentItem, feeder;
    function CallbackView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        if (config.callback != "analytics")
        {
            varsObject = new LoadVars();
        } // end if
    } // End of the function
    function setState(pr1)
    {
        if (pr1 == 3)
        {
            var _loc2 = Math.round(new Date().valueOf() / 1000 - startStamp);
            this.sendVars("stop", _loc2, true);
            playSent = false;
        }
        else if (pr1 == 2 && playSent == false)
        {
            playSentInt = setInterval(this, "sendVars", 500, "start", 0);
            playSent = true;
            startStamp = new Date().valueOf() / 1000;
        } // end else if
    } // End of the function
    function setItem(pr1)
    {
        if (playSent == true && currentItem != undefined)
        {
            var _loc2 = Math.round(new Date().valueOf() / 1000 - startStamp);
            this.sendVars("stop", _loc2, false);
            playSent = false;
        } // end if
        currentItem = pr1;
    } // End of the function
    function sendVars(stt, dur, cpl)
    {
        clearInterval(playSentInt);
        if (config.callback == "analytics")
        {
            var _loc4 = feeder.feed[currentItem].file.lastIndexOf("/");
            var _loc2 = feeder.feed[currentItem].file.substr(_loc4 + 1);
            if (stt == "start")
            {
                getURL("javascript:urchinTracker(\'/start_stream/" + _loc2 + "\');", "");
            }
            else if (stt == "stop" && cpl == true)
            {
                getURL("javascript:urchinTracker(\'/end_stream/" + _loc2 + "\');", "");
            } // end else if
        }
        else
        {
            varsObject.file = feeder.feed[currentItem].file;
            varsObject.title = feeder.feed[currentItem].title;
            varsObject.id = feeder.feed[currentItem].id;
            varsObject.state = stt;
            varsObject.duration = dur;
            varsObject.sendAndLoad(config.callback, varsObject, "POST");
        } // end else if
    } // End of the function
    var playSent = false;
} // End of Class
