class com.jeroenwijering.players.AudioView extends com.jeroenwijering.players.AbstractView
{
    var sync, config, audioClip, audioObject, start, currentItem, currentState, audioTime, isLoaded, feeder;
    function AudioView(ctr, cfg, fed, snc)
    {
        super(ctr, cfg, fed);
        sync = snc;
        var ref = this;
        audioClip = config.clip.createEmptyMovieClip("audio", config.clip.getNextHighestDepth());
        audioClip.setStart = function ()
        {
            if (ref.stopTime == undefined && ref.sync == false)
            {
                ref.audioObject.loadSound(ref.feeder.feed[0].audio, true);
                ref.audioObject.start(0);
            }
            else if (ref.sync == false)
            {
                ref.audioObject.start(ref.stopTime);
            }
            else if (ref.currentState == 2)
            {
                ref.audioObject.start(currentTime);
            } // end else if
        };
        audioClip.setStop = function ()
        {
            ref.audioObject.stop();
            ref.stopTime = ref.audioObject.position / 1000;
        };
        audioObject = new Sound(audioClip);
        audioObject.setVolume(config.volume);
        if (config.useaudio == "true" && sync == false)
        {
            audioClip.setStart();
        } // end if
        if (sync == false)
        {
            audioObject.onSoundComplete = function ()
            {
                this.start();
            };
        } // end if
    } // End of the function
    function setItem(idx)
    {
        currentItem = idx;
    } // End of the function
    function setState(stt)
    {
        currentState = stt;
        if (sync == false)
        {
            return;
        } // end if
        if (stt == 2 && config.useaudio == "true")
        {
            audioObject.start(currentTime);
        }
        else
        {
            audioObject.stop();
        } // end else if
    } // End of the function
    function setTime(elp, rem)
    {
        if (sync == false)
        {
            return;
        } // end if
        if (Math.abs(elp - currentTime) > 1)
        {
            currentTime = elp;
            audioTime = audioObject.position / 1000;
            if (Math.abs(currentTime - audioTime) > 1 && config.useaudio == "true")
            {
                audioObject.start(currentTime);
            } // end if
        } // end if
        if (isLoaded != feeder.feed[currentItem].audio)
        {
            isLoaded = feeder.feed[currentItem].audio;
            audioObject.loadSound(isLoaded, true);
        } // end if
    } // End of the function
    var currentTime = 0;
} // End of Class
