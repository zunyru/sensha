class com.jeroenwijering.players.ImageModel extends com.jeroenwijering.players.AbstractModel
{
    var imageClip, config, imageLength, imageLoader, sourceHeight, sourceWidth, sourceLength, positionInterval, feeder, currentItem, currentURL, isSWF, currentState, sendUpdate, currentPosition, sendCompleteEvent;
    function ImageModel(vws, ctr, cfg, fed, imc, scl)
    {
        super(vws, ctr, cfg, fed);
        imageClip = imc;
        var ref = this;
        imageLength = config.rotatetime;
        if (arguments[5] == true)
        {
            imageLoader = new com.jeroenwijering.utils.ImageLoader(imageClip, config.overstretch, config.width, config.height);
        }
        else
        {
            imageLoader = new com.jeroenwijering.utils.ImageLoader(imageClip);
        } // end else if
        imageLoader.onLoadFinished = function ()
        {
            ref.currentState = 2;
            ref.sendUpdate("state", 2);
            ref.sendUpdate("load", 100);
        };
        imageLoader.onLoadProgress = function (tgt, btl, btt)
        {
            ref.sendUpdate("load", Math.round(btl / btt * 100));
        };
        imageLoader.onMetaData = function ()
        {
            ref.sendUpdate("size", sourceWidth, sourceHeight);
            if (sourceLength > ref.imageLength)
            {
                ref.imageLength = sourceLength;
            } // end if
        };
    } // End of the function
    function setStart(pos)
    {
        if (pos < 1)
        {
            pos = 0;
        }
        else if (pos > imageLength - 1)
        {
            pos = imageLength - 1;
        } // end else if
        clearInterval(positionInterval);
        if (feeder.feed[currentItem].file != currentURL)
        {
            imageClip._visible = true;
            currentURL = feeder.feed[currentItem].file;
            imageLength = config.rotatetime;
            if (feeder.feed[currentItem].file.indexOf(".swf") == -1)
            {
                isSWF = false;
            }
            else
            {
                isSWF = true;
            } // end else if
            imageLoader.loadImage(feeder.feed[currentItem].file);
            currentState = 1;
            this.sendUpdate("state", 1);
            this.sendUpdate("load", 0);
        }
        else
        {
            currentState = 2;
            this.sendUpdate("state", 2);
        } // end else if
        if (pos != undefined)
        {
            currentPosition = pos;
            isSWF == true ? (imageClip.mc.gotoAndPlay(pos * 20)) : (null);
            pos == 0 ? (this.sendUpdate("time", 0, imageLength)) : (null);
        }
        else
        {
            isSWF == true ? (imageClip.mc.play()) : (null);
        } // end else if
        positionInterval = setInterval(this, "updatePosition", 200);
    } // End of the function
    function updatePosition()
    {
        if (currentState == 2)
        {
            currentPosition = currentPosition + 2.000000E-001;
            if (currentPosition >= imageLength)
            {
                currentState = 3;
                this.sendUpdate("state", 3);
                this.sendCompleteEvent();
            }
            else
            {
                this.sendUpdate("time", currentPosition, imageLength - currentPosition);
            } // end if
        } // end else if
    } // End of the function
    function setPause(pos)
    {
        if (pos < 1)
        {
            pos = 0;
        }
        else if (pos > imageLength - 1)
        {
            pos = imageLength - 1;
        } // end else if
        clearInterval(positionInterval);
        currentState = 0;
        this.sendUpdate("state", 0);
        if (pos != undefined)
        {
            currentPosition = pos;
            this.sendUpdate("time", currentPosition, imageLength - currentPosition);
            isSWF == true ? (imageClip.mc.gotoAndStop(pos * 20 + 1)) : (null);
        }
        else
        {
            isSWF == true ? (imageClip.mc.stop()) : (null);
        } // end else if
    } // End of the function
    function setStop()
    {
        delete this.currentURL;
        clearInterval(positionInterval);
        imageLength = config.rotatetime;
        currentPosition = 0;
        isSWF == true ? (imageClip.mc.gotoAndStop(1)) : (null);
        if (imageClip.bg == undefined)
        {
            imageClip.mc.removeMovieClip();
            imageClip.smc.removeMovieClip();
            imageClip._visible = false;
        } // end if
    } // End of the function
    var mediatypes = new Array("jpg", "gif", "png", "swf");
} // End of Class
