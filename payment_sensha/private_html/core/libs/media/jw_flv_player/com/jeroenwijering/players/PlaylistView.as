class com.jeroenwijering.players.PlaylistView extends com.jeroenwijering.players.AbstractView implements com.jeroenwijering.feeds.FeedListener
{
    var config, listRight, listWidth, feeder, listLength, _parent, _alpha, thumbLoader, listScroller, currentItem;
    function PlaylistView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        if (config.displaywidth < config.width)
        {
            listRight = true;
            listWidth = config.width - config.displaywidth - 1;
        }
        else
        {
            listRight = false;
            listWidth = config.width;
        } // end else if
        this.setButtons();
        Stage.addListener(this);
        feeder.addListener(this);
    } // End of the function
    function setButtons()
    {
        var ref = this;
        var _loc2 = config.clip.playlist;
        _loc2.btn._visible = false;
        listLength = feeder.feed.length;
        var _loc4 = 0;
        for (var _loc3 = 0; _loc3 < feeder.feed.length; ++_loc3)
        {
            if (feeder.feed[_loc3].category == "preroll" || feeder.feed[_loc3].category == "postroll")
            {
                continue;
            } // end if
            _loc2.btn.duplicateMovieClip("btn" + _loc3, _loc3);
            _loc2["btn" + _loc3].txt._width = listWidth - 20;
            _loc2["btn" + _loc3].col = new Color(_loc2["btn" + _loc3].bck);
            _loc2["btn" + _loc3].col.setRGB(config.frontcolor);
            _loc2["btn" + _loc3].col2 = new Color(_loc2["btn" + _loc3].icn);
            _loc2["btn" + _loc3].col2.setRGB(config.frontcolor);
            _loc2["btn" + _loc3].bck._width = listWidth;
            _loc2["btn" + _loc3].bck.onRollOver = function ()
            {
                _parent.txt.textColor = ref.config.backcolor;
                _parent.col.setRGB(ref.config.lightcolor);
                _parent.col2.setRGB(ref.config.backcolor);
                if (ref.currentItem != _parent.getDepth())
                {
                    _alpha = 90;
                } // end if
            };
            _loc2["btn" + _loc3].bck.onRollOut = function ()
            {
                _parent.col.setRGB(ref.config.frontcolor);
                if (ref.currentItem != _parent.getDepth())
                {
                    _parent.txt.textColor = ref.config.frontcolor;
                    _parent.col2.setRGB(ref.config.frontcolor);
                    _alpha = 10;
                } // end if
            };
            _loc2["btn" + _loc3].bck.onRelease = function ()
            {
                ref.sendEvent("playitem", _parent.getDepth());
            };
            if (config.thumbsinplaylist == "true")
            {
                _loc2["btn" + _loc3].bck._height = 40;
                _loc2["btn" + _loc3].icn._y = _loc2["btn" + _loc3].icn._y + 9;
                _loc2["btn" + _loc3]._y = _loc4 * 41;
                _loc2["btn" + _loc3].txt._height = _loc2["btn" + _loc3].txt._height + 20;
                if (feeder.feed[_loc3].author == undefined)
                {
                    _loc2["btn" + _loc3].txt.htmlText = "<b>" + (_loc3 + 1) + "</b>:<br />" + feeder.feed[_loc3].title;
                }
                else
                {
                    _loc2["btn" + _loc3].txt.htmlText = "<b>" + feeder.feed[_loc3].author + "</b>:<br />" + feeder.feed[_loc3].title;
                } // end else if
                if (feeder.feed[_loc3].image != undefined)
                {
                    _loc2["btn" + _loc3].txt._x = _loc2["btn" + _loc3].txt._x + 60;
                    _loc2["btn" + _loc3].txt._width = _loc2["btn" + _loc3].txt._width - 60;
                    thumbLoader = new com.jeroenwijering.utils.ImageLoader(_loc2["btn" + _loc3].img, "true", 60, 40);
                    thumbLoader.loadImage(feeder.feed[_loc3].image);
                    _loc2["btn" + _loc3].img.setMask(_loc2["btn" + _loc3].msk);
                }
                else
                {
                    _loc2["btn" + _loc3].msk._height = 10;
                    _loc2["btn" + _loc3].img._visible = _loc2["btn" + _loc3].msk._visible = false;
                } // end else if
            }
            else
            {
                _loc2["btn" + _loc3]._y = _loc4 * 23;
                if (feeder.feed[_loc3].author == undefined)
                {
                    _loc2["btn" + _loc3].txt.htmlText = feeder.feed[_loc3].title;
                }
                else
                {
                    _loc2["btn" + _loc3].txt.htmlText = feeder.feed[_loc3].author + " - " + feeder.feed[_loc3].title;
                } // end else if
                _loc2["btn" + _loc3].msk._height = 10;
                _loc2["btn" + _loc3].img._visible = _loc2["btn" + _loc3].msk._visible = false;
            } // end else if
            _loc2["btn" + _loc3].txt.textColor = config.frontcolor;
            if (feeder.feed[_loc3].link != undefined)
            {
                _loc2["btn" + _loc3].txt._width = _loc2["btn" + _loc3].txt._width - 20;
                _loc2["btn" + _loc3].icn._x = listWidth - 24;
                _loc2["btn" + _loc3].icn.onRollOver = function ()
                {
                    _parent.col2.setRGB(ref.config.lightcolor);
                };
                _loc2["btn" + _loc3].icn.onRollOut = function ()
                {
                    if (ref.currentItem == _parent.getDepth())
                    {
                        _parent.col2.setRGB(ref.config.backcolor);
                    }
                    else
                    {
                        _parent.col2.setRGB(ref.config.frontcolor);
                    } // end else if
                };
                _loc2["btn" + _loc3].icn.onPress = function ()
                {
                    ref.sendEvent("getlink", _parent.getDepth());
                };
            }
            else
            {
                _loc2["btn" + _loc3].icn._visible = false;
            } // end else if
            ++_loc4;
        } // end of for
        var _loc5 = config.clip.playlistmask;
        if (listRight == true)
        {
            _loc5._x = _loc2._x = Number(config.displaywidth) + 1;
            _loc5._y = _loc2._y = 0;
            _loc5._height = config.displayheight;
        }
        else
        {
            _loc5._y = _loc2._y = Number(config.displayheight) + Number(config.controlbar) - 1;
            _loc5._height = Number(config.height) + 1 - Number(config.controlbar) - Number(config.displayheight);
        } // end else if
        _loc5._width = listWidth;
        _loc2.setMask(_loc5);
        if (_loc2._height > _loc5._height + 2 && feeder.feed.length > 1)
        {
            if (config.autoscroll == "false")
            {
                _loc5._width = _loc5._width - 10;
                for (var _loc3 = 0; _loc3 < feeder.feed.length; ++_loc3)
                {
                    _loc2["btn" + _loc3].bck._width = _loc2["btn" + _loc3].bck._width - 10;
                    _loc2["btn" + _loc3].icn._x = _loc2["btn" + _loc3].icn._x - 10;
                } // end of for
                listScroller = new com.jeroenwijering.utils.Scroller(_loc2, _loc5, false, config.frontcolor, config.lightcolor);
            }
            else
            {
                listScroller = new com.jeroenwijering.utils.Scroller(_loc2, _loc5, true, config.frontcolor, config.lightcolor);
            } // end if
        } // end else if
    } // End of the function
    function setItem(itm)
    {
        var _loc2 = config.clip.playlist;
        _loc2["btn" + currentItem].col.setRGB(config.frontcolor);
        _loc2["btn" + currentItem].bck._alpha = 10;
        _loc2["btn" + currentItem].col2.setRGB(config.frontcolor);
        _loc2["btn" + currentItem].txt.textColor = config.frontcolor;
        currentItem = itm;
        _loc2["btn" + currentItem].txt.textColor = config.backcolor;
        _loc2["btn" + currentItem].col2.setRGB(config.backcolor);
        _loc2["btn" + currentItem].bck._alpha = 90;
        if (config.autoscroll == "false")
        {
            listScroller.scrollTo(_loc2["btn" + currentItem]._y);
        } // end if
    } // End of the function
    function setTime(elp, rem)
    {
        if (feeder.ischapters == true && Math.abs(elp - currentTime) > 5)
        {
            currentTime = elp;
            for (var _loc2 = 0; _loc2 < feeder.feed.length; ++_loc2)
            {
                if (feeder.feed[_loc2].start > currentTime)
                {
                    if (_loc2 != currentItem + 1)
                    {
                        this.setItem(_loc2 - 1);
                    } // end if
                    break;
                } // end if
            } // end of for
        } // end if
    } // End of the function
    function onFullScreen(fs)
    {
        if (listScroller == undefined)
        {
        }
        else if (fs == true)
        {
            config.clip.scrollbar._visible = false;
        }
        else
        {
            config.clip.scrollbar._visible = true;
        } // end else if
    } // End of the function
    function onFeedUpdate()
    {
        var _loc3 = config.clip.playlist;
        for (var _loc2 = 0; _loc2 < 99; ++_loc2)
        {
            _loc3["btn" + _loc2].removeMovieClip();
        } // end of for
        listScroller.purgeScrollbar();
        this.setButtons();
        this.setItem(currentItem);
    } // End of the function
    var currentTime = -10;
} // End of Class
