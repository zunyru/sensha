class com.jeroenwijering.utils.BandwidthCheck
{
    var loader, clip, connector, startTime;
    function BandwidthCheck(fil)
    {
        var ref = this;
        if (fil.indexOf("rtmp") == -1)
        {
            loader = new MovieClipLoader();
            loader.addListener(this);
            clip = _root.createEmptyMovieClip("_bwchecker", 1);
            loader.loadClip(fil + "?" + random(9999), clip);
        }
        else
        {
            connector = new NetConnection();
            connector.onStatus = function (info)
            {
                if (info.code != "NetConnection.Connect.Success")
                {
                    ref.onComplete(0);
                } // end if
            };
            connector.connect(fil, true);
            connector.onBWDone = function (kbps, dtd, dtt, lat)
            {
                ref.onComplete(kbps);
            };
            connector.onBWCheck = function ()
            {
            };
        } // end else if
    } // End of the function
    function onLoadComplete(tgt, hts)
    {
        tgt._visible = false;
        var _loc4 = new Date();
        var _loc6 = clip.getBytesTotal();
        var _loc3 = (_loc4.getTime() - startTime) / 1000;
        var _loc2 = _loc6 * 7.812500E-003 * 9.300000E-001;
        var _loc5 = Math.floor(_loc2 / _loc3);
        this.onComplete(_loc5);
        clip.removeMovieClip();
    } // End of the function
    function onLoadError(tgt, err, stt)
    {
        this.onComplete(0);
    } // End of the function
    function onLoadStart()
    {
        var _loc2 = new Date();
        startTime = _loc2.getTime();
    } // End of the function
    function onComplete()
    {
    } // End of the function
} // End of Class
