class com.jeroenwijering.players.AbstractView
{
    var controller, config, feeder;
    function AbstractView(ctr, cfg, fed)
    {
        controller = ctr;
        config = cfg;
        feeder = fed;
    } // End of the function
    function getUpdate(typ, pr1, pr2)
    {
        trace ("view: " + typ + ": " + pr1 + "," + pr2);
        switch (typ)
        {
            case "state":
            {
                this.setState(pr1);
                break;
            } 
            case "load":
            {
                this.setLoad(pr1);
                break;
            } 
            case "time":
            {
                this.setTime(pr1, pr2);
                break;
            } 
            case "item":
            {
                this.setItem(pr1);
                break;
            } 
            case "size":
            {
                this.setSize(pr1, pr2);
                break;
            } 
            case "volume":
            {
                this.setVolume(pr1);
                break;
            } 
            case "datarate":
            {
                this.setDatarate(pr1);
                break;
            } 
            default:
            {
                trace ("View: incompatible update received");
                break;
            } 
        } // End of switch
    } // End of the function
    function setState(pr1)
    {
    } // End of the function
    function setLoad(pr1)
    {
    } // End of the function
    function setTime(pr1, pr2)
    {
    } // End of the function
    function setItem(pr1)
    {
    } // End of the function
    function setSize(pr1, pr2)
    {
    } // End of the function
    function setVolume(pr1)
    {
    } // End of the function
    function setDatarate(pr1, pr2)
    {
    } // End of the function
    function sendEvent(typ, prm)
    {
        controller.getEvent(typ, prm);
    } // End of the function
} // End of Class
