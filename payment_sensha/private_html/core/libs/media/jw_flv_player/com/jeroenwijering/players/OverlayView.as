class com.jeroenwijering.players.OverlayView extends com.jeroenwijering.players.AbstractView
{
    var config, overlay, loader, feeder;
    function OverlayView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        var ref = this;
        overlay = config.clip.overlay;
        overlay._alpha = 0;
        overlay.icn.swapDepths(2);
        overlay.icn.onPress = function ()
        {
            com.jeroenwijering.utils.Animations.fadeOut(ref.overlay, 0);
            ref.state = 3;
        };
        overlay.createEmptyMovieClip("img", 1);
        loader = new com.jeroenwijering.utils.ImageLoader(overlay.img, "none");
        loader.onLoadFinished = function ()
        {
            ref.setDimensions();
        };
        Stage.addListener(this);
    } // End of the function
    function setDimensions()
    {
        overlay.icn._x = 0;
        overlay.img.mc.gotoAndPlay(1);
        if (Stage.displayState == "fullScreen")
        {
            overlay._xscale = overlay._yscale = 200;
            overlay._x = Stage.width / 2 - overlay._width / 2;
            overlay._y = Stage.height - overlay._height - 50;
            overlay.icn._x = overlay._width / 2 - 20;
        }
        else
        {
            overlay._xscale = overlay._yscale = 100;
            overlay._x = config.displaywidth / 2 - overlay._width / 2;
            overlay._y = config.displayheight - overlay._height - 10;
            overlay.icn._x = overlay._width - 20;
        } // end else if
    } // End of the function
    function setItem(itm)
    {
        if (feeder.feed[itm].overlayfile != undefined)
        {
            loader.loadImage(feeder.feed[itm].overlayfile);
            var lnk = feeder.feed[itm].overlaylink;
            var tgt = config.linktarget;
            overlay.img.onPress = function ()
            {
                getURL(lnk, tgt);
            };
            state = 1;
        }
        else
        {
            overlay._visible = false;
            state = 0;
        } // end else if
    } // End of the function
    function setTime(elp, rem)
    {
        if (elp > 2 && state == 1)
        {
            state = 2;
            overlay.img.mc.gotoAndPlay(1);
            com.jeroenwijering.utils.Animations.fadeIn(overlay, 100);
        }
        else if (rem < 2 && state == 2)
        {
            com.jeroenwijering.utils.Animations.fadeOut(overlay, 0);
            state = 3;
        } // end else if
    } // End of the function
    function setState(stt)
    {
        if (stt == 3 && state == 3)
        {
            state = 1;
        } // end if
    } // End of the function
    function onResize()
    {
        this.setDimensions();
    } // End of the function
    function onFullScreen(fs)
    {
        if (fs == false)
        {
            this.setDimensions();
        } // end if
    } // End of the function
    var state = 0;
} // End of Class
