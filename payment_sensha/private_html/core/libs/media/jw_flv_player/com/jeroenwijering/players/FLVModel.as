class com.jeroenwijering.players.FLVModel extends com.jeroenwijering.players.AbstractModel
{
    var connectObject, videoClip, config, soundObject, isActive, flvType, feeder, currentItem, currentPosition, currentURL, streamObject, positionInterval, loadedInterval, sendUpdate, currentState, sendCompleteEvent, currentVolume, onMetaData;
    function FLVModel(vws, ctr, cfg, fed, fcl)
    {
        super(vws, ctr, cfg, fed);
        connectObject = new NetConnection();
        videoClip = fcl;
        if (config.smoothing == "false")
        {
            videoClip.display.smoothing = false;
            videoClip.display.deblocking = 1;
        }
        else
        {
            videoClip.display.smoothing = true;
            videoClip.display.deblocking = 4;
        } // end else if
        videoClip.createEmptyMovieClip("snd", videoClip.getNextHighestDepth());
        soundObject = new Sound(videoClip.snd);
    } // End of the function
    function setItem(idx)
    {
        super.setItem(idx);
        if (isActive == true)
        {
            if (config.streamscript != undefined)
            {
                flvType = "HTTP";
            }
            else if (feeder.feed[currentItem].type == "rtmp")
            {
                flvType = "RTMP";
            }
            else
            {
                flvType = "FLV";
            } // end else if
        } // end else if
    } // End of the function
    function setStart(pos)
    {
        if (pos != undefined)
        {
            currentPosition = pos;
        } // end if
        if (pos < 1)
        {
            pos = 0;
        }
        else if (pos > metaDuration - 1)
        {
            pos = metaDuration - 1;
        } // end else if
        if (flvType == "RTMP" && feeder.feed[currentItem].id != currentURL)
        {
            connectObject.connect(feeder.feed[currentItem].file);
            currentURL = feeder.feed[currentItem].id;
            this.setStreamObject(connectObject);
            streamObject.play(currentURL);
        }
        else if (flvType != "RTMP" && feeder.feed[currentItem].file != currentURL)
        {
            connectObject.connect(null);
            currentURL = feeder.feed[currentItem].file;
            if (flvType == "HTTP")
            {
                this.setStreamObject(connectObject);
                if (config.streamscript == "lighttpd")
                {
                    streamObject.play(currentURL);
                }
                else
                {
                    streamObject.play(config.streamscript + "?file=" + currentURL);
                } // end else if
            }
            else
            {
                this.setStreamObject(connectObject);
                streamObject.play(currentURL);
            } // end else if
        }
        else
        {
            if (pos != undefined)
            {
                streamObject.seek(pos);
            } // end if
            streamObject.pause(false);
        } // end else if
        videoClip._visible = true;
        videoClip._parent.thumb._visible = false;
        if (flvType == "HTTP" && pos > 0)
        {
            this.playKeyframe(currentPosition);
        }
        else if (flvType == "FLV" && pos > 0)
        {
            streamObject.seek(currentPosition);
        }
        else if (flvType == "RTMP" && pos > 0)
        {
            streamObject.seek(currentPosition);
        } // end else if
        clearInterval(positionInterval);
        positionInterval = setInterval(this, "updatePosition", 200);
        clearInterval(loadedInterval);
        loadedInterval = setInterval(this, "updateLoaded", 200);
    } // End of the function
    function updateLoaded()
    {
        if (flvType == "FLV")
        {
            var _loc2 = Math.round(streamObject.bytesLoaded / streamObject.bytesTotal * 100);
        }
        else
        {
            _loc2 = Math.round(streamObject.bufferLength / streamObject.bufferTime * 100);
        } // end else if
        if (isNaN(_loc2))
        {
            currentLoaded = 0;
            this.sendUpdate("load", 0);
        }
        else if (_loc2 > 95)
        {
            clearInterval(loadedInterval);
            currentLoaded = 100;
            this.sendUpdate("load", 100);
        }
        else if (_loc2 != currentLoaded)
        {
            currentLoaded = _loc2;
            this.sendUpdate("load", currentLoaded);
        } // end else if
    } // End of the function
    function updatePosition()
    {
        var _loc2 = streamObject.time;
        if (config.bufferlength < 3)
        {
            var _loc3 = 2;
        }
        else
        {
            _loc3 = config.bufferlength - 1;
        } // end else if
        if (_loc2 == currentPosition && currentState != 1 && streamObject.bufferLength < _loc3)
        {
            currentState = 1;
            this.sendUpdate("state", 1);
        }
        else if (_loc2 != currentPosition && currentState != 2)
        {
            currentState = 2;
            this.sendUpdate("state", 2);
        } // end else if
        if (_loc2 != currentPosition)
        {
            currentPosition = _loc2;
            if (metaDuration < currentPosition)
            {
                metaDuration = currentPosition;
            } // end if
            this.sendUpdate("time", currentPosition, metaDuration - currentPosition);
        }
        else if (streamObject.bufferLength < _loc3 && stopFired == true)
        {
            currentState = 3;
            videoClip._visible = false;
            videoClip._parent.thumb._visible = true;
            this.sendUpdate("state", 3);
            this.sendCompleteEvent();
            stopFired = false;
        } // end else if
    } // End of the function
    function setPause(pos)
    {
        if (pos < 1)
        {
            pos = 0;
        } // end if
        clearInterval(positionInterval);
        if (pos != undefined)
        {
            currentPosition = pos;
            this.sendUpdate("time", currentPosition, Math.abs(metaDuration - currentPosition));
            streamObject.seek(currentPosition);
        } // end if
        streamObject.pause(true);
        currentState = 0;
        this.sendUpdate("state", 0);
    } // End of the function
    function setStop(pos)
    {
        clearInterval(loadedInterval);
        clearInterval(positionInterval);
        videoClip._visible = false;
        delete this.currentURL;
        delete this.currentLoaded;
        delete this.currentPosition;
        delete this.metaKeyframes;
        metaDuration = 0;
        currentLoaded = 0;
        stopFired = false;
        streamObject.close();
        delete this.streamObject;
    } // End of the function
    function setVolume(vol)
    {
        super.setVolume(vol);
        currentVolume = vol;
        soundObject.setVolume(vol);
    } // End of the function
    function setStreamObject(cnt)
    {
        var ref = this;
        currentLoaded = 0;
        this.sendUpdate("load", 0);
        streamObject = new NetStream(cnt);
        streamObject.setBufferTime(config.bufferlength);
        streamObject.onMetaData = function (obj)
        {
            ref.sendUpdate("datarate", obj.videodatarate, obj.audiodatarate);
            obj.duration > 1 ? (ref.metaDuration = obj.duration) : (null);
            if (obj.width > 10)
            {
                ref.sendUpdate("size", obj.width, obj.height);
            } // end if
            ref.frameRate = obj.framerate;
            ref.metaKeyframes = obj.keyframes;
            if (ref.feeder.feed[ref.currentItem].start > 0)
            {
                if (ref.flvType == "HTTP")
                {
                    ref.playKeyframe(ref.feeder.feed[ref.currentItem].start);
                }
                else if (ref.flvType == "RTMP")
                {
                    ref.setStart(ref.feeder.feed[ref.currentItem].start);
                } // end if
            } // end else if
            false;
            delete this.onMetaData;
        };
        streamObject.onStatus = function (object)
        {
            trace (object.code);
            if (object.code == "NetStream.Buffer.Flush" || object.code == "NetStream.Play.Stop")
            {
                ref.stopFired = true;
            }
            else if (object.code == "NetStream.Play.StreamNotFound")
            {
                ref.currentState = 3;
                ref.videoClip._visible = false;
                ref.sendUpdate("state", 3);
                ref.sendCompleteEvent();
                ref.stopFired = false;
            }
            else if (object.code == "NetStream.Play.Start")
            {
                ref.stopFired = false;
            } // end else if
        };
        streamObject.onCaption = function (cap)
        {
            ref.capView.onCaptionate(cap);
        };
        videoClip.display.attachVideo(streamObject);
        videoClip.snd.attachAudio(streamObject);
    } // End of the function
    function playKeyframe(pos)
    {
        for (var _loc2 = 0; _loc2 < metaKeyframes.times.length; ++_loc2)
        {
            if (metaKeyframes.times[_loc2] <= pos && metaKeyframes.times[_loc2 + 1] >= pos)
            {
                if (config.streamscript == "lighttpd")
                {
                    streamObject.play(currentURL + "?start=" + metaKeyframes.filepositions[_loc2]);
                }
                else
                {
                    streamObject.play(config.streamscript + "?file=" + currentURL + "&pos=" + metaKeyframes.filepositions[_loc2]);
                } // end else if
                break;
            } // end if
        } // end of for
    } // End of the function
    var mediatypes = new Array("flv", "rtmp", "mp4", "m4v", "m4a", "mov", "3gp", "3g2");
    var currentLoaded = 0;
    var metaDuration = 0;
    var metaKeyframes = new Object();
    var stopFired = false;
} // End of Class
