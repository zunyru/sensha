class com.jeroenwijering.feeds.ATOMParser extends com.jeroenwijering.feeds.AbstractParser
{
    var elements, iso2Date, mimetypes;
    function ATOMParser()
    {
        super();
    } // End of the function
    function setElements()
    {
        elements = new Object();
        elements.title = "title";
        elements.id = "id";
    } // End of the function
    function parse(xml)
    {
        var _loc9 = new Array();
        for (var _loc6 = xml.firstChild.firstChild; _loc6 != null; _loc6 = _loc6.nextSibling)
        {
            if (_loc6.nodeName.toLowerCase() == "entry")
            {
                var _loc3 = new Object();
                for (var _loc7 = 0; _loc7 < _loc6.childNodes.length; ++_loc7)
                {
                    var _loc2 = _loc6.childNodes[_loc7];
                    var _loc4 = _loc2.nodeName.toLowerCase();
                    if (elements[_loc4] != undefined)
                    {
                        _loc3[elements[_loc4]] = _loc2.firstChild.nodeValue;
                        continue;
                    } // end if
                    if (_loc4 == "link" && _loc2.attributes.rel == "alternate")
                    {
                        _loc3.link = _loc2.attributes.href;
                        continue;
                    } // end if
                    if (_loc4 == "summary")
                    {
                        _loc3.description = com.jeroenwijering.utils.StringMagic.stripTagsBreaks(_loc2.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "published")
                    {
                        _loc3.date = this.iso2Date(_loc2.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "updated")
                    {
                        _loc3.date = this.iso2Date(_loc2.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "modified")
                    {
                        _loc3.date = this.iso2Date(_loc2.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "category")
                    {
                        _loc3.category = _loc2.attributes.term;
                        continue;
                    } // end if
                    if (_loc4 == "author")
                    {
                        for (var _loc5 = 0; _loc5 < _loc2.childNodes.length; ++_loc5)
                        {
                            if (_loc2.childNodes[_loc5].nodeName == "name")
                            {
                                _loc3.author = _loc2.childNodes[_loc5].firstChild.nodeValue;
                            } // end if
                        } // end of for
                        continue;
                    } // end if
                    if (_loc4 == "link" && _loc2.attributes.rel == "enclosure")
                    {
                        var _loc8 = _loc2.attributes.type.toLowerCase();
                        if (mimetypes[_loc8] != undefined && _loc3.type != "flv")
                        {
                            _loc3.file = _loc2.attributes.href;
                            _loc3.type = mimetypes[_loc8];
                            if (_loc3.file.substr(0, 4) == "rtmp")
                            {
                                _loc3.type = "rtmp";
                            } // end if
                        } // end if
                        continue;
                    } // end if
                    if (_loc4 == "link" && _loc2.attributes.rel == "captions")
                    {
                        _loc3.captions = _loc2.attributes.href;
                        continue;
                    } // end if
                    if (_loc4 == "link" && _loc2.attributes.rel == "audio")
                    {
                        _loc3.audio = _loc2.attributes.href;
                        continue;
                    } // end if
                    if (_loc4 == "link" && _loc2.attributes.rel == "image")
                    {
                        _loc3.image = _loc2.attributes.href;
                    } // end if
                } // end of for
                _loc3.author == undefined ? (_loc3.author = ttl) : (null);
                _loc9.push(_loc3);
                continue;
            } // end if
            if (_loc6.nodeName == "title")
            {
                var ttl = _loc6.firstChild.nodeValue;
            } // end if
        } // end of for
        return (_loc9);
    } // End of the function
} // End of Class
