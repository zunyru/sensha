class com.jeroenwijering.players.ControlbarView extends com.jeroenwijering.players.AbstractView implements com.jeroenwijering.feeds.FeedListener
{
    var feeder, config, col1, col2, col, _parent, col3, msk, _xmouse, hideInt, barWidths, itemLength, currentItem;
    function ControlbarView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        this.setColorsClicks();
        this.setDimensions();
        Stage.addListener(this);
        feeder.addListener(this);
    } // End of the function
    function setColorsClicks()
    {
        var ref = this;
        var _loc2 = config.clip.controlbar;
        _loc2.col = new Color(_loc2.back);
        _loc2.col.setRGB(config.backcolor);
        _loc2.playpause.col1 = new Color(_loc2.playpause.ply);
        _loc2.playpause.col1.setRGB(config.frontcolor);
        _loc2.playpause.col2 = new Color(_loc2.playpause.pas);
        _loc2.playpause.col2.setRGB(config.frontcolor);
        _loc2.playpause.onRollOver = function ()
        {
            col1.setRGB(ref.config.lightcolor);
            col2.setRGB(ref.config.lightcolor);
        };
        _loc2.playpause.onRollOut = function ()
        {
            col1.setRGB(ref.config.frontcolor);
            col2.setRGB(ref.config.frontcolor);
        };
        _loc2.playpause.onPress = function ()
        {
            ref.sendEvent("playpause");
        };
        _loc2.prev.col = new Color(_loc2.prev.icn);
        _loc2.prev.col.setRGB(config.frontcolor);
        _loc2.prev.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
        };
        _loc2.prev.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
        };
        _loc2.prev.onPress = function ()
        {
            ref.sendEvent("prev");
        };
        _loc2.next.col = new Color(_loc2.next.icn);
        _loc2.next.col.setRGB(config.frontcolor);
        _loc2.next.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
        };
        _loc2.next.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
        };
        _loc2.next.onPress = function ()
        {
            ref.sendEvent("next");
        };
        _loc2.scrub.elpTxt.textColor = config.frontcolor;
        _loc2.scrub.remTxt.textColor = config.frontcolor;
        _loc2.scrub.col = new Color(_loc2.scrub.icn);
        _loc2.scrub.col.setRGB(config.frontcolor);
        _loc2.scrub.col2 = new Color(_loc2.scrub.bar);
        _loc2.scrub.col2.setRGB(config.frontcolor);
        _loc2.scrub.col3 = new Color(_loc2.scrub.bck);
        _loc2.scrub.col3.setRGB(config.frontcolor);
        _loc2.scrub.bck.onRollOver = function ()
        {
            _parent.col.setRGB(ref.config.lightcolor);
        };
        _loc2.scrub.bck.onRollOut = function ()
        {
            _parent.col.setRGB(ref.config.frontcolor);
        };
        _loc2.scrub.bck.onPress = function ()
        {
            function onEnterFrame()
            {
                var _loc2 = _parent._xmouse;
                if (_loc2 < _parent.bck._width + _parent.bck._x && _loc2 > _parent.bck._x)
                {
                    _parent.icn._x = _parent._xmouse - 1;
                } // end if
            } // End of the function
        };
        _loc2.scrub.bck.onRelease = _loc2.scrub.bck.onReleaseOutside = function ()
        {
            var _loc2 = (_parent._xmouse - _parent.bar._x) / ref.barWidths * ref.itemLength;
            ref.sendEvent("scrub", Math.round(_loc2));
            delete this.onEnterFrame;
        };
        _loc2.scrub.bck.tabEnabled = false;
        _loc2.fs.col1 = new Color(_loc2.fs.ns);
        _loc2.fs.col2 = new Color(_loc2.fs.fs);
        _loc2.fs.col.setRGB(ref.config.frontcolor);
        _loc2.fs.col2.setRGB(ref.config.frontcolor);
        _loc2.fs.onRollOver = function ()
        {
            col1.setRGB(ref.config.lightcolor);
            col2.setRGB(ref.config.lightcolor);
        };
        _loc2.fs.onRollOut = function ()
        {
            col1.setRGB(ref.config.frontcolor);
            col2.setRGB(ref.config.frontcolor);
        };
        _loc2.fs.onPress = function ()
        {
            ref.sendEvent("fullscreen");
            col1.setRGB(ref.config.frontcolor);
            col2.setRGB(ref.config.frontcolor);
        };
        _loc2.cc.col = new Color(_loc2.cc.icn);
        _loc2.cc.col.setRGB(ref.config.frontcolor);
        _loc2.cc.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
        };
        _loc2.cc.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
        };
        _loc2.cc.onPress = function ()
        {
            ref.sendEvent("captions");
        };
        _loc2.au.col = new Color(_loc2.au.icn);
        _loc2.au.col.setRGB(ref.config.frontcolor);
        _loc2.au.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
        };
        _loc2.au.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
        };
        _loc2.au.onPress = function ()
        {
            ref.sendEvent("audio");
        };
        _loc2.dl.col = new Color(_loc2.dl.icn);
        _loc2.dl.col.setRGB(ref.config.frontcolor);
        _loc2.dl.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
        };
        _loc2.dl.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
        };
        _loc2.dl.onPress = function ()
        {
            ref.sendEvent("getlink", ref.currentItem);
        };
        _loc2.vol.col = new Color(_loc2.vol.bar);
        _loc2.vol.col.setRGB(config.frontcolor);
        _loc2.vol.col2 = new Color(_loc2.vol.bck);
        _loc2.vol.col2.setRGB(config.frontcolor);
        _loc2.vol.col3 = new Color(_loc2.vol.icn);
        _loc2.vol.col3.setRGB(config.frontcolor);
        _loc2.vol.onRollOver = function ()
        {
            col.setRGB(ref.config.lightcolor);
            col3.setRGB(ref.config.lightcolor);
        };
        _loc2.vol.onRollOut = function ()
        {
            col.setRGB(ref.config.frontcolor);
            col3.setRGB(ref.config.frontcolor);
        };
        _loc2.vol.onPress = function ()
        {
            function onEnterFrame()
            {
                msk._width = _xmouse - 12;
            } // End of the function
        };
        _loc2.vol.onRelease = _loc2.vol.onReleaseOutside = function ()
        {
            ref.sendEvent("volume", (_xmouse - 12) * 5);
            delete this.onEnterFrame;
        };
        if (config.displayheight == config.height)
        {
            Mouse.addListener(this);
        } // end if
    } // End of the function
    function setDimensions()
    {
        clearInterval(hideInt);
        var _loc2 = config.clip.controlbar;
        if (Stage.displayState == "fullScreen")
        {
            _loc2._x = Math.round(Stage.width / 2 - 200);
            var _loc4 = 400;
            _loc2._y = Stage.height - 40;
            _loc2._alpha = 100;
            _loc2.back._alpha = 40;
            _loc2.fs.fs._visible = false;
            _loc2.fs.ns._visible = true;
        }
        else if (config.displayheight == config.height)
        {
            _loc2._y = config.displayheight - 40;
            if (config.displaywidth > 450 && config.displaywidth == config.width)
            {
                _loc2._x = Math.round(Stage.width / 2 - 200);
                _loc4 = 400;
            }
            else
            {
                _loc2._x = 20;
                _loc4 = config.displaywidth - 40;
            } // end else if
            _loc2._alpha = 0;
            _loc2._visible = false;
            _loc2.back._alpha = 40;
            _loc2.fs.fs._visible = true;
            _loc2.fs.ns._visible = false;
        }
        else
        {
            _loc2._x = 0;
            _loc2._y = config.displayheight;
            _loc4 = config.width;
            _loc2._alpha = 100;
            _loc2.back._alpha = 100;
            _loc2.fs.fs._visible = true;
            _loc2.fs.ns._visible = false;
        } // end else if
        if (config.largecontrols == "true")
        {
            _loc2._xscale = _loc2._yscale = 200;
            if (Stage.displayState == "fullScreen")
            {
                _loc2._y = Stage.height - 60;
                _loc4 = 300;
                _loc2._x = Math.round(Stage.width / 2 - 300);
            }
            else
            {
                _loc4 = _loc4 / 2;
            } // end if
        } // end else if
        _loc2.back._width = _loc4;
        if (feeder.feed.length - feeder.numads == 1 || config.displayheight < config.height - 50 && _loc4 < 200 || config.displaywidth < config.width - 50 && _loc4 < 200)
        {
            _loc2.prev._visible = _loc2.next._visible = false;
            _loc2.scrub.shd._width = _loc4 - 17;
            _loc2.scrub._x = 17;
        }
        else
        {
            _loc2.prev._visible = _loc2.next._visible = true;
            _loc2.scrub.shd._width = _loc4 - 51;
            _loc2.scrub._x = 51;
        } // end else if
        var _loc3 = _loc4;
        if (_loc4 > 50 && config.showvolume == "true")
        {
            _loc3 = _loc3 - 37;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 37;
            _loc2.vol._x = _loc3;
        }
        else
        {
            _loc3 = _loc3 - 1;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 1;
            _loc2.vol._x = _loc3;
        } // end else if
        if (feeder.audio == true)
        {
            _loc3 = _loc3 - 17;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 17;
            _loc2.au._x = _loc3;
        }
        else
        {
            _loc2.au._visible = false;
        } // end else if
        if (feeder.captions == true)
        {
            _loc3 = _loc3 - 17;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 17;
            _loc2.cc._x = _loc3;
        }
        else
        {
            _loc2.cc._visible = false;
        } // end else if
        if (config.showdownload == "true")
        {
            _loc3 = _loc3 - 17;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 17;
            _loc2.dl._x = _loc3;
        }
        else
        {
            _loc2.dl._visible = false;
        } // end else if
        if ((Stage.displayState == undefined || config.usefullscreen == "false" || feeder.onlymp3s == true) && config.fsbuttonlink == undefined)
        {
            _loc2.fs._visible = false;
        }
        else
        {
            _loc3 = _loc3 - 18;
            _loc2.scrub.shd._width = _loc2.scrub.shd._width - 18;
            _loc2.fs._x = _loc3;
        } // end else if
        if (config.showdigits == "false" || _loc2.scrub.shd._width < 120 || System.capabilities.version.indexOf("7,0,") > -1)
        {
            _loc2.scrub.elpTxt._visible = _loc2.scrub.remTxt._visible = false;
            _loc2.scrub.bar._x = _loc2.scrub.bck._x = _loc2.scrub.icn._x = 5;
            barWidths = _loc2.scrub.bck._width = _loc2.scrub.shd._width - 10;
        }
        else
        {
            _loc2.scrub.elpTxt._visible = _loc2.scrub.remTxt._visible = true;
            _loc2.scrub.bar._x = _loc2.scrub.bck._x = _loc2.scrub.icn._x = 42;
            barWidths = _loc2.scrub.bck._width = _loc2.scrub.shd._width - 84;
            _loc2.scrub.remTxt._x = _loc2.scrub.shd._width - 39;
        } // end else if
        _loc2.scrub.bar._width = 0;
    } // End of the function
    function setState(stt)
    {
        var _loc2 = config.clip.controlbar.playpause;
        switch (stt)
        {
            case 0:
            {
                _loc2.ply._visible = true;
                _loc2.pas._visible = false;
                break;
            } 
            case 1:
            {
                _loc2.pas._visible = true;
                _loc2.ply._visible = false;
                break;
            } 
            case 2:
            {
                _loc2.pas._visible = true;
                _loc2.ply._visible = false;
                break;
            } 
        } // End of switch
    } // End of the function
    function setTime(elp, rem)
    {
        itemLength = elp + rem;
        itemProgress = Math.round(rem / itemLength * 100);
        var _loc3 = config.clip.controlbar.scrub;
        var _loc6 = Math.floor(elp / (elp + rem) * barWidths) - 2;
        elp == 0 || _loc6 < 2 ? (_loc3.bar._width = 0) : (_loc3.bar._width = _loc6 - 2);
        _loc3.icn._x = _loc3.bar._width + _loc3.bar._x + 1;
        _loc3.elpTxt.text = com.jeroenwijering.utils.StringMagic.addLeading(elp / 60) + ":" + com.jeroenwijering.utils.StringMagic.addLeading(elp % 60);
        if (_loc3.bck._width == barWidths)
        {
            if (_root.showdigits == "total")
            {
                _loc3.remTxt.text = com.jeroenwijering.utils.StringMagic.addLeading((elp + rem) / 60) + ":" + com.jeroenwijering.utils.StringMagic.addLeading((elp + rem) % 60);
            }
            else
            {
                _loc3.remTxt.text = com.jeroenwijering.utils.StringMagic.addLeading(rem / 60) + ":" + com.jeroenwijering.utils.StringMagic.addLeading(rem % 60);
            } // end if
        } // end else if
    } // End of the function
    function setItem(prm)
    {
        wasLoaded = false;
        currentItem = prm;
        if (feeder.feed[currentItem].category == "preroll" || feeder.feed[currentItem].category == "postroll")
        {
            config.clip.controlbar.scrub.icn._alpha = 0;
        }
        else
        {
            config.clip.controlbar.scrub.icn._alpha = 100;
        } // end else if
    } // End of the function
    function setLoad(pct)
    {
        var _loc2 = config.clip.controlbar.scrub;
        if (wasLoaded == false)
        {
            _loc2.bck._width = Math.round(barWidths * pct / 100);
        } // end if
        _loc2.remTxt.text = Math.round(pct) + " %";
        pct == 100 ? (wasLoaded = true) : (null);
    } // End of the function
    function setVolume(pr1)
    {
        var _loc2 = config.clip.controlbar.vol;
        _loc2.msk._width = Math.round(pr1 / 5);
        if (pr1 == 0)
        {
            _loc2.icn._alpha = 40;
        }
        else
        {
            _loc2.icn._alpha = 100;
        } // end else if
    } // End of the function
    function onResize()
    {
        if (_root.displayheight > config.height + 10)
        {
            config.height = config.displayheight = Stage.height;
            config.width = config.displaywidth = Stage.width;
        } // end if
        this.setDimensions();
    } // End of the function
    function onFullScreen(fs)
    {
        if (fs == false)
        {
            this.setDimensions();
        } // end if
    } // End of the function
    function hideBar()
    {
        com.jeroenwijering.utils.Animations.fadeOut(config.clip.controlbar);
        clearInterval(hideInt);
    } // End of the function
    function onMouseMove()
    {
        if (Stage.displayState != "fullScreen" && config.clip._xmouse < config.displaywidth && config.showicons == "true")
        {
            com.jeroenwijering.utils.Animations.fadeIn(config.clip.controlbar);
            clearInterval(hideInt);
            if (!config.clip.controlbar.hitTest(_xmouse, _ymouse))
            {
                hideInt = setInterval(this, "hideBar", 2000);
            } // end if
        } // end if
    } // End of the function
    function onFeedUpdate()
    {
        this.setDimensions();
    } // End of the function
    var itemProgress = 0;
    var wasLoaded = false;
} // End of Class
