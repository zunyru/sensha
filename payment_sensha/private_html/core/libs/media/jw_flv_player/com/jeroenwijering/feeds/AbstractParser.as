class com.jeroenwijering.feeds.AbstractParser
{
    var elements, mimetypes;
    function AbstractParser(pre)
    {
        this.setElements();
        this.setMimes();
    } // End of the function
    function setElements()
    {
        elements = new Object();
    } // End of the function
    function setMimes()
    {
        mimetypes = new Object();
        mimetypes.mp3 = "mp3";
        mimetypes["audio/mpeg"] = "mp3";
        mimetypes.flv = "flv";
        mimetypes["video/x-flv"] = "flv";
        mimetypes.jpeg = "jpg";
        mimetypes.jpg = "jpg";
        mimetypes["image/jpeg"] = "jpg";
        mimetypes.png = "png";
        mimetypes["image/png"] = "png";
        mimetypes.gif = "gif";
        mimetypes["image/gif"] = "gif";
        mimetypes.rtmp = "rtmp";
        mimetypes.swf = "swf";
        mimetypes["application/x-shockwave-flash"] = "swf";
        mimetypes.rtmp = "rtmp";
        mimetypes["application/x-fcs"] = "rtmp";
    } // End of the function
    function parse(xml)
    {
        var _loc3 = new Array();
        for (var _loc1 = 0; _loc1 < xml.firstChild.childNodes.length; ++_loc1)
        {
            _loc3.push(xml.firstChild.childNodes[_loc1].nodeName);
        } // end of for
        return (_loc3);
    } // End of the function
    function rfc2Date(dat)
    {
        if (isNaN(dat))
        {
            var _loc2 = dat.split(" ");
            _loc2[1] == "" ? (_loc2.splice(1, 1)) : (null);
            var _loc9 = MONTH_INDEXES[_loc2[2]];
            var _loc7 = _loc2[1].substring(0, 2);
            var _loc10 = _loc2[3];
            var _loc4 = _loc2[5];
            var _loc5 = _loc2[4].split(":");
            var _loc6 = new Date(_loc10, _loc9, _loc7, _loc5[0], _loc5[1], _loc5[2]);
            var _loc3 = Math.round(_loc6.valueOf() / 1000) - _loc6.getTimezoneOffset() * 60;
            if (isNaN(_loc4))
            {
                _loc3 = _loc3 - 3600 * timezones[_loc4];
            }
            else
            {
                _loc3 = _loc3 - (3600 * Number(_loc4.substring(0, 3)) - 60 * Number(_loc4.substring(3, 2)));
            } // end else if
            dat = new Date(_loc3 * 1000);
            return (_loc3);
        }
        else
        {
            return (dat);
        } // end else if
    } // End of the function
    function iso2Date(dat)
    {
        if (isNaN(dat))
        {
            while (dat.indexOf(" ") > -1)
            {
                var _loc1 = dat.indexOf(" ");
                dat = dat.substr(0, _loc1) + dat.substr(_loc1 + 1);
            } // end while
            var _loc3 = new Date(dat.substr(0, 4), dat.substr(5, 2) - 1, dat.substr(8, 2), dat.substr(11, 2), dat.substr(14, 2), dat.substr(17, 2));
            var _loc2 = Math.round(_loc3.valueOf() / 1000) - _loc3.getTimezoneOffset() * 60;
            if (dat.length > 20)
            {
                var _loc4 = Number(dat.substr(20, 2));
                var _loc5 = Number(dat.substr(23, 2));
                if (dat.charAt(19) == "-")
                {
                    _loc2 = _loc2 - _loc4 * 3600 - _loc5 * 60;
                }
                else
                {
                    _loc2 = _loc2 + (_loc4 * 3600 + _loc5 * 60);
                } // end else if
                dat = new Date(_loc2 * 1000);
            } // end if
            return (_loc2);
        }
        else
        {
            return (dat);
        } // end else if
    } // End of the function
    var timezones = {IDLW: -12, NT: -11, AHST: -10, CAT: -10, HST: -10, YST: -9, PST: -8, MST: -7, PDT: -7, CST: -6, EST: -5, CDT: -5, EDT: -4, ADT: -3, WBT: -4, AST: -4, NT: -3.500000E+000, EBT: -3, AT: -2, WAT: -1, UTC: 0, UT: 0, GMT: 0, WET: 0, CET: 1, CEST: 1, EET: 2, EEDT: 3, MSK: 3, IRT: 3.500000E+000, SAMT: 4, YEKT: 5, TMT: 5, TJT: 5, OMST: 6, NOVT: 6, LKT: 6, MMT: 6.500000E+000, KRAT: 7, ICT: 7, WIT: 7, WAST: 7, IRKT: 8, ULAT: 8, CST: 8, CIT: 8, BNT: 8, YAKT: 9, JST: 9, KST: 9, EIT: 9, ACST: 9.500000E+000, VLAT: 10, ACDT: 1.050000E+001, SAKT: 10, GST: 10, MAGT: 11, IDLE: 12, PETT: 12, NZST: 12};
    var MONTH_INDEXES = {January: 0, February: 1, March: 2, April: 3, May: 4, June: 5, July: 6, August: 7, September: 8, October: 9, November: 10, December: 11, Jan: 0, Feb: 1, Mar: 2, Apr: 3, May: 4, Jun: 5, Jul: 6, Aug: 7, Sep: 8, Oct: 9, Nov: 10, Dec: 11};
} // End of Class
