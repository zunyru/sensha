class com.jeroenwijering.players.AbstractController implements com.jeroenwijering.feeds.FeedListener
{
    var config, feeder, randomizer, currentItem, currentURL, registeredModels;
    function AbstractController(cfg, fed)
    {
        config = cfg;
        feeder = fed;
        if (config.shuffle == "true")
        {
            randomizer = new com.jeroenwijering.utils.Randomizer(feeder.feed);
            currentItem = randomizer.pick();
        }
        else
        {
            currentItem = 0;
        } // end else if
        feeder.addListener(this);
    } // End of the function
    function startMCV(mar)
    {
    } // End of the function
    function getEvent(typ, prm)
    {
        trace ("controller: " + typ + ": " + prm);
        switch (typ)
        {
            case "playpause":
            {
                this.setPlaypause(prm);
                break;
            } 
            case "prev":
            {
                this.setPrev();
                break;
            } 
            case "next":
            {
                this.setNext();
                break;
            } 
            case "stop":
            {
                this.setStop();
                break;
            } 
            case "scrub":
            {
                this.setScrub(prm);
                break;
            } 
            case "volume":
            {
                this.setVolume(prm);
                break;
            } 
            case "playitem":
            {
                this.setPlayitem(prm);
                break;
            } 
            case "getlink":
            {
                this.setGetlink(prm);
                break;
            } 
            case "fullscreen":
            {
                this.setFullscreen();
                break;
            } 
            case "complete":
            {
                this.setComplete();
                break;
            } 
            case "captions":
            {
                this.setCaptions();
                break;
            } 
            case "audio":
            {
                this.setAudio();
                break;
            } 
            default:
            {
                trace ("controller: incompatible event received");
                break;
            } 
        } // End of switch
    } // End of the function
    function setPlaypause()
    {
    } // End of the function
    function setPrev()
    {
    } // End of the function
    function setNext()
    {
    } // End of the function
    function setStop()
    {
    } // End of the function
    function setScrub(prm)
    {
    } // End of the function
    function setPlayitem(itm)
    {
        currentURL = feeder.feed[itm].file;
    } // End of the function
    function setGetlink(idx)
    {
    } // End of the function
    function setComplete()
    {
    } // End of the function
    function setVolume(prm)
    {
    } // End of the function
    function setFullscreen()
    {
    } // End of the function
    function setCaptions()
    {
    } // End of the function
    function setAudio()
    {
    } // End of the function
    function sendChange(typ, prm)
    {
        for (var _loc2 = 0; _loc2 < registeredModels.length; ++_loc2)
        {
            registeredModels[_loc2].getChange(typ, prm);
        } // end of for
    } // End of the function
    function onFeedUpdate()
    {
        for (var _loc2 = 0; _loc2 < feeder.feed.length; ++_loc2)
        {
            if (feeder.feed[_loc2].file == currentURL)
            {
                currentItem = _loc2;
                this.sendChange("item", currentItem);
            } // end if
        } // end of for
        if (feeder.feed[currentItem].file != currentURL)
        {
            this.setStop();
            if (randomizer != undefined)
            {
                randomizer = new com.jeroenwijering.utils.Randomizer(feeder.feed);
            } // end if
            if (currentItem >= feeder.feed.length)
            {
                if (config.shuffle == "false")
                {
                    currentItem = 0;
                }
                else
                {
                    currentItem = randomizer.pick();
                } // end else if
                this.sendChange("item", currentItem);
            } // end if
            if (config.autostart == "true")
            {
                this.sendChange("start");
            } // end if
        } // end if
    } // End of the function
    var itemsPlayed = 0;
} // End of Class
