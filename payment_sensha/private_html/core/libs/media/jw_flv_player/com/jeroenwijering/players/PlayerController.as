class com.jeroenwijering.players.PlayerController extends com.jeroenwijering.players.AbstractController
{
    var playerSO, config, registeredModels, feeder, currentItem, sendChange, isPlaying, currentURL, itemsPlayed, randomizer;
    function PlayerController(cfg, fed)
    {
        super(cfg, fed);
        playerSO = SharedObject.getLocal("com.jeroenwijerin.players", "/");
        if (playerSO.data.volume != undefined && _root.volume == undefined)
        {
            config.volume = playerSO.data.volume;
        } // end if
        if (playerSO.data.usecaptions != undefined && _root.usecaptions == undefined)
        {
            config.usecaptions = playerSO.data.usecaptions;
        } // end if
        if (playerSO.data.useaudio != undefined && _root.useaudio == undefined)
        {
            config.useaudio = playerSO.data.useaudio;
        } // end if
    } // End of the function
    function startMCV(mar)
    {
        registeredModels = mar;
        if (feeder.feed[currentItem - 1].category == "preroll")
        {
            --currentItem;
        } // end if
        this.sendChange("item", currentItem);
        if (config.autostart == "muted")
        {
            this.sendChange("volume", 0);
        }
        else
        {
            this.sendChange("volume", config.volume);
        } // end else if
        if (config.usecaptions == "false")
        {
            config.clip.captions._visible = false;
            config.clip.controlbar.cc.icn._alpha = 40;
        } // end if
        if (config.useaudio == "false")
        {
            config.clip.audio.setStop();
            config.clip.controlbar.au.icn._alpha = 40;
        } // end if
        if (config.autostart == "false")
        {
            this.sendChange("pause", feeder.feed[currentItem].start);
            isPlaying = false;
        }
        else
        {
            this.sendChange("start", feeder.feed[currentItem].start);
            isPlaying = true;
        } // end else if
    } // End of the function
    function setPlaypause(dpl)
    {
        if ((feeder.feed[currentItem].category == "preroll" || feeder.feed[currentItem].category == "postroll") && dpl == 1)
        {
            getURL(feeder.feed[currentItem].link, config.linktarget);
        }
        else if (isPlaying == true)
        {
            isPlaying = false;
            this.sendChange("pause");
        }
        else
        {
            isPlaying = true;
            this.sendChange("start");
        } // end else if
    } // End of the function
    function setPrev()
    {
        if (currentItem == 0)
        {
            var _loc2 = feeder.feed.length - 1;
        }
        else
        {
            _loc2 = currentItem - 1;
        } // end else if
        this.setPlayitem(_loc2);
    } // End of the function
    function setNext()
    {
        if (currentItem == feeder.feed.length - 1)
        {
            var _loc2 = 0;
        }
        else if (feeder.feed[currentItem].category == "preroll")
        {
            _loc2 = currentItem + 2;
        }
        else
        {
            _loc2 = currentItem + 1;
        } // end else if
        this.setPlayitem(_loc2);
    } // End of the function
    function setStop()
    {
        this.sendChange("pause", 0);
        this.sendChange("stop");
        this.sendChange("item", currentItem);
        isPlaying = false;
    } // End of the function
    function setScrub(prm)
    {
        if (feeder.feed[currentItem].category == "preroll" || feeder.feed[currentItem].category == "postroll")
        {
            return;
        }
        else if (isPlaying == true)
        {
            this.sendChange("start", prm);
        }
        else
        {
            this.sendChange("pause", prm);
        } // end else if
    } // End of the function
    function setPlayitem(itm)
    {
        if (feeder.feed[itm - 1].category == "preroll" && currentItem != itm - 1)
        {
            --itm;
        }
        else if (feeder.feed[itm].category == "postroll" && currentItem == itm + 1)
        {
            --itm;
        } // end else if
        if (itm != currentItem)
        {
            itm > feeder.feed.length - 1 ? (itm = feeder.feed.length - 1, feeder.feed.length - 1) : (null);
            if (feeder.feed[currentItem].file != feeder.feed[itm].file)
            {
                this.sendChange("stop");
            } // end if
            currentItem = itm;
            this.sendChange("item", itm);
        } // end if
        this.sendChange("start", feeder.feed[itm].start);
        currentURL = feeder.feed[itm].file;
        isPlaying = true;
    } // End of the function
    function setGetlink(idx)
    {
        if (feeder.feed[idx].link == undefined)
        {
            this.setPlaypause();
        }
        else
        {
            getURL(feeder.feed[idx].link, config.linktarget);
        } // end else if
    } // End of the function
    function setComplete()
    {
        ++itemsPlayed;
        if (feeder.feed[currentItem].type == "rtmp" || config.streamscript != undefined)
        {
            this.sendChange("stop");
        } // end if
        if (feeder.feed[currentItem].category == "preroll" || feeder.feed[currentItem + 1].category == "postroll")
        {
            this.setPlayitem(currentItem + 1);
        }
        else if (config.repeat == "false" || config.repeat == "list" && itemsPlayed == feeder.feed.length)
        {
            this.sendChange("pause", 0);
            isPlaying = false;
            itemsPlayed = 0;
            if (feeder.feed[currentItem].category == "postroll")
            {
                --currentItem;
                this.sendChange("stop");
                this.sendChange("item", currentItem);
            } // end if
        }
        else
        {
            if (config.shuffle == "true")
            {
                var _loc2 = randomizer.pick();
            }
            else if (currentItem == feeder.feed.length - 1)
            {
                _loc2 = 0;
            }
            else
            {
                _loc2 = currentItem + 1;
            } // end else if
            this.setPlayitem(_loc2);
        } // end else if
    } // End of the function
    function setVolume(prm)
    {
        if (prm < 0)
        {
            prm = 0;
        }
        else if (prm > 100)
        {
            prm = 100;
        } // end else if
        if (config.volume == 0 && prm == 0)
        {
            prm = 80;
        } // end if
        config.volume = prm;
        this.sendChange("volume", prm);
        playerSO.data.volume = prm;
        playerSO.flush();
    } // End of the function
    function setFullscreen()
    {
        if (Stage.displayState == "normal" && config.usefullscreen == "true")
        {
            Stage.displayState = "fullScreen";
        }
        else if (Stage.displayState == "fullScreen" && config.usefullscreen == "true")
        {
            Stage.displayState = "normal";
        }
        else if (config.fsbuttonlink != undefined)
        {
            this.sendChange("stop");
            getURL(config.fsbuttonlink, config.linktarget);
        } // end else if
    } // End of the function
    function setCaptions()
    {
        if (config.usecaptions == "true")
        {
            config.usecaptions = "false";
            config.clip.captions._visible = false;
            config.clip.controlbar.cc.icn._alpha = 40;
        }
        else
        {
            config.usecaptions = "true";
            config.clip.captions._visible = true;
            config.clip.controlbar.cc.icn._alpha = 100;
        } // end else if
        playerSO.data.usecaptions = config.usecaptions;
        playerSO.flush();
    } // End of the function
    function setAudio()
    {
        if (config.useaudio == "true")
        {
            config.useaudio = "false";
            config.clip.audio.setStop();
            config.clip.controlbar.au.icn._alpha = 40;
        }
        else
        {
            config.useaudio = "true";
            config.clip.audio.setStart();
            config.clip.controlbar.au.icn._alpha = 100;
        } // end else if
        playerSO.data.useaudio = config.useaudio;
        playerSO.flush();
    } // End of the function
} // End of Class
