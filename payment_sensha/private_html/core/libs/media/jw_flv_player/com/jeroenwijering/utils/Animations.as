class com.jeroenwijering.utils.Animations
{
    var _alpha, onEnterFrame, _visible, removeMovieClip, _x, _y, i, rnd, tf, str, hstr, more;
    function Animations()
    {
    } // End of the function
    static function fadeIn(tgt, end, spd)
    {
        arguments.length < 3 ? (spd = 20) : (null);
        arguments.length < 2 ? (end = 100) : (null);
        tgt._visible = true;
        tgt.onEnterFrame = function ()
        {
            if (_alpha > end - spd)
            {
                delete this.onEnterFrame;
                _alpha = end;
            }
            else
            {
                _alpha = _alpha + spd;
            } // end else if
        };
    } // End of the function
    static function fadeOut(tgt, end, spd, rmv)
    {
        arguments.length < 4 ? (rmv = false) : (null);
        arguments.length < 3 ? (spd = 20) : (null);
        arguments.length < 2 ? (end = 0) : (null);
        tgt.onEnterFrame = function ()
        {
            if (_alpha < end + spd)
            {
                delete this.onEnterFrame;
                _alpha = end;
                end == 0 ? (_visible = false) : (null);
                rmv == true ? (this.removeMovieClip()) : (null);
            }
            else
            {
                _alpha = _alpha - spd;
            } // end else if
        };
    } // End of the function
    static function crossfade(tgt, alp)
    {
        var phs = "out";
        var pct = alp / 5;
        tgt.onEnterFrame = function ()
        {
            if (phs == "out")
            {
                _alpha = _alpha - pct;
                if (_alpha < 1)
                {
                    phs = "in";
                } // end if
            }
            else
            {
                _alpha = _alpha + pct;
                _alpha >= alp ? (delete this.onEnterFrame) : (null);
            } // end else if
        };
    } // End of the function
    static function easeTo(tgt, xps, yps, spd)
    {
        arguments.length < 4 ? (spd = 2) : (null);
        tgt.onEnterFrame = function ()
        {
            _x = xps - (xps - _x) / (1 + 1 / spd);
            _y = yps - (yps - _y) / (1 + 1 / spd);
            if (_x > xps - 1 && _x < xps + 1 && _y > yps - 1 && _y < yps + 1)
            {
                _x = Math.round(xps);
                _y = Math.round(yps);
                delete this.onEnterFrame;
            } // end if
        };
    } // End of the function
    static function easeText(tgt, rnd, txt, spd)
    {
        if (arguments.length < 3)
        {
            tgt.str = tgt.tf.text;
            tgt.hstr = tgt.tf.htmlText;
        }
        else
        {
            tgt.str = tgt.hstr = txt;
        } // end else if
        if (arguments.length < 4)
        {
            spd = 1.400000E+000;
        } // end if
        tgt.tf.text = "";
        tgt.i = 0;
        tgt.rnd = rnd;
        tgt.onEnterFrame = function ()
        {
            if (i > this.rnd)
            {
                tf.text = str.substr(0, str.length - Math.floor((str.length - tf.text.length) / spd));
            } // end if
            if (tf.text == str)
            {
                tf.htmlText = hstr;
                if (more != undefined)
                {
                    more._visible = true;
                } // end if
                delete this.onEnterFrame;
            } // end if
            ++i;
        };
    } // End of the function
} // End of Class
