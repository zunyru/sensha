class com.jeroenwijering.feeds.RSSParser extends com.jeroenwijering.feeds.AbstractParser
{
    var elements, rfc2Date, iso2Date, mimetypes;
    function RSSParser()
    {
        super();
    } // End of the function
    function setElements()
    {
        elements = new Object();
        elements.title = "title";
        elements.guid = "id";
        elements.author = "author";
        elements.category = "category";
        elements.link = "link";
        elements.geo:lat = "latitude";
        elements.geo:long = "longitude";
        elements.geo:city = "city";
    } // End of the function
    function parse(xml)
    {
        var _loc12 = new Array();
        for (var _loc6 = xml.firstChild.firstChild.firstChild; _loc6 != null; _loc6 = _loc6.nextSibling)
        {
            if (_loc6.nodeName.toLowerCase() == "item")
            {
                var _loc2 = new Object();
                for (var _loc10 = 0; _loc10 < _loc6.childNodes.length; ++_loc10)
                {
                    var _loc3 = _loc6.childNodes[_loc10];
                    var _loc4 = _loc3.nodeName.toLowerCase();
                    if (elements[_loc4] != undefined)
                    {
                        _loc2[elements[_loc4]] = _loc3.firstChild.nodeValue;
                        continue;
                    } // end if
                    if (_loc4 == "description")
                    {
                        _loc2.description = com.jeroenwijering.utils.StringMagic.stripTagsBreaks(_loc3.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "pubdate")
                    {
                        _loc2.date = this.rfc2Date(_loc3.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "dc:date")
                    {
                        _loc2.date = this.iso2Date(_loc3.firstChild.nodeValue);
                        continue;
                    } // end if
                    if (_loc4 == "media:thumbnail")
                    {
                        _loc2.image = _loc3.attributes.url;
                        continue;
                    } // end if
                    if (_loc4 == "itunes:image")
                    {
                        _loc2.image = _loc3.attributes.href;
                        continue;
                    } // end if
                    if (_loc4 == "geo")
                    {
                        _loc2.latitude = _loc3.attributes.latitude;
                        _loc2.longitude = _loc3.attributes.longitude;
                        _loc2.city = _loc3.attributes.city;
                        continue;
                    } // end if
                    if (_loc4 == "georss:point")
                    {
                        var _loc11 = _loc3.firstChild.nodeValue.split(" ");
                        _loc2.latitude = Number(_loc11[0]);
                        _loc2.longitude = Number(_loc11[1]);
                        continue;
                    } // end if
                    if (_loc4 == "enclosure" || _loc4 == "media:content")
                    {
                        var _loc7 = _loc3.attributes.type.toLowerCase();
                        if (mimetypes[_loc7] != undefined && _loc2.type != "flv")
                        {
                            _loc2.type = mimetypes[_loc7];
                            _loc2.file = _loc3.attributes.url;
                            if (_loc2.file.substr(0, 4) == "rtmp")
                            {
                                _loc2.type = "rtmp";
                            } // end if
                            if (_loc3.childNodes[0].nodeName == "media:thumbnail")
                            {
                                _loc2.image = _loc3.childNodes[0].attributes.url;
                            } // end if
                        }
                        else if (_loc7 == "captions")
                        {
                            _loc2.captions = _loc3.attributes.url;
                        }
                        else if (_loc7 == "audio")
                        {
                            _loc2.audio = _loc3.attributes.url;
                        } // end else if
                        continue;
                    } // end if
                    if (_loc4 == "media:group")
                    {
                        for (var _loc5 = 0; _loc5 < _loc3.childNodes.length; ++_loc5)
                        {
                            var _loc8 = _loc3.childNodes[_loc5].nodeName.toLowerCase();
                            if (_loc8 == "media:content")
                            {
                                var _loc9 = _loc3.childNodes[_loc5].attributes.type.toLowerCase();
                                if (mimetypes[_loc9] != undefined && _loc2.type != "flv")
                                {
                                    _loc2.file = _loc3.childNodes[_loc5].attributes.url;
                                    _loc2.type = mimetypes[_loc9];
                                    if (_loc2.file.substr(0, 4) == "rtmp")
                                    {
                                        _loc2.type = "rtmp";
                                    } // end if
                                } // end if
                            } // end if
                            if (_loc8 == "media:thumbnail")
                            {
                                _loc2.image = _loc3.childNodes[_loc5].attributes.url;
                            } // end if
                        } // end of for
                    } // end if
                } // end of for
                if (_loc2.latitude == undefined && lat != undefined)
                {
                    _loc2.latitude = lat;
                    _loc2.longitude = lng;
                } // end if
                if (_loc2.image == undefined)
                {
                    if (_loc2.file.indexOf(".jpg") > 0 || _loc2.file.indexOf(".png") > 0 || _loc2.file.indexOf(".gif") > 0)
                    {
                        _loc2.image = _loc2.file;
                    }
                    else if (img != undefined)
                    {
                        _loc2.image = img;
                    } // end if
                } // end else if
                if (_loc2.author == undefined)
                {
                    _loc2.author = ttl;
                } // end if
                _loc12.push(_loc2);
                continue;
            } // end if
            if (_loc6.nodeName == "title")
            {
                var ttl = _loc6.firstChild.nodeValue;
                continue;
            } // end if
            if (_loc6.nodeName == "geo:lat")
            {
                var lat = _loc6.firstChild.nodeValue;
                continue;
            } // end if
            if (_loc6.nodeName == "geo:long")
            {
                var lng = _loc6.firstChild.nodeValue;
                continue;
            } // end if
            if (_loc6.nodeName == "itunes:image")
            {
                var img = _loc6.attributes.href;
            } // end if
        } // end of for
        return (_loc12);
    } // End of the function
} // End of Class
