class com.jeroenwijering.players.DisplayView extends com.jeroenwijering.players.AbstractView
{
    var config, itemSize, thumbSize, imageLoader, targetClip, startPos, feeder, currentItem;
    function DisplayView(ctr, cfg, fed)
    {
        super(ctr, cfg, fed);
        Stage.addListener(this);
        itemSize = new Array(config.displaywidth, config.displayheight);
        thumbSize = new Array(config.displaywidth, config.displayheight);
        var ref = this;
        var tgt = config.clip;
        imageLoader = new com.jeroenwijering.utils.ImageLoader(tgt.display.thumb);
        imageLoader.onLoadFinished = function ()
        {
            ref.thumbSize = new Array(targetClip._width, targetClip._height);
            ref.scaleClip(tgt.display.thumb, targetClip._width, targetClip._height);
        };
        startPos = new Array(tgt._x, tgt._y);
        this.setColorsClicks();
        this.setDimensions();
    } // End of the function
    function setColorsClicks()
    {
        var ref = this;
        var tgt = config.clip.back;
        tgt.col = new Color(tgt);
        tgt.col.setRGB(config.backcolor);
        var tgt = config.clip.display;
        tgt.col = new Color(tgt.back);
        tgt.col.setRGB(config.screencolor);
        tgt.setMask(config.clip.mask);
        if (config.showicons == "false")
        {
            tgt.playicon._visible = false;
            tgt.muteicon._visible = false;
        } // end if
        tgt.activity._visible = false;
        tgt.back.tabEnabled = false;
        if (config.autostart == "muted")
        {
            tgt.back.onRelease = function ()
            {
                ref.sendEvent("volume", 80);
                ref.firstClick();
            };
        }
        else if (config.autostart == "false")
        {
            tgt.muteicon._visible = false;
            tgt.back.onRelease = function ()
            {
                ref.sendEvent("playpause");
                ref.firstClick();
            };
        }
        else
        {
            ref.firstClick();
        } // end else if
        if (config.logo != "undefined")
        {
            var _loc2 = new com.jeroenwijering.utils.ImageLoader(tgt.logo, "none");
            _loc2.onLoadFinished = function ()
            {
                tgt.logo._x = ref.config.displaywidth - tgt.logo._width - 10;
                tgt.logo._y = 10;
            };
            _loc2.loadImage(config.logo);
            tgt.logo.onRelease = function ()
            {
                ref.sendEvent("getlink", ref.currentItem);
            };
        } // end if
    } // End of the function
    function setDimensions()
    {
        var _loc2 = config.clip.back;
        if (Stage.displayState == "fullScreen")
        {
            config.clip._x = config.clip._y = 0;
            _loc2._width = Stage.width;
            _loc2._height = Stage.height;
        }
        else
        {
            config.clip._x = startPos[0];
            config.clip._y = startPos[1];
            _loc2._width = config.width;
            _loc2._height = config.height;
            if (config.displayheight >= config.height - config.controlbar && config.displaywidth == config.width)
            {
                --_loc2._height;
            } // end if
        } // end else if
        _loc2 = config.clip.display;
        this.scaleClip(_loc2.thumb, thumbSize[0], thumbSize[1]);
        this.scaleClip(_loc2.image, itemSize[0], itemSize[1]);
        this.scaleClip(_loc2.video, itemSize[0], itemSize[1]);
        if (Stage.displayState == "fullScreen")
        {
            config.clip.mask._width = _loc2.back._width = Stage.width;
            config.clip.mask._height = _loc2.back._height = Stage.height;
        }
        else
        {
            config.clip.mask._width = _loc2.back._width = config.displaywidth;
            config.clip.mask._height = _loc2.back._height = config.displayheight;
        } // end else if
        _loc2.playicon._x = _loc2.activity._x = _loc2.muteicon._x = Math.round(_loc2.back._width / 2);
        _loc2.playicon._y = _loc2.activity._y = _loc2.muteicon._y = Math.round(_loc2.back._height / 2);
        if (Stage.displayState == "fullScreen")
        {
            _loc2.playicon._xscale = _loc2.playicon._yscale = _loc2.muteicon._xscale = _loc2.muteicon._yscale = _loc2.activity._xscale = _loc2.activity._yscale = _loc2.logo._xscale = _loc2.logo._yscale = 200;
            _loc2.logo._x = Stage.width - _loc2.logo._width - 20;
            _loc2.logo._y = 20;
        }
        else
        {
            _loc2.playicon._xscale = _loc2.playicon._yscale = _loc2.muteicon._xscale = _loc2.muteicon._yscale = _loc2.activity._xscale = _loc2.activity._yscale = _loc2.logo._xscale = _loc2.logo._yscale = 100;
            if (_loc2.logo._height > 1)
            {
                _loc2.logo._x = config.displaywidth - _loc2.logo._width - 10;
                _loc2.logo._y = 10;
            } // end if
        } // end else if
    } // End of the function
    function setState(stt)
    {
        var _loc2 = config.clip.display;
        switch (stt)
        {
            case 0:
            {
                if (config.linkfromdisplay == "false" && config.showicons == "true" && feeder.feed[currentItem].category != "preroll" && feeder.feed[currentItem].category != "postroll")
                {
                    _loc2.playicon._visible = true;
                } // end if
                _loc2.activity._visible = false;
                break;
            } 
            case 1:
            {
                _loc2.playicon._visible = false;
                if (config.showicons == "true")
                {
                    _loc2.activity._visible = true;
                } // end if
                break;
            } 
            case 2:
            {
                _loc2.playicon._visible = false;
                _loc2.activity._visible = false;
                break;
            } 
        } // End of switch
    } // End of the function
    function setSize(wid, hei)
    {
        itemSize = new Array(wid, hei);
        var _loc2 = config.clip.display;
        this.scaleClip(_loc2.image, itemSize[0], itemSize[1]);
        this.scaleClip(_loc2.video, itemSize[0], itemSize[1]);
    } // End of the function
    function scaleClip(tgt, wid, hei)
    {
        var _loc9 = tgt.mc._currentframe;
        tgt.mc.gotoAndStop(1);
        if (Stage.displayState == "fullScreen")
        {
            var _loc6 = Stage.width;
            var _loc5 = Stage.height;
        }
        else
        {
            _loc6 = config.displaywidth;
            _loc5 = config.displayheight;
        } // end else if
        var _loc3 = _loc6 / wid;
        var _loc4 = _loc5 / hei;
        if (_loc3 < _loc4 && config.overstretch == "false" || _loc4 < _loc3 && config.overstretch == "true")
        {
            tgt._width = wid * _loc3;
            tgt._height = hei * _loc3;
        }
        else if (config.overstretch == "none")
        {
            tgt._width = wid;
            tgt._height = hei;
        }
        else if (config.overstretch == "fit")
        {
            tgt._width = _loc6;
            tgt._height = _loc5;
        }
        else
        {
            tgt._width = wid * _loc4;
            tgt._height = hei * _loc4;
        } // end else if
        tgt._x = _loc6 / 2 - tgt._width / 2;
        tgt._y = _loc5 / 2 - tgt._height / 2;
        tgt.mc.gotoAndPlay(_loc9);
    } // End of the function
    function setItem(idx)
    {
        currentItem = idx;
        var _loc2 = config.clip.display;
        if (feeder.feed[idx].image == "undefined")
        {
            _loc2.thumb.clear();
            _loc2.thumb._visible = false;
        }
        else
        {
            imageLoader.loadImage(feeder.feed[idx].image);
            _loc2.thumb._visible = true;
        } // end else if
    } // End of the function
    function onResize()
    {
        if (_root.displayheight > config.height + 10)
        {
            config.height = config.displayheight = Stage.height;
            config.width = config.displaywidth = Stage.width;
        } // end if
        this.setDimensions();
    } // End of the function
    function onFullScreen(fs)
    {
        if (fs == false)
        {
            this.setDimensions();
        } // end if
    } // End of the function
    function firstClick()
    {
        var ref = this;
        var _loc2 = config.clip.display;
        _loc2.playicon._visible = false;
        _loc2.muteicon._visible = false;
        if (config.linkfromdisplay == "true")
        {
            _loc2.back.onRelease = function ()
            {
                ref.sendEvent("getlink", ref.currentItem);
            };
        }
        else
        {
            _loc2.back.onRelease = function ()
            {
                ref.sendEvent("playpause", 1);
            };
        } // end else if
    } // End of the function
} // End of Class
