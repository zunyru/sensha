class com.jeroenwijering.players.AbstractModel
{
    var registeredViews, controller, config, feeder, isActive, currentItem, mediatypes, currentPosition;
    function AbstractModel(vws, ctr, cfg, fed)
    {
        registeredViews = vws;
        controller = ctr;
        config = cfg;
        feeder = fed;
    } // End of the function
    function getChange(typ, prm)
    {
        trace ("model: " + typ + ": " + prm);
        switch (typ)
        {
            case "item":
            {
                this.setItem(prm);
                break;
            } 
            case "start":
            {
                isActive == true ? (this.setStart(prm)) : (null);
                break;
            } 
            case "pause":
            {
                isActive == true ? (this.setPause(prm)) : (null);
                break;
            } 
            case "stop":
            {
                isActive == true ? (this.setStop()) : (null);
                break;
            } 
            case "volume":
            {
                this.setVolume(prm);
                break;
            } 
            default:
            {
                trace ("Model: incompatible change received");
                break;
            } 
        } // End of switch
    } // End of the function
    function setItem(idx)
    {
        currentItem = idx;
        var _loc4 = false;
        for (var _loc2 = 0; _loc2 < mediatypes.length; ++_loc2)
        {
            if (feeder.feed[idx].type == mediatypes[_loc2])
            {
                _loc4 = true;
            } // end if
        } // end of for
        if (feeder.feed[idx].start > 0)
        {
            currentPosition = feeder.feed[idx].start;
        } // end if
        if (_loc4 == true)
        {
            isActive = true;
            this.sendUpdate("item", idx);
        }
        else
        {
            isActive = false;
        } // end else if
    } // End of the function
    function setStart(prm)
    {
    } // End of the function
    function setPause(prm)
    {
    } // End of the function
    function setStop()
    {
    } // End of the function
    function setVolume(vol)
    {
        isActive == true ? (this.sendUpdate("volume", vol)) : (null);
    } // End of the function
    function sendUpdate(typ, prm, pr2)
    {
        for (var _loc2 = 0; _loc2 < registeredViews.length; ++_loc2)
        {
            registeredViews[_loc2].getUpdate(typ, prm, pr2);
        } // end of for
    } // End of the function
    function sendCompleteEvent()
    {
        controller.getEvent("complete");
    } // End of the function
} // End of Class
