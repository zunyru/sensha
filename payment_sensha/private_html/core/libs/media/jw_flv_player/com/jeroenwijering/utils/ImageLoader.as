class com.jeroenwijering.utils.ImageLoader
{
    var targetClip, targetWidth, targetHeight, mcLoader, useSmoothing, sourceURL, sourceWidth, sourceHeight, sourceLength, metaInt;
    function ImageLoader(tgt, ost, wid, hei)
    {
        targetClip = tgt;
        arguments.length > 1 ? (overStretch = String(ost)) : (null);
        if (arguments.length > 2)
        {
            targetWidth = wid;
            targetHeight = hei;
        } // end if
        mcLoader = new MovieClipLoader();
        mcLoader.addListener(this);
    } // End of the function
    function onLoadInit(inTarget)
    {
        if (useSmoothing == true)
        {
            var _loc3 = new flash.display.BitmapData(targetClip.mc._width, targetClip.mc._height, true, 0);
            _loc3.draw(targetClip.mc);
            var _loc2 = targetClip.createEmptyMovieClip("smc", targetClip.getNextHighestDepth());
            _loc2.attachBitmap(_loc3, _loc2.getNextHighestDepth(), "auto", true);
            targetClip.mc.unloadMovie();
            targetClip.mc.removeMovieClip();
            delete targetClip.mc;
            this.scaleImage(targetClip.smc);
            this.onLoadFinished();
        }
        else
        {
            if (sourceURL.toLowerCase().indexOf(".swf") == -1)
            {
                this.scaleImage(targetClip.mc);
            } // end if
            this.onLoadFinished();
        } // end else if
    } // End of the function
    function scaleImage(tgt)
    {
        targetClip._xscale = targetClip._yscale = 100;
        var _loc5 = tgt._currentframe;
        tgt.gotoAndStop(1);
        sourceWidth = tgt._width;
        sourceHeight = tgt._height;
        sourceLength = tgt._totalframes / 20;
        var _loc3 = targetWidth / sourceWidth;
        var _loc4 = targetHeight / sourceHeight;
        if (overStretch == "fit" || Math.abs(_loc3 - _loc4) < 1.000000E-001)
        {
            tgt._width = targetWidth;
            tgt._height = targetHeight;
        }
        else if (overStretch == "true" && _loc3 > _loc4 || overStretch == "false" && _loc3 < _loc4)
        {
            tgt._xscale = tgt._yscale = _loc3 * 100;
        }
        else if (overStretch == "none")
        {
            tgt._xscale = tgt._yscale = 100;
        }
        else
        {
            tgt._xscale = tgt._yscale = _loc4 * 100;
        } // end else if
        if (targetWidth != undefined)
        {
            tgt._x = targetWidth / 2 - tgt._width / 2;
            tgt._y = targetHeight / 2 - tgt._height / 2;
        } // end if
        tgt.gotoAndPlay(_loc5);
        this.onMetaData();
    } // End of the function
    function loadImage(img)
    {
        sourceURL = img;
        targetClip.mc.clear();
        targetClip.smc.unloadMovie();
        targetClip.smc.removeMovieClip();
        delete targetClip.smc;
        this.checkSmoothing(img);
        var _loc3 = targetClip.createEmptyMovieClip("mc", 1);
        mcLoader.loadClip(img, _loc3);
        if (img.toLowerCase().indexOf(".swf") > -1)
        {
            metaInt = setInterval(this, "setSWFMeta", 200);
        } // end if
    } // End of the function
    function checkSmoothing(img)
    {
        var _loc3 = _root._url.indexOf("/", 8);
        var _loc4 = _root._url.substring(0, _loc3);
        if (System.capabilities.version.indexOf("7,0,") > -1 || img.toLowerCase().indexOf(".swf") > -1 || _root._url.indexOf("file://") > -1 || img.indexOf(_loc4) == -1)
        {
            useSmoothing = false;
        }
        else
        {
            useSmoothing = true;
        } // end else if
    } // End of the function
    function setSWFMeta()
    {
        if (targetClip.mc._currentframe > 0)
        {
            clearInterval(metaInt);
            this.scaleImage(targetClip.mc);
        } // end if
    } // End of the function
    function onLoadProgress(tgt, btl, btt)
    {
    } // End of the function
    function onLoadFinished()
    {
    } // End of the function
    function onMetaData()
    {
    } // End of the function
    var overStretch = "none";
} // End of Class
