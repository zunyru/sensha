class com.jeroenwijering.feeds.XSPFParser extends com.jeroenwijering.feeds.AbstractParser
{
    var elements, mimetypes;
    function XSPFParser()
    {
        super();
    } // End of the function
    function setElements()
    {
        elements = new Object();
        elements.title = "title";
        elements.creator = "author";
        elements.info = "link";
        elements.image = "image";
        elements.identifier = "id";
        elements.album = "category";
    } // End of the function
    function parse(xml)
    {
        var _loc9 = new Array();
        for (var _loc7 = xml.firstChild.firstChild; _loc7 != null; _loc7 = _loc7.nextSibling)
        {
            if (_loc7.nodeName == "trackList")
            {
                for (var _loc6 = 0; _loc6 < _loc7.childNodes.length; ++_loc6)
                {
                    var _loc3 = new Object();
                    for (var _loc5 = 0; _loc5 < _loc7.childNodes[_loc6].childNodes.length; ++_loc5)
                    {
                        var _loc2 = _loc7.childNodes[_loc6].childNodes[_loc5];
                        var _loc4 = _loc2.nodeName.toLowerCase();
                        if (elements[_loc4] != undefined)
                        {
                            _loc3[elements[_loc4]] = _loc2.firstChild.nodeValue;
                            continue;
                        } // end if
                        if (_loc4 == "location" && _loc3.type != "flv")
                        {
                            _loc3.file = _loc2.firstChild.nodeValue;
                            var _loc8 = _loc3.file.substr(-3).toLowerCase();
                            if (_loc3.file.substr(0, 4) == "rtmp")
                            {
                                _loc3.type = "rtmp";
                            }
                            else if (mimetypes[_loc8] != undefined)
                            {
                                _loc3.type = mimetypes[_loc8];
                            } // end else if
                            continue;
                        } // end if
                        if (_loc4 == "annotation")
                        {
                            _loc3.description = com.jeroenwijering.utils.StringMagic.stripTagsBreaks(_loc2.firstChild.nodeValue);
                            continue;
                        } // end if
                        if (_loc4 == "link" && _loc2.attributes.rel == "captions")
                        {
                            _loc3.captions = _loc2.firstChild.nodeValue;
                            continue;
                        } // end if
                        if (_loc4 == "link" && _loc2.attributes.rel == "audio")
                        {
                            _loc3.audio = _loc2.firstChild.nodeValue;
                            continue;
                        } // end if
                        if (_loc4 == "meta" && _loc2.attributes.rel == "start")
                        {
                            _loc3.start = _loc2.firstChild.nodeValue;
                            continue;
                        } // end if
                        if (_loc4 == "meta" && _loc2.attributes.rel == "type")
                        {
                            _loc3.type = _loc2.firstChild.nodeValue;
                        } // end if
                    } // end of for
                    _loc9.push(_loc3);
                } // end of for
            } // end if
        } // end of for
        return (_loc9);
    } // End of the function
} // End of Class
