class com.jeroenwijering.players.MediaPlayer extends com.jeroenwijering.players.AbstractPlayer
{
    var loadFile, feeder, controller;
    function MediaPlayer(tgt)
    {
        super(tgt);
    } // End of the function
    function loadConfig(tgt)
    {
        for (var _loc3 in config)
        {
            if (_root[_loc3] != undefined)
            {
                config[_loc3] = unescape(_root[_loc3]);
            } // end if
        } // end of for...in
        config.largecontrols == "true" ? (config.controlbar = config.controlbar * 2) : (null);
        if (config.displayheight == undefined)
        {
            config.displayheight = config.height - config.controlbar;
        }
        else if (Number(config.displayheight) > Number(config.height))
        {
            config.displayheight = config.height;
        } // end else if
        if (config.displaywidth == undefined)
        {
            config.displaywidth = config.width;
        } // end if
        config.bwstreams == undefined ? (this.loadFile()) : (this.checkStream());
    } // End of the function
    function checkStream()
    {
        var ref = this;
        var str = config.bwstreams.split(",");
        var _loc4 = new com.jeroenwijering.utils.BandwidthCheck(config.bwfile);
        _loc4.onComplete = function (kbps)
        {
            trace ("bandwidth: " + kbps);
            var _loc4 = new ContextMenuItem("Detected bandwidth: " + kbps + " kbps", _root.goTo);
            _loc4.separatorBefore = true;
            _root.mnu.customItems.push(_loc4);
            if (ref.config.enablejs == "true" && flash.external.ExternalInterface.available)
            {
                flash.external.ExternalInterface.call("getBandwidth", kbps);
            } // end if
            for (var _loc2 = 1; _loc2 < str.length; ++_loc2)
            {
                if (kbps < Number(str[_loc2]))
                {
                    ref.loadFile(str[_loc2 - 1]);
                    return;
                } // end if
            } // end of for
            ref.loadFile(str[str.length - 1]);
        };
    } // End of the function
    function setupMCV()
    {
        controller = new com.jeroenwijering.players.PlayerController(config, feeder);
        var _loc7 = new com.jeroenwijering.players.DisplayView(controller, config, feeder);
        var _loc16 = new com.jeroenwijering.players.ControlbarView(controller, config, feeder);
        var _loc2 = new Array(_loc7, _loc16);
        if (config.displayheight < config.height - config.controlbar || config.displaywidth < config.width)
        {
            var _loc9 = new com.jeroenwijering.players.PlaylistView(controller, config, feeder);
            _loc2.push(_loc9);
        }
        else
        {
            config.clip.playlist._visible = config.clip.playlistmask._visible = false;
        } // end else if
        if (config.usekeys == "true")
        {
            var _loc11 = new com.jeroenwijering.players.InputView(controller, config, feeder);
            _loc2.push(_loc11);
        } // end if
        if (config.showeq == "true")
        {
            var _loc8 = new com.jeroenwijering.players.EqualizerView(controller, config, feeder);
            _loc2.push(_loc8);
        }
        else
        {
            config.clip.equalizer._visible = false;
        } // end else if
        if (feeder.captions == true)
        {
            var _loc3 = new com.jeroenwijering.players.CaptionsView(controller, config, feeder);
            _loc2.push(_loc3);
        }
        else
        {
            config.clip.captions._visible = false;
        } // end else if
        if (feeder.audio == true)
        {
            var _loc14 = new com.jeroenwijering.players.AudioView(controller, config, feeder, true);
            _loc2.push(_loc14);
        } // end if
        if (config.enablejs == "true")
        {
            var _loc10 = new com.jeroenwijering.players.JavascriptView(controller, config, feeder);
            _loc2.push(_loc10);
        } // end if
        if (config.callback != undefined)
        {
            var _loc5 = new com.jeroenwijering.players.CallbackView(controller, config, feeder);
            _loc2.push(_loc5);
        } // end if
        if (feeder.overlays == true)
        {
            var _loc6 = new com.jeroenwijering.players.OverlayView(controller, config, feeder);
            _loc2.push(_loc6);
        }
        else
        {
            config.clip.overlay._visible = false;
        } // end else if
        var _loc15 = new com.jeroenwijering.players.MP3Model(_loc2, controller, config, feeder, config.clip);
        var _loc4 = new com.jeroenwijering.players.FLVModel(_loc2, controller, config, feeder, config.clip.display.video);
        var _loc12 = new com.jeroenwijering.players.ImageModel(_loc2, controller, config, feeder, config.clip.display.image);
        var _loc13 = new Array(_loc15, _loc4, _loc12);
        if (feeder.captions == true)
        {
            _loc4.capView = _loc3;
        } // end if
        controller.startMCV(_loc13);
    } // End of the function
    var config = {clip: undefined, controlbar: 20, height: undefined, width: undefined, file: "playlist.xml", displayheight: undefined, frontcolor: 0, backcolor: 16777215, lightcolor: 0, screencolor: 0, autoscroll: "false", displaywidth: undefined, largecontrols: "false", logo: undefined, showdigits: "true", showdownload: "false", showeq: "false", showicons: "true", showvolume: "true", thumbsinplaylist: "false", usefullscreen: "true", fsbuttonlink: undefined, autostart: "false", bufferlength: 3, overstretch: "false", repeat: "false", rotatetime: 10, shuffle: "true", smoothing: "true", volume: 80, bwfile: "100k.jpg", bwstreams: undefined, callback: undefined, enablejs: "false", javascriptid: "", linkfromdisplay: "false", linktarget: undefined, streamscript: undefined, useaudio: "true", usecaptions: "true", usekeys: "true", version: "JW Media Player 3.12"};
} // End of Class
