class com.jeroenwijering.players.CaptionsParser
{
    var parseURL, parseArray, parseLV, parseXML, firstChild;
    function CaptionsParser()
    {
    } // End of the function
    function parse(url)
    {
        parseURL = url;
        parseArray = new Array();
        parseURL.indexOf(".srt") == -1 ? (this.parseTT()) : (this.parseSRT());
    } // End of the function
    function parseSRT()
    {
        var ref = this;
        parseLV = new LoadVars();
        parseLV.onLoad = function (scs)
        {
            if (scs)
            {
                var _loc11 = "";
                var _loc5 = -2;
                while (_loc5 < unescape(this).length)
                {
                    var _loc10 = _loc5;
                    _loc5 = unescape(this).indexOf("=&", _loc5 + 2);
                    _loc5 == -1 ? (_loc5 = unescape(this).length, unescape(this).length) : (null);
                    _loc11 = "&" + unescape(this).substring(_loc10 + 2, _loc5) + _loc11;
                } // end while
                var _loc3 = _loc11.split("\r\n\r\n");
                for (var _loc2 = 0; _loc2 < _loc3.length; ++_loc2)
                {
                    var _loc4 = new Object();
                    var _loc6 = _loc3[_loc2].indexOf(":");
                    _loc4.bgn = Number(_loc3[_loc2].substr(_loc6 - 2, 2)) * 3600 + Number(_loc3[_loc2].substr(_loc6 + 1, 2)) * 60 + Number(_loc3[_loc2].substr(_loc6 + 4, 2) + "." + _loc3[_loc2].substr(_loc6 + 7, 2));
                    var _loc7 = _loc3[_loc2].indexOf(":", _loc6 + 6);
                    _loc4.dur = Number(_loc3[_loc2].substr(_loc7 - 2, 2)) * 3600 + Number(_loc3[_loc2].substr(_loc7 + 1, 2)) * 60 + Number(_loc3[_loc2].substr(_loc7 + 4, 2) + "." + _loc3[_loc2].substr(_loc7 + 7, 2)) - _loc4.bgn;
                    var _loc8 = _loc3[_loc2].indexOf("\r\n", _loc7);
                    if (_loc3[_loc2].indexOf("\r\n", _loc8 + 5) > -1)
                    {
                        var _loc9 = _loc3[_loc2].indexOf("\r\n", _loc8 + 5);
                        _loc3[_loc2] = _loc3[_loc2].substr(0, _loc9) + "<br />" + _loc3[_loc2].substr(_loc9 + 2);
                    } // end if
                    _loc4.txt = _loc3[_loc2].substr(_loc8 + 2);
                    if (!isNaN(_loc4.bgn))
                    {
                        ref.parseArray.push(_loc4);
                    } // end if
                    false;
                } // end of for
            }
            else
            {
                parseArray.push({txt: "File not found: " + ref.parseURL, bgn: 1, dur: 5});
            } // end else if
            if (parseArray.length == 0)
            {
                parseArray.push({txt: "Empty file: " + ref.parseURL, bgn: 1, dur: 5});
            } // end if
            delete ref.parseLV;
            ref.onParseComplete();
        };
        if (_root._url.indexOf("file://") > -1)
        {
            parseLV.load(parseURL);
        }
        else if (parseURL.indexOf("?") > -1)
        {
            parseLV.load(parseURL + "&" + random(999));
        }
        else
        {
            parseLV.load(parseURL + "?" + random(999));
        } // end else if
    } // End of the function
    function parseTT()
    {
        var ref = this;
        parseXML = new XML();
        parseXML.ignoreWhite = true;
        parseXML.onLoad = function (scs)
        {
            if (scs)
            {
                if (firstChild.nodeName.toLowerCase() == "tt")
                {
                    var _loc5 = firstChild.childNodes[1];
                    if (_loc5.firstChild.firstChild.attributes.begin == undefined)
                    {
                        for (var _loc3 = 0; _loc3 < _loc5.childNodes.length; ++_loc3)
                        {
                            var _loc2 = new Object();
                            var _loc4 = _loc5.childNodes[_loc3].attributes.begin;
                            _loc2.bgn = Number(_loc4.substr(0, _loc4.length - 1));
                            var _loc7 = _loc5.childNodes[_loc3].attributes.dur;
                            _loc2.dur = Number(_loc7.substr(0, _loc7.length - 1));
                            _loc2.txt = _loc5.childNodes[_loc3].firstChild.firstChild.nodeValue;
                            ref.parseArray.push(_loc2);
                        } // end of for
                    }
                    else
                    {
                        var _loc6 = _loc5.firstChild;
                        for (var _loc3 = 0; _loc3 < _loc6.childNodes.length; ++_loc3)
                        {
                            _loc2 = new Object();
                            _loc4 = _loc6.childNodes[_loc3].attributes.begin;
                            _loc2.bgn = Number(_loc4.substr(_loc4.lastIndexOf(":") + 1)) + 60 * Number(_loc4.substr(_loc4.indexOf(":") + 1, 2));
                            trace (_loc2.bgn);
                            var _loc8 = _loc6.childNodes[_loc3].attributes.end;
                            _loc2.dur = Number(_loc8.substring(_loc8.lastIndexOf(":") + 1)) - _loc2.bgn;
                            _loc2.txt = _loc6.childNodes[_loc3].firstChild.nodeValue;
                            ref.parseArray.push(_loc2);
                        } // end of for
                    } // end if
                } // end else if
            }
            else
            {
                parseArray.push({txt: "File not found: " + ref.parseURL});
            } // end else if
            if (parseArray.length == 0)
            {
                parseArray.push({txt: "Incompatible file: " + ref.parseURL});
            } // end if
            delete ref.parseXML;
            ref.onParseComplete();
        };
        if (_root._url.indexOf("file://") > -1)
        {
            parseXML.load(parseURL);
        }
        else if (parseURL.indexOf("?") > -1)
        {
            parseXML.load(parseURL + "&" + random(999));
        }
        else
        {
            parseXML.load(parseURL + "?" + random(999));
        } // end else if
    } // End of the function
    function onParseComplete()
    {
    } // End of the function
} // End of Class
