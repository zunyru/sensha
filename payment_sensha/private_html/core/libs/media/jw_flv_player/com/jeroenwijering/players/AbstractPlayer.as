class com.jeroenwijering.players.AbstractPlayer implements com.jeroenwijering.feeds.FeedListener
{
    var config, feeder, controller;
    function AbstractPlayer(tgt, vie)
    {
        config.clip = tgt;
        config.clip._visible = false;
        Stage.width > 0 ? (config.width = Stage.width) : (null);
        Stage.height > 0 ? (config.height = Stage.height) : (null);
        this.loadConfig();
    } // End of the function
    function loadConfig()
    {
        for (var _loc3 in config)
        {
            if (_root[_loc3] != undefined)
            {
                config[_loc3] = unescape(_root[_loc3]);
            } // end if
        } // end of for...in
        this.loadFile();
    } // End of the function
    function loadFile(str)
    {
        feeder = new com.jeroenwijering.feeds.FeedManager(true, config.enablejs, _root.prefix, str);
        feeder.addListener(this);
        feeder.loadFile({file: config.file});
    } // End of the function
    function onFeedUpdate()
    {
        if (controller == undefined)
        {
            config.clip._visible = true;
            _root.activity._visible = false;
            this.setupMCV();
        } // end if
    } // End of the function
    function setupMCV()
    {
        controller = new com.jeroenwijering.players.AbstractController(config, feeder);
        var _loc5 = new com.jeroenwijering.players.AbstractView(controller, config, feeder);
        var _loc2 = new Array(_loc5);
        var _loc3 = new com.jeroenwijering.players.AbstractModel(_loc2, controller, config, feeder);
        var _loc4 = new Array(_loc3);
        controller.startMCV(_loc4);
    } // End of the function
} // End of Class
