class com.jeroenwijering.utils.Draw
{
    function Draw()
    {
    } // End of the function
    static function square(tgt, wth, hei, clr, tck, cls)
    {
        tgt.clear();
        if (tck != undefined)
        {
            tgt.lineStyle(tck, cls, 100);
        } // end if
        tgt.beginFill(clr, 100);
        tgt.moveTo(0, 0);
        tgt.lineTo(wth, 0);
        tgt.lineTo(wth, hei);
        tgt.lineTo(0, hei);
        tgt.lineTo(0, 0);
        tgt.endFill();
    } // End of the function
    static function roundedSquare(tgt, wth, hei, rad, clr, tck, cls, xof, yof, alp)
    {
        tgt.clear();
        if (tck > 0)
        {
            tgt.lineStyle(tck, cls, 100);
        } // end if
        if (xof == undefined)
        {
            yof = 0;
            xof = 0;
        } // end if
        if (alp == undefined)
        {
            alp = 100;
        } // end if
        tgt.beginFill(clr, alp);
        tgt.moveTo(rad + xof, yof);
        tgt.lineTo(wth - rad + xof, yof);
        tgt.curveTo(wth + xof, yof, wth + xof, rad + yof);
        tgt.lineTo(wth + xof, hei - rad + yof);
        tgt.curveTo(wth + xof, hei + yof, wth - rad + xof, hei + yof);
        tgt.lineTo(rad + xof, hei + yof);
        tgt.curveTo(xof, hei + yof, xof, hei - rad + yof);
        tgt.lineTo(xof, rad + yof);
        tgt.curveTo(xof, yof, rad + xof, yof);
        tgt.endFill();
    } // End of the function
} // End of Class
