<?php
class Youtube{
	var $feed_url;
	function Youtube(){
	}
	
	function get_info($code){
		$this->feed_url = 'http://gdata.youtube.com/feeds/base/videos?q='.$code.'&client=ytapi-youtube-search&v=2'; 		
		$sxml = simplexml_load_file($this->feed_url);			
		foreach ($sxml->entry as $entry){
			$details = $entry->content;	
			$info["title"] = $entry->title;
			$info["published"] = $entry->published;
		}
		$details_notags = strip_tags($details);
		$texto = explode("From",$details_notags);
		$info["description"] = $texto[0];
		$aux = explode("Views:",$texto[1]);
		$aux2 = explode(" ",$aux[1]);
		$info["views"] = $aux2[0];
		$info['img'] = 'http://i1.ytimg.com/vi/'.$code.'/hqdefault.jpg';
		$info['img_small'] = 'http://i1.ytimg.com/vi/'.$code.'/default.jpg';
		
		return $info;
	}
	
	function player($code, $width, $height){
		return '<object width="'.$width.'" height="'.$height.'"><param name="movie" value="http://www.youtube.com/v/'.$code.'&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/'.$code.'&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$width.'" height="'.$height.'"></embed></object>';	
	}
	
	function get_comment($code){
		$url = "http://gdata.youtube.com/feeds/api/videos/".$code."/comments";
		$comments = simplexml_load_file($url);
		$comment_list = array();
		foreach($comments->entry as $comment){
			 $comment_list[] = $comment->content;
		}
		
		$data['total'] = count($comments->entry);
		$data['data'] = $comment_list;
		
		return $data;
	}
	
	function get_youtube_id($url) {
        $link = parse_url($url,PHP_URL_QUERY);

		/**split the query string into an array**/
		if($link == null) $arr['v'] = $url;
		else  parse_str($link, $arr);
		/** end split the query string into an array**/
		if(! isset($arr['v'])) return false; //fast fail for links with no v attrib - youtube only
	
		$checklink = YOUTUBE_CHECK . $arr['v'];
	
		//** curl the check link ***//
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$checklink);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$result = curl_exec($ch);
		curl_close($ch);
	
		$return = $arr['v'];
		if(trim($result)=="Invalid id") $return = false; //you tube response
	
		return $return; //the stream is a valid youtube id.
	}

	function contentEmbedYoutube($text, $width = '', $height = ''){
	$_width = $width ? $width : 186;
	$_height = $height ? $height : 107;
	
	$search = '%          # Match any youtube URL in the wild.
		(?:https?://)?    # Optional scheme. Either http or https
		(?:www\.)?        # Optional www subdomain
		(?:               # Group host alternatives
		  youtu\.be/      # Either youtu.be,
		| youtube\.com    # or youtube.com
		  (?:             # Group path alternatives
			/embed/       # Either /embed/
		  | /v/           # or /v/
		  | /watch\?v=    # or /watch\?v=
		  )               # End path alternatives.
		)                 # End host alternatives.
		([\w\-]{10,12})   # Allow 10-12 for 11 char youtube id.
		\b                # Anchor end to word boundary.
		%x';
		$replace = '
		<div class="content_youtube">
		<object width="'.$_width.'" height="'.$_height.'">
		<param name="movie" value="http://www.youtube.com/v/$1?fs=1&version=3&autohide=1&showinfo=0"</param>
		<param name="allowFullScreen" value="true"></param>
		<param name="allowScriptAccess" value="always"></param>
		<embed src="http://www.youtube.com/v/$1?fs=1&version=3&autohide=1&showinfo=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowFullScreen="true" width="'.$_width.'" height="'.$_height.'">
		</embed>
		</object>
		</div>
		';
	
	  return preg_replace($search, $replace, $text);
	 }
	 
}

class xYoutube{
	public $url;
	public $id;
	public $code;
	
	public function xYoutube($code){
		$this->code = $code;
		$this->url = "http://www.youtube.com/watch?v=".$code;
	}
	
	public function url2id(){
		$aux = explode("?",$this->url);
		$aux2 = explode("&",$aux[1]);			
		foreach($aux2 as $campo => $valor){
			$aux3 = explode("=",$valor);
			if($aux3[0] == 'v') $video = $aux3[1];
		}
		return $this->id = $video;
	}
	
	public function url2id_(){
		$url = "http://www.youtube.com/watch?v=".$this->code."&feature=channel";
		$aux = explode("?",$url);
		$aux2 = explode("&",$aux[1]);			
		foreach($aux2 as $campo => $valor){
			$aux3 = explode("=",$valor);
			if($aux3[0] == 'v') $video = $aux3[1];
		}
		return $this->id = $video;
	}
	
	public function thumb_url($tamanho=NULL){
		$tamanho = $tamanho == "large"?"hq":"";				
		$this->url2id();
		return 'http://i1.ytimg.com/vi/'.$this->id.'/'.$tamanho.'default.jpg';
	}
	
	public function thumb($tamanho=NULL){
		$tamanho = $tamanho == "large"?"hq":"";
		$this->url2id();	
		return '<img src="http://i1.ytimg.com/vi/'.$this->id.'/'.$tamanho.'default.jpg">';			
	}
	
	public function info(){
		$this->url2id();
		$feedURL = 'http://gdata.youtube.com/feeds/base/videos?q='.$this->id.'&client=ytapi-youtube-search&v=2';    
		$sxml = simplexml_load_file($feedURL);						
		foreach ($sxml->entry as $entry){
			$details = $entry->content;	
			$info["title"] = $entry->title;
		}
		$details_notags = strip_tags($details);
		$texto = explode("From",$details_notags);
		$info["description"] = $texto[0];
		$aux = explode("Views:",$texto[1]);
		$aux2 = explode(" ",$aux[1]);
		$info["views"] = $aux2[0];
		
		$aux = explode("Time:",$texto[1]);
		$aux2 = explode("More",$aux[1]);
		$info["tempo"] = $aux2[0];
		
		$imgs = strip_tags($details,'<img>');
		$aux = explode("<img",$imgs);
		array_shift($aux);
		array_shift($aux);
		$aux2 = explode("gif\">",$aux[4]);
		array_pop($aux);
		$aux3 = $aux2[0].'gif">';
		$aux[] = $aux3;
		$imagens = '';
		foreach($aux as $campo => $valor){
			$imagens .= '<img'.$valor;
		}
		$info["estrelas"] = $imagens;
		return $info;
	}
	
	public function busca($palavra){
		$feedURL = 'http://gdata.youtube.com/feeds/base/videos?q='.$palavra.'&client=ytapi-youtube-search&v=2';    
		$sxml = simplexml_load_file($feedURL);	
		$i=0;
		foreach ($sxml->entry as $entry){
			$details = $entry->content;	
			$info[$i]["titulo"] = $entry->title;	
			$aux = explode($info[$i]["titulo"],$details);			
			$aux2 = explode("<a",$aux[0]);				
			$aux3 = explode('href="',$aux2[1]);
			$aux4 = explode('&',$aux3[1]);
			$info[$i]["link"] = $aux4[0];
			$details_notags = strip_tags($details);
			$texto = explode("From",$details_notags);
			$info[$i]["descricao"] = $texto[0];
			$aux = explode("Views:",$texto[1]);
			$aux2 = explode(" ",$aux[1]);
			$info[$i]["views"] = $aux2[0];
			
			$aux = explode("Time:",$texto[1]);
			$aux2 = explode("More",$aux[1]);
			$info[$i]["tempo"] = $aux2[0];
			
			$imgs = strip_tags($details,'<img>');
			$aux = explode("<img",$imgs);
			array_shift($aux);
			array_shift($aux);
			$aux2 = explode("gif\">",$aux[4]);
			array_pop($aux);
			$aux3 = $aux2[0].'gif">';
			$aux[] = $aux3;
			$imagens = '';
			foreach($aux as $campo => $valor)
			{
				$imagens .= '<img'.$valor;
			}
			$info[$i]["estrelas"] = $imagens;
			$i++;
		}
		return $info;
	}
	
	public function player($width,$height){
		$this->url2id();
		print '<object width="'.$width.'" height="'.$height.'"><param name="movie" value="http://www.youtube.com/v/'.$this->id.'&fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/'.$this->id.'&fs=1" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="'.$width.'" height="'.$height.'"></embed></object>';	
	}
}

/**
$obj->url = "http://www.youtube.com/watch?v=c3X91qbVWck";
$obj->player("480","385");
print '<br>';
print $obj->url2id();
print '<br>';
print $obj->url2id_("http://www.youtube.com/watch?v=TNMpa5yBf5o&feature=channel");
print '<br>';
print $obj->thumb_url("maior");
print '<br>';
print $obj->thumb_url();
print '<br>';
print $obj->thumb();
print '<br>';
print $obj->thumb("maior");
print '<br>';
$info = $obj->info();
print $info["titulo"];
print '<br>';
print $info["descricao"];
print '<br>';
print $info["views"];
print '<br>';
print $info["tempo"];
print '<br>';
print $info["estrelas"];
print '<br>';
$busca = $obj->busca("alice wonders");
foreach($busca as $campo => $valor)
{
	foreach($valor as $campo2 => $valor2)
	{
		print $campo2.' = '.$valor2.'<br>';
	}
	print '--------------<br>';
}
*/
?>
