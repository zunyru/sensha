<?php
class URL{
	function _($link, $setting = ''){
		if( URL_REWRITE == 2 && WEB_PAGE == 2 ){
			$char = array('=', '&', '?');
			$link = str_replace( $char, '/', BASE_URL.'/'.$link );			
		}
		
		if($link == 'get'){
			$link = URL::get($setting);	
		}
		
		return $link;
	}
	
	function get($_strip_param = ''){		
		$params = explode(',',trim($_strip_param));
		foreach($params as $param){
			$strip_param[] = trim($param);	
		}
		
		$g_url = $_SERVER['QUERY_STRING'];
		$g_char = array('=', '&');
		$g_link = str_replace( $g_char, '/', $g_url );									
		
		$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
		$g_url = $base_url."/index.php?";
		
		$g_viewcode = explode('/', $g_link);
		$g_viewcode = array_merge(array(''), $g_viewcode);
		
		$g_num_c = count($g_viewcode);
		for($j = 0; $j < $g_num_c; $j+=2) {
			if( !in_array($g_viewcode[$j+1], $strip_param ) ){
				$g_url .= ($j==0 ? '' : '&').$g_viewcode[$j+1].'='.$g_viewcode[$j+2];
			}
		}
		if( $g_url ){
			$g_url = str_replace( array('&='), '', $g_url);
			$url = $g_url;
		}
		return $url;
	}	
		
	 function redirect($url) {
		if (headers_sent()) {
			echo "<script>document.location.href='".$url."';</script>\n";
		} else {
			@ob_end_clean(); // clear output buffer
			header( 'HTTP/1.1 301 Moved Permanently' );
			header( "Location: ". $url );
		}
		exit();
	 }
}
class SEF{
	function _($link, $options = array()){
		
		$link_normal = $link;
		$link = str_replace(array('?', 'index.php'), '', $link);
		$link = str_replace(array('&', '='), '/', $link);
		$q_code = explode('/', $link);
		$q_set = array();
		for($i = 0; $i < count($q_code); $i++){			
			if( $i%2 == 0 ){
				$q_set[$q_code[$i]] = $q_code[$i+1];
				${$q_code[$i]} = $q_code[$i+1];
			}
		}
		
		$option_skip = count($options['skip']) > 0 ? $options['skip'] : array();
		$option_replace = count($options['replace']) > 0 ? $options['replace'] : array();
		$option_merge = count($options['merge']) > 0 ? $options['merge'] : array();
		
		$url = array();
	
		foreach($q_set as $var => $val){
			foreach($option_replace as $opt_rep_k => $opt_rep_v){
				if( $var == $opt_rep_k ){
					$val = $q_set[$opt_rep_v];
					array_push($option_skip, $opt_rep_v);
				}
			}
			if( !in_array($var, $option_skip) ){
				foreach($option_merge as $om){
					$merge_fnc = explode(':', $om);
					if( count($merge_fnc) > 1 ){
						if( $merge_fnc[0] == 'gencode' ){
							if( in_array($var, $merge_fnc) ){
								$url_merge = array();
								foreach($option_merge as $om2){
									if( $om2 == ($merge_fnc[0].':'.$merge_fnc[1]) ){
										$url_merge[] = SEF::gencode($q_set[$merge_fnc[1]], 3);
									}
									else{
										$url_merge[] = $om2;	
									}
								}
								$val = implode('', $url_merge);
							}							
						}
					}
				}
				$url[] = $val;
			}			
		}
		if( URL_REWRITE ){
			return BASE_URL.'/'.implode('/', $url);
		}
		else{
			return BASE_URL.'/'.$link_normal;
		}
		
	}

	function gencode($value, $digit_count){
	
		$len = strlen($value);
		$code = "";
		for($i = 0; $i < $digit_count; $i++){
			$code .= "0";
		}
		$code = substr($code, $len);
		$code = $len>=$digit_count ? $value : $code.$value;
		
		return $code;
	}
}
?>