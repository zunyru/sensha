<?php
class Paging{
	function __construct($params){
		
		$this->total_row = $params['total_row'];
		
		$this->per_page = $params['per_page'] ? $params['per_page'] : 50;
		$this->total_page = ceil(@($this->total_row/$this->per_page));
		
		/**
		 * $max_page กำหนดค่าแสดงจำนวนเลขหน้าในแต่ล่ะ set กรณีมีมากกว่า 10 หน้า
		 */
		$this->max_page = 10;
		
		$this->current_page = $params['page'] ? $params['page'] : (param('page') ? param('page') : 1);
		
		/**
		 * $page_center_rank กำหนดช่วงแสดงของชุดเลขหน้า ไว้ตรงกลางที่เท่าไหร่
		 */
		$this->page_center_rank = 4;
		
		$this->page_start = 1;
		
		if( $this->current_page >= 7 && $this->current_page > $this->page_center_rank ){
			$this->page_start = $this->current_page - $this->page_center_rank;
			if( $this->current_page < ($this->total_page-($this->page_center_rank)) ){				
				$this->max_page = $this->max_page + ($this->page_start-1);
			}
			else{
				$this->max_page = $this->max_page + ($this->page_start-2);
			}
		}
		
		if( $this->max_page > $this->total_page ){
			$this->max_page = $this->total_page;	
		}
		
		if( $this->current_page+$this->page_center_rank > $this->total_page ){
			$this->page_start = $this->page_start - ($this->current_page+$this->page_center_rank - $this->total_page) - 1;
		}
		
		/**
		 * Text
		 */
		 $this->previous_text = $params['previous_text'] ? $params['previous_text'] : "ก่อนหน้า";
		 $this->next_text = $params['next_text'] ? $params['next_text'] : "ถัดไป";
		
	}
	
	function get_paging($params = array()){		

		$paging = array();
		
		if( $this->current_page > 1 ){
			$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page=1">«</a></li>';
			$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page='.($this->current_page-1).'">‹</a></li>';
			if( $this->current_page >= 7 ){
				$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page=1">1</a></li>';
				$paging[] = '<li class="disabled"><a href="javascript:void(0);">...</a></li>';
			}
		}
		
		if( $this->page_start < 1 ){
			$this->page_start = 1;	
		}
		
		for($i = $this->page_start; $i <= $this->total_page; $i++){	
		
			if( $this->current_page == $i	){
				$page_active_class[$i] = 'class="active"';	
			}
			if( $i <= $this->max_page ){
				$paging[] = '<li '.$page_active_class[$i].'><a href="'.(URL::_('get', 'page')).'&page='.$i.'">'.$i.'</a></li>';	
			}
		}
		
		if( $this->current_page < $this->total_page ){
			if( $this->current_page < ($this->total_page-($this->page_center_rank+1)) ){
				$paging[] = '<li class="disabled"><a href="javascript:void(0);">...</a></li>';
				$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page='.$this->total_page.'">'.$this->total_page.'</a></li>';
			}
			$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page='.($this->current_page+1).'">›</a></li>';	
			$paging[] = '<li><a href="'.(URL::_('get', 'page')).'&page='.$this->total_page.'">»</a></li>';		
		}
		
		if( $params['render'] == 1 or $params['render'] == true ){
			
			$html = '<ul class="pagination">';
			foreach($paging as $p){
				$html .= $p;
			}
			$html .= '</ul>';
			
			return $html;
		}
		else{		
			return $paging;
		}
		
	}
	
	function get_paging_frontend($params = array()){		

		$paging = array();
		$link = $params['link'];
		
		if( $this->current_page > 1 ){
			$paging[] = '<li><a href="'.URL::_($link.'/page-'.($this->current_page-1)).'">'.$this->previous_text.'</a></li>';
			if( $this->current_page >= 7 ){
				$paging[] = '<li><a href="'.URL::_($link.'/page-1').'">1</a></li>';
				$paging[] = '<li><span class="pipe-page" style="margin-right:5px;">...</span></li>';
			}
		}
		
		if( $this->page_start < 1 ){
			$this->page_start = 1;	
		}
		
		for($i = $this->page_start; $i <= $this->total_page; $i++){	
		
			if( $this->current_page == $i	){
				$page_active_class[$i] = ' active';	
			}
			if( $i <= $this->max_page ){
				$paging[] = '<li class="'.$page_active_class[$i].'"><a href="'.URL::_($link.'/page-'.$i).'" tabindex="'.$i.'">'.$i.'</a></li>';	
			}
		}
		
		if( $this->current_page < $this->total_page ){
			if( $this->current_page < ($this->total_page-($this->page_center_rank+1)) ){
				$paging[] = '<li><span class="pipe-page" style="margin-right:5px;">...</span></li>';
				$paging[] = '<li><a href="'.URL::_($link.'/page-'.$this->total_page).'">'.$this->total_page.'</a></li>';
			}
			$paging[] = '<li><a href="'.URL::_($link.'/page-'.($this->current_page+1)).'">'.$this->next_text.'</a></li>';			
		}
		
		if( $params['render'] == 1 or $params['render'] == true ){
			
			$html = '<ul class="pagination pagination-sm">';
			foreach($paging as $p){
				$html .= $p;
			}
			$html .= '</ul>';
			
			return $html;
		}
		else{		
			return $paging;
		}
		
	}
}

 class KMSPaging{
 	var $total;
	var $p_size;
	var $total_page;
	var $page;
	var $start;
	var $perpage;
	var $pageLink;
	var $page_limit = 5;
	
 	function getTotalRecord(){
		return $this->total;
	}
	
	function getTotalPage(){
		return $this->total_page;
	}
	
	function getStartPage(){
		return $this->start;
	}
	
	function getPerPage(){
		return $this->perpage;
	}
	
	function getPage(){
		return $this->page;
	}
	
	function getPageSize(){
		return $this->p_size;
	}

 	function KMSPaging($total, $p_size,  $curr_page, $pageLink = NULL){
		
		$this->total 	= $total;//count($result);
		$this->p_size	= $p_size;
		$this->page		= $curr_page;
		$this->pageLink = $pageLink;

	 	$this->total_page = (int)($this->total/$this->p_size);
	 	if(($this->total % $this->p_size) != 0){
			$this->total_page++;
	 	}
	 	if(empty($this->page)){
			$this->page = 1;
			$this->start = 0;
	 	}else{
			if($this->page > $this->total_page){
				$this->page = $this->total_page;
			}
			else{
				$this->page = $this->page;
			}
			$this->start = $this->p_size*($this->page-1);
	 	}
	 	$this->perpage = (($this->page-1)*$this->p_size+$this->p_size);
		
		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		
		$this->btn_start_enable = '
		<a class="first paginate_button" tabindex="0" id="datagrid_first" href="'.$this->pageLink.((SEF == 1 && SYS_PAGE != 1) ? "/page-1" : "&page=1").'">'.PAGING_START.'</a>
		';
		$this->btn_start_disable = '
		<a class="first paginate_button paginate_button_disabled" tabindex="0" id="datagrid_first">'.PAGING_START.'</a>
		';
		
		$this->btn_previous_enable = '
		<a class="previous paginate_button" tabindex="0" id="datagrid_previous" href="'.$this->pageLink.((SEF == 1 && SYS_PAGE != 1) ? "/page-".$prev_page : "&page=".$prev_page).'">'.PAGING_PREVIOUS.'</a>
		';
		$this->btn_previous_disable = '
		<a class="previous paginate_button paginate_button_disabled" tabindex="0" id="datagrid_previous">'.PAGING_PREVIOUS.'</a>
		';
		
		$this->btn_next_enable = '
		<a class="next paginate_button" tabindex="0" id="datagrid_next" href="'.$this->pageLink.((SEF == 1 && SYS_PAGE != 1) ? "/page-".$next_page : "&page=".$next_page).'">'.PAGING_NEXT.'</a>
		';
		$this->btn_next_disable = '
		<a class="next paginate_button paginate_button_disabled" tabindex="0" id="datagrid_next">'.PAGING_NEXT.'</a>
		';
		
		$this->btn_end_enable = '
		<a class="last paginate_button" tabindex="0" id="datagrid_last" href="'.$this->pageLink.((SEF == 1 && SYS_PAGE != 1) ? "/page-".$this->total_page : "&page=".$this->total_page).'">'.PAGING_END.'</a>
		';
		$this->btn_end_disable = '
		<a class="last paginate_button paginate_button_disabled" tabindex="0" id="datagrid_last">'.PAGING_END.'</a>
		';
		
		$this->btn_page_box_left = '
		<div class="dataTables_paginate paging_full_numbers" id="datagrid_paginate">
		';
		$this->btn_page_box_right = '
		</div>
		';
		
		$this->btn_page_item_current = '<a class="paginate_active" tabindex="0">%s</a>';
		$this->btn_page_item = '%s';
		
		$this->element_first1 = "";
		$this->element_first2 = "&laquo;";
		
		$this->element_prev1 = "";
		$this->element_prev2 = "&lsaquo;";
		
		$this->element_next1 = "";
		$this->element_next2 = "&rsaquo;";
		
		$this->element_last1 = "";
		$this->element_last2 = "&raquo;";
	}
	
	 function paging(){

		$paging .= $this->btn_page_box_left;
		
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			if( $this->page == 1 || !$this->page){
				$paging .= $this->btn_start_disable;
				$paging .= $this->btn_previous_disable;
			}
			else{
				$paging .= $this->btn_start_enable;
				$paging .= $this->btn_previous_enable;
			}
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			/*********************************************************************************************/
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
			}			
			/*********************************************************************************************/
			
			$paging .= '<span>';
			
			for($i = $page_start; $i < $page_end; $i++) {
				if( ($i+1) == $this->page ){
					$paging .= sprintf($this->btn_page_item_current, ($i+1));
				}
				else{
					$paging .= sprintf($this->btn_page_item, $this->setLink(( $i+1 ), $this->pageLink .((SEF == 1 && SYS_PAGE != 1) ? "/page-".($i+1) : "&page=".($i+1)), 'paginate_button'));//$this->setLink(( $i+1 ), $this->pageLink .((SEF == 1 && SYS_PAGE != 1) ? "/page-".($i+1) : "&page=".($i+1)), $current_page_class);
				}
			}
			
			$paging .= '</span>';			
			
			if( $this->page < $this->total_page){
				$paging .= $this->btn_next_enable;
				$paging .= $this->btn_end_enable;
			}
			
			if( $this->page == $this->total_page){
				$paging .= $this->btn_next_disable;
				$paging .= $this->btn_end_disable;
			}
		}
		
		$paging .= $this->btn_page_box_right;
		
		return $paging;
	
	}
	
	 function paging_front(){
		 
		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		 
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			if( $this->page == 1 || !$this->page){
			}
			else{
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-1" : "&page=1").'">&laquo;</a>';
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$prev_page : "&page=".$prev_page).'">&lsaquo;</a>';
			}
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			/*********************************************************************************************/
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
			}			
			/*********************************************************************************************/
			
			for($i = $page_start; $i < $page_end; $i++) {
				if( ($i+1) == $this->page ){
					$paging .= '<a href="'.($link).'" class="current_page">'.($i+1).'</a>';
				}
				else{
					$paging .= sprintf($this->btn_page_item, $this->setLink(( $i+1 ), $this->pageLink .((SEF == 1 && SYS_PAGE != 1) ? "/page-".($i+1) : "&page=".($i+1)), $current_page_class));
				}
			}
			
			if( $this->page < $this->total_page){
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$next_page : "&page=".$next_page).'" >&rsaquo;</a>';
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$this->total_page : "&page=".$this->total_page).'">&raquo;</a>';
			}
			
		}
	
		return $paging;
	
	}
	
	/*
	function paging_custom(){
		 
		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		 
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			if( $this->page == 1 || !$this->page){
			}
			else{
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-1" : "&page=1").'" class="first first_page">|<</a>';
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$prev_page : "&page=".$prev_page).'" class="prev"><</a>';
			}
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			///////////
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
			}			
			//////////
			
			for($i = $page_start; $i < $page_end; $i++) {
				if( ($i+1) == $this->page ){
					$paging .= '<a href="'.($link).'" class="current_page'.($i == 0 ? ' first_page' : '').'">'.($i+1).'</a>';
				}
				else{
					 if($i == 0){
					 	$current_page_class = 'first_page';
					 }
					$paging .= sprintf($this->btn_page_item, $this->setLink(( $i+1 ), $this->pageLink .((SEF == 1 && SYS_PAGE != 1) ? "/page-".($i+1) : "&page=".($i+1)), $current_page_class));
				}
			}
			
			if( $this->page < $this->total_page){
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$next_page : "&page=".$next_page).'" class="next">></a>';
				$paging .= '<a href="'.$this->pageLink.(SEF == 1 ? "/page-".$this->total_page : "&page=".$this->total_page).'" class="last">>|</a>';
			}
			
		}
	 
		return $paging;
	
	}
	*/
	
	function paging_custom(){
		 
		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		 
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			if( $this->page == 1 || !$this->page){
			}
			else{
				$paging .= '<li class="arrow"><a href="'.$this->pageLink.'/page/1" class="first first_page">&lt;&lt;</a></li>';
				$paging .= '<li class="arrow"><a href="'.$this->pageLink.'/page/'.$prev_page.'" class="prev">&lt;</a></li>';
			}
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			///////////
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
			}			
			//////////
			
			for($i = $page_start; $i < $page_end; $i++) {
				if( ($i+1) == $this->page ){
					$paging .= '<li class="current"><a href="'.($link).'">'.($i+1).'</a></li>';
				}
				else{
					 /*if($i == 0){
					 	$current_page_class = 'current';
					 }*/
					$paging .= sprintf($this->btn_page_item, $this->setLinkCustom(( $i+1 ), $this->pageLink."/page/".($i+1), $current_page_class));
				}
			}
			
			if( $this->page < $this->total_page){
				$paging .= '<li class="arrow"><a href="'.$this->pageLink."/page/".$next_page.'" class="next">&gt;</a></li>';
				$paging .= '<li class="arrow"><a href="'.$this->pageLink."/page/".$this->total_page.'" class="last">&gt;&gt;</a></li>';
			}
			
		}
	 
		return $paging;
	
	}
	
	function paging_ajax($ajax_fnc, $page, $params = array()){
	
		$params = @implode("','", $params);
		$params = "'".$params."'";

		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		
		$element_first1 = $this->element_first1;
		$element_first2 = $this->element_first2;
		
		$element_prev1 = $this->element_prev1;
		$element_prev2 = $this->element_prev2;
		
		$element_next1 = $this->element_next1;
		$element_next2 = $this->element_next2;
		
		$element_last1 = $this->element_last1;
		$element_last2 = $this->element_last2;
		
		if( $this->page == 1){
			$first_page 	= $element_first1."&nbsp;";
			$prev_page = $element_prev1;
		}
		else{
			$first_page = $this->setLinkAjax($element_first2, $ajax_fnc.'(1,'.$params.');', 'first_page');
			$prev_page = $this->setLinkAjax($element_prev2, $ajax_fnc.'('.$prev_page.','.$params.');', 'previous_page');
		}
		
		if( $this->page == $this->total_page  && $this->total_page <= $this->page_limit){
			$next_page 	= $element_next1;
			$last_page 	= $element_last1;
		}
		else{
			$next_page = $this->setLinkAjax($element_next2, $ajax_fnc.'('.$next_page.','.$params.');', 'next_page');
			$last_page = $this->setLinkAjax($element_last2, $ajax_fnc.'('.$this->total_page.','.$params.');', 'last_page');
		}
		
		
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			if( $this->page != 1){
				$paging .= '<label class="none_link">'.$first_page.'</label>';
				$paging .= '<label class="none_link">'.$prev_page.'</label>';
			}
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			/*********************************************************************************************/
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
			}			
			/*********************************************************************************************/
			for($i = $page_start; $i < $page_end; $i++) {
				$current_page_class = ($i+1) == $this->page ? 'current_page' : 'normal_page';
				$paging .= $this->setLinkAjax('<span>'.( $i+1 ).'</span>', $ajax_fnc.'('.($i+1).','.$params.');', $current_page_class);
			}
			if( $this->page != $this->total_page ){// && $this->total_page <= $this->page_limit){				
				$paging .= $next_page;
				$paging .= $last_page;
			}
		}
	
		return $paging;
	
	}

	function setLinkCustom($title, $link = '', $element_class = ''){
		return '<li '.($element_class ? 'class="'.$element_class.'"' : '').'><a href="'.($link ? $link : 'javascript:void(null);').'">'.$title.'</a></li>';
	}

	function setLink($title, $link = '', $element_class = ''){
		return '<a href="'.($link ? $link : 'javascript:void(null);').'" '.($element_class ? 'class="'.$element_class.'"' : '').'>'.$title.'</a>';
	}

	function setLinkAjax($title, $ajax_fnc = '', $element_class = ''){
		return '<a href= "javascript:void(null);" '.($ajax_fnc ? 'onClick="'.$ajax_fnc.'"' : '').' '.($element_class ? 'class="'.$element_class.'"' : '').'>'.$title.'</a>';
	}
	
	function paging_ajax_v2($ajax_fnc, $page, $params = array()){
	
		$params = @implode("','", $params);
		$params = "'".$params."'";

		if( ($this->page -1) <= 0 ){
			$prev_page = 1;
		}
		else{
			$prev_page = $this->page - 1;
		}
		
		if( $this->page == $this->total_page ){
			$next_page = $this->total_page;
		}
		else{
			$next_page = $this->page + 1;
		}
		
		if( $this->page == 1){
			$prev_page = '<a href= "javascript:void(null);" onClick="'.$ajax_fnc.'('.$prev_page.','.$params.');" class="pre">&laquo;</a>';
		}
		else{
			$prev_page = '<a href= "javascript:void(null);" onClick="'.$ajax_fnc.'('.$prev_page.','.$params.');" class="pre">&laquo;</a>';
		}
		
		if( $this->page == $this->total_page  && $this->total_page <= $this->page_limit){
			$next_page = '<a href="javascript:void(null);" class="next">&raquo;</a>';
		}
		else{
			$next_page = '<a href= "javascript:void(null);" onClick="'.$ajax_fnc.'('.$next_page.','.$params.');" class="next">&raquo;</a>';
		}
		
	 	if($this->total_page == 0){
			$paging = "&nbsp;";
	 	}
	 	else{
			$paging .= $first_page;
			$paging .= $prev_page;
			$paging .= "&nbsp;";
			
			if( $this->page > $this->limit_page ){
				$start_page = ($this->limit_page + $this->page) - $this->limit_page;
			}
			else{
				$start_page = 0;
			}
			/*********************************************************************************************/
			$page_start = $this->page-$this->page_limit;
			
			if($page_start<=0) { $page_start = 0; }
							
			$page_end = $page_start+( ($this->page_limit * 2)-1 );
			if($page_end>$this->total_page){
				$page_end = $this->total_page;
				switch ($this->page){
				}
			}

			/*********************************************************************************************/
			for($i = $page_start; $i < $page_end; $i++) {
				if( ($i+1) == $this->page ){
					$paging .= '<a href= "javascript:void(null);" onClick="'.$ajax_fnc.'('.($i+1).','.$params.');" class="active" rel="'.($i+1).'">'.($i+1).'</a>';
				}
				else{
					$paging .= '<a href= "javascript:void(null);" onClick="'.$ajax_fnc.'('.($i+1).','.$params.');" rel="'.($i+1).'">'.($i+1).'</a>';
				}
				
				if( ($i+1) != ($this->total_page) ){
					$paging .= "&nbsp;";
				}
			}
			if( $this->total_page > $this->limit_page){
			}
			
			$paging .= "&nbsp;";
			$paging .= $next_page;
		}
	
		return $paging;
	
	}
	
	function set_button($type, $value){
		$this->{$type} = $value;
	}
	
	function get_button($type){
		return $this->{$type};
	}

 }
?>