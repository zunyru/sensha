function setPublish(frmAction){
	
	
	var v1 = document.forms['adminForm'].elements['is_publish'].value = '1';
	
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			document.forms['adminForm'].action = frmAction;
			document.forms['adminForm'].submit();	
		}
	}
}

function setUnPublish(frmAction){
	var v1 = document.forms['adminForm'].elements['unpublish'].value = '0';
	
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			document.forms['adminForm'].action = frmAction;
			document.forms['adminForm'].submit();	
		}
	}
}

function setSort(frmAction){
	var v1 = document.forms['adminForm'].elements['sort'].value = '1';
	if(v1 != ""){
		document.forms['adminForm'].action = frmAction;	
		document.forms['adminForm'].submit();	
	}
}

function setSave(frmAction){
	var v1 = document.forms['adminForm'].elements['save'].value = '1';
	if(v1 != ""){
		document.forms['adminForm'].action = frmAction;	
		document.forms['adminForm'].submit();	
	}
}

function setApply(frmAction){
	var v1 = document.forms['adminForm'].elements['apply'].value = '1';
	if(v1 != ""){
		document.forms['adminForm'].action = frmAction;
		document.forms['adminForm'].submit();	
	}
}

function setDelete(frmAction){
	var v1 = document.forms['adminForm'].elements['deletion'].value = '1';
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			if( confirm('Delete Confirm ?') == true){
				document.forms['adminForm'].action = frmAction;
				document.forms['adminForm'].submit();	
			}
			else{
				return false;	
			}
		}
	}
}

function setRemove(frmAction){
	var v1 = document.forms['adminForm'].elements['remove_able'].value = '1';
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			if( confirm('Remove Confirm ?') == true){
				document.forms['adminForm'].action = frmAction;
				document.forms['adminForm'].submit();	
			}
			else{
				return false;	
			}
		}
	}
}

function setEdit(frmAction){
	var v1 = document.forms['adminForm'].elements['edition'].value = '1';
	
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			document.forms['adminForm'].action = frmAction;
			document.forms['adminForm'].submit();	
		}
	}
}

function setSubmition(frmAction){
	var v1 = document.forms['adminForm'].elements['submition'].value = '1';
	
	if( isSelected() == false ){
		alert('กรุณาเลือก');
	}
	else{
		if(v1 != ""){
			document.forms['adminForm'].action = frmAction;
			document.forms['adminForm'].submit();	
		}
	}
}

function isSelected() {
  	$("form#adminForm .itemID").each(function(){
		if( $(this).attr('checked') ){
			document.adminForm.cbvalue.value = 1;	
		}
	});
	
	if(document.adminForm.cbvalue.value <= 0){
		return false;
	}
	else{
		return true;
	}
}

function form_save_default(){
	$("#admin_form").submit();
	//$("#form_save_default_loading").html('<img src="'+base_url+'/images/loading/ajax-loader-f4.gif">');
}

function form_save_exit(){
	$("#form_save_exit").val(1);
	//$("#form_save_exit_loading").html('<img src="'+base_url+'/images/loading/ajax-loader-f4.gif">');
	$("#admin_form").submit();
}

function form_save_new(){
	$("#form_save_new").val(1);
	//$("#form_save_new_loading").html('<img src="'+base_url+'/images/loading/ajax-loader-f4.gif">');
	$("#admin_form").submit();
}

