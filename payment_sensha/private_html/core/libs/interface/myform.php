<?php
class MyXajaxForm{
	var $form_title;
	var $name;
	var $action_function;
	var $form;
	var $db;
	function MyXajaxForm($setting, $db){
		$this->db = $db;
		$this->setting($setting);
		$this->form = '<form id="'.$this->name.'" name="'.$this->name.'" method="post" action="javascript:void(null)" onsubmit="xajax.upload(\''.$this->action_function.'\', \''.$this->name.'\');" enctype="multipart/form-data">';
		$this->form .= '<table width="100%" border="0" cellspacing="0" cellpadding="3" class="adminlist">';
			 $this->form .= '<tr>';
				$this->form .= '<th height="25" colspan="4" align="left"><h3 style="padding:0; margin:0;">'.$this->form_title.'</h3></th>';
			 $this->form .= '</tr>';
	}
	
	function render(){		
		$this->form .= '<tr class="trBody">';
		   	$this->form .= '<td height="25" align="right" class="font_th">&nbsp;</td>';
		   	$this->form .= '<td>';
		     	$this->form .= '<input type="submit" name="button" id="button" value="Save" />';
		     	$this->form .= '<input type="reset" name="button2" id="button2" value="Reset" />';
		   	$this->form .= '</td>';
		   	$this->form .= '<td>&nbsp;</td>';
		   	$this->form .= '<td>&nbsp;</td>';
      	$this->form .= '</tr>';
		$this->form .= '</table>';
		$this->form .= '</form>';
		return $this->form;
	}
	
	function setting($value){
		$this->form_title			= $value['form_title'];
		$this->name 					= $value['name'];
		$this->action_function 	= $value['action_function'];
	}
	
	function setText($value){
		global $objResponse;
		$this->form .= '<tr class="trBody">';
			$this->form .= '<td width="34%" height="25" align="right" class="font_th">'.$value['label'].'</td>';
			$this->form .= '<td width="21%"><input type="text" name="'.$value['name'].'"  id="'.$value['name'].'" class="textbox" style="width:250px" maxlength="255" value="'.$value['value'].'" '.$value['event'].'/></td>';
			$this->form .= '<td width="1%">'.($value['marked'] ? '<span class="marked">*</span>' : '').'</td>';
			$this->form .= '<td width="44%"><span id="xerror_'.$value['name'].'" class="errorMessageFalse" >&nbsp;</span></td>';
		$this->form .= '</tr>';
		$objResponse->addAssign("xerror_title","innerHTML", "Please enter title");
		return $objResponse;
	}
	
	function setSelectFromQuery($value){
		$this->form .= '<tr class="trBody">';
			$this->form .= '<td width="34%" height="25" align="right" class="font_th">'.$value['label'].'</td>';
			$this->form .= '<td width="21%">';
				$this->form .= '<select name="'.$value['name'].'" id="'.$value['name'].'" style="width:250px;" class="textbox">';
				
				$rs = $this->db->GetArray( $value['query'] );
				for( $i = 0; $i < count($rs); $i++){
					$this->form .= '<option value="'.$rs[$i]['cateid'].'">'.$rs[$i]['title'].'</option>';
				}
				
				$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '<td width="1%">'.($value['marked'] ? '<span class="marked">*</span>' : '').'</td>';
			$this->form .= '<td width="44%"><span id="error_'.$value['name'].'" class="errorMessageFalse" >&nbsp;</span></td>';
		$this->form .= '</tr>';
	}
	
}
?>