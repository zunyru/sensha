<?php
class SCToolsbar {

	var $link_set = array();
	var $title_set = array();
	
	function SCToolsbar(){}

	function load(){
		$fnc = getParam('fnc');
		$app = getParam('app');
		$class = class_exists('Toolsbar') ? 'Toolsbar' : 'TB_'.ucfirst($app);
		$toolsbar = new $class();		
		if($fnc){  
			return $toolsbar->$fnc();
		}
		else{
			return $toolsbar->default_index();
		}
	}
	
	// New Button
	function newButton($link = '#', $text = 'New', $title = 'New', $alt = 'New'){	
		//$html = '<a href="'.$link.'" class="btn" title="'.$text.'"><i class="icol-add"></i> '.$text.'</a>';
		$html = '<a href="'.$link.'" class="btn btn-large btn-teal"><i class="icon-plus"></i> '.$text.'</a> ';
		return $html;		
	}

	// Save Button
	function saveButton($link = '', $text = 'Save', $title = 'Save', $alt = 'Save'){
		
//		$prob = "onmouseover=\"this.style.border='1px solid #000000' \"
//					 onmouseout=\"this.style.border='1px solid #CCCCCC' \"";		
//					 	
//		$btn  = BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save.png";
//		$link = '<div id="divButton" ><div class="divButton-inner" '.$prob.' ><input name="btnSave" id="btnSave" type="image" src="'.$btn.'" align="middle" alt="'.$alt.'"  /><br />'.$text.'</div></div>';		
		
//		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save.png' border='0' alt='".$alt."'/><br />".$text."";
//		$btn2 = "<img src=' ".BASE_BACKEND_URL ."/images/loading/button_loader.gif' border='0' alt='".$alt."'/><br />...Saving...";
//		$link = 'form_save_default();';
//		$link = "<div id='divButton-saving' style='display:none;'><a href='javascript:void(null);' title='".$title."'>".$btn2."</a></div>";		
//		$link .= "<div id='divButton-save'><a href='javascript:void(null);' onclick='".$link."' title='".$title."' id='buttom_form_save'>".$btn."</a></div>";
		
		$btn_disable  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save_disable.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$btn  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$btn2 = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/loading.gif" width="32" border="0" alt="'.$alt.'"/><br />...Saving...';
		$button = '<div id="divButton-saving" style="display:none;"><a href="javascript:void(null);" title="'.$title.'" style="border:none; background:none;">'.$btn2.'</a></div>';
		$button .= '<div id="divButton-save"><a href="javascript:void(null);" onclick="'.$link.'" title="'.$title.'" id="button_form_save">'.$btn.'</a><a href="javascript:void(null);" id="button_form_save_disable" style="display:none;">'.$btn_disable.'</a></div>';
		
		return $button;		
	}

	// Save & Exit Button
	function saveExitButton($link = '', $text = 'Save & Exit', $title = 'Save & Exit', $alt = 'Save & Exit'){
		
//		$prob = "onmouseover=\"this.style.border='1px solid #000000' \"
//					 onmouseout=\"this.style.border='1px solid #CCCCCC' \"";		
//					 	
//		$btn  = BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save_exit.png";
//		$link = '<div id="divButton" ><div class="divButton-inner" '.$prob.' ><input name="btnSaveExit" id="btnSaveExit" type="image" src="'.$btn.'" align="middle" alt="'.$alt.'" /><br />'.$text.'</div></div>';		
	
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save_exit.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'form_save_exit();';
		$link = "<div id='divButton-save-exit'><a href='javascript:void(null);' onclick='".$link."' title='".$title."' id='button_form_save_exit'>".$btn."</a></div>";		
		
		$btn_disable  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save_exit_disable.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$btn  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save_exit.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$button = '<div id="divButton-save-exit"><a href="javascript:void(null);" onclick="'.$link.'" title="'.$title.'" id="button_form_save_exit">'.$btn.'</a><a href="javascript:void(null);" id="button_form_save_exit_disable" style="display:none;">'.$btn_disable.'</a></div>';
		
		return $button;		
	}

	// Save & New Button
	function saveNewButton($link = '', $text = 'Save & New', $title = 'Save & New', $alt = 'Save & New'){
		
//		$prob = "onmouseover=\"this.style.border='1px solid #000000' \"
//					 onmouseout=\"this.style.border='1px solid #CCCCCC' \"";		
//					 	
//		$btn  = BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save_new.png";
//		$link = '<div id="divButton" ><div class="divButton-inner" '.$prob.' ><input name="btnSaveNew" id="btnSaveNew" type="image" src="'.$btn.'" align="middle" alt="'.$alt.'" /><br />'.$text.'</div></div>';		
	
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_save_new.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'form_save_new();';
		$link = "<div id='divButton-save-new'><a href='javascript:void(null);' onclick='".$link."' title='".$title."'  id='button_form_save_new'>".$btn."</a></div>";		
		
		$btn_disable  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save_new_disable.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$btn  = '<img src="'.BASE_BACKEND_URL.'/images/kms_toolsbar/apl_save_new.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$button = '<div id="divButton-save-new"><a href="javascript:void(null);" onclick="'.$link.'" title="'.$title.'" id="button_form_save_new">'.$btn.'</a><a href="javascript:void(null);" id="button_form_save_new_disable" style="display:none;">'.$btn_disable.'</a></div>';
		
		return $button;		
	}

	// Send Mail Button
	function sendMailButton($link = '', $text = 'Send Mail', $title = 'Send Mail', $alt = 'Send Mail'){
		
		$prob = "onmouseover=\"this.style.border='1px solid #000000' \"
					 onmouseout=\"this.style.border='1px solid #CCCCCC' \"";		
					 	
		$btn  = BASE_BACKEND_URL ."/images/kms_toolsbar/apl_send_mail.png";
		$link = '<div id="divButton" ><div class="divButton-inner" '.$prob.' ><input name="btnSendMail" id="btnSendMail" type="image" src="'.$btn.'" align="middle" alt="'.$alt.'"  /><br />'.$text.'</div></div>';		
		return $link;		
	}

	// Previous Button
	function backButton($link = '', $text = 'Previous', $title = 'Previous', $alt = 'Previous'){
	
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_back.png' border='0' alt='".$alt."'/><br />".$text."";
		//$link = 'setSubmition(" '. $link .' ");';
		//$link = "<div id='divButton'><a href='#' onclick='".$link."' title='".$title."'>".$btn."</a></div>";
		$link = SCToolsbar::setLink($link, $btn, $title);
		return $link;		
	}

	// Next Button
	function nextButton($link = '', $text = 'Next', $title = 'Next', $alt = 'Next'){
	
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_next.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'setSubmition(" '. $link .' ");';
		$link = "<div id='divButton'><a href='#' onclick='".$link."' title='".$title."'>".$btn."</a></div>";		
		return $link;		
	}
	// Edit Button
	function editButton($link = '#', $text = 'Edit', $title = 'Edit', $alt = 'Edit'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_edit.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'setEdit(" '. $link .' ");';
		$link = "<div id='divButton'><a href='#' onclick='".$link."' title='".$title."'>".$btn."</a></div>";		
		return $link;		
	}

	// Config Button
	function configButton($link = '#', $text = 'Config', $title = 'Config', $alt = 'Config'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_config.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Exit Button
	function cancelButton($link = '#', $text = 'Exit', $title = 'Exit', $alt = 'Exit'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_cancel.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Delete Button
	function deleteButton($link = '#', $text = 'Delete', $title = 'Delete', $alt = 'Delete'){	
		//$html = '<a href="javascript:void(null);" onclick="setDelete(\''.$link.'\');" class="btn" title="'.$text.'"><i class="icol-delete"></i> '.$text.'</a>';
		$html = '<button class="btn btn-large btn-danger" onclick="setDelete(\''.$link.'\');"><i class="icon-minus"></i> '.$text.'</button>';
		return $html;
	}

	// Remove Button
	function removeButton($link = '#', $text = 'Remove', $title = 'Remove', $alt = 'Remove'){	
		$html = '<a href="javascript:void(null);" onclick="setRemove(\''.$link.'\');" class="btn" title="'.$text.'"><i class="icol-bin-closed"></i> '.$text.'</a>';
		return $html;
	}

	// Apply Button2
	function applyButton2($link = '#', $text = 'Apply', $title = 'Apply', $alt = 'Apply'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_apply.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'setApply(" '. $link .' ");';
		$link = "<div id='divButton'><a href='#' onclick='".$link."' title='".$title."'>".$btn."</a></div>";		
		return $link;		
	}
	
	// Apply Button
	function applyButton($link = '', $text = 'Apply', $title = 'Apply', $alt = 'Apply'){
		
		$prob = "onmouseover=\"this.style.border='1px solid #000000' \"
					 onmouseout=\"this.style.border='1px solid #CCCCCC' \"";		

		$btn  = BASE_BACKEND_URL ."/images/kms_toolsbar/apl_apply.png";
		$link = '<div id="divButton" ><div class="divButton-inner" '.$prob.' ><input name="btnApply" id="btnApply" type="image" src="'.$btn.'" align="middle" alt="'.$alt.'"  /><br />'.$text.'</div></div>';		
		return $link;		
	}

	// Reload Button
	function reloadButton($link = '#', $text = 'Reload', $title = 'Reload', $alt = 'Reload'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_reload.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Redo Button
	function redoButton($link = '#', $text = 'Redo', $title = 'Redo', $alt = 'Redo'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_redo.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Undo Button
	function undoButton($link = '#', $text = 'Undo', $title = 'Undo', $alt = 'Undo'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_undo.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Search Button
	function searchButton($link = '#', $text = 'Search', $title = 'Search', $alt = 'Search'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_search.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Printer Button
	function printerButton($link = '#', $text = 'Printer', $title = 'Printer', $alt = 'Printer'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_printer.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}

	// Help Button
	function helpButton($link = '#', $text = 'Help', $title = 'Help', $alt = 'Help'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_help.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}
	
	// Stats Button
	function statsButton($link = '#', $text = 'Stats', $title = 'Stats', $alt = 'Stats'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_stats.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}
	
	// Excel Export Button
	function excelExportButton($link = '#', $text = 'Excel Export', $title = 'Excel Export', $alt = 'Excel Export'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/apl_excel_export.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = SCToolsbar::setLink($link, $btn, $title);		
		return $link;		
	}
	
	// Publish Button
	function publishButton($link = '#', $text = 'Publish', $title = 'Publish', $alt = 'Publish'){	
		//$html = '<a href="javascript:void(null);" onclick="setPublish(\''.$link.'\');" class="btn" title="'.$text.'"><i class="icol-connect"></i> '.$text.'</a>';
		$html = '<button class="btn btn-large btn-blue" onclick="setPublish(\''.$link.'\');"><i class="icon-eye-open"></i> '.$text.'</button>';
		return $html;
	}
	
	// Unpublish Button
	function unpublishButton($link = '#', $text = 'UnPublish', $title = 'UnPublish', $alt = 'UnPublish'){	
		//$html = '<a href="javascript:void(null);" onclick="setUnPublish(\''.$link.'\');" class="btn" title="'.$text.'"><i class="icol-disconnect"></i> '.$text.'</a>';
		$html = '<button class="btn btn-large" onclick="setUnPublish(\''.$link.'\');"><i class="icon-eye-close"></i> '.$text.'</button>';
		return $html;
	}
	
	// Unpublish Button
	function sortButton($link = '#', $text = 'Sort', $title = 'Sort', $alt = 'Sort'){	
		
		$btn  = "<img src=' ".BASE_BACKEND_URL ."/images/kms_toolsbar/unknow.png' border='0' alt='".$alt."'/><br />".$text."";
		$link = 'setSort(" '. $link .' ");';
		$link = "<div id='divButton'><a href='#' onclick='".$link."' title='".$title."'>".$btn."</a></div>";		
		return $link;		
	}
	
	// Copy Function Button
	function copyFunctionButton($fnc_name = '', $text = 'Copy', $title = 'Copy', $alt = 'Copy'){	
		
		$btn  = '<img src="'.BASE_BACKEND_URL .'/images/kms_toolsbar/apl_copy.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$link = '<div id="divButton"><a href="javascript:void(null);" onclick="'.$fnc_name.'" title="'.$title.'">'.$btn.'</a></div>';		
		return $link;		
	}
	
	// Save Function Button
	function saveFunctionButton($fnc_name = '', $text = 'Save', $title = 'Save', $alt = 'Save'){	
		
		$btn  = '<img src="'.BASE_BACKEND_URL .'/images/kms_toolsbar/apl_save.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$link = '<div id="divButton"><a href="javascript:void(null);" onclick="'.$fnc_name.'" title="'.$title.'">'.$btn.'</a></div>';		
		return $link;		
	}
	
	// Cancel Function Button
	function cancelFunctionButton($fnc_name = '', $text = 'Exit', $title = 'Exit', $alt = 'Exit'){	
		
		$btn  = '<img src="'.BASE_BACKEND_URL .'/images/kms_toolsbar/apl_cancel.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$link = '<div id="divButton"><a href="javascript:void(null);" onclick="'.$fnc_name.'" title="'.$title.'">'.$btn.'</a></div>';		
		return $link;		
	}
	
	// Rss Function Button
	function rssFunctionButton($fnc_name = '', $text = 'Rss Feed', $title = 'Rss Feed', $alt = 'Rss Feed'){	
		
		$btn  = '<img src="'.BASE_BACKEND_URL .'/images/kms_toolsbar/apl_rss_feed.png" border="0" alt="'.$alt.'"/><br />'.$text;
		$link = '<div id="divButton"><a href="javascript:void(null);" onclick="'.$fnc_name.'" title="'.$title.'">'.$btn.'</a></div>';		
		return $link;		
	}
	
	/*****************************************************************************************************/
	function setLink($link, $btn, $title){
		return "<div id='divButton'><a href='".$link."' title='".$title."'>".$btn."</a></div>";
	}
	
	function setToolsbarLink( $link_set, $title_set = array() ){
		$this->link_set = $link_set;
		$this->title_set = $title_set;
	}
	
	function Toolsbar(){
	
//		$db = Database::getInstance();
//
//		$sqlPU = "
//			SELECT a.ActCode AS actioncode
//			FROM ".DB_PREFIX."user_permission_group up
//			LEFT JOIN ".DB_PREFIX."resources r ON up.ResID=r.ResID
//			LEFT JOIN ".DB_PREFIX."actions a ON up.ActID=a.ActID
//			WHERE up.Active = '1'
//			AND up.UsrGID = '".$_SESSION['session_admin_group']."'
//			ORDER BY a.Ordering ASC
//		";		
			
		//$rsPU = $db->GetArray( $sqlPU );		
		//for( $i = 0; $i < count( $this->link_set ); $i++){
		$i = 0;
		foreach($this->link_set as $a => $b){
			$rsPU[$i]['actioncode'] = $a;
			switch($rsPU[$i]['actioncode']):
				case "add":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::newButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "save":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::saveButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "edit":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::editButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "config":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::configButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "apply":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::applyButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "cancel":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::cancelButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "delete": 
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::deleteButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "remove": 
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::removeButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "reload":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::reloadButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "redo":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::redoButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "undo":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::undoButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "search":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::searchButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "printer":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::printerButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "help":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::helpButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "status":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::statsButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "publish":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::publishButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "unpublish":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::unpublishButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "sort":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::sortButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;	
				case "back":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::backButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "next":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::nextButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "send_mail":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::sendMailButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "copy_fnc":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::copyFunctionButton( $this->link_set[$rsPU[$i]['actioncode']], $this->title_set[$rsPU[$i]['actioncode']], $this->title_set[$rsPU[$i]['actioncode']], $this->title_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "save_fnc":
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::saveFunctionButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "cancel_fnc":	
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::cancelFunctionButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "rss_fnc":	
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::rssFunctionButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "save_exit":	
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::saveExitButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "save_new":	
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::saveNewButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
				case "excel_export":	
					if( $this->link_set[$rsPU[$i]['actioncode']] ){
						$tools_bar .= SCToolsbar::excelExportButton( $this->link_set[$rsPU[$i]['actioncode']] );
					}
				break;
			endswitch;
			$i++;
		}
		
		return $tools_bar;
	}
	
}

?>
