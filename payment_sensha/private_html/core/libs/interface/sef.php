<?php
    function SEF($link){ 
		if( APP_URL_REWRITE && WEB_PAGE != 1 ){			
			$link = URL::_($link);
		}
  		return $link;
  	}
	
	/**
	 * With BASE_URL
	 */
	function sUrl($link){ 
		$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
		return $base_url.'/'.$link;
	}
	
	class XURL{	
		function _($type = '', $_link = '', $_setting = ''){
			switch($type):
				case 'default':
					$url = BASE_URL.'/'.$_link;
					break;
				case 'uri':
					$url = SUB_DIR.'/index.php?'.$_SERVER['QUERY_STRING'];
					break;
				case 'admin':
					$url = BASE_BACKEND_URL."/".$_link;
					break;
				case 'redirect':
					URL::redirect(URL::sef($_link));
					break;
				case 'url':
					$url = URL::sef($_link, $_extension);
					break;
				case 'safe':
					$url = URL::safe($_link);
					break;
				case 'get':
					$customer_link = $_setting ? explode('?', $_setting) : '';
					$params = explode(',',trim($_link));
					foreach($params as $param){
						$strip_param[] = trim($param);	
					}
					
					$g_url = $customer_link[1] ? $customer_link[1] : $_SERVER['QUERY_STRING'];
					$g_char = array('=', '&');
					$g_link = str_replace( $g_char, '/', $g_url );									
					
					$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
					$g_url = $base_url."/index.php?";
					
					$g_viewcode = explode('/', $g_link);
					$g_viewcode = array_merge(array(''), $g_viewcode);
					
					$g_num_c = count($g_viewcode);
					for($j = 0; $j < $g_num_c; $j+=2) {
						if( !in_array($g_viewcode[$j+1], $strip_param ) ){
							$g_url .= ($j==0 ? '' : '&').$g_viewcode[$j+1].'='.$g_viewcode[$j+2];
						}
					}
					if( $g_url ){
						$g_url = str_replace( array('&='), '', $g_url);
						$url = $g_url;
					}
					break;
				default:				
					$link 		= $type;					
					$setting = $_link;					
					$url = URL::sef($link, $setting);
					break;
			endswitch;
			
			return $url;
		}	
		
		 function redirect($url) {
			if (headers_sent()) {
				echo "<script>document.location.href='".$url."';</script>\n";
			} else {
				@ob_end_clean(); // clear output buffer
				header( 'HTTP/1.1 301 Moved Permanently' );
				header( "Location: ". $url );
			}
			exit();
		 }
		 
		 function safe($string){
		 	$string = str_replace( array('/', '&', '"', '\'', ',', ' ', '<script', '</script', 'amp;'), '-', trim($string) );
			$string = str_replace('---', '-', $string);
			$string = str_replace('--', '-', $string);
			return $string;	 
		 }
		 
		 function sef($_link, $_setting = ''){
			
			$char = array('=', '&', '?');
			$_link = str_replace( $char, '/', BASE_URL.'/'.$_link );			
			
			return $_link;
			/*
			$_URL_SKIP_VARS = array();
			$param_merge = array();
			
			if( is_array($_setting) && count($_setting) > 0 ){
				foreach($_setting as $mode => $mode_value):
					if( $mode == 'skip' ){
						$_URL_SKIP_VARS = $mode_value;	
					}
					
					if( $mode == 'merge' ){
						$param_merge = $mode_value;
					}
				
					//echo $mode." : ".$mode_value."<br>";
					if( is_array($mode_value) && count($mode_value) > 0 ){
						foreach($mode_value as $type => $value):	
							//echo "&nbsp;&nbsp;&nbsp;&nbsp;".$type." : ".$value."<br>";
							
							if( $mode == 'replace' && URL_REWRITE == 1 ){
								$_link = str_replace($type, $value, $_link);
							}
						endforeach;
					}
				endforeach;
				//echo "<hr>";
			}
			
		 	$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
			if(URL_REWRITE == 1 && WEB_PAGE != 1){
				$_url_title = explode('&URL_TITLE=', $_link);
				if( count($_url_title) > 1 ){
					for($i = 0; $i < count($_url_title); $i++){						
						$_url_title[1] = str_replace( '&page=', 'APP_PAGE-', $_url_title[1]);
						$_url_title[1] = str_replace( array('/', '&', '"', '\'', ',', ' ', '<script', '</script'), '-', trim($_url_title[1]) );
						$_link = $_url_title[0].'&URL_TITLE='.$_url_title[1];
					}
				}
				else{
					$has_page_var = explode('&page=', $_link);
					if( count($has_page_var) > 0 ){
						$_link = str_replace( '&page=', 'APP_PAGE-', $_link);	
					}
				}
				//$_link = str_replace('&URL_TITLE=', ':', $_link);
			}
			$char = array('=', '&');
			$link = str_replace( $char, '/', $_link );			
			$link = str_replace(' ', '-', $link);
			
//			if( is_file(BASE_PATH.'/applications/app.'.getParam('app').'/router.php') ){
//				//include(BASE_PATH.'/applications/app.'.getParam('app').'/router.php');
//			}
//			if( count($URL_SKIP_VARS) > 0 && is_array($URL_SKIP_VARS) ){
//				$_URL_SKIP_VARS = $URL_SKIP_VARS;
//			}
//			else{
//				$_URL_SKIP_VARS = array();
//			}
			
			$sef_url = "";
			$viewcode = explode('/', $link);
			$viewcode = array_merge(array(''), $viewcode);
			$viewcode[1] = 'app';
		
			$url_sufix = array();
			$num_c = count($viewcode);
			for($i = 0; $i < $num_c; $i+=2) {
				${$viewcode[$i+1]}=$viewcode[$i+2];
				$slash = $i < $num_c-1 ? "/" : "";

				if($viewcode[$i+1] == 'lang'){
					$lang = ${$viewcode[$i+1]};
				}
				else{
					if( !in_array($viewcode[$i+1], $_URL_SKIP_VARS) ){
												
						if( in_array($viewcode[$i+1], $param_merge) ){
							$url_sufix[] = $viewcode[$i+2];
						}
												
						if( !in_array($viewcode[$i+1], $param_merge) ){
							$sef_url .= $slash.$viewcode[$i+2];
						}
					}				
				}
			}
			
			$sef_url .= (count($url_sufix) > 0 ? '/'.implode('-', $url_sufix) : '');
			
			if(URL_REWRITE == 1 && WEB_PAGE != 1){
				if( $lang == '' ){
					$lang = getParam('lang') ? getParam('lang') : $_SESSION['SYS_Lang'];
				}
				$char1 		= array(' ', '.','--');
				$char2 		= array('\'', '"', '>', '<', '!', '@', '$', '%', '^', '&', '*', '=', '?');
				$sef_url 	= str_replace( $char1, '-', $sef_url );
				$sef_url 	= str_replace( $char2, '', $sef_url );
				$sef_url 	= str_replace( ':', '/', $sef_url );
				$sef_url 	= str_replace( 'APP_PAGE-', '/page-', $sef_url );
				
				$url = $base_url.(($lang && $lang != GetDefine('DEFAULT_LANGUAGE')) ? '/'.$lang : '' ).$sef_url.(SEF_URL_EXTENSION ? '.'.SEF_URL_EXTENSION : '');
				$url = str_replace('/.'.SEF_URL_EXTENSION, '.'.SEF_URL_EXTENSION, $url);
			}
			else{
				$url = $base_url.'/'.$_link;
			}
			
			return $url;
			*/
		 }
		 
		 function set_content_media($value){
			 $value = str_replace(BASE_URL, '{{$BASE_URL}}' , $value);
			 return $value;
		 }
		 
		 function get_content_media($value){
			 $value = str_replace('{{$BASE_URL}}', BASE_URL , $value);
			 return $value;
		 }

		function Bitly($url){
			$bitly = new bitly(BITLY_LOGIN, BITLY_APIKEY);
			return $bitly->shorten($url);		
		}
		
	}	
?>
