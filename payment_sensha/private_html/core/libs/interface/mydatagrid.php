<?php
class MyDataGrid{
	var $datagird ;
	var $sql;
	var $pk;
	var $db;
	var $table;
	var $data_set;
	var $row_total;
	var $app;
	var $perpage;
	var $link_paging;
	var $paging_param_expect;
	var $getpage;
	var $fields = array();
	var $fields_text = array();
	var $fields_width = array();
	var $fields_align = array();
	var $fields_sort = array();
	var $fields_data_type = array();
	var $fields_link = array();
	var $fields_link_attr = array();
	var $header_sort_fields = array();
	var $header_sort_fields_link = array();
	var $link_extra;
	var $link_publish;
	var $link_unpublish;
	var $link_featured;
	var $link_unfeatured;
	var $link_ordering_up;
	var $link_ordering_down;
	var $link_edit;
	var $link_delete;
	var $link_permission;
	var $link_param;
	var $link_subfix;
	var $link_detail;
	var $tools_button_left;
	var $tools_button_right;
	var $title;
	var $hidden_param;
	var $text_crop_count = 80;
	var $js_select_item_function_name;
	var $js_select_item_function_params = array();
	var $js_select_item_function_params_prefix = array();
	
	var $dis_column = 0;
	
	var $can_ordering = false;
	var $can_ordering_click = true;
	var $can_ordering_dragable = true;
	var $can_permission = false;
	var $can_publish = false;
	var $can_edit = true;
	var $can_delete = true;
	var $can_detail = false;	
	var $can_detail_modal = false;
	var $can_checkbox = true;
	var $can_loop_number = true;
	var $can_second_row = false;
	var $can_extra = false;
	var $can_language = true;
	var $can_search = true;
	var $can_search_tools = true;
	var $can_toolsbar = true;
	var $second_row;	
	var $url_sufix = array();
	var $datagrid_url;
	var $multi_languages = false;
	var $header_sortable_fileds = array();

	function MyDataGrid($db){
		$this->db = $db;		
	}
	
	function render(){
		$this->datagrid_url = BASE_URL_ADMIN.'/index.php?app='.getParam('app');
		$_param_strip = array();

		if( getParam('filter_search') ){			
			$this->datagrid_url .= '&filter_search='.getParam('filter_search');
		}
		if( getParam('limit') ){
			$this->datagrid_url .= '&limit='.getParam('limit');
		}
		if( getParam('page') ){
			$this->datagrid_url .= '&page='.getParam('page');
		}
		if( is_array($this->url_sufix) && count($this->url_sufix) > 0 ){
			foreach($this->url_sufix as $subfix){
				if( getParam($subfix) ){
					$this->datagrid_url .= '&'.$subfix.'='.getParam($subfix);
				}
			}
		}		
		
		if( $this->data_set ){
			$rs = $this->data_set;	
		}
		else{
			$rs = $this->db->GetArray( $this->sql );
		}
	 
		/*  Paging */
		$paging = new KMSPaging($this->row_total, $this->perpage,  getParam('page'),  URL::_('get', 'page'));
		
		$body = '';
		$body .= '<tbody>';
		
		$dg_start = $this->data_set ? $paging->getStartPage() : 0;
		$dg_per_page = $this->data_set ? $paging->getPerPage() : $this->perpage;
		$dg_total = $this->data_set ? $paging->getTotalRecord() : $this->row_total;

//echo "<hr>";
		$i_row = 0;//$dg_start;
		for($i = $dg_start; $i < $dg_per_page && $i < $dg_total; $i++){
			
//echo $rs[$i_row]['title'];
//echo " : ";
//echo $i_row;
//echo "<br>";
			$row_tools = array();
			
			$rowCheckbox[$i_row] 	= App_HTML::getCheckboxList( $i_row, $rs[$i_row][$this->pk] ); 
			$rowColor[$i_row] 		= $i_row%2==0 ? "#F3F8FB" : "";
			
			$rowPublish[$i_row] 		= $rs[$i_row]['Publish'] == '1' 
												 ? $this->dataTools('publish_y', SEF( $this->replaceLoopParam(URL::_('get').'&fnc=unpublish&'.$this->pk.'={0}', $rs[$i_row]) ) ) 
												 : $this->dataTools('publish_n', SEF( $this->replaceLoopParam(URL::_('get').'&fnc=publish&'.$this->pk.'={0}', $rs[$i_row]) ) );	
												
		
			$rowOrdering[$i_row] 	= $this->setOrdering(
													$i_row, 
													SEF( $this->replaceLoopParam(URL::_('get').'&fnc=ordering_up&'.$this->pk.'={0}', $rs[$i_row]) ),
													SEF( $this->replaceLoopParam(URL::_('get').'&fnc=ordering_down&'.$this->pk.'={0}', $rs[$i_row]) ),
													$dg_total
													);		
			if($this->can_extra){				
				$row_tools[$i_row][] = $this->dataTools('extra', SEF( $this->replaceLoopParam($this->link_extra, $rs[$i_row]) ), $rs[$i_row][$this->pk] );
			}
			
			if($this->can_featured){
				$row_tools[$i_row][] = $rs[$i_row]['featured'] == '1' 
 								? $this->dataTools('featured_y', SEF( $this->replaceLoopParam($this->link_featured, $rs[$i_row]) ) ) 
								: $this->dataTools('featured_n', SEF( $this->replaceLoopParam($this->link_unfeatured, $rs[$i_row]) ) );	
			}
			
			if($this->can_detail){				
				$row_tools[$i_row][] = $this->dataTools('detail', SEF( $this->replaceLoopParam($this->link_detail, $rs[$i_row]) ) );
			}
			
			if($this->can_edit){								
				$row_tools[$i_row][] = $this->dataTools('edit', SEF( $this->replaceLoopParam(URL::_('get').'&fnc=form&'.$this->pk.'={0}', $rs[$i_row]) ) );				
			}
			
			if($this->can_delete){
				$row_tools[$i_row][] = $this->dataTools('delete', SEF( $this->replaceLoopParam(URL::_('get').'&fnc=delete&'.$this->pk.'={0}', $rs[$i_row]) ) );
			}
			
			if($this->can_permission){
				$row_tools[$i_row][] = $this->dataTools('permission', SEF( $this->replaceLoopParam($this->link_permission, $rs[$i_row]) ) );
			}

			if(count($row_tools[$i_row])==0)
				$row_tools[$i_row][] = '';
			
			$rowTools[$i_row] = implode('', $row_tools[$i_row]);
			
			$row_class = ($i_row%2 == 0) ? 'class="row1"' : 'class="row0"';			
			$body .= '<tr '.$row_class.' id="recordsArray_'.$rs[$i_row][$this->pk].'">';
				if( $this->data_set ){
					$row_number = ($i+1);
				}
				else{
					if(getParam('page') <= 1){
						$row_number = ($i+1);
					}
					else{
						$row_number = ($this->perpage*(getParam('page')-1))+($i+1);
					}
				}
				
				if( $this->can_loop_number ){
					$body .= '<td align="right" valign="top"><b id="col_number_'.$rs[$i_row][$this->pk].'">'.($row_number).'</b></td>';
				}
				
				if( $this->can_checkbox ){
					$body .= '<td align="center" valign="top">'.($this->can_checkbox ? $rowCheckbox[$i_row] : '').'</td>';
				}
				
				if( $this->can_js_select_item ){
					
					if( is_array($this->js_select_item_function_params) && count($this->js_select_item_function_params) > 0 ){
						$_js_params_val = array();
						$_js_params_form_field = array();
						foreach($this->js_select_item_function_params as $form_pf => $db_pf){
							$_js_params_val[] = $rs[$i_row][$db_pf];
							if( strlen($form_pf) > 1){
								$_js_params_form_field[] = $form_pf;
							}
						}
						
						if( count($_js_params_form_field) > 0 ){
							$_js_params_val = array_merge($_js_params_val, $_js_params_form_field);
						}
						
						$js_params = @implode("','", $_js_params_val);
						$js_params = "'".$js_params."'";
					}
					
					if( is_array($this->js_select_item_function_params_prefix) && count($this->js_select_item_function_params_prefix) > 0 ){
						$_js_params_prefix_val = array();
						foreach($this->js_select_item_function_params_prefix as $pf){
							$_js_params_prefix_val[] = $pf;
						}
						
						$js_params_prefix = @implode("','", $_js_params_prefix_val);
						$js_params_prefix = ",'".$js_params_prefix."'";
					}
					
					$body .= '<td align="center" valign="top">';
					if( $this->can_js_select_single_item ){
						$body .= '<a href="javascript:void(null);" id="dg_select_item_'.$rs[$i_row][$this->pk].'" class="dg_select_item" onclick="'.$this->js_select_item_function_name.'('.$js_params.$js_params_prefix.');">เลือก</a>';
					}
					else{
						$body .= '<input type="checkbox" class="dg_select_item" name="dg_select_item" id="dg_select_item_'.$i_row.'" value="'.$rs[$i_row][$this->pk].'">';
					}
					$body .= '</td>';
					
				}
								
				for($j = 0; $j < count($this->fields); $j++){
					$body .= '<td align="'.$this->fields_align[$j].'" valign="top">';
					if($this->fields_link[$j]){
						$body .= '<a href="'. $this->replaceLoopParam($this->fields_link[$j], $rs[$i_row]).'" title="Click to edit" '.$this->fields_link_attr[$j].'>';
						$body .= $this->getDataType($this->fields_data_type[$j], $rs[$i_row][$this->fields[$j]]);
						$body .= '</a>';
					}
					else{
						$body .= $this->getDataType($this->fields_data_type[$j], $rs[$i_row][$this->fields[$j]]);
					}
					if($this->second_row[0] == $this->fields[$j] && $this->can_second_row == true){
						$body .= '<div>';
						
						$second_sql = str_replace('{0}', $rs[$i_row][$this->pk], $this->second_row[1]);
						$srs = $this->db->GetArray($second_sql);
						
						for($si = 0; $si < count($srs); $si++){
							
							$body .= '<a href="'.$this->replaceLoopParam($this->second_row[2], $srs[$si]).'">'.$srs[$si]['Title'].'</a>';
						
							if($si != count($srs)-1){
								$body .= ', ';
							}
						}						
						
						$body .= '</div>';
					}
					
					$body .= '</td>';
				}

				$db_tools[$i_row] = array();
				if($this->can_extra){		
					$dg_tools[$i_row][] = $this->dataTools('extra', SEF( $this->replaceLoopParam($this->link_extra, $rs[$i_row]) ), $rs[$i_row][$this->pk], _DG_Tools_Extra );
				}								
				if($this->can_featured){
					$dg_tools[$i_row][] = $rs[$i_row]['Featured'] == '1' 
							? $this->dataTools('featured_y', SEF( $this->replaceLoopParam($this->link_featured, $rs[$i_row]) ), '', _DG_Tools_Featured ) 
							: $this->dataTools('featured_n', SEF( $this->replaceLoopParam($this->link_unfeatured, $rs[$i_row]) ), '', _DG_Tools_Featured );
				}								
				if($this->can_detail){	
					$dg_tools[$i_row][] = $this->dataTools('detail', SEF( $this->replaceLoopParam($this->link_detail, $rs[$i_row]) ), '', _DG_Tools_Detail );
				}
				if($this->can_edit){		
					$dg_tools[$i_row][] = $this->dataTools('edit', SEF( $this->replaceLoopParam($this->link_edit, $rs[$i_row]) ), '', _DG_Tools_Edit);
				}								
				if($this->can_delete){	
					$dg_tools[$i_row][] = $this->dataTools('delete', SEF( $this->replaceLoopParam($this->link_delete, $rs[$i_row]) ), '', _DG_Tools_Delete );
				}								
				if($this->can_permission){
					$dg_tools[$i_row][] = $this->dataTools('permission', SEF( $this->replaceLoopParam($this->link_permission, $rs[$i_row]) ), '', _DG_Tools_Permission);
				}
				
				$data_tools = array();
				if( $this->can_edit ){
					
				}
				if($this->can_delete || $this->can_edit || $this->can_delete || $this->can_publish || $this->can_detail || $this->can_permission){
					$body .= '<td align="center" valign="top">';
					$body .= '<span class="btn-group">';
					$body .= $rowTools[$i_row];
					if( $this->can_publish ){
						//$body .= '&nbsp;&nbsp;|&nbsp;&nbsp;';
						$body .= $rowPublish[$i_row];
					}
					$body .= '</span>';
					$body .= '</td>';
				}	
				
				if($this->can_publish){
					//$body .= '<td align="center" valign="top">'.$rowPublish[$i].'</td>';				
				}
				
				if($this->can_ordering){
					$body .= '<td align="center" valign="top">';
					if( $this->can_ordering_click ){
						$body .= $rowOrdering[$i_row];
					}
					
					if($this->can_ordering_dragable){
						$body .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="'.BASE_URL_ADMIN.'/images/drag_icon.png" class="btn_dragable" alt="drag to ordering" title="drag to ordering" style="cusor:pointer;" id="'.$rs[$i_row][$this->pk].'"> ';		
					}
					
					$body .= '</td>';
				}
				
			$body .= '</tr>';			
		$i_row++;
		}
		$body .= '</tbody>';
		$this->datagrid = $this->setHeader($paging->getTotalRecord());
		$this->datagrid .= $body;
		$this->datagrid .= $this->setFooter($paging->paging(), sprintf(DG_TOTAL_RECORD_TOTAL_PAGE, $paging->getTotalRecord(), $paging->total_page));
		
	}
	
	/** field, text, width, align, sort , data type*/
	function column($fields, $fields_text, $fields_width, $fields_align, $fields_sort = false, $fields_data_type = '', $fields_link = '', $fields_link_attr = ''){
		array_push($this->fields, $fields);
		array_push($this->fields_text, $fields_text);
		array_push($this->fields_width, $fields_width);
		array_push($this->fields_align, $fields_align);
		array_push($this->fields_sort, $fields_sort);
		array_push($this->fields_data_type, $fields_data_type);
		array_push($this->fields_link, $fields_link);
		array_push($this->fields_link_attr, $fields_link_attr);
	}
	
	function header_sort($fields){
		if( is_array($fields) && count($fields) > 0 ){
			foreach($fields as $field){
				array_push($this->header_sort_fields, $field);
			}
		}
		$this->header_sortable_fileds = $fields;
	}
	
	function setHeader($total_record){
		$app = getParam('app');
		$class = class_exists('Toolsbar') ? 'Toolsbar' : 'TB_'.ucfirst($app);
		
		$toolsbar = new $class();
		$header = '';
		
		$header .= '
		<script type="text/javascript">
		$(document).ready(function(){ 
								   
			$(".adminlist tbody").sortable({
				opacity: 0.6, 
				cursor: "move",
				handle: ".btn_dragable",
				update: function(){
					var order = $(this).sortable("serialize") + "&action=updateRecordsListings&db_table='.$this->table.'&db_pk='.$this->pk.'&limit="+$("#limit").val()+"&page='.(param('page') ? param('page') : 1 ).'"; 
					$.post(
						"'.BASE_URL_ADMIN.'/index.php?app=core&fnc=update_ordering", 
						order, 
						function(theResponse){
							//$("#debug").html(theResponse);	
							var o_json = $.parseJSON(theResponse);
							var debug = "";
							for(i = 0; i < o_json.length; i++){
								$("#col_number_"+o_json[i].id).html(o_json[i].number);
								$("#recordsArray_"+o_json[i].id).removeClass();
								$("#recordsArray_"+o_json[i].id).addClass(o_json[i].class);
							}
						}
					); 															 
				},
				stop: function(){
					//alert($(".drag_icon").click().attr("id"));
					//$(this).fadeIn(500);$(this).fadeOut(8000);
				}
			});
		
			$("#btn_search_reset").click(function(){
				$("#filter_search_box").html(\'<input type="text" name="filter_search" id="filter_search" value="" title="" />\');
			});
		});	
		</script>
		';
			if($this->can_toolsbar){
			$header .= '
			<div class="mws-panel-toolbar" style="margin:0 -1px;">
				<div class="btn-toolbar">
					<div class="btn-group">
						'.$toolsbar->load().'
					</div>
				</div>
			</div>
			';
			}
			
			if( $this->multi_languages ){
				$form_lang = param('form_lang') ? param('form_lang') : GetLangID();
				
				$lang_sql = "SELECT * FROM ".getTableName('languages')." WHERE active = 1 AND publish = 1 ORDER BY ordering ASC";
				$lang_list = $this->db->GetArray($lang_sql);
				if( count($lang_list) > 1 ){
				 $header .= '<ul id="form_tab" class="shadetabs">';
					for($i = 0; $i < count($lang_list); $i++){ 
						 $header .= '<li style="margin-right:5px;"><a href="'.URL::_('get', 'form_lang').'&form_lang='.$lang_list[$i]['lang_id'].'" '.($lang_list[$i]['lang_id'] == $form_lang ? ' class="selected"' : '').'><img src="'.BASE_URL.'/images/lang/'.$lang_list[$i]['code'].'.png" align="absbottom" style="margin-right:5px;" />'.$lang_list[$i]['title'].'</a></li>';
					} 
				 $header .= '</ul>';
				}
			}
				$header .= '
				  ';
				  
                    
				  $header .= '
				';
				/*
				if( $this->multi_languages ){
				
					$lang_model = Model("languages");
					if( getParam('clang_id') ){
						$lang_where = "AND lang_id <> '".getParam('clang_id')."' ";
					}
					$rs_lang = array();
					
					$o_lang = new DBTable("languages", "lang_id");
					$lang = $o_lang->select(getParam('clang_id'));
					
					if( count($rs_lang) > 0 ){
						$lang_link = '<ul id="nav-lang">';
							if( !getParam('clang_id')){
								$lang_link .= '<li><a href="javascript:void(null);">แสดงข้อมูลทุกภาษา</a>';
							}
							else{
								$lang_link .= '<li><a href="javascript:void(null);"><span>แสดงข้อมูลเฉพาะภาษา <img src="'.BASE_URL.'/images/lang/'.$lang['icon'].'" border="0" align="absmiddle" style="float:none;">'.$lang['title'].'</span></a>';
							}
							$lang_link .= '<ul>';
							
							if( getParam('clang_id')){
								$lang_link .= '<li><a href="'.URL::_('get', 'clang_id').'">แสดงข้อมูลทุกภาษา</a></li>';
							}
							
							for( $i = 0; $i < count($rs_lang); $i++){
								$lang_link .= '					
								<li><a href="'.URL::_('get', 'clang_id').'&clang_id='.$rs_lang[$i]['lang_id'].'"><img src="'.BASE_URL.'/images/lang/'.$rs_lang[$i]['icon'].'" border="0" align="absmiddle"> แสดงข้อมูลเฉพาะภาษา '.$rs_lang[$i]['title'].'</a></li>
								';
							}
					
							$lang_link .= '</ul>';
							$lang_link .= '</li>';
						$lang_link .= '</ul>';
					}
				
				}
				*/
				
				if($this->can_search){
					$header .= '
					<div id="datagrid_length" class="dataTables_length" style="width:50%; height:25px;">						<form id="frmAdminPaging2Go" name="frmAdminPaging2Go" method="get" action="">
							'.$this->set_hidden_param(array('limit', 'page')).'
							แสดงหน้าละ
							<select id="limit" name="limit" class="inputbox" size="1" onchange="document.forms[\'frmAdminPaging2Go\'].submit();" style="text-align:center;">
								<option value="5" '.($this->perpage == 5 ? 'selected="selected"' : '').'>5</option>
								<option value="10" '.($this->perpage == 10 ? 'selected="selected"' : '').'>10</option>
								<option value="20" '.($this->perpage == 20 ? 'selected="selected"' : '').'>20</option>
								<option value="30" '.($this->perpage == 30 ? 'selected="selected"' : '').'>30</option>
								<option value="50" '.($this->perpage == 50 ? 'selected="selected"' : '').'>50</option>
								<option value="100" '.($this->perpage == 100 ? 'selected="selected"' : '').'>100</option>
								<option value="'.$this->row_total.'" '.($this->perpage == $this->row_total ? 'selected="selected"' : '').'>'.DG_ALL.'</option>
							</select>
							รายการ
						</form>
					</div>
					<div class="dataTables_filter" id="datagrid_filter">
					  	<form id="frmAdminSearchLeft" name="frmAdminSearchLeft" method="get" action="">				  		
							<label>'.DG_FILTER.':
								<input type="text" name="filter_search" id="filter_search" value="'.(getParam('filter_search') ? getParam('filter_search') : '').'" aria-controls="datagrid" />
							</label>
							<button type="submit" class="btn  btn-small">'.DG_SEARCH.'</button>
							'.$this->set_hidden_param($this->url_sufix).'
							'.$this->tools_button_right.'
						</form>	
					</div>
					';
				}
				
//		//$header .= '<h2 class="app_header">'.$this->title.'</h2>';
//		$header .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">';
//			$header .= '<tr>';
//				$header .= '<td width="50%" align="left">';
//				//$header .= $toolsbar->load();
//				$header .= '</td>';
//				$header .= '<td width="50%" align="right">';
//				$header .= $this->tools_button_right;
//				$header .= '</td>';
//			$header .= '<tr>';	
//		$header .= '</table>';

		$header .= '<form id="adminForm" name="adminForm" method="post" action="index.php'.$this->url_sufix.'">';
		//$header .= '<table id="datagrid" class="mws-datatable-fn mws-table dataTable" aria-describedby="datagrid_info">';
		$header .= '<table class="table table-hover table-nomargin table-bordered dataTable dataTable-nosort" data-nosort="0">';
			$header .= '<thead>';
			$header .= '<tr role="thefilter">';
			
				if($this->can_loop_number){
				$header .= '<th width="2%" align="left">&nbsp;</th>';
				}
				else{
					$this->dis_column += 1;
				}
				
				if($this->can_checkbox){
				//$header .= '<th width="2%" align="center">'.($this->can_checkbox ? '<input type="checkbox" name="toggle" value="" onclick="checkAll(this);" />' : '').'</th>';
				$header .= '<th class="with-checkbox"><input type="checkbox" name="check_all" id="check_all"></th>';
				}
				else{
					$this->dis_column += 1;
				}
				
				if($this->can_js_select_item){
					
					if( is_array($this->js_select_item_function_params) && count($this->js_select_item_function_params) > 0 ){
						$_js_params_val = array();
						foreach($this->js_select_item_function_params as $pf){
							$_js_params_val[] = $rs[$i_row][$pf];
						}
						
						$js_params = @implode("','", $_js_params_val);
						$js_params = "'".$js_params."'";
					}
					
					if( is_array($this->js_select_item_function_params_prefix) && count($this->js_select_item_function_params_prefix) > 0 ){
						$_js_params_prefix_val = array();
						foreach($this->js_select_item_function_params_prefix as $pf){
							$_js_params_prefix_val[] = $pf;
						}
						
						$js_params_prefix = @implode("','", $_js_params_prefix_val);
						$js_params_prefix = ",'".$js_params_prefix."'";
					}
					
					$header .= '<th width="5%" align="center">&nbsp;';
					if( !$this->can_js_select_single_item ){
						$header .= '<input type="checkbox" name="toggle" id="toggle" value="" onclick="jqCheckAll(\'adminForm\', \'dg_select_item\', \'toggle\');" />';
					}
					$header .= '</th>';
				}
				else{
					$this->dis_column += 1;
				}
				
				for($i = 0; $i < count($this->fields); $i++){
					if( param('ORDER_BY') == 'ASC' ){
						$sorting_class .= 'sorting_asc';	
					}
					elseif( param('ORDER_BY') == 'DESC' ){
						$sorting_class .= 'sorting_desc';	
					}	
					else{
						$sorting_class .= 'sorting';	
					}					
					$header .= '<th align="'.$this->fields_align[$i].'" '.($this->fields_width[$i] ? 'width="'.$this->fields_width[$i].'"' : '').' class="'.$sorting_class.'">';					
					//if( $this->header_sort_fields[$i] ){
					   if( in_array( $this->fields[$i], $this->header_sortable_fileds ) ){ 
						
//						if( $this->paging_param_expect ){
//							$_arr_url_sufix = explode(',',$this->paging_param_expect);
//							foreach($_arr_url_sufix as $url){
//								if( getParam(trim($url)) != '' ){
//									$_url_sufx = '&'.trim($url).'='.getParam(trim($url));	
//								}
//							}
//							$_paging_param_expect = $this->paging_param_expect.', sort_field, ORDER_BY';
//						}
//						else{
//							$_paging_param_expect = 'sort_field, ORDER_BY';
//						}
						
						$sort_link = URL::_('get', 'sort_field,ORDER_BY')."&sort_field=".$this->fields[$i];//$this->datagrid_url."&sort_field=".$this->fields[$i];

						if( getParam('ORDER_BY') == 'ASC' ){
							$sort_link .= '&ORDER_BY=DESC';	
						}
						else{
							$sort_link .= '&ORDER_BY=ASC';	
						}					

						$header .= '<a href="'.$sort_link.'">';
						$header .= $this->fields_text[$i];
						if( getParam('sort_field') && getParam('ORDER_BY') && getParam('sort_field') == $this->fields[$i] ){							
							if( getParam('ORDER_BY') == 'ASC' ){
								//$header .= ' <img src="'.BASE_URL.'/libraries/jcore/system/images/sort_asc.png" alt="" border="0"  />';
							}
							if( getParam('ORDER_BY') == 'DESC' ){
								//$header .= ' <img src="'.BASE_URL.'/libraries/jcore/system/images/sort_desc.png" alt="" border="0"  />';
							}									
						}
						$header .= '</a>';		
						if( getParam('sort_field') && getParam('ORDER_BY') && getParam('sort_field') == $this->fields[$i] ){
							$sort_link = str_replace('&sort_field='.getParam('sort_field'), '', $sort_link);
							$sort_link = str_replace('&ORDER_BY=ASC', '', $sort_link);
							$sort_link = str_replace('&ORDER_BY=DESC', '', $sort_link);
							$header .= ' | <a href="'.$sort_link.'">';
							$header .= 'Reset';		
							$header .= '</a>';
						}
						
					}
					else{
						$header .= $this->fields_text[$i];
					}
					
					$header .= '</th>';
				}
				
				if($this->can_detail || $this->can_edit || $this->can_delete){
					$header .= '<th width="15%" align="center">'.TOOLS.'</th>';
				}
				else{
					$this->dis_column += 1;
				}
				
				if($this->can_publish){
					//$header .= '<th width="8%" align="center">'._DG_Publish.'</th>';				
				}
				else{
					//$this->dis_column += 1;
				}
				
				if($this->can_ordering){
					$header .= '<th width="8%" align="center">'.ORDERING.'</th>';					
				}
				else{
					$this->dis_column += 1;
				}
				
			$header .= '</tr>';
			$header .= '</thead>';
			
		return $header;
	}
	
	function setFooter($paging_left = '', $paging_right = '', $footer_text = ''){
		$total = count($this->fields) + 5 - $this->dis_column;
		$colspan_left 		= ceil($total / 2);		
		$colspan_right 	= $total - $colspan_left;
		$footer .= '<input type="hidden" name="is_publish"	 	id="is_publish"		value="" />';
		$footer .= '<input type="hidden" name="unpublish" 	id="unpublish"	value="" />';
		$footer .= '<input type="hidden" name="deletion"		id="deletion"		value=""/>';
		$footer .= '<input type="hidden" name="remove_able"		id="remove_able"		value=""/>';
		$footer .= '<input type="hidden" name="edition"		id="edition"		value=""/>	';
		$footer .= '<input type="hidden" name="save"			id="save"			value=""/>	';
		$footer .= '<input type="hidden" name="apply"			id="apply"			value=""/>';	
		$footer .= '<input type="hidden" name="sort"			id="sort"			value=""/>	';
		$footer .= '<input type="hidden" name="id2"			id="id2"				value="{{$ID2}}"/>';	
		$footer .= '<input type="hidden" name="cbvalue" 		id="cbvalue" 		value="0" />';
		$footer .= '<input type="hidden" name="submition"	id="submition"	value=""/>	';
		$footer .= '<input type="hidden" name="hidden_param"		id="hidden_param"	value="'.$this->hidden_param.'"/>	';
		$footer .= '</form>';
		$footer .= '</table>';
		$footer .= '<div class="dataTables_info" id="datagrid_info">'.$paging_right.'</div>';
		$footer .= $paging_left;
		
		return $footer;
	}
	
	function setting($value = array()){
		
		$this->sql 							= $value['db.sql'];
		$this->data_set						= $value['db.data_set'];
		$this->pk 							= $value['db.pk'] ? $value['db.pk'] : Database::get_pk_name($value['db.table']);
		//$value['db.pk'];
		$this->table 						= $value['db.table'];
		$this->row_total					= $value['db.row_total'];
		$this->app 							= getParam('app');
		//$value['app'];
		$this->title						= $value['title'];
		$this->perpage 						= $value['paging.perpage'] ? $value['paging.perpage'] : 20;
		//$this->getpage 					= $value['paging.page'];
		$this->link_paging 					= $value['paging.link'];
		$this->url_sufix					= $value['paging.url_sufix'];
		//$this->paging_param_expect= $value['paging.page.param.expect'];
		//$this->link_publish				= $value['tools.publish.link_publish'];
		//$this->link_unpublish				= $value['tools.publish.link_unpublish'];
		$this->link_featured				= $value['tools.featured.link'];
		$this->link_unfeatured				= $value['tools.unfeatured.link'];
		//$this->link_ordering_up 			= $value['tools.ordering.link_up'];
		//$this->link_ordering_down			= $value['tools.ordering.link_down'];
		$this->link_extra 					= $value['tools.extra.link'];
		$this->link_detail 					= $value['tools.detail.link'];
		//$this->link_edit 					= $value['tools.edit.link'];
		//$this->link_delete 				= $value['tools.delete.link'];
		$this->link_permission				= $value['tools.permission.link'];
		$this->link_row_param 				= $value['tools.link_row_param'];
		$this->link_subfix	 				= $value['tools.subfix.link'];
		$this->tools_button_left 			= $value['tools.button_left'];
		$this->tools_button_right			= $value['tools.button_right'];		
		$this->second_row					= $value['second.row'];
		$this->js_select_item_function_name = $value['js_select_item_function_name'];
		$this->js_select_item_function_params = $value['js_select_item_function_params'];
		$this->js_select_item_function_params_prefix = $value['js_select_item_function_params_prefix'];
	}
	
	function replaceLoopParam($source, $rs){
		$source = str_replace('{0}', $rs[$this->link_row_param[0]], $source);
		$source = str_replace('{1}', $rs[$this->link_row_param[1]], $source);
		$source = str_replace('{2}', $rs[$this->link_row_param[2]], $source);
		$source = str_replace('{3}', $rs[$this->link_row_param[3]], $source);
		$source = str_replace('{4}', $rs[$this->link_row_param[4]], $source);
		return $source;
	}
	
	function getDataGrid(){
		$this->render();
		return $this->datagrid;
	}
	
	function showDataGrid(){
		$this->render();
		echo $this->datagrid;
	}
	
	function dataTools( $type, $link, $id = '', $text = '' ){
		
		switch( $type ):
			case "extra":
				$tools = '<span id="datagrid_extra_icon'.($id ? '_'.$id : '').'"><a href="'.$link.'" ';
				$tools .= 'title="'.(defined('_DG_Tools_Extra_Plus') ? _DG_Tools_Extra_Plus : _DG_Tools_Extra).'"';
				$tools .= '>';
				$tools .= '<img src="'.BASE_URL_ADMIN.'/images/icon/'.(defined('_DG_Tools_Extra_Icon') ? _DG_Tools_Extra_Icon : 'list.png').'"border="0" alt="'.(defined('_DG_Tools_Extra_Plus') ? _DG_Tools_Extra_Plus : _DG_Tools_Extra).'" title="'.(defined('_DG_Tools_Extra_Plus') ? _DG_Tools_Extra_Plus : _DG_Tools_Extra).'" />&nbsp;'.$text.'</a>';
				$tools .= '</span>';
				break;
			case "detail":
				//<a href="index.php?app=news&fnc=search_relate_news" title="xxx" onclick="Modalbox.show(this.href, {title: this.title, width: 700, height: 400}); return false;">xxx</a>
				$tools = '<a href="'.$link.'"';
				if($this->can_detail_modal){
					$tools .= 'title="'._DG_Tools_Detail.'" onclick="Modalbox.show(this.href, {title: this.title, width: 700, height: 400}); return false;"';
				}
				else{
					$tools .= 'title="'.(defined('_DG_Tools_Detail_Plus') ? _DG_Tools_Detail_Plus : _DG_Tools_Detail).'"';
				}
				$tools .= '>';
				$tools .= '<img src="'.BASE_URL_ADMIN.'/images/icon/'.(defined('_DG_Tools_Detail_Icon') ? _DG_Tools_Detail_Icon : 'list.png').'"border="0" alt="'.(defined('_DG_Tools_Detail_Plus') ? _DG_Tools_Detail_Plus : _DG_Tools_Detail).'" title="'.(defined('_DG_Tools_Detail_Plus') ? _DG_Tools_Detail_Plus : _DG_Tools_Detail).'" />&nbsp;'.$text.'</a>';
				break;
			case "edit":
				$tools = '<a href="'.$link.'"class="btn btn-small" title="edit"><i class="icon-pencil"></i></a>';
				break;
			case "delete":
				$tools = '<a href="'.$link.'" onClick="return confirm(\''._DG_Tools_Delete_Confirm.'\');" class="btn btn-small" title="delete"><i class="icol-delete"></i></a>';
				break;
			case "publish_y":
				$tools = '<a href="'.$link.'" class="btn btn-small" title="publish"><i class="icol-connect"></i></a>';
				break;
			case "publish_n":
				$tools = '<a href="'.$link.'" class="btn btn-small" title="unpublish"><i class="icol-disconnect"></i></a>';
				break;
			case "featured_y":
				$tools = '<a href="'.$link.'"><img src="'.BASE_URL_ADMIN.'/images/icon/thumb_up.png"border="0" alt="'._DG_Tools_Featured.'" title="'._DG_Tools_Featured.'" align="absmiddle"/></a>';
				break;
			case "featured_n":
				$tools = '<a href="'.$link.'"><img src="'.BASE_URL_ADMIN.'/images/icon/thumb_down.png"border="0" alt="'._DG_Tools_UnFeatured.'" title="'._DG_Tools_UnFeatured.'"/></a>';
				break;
			case "permission":
				$tools = '<a href="'.$link.'"><img src="'.BASE_URL_ADMIN.'/images/icon/permission.png"border="0" alt="'._DG_Tools_Permission.'" title="'._DG_Tools_Permission.'" /></a>';
				break;
			default:
				$tools = "";
				break;
		endswitch;
		return $tools;
	}
	
	function setOrdering($loopNumber, $linkUp, $linkDown, $numRows){
		
		$tools = "";
		
		if( !in_array(getParam('app'), array('categories')) ){
			$linkUp = str_replace('app='.$this->app, 'app=core', $linkUp);
			$linkUp .= '&tbl='.$this->table;	
			
			$linkDown = str_replace('app='.$this->app, 'app=core', $linkDown);
			$linkDown .= '&tbl='.$this->table;	
		}
		
		if( $numRows == 1	 ) {
			$tools .= "";
		}
		elseif($loopNumber == 0){
			$tools .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$tools .= '<a href="'.$linkDown.'"><img src="'.BASE_URL_ADMIN.'/images/icon/arrow_down.png"border="0" alt="'._DG_Ordering_Down.'" /></a>';
		}
		elseif($loopNumber == ($numRows - 1)){
			$tools .= '<a href="'.$linkUp.'"><img src="'.BASE_URL_ADMIN.'/images/icon/arrow_up.png"border="0" alt="'._DG_Ordering_Up.'" /></a>';
			$tools .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		else{
			$tools .= '<a href="'.$linkUp.'"><img src="'.BASE_URL_ADMIN.'/images/icon/arrow_up.png"border="0" alt="'._DG_Ordering_Up.'" /></a>';
			$tools .= '<a href="'.$linkDown.'"><img src="'.BASE_URL_ADMIN.'/images/icon/arrow_down.png"border="0" alt="'._DG_Ordering_Down.'" /></a>';
		}
		
		return $tools;
	}
	
	function getDataType($type, $data){
		$type = explode(':', $type);
		$type2 = explode('/', $type[1]);
		switch($type[0]):
			case 'date':
				if($data && $type2[0] == 'dmy'){
					$data = explode(' ', $data);
					$date = explode('-', $data[0]);
					$d = $date[2];
					$m = $date[1];
					$y = $date[0];
					return $d.'/'.$m.'/'.($type2[1] == 'th' ? ($y > 0 ? $y+543 : '00') : $y);
				}
				else {
					return '';
				}
				break;				
			case 'crop':
				if(mb_strlen($data) > $this->text_crop_count){
					$subfix .= '...';
				}
				$data  = mb_substr($data, 0, $this->text_crop_count, 'UTF-8').$subfix;
				return $data;
				break;	
			case 'set_lang_icon':
				$class_name = $type[1];
				$cont_id = $data;
				eval('$o = new '.$class_name.'();');
				$data = $o->show_item_lang_flag($cont_id);
				
				return $data;
				break;
			case 'text_color':
				$colors = $type[1];
				$colors = str_replace('{', '', $colors);
				$colors = str_replace('}', '', $colors);
				$colors = str_replace('"', '', $colors);
				
				$colors = explode('|', $colors);
				
				$c_set = array();
				foreach($colors as $color){
					$a = explode(',', $color);
					if( $data == $a[0] ){
						$data = '<span style="color:'.$a[1].'">'.$data.'</span>';	
					}
				}
				
				return $data;
				break;	
			case 'static':
				$data = $type[1];
				return $data;
				break;	
			case 'strip_tags':
				$data = strip_tags($data);
				return $data;
				break;	
			case 'fnc':
				$cls_fnc = explode('#', $type[1]);
				$cls_name = $cls_fnc[0];	
					
				$fnc = explode('(', end($cls_fnc));
				$fnc_name = $fnc[0];
				
				$_p = str_replace(')', '', $fnc);
				$p = explode(',', end($_p));
				
				//$data = call_user_func(array($cls_name,$fnc_name));
				
				$obj = new $cls_name;
				$data = call_user_func_array(array($obj, $fnc_name), array($data));
				
				return $data;
				break;
			default:
				return $data;
				break;
		endswitch;
	}
	
	function debug(){
		$this->render();
		echo '<div style="width:100%; background-color:#FFFF99;min-height:50px;text-align:center;">';
		echo $this->sql;
		echo '<br>';
		echo $this->datagrid;
		echo '</div>';
	}
	
 	function getLanguageIconTools($id, $relation_id, $lang_id, $app, $table, $pk){
		$lang = $this->db->GetArray("SELECT COUNT(LangID) AS TotalLang FROM ".getTableName('languages')." WHERE Active = 1 AND Publish = 1");
		$total_lang = $lang[0]['TotalLang'];
		if($total_lang > 1){
			$sql = "SELECT * FROM ".getTableName('languages')." WHERE Active = 1 AND Publish = 1 AND LangID <> ".$lang_id." ORDER BY Ordering ASC";
			$rows = $this->db->GetArray($sql);
			$icons = array();
			$icon  = "<hr>".TRANSLATE_TO_LANGUAGE.'<br />';
			for($i = 0; $i < count($rows); $i++){
			
				$sql2[$i] = "SELECT * FROM ".getTableName($table)." WHERE Active = 1 AND LangID = ".$rows[$i]['LangID']." AND (RelationID = ".$id." OR ".$pk." = '".$relation_id."' ) ORDER BY Ordering ASC";			
				
				$rows2[$i] = $this->db->GetArray($sql2[$i]);
				if( count($rows2[$i]) > 0 ):
					$status_text = EDIT;
					$url = BASE_URL_ADMIN.'/index.php?app='.$app.'&fnc=form&'.$pk.'='.$rows2[$i][0][$pk].'&relation_id='.$id.'&lang='.$rows[$i]['Code'];
				else:
					$status_text = ADD;
					$url = BASE_URL_ADMIN.'/index.php?app='.$app.'&fnc=form&relation_id='.$id.'&lang='.$rows[$i]['Code'];
				endif;
				
				$icons[] = '<div style="padding:2px 0;"><a href="'.SEF( $url ).'" title="'.$rows[$i]['Title'].'"><img src="'.BASE_URL.'/images/lang/'.$rows[$i]['Icon'].'" border="0" align="absmiddle" alt="'.$rows[$i]['Title'].'"/> '.$rows[$i]['Title'].'</a> ('.$status_text.')</div>';
			}
			$icon .= implode('', $icons);
		}
		return $icon;
 	}
	
	function set_hidden_param($param_strips){
		$g_url = $_SERVER['QUERY_STRING'];
		$g_char = array('=', '&');
		$g_link = str_replace( $g_char, '/', $g_url );									
		
		$value = "";		
		$g_viewcode = explode('/', $g_link);
		$g_viewcode = array_merge(array(''), $g_viewcode);
		
		$pset = array();
		
		$g_num_c = count($g_viewcode);
		for($j = 0; $j < $g_num_c; $j+=2) {
			if( $g_viewcode[$j+1] ){
				if( !in_array($g_viewcode[$j+1], $param_strips) ){
					$value .= '<input type="hidden" name="'.$g_viewcode[$j+1].'" value="'.$g_viewcode[$j+2].'" />';
				}
			}
		}
		return $value;
	}
}
?>
