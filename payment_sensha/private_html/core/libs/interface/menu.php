<?php
	function admin_top_menu(){
		global $lang2, BASE_URL_ADMIN;
		$menu	 = '<a href="'.SC_SEF( BASE_URL_ADMIN.'/index.php' ).'">'.$lang2['HOME'].'</a> | ';
		$menu	.= '<a href="'.SC_SEF( BASE_URL_ADMIN.'/index.php?app=member' ).'">'.$lang2['MEMBER'].'</a>';
		
		return $menu;
	}
	
	// Administrator Menu bar
	function admin_menubar(){
		$db = Database::getInstance();
		global DB_PREFIX;
		global BASE_URL_ADMIN;
		
		$menu_width1 = 150;
		$menu_width2 = 150;
		
		$kmsMenu .= '
		<script language="javascript">
		function createjsDOMenu() {
		';
			$rows1 = $db->GetArray("SELECT * FROM ".DB_PREFIX."menu WHERE active = 'Y' AND parentID = '".$id."' AND menuType = 'menubar' ORDER BY ordering ASC");

			$kmsMenu .= 'absoluteMenuBar = new jsDOMenuBar();';
			// Menu Bar Items
			for($i = 0; $i < count($rows1); $i++){
				$kmsMenu .= '    
					absoluteMenu'.($i+1).' = new jsDOMenu('.$menu_width1.', "absolute");
				';
				// Menu Items
				$rows2  = $db->GetArray("SELECT * FROM ".DB_PREFIX."menu WHERE active = 'Y' AND parentID = '".$rows1[$i]['menuID']."' AND menuType = 'menubar' ORDER BY ordering ASC");
				for($j = 0; $j < count($rows2); $j++){
					if( $rows2[$j]['menuLink'] == "-" ){
						$kmsMenu .= '    
							absoluteMenu'.($i+1).'.addMenuItem(new menuItem("-"));
							absoluteMenu'.($i+1).'_'.($j+1).' = new jsDOMenu('.$menu_width2.', "absolute");
						';
					}
					else{
						$kmsMenu .= '    
							absoluteMenu'.($i+1).'.addMenuItem(new menuItem("'.$rows2[$j]['menuName'].'", "item'.($j+1).'", "'.SC_SEF(BASE_URL_ADMIN.'/'.$rows2[$j]['menuLink']).'"));
							absoluteMenu'.($i+1).'_'.($j+1).' = new jsDOMenu('.$menu_width2.', "absolute");
						';
					}
					// Sub Menu Items
					$rows3  = $db->GetArray("SELECT * FROM ".DB_PREFIX."menu WHERE active = 'Y' AND parentID = '".$rows2[$j]['menuID']."' AND menuType = 'menubar' ORDER BY ordering ASC");
					for($k = 0; $k < count($rows3); $k++){
						$kmsMenu .= ' 						
							absoluteMenu'.($i+1).'_'.($j+1).'.addMenuItem(new menuItem("'.$rows3[$k]['menuName'].'", "", "'.SC_SEF(BASE_URL_ADMIN.'/'.$rows3[$k]['menuLink']).'"));
							absoluteMenu'.($i+1).'.items.item'.($j+1).'.setSubMenu(absoluteMenu'.($i+1).'_'.($j+1).');
						';
					}
				}
				// Menu Bar Items
				$kmsMenu .= '    				
					absoluteMenuBar.addMenuBarItem(new menuBarItem("'.$rows1[$i]['menuName'].'", absoluteMenu'.($i+1).'));
				';
			}
		$kmsMenu .= '    
			absoluteMenuBar.moveTo(140, 75);
		}
		</script>
		';
		return $kmsMenu;
	}
?>