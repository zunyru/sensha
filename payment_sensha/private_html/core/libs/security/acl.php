<?php
	class ACL{
		var $db;
		var $userid;
		var $except_pages = array();
		var $can_access = 1;
		
		function ACL($db, $permission_except_pages){
			$this->db = Database::getInstance();
			$this->except_pages = $permission_except_pages;
			//$this->userid  = SYS_PAGE == 1 ? get_access_id() : $_SESSION['session_member_id'];
			//$this->usergroupid = SYS_PAGE == 1 ? $_SESSION['session_admin_group'] : $_SESSION['session_member_group'];
		}
		
		function checkUserPermission(){
			
			$sql1 = "
				SELECT r.Code
				FROM ".SetDBTable('resources')." r
				WHERE r.Active = '1' 
				AND r.Publish = '1' 
				AND r.ResGID = '".SYS_PAGE."' 
				AND r.Code = '".getParam('app')."'
			";							
			$rs1 = $this->db->Execute($sql1);	

			if(!$this->except_page() && getParam('app') != "" && $this->usergroupid != 1){
				if($rs1->RecordCount() > 0){
					$sql2 = "
						SELECT up.UsrPmsGID, r.Title, r.Code, up.Functions
						FROM ".SetDBTable('user_permission_group')." up
						LEFT JOIN ".SetDBTable('resources')." r ON up.ResID=r.ResID
						WHERE up.Active = '1' AND up.Publish = '1' AND r.Code = '".getParam('app')."' AND up.UsrGID = '".$this->usergroupid."'
						GROUP BY up.ResID
					";	
					$rs2 = $this->db->Execute($sql2);
					$ds2 = $this->db->GetArray($sql2);
				
					if($rs2->RecordCount() <= 0){				
						SYS_Message("ขออภัย! คุณไม่ได้รับสิทธิในการเข้าใช้งาน", 'false');
						if(SYS_PAGE == 1){
							redirect(URL::_('admin', 'index.php?app=error'));	
						}
						else{
							redirect(URL::_('url', 'index.php?app=error'));	
						}
						$this->can_access = 0;
					}
					else{
						$functions = $ds2[0]['Functions'] ? explode(',', $ds2[0]['Functions']) : array();
						for($i = 0; $i < count($functions); $i++){
							$functions[$i] = trim($functions[$i]);
						}

						if(!in_array(getParam('fnc'), $functions) && getParam('fnc') != ""){
							SYS_Message("ขออภัย! คุณไม่ได้รับสิทธิในการเข้าใช้งาน", 'false');
							redirect(sUrl('index.php?app=error'));	
							$this->can_access = 0;
						}
					}
				}
			}
		}
		
		function can_access(){
			return $this->can_access;
		}
		
		function except_page(){
			return in_array($_GET['app'], $this->except_pages);
		}
		
		
	}
?>