<?php
	class ACL{
		var $db;
		var $userid;
		var $except_pages = array();
		
		function ACL($db, $permission_except_pages){
			$this->db = $db;
			$this->except_pages = $permission_except_pages;
			$this->userid  = 2;
			$this->usergroupid = 3;
		}
		
		function checkUserPermission(){
			
			$sql = "
				SELECT up.UsrPmsID
				FROM ".SetDBTable('user_permission')." up
				LEFT JOIN ".SetDBTable('user')." u ON up.UsrID=u.UsrID				
				LEFT JOIN ".SetDBTable('resources')." r ON up.ResID=r.ResID
				LEFT JOIN ".SetDBTable('resources_action')." ra ON up.ResAID=ra.ResAID
				LEFT JOIN ".SetDBTable('actions')." a ON ra.ActID=a.ActID
				WHERE u.Active = '1' AND u.Publish = '1'				
				AND up.Active = '1' AND up.Publish = '1'	
				AND r.Active = '1' AND r.Publish = '1'	
				AND ra.Active = '1' AND ra.Publish = '1'	
				AND up.UsrID = '".$this->userid."'
				".($_GET['app'] ? "AND r.Code = '".$_GET['app']."'" : "")."
				".($_GET['fnc'] ? "AND ra.Code = '".$_GET['fnc']."'" : "")."
			";	
						
			$rs = $this->db->Execute($sql);	
			
			if($rs && !$rs->RecordCount() && !$this->except_page()){
				if($_GET['fnc']){
					$sql = "
						SELECT pd.UrlRedirect
						FROM ".SetDBTable('resources_action')." ra
						LEFT JOIN ".SetDBTable('permission_deny_type')." pd ON ra.DnyTID=pd.DnyTID
						WHERE ra.Active = '1'
						AND ra.Publish = '1'
						AND ra.Code = '".$_GET['fnc']."'
						LIMIT 1
					";
					$rs = $this->db->GetArray($sql);					
					redirect(BASE_URL.'/'.$rs[0]['UrlRedirect']);			
				}
				elseif($_GET['app']){
					$sql = "
						SELECT pd.UrlRedirect
						FROM ".SetDBTable('resources')." r
						LEFT JOIN ".SetDBTable('permission_deny_type')." pd ON r.DnyTID=pd.DnyTID
						WHERE r.Active = '1'
						AND r.Publish = '1'
						AND r.Code = '".$_GET['app']."'
						LIMIT 1
					";
					$rs = $this->db->GetArray($sql);
					redirect(BASE_URL.'/'.$rs[0]['UrlRedirect']);	
				}		
			}
		}
		
		function except_page(){
			return in_array($_GET['app'], $this->except_pages);
		}
	}
?>