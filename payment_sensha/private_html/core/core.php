<?php
class Libraries {

	function Libraries(){
		$this->Load('database.adodb5.inc');
		$this->Load('database.adodb.active-record');
		$this->Load('database.model');
		$this->Load('database.dbtable');
		$this->Load('template.smarty.inc');
		$this->Load('template.smarty');
		$this->Load('application', 'core');
		$this->Load('interface.toolsbar');
		$this->Load('interface.paging');
		$this->Load('interface.paging_admin');
		$this->Load('interface.message');
		$this->Load('security.acl');
		$this->Load('file.mpdf60.mpdf');	
		$this->Load('mobile_detect');
		$this->Load('CreditCard');
		$this->Load('phpqrcode.qrlib');
	}

	function Load($value, $mode = ''){  //$lib->Load('database.adodb.inc');
		$value_usage = $value;
		$value = str_replace( '.' ,'/', $value);

		$path = BASE_PATH.'/core'.($mode == 'core' ? '' : '/libs').'/'.$value.'.php';
		if(is_file($path)){
			require_once($path);
		}
		else{
			echo '
			<div class="error_pane">
				<b><span style="color:red;">Unknow!</span> [ <span style="color:blue;">'.$value_usage.'</span> ] </b>
				Please check Libraries path and file.
			</div>
			<br>
			';
		}
	}

	function Inc($value){
		$value_usage = $value;
		$value = str_replace( '.' ,'/', $value);
		if(is_file(BASE_PATH.'/includes/'.$value.'.php')){
			require_once(BASE_PATH.'/includes/'.$value.'.php');
		}
		else{
			echo '
			<div class="error_pane">
				<b><span style="color:red;">Unknow!</span> [ <span style="color:blue;">'.$value_usage.'</span> ] </b>
				Please check includes path and file.
			</div>
			<br>
			';
		}
	}
}

/**
 * This function calling by smarty function on template files.
 */
function LoadModule($params){
	$mod_path = SITE_PATH."/modules";

	if(is_file($mod_path.'/'.$params['name'].'.php')){
		require_once $mod_path.'/'.$params['name'].'.php';
		if(function_exists($params['name'])){
			$function_name = $params['name'];
			echo $function_name($params);
		}
		elseif(function_exists('mod_'.$params['name'])){
			$function_name = 'mod_'.$params['name'];
			echo $function_name($params);
		}
	}
	else{
		echo '
		<div class="error_pane">
			<b><span style="color:red;">Module not found!</span> [ <span style="color:blue;">'.'mod.'.$params['name'].'.php'.'</span> ] </b>
			Please check apps.modules path and file.
		</div>
		';
	}
}

/**
 * This function calling by smarty function on template files.
 */
function LoadModuleAdmin($params){
	$mod_path = BASE_BACKEND_PATH."/modules";

	if(is_file($mod_path.'/'.$params['name'].'.php')){
		require_once $mod_path.'/'.$params['name'].'.php';
		if(function_exists($params['name'])){
			$function_name = $params['name'];
			echo $function_name($params);
		}
		elseif(function_exists('mod_'.$params['name'])){
			$function_name = 'mod_'.$params['name'];
			echo $function_name($params);
		}
	}
	else{
		echo '
		<div class="error_pane">
			<b><span style="color:red;">Module not found!</span> [ <span style="color:blue;">'.'mod.'.$params['name'].'.php'.'</span> ] </b>
			Please check apps.modules path and file.
		</div>
		';
	}
}

/**
 * Load model function for Application Active record files.
 */
function LoadModel($model_name){
	$model_path = (WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH)."/apps/model";

	if(is_file($model_path.'/'.$model_name.'.php')){
		require_once $model_path.'/'.$model_name.'.php';
	}
	else{
		echo '
		<div class="error_pane">
			<b><span style="color:red;">Model not found!</span> [ <span style="color:blue;">'.$model_name.'.php'.'</span> ] </b>
			Please check apps.model path and file.
		</div>
		';
	}
}

function Model($model_name, $type = ''){

	if($type){
		$base_path = str_replace(SITE_DIR, $type, SITE_PATH);
	}
	else{
		$base_path = SITE_PATH;
	}

	$app_model_path = $base_path."/apps/".$model_name."/model.php";
	
	if( is_file($app_model_path) ){
		$model_path = $app_model_path;
	}
	else{
		$model_path = $base_path."/model/".$model_name.".php";
	}

	if(is_file($model_path)){
		require_once $model_path;
		$model_class = "Model_".ucfirst($model_name);
		$oModel = new $model_class();

		return $oModel;
	}
	else{
		return false;
	}
}

function SetDBTable($table){
	return DB_PREFIX.$table;
}

function getParam( $value ){
	return param($value);
}

function param($value, $type = ''){

	$link = str_replace(array(SUB_DIR, '/'.SITE_DIR, '/index.php'), '', $_SERVER['REQUEST_URI']);
	
	$viewcode = explode('/', $link);
	$num_c = count($viewcode);
	for($i = 0; $i < $num_c; $i+=2) {
		${$viewcode[$i+1]} = $viewcode[$i+2];
		$_GET[$viewcode[$i+1]] = addslashes(htmlspecialchars(strip_tags($viewcode[$i+2])));
	}

	switch($type):
		case 'both':
			return addslashes(htmlspecialchars(strip_tags($_REQUEST[$value])));
			break;
		case 'post':
			return $_POST[$value];
			break;
		default:
			return addslashes(htmlspecialchars(strip_tags($_GET[$value])));
			break;
	endswitch;
}

function LoadLanguage($value, $type = ''){  // th, admin
	if($type == 'admin'){
		$value = $value ? $value : GetDefine('DEFAULT_LANGUAGE_BACKEND');
		if( is_file( BASE_BACKEND_PATH.'/language/'.$value.($type ? '.'.$type : '').'.ini' ) ):
			$language_ini_file = BASE_BACKEND_PATH.'/language/'.$value.($type ? '.'.$type : '').'.ini';
		else:
			$language_ini_file = BASE_BACKEND_PATH.'/language/'.GetDefine('DEFAULT_LANGUAGE_BACKEND').($type ? '.'.$type : '').'.ini';
		endif;

		$oLanguage = new ini_manager();
		$lang_val = $oLanguage->get_ini_array( $language_ini_file );
		foreach( $lang_val as $lant_defined => $lang_text):
			define($lant_defined, $lang_text);
		endforeach;
	}
	else{
		$value = $value ? $value : GetDefine('DEFAULT_LANGUAGE');
		if( is_file( BASE_BACKEND_PATH.'/language/'.$value.'.ini' ) ):
			$language_ini_file = BASE_BACKEND_PATH.'/language/'.$value.'.ini';
		else:
			$language_ini_file = BASE_BACKEND_PATH.'/language/'.GetDefine('DEFAULT_LANGUAGE').'.ini';
		endif;

		$oLanguage = new ini_manager();
		$lang_val = $oLanguage->get_ini_array( $language_ini_file );
		foreach( $lang_val as $lant_defined => $lang_text):
			define($lant_defined, trim($lang_text));
		endforeach;
	}

}

function LoadToolsBar(){
	if(is_file(BASE_BACKEND_PATH.'/apps/'.getParam('app').'/toolsbar.php')){
		require_once(BASE_BACKEND_PATH.'/apps/'.getParam('app').'/toolsbar.php');
	}
}

function GetLangID($code = ''){
	$db = Database::getInstance();

	if(WEB_PAGE == 1){
		$lang = GetSession('SYS_Lang_Admin') ? GetSession('SYS_Lang_Admin') : GetDefaultLangCode();
	}
	elseif(WEB_PAGE == 2){
		//$lang = GetSession('SYS_Lang') ? GetSession('SYS_Lang') : GetDefaultLangID();
		$lang = param('lang') ? param('lang') : GetDefaultLangID();
	}

	$sql = "
		SELECT lang_id
		FROM ".DB_PREFIX."languages
		WHERE active = '1'
		AND publish = '1'
		AND code = '".($code ? $code : $lang)."'
		LIMIT 1
	";
	$rs = $db->GetArray( $sql );
	return $rs[0]['lang_id'] ? $rs[0]['lang_id'] : $lang;
}

function GetDefaultLangID(){
	$db = Database::getInstance();

	if(WEB_PAGE == 1){
		$lang = GetDefine('DEFAULT_LANGUAGE_BACKEND');
	}
	elseif(WEB_PAGE == 2){
		$lang = GetDefine('DEFAULT_LANGUAGE');
	}

	$sql = "
		SELECT lang_id
		FROM ".DB_PREFIX."languages
		WHERE active = '1'
		AND publish = '1'
		AND code = '".$lang."'
		LIMIT 1
	";
	$rs = $db->GetArray( $sql );
	return $rs[0]['lang_id'];
}

function GetLangTitle(){
	$db = Database::getInstance();

	if(WEB_PAGE == 1){
		$lang = $_SESSION['SYS_Lang_Admin'] ? $_SESSION['SYS_Lang_Admin'] : DEFAULT_LANGUAGE_BACKEND;
	}
	elseif(WEB_PAGE == 2){
		$lang = $_SESSION['SYS_Lang'] ? $_SESSION['SYS_Lang'] : DEFAULT_LANGUAGE;
	}

	$sql = "
		SELECT title
		FROM ".DB_PREFIX."languages
		WHERE active = '1'
		AND publish = '1'
		AND code = '".$lang."'
		LIMIT 1
	";
	$rs = $db->GetArray( $sql );
	return $rs[0]['title'];
}

function GetLangCode(){
	if(WEB_PAGE == 1){
		return GetSession('SYS_Lang_Admin') ? GetSession('SYS_Lang_Admin') : GetDefine('DEFAULT_LANGUAGE_BACKEND');
	}
	elseif(WEB_PAGE == 2){
		//return GetSession('SYS_Lang') ? GetSession('SYS_Lang') : GetDefine('DEFAULT_LANGUAGE');
		return param('lang') ? param('lang') : GetDefine('DEFAULT_LANGUAGE');
	}
}

function GetDefaultLangCode(){
	$db = Database::getInstance();

	if(WEB_PAGE == 1){
		$lang = GetDefine('DEFAULT_LANGUAGE_BACKEND');
	}
	elseif(WEB_PAGE == 2){
		$lang = GetDefine('DEFAULT_LANGUAGE');
	}

	return $lang;
}

function GetLangText(){
	$dValue = get_defined_constants();
	return $dValue;
}

function GetDefine($var){
	return constant(WEB_SECRET_CODE.$var);
}

function SetDefine($var, $value){
	define(WEB_SECRET_CODE.$var, $value);
}

function GetSession($var){
	return $_SESSION[WEB_SECRET_CODE.$var];
}

function SetSession($var, $value){
	$_SESSION[WEB_SECRET_CODE.$var] = $value;
}

function UnSetSession($var){
	//@session_unregister(WEB_SECRET_CODE.$var);
	$_SESSION[WEB_SECRET_CODE.$var] = "";
	unset($_SESSION[WEB_SECRET_CODE.$var]);
}

function magic_quotes_off(){

	if (get_magic_quotes_gpc()) {
		$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
		while (list($key, $val) = each($process)) {
			foreach ($val as $k => $v) {
				unset($process[$key][$k]);
				if (is_array($v)) {
					$process[$key][stripslashes($k)] = $v;
					$process[] = &$process[$key][stripslashes($k)];
				} else {
					$process[$key][stripslashes($k)] = stripslashes($v);
				}
			}
		}
		unset($process);
	}

}

function set_data_row($value, $fields){

  $row = array();

  foreach($value as $val_k => $val_v){
   if( in_array($val_k, $fields) ){
    $row[$val_k] = $val_v;
   }
  }
  return $row;

}

function ID_Encode($ids,$salt,$length=10){
	
    $hashids 	= new Hashids\Hashids(WEB_SECRET_CODE.'.'.$salt,$length);
    $id_encoded = $hashids->encode($ids);
    unset($hashids);
	
    return $id_encoded;
}

function ID_Decode($id_encoded,$salt,$length=10){
	
    $hashids = new Hashids\Hashids(WEB_SECRET_CODE.'.'.$salt,$length);
    $numbers = $hashids->decode($id_encoded);

    unset($hashids);

    if( is_array($numbers) && count($numbers) > 0 ){
        if( count($numbers) > 1 ){
            return $numbers;
        }
        else{
            return $numbers[0];
        }
    }
    else{
        return '';
    }
	
}

function randomCode($len){
	srand((double)microtime()*10000000);
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	$ret_msg = "";
	$num = strlen($chars);
	for($i = 0; $i < $len; $i++){
		$ret_msg.= $chars[rand()%$num];
		$ret_msg.="";
	}
	return $ret_msg;
}

function set_access_id($id){
	return ID_Encode($id,'auth',16);
}

function get_access_id(){
	return ID_Decode(GetAuthAccessID(),'auth',16);
}

function SetAuthAccessID($id){
	SetSession('AuthAccessID.'.SITE_DIR, set_access_id($id));
}

function GetAuthAccessID(){
	return GetSession('AuthAccessID.'.SITE_DIR);
}

?>
