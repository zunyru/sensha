<?php
defined('WEB_ACCESS') or die('Access ERROR!!');

$core_path = PATH_LEVEL.(PRIVATE_DIR ? PRIVATE_DIR.'/' : '').'core/';

function SetLang($var){
	$_SESSION[WEB_SECRET_CODE.'ACTIVELANG'] = $var;
}

function GetLang(){
	return $_SESSION[WEB_SECRET_CODE.'ACTIVELANG'];
}

require_once $core_path.'configs/config.php';
require_once $core_path.'configs/setting.php';
require_once $core_path.'libs/hashids/HashGenerator.php';
require_once $core_path.'libs/hashids/Hashids.php';
require_once $core_path.'core.php';
require_once $core_path.'plugins/securimage/securimage.php';

/**
* Core function
*/
$Lib = new Libraries();

$Lib->Load('database.database');
$Lib->Load('mail.phpmailer');
$Lib->Load('mail.mail');
$Lib->Load('file.upload.class');
$Lib->Load('file.file');
$Lib->Load('file.resize_img');
$Lib->Load('file.smart_resize');
$Lib->Load('file.php_image_magician');
$Lib->Load('security.login');
$Lib->Load('security.acl');
$Lib->Load('security.password');
$Lib->Load('session');
$Lib->Load('button');
$Lib->Load('excel.PHPExcel');
$Lib->Load('excel.PHPExcel.IOFactory');

$Lib->Load('validator.application');
$Lib->Load('lang.thai');
$Lib->Load('interface.sef');
$Lib->Load('interface.url');
$Lib->Load('interface.html');
$Lib->Load('interface.mydatagrid');
$Lib->Load('interface.bitly');
$Lib->Load('html.html');
$Lib->Load('file.ini_manager');
$Lib->Load('media.youtube');

$Lib->Inc('functions');
$Lib->Inc('includes');
$Lib->Inc('appini');
$Lib->Inc('apphtml');

 /**
  * Database new instance and connection
  */
//   	$db = ADONewConnection('mysql');
//   	$db->debug = false;
//   	$db->Connect( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
//   	$db->Execute('SET NAMES UTF8');
//
//   	ADODB_Active_Record::SetDatabaseAdapter($db);

$sc_auth = new securelogin();
$member_auth = new securelogin();

$permission_except_pages = array('error', 'index');

$Acl = new ACL($db, $permission_except_pages);

/**
* New instance for password class
*/
$SYSPwd = new Password(WEB_SECRET_CODE);


/**
* Auto Loing if cookie no empty
*/
auto_login();


/**
* Template engine new instance
*/
$tpl_site = new Web_Smarty_Template();
$tpl_site_app	= new Web_Smarty_Application();

//$tpl_admin 		= new Web_Smarty_Template( 'admin' );
//$tpl_admin_app 	= new Web_Smarty_Application( 'admin' );

$app_site		= new SC_Application();
$app_admin 		= new SC_Application( 'admin' );

$tpl_site_app->register_function('Module', 'LoadModule');
$tpl_site_app->register_function('SessionMember', 'SessionMember');
$tpl_site_app->register_function('GetSetting', 'getSysSetting');
$tpl_site_app->register_function('AppLink', 'appLink');
//$tpl_admin_app->register_function('GetSetting', 'getSysSetting');
$tpl_site_app->register_function('ModuleAdmin', 'LoadModuleAdmin');


/** initial application */
$App_INI = new App_INI();
?>
