<?php
/**
 * class SC_Application
 * this class searching for apps to process
 */

class SC_Application {

	var $app_file;
	var $app_path;
	var $error_404;

	function SC_Application( $app_type = '' ){
		if( $app_type == 'admin' ){
			$this->app_path = "";
		}
		else{
			$this->app_path = "";
		}
	}

	function load( $app_name ) {

		$this->app_file = SITE_PATH."/".$this->app_path."apps/".$app_name."/index.php";
		$this->error_404 = (WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH) . "/includes/error/error.app.404.php";

		if( is_file( $this->app_file ) ){
			return  $this->app_file;
		}
		else{
			return $this->error_404;
		}
	}

}

/**
 * class Controller
 * this class for get application class and method to process in application
 */
class Application {
	var $sc_sitename;
	var $sc_Message;
	var $mb_endcoding;
	var $tpl_site_app;
	//var $tpl_admin_app;
	var $arrItemID;
	var $app_site;
	var $app_admin;
	var $page;
	var $lang;
	var $lang2;
	var $app;
	var $url;
	var $db_table_name	= array();
	var $db_table_pk		= array();
	var $db_table 			= array();
	var $db;
	var $a;
	var $fnc;
	var $Lib;
	var $Acl;
	var $App_INI;
	var $SYSPwd;
	var $site_js_file;
	var $addition_js_file;
	var $site_css_file;
	var $addition_css_file;
	var $error_message_set = array();

	function Application(){

		$this->sc_sitename			= $GLOBALS['sc_sitename'];
		$this->sc_Message			= $GLOBALS['sc_Message'];
		$this->mb_endcoding			= $GLOBALS['mb_endcoding'];
		$this->tpl_site_app			= $GLOBALS['tpl_site_app'];
		//$this->tpl_admin_app		= $GLOBALS['tpl_admin_app'];
		$this->arrItemID			= $GLOBALS['arrItemID'];
		$this->app_site				= $GLOBALS['app_site'];
		$this->app_admin			= $GLOBALS['app_admin'];
		$this->page					= htmlspecialchars(strip_tags($_GET['page']));
		$this->lang					= $GLOBALS['lang'];
		$this->lang2				= $GLOBALS['lang2'];
		$this->app					= htmlspecialchars(strip_tags($_GET['app']));
		$this->url					= $GLOBALS['url'];
		$this->db_table_name		= $GLOBALS['db_table_name'];
		$this->db_table_pk			= $GLOBALS['db_table_pk'];
		$this->db_table				= $GLOBALS['db_table'];
		$this->db					= Database::getInstance();
		$this->fnc					= htmlspecialchars(strip_tags($_GET['fnc']));
		$this->Lib					= $GLOBALS['Lib'];
		$this->Acl 					= $GLOBALS['Acl'];
		$this->App_INI 				= $GLOBALS['App_INI'];
		$this->SYSPwd				= new Password(WEB_SECRET_CODE);
		$this->canCallAppObject		= false;

		if( !$this->app ){
			$this->app = "index";
		}

		if( !$this->fnc ){
			$this->fnc = "default_index";
		}

		$this->set('BASE_PUBLIC', BASE_PUBLIC);
		$this->set('BASE_PRIVATE', BASE_PRIVATE);

		$this->set('BASE_URL', BASE_URL);
		$this->set('BASE_PATH', BASE_PATH);

		$this->set('SITE_PATH_PUBLIC', SITE_PATH_PUBLIC);
		$this->set('SITE_URL', SITE_URL);

		$this->txtLang = GetLangText();
		$this->set('Lang', $this->txtLang);

		//$this->call_model();
		$this->call('model');

		if( WEB_PAGE == 1 ){
			//$this->loadValidatorAdmin();
		}

		if( WEB_PAGE == 2 ){
			//$this->loadValidator();
		}
		$this->loadValidator();

	}

	function run($xajax_ini = ''){
		if($xajax_ini != ''){
			$this->set('XAJAX_INI', $xajax_ini);
		}
		$this->isAppClass();
		$this->isActionClass();
	}

	function isAppClass(){
		if( strtolower($this->app) == strtolower( get_class( $this ) ) ){
			return true;
		}
		else{
			if(!$this->canCallAppObject){
				/*
				echo '
				<div class="error_pane">
					<span style="color:red;">Class [ <b>'.get_class( $this ).'</b> ] not support App [ <b>'.$this->app.'</b> ]
				</div>
				';
				*/
				header('location: '.BASE_URL);
				exit;
			}
		}
	}

	function isActionClass(){
		if( $this->isMethod($this, $this->fnc)  ){
			$fnc = $this->fnc;
			$this->$fnc();
		}
		else{
			if(!$this->canCallAppObject){
				if( !$this->fnc ){ $this->fnc = "..."; }
				/*
				echo '
				<div class="error_pane">
					<b><span style="color:red;">Unknow! function </span> [ <span style="color:blue;">'.$this->fnc.'</span> ] </b>
					on [ <b>'.get_class( $this ).'</b> ] class.
				</div>
				';
				*/
				header('location: '.BASE_URL);
				exit;
			}
		}
	}

	function isMethod( $obj, $method ){
		if( method_exists( $obj, $method ) ){
			return true;
		}
		else{
			return false;
		}
	}

	function getAppName( $file ){
		return $this->app."/views/".$file;
	}

	function isAdmin(){
	 	if( !session_is_registered('session_admin_id') && !session_is_registered('session_admin_group') ){ exit(); }
	}

	function setTableName( $tablename ){
		return DB_PREFIX.$tablename;
	}

	function getTableName( $tablename ){
		return DB_PREFIX.$tablename;
	}

	function dataTools( $type, $link ){

		switch( $type ):
			case "edit":
				$tools = '<a href="'.$link.'"><img src="'.BASE_BACKEND_URL.'/images/icon/edit.png"border="0" alt="'.$this->lang2['DT_TOOLS_EDIT_DATA'].'" /></a>';
				break;
			case "delete":
				$tools = '<a href="'.$link.'" onClick="return confirm(\''.$this->lang2['DT_TOOLS_DELETE_CONFIRM'].'\');"><img src="'.BASE_BACKEND_URL.'/images/icon/delete.png"border="0" alt="'.$this->lang2['DT_TOOLS_DELETE_DATA'].'" /></a>';
				break;
			case "publish_y":
				$tools = '<a href="'.$link.'"><img src="'.BASE_BACKEND_URL.'/images/icon/publish_y.png"border="0" alt="'.$this->lang2['DT_TOOLS_PUBLISH_Y'].'" /></a>';
				break;
			case "publish_n":
				$tools = '<a href="'.$link.'"><img src="'.BASE_BACKEND_URL.'/images/icon/publish_n.png"border="0" alt="'.$this->lang2['DT_TOOLS_PUBLISH_N'].'" /></a>';
				break;
			case "permission":
				$tools = '<a href="'.$link.'"><img src="'.BASE_BACKEND_URL.'/images/icon/permission.png"border="0" alt="'.$this->lang2['DT_TOOLS_PERMISSION'].'" /></a>';
				break;
			default:
				$tools = "";
				break;
		endswitch;

		return $tools;
	}

	function setOrdering($loopNumber, $linkUp, $linkDown, $numRows){

			$tools = "";

		if( $numRows == 1	 ) {
			$tools .= "";
		}
		elseif($loopNumber == 0){
			$tools .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$tools .= '<a href="'.$linkDown.'"><img src="'.BASE_BACKEND_URL.'/images/icon/arrow_down.png"border="0" alt="'.$this->lang2['DT_TOOLS_ORDERING_DOWN'].'" /></a>';
		}
		elseif($loopNumber == ($numRows - 1)){
			$tools .= '<a href="'.$linkUp.'"><img src="'.BASE_BACKEND_URL.'/images/icon/arrow_up.png"border="0" alt="'.$this->lang2['DT_TOOLS_ORDERING_UP'].'" /></a>';
			$tools .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		else{
			$tools .= '<a href="'.$linkUp.'"><img src="'.BASE_BACKEND_URL.'/images/icon/arrow_up.png"border="0" alt="'.$this->lang2['DT_TOOLS_ORDERING_UP'].'" /></a>';
			$tools .= '<a href="'.$linkDown.'"><img src="'.BASE_BACKEND_URL.'/images/icon/arrow_down.png"border="0" alt="'.$this->lang2['DT_TOOLS_ORDERING_DOWN'].'" /></a>';
		}
		return $tools;
	}

	function loadValidator(){
		if( is_file( SITE_PATH . '/apps/'.$this->app.'/validator.php' ) ){
			require_once( SITE_PATH . '/apps/'.$this->app.'/validator.php' );
			if( class_exists('Validator') ){
				$this->validator =  new Validator();
			}
		}
	}

	function loadValidatorAdmin(){
		if( is_file( BASE_BACKEND_PATH . '/apps/'.$this->app.'/validator.php' ) ){
			require_once( BASE_BACKEND_PATH . '/apps/'.$this->app.'/validator.php' );
			if( class_exists('Validator') ){
				$this->validator =  new Validator();
			}
		}
	}

//	function model(){
//		if(WEB_PAGE == 1){
//			if( is_file( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/model.php' ) ){
//				require_once( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/model.php' );
//			}
//		}
//		else{
//			if( is_file( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/model.php' ) ){
//				require_once( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/model.php' );
//			}
//		}
//		$oModel = new AppModel();
//		return $oModel;
//	}

	function setMessage($message,$type = ''){
		
		/*
		$messege 	= stripslashes(htmlspecialchars($message));
		$type 		= stripslashes(htmlspecialchars($type));
		$messege 	= str_replace('<', '', $message);
		$messege 	= str_replace('>', '', $message);
		$type 		= str_replace('<', '', $type);
		$type 		= str_replace('>', '', $type);
		$messege 	= str_replace('%3C', '', $message);
		$messege 	= str_replace('%3E', '', $message);
		$type 		= str_replace('%3C', '', $type);
		$type 		= str_replace('%3E', '', $type);
		if($type == "true"){
			$msg = $messege ? '<label class="green">'.$messege.'</label>' : '';
		}
		elseif($type == "false"){
			$msg = $messege ? '<label class="red">'.$messege.'</label>' : '';
		}
		elseif($type == "delete"){
			$msg = $messege ? '<label class="orange">'.$messege.'</label>' : '';
		}
		else{
			$msg = $messege ? '<label >'.$message.'</label>' : '';
		}
		return $msg ? BaseHtml::showError($msg) : "";
		*/
		
		return $message;
		
	}

	function view($filename){

		if( $this->site_js_file ){
			$this->set('SITE_JS', $this->site_js_file);
		}

		if( $this->addition_js_file ){
			$this->set('Html_Addition_JS', $this->addition_js_file);
		}

		if( $this->site_css_file ){
			$this->set('SITE_CSS', $this->site_css_file);
		}

		if( $this->addition_css_file ){
			$this->set('Html_Addition_CSS', $this->addition_css_file);
		}

		$this->tpl_site_app->display( $this->getAppName( $filename ) );
	}

	function set($var, $value){
		$this->tpl_site_app->assign( $var,  $value);
	}

	function param($param_set){
		$params = explode(',', $param_set);

		for($i = 0; $i < count($params); $i++){
			$param_var = explode('=', $params[$i]);
			$param[$param_var[0]] = $param_var[1];
		}
		return $param;
	}

	function call($type, $customize_name = ''){
		switch($type):
			case 'model':
				$name = $customize_name ? $customize_name : $this->app;
				if( $customize_name ){
					return Model($name);
				}
				else{
					$this->model = Model($name);
				}
				break;
		endswitch;
	}

	/*
	function call($file_name){
		if(WEB_PAGE == 1){
			if( is_file( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/'.$file_name.'.php' ) ){
				require_once( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/'.$file_name.'.php' );
			}
		}
		else{
			if( is_file( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/'.$file_name.'.php' ) ){
				require_once( BASE_BACKEND_PATH . '/apps/'.getParam('app').'/'.$file_name.'.php' );
			}
		}
	}
	*/

	function callAppObject($app_name, $file_name = ''){
		$app_name = $app_name ? $app_name : getParam('app');
		$file_name = $file_name ? $file_name : 'index.php';
		if(WEB_PAGE == 1){
			if( is_file( (WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH) . '/apps/'.$app_name.'/'.$file_name ) ){
				require_once( (WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH) . '/apps/'.$app_name.'/'.$file_name );
			}
		}
		else{
			if( is_file( BASE_PATH . '/apps/'.$app_name.'/'.$file_name ) ){
				require_once( BASE_PATH . '/apps/'.$app_name.'/'.$file_name );
			}
		}
		$app_class = ucfirst($app_name);
		$oApp = new $app_class();
		return $oApp;
	}

	function call_model($customize_name = ''){

		$base_path = WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH;

		$name = $customize_name ? $customize_name : $this->app;
		if(is_file($base_path.'/model/'.$name.'.php')){
			require_once $base_path.'/model/'.$name.'.php';
			$model_class_name = "Model_".ucfirst($name);
			if( class_exists($model_class_name) ){
				if( $customize_name ){
					$o_model = new $model_class_name();
					return $o_model;
				}
				else{
					$this->model = new $model_class_name();
				}
			}
		}
	}

	function _js($js_file){ // load template js

		$base_url = SITE_URL;

		if(is_array($js_file) && count($js_file) > 0){
			foreach($js_file as $js){
				$base_path = SITE_PATH_PUBLIC;

				$js_path = $base_path.'/'.$js;
				$js_url = $base_url.'/'.$js;

				if( is_file($js_path) ){
					$this->site_js_file .= '<script type="text/javascript" src="'.$js_url.'"></script>';
				}
			}
		}
	}

	function load_js($js_file, $path_type = ''){

		$app_name = param('app') ? param('app') : 'index';

		$base_url = SITE_URL;

		$_site = str_replace('/', '', SITE_DIR);
		if($_site != 'default'){
			$site = $_site;
		}

		if( $path_type == 'f' ){
			$base_path = SITE_PATH_PUBLIC;
			$js_path = $base_path.'/assets/js/'.$js_file.'.js';
			$js_url = $base_url.'/assets/js/'.$js_file.'.js';
		}
		else{
			$base_path = SITE_PATH;
			$js_path = $base_path.'/apps/'.$app_name.'/js/'.$js_file.'.js';
			$js_url = BASE_URL.'/index.php/app/core/fnc/load_js'.($site ? '/_site/'.$site : '').'/_app/'.$app_name.'/js/'.$js_file;
		}

		if( is_file($js_path) ){
			if(URL_SERVICE == 1){
				$js_url = BASE_URL.'/load_js'.($site ? '/'.$site : '').'/'.$app_name.'/'.$js_file.'.js';
			}

			if( $js_url ){
				$this->addition_js_file .= '<script type="text/javascript" src="'.$js_url.'"></script>';
			}
		}
	}

	function load_css($css_file, $path_type = '', $media = ''){

		/*
		$app_name = param('app') ? param('app') : 'index';

		if( $path_type == 'f' ){
			$base_path = SITE_PATH_PUBLIC;
			$base_url = SITE_URL;

			$css_path = $base_path.'/assets/css/'.$css_file.'.css';
			$css_url = $base_url.'/assets/css/'.$css_file.'.css';
			$css_path_encode = $css_url;
		}
		else{
			$base_path = SITE_PATH_PUBLIC;
			$css_path = $base_path.'/apps/'.$app_name.'/css/'.$css_file.'.css';
			$js_url = BASE_URL.'/index.php/app/core/fnc/load_css/_app/'.$app_name.'/css/'.$css_file;
			//$css_path_encode = File::encode($css_path);
		}
		if( is_file($css_path) ){
			if(URL_SERVICE == 1){
				$css_url = BASE_URL.'/load_css/'.$app_name.'/'.$css_file.'.css';
			}
			if( $css_url ){
				$this->addition_css_file .= '<link href="'.$css_url.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').'/>';
			}
		}
		*/

		$app_name = param('app') ? param('app') : 'index';

		$base_url = SITE_URL;

		$_site = str_replace('/', '', SITE_DIR);
		if($_site != 'default'){
			$site = $_site;
		}

		if( $path_type == 'f' ){
			$base_path = SITE_PATH_PUBLIC;
			$css_path = $base_path.'/assets/css/'.$css_file.'.css';
			$css_url = $base_url.'/assets/css/'.$css_file.'.css';
		}
		else{
			$base_path = SITE_PATH;
			$css_path = $base_path.'/apps/'.$app_name.'/css/'.$css_file.'.css';
			$css_url = BASE_URL.'/index.php/app/core/fnc/load_css'.($site ? '/_site/'.$site : '').'/_app/'.$app_name.'/css/'.$js_file;
		}

		if( is_file($css_path) ){
			if(URL_SERVICE == 1){
				$css_url = BASE_URL.'/load_css'.($site ? '/'.$site : '').'/'.$app_name.'/'.$css_file.'.css';
			}

			if( $css_url ){
				$this->addition_css_file .= '<link href="'.$css_url.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').'/>';
			}
		}

	}
	
	function _css($css_file){ // load template css

		$base_url = SITE_URL;

		if(is_array($css_file) && count($css_file) > 0){
			foreach($css_file as $css){
				$media = '';
				$base_path = SITE_PATH_PUBLIC;

				$css_path = $base_path.'/'.$css;
				$css_url = $base_url.'/'.$css;

				if( is_file($css_path) ){
					$this->site_css_file .= '<link href="'.$css_url.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').'/>';
				}
			}
		}
	}
	/*
	function load_js($js_file, $path_type = ''){

		if( $path_type ){
			switch($path_type){
				case 1:
				case 'b':
					$base_url 	= BASE_BACKEND_URL;
					$base_path 	= BASE_BACKEND_PATH;
					break;
				case 2:
				case 'f':
					$base_url 	= BASE_URL;
					$base_path 	= BASE_PATH;
					break;
				case 'ext':
					$ext_url = $js_file;
					break;
				default:break;
			}
		}
		else{
			$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
			$base_path = WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH;
		}
		if( $ext_url ){
			$this->addition_js_file .= '
<script type="text/javascript" src="'.$ext_url.'"></script> ';
		}
		else{
		if( is_file($base_path.'/apps/'.(getParam('app') ? getParam('app') : 'index').'/views/'.$js_file) ){
			$this->addition_js_file .= '
<script type="text/javascript" src="'.$base_url.'/apps/'.(getParam('app') ? getParam('app') : 'index').'/views/'.$js_file.'"></script> ';
		}

		if( is_file($base_path.'/'.$js_file) ){
			$this->addition_js_file .= '
<script type="text/javascript" src="'.$base_url.'/'.$js_file.'"></script> ';
		}
		}
	}

	function load_css($css_file, $path_type = '', $media = ''){

		if( $path_type ){
			switch($path_type){
				case 1:
				case 'b':
					$base_url 		= BASE_BACKEND_URL;
					$base_path 	= BASE_BACKEND_PATH;
					break;
				case 2:
				case 'f':
					$base_url 	= BASE_URL;
					$base_path 	= BASE_PATH;
					break;
				case 'ext':
					$ext_url = $css_file;
					break;
				default:break;
			}
		}
		else{
			$base_url = WEB_PAGE == 1 ? BASE_BACKEND_URL : BASE_URL;
			$base_path = WEB_PAGE == 1 ? BASE_BACKEND_PATH : BASE_PATH;
		}

		if( $ext_url ){
			$this->addition_js_file .= '
<link href="'.$ext_url.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').'/> ';
		}
		else{
		if( is_file($base_path.'/apps/'.(getParam('app') ? getParam('app') : 'index').'/views/'.$css_file) ){
			$this->addition_css_file .= '
<link href="'.$base_url.'/apps/'.(getParam('app') ? getParam('app') : 'index').'/views/'.$css_file.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').'/> ';
		}
		elseif( is_file($base_path.'/'.$css_file) ){
			$this->addition_css_file .= '
<link href="'.$base_url.'/'.$css_file.'" rel="stylesheet" type="text/css" '.($media ? 'media="'.$media.'"' : '').' /> ';
		}
		}
	}
*/
	function set_error($type, $message){
		$this->error_message_set['error_'.$type] = $message;
		array_push($this->error_message_set, $this->error_message_set['error_'.$type]);
		echo "false";
		return false;
	}

	function unset_error(){
		echo "true";
		return true;
	}

	function get_error_message(){
		return $this->error_message_set;
	}

	function meta_setup($title, $keyword = '', $desc = ''){

		$Page_Info['title']  			= setPageTitle($title, 4); // only value
		$Page_Info['keyword'] 	= setPageKeyword($keyword, 2);
		$Page_Info['description']	= setPageDescription($desc, 1);
		$this->set( 'Page_Info' , $Page_Info );

	}

	function meta_title_type($type){
		$this->meta_title_type = $type;
	}

	function encode($value){
		return base64_encode($value);
	}

	function decode($value){
		return base64_decode($value);
	}
}

class Validator_Application {

	var $error_set = 0;
	var $xajax;
	var $Lib;
	var $SYSPwd;
	var $error_message_set = array();

	function Validator_Application(){
		$db = Database::getInstance();
		$this->db = $db;
		$this->Lib = $GLOBALS['Lib'];
		$this->SYSPwd = $GLOBALS['SYSPwd'];
	}

	function isNoError(){
		if( $this->error_set > 0 ){
			return false;
		}
		else{
			return true;
		}
	}

	function is_value($field_data, $field_name, $message){
		if( $field_data == ""){
			$this->set_error($field_name, $message);
		}
		else{
			$this->unset_error($field_name);
		}
	}

	function setErrorSyle($message){
		global $objResponse;

		$objResponse->addScript($message);
		$this->error_set += 1;

		return $objResponse;
	}

	function setError($area = "", $message = ""){
		global $objResponse;

		if($area != ""){
			$objResponse->addAssign($area ,"innerHTML", $message);
		}
		$this->error_set += 1;

		return $objResponse;
	}

	function unSetError($area = "", $message = ""){
		global $objResponse;

		if($area != ""){
			$objResponse->addAssign($area ,"innerHTML", $message);
		}
		$this->error_set += 0;

		return $objResponse;
	}

	function getTableName( $tablename ){
		return DB_PREFIX.$tablename;
	}

	function showErrorBlink($element_id){
		global $objResponse;

		$objResponse->addScript("$('#".$element_id."').fadeIn(500);$('#".$element_id."').fadeOut(8000);");

		return $objResponse;
	}

	function isValue($value, $error_pane, $message = ''){
		if( $value == "" ){
			$this->setError($error_pane, $message ? $message : _Please_Fill);
		}
		else{
			$this->unSetError($error_pane);
		}
	}

	function isNumber($value, $error_pane){
		if( !App_Validator::isNumber($value) ){
			$this->setError($error_pane, _Please_Fill_InNumber);
		}
		else{
			$this->unSetError($error_pane);
		}
	}

	function set_error($type, $message){
		$this->error_message_set[$type] = $message;
		$this->error_set += 1;
	}

	function unset_error($type){
		$this->error_message_set[$type] = "";
		$this->error_set += 0;
	}

	function get_error_message(){
		return $this->error_message_set;
	}

}

function SYS_Message($message, $type = ''){
	if(WEB_PAGE == 2){
		$_SESSION['SYS_Message_Front'] = $message;
		$_SESSION['SYS_Message_Type_Front'] = $type;
	}
	else{
		SetSession('sys_message',$message);
		SetSession('sys_message_type',$type);
	}
}

function SYS_Message_Error($message){
	SetSession('ERROR_MESSAGE', $message);
}

function SessionMember($params){
	if($_SESSION['session_member_id']){
		$dbObj = new DBTable ("user", "UsrID" );
		$rs = $dbObj->select($_SESSION['session_member_id']);
		switch($params['get']):
			case 'id':
				return $rs->USRID;
				break;
			case 'name':
				return $rs->USERNAME;
				break;
			case 'picture':
				return $rs->PICTURE;
				break;
			case 'email':
				return $rs->EMAIL;
				break;
			case 'fullname':
				return $rs->FIRSTNAME." ".$rs->LASTNAME;
				break;
			case 'address':
				return $rs->ADDRESS;
				break;
			case 'ampid':
				return $rs->AMPID;
				break;
			case 'provid':
				return $rs->PROVID;
				break;
			case 'tambon':
				return $rs->TAMBON;
				break;
			case 'zipcode':
				return $rs->ZIPCODE;
				break;
			case 'phone':
				return $rs->PHONE;
				break;
			case 'fax':
				return $rs->FAX;
				break;
			default:
				return false;
				break;
		endswitch;
	}
}

function is_not_admin(){
	if( WEB_PAGE == 2 || WEB_PAGE != 1 ){
		return true;
	}
	else{
		return false;
	}
}

?>
