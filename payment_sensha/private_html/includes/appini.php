<?php
	class App_INI {
		
		var $db;
		var $list_name;
		var $sql;
		var $value_id;
		var $value_label;
	
		var $onchange;
		var $onchange_form;
		var $onchange_category;
		var $onchange_category_form;
		var $onchange_user_level;
		var $onchange_user_level_form;
		var $onchange_user_level_group;
		var $onchange_user_level_group_form;
		var $onchange_resource_group;
		var $onchange_resource_group_form;
		var $onchange_resource;
		var $onchange_resource_form;
		var $onchange_action;
		var $onchange_action_form;
		var $onchange_deny_type;
		var $onchange_deny_type_form;
		
		var $option_only = false;
		var $option_only_form = false;
		var $option_only_category = false;
		var $option_only_category_form = false;
		var $option_only_user_level = false;
		var $option_only_user_level_form = false;
		var $option_only_user_level_group = false;
		var $option_only_user_level_group_form = false;
		var $option_only_resource_group = false;
		var $option_only_resource_group_form = false;
		var $option_only_resource = false;
		var $option_only_resource_form = false;
		var $option_only_action = false;
		var $option_only_action_form = false;
		var $option_only_deny_type = false;
		var $option_only_deny_type_form = false;
		
		var $disabled_resource_group = false;
		var $disabled_resource = false;
		var $limit_resource;
		var $limit_resource_group;
		
		function App_INI(){
			$db = Database::getInstance();
			
			$this->db = $db;
		}
		
		/** Category */
		function category_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_category;
			$this->option_only = $this->option_only_category;
			
			$this->list_name = 'cateid';
			$this->value_id = 'CateID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT CateID, Title
				FROM ".$this->getTableName('categories')."
				WHERE Active = '1' 
				AND LangID = '".GetLangID()."'
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function category_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_category_form;
			$this->option_only_form = $this->option_only_category_form;
			
			$this->list_name = 'cateid';
			$this->value_id = 'CateID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT CateID, Title
				FROM ".$this->getTableName('categories')."
				WHERE Active = '1' 
				AND LangID = '".GetLangID()."'
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** User Level */
		function user_level_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_user_level;
			$this->option_only = $this->option_user_level;
			
			$this->list_name = 'UsrGID';
			$this->value_id = 'UsrGID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT UsrGID, Title
				FROM ".$this->getTableName('user_group')."
				WHERE Active = '1' 
				AND UsrGid <> '1'
				".$condition."
				ORDER BY Ordering ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function user_level_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_user_level_form;
			$this->option_only_form = $this->option_user_level_form;
			
			$this->list_name = 'UsrGID';
			$this->value_id = 'UsrGID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT UsrGID, Title
				FROM ".$this->getTableName('user_group')."
				WHERE Active = '1' 
				AND UsrGid <> '1'
				".$params['condition']."
				ORDER BY Title, Ordering ASC
			";
			return $this->select_list_form($params);
		}
		
		/** User Level Group*/
		function user_level_group_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_user_level;
			$this->option_only = $this->option_user_level;
			
			$this->list_name = 'UsrGTID';
			$this->value_id = 'UsrGTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT UsrGTID, Title
				FROM ".$this->getTableName('user_group_type')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function user_level_group_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_user_level_form;
			$this->option_only_form = $this->option_user_level_form;
			
			$this->list_name = 'UsrGTID';
			$this->value_id = 'UsrGTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT UsrGTID, Title
				FROM ".$this->getTableName('user_group_type')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Permission Resource Group */
		function resource_group_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_resource_group;
			$this->option_only = $this->option_only_resource_group;
			$this->disabled = $this->disabled_resource_group;
			
			$this->list_name = 'ResGID';
			$this->value_id = 'ResGID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT ResGID, Title
				FROM ".$this->getTableName('resources_group')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
				".($this->limit_resource ? $this->limit_resource : '')."
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function resource_group_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_resource_group_form;
			$this->option_only_form = $this->option_only_resource_group_form;
			
			$this->list_name = 'ResGID';
			$this->value_id = 'ResGID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT ResGID, Title
				FROM ".$this->getTableName('resources_group')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Permission Resource */
		function resource_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_resource;
			$this->option_only = $this->option_only_resource;
			$this->disabled = $this->disabled_resource;
			
			$this->list_name = 'ResID';
			$this->value_id = 'ResID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT ResID, Title
				FROM ".$this->getTableName('resources')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
				".($this->limit_resource ? $this->limit_resource : '')."
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function resource_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_resource_form;
			$this->option_only_form = $this->option_only_resource_form;
			
			$this->list_name = 'ResID';
			$this->value_id = 'ResID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT ResID, Title
				FROM ".$this->getTableName('resources')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		
		/** Actions */
		function action_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_action;
			$this->option_only = $this->option_only_action;
			
			$this->list_name = 'ActID';
			$this->value_id = 'ActID';
			$this->value_label = 'ActTitle';
			$this->sql = "
				SELECT ActID, ActTitle
				FROM ".$this->getTableName('actions')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY ActTitle ASC				
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function action_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_action_form;
			$this->option_only_form = $this->option_only_action_form;
			
			$this->list_name = 'ActID';
			$this->value_id = 'ActID';
			$this->value_label = 'ActTitle';
			$this->sql = "
				SELECT ActID, ActTitle
				FROM ".$this->getTableName('actions')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY ActTitle ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Permission Deny Type */
		function deny_type_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_deny_type;
			$this->option_only = $this->option_only_deny_type;
			
			$this->list_name = 'DnyTID';
			$this->value_id = 'DnyTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT DnyTID, Title
				FROM ".$this->getTableName('permission_deny_type')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function deny_type_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_deny_type_form;
			$this->option_only_form = $this->option_only_deny_type_form;
			
			$this->list_name = 'DnyTID';
			$this->value_id = 'DnyTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT DnyTID, Title
				FROM ".$this->getTableName('permission_deny_type')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Category Type */
		function category_type_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_category_type;
			$this->option_only = $this->option_only_category_type;
			
			$this->list_name = 'CateTID';
			$this->value_id = 'CateTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT CateTID, Title
				FROM ".$this->getTableName('categories_type')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function category_type_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_category_type_form;
			$this->option_only_form = $this->option_only_category_type_form;
			
			$this->list_name = 'CateTID';
			$this->value_id = 'CateTID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT CateTID, Title
				FROM ".$this->getTableName('categories_type')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Prefix */
		function prefix_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_category_type;
			$this->option_only = $this->option_only_category_type;
			
			$this->list_name = 'PrefixID';
			$this->value_id = 'PrefixID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT PrefixID, Title
				FROM ".$this->getTableName('prefix')."
				WHERE Active = '1' 
				AND LangID = '".GetLangID()."'
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function prefix_type_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_category_type_form;
			$this->option_only_form = $this->option_only_category_type_form;
			
			$this->list_name = 'PrefixID';
			$this->value_id = 'PrefixID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT PrefixID, Title
				FROM ".$this->getTableName('prefix')."
				WHERE Active = '1' 
				AND LangID = '".GetLangID()."'
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}
		
		/** Language */
		function lang_select_list($selected_value = '', $condition = '', $fst_label = ''){
			$this->onchange = $this->onchange_category_type;
			$this->option_only = $this->option_only_category_type;
			
			$this->list_name = 'LangID';
			$this->value_id = 'LangID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT LangID, Title
				FROM ".$this->getTableName('languages')."
				WHERE Active = '1' 
				".$condition."
				ORDER BY Title ASC
			";
			return $this->select_list($selected_value, $condition, $fst_label);
		}
		
		function lang_select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$this->onchange_form = $this->onchange_category_type_form;
			$this->option_only_form = $this->option_only_category_type_form;
			
			$this->list_name = 'LangID';
			$this->value_id = 'LangID';
			$this->value_label = 'Title';
			$this->sql = "
				SELECT LangID, Title
				FROM ".$this->getTableName('languages')."
				WHERE Active = '1' 
				".$params['condition']."
				ORDER BY Title ASC
			";
			return $this->select_list_form($params);
		}

		/**
		 * Select list
		 */
		function select_list($selected_value = '', $condition = '', $fst_label = ''){
			$rs = $this->db->GetArray( $this->sql );
			
			if(!$this->option_only){
			$html = '<select name="'.$this->list_name.'" id="'.$this->list_name.'" style="width:250px;" class="txt_input" '.($this->onchange ? 'onChange="'.$this->onchange.'"' : '').' '.($this->disabled ? 'disabled="disabled"' : '').'>';
				$html .= '<option value="">'.($fst_label ? $fst_label : _Please_Select).'</option>';
			}
				
			for( $i = 0; $i < count($rs); $i++){
				$selected = $selected_value == $rs[$i][$this->value_id]  ? 'selected="selected"' : '';
				$html .= '<option value="'.$rs[$i][$this->value_id].'" '.$selected.'>'.$rs[$i][$this->value_label].'</option>';
			}
			if(!$this->option_only){
			$html .= '</select>';
			}
			
			return $html;
		}
		
		function select_list_form($params = array('selected'=>'', 'condition'=>'', 'action_form'=>'')){
			$html = '';
			
			$rs = $this->db->GetArray( $this->sql );
			
			if(!$this->option_only_form){
			$html .= '<script language="javascript">';
				$html .= 'function Cate2Go(){';
					$html .= 'document.forms[\'adminForm\'].action = \''.$params['action_form'].'\';';
					$html .= 'document.forms[\'adminForm\'].submit();';
				$html .= '}';
			$html .= '</script>';
			
			$html .= '<select name="'.$this->list_name.'" id="'.$this->list_name.'" style="width:250px;" class="txt_input"  '.($this->onchange_form ? 'onChange="Cate2Go(); '.$this->onchange_form.'"' : 'onchange="Cate2Go();"').'>';
				if($params['first_label']){
					$html .= '<option value="">'.$params['first_label'].'</option>';
				}
				else{
					$html .= '<option value="">'._Display_All.'</option>';
				}
			}	
				
			for( $i = 0; $i < count($rs); $i++){
				$selected = $params['selected'] == $rs[$i][$this->value_id]  ? 'selected="selected"' : '';
				$html .= '<option value="'.$rs[$i][$this->value_id].'" '.$selected.'>'.$rs[$i][$this->value_label].'</option>';
			}
			
			if(!$this->option_only_form){
			$html .= '</select>';
			}
			
			return $html;
		}
	
		/* set table name from db prefix and table name */
		function getTableName( $tablename ){
			return DB_PREFIX.$tablename;
		}
		
		function findThaiChar($value){
			if(preg_match("/^[ก-๙]+$/i", $value)){
				return true;
			}
			else{
				return false;
			}
		}
	
	}

?>