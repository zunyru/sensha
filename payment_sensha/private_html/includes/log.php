<?php
$query_string	= $_SERVER['QUERY_STRING'];
$ip				= $_SERVER['REMOTE_ADDR'];
$time 			= date("H:i:s");
$app				= getParam('app') ? getParam('app') : 'index';
$fnc				= getParam('fnc') ? getParam('fnc') : 'default_index';
$user_id			= get_access_id();

$char = array('=', '&');
$link = str_replace( $char, '/', $query_string );

$viewcode = explode('/', $link);
$num_c = count($viewcode);
$params = array();
for($i = 0; $i < $num_c; $i+=2) {	
	$params[] = $viewcode[$i].':"'.$viewcode[$i+1].'"';
}
$param = count($params) > 0 ? implode(', ', $params) : '';

$data = '{time:"'.$time.'", '.$param.', ip:"'.$ip."'},\n";

$dir1	= BASE_PATH."/log/".date("Y");
$dir2	= BASE_PATH."/log/".date("Y")."/".date("m");
$dir3 	= BASE_PATH."/log/".date("Y")."/".date("m")."/".date("d");	

if(!is_dir($dir1)) {
	mkdir($dir1,0777);
}

if(!is_dir($dir2)) {
	mkdir($dir2,0777);
}

if(!is_dir($dir3)) {
	mkdir($dir3,0777);
}

$filename	= "log-user_id-".$user_id.".txt";
$file = fopen($dir3."/".$filename,"a+");
fwrite($file,$data);
fclose($file);
chmod($dir3."/".$filename,0777);
?>