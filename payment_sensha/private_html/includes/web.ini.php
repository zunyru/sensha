<?php
 defined('WEB_ACCESS') or die('Access ERROR!!');

 /**
  * Core function
  */
 	require_once(BASE_PATH.'/libraries/core.php');
	$Lib = new Libraries();

	$Lib->Load('database.database');
	$Lib->Load('mail.phpmailer');
	$Lib->Load('mail.mail');
	$Lib->Load('file.upload.class');
	$Lib->Load('file.file');
	$Lib->Load('security.login');
	$Lib->Load('security.acl');
	$Lib->Load('security.password');
	$Lib->Load('session');

	$Lib->Load('validator.application');
	$Lib->Load('lang.thai');
	$Lib->Load('interface.sef');
	$Lib->Load('interface.url');
	$Lib->Load('interface.html');
	$Lib->Load('interface.mydatagrid');
	$Lib->Load('interface.bitly');
	$Lib->Load('html.html');
	$Lib->Load('file.ini_manager');
	$Lib->Load('media.youtube');

	$Lib->Inc('functions');
	$Lib->Inc('includes');
	$Lib->Inc('appini');
	$Lib->Inc('apphtml');

 /**
  * Database new instance and connection
  */
//   	$db = ADONewConnection('mysql');
//   	$db->debug = false;
//   	$db->Connect( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
//   	$db->Execute('SET NAMES UTF8');
//
//   	ADODB_Active_Record::SetDatabaseAdapter($db);

	$sc_auth = new securelogin();
	$member_auth = new securelogin();

	$permission_except_pages = array('error', 'index');

	$Acl = new ACL($db, $permission_except_pages);

 /**
  * New instance for password class
  */
  $SYSPwd = new Password(SYS_SECRET_CODE);


 /**
  * Auto Loing if cookie no empty
  */
  auto_login();


 /**
  * Template engine new instance
  */
   $tpl_site 		= new Web_Smarty_Template();
   $tpl_site_app	= new Web_Smarty_Application();

   $tpl_admin 		= new Web_Smarty_Template( 'admin' );
   $tpl_admin_app 	= new Web_Smarty_Application( 'admin' );

   $app_site		= new SC_Application();
   $app_admin 		= new SC_Application( 'admin' );

   $tpl_site_app->register_function('Module', 'LoadModule');
   $tpl_site_app->register_function('SessionMember', 'SessionMember');
   $tpl_site_app->register_function('GetSetting', 'getSysSetting');
   $tpl_site_app->register_function('AppLink', 'appLink');
   $tpl_admin_app->register_function('GetSetting', 'getSysSetting');
   $tpl_admin_app->register_function('ModuleAdmin', 'LoadModuleAdmin');


   /** initial application */
   $App_INI = new App_INI();

 /**
  * XAJAX
  */

	$Not_Use_XAJAX = true;
	if(!$Not_Use_XAJAX){
		require_once(BASE_PATH.'/libraries/xajax/xajaxExtend.php');
		function getLoading(){
			global $objResponse;
			return $objResponse;
		}
		class myXajax   {
			function myXajax( $objClass, $fncSet = array() ){
				global $objResponse, $tpl_site_app;

				$xajax = new xajaxExtend();
				$objResponse = new xajaxResponse();
				$xajax->setRequestURI($_SERVER['REQUEST_URI']);
				$xajax->registerFunction("getLoading");

				for( $i = 0; $i < count( $fncSet ); $i++ ){
					$xajax->registerFunction( array( $fncSet[$i], &$GLOBALS[$objClass] , $fncSet[$i]) );
				}

				$xajax->processRequests();
				//$_SESSION['XAJAX_INI'] = $xajax->getJavascript( BASE_URL.'/libraries/xajax' );
				SetSession('XAJAX_INI', $xajax->getJavascript( BASE_URL.'/libraries/xajax' ));
			}
		}
		if(!GetSession('XAJAX_INI')){ new myXajax(''); }
	}

 /**
  * Set Memebory Limit for query
  */
   ini_set("memory_limit","64M");


define('PRODUCT_LEASING_URL', 'product/leasing.html');
define('PRODUCT_VIP_URL', 'product/vip.html');
define('PRODUCT_EXPORT_URL', 'product/export.html');

$tpl_site_app->assign( 'PRODUCT_LEASING_URL'	, PRODUCT_LEASING_URL );
$tpl_site_app->assign( 'PRODUCT_VIP_URL'	, PRODUCT_VIP_URL );
$tpl_site_app->assign( 'PRODUCT_EXPORT_URL'	, PRODUCT_EXPORT_URL );

$tpl_admin_app->assign( 'PRODUCT_LEASING_URL'	, PRODUCT_LEASING_URL );
$tpl_admin_app->assign( 'PRODUCT_VIP_URL'	, PRODUCT_VIP_URL );
$tpl_admin_app->assign( 'PRODUCT_EXPORT_URL'	, PRODUCT_EXPORT_URL );
?>
