<?php
 
function getSetting($code){
 	
	$db = Database::getInstance();
			
	$sql_param = array();
	array_push($sql_param, $code);
				
 	$sql = " SELECT value FROM ".DB_PREFIX.'setting'." WHERE active = '1' AND code = ? LIMIT 1 ";
	$rs= $db->GetArray($sql,$sql_param);
	
	return $rs[0]['value'];
	
}
 
function getSysSetting( $params ){
	 
 	$db = Database::getInstance();
 	$sql = "
	SELECT title,value 
	FROM ".DB_PREFIX.'setting'."
	 WHERE active = '1' 
	 AND code = '".$params['code']."'
	 LIMIT 1
	";
	$rs= $db->GetArray( $sql );
	switch($params['type']):
		case 'title':
			return $rs[0]['title'];
			break;
		case 'value':
			return $rs[0]['value'];
			break;
		default:
			return "";
			break;
	endswitch;	
}
 
/*
* set page title 
* type , 1 = order by site_name, value
* type , 2 = order by value, site_name
* type , 3 = only value
* type , 4 = sitename + colon + value
*/
function setPageTitle( $value = '', $type = 0){
 	if( $value && $type == 1 ){
		$page_title = getSetting('site_name_'.GetLangID()).' , '.$value;
	}
	elseif( $value && $type == 2 ){
		$page_title = $value.' , '.getSetting('site_name_'.GetLangID()) ;
	}
	elseif( $value && $type == 3){
		$page_title = $value;
	}
	elseif( $value && $type == 4){
		$page_title = getSetting('site_name_'.GetLangID()).' : '.$value ;
	}
	else{
		$page_title = getSetting('site_name_'.GetLangID());
	} 
 	return $page_title;
}
 
/*
* set page keyword 
* type , 1 = order by keyword, value
* type , 2 = order by value, keyword
* type , 3 = only value
*/
function setPageKeyword( $value = '', $type = 0){
 	if( $value && $type == 1 ){
		$keyword = getSetting('site_meta_keyword_'.GetLangID()).' , '.$value;
	}
	elseif( $value && $type == 2 ){
		$keyword = $value.' , '.getSetting('site_meta_keyword_'.GetLangID()) ;
	}
	elseif( $value && $type == 3){
		$keyword = $value;
	}
	else{
		$keyword = getSetting('site_meta_keyword_'.GetLangID());
	} 
 	return $keyword;
}
 
/*
* set page description 
* type , 1 = order by description, value
* type , 2 = order by value, description
* type , 3 = only value
*/
function setPageDescription( $value = '', $type = 0){
 	if( $value && $type == 1 ){
		$description = getSetting('site_meta_description_'.GetLangID()).' '.$value;
	}
	elseif( $value && $type == 2 ){
		$description = $value.' , '.getSetting('site_meta_description_'.GetLangID()) ;
	}
	elseif( $value && $type == 3){
		$description = $value;
	}
	else{
		$description = getSetting('site_meta_description_'.GetLangID());
	} 
 	return $description;
}
 
function getTableName($name){
 	return DB_PREFIX.$name;
}
 
function dbTable($name){
 	return DB_PREFIX.$name;
}

function appLink($params){
	$link = $params['value'];
	if(WEB_PAGE == 1){
		return $link;
	}
	else{
		return URL::_($link);
	}
}

function iconv_web($f) {
	$f = trim($f);
	$f = str_replace(array('"','�','�'),'&quot;',$f);
	$f = iconv('TIS-620','UTF-8',$f);
	return $f;
}

function iconv_web_case($f,$f_name,$f_edit_arr) {
	$f = trim($f);
	$f = in_array($f_name,$f_edit_arr) ? $f : str_replace(array('"','�','�'),'&quot;',$f);
	$f = iconv('TIS-620','UTF-8',$f);
	return $f;
}

function iconv_db($f) {
	$f = str_replace("'","''",$f);
	$f = iconv('UTF-8','TIS-620',$f);
	return $f;
}

function date_to_db($date, $lang = ''){
	if( $date ){
		$_date = explode('/', $date);
		return ($lang == 'th' ? $_date[2]-543 : $_date[0]).'-'.$_date[1].'-'.$_date[0];
	}
}

function date_to_web($date, $lang = ''){
	if( $date ){
		$_date = explode('-', $date);
		return $_date[2].'/'.$_date[1].'/'.($lang == 'th' ? $_date[0]+543 : $_date[0]);
	}
}
 
function getDataToHtml($value) {
	return htmlspecialchars(strip_tags($value));
}
 
function getDataToHtmlArray($arr) {
	foreach ($arr as $key => $value) {
		$arr[$key] = htmlspecialchars(strip_tags($value));
	}
	return $arr;
}

?>