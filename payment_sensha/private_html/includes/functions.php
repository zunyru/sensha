<?PHP
	function tis2utf8($tis) {
		for( $i=0 ; $i< strlen($tis) ; $i++ ){
		$s = substr($tis, $i, 1);
		$val = ord($s);
		if ($val < 0x80){
		$utf8 .= $s;
		} elseif ( ( 0xA1 <= $val and $val <= 0xDA ) or ( 0xDF <= $val and $val <= 0xFB ) ){
		$unicode = 0x0E00 + $val - 0xA0;
		$utf8 .= chr( 0xE0 | ($unicode >> 12) );
		$utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
		$utf8 .= chr( 0x80 | ($unicode & 0x3F) );
		}
		}
		return $utf8;
	}

	function aliasRender($obj_input, $value){
		global $objResponse;

		$char1 = array(' ', '/');
		$char2 = array('\'', '"', '>', '<', '!', '@', '#', '$', '%', '^', '&', '*', '.');
		$title = trim($value);
		$title = str_replace( $char1, '-', $title );
		$title = str_replace( $char2, '', $title );
		$objResponse->addAssign($obj_input ,"value", $title);

		return $objResponse;
	}

	function read_dir( $dir_path ){

		$arr_filename = array();

		$dirOpen 	= opendir( $dir_path );
		while($fullName = readdir($dirOpen)){
			if(($fullName != ".") && ($fullName != "..") && ($fullName != "Thumbs.db")){
				array_push($arr_filename, $fullName);
			}
		}

		closedir($dirOpen);
		return $arr_filename;
	}

	function directoryList($start,$win32=false){
	   if($win32){
		   $slash="\\";
	   }else{
		   $slash="/";
	   }
	   $basename = pathinfo($start);
	   $basename = $basename['basename'];
	   $ls=array();
	   $dir = dir($start);
	   while($item = $dir->read()){
		   if(is_dir($start.$slash.$item)&& $item!="." && $item!=".."){
			   $ls[$basename][]=directoryList($start.$slash.$item,$win32);
		   }else{
			   if($item!="."&&$item!=".."){
				   $ls[$basename][]=$item;
			   }
		   }
	   }
	   return $ls;
	}

	function DateFormat($date, $format = '', $lang = ''){
		$month_name_list = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");

	    // 2008-12-16 00:54:21
		$d = (int)substr($date, 8, 2);
		$m1 = substr($date, 5, 2);
		$m2 = $month_name_list[$m1-1];

		if( $lang == 'en' ){
			$y1 = substr($date, 0, 4);
			$y2 = substr($date, 2, 2);
		}
		else{
			$y1 = substr($date, 0, 4) + 543;
			$y2 = substr($date, 2, 2) + 43;
		}

		//$y1 = (GetLangCode() == 'th' || $lang == 'th') ? substr($date, 0, 4) + 543 : substr($date, 0, 4); // 4 digit
		//$y2 = (GetLangCode() == 'th' || $lang == 'th') ? substr($date, 2, 2) + 43 : substr($date, 2, 2); // 2 digit

		$h = substr($date, 11, 2);
		$i = substr($date, 14, 2);
		$s = substr($date, 17, 2);
		$D = date('D', mktime(0, 0, 0, $m1  , $d, substr($date, 0, 4)));
		$d = date('d', mktime(0, 0, 0, $m1  , $d, substr($date, 0, 4)));
		$w = date('w', mktime(0, 0, 0, $m1  , $d, substr($date, 0, 4)));
		$m3 = date('M', mktime(0, 0, 0, $m1  , $d, substr($date, 0, 4)));

		if($format != ''){
			$format = str_replace('%D', $D, $format);
			$format = str_replace('%d', $d, $format);
			$format = str_replace('%m', $m1, $format);
			if( $lang == 'en' ){
				$format = str_replace('%M', $m3, $format);
			}
			else{
				$format = str_replace('%M', $m2, $format);
			}
			$format = str_replace('%y', $y2, $format);
			$format = str_replace('%Y', $y1, $format);
			$format = str_replace('%H', $h, $format);
			$format = str_replace('%i', $i, $format);
			$format = str_replace('%s', $s, $format);
			$format = str_replace('%w', $w, $format);
			$date = $format;
		}

		return $date;
	}
	
	function DateEngtoDB($date,$format='date') {
		if($date) {
			$arr 	= explode('/',$date);
			$day 	= $arr[0];
			$month 	= $arr[1];
			$year 	= $arr[2];
			return $year.'-'.$month.'-'.$day;
		}
		else {
			return '';
		}
	}
	
	function DateThaitoDB($date,$format='date') {
		if($date) {
			$arr 	= explode('/',$date);
			$day 	= $arr[0];
			$month 	= $arr[1];
			$year 	= $arr[2]-543;
			return $year.'-'.$month.'-'.$day;
		}
		else {
			return '';
		}
	}

	function DateThaiFull($date, $format = ''){
		$arrMonthName =	array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรฎาคม"
								,"สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");

		$arrDate = explode('-', $date);
		$day 	 = $arrDate[2];
		$month	 = $arrDate[1]-1;
		$year	 = $arrDate[0]+543;

		$timeSet = explode(' ', $date);
		$arrTime = explode(':', $timeSet[1]);
		if(count($arrTime) != 0){
			$time	 = "[".$arrTime[0].":".$arrTime[1].":".$arrTime[2]."]";
		}
		if(count($arrDate) != 3 ){
			return "-";
		}
		else{
			switch($format):
				case 1:
					return (int)$day." ".$arrMonthName[(int)$month]." ".$year;
					break;
				case 'time':
					return $time;
					break;
				default:
					return (int)$day." ".$arrMonthName[(int)$month]." ".$year." ".$time;
					break;
			endswitch;
		}
	}

	function DateThai($date){
		$time = date("H:i:s");
		$day		= 	substr("$date",8,2);
		$month 		=	substr("$date",5,2);
	//	$month 		=   (int)$month - 1;
		$year1 		= 	substr("$date",0,4);
		$year 		= 	$year1 + 543;
		$thaimonth 	= 	array("..","..","..","..","..","..","..","..","..","..","..","..");
	//	$month		= 	$thaimonth[$month];

		if(!$date){
			return "-";//(".$time.")";
		}
		elseif($day == 0 && $month == 0 && $year1 == 0){
			return "-";//(".$time.")";
		}
		else{
			return (int)$day."/".$month."/".$year;//." (".$time.")";
		}
	}

	function getThaiDayShort($iDay){
		$day = array('อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.');
		return $day[$iDay];
	}

	function email_validator( $mail ){
		if( !(preg_match('!@.*@|\.\.|\,|\;!', $mail) || !preg_match('!^.+\@(\[?)[a-zA-Z0-9\.\-]+\.([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$!', $mail))  ){
			return true;
		}
		else{
			return false;
		}
	}

	function username_validator( $value ){
		if( !preg_match("/^[a-zA-Z0-9]+$/i", $value) ) {
			return true;
		}
		else{
			return false;
		}
	}

	 function findEmoIcon( $value ){

		$value = str_replace('{emo','<img src="'.BASE_URL.'/images/emotion/emo',$value);
		$value = str_replace('}','.gif"  align="absmiddle">',$value);
		return $value;
	}

	function showEmoIcon(){

		$dir_path = BASE_PATH."/images/emotion";

		$dirOpen 	= opendir( $dir_path );
				$icon .= "<div style='float: none;position:relative;padding:0px 0px 0px 0px;'>";
		while($fullName = readdir($dirOpen)){
			if(($fullName != ".") && ($fullName != "..") && ($fullName != "Thumbs.db")){
				$icon_name = explode('.', $fullName);
				$icon .= "<span style='padding:5px;'>";
				$icon .= "<a href=\"javascript:emoticon('{".$icon_name[0]."}')\">";
				$icon .= "<img  src='".BASE_URL."/images/emotion/".$fullName."' border='0' alt='".$fullName."'>";
				$icon .= "</a>";
				$icon .= "</span>";
			}
		}
				$icon .= "</div>";

		closedir($dirOpen);
		return $icon;
  }

  function hiddenIP( $ip, $user_id = 0 ){
  	if($user_id){
		$oUser = new DBTable('user', 'UsrID');
		$rsUser = $oUser->select($user_id);

		$oGUser = new DBTable('user_group', 'UsrGID');
		$rsGUser = $oGUser->select($rsUser->USRGID);
    }

  	if($rsGUser->USRGTID == 1 || $rsUser->USRGID == 1 || $rsUser->USRGID == 2 || $rsUser->USRGID == 3 || $rsUser->USRGID == 4 || $rsUser->USRGID == 13 || $rsUser->USRGID == 14){
		return "n/a";
	}
	else{
		$arrIP = explode('.', $ip);
		if( count( $arrIP ) == 4 ){
			$arrIP[3] = "xxx";
			return implode('.', $arrIP);
		}
		else{
			return " - ";
		}
	}
  }

 function wb_replace ($body){

	$body = eregi_replace ( "(\[url])(http://[^{{space}}<>]{3,})(\[/url])", "<a href=\"\\2\" target=_blank>\\2</a>" , $body ) ;
	$body = eregi_replace ( "(\[img])(http://[^{{space}}<>]{3,})(\[/img])", "<div align=center><img src=\"\\2\" border=\"0\"></div>" , $body ) ;
	$body = eregi_replace ( "\[---]", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" , $body ) ;
	$body = eregi_replace ( "\[b]", "<b>" , $body ) ;
	$body = eregi_replace ( "\[/b]", "</b>" , $body ) ;
	$body = eregi_replace ( "\[i]", "<i>" , $body ) ;
	$body = eregi_replace ( "\[/i]", "</i>" , $body ) ;
	$body = eregi_replace ( "\[u]", "<u>" , $body ) ;
	$body = eregi_replace ( "\[/u]", "</u>" , $body ) ;
	$body = eregi_replace ( "\[sub]", "<sub>" , $body ) ;
	$body = eregi_replace ( "\[/sub]", "</sub>" , $body ) ;
	$body = eregi_replace ( "\[sup]", "<sup>" , $body ) ;
	$body = eregi_replace ( "\[/sup]", "</sup>" , $body ) ;
	$body = eregi_replace ( "\[glow]","<table style=filter:glow(color=#33ccff, strength=3)>", $body ) ;
	$body = eregi_replace ( "\[/glow]", "</table>" , $body ) ;
	$body = eregi_replace ( "\[shadow]","<table style=filter:shadow(color=#33ccff, direction=left)>", $body ) ;
	$body = eregi_replace ( "\[/shadow]", "</table>" , $body ) ;
	$body = eregi_replace ( "\[list]", "<li>" , $body ) ;
	$body = eregi_replace ( "\[/list]", "</li>" , $body ) ;
	$body = eregi_replace ( "\[quote]", "<table width=95% border=1 bordercolor=#EBEBEB  align=center cellpadding=3 cellspacing=0 class=fontsmall style=\"border-collapse: collapse\"><tr><td bgcolor=#EBEBEB><font color=#808080>Quote :</font></td></tr><tr><td><font color=#808080>" , $body ) ;
	$body = eregi_replace ( "\[/quote]", "</font></td></tr></table>" , $body ) ;
	$body = eregi_replace ( "\[code]", "<table width=95% border=1 bordercolor=#EBEBEB  align=center cellpadding=3 cellspacing=0 class=fontsmall style=\"border-collapse: collapse\"><tr><td bgcolor=#EBEBEB><font color=#808080>Code :</font></td></tr><tr><td><font color=#808080>" , $body ) ;
	$body = eregi_replace ( "\[/code]", "</font></td></tr></table>" , $body ) ;
	$body = eregi_replace ( "\[color=darkred]", "<font color=darkred>" , $body ) ;
	$body = eregi_replace ( "\[color=red]", "<font color=red>" , $body ) ;
	$body = eregi_replace ( "\[color=orange]", "<font color=orange>" , $body ) ;
	$body = eregi_replace ( "\[color=brown]", "<font color=brown>" , $body ) ;
	$body = eregi_replace ( "\[color=yellow]", "<font color=yellow>" , $body ) ;
	$body = eregi_replace ( "\[color=green]", "<font color=green>" , $body ) ;
	$body = eregi_replace ( "\[color=olive]", "<font color=olive>" , $body ) ;
	$body = eregi_replace ( "\[color=cyan]", "<font color=cyan>" , $body ) ;
	$body = eregi_replace ( "\[color=blue]", "<font color=blue>" , $body ) ;
	$body = eregi_replace ( "\[color=darkblue]", "<font color=darkblue>" , $body ) ;
	$body = eregi_replace ( "\[color=indigo]", "<font color=indigo>" , $body ) ;
	$body = eregi_replace ( "\[color=violet]", "<font color=violet>" , $body ) ;
	$body = eregi_replace ( "\[color=white]", "<font color=white>" , $body ) ;
	$body = eregi_replace ( "\[color=black]", "<font color=black>" , $body ) ;
	$body = eregi_replace ( "\[color=gray]", "<font color=gray>" , $body ) ;
	$body = eregi_replace ( "\[/color]", "</font>" , $body ) ;
	$body = eregi_replace ( "\[blockquote]", "<table width=95% border=1 bordercolor=#EBEBEB  align=center cellpadding=3 cellspacing=0 class=fontsmall style=\"border-collapse: collapse\"><tr><td bgcolor=#EBEBEB><font color=#808080>Quote :</font></td></tr><tr><td><font color=#808080>" , $body ) ;
	$body = eregi_replace ( "\[/blockquote]", "</td></tr></table>" , $body ) ;
	//$body = eregi_replace ( "\[blockquote]", "<blockquote>" , $body ) ;
	//$body = eregi_replace ( "\[/blockquote]", "</blockquote>" , $body ) ;
	$body = eregi_replace ( "\[hr]", "<hr border=1 width=100%>" , $body ) ;

	$body = eregi_replace(chr(13),"<br>", $body);
	// - - - - - - - - - - Emoticon - - - - - - - - - -
	/*
	$body = eregi_replace ( ":emo01:", "<img src=icon/emoticon/emo01.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo02:", "<img src=icon/emoticon/emo02.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo03:", "<img src=icon/emoticon/emo03.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo04:", "<img src=icon/emoticon/emo04.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo05:", "<img src=icon/emoticon/emo05.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo06:", "<img src=icon/emoticon/emo06.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo07:", "<img src=icon/emoticon/emo07.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo08:", "<img src=icon/emoticon/emo08.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo09:", "<img src=icon/emoticon/emo09.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo10:", "<img src=icon/emoticon/emo10.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo11:", "<img src=icon/emoticon/emo11.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo12:", "<img src=icon/emoticon/emo12.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo13:", "<img src=icon/emoticon/emo13.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo14:", "<img src=icon/emoticon/emo14.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo15:", "<img src=icon/emoticon/emo15.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo16:", "<img src=icon/emoticon/emo16.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo17:", "<img src=icon/emoticon/emo17.gif width=20 height=20 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo18:", "<img src=icon/emoticon/emo18.gif width=20 height=20 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo19:", "<img src=icon/emoticon/emo19.gif width=20 height=20 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo20:", "<img src=icon/emoticon/emo20.gif width=20 height=20 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo21:", "<img src=icon/emoticon/emo21.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo22:", "<img src=icon/emoticon/emo22.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo23:", "<img src=icon/emoticon/emo23.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo24:", "<img src=icon/emoticon/emo24.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo25:", "<img src=icon/emoticon/emo25.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo26:", "<img src=icon/emoticon/emo26.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo27:", "<img src=icon/emoticon/emo27.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo28:", "<img src=icon/emoticon/emo28.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo29:", "<img src=icon/emoticon/emo29.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo30:", "<img src=icon/emoticon/emo30.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo31:", "<img src=icon/emoticon/emo31.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo32:", "<img src=icon/emoticon/emo32.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo33:", "<img src=icon/emoticon/emo33.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo34:", "<img src=icon/emoticon/emo34.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo35:", "<img src=icon/emoticon/emo35.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo36:", "<img src=icon/emoticon/emo36.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo37:", "<img src=icon/emoticon/emo37.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo38:", "<img src=icon/emoticon/emo38.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo39:", "<img src=icon/emoticon/emo39.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo40:", "<img src=icon/emoticon/emo40.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo41:", "<img src=icon/emoticon/emo41.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo42:", "<img src=icon/emoticon/emo42.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo43:", "<img src=icon/emoticon/emo43.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo44:", "<img src=icon/emoticon/emo44.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo45:", "<img src=icon/emoticon/emo45.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo46:", "<img src=icon/emoticon/emo46.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo47:", "<img src=icon/emoticon/emo47.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo48:", "<img src=icon/emoticon/emo48.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo49:", "<img src=icon/emoticon/emo49.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	$body = eregi_replace ( ":emo50:", "<img src=icon/emoticon/emo50.gif width=19 height=19 border=0 align=absmiddle hspace=2 vspace=2>" , $body ) ;
	*/

	return $body;

}

 function redirect( $url, $msg='', $msgtype='' ) {
	if (trim( $msg )) {
	 	if (strpos( $url, '?' )) {
			$url .= '&kmsmsg=' . urlencode( $msg ).'&msgtype=' . $msgtype;
		} else {
			$url .= '?kmsmsg=' . urlencode( $msg ).'&msgtype=' . $msgtype;
		}
	}

	if (headers_sent()) {
		echo "<script>document.location.href='$url';</script>\n";
	} else {
		@ob_end_clean(); // clear output buffer
		header( 'HTTP/1.1 301 Moved Permanently' );
		header( "Location: ". $url );
	}
	exit();
 }

 function byteToKByte($byte){
	$kb = number_format(($byte/1024),2,'.','');
	return $kb;
 }

 function byteToMByte($byte){
	$mb = number_format((($byte/1024)/1024),2,'.','');
	return $mb;
 }

 function kByteToMByte($kByte){
	$mb = number_format(($kByte/1024),2,'.','');
	return $mb;
 }

function question_Rating( $view, $repied ){
	global $kmsConFTopicView_limit;
	global $kmsConFTopicRepied_limit;
	if( $view >= $kmsConFTopicView_limit or $repied >= $kmsConFTopicRepied_limit ){
			$icon = "<img src='".$kmsConFBaseURL."/images/icon_hot.gif'>";
	}
	else{
		$icon = "<img src='".$kmsConFBaseURL."/images/icon_new.gif'>";
	}
	return $icon;
}

function blankReplace($data){
	$data = '&'.str_replace(' ', '+', $data);
	return $data;
}

function Message_Red($message){
	return '<span class="red">'.$message.'</span>';
}

function Message_Green($message){
	return '<span class="green">'.$message.'</span>';
}

function Message_Blue($message){
	return '<span class="blue">'.$message.'</span>';
}

function Message_Orange($message){
	return '<span class="orange">'.$message.'</span>';
}

function show_no_picture(){
	return '<img src="'.BASE_URL.'/images/noimage.png" title="No Image">';
}

function genCodeFormat($value, $digit_count){

	$len = strlen($value);
	$code = "";
	for($i = 0; $i < $digit_count; $i++){
		$code .= "0";
	}
	$code = substr($code, $len);
	$code = $len>=$digit_count ? $value : $code.$value;

	return $code;
}


function get_file_icon($file){
	switch( $file ){
		//case "text/html": $file = "html.png"; $alt = "HTML File"; break;
		//case "text/shtml": $file = "html.png"; $alt = "HTML File"; break;
		case "image/pjpeg": $file = "image.png"; $alt = "Progressive Jpeg Image"; break;
		case "image/jpeg":$file = "image.png"; $alt = "Jpeg Image"; break;
		case "image/png": $file = "image.png"; $alt = "PNG Image"; break;
		case "image/gif": $file = "image.png"; $alt = "GIF Image"; break;
		case "image/tiff": $file = "image.png"; $alt = "Tiff Image"; break;
		case "application/pdf": $file = "pdf.png"; $alt = "PDF File"; break;
		case "application/word": $file = "word.png"; $alt = "Word Document"; break;
		case "application/octet-stream": $file = "word.png"; $alt = "Word Document"; break;
		case "application/msword": $file = "word.png"; $alt = "Word Document"; break;
		case "application/powerpoint": $file = "powerpoint.png"; $alt = "Excel Spreadsheet"; break;
		case "application/excel": $file = "excel.png"; $alt = "Powerpoint Presentation"; break;
		case "text/plain": $file = "txt.png"; $alt = "Plain Text File"; break;
		case "application/zip": $file = "zip.png"; $alt = "Zip Archive"; break;
		//case "application/x-stuffit": $file = "sit.png"; $alt = "Stuffit Archive"; break;
		//case "application/x-msdownload": $file = "exe.png"; $alt = "Executable"; break;
		default: $file = "file.png"; $alt = "Unknown Tile Type"; break;
	}
	$imgTag = "<img src=\"".BASE_URL."/images/filetype/".$file."\" alt=\"".$alt."\" align=\"absmiddle\" />";

	return $imgTag;
}

function get_mimetype($value='') {

$ct['htm'] = 'text/html';
$ct['html'] = 'text/html';
$ct['txt'] = 'text/plain';
$ct['asc'] = 'text/plain';
$ct['bmp'] = 'image/bmp';
$ct['gif'] = 'image/gif';
$ct['jpeg'] = 'image/jpeg';
$ct['jpg'] = 'image/jpeg';
$ct['jpe'] = 'image/jpeg';
$ct['png'] = 'image/png';
$ct['ico'] = 'image/vnd.microsoft.icon';
$ct['mpeg'] = 'video/mpeg';
$ct['mpg'] = 'video/mpeg';
$ct['mpe'] = 'video/mpeg';
$ct['qt'] = 'video/quicktime';
$ct['mov'] = 'video/quicktime';
$ct['avi'] = 'video/x-msvideo';
$ct['wmv'] = 'video/x-ms-wmv';
$ct['mp2'] = 'audio/mpeg';
$ct['mp3'] = 'audio/mpeg';
$ct['rm'] = 'audio/x-pn-realaudio';
$ct['ram'] = 'audio/x-pn-realaudio';
$ct['rpm'] = 'audio/x-pn-realaudio-plugin';
$ct['ra'] = 'audio/x-realaudio';
$ct['wav'] = 'audio/x-wav';
$ct['css'] = 'text/css';
$ct['zip'] = 'application/zip';
$ct['pdf'] = 'application/pdf';
$ct['doc'] = 'application/msword';
$ct['docx'] = 'application/octet-stream';
$ct['bin'] = 'application/octet-stream';
$ct['exe'] = 'application/octet-stream';
$ct['class']= 'application/octet-stream';
$ct['dll'] = 'application/octet-stream';
$ct['xls'] = 'application/vnd.ms-excel';
$ct['ppt'] = 'application/vnd.ms-powerpoint';
$ct['wbxml']= 'application/vnd.wap.wbxml';
$ct['wmlc'] = 'application/vnd.wap.wmlc';
$ct['wmlsc']= 'application/vnd.wap.wmlscriptc';
$ct['dvi'] = 'application/x-dvi';
$ct['spl'] = 'application/x-futuresplash';
$ct['gtar'] = 'application/x-gtar';
$ct['gzip'] = 'application/x-gzip';
$ct['js'] = 'application/x-javascript';
$ct['swf'] = 'application/x-shockwave-flash';
$ct['tar'] = 'application/x-tar';
$ct['xhtml']= 'application/xhtml+xml';
$ct['au'] = 'audio/basic';
$ct['snd'] = 'audio/basic';
$ct['midi'] = 'audio/midi';
$ct['mid'] = 'audio/midi';
$ct['m3u'] = 'audio/x-mpegurl';
$ct['tiff'] = 'image/tiff';
$ct['tif'] = 'image/tiff';
$ct['rtf'] = 'text/rtf';
$ct['wml'] = 'text/vnd.wap.wml';
$ct['wmls'] = 'text/vnd.wap.wmlscript';
$ct['xsl'] = 'text/xml';
$ct['xml'] = 'text/xml';

//$extension = get_file_extension($value);

if (!$type = $ct[strtolower($value)]) {

$type = 'text/html';
}

return $type;

}

function getMemberLevelIcon($level){
	if(is_file(BASE_PATH.'/images/member_level/'.$level.'.png')){
		return BaseHtml::showPicture(BASE_URL.'/images/member_level/'.$level.'.png');
	}
	else{
		return "";
	}
}

function auto_login(){
	global $db, $SYSPwd, $sc_member_secret;

	/*
	$o_member 	= new DBTable('member', 'member_id');

	if(GetSession('MEMBER_LOGEDIN_ID')) {

		$member_logedin_id 	= Application::decode(GetSession('MEMBER_LOGEDIN_ID'));
		$member 			= $o_member->select($member_logedin_id);

		if(!($member['member_id'] && $member['active']=='1' && $member['publish']=='1')){
			setcookie("MEMBER_IHEREUN_CKEY", "", time()-3600, '/');
			UnSetSession('MEMBER_LOGEDIN_ID');
		}

	}
	else if(!empty($_COOKIE['MEMBER_IHEREUN_CKEY'])) {

		$member = $o_member->select($SYSPwd->decrypt($_COOKIE['MEMBER_IHEREUN_CKEY']),'username');

		if($member['member_id'] && $member['active']=='1' && $member['publish']=='1'){
			$time = time()+3600*24*356;
			setcookie("MEMBER_IHEREUN_CKEY" , $SYSPwd->encrypt($member['username']), $time, '/');
			SetSession('MEMBER_LOGEDIN_ID', Application::encode($member['member_id']));
		}
		else {
			setcookie("MEMBER_IHEREUN_CKEY", "", time()-3600, '/');
			UnSetSession('MEMBER_LOGEDIN_ID');
		}

	}
	*/

}

function strlen_thai($str, $encoding = ''){
	$t = array('่', '้', '๊', '๋', 'ั', '์', 'ิ', 'ี', 'ึ', 'ื', 'ุ', 'ู', 'ำ');
	$t_str = str_replace($t, '', $str);
	$t_str_len = mb_strlen($t_str, ($encoding ? $encoding : 'UTF-8'));

	return $t_str_len;
}

function calage($pbday){
	$today = date("Y-m-d");
	list($byear ,$bmonth ,$bady) = explode("-" , $pbday);
	list($tyear ,$tmonth ,$tday ) = explode("-" , $today);

 	if($byear < 1970){
  		$yearad =1970 - $byear;
 		$byear =1970;
  	}
	else{
  		$yearad = 0;
	}

 	$mbirth = mktime(0,0,0,$bmonth,$bday,$byear);
  	$mnow = mktime(0,0,0,$tmonth,$tday,$tyear);
  	$mage= ($mnow - $mbirth);
 	$age_year = (date("Y",$mage)-1970 + $yearad);
	$age_month = (date("m", $mage)-1);
	$age_day = (date("d", $mage)-1);

	$ages['year'] = $age_year;
	$ages['month'] = $age_month;
	$ages['day'] = $age_day;

	return $ages;
}
//function date_diff($strDate1,$strDate2){
function zen_date_diff($strDate1,$strDate2){
	return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}

function time_diff($strTime1,$strTime2){
	return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

function date_time_diff($strDateTime1,$strDateTime2){
	return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}

if(!function_exists('json_encode')){
	function json_encode($data){
		return my_json_encode( $data );
	}
}

function my_json_encode( $data ) {
    if( is_array($data) || is_object($data) ) {
        $islist = is_array($data) && ( empty($data) || array_keys($data) === range(0,count($data)-1) );

        if( $islist ) {
            $json = '[' . implode(',', array_map('my_json_encode', $data) ) . ']';
        } else {
            $items = Array();
            foreach( $data as $key => $value ) {
                $items[] = my_json_encode("$key") . ':' . my_json_encode($value);
            }
            $json = '{' . implode(',', $items) . '}';
        }
    } elseif( is_string($data) ) {
        # Escape non-printable or Non-ASCII characters.
        # I also put the \\ character first, as suggested in comments on the 'addclashes' page.
        $string = '"' . addcslashes($data, "\\\"\n\r\t/" . chr(8) . chr(12)) . '"';
        $json    = '';
        $len    = strlen($string);
        # Convert UTF-8 to Hexadecimal Codepoints.
        for( $i = 0; $i < $len; $i++ ) {

            $char = $string[$i];
            $c1 = ord($char);

            # Single byte;
            if( $c1 <128 ) {
                $json .= ($c1 > 31) ? $char : sprintf("\\u%04x", $c1);
                continue;
            }

            # Double byte
            $c2 = ord($string[++$i]);
            if ( ($c1 & 32) === 0 ) {
                $json .= sprintf("\\u%04x", ($c1 - 192) * 64 + $c2 - 128);
                continue;
            }

            # Triple
            $c3 = ord($string[++$i]);
            if( ($c1 & 16) === 0 ) {
                $json .= sprintf("\\u%04x", (($c1 - 224) <<12) + (($c2 - 128) << 6) + ($c3 - 128));
                continue;
            }

            # Quadruple
            $c4 = ord($string[++$i]);
            if( ($c1 & 8 ) === 0 ) {
                $u = (($c1 & 15) << 2) + (($c2>>4) & 3) - 1;

                $w1 = (54<<10) + ($u<<6) + (($c2 & 15) << 2) + (($c3>>4) & 3);
                $w2 = (55<<10) + (($c3 & 15)<<6) + ($c4-128);
                $json .= sprintf("\\u%04x\\u%04x", $w1, $w2);
            }
        }
    } else {
        # int, floats, bools, null
        $json = strtolower(var_export( $data, true ));
    }
    return $json;
}

function getRealIpAddr() {

    if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
		//check ip from share internet
      	$ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    	//to check ip is pass from proxy
      	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
      	$ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}


?>
