<?php
	class App_HTML {
		
		var $db;
		
		function App_HTML(){
			$db = Database::getInstance();
			
			$this->db = $db;
		}
		
		function getCheckboxList( $i, $value='', $id='', $fnc = '' ){
			$selected = $value == $id ? 'checked="checked"' : '';
			return '<input type="checkbox" id="cb'.$i.'" name="itemID[]" class="itemID" value="'.$value.'" onclick="'.($fnc ? $fnc.'(this.checked);' : 'isChecked(this.checked);').'" '.$selected.'  />';
		}
		
		function makeCheckbox( $i, $name, $value='', $id='', $fnc = '' ){
			$selected = $value == $id ? 'checked="checked"' : '';
			return '<input type="checkbox" id="cb'.$i.'" name="itemID_'.$name.'[]" class="itemID" value="'.$value.'" onclick="'.($fnc ? $fnc.'(this.checked);' : 'isChecked(this.checked);').'" '.$selected.'  />';
		}
		
		/* set table name from db prefix and table name */
		function getTableName( $tablename ){
			return DB_PREFIX.$tablename;
		}
		
		function setLink($title, $link, $icon = ''){
			return ($icon ? $icon.'&nbsp;' : '').'<a href="'.$link.'" title="'.$title.'">'.$title.'</a>';
		}
		
		function getAdminIcon($icon, $title = ''){
			return '<img src="'.BASE_URL_ADMIN.'/images/icon/'.$icon.'" alt="'.$title.'" align="absmiddle">';
		}
	}
?>