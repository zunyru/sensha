<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{
    var $user_lang;
    var $coutry_iso;

    function __construct()
    {
        parent::__construct();
        $this->load->model('database/datacontrol_model');
        // delete_cookie('user_language');
        // $this->user_lang = get_cookie('user_language');
        $this->user_lang = 'Global';
        $this->coutry_iso = "";

        if (!$this->ion_auth->logged_in()) {
            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page') {
                $this->db->where('iso', strtoupper($this->uri->segment(1)));
                $r = $this->datacontrol_model->getRowData('countries');

                //zun lang
                $lang = strtolower($r->nicename);
                $lang = str_replace(" ", "_", $lang);
                $this->user_lang = $lang;
                $this->coutry_iso = strtolower($r->iso);
            }
        } else {
            if ($this->session->has_userdata('user_language')) {
                $this->user_lang = $this->session->userdata('user_language');
            }

            $this->db->where('nicename', $this->user_lang);
            $r = $this->datacontrol_model->getRowData('countries');
            $this->coutry_iso = strtolower($r->iso);

            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso) {
                $segs = $this->uri->segment_array();
                $segs[1] = $this->coutry_iso;
                // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
                redirect(base_url($segs), 'refresh');
            }
        }

        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
            $this->coutry_iso = "";
        }

        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user_country = $this->ion_auth->user()->row()->country;
        if ($this->ion_auth->logged_in()) {
            $this->db->where('country', $user_country)->or_where('country', 'Global');
        } else {
            $this->db->where('country', 'Global');
        }
        $this->db->limit(10);
        $this->db->order_by('id', 'desc');
        $data['news_post'] = $this->datacontrol_model->getAllData('news_post');

        $this->db->where('country', $this->user_lang);
        $data['page_content'] = $this->datacontrol_model->getRowData('page_home');
        $data['htmlTitle'] = $data['page_content']->meta_title;
        $data['htmlDes'] = $data['page_content']->meta_description;

        $this->db->where('is_show_home', 1);
        if ($this->ion_auth->logged_in()) {
            $this->db->where('product_country', $user_country);
        } else {
            $this->db->where('product_country', 'Global');
        }
        $this->db->limit(5);
        $data['home_products'] = $this->datacontrol_model->getAllData('product_general');

        $data['coutry_iso'] = $this->coutry_iso . "/";

        $data['page'] = 'home_v';
        $this->load->view('front_template', $data);
    }

    public function about()
    {
        $this->db->where('country', $this->user_lang);
        $data['page_content'] = $this->datacontrol_model->getRowData('page_about');
        $data['htmlTitle'] = $data['page_content']->meta_title;
        $data['htmlDes'] = $data['page_content']->meta_description;

        $data['coutry_iso'] = $this->coutry_iso . "/";

        $data['page'] = 'about_v';
        $this->load->view('front_template', $data);
    }

    public function about_ppf()
    {
        $this->db->where('country', $this->user_lang);
        $data['page_content'] = $this->datacontrol_model->getRowData('page_ppf');
        $data['htmlTitle'] = $data['page_content']->meta_title;
        $data['htmlDes'] = $data['page_content']->meta_description;

        $data['coutry_iso'] = $this->coutry_iso . "/";

        $data['page'] = 'ppf_v';
        $this->load->view('front_template', $data);
    }

    public function agent()
    {
        $this->db->where('country', $this->user_lang);
        $data['page_content'] = $this->datacontrol_model->getRowData('page_agent');
        $data['htmlTitle'] = $data['page_content']->meta_title;
        $data['htmlDes'] = $data['page_content']->meta_description;

        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['countries'] = $this->datacontrol_model->getAllData('countries');

        $data['page'] = 'agent_v';
        $this->load->view('front_template', $data);
    }

    public function agent_confirm()
    {
        $this->session->set_userdata('agent', $this->input->post());
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'agent_confirm_v';
        $this->load->view('front_template', $data);
    }

    public function agent_complete()
    {
        //$_POST = $this->session->userdata('agent');
        $send_list = array($this->input->post('email') => $shipping_info->name);
        $bcc = array($this->config->item('contact_email') => 'Sensha');

        $msg = "From: " . $this->input->post('name', TRUE);
        $msg .= "<br />Email: " . $this->input->post('email', TRUE);
        $msg .= "<br />Tel: " . $this->input->post('tel', TRUE);
        $msg .= "<br />Country: " . $this->input->post('country', TRUE);
        $msg .= "<br />Message: " . $this->input->post('msg', TRUE);
        //    mail_to(array("info@sensha-world.com" =>  "Sensha"), "Apply to Agent", $msg);
        mail_to($send_list, "Apply to Agent", $msg, $bcc);

        //$this->session->unset_userdata('agent');
        $data['page'] = 'agent_complete_v';
        $this->load->view('front_template', $data);
    }

    public function send_mail_ppf()
    {
        if (!is_dir('uploads/' . 'email')) {
            mkdir('./uploads/' . 'email', 0777, TRUE);
        }
        $config['upload_path'] = './uploads/email';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 7500;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_image')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);

        } else {
            $profile_image = array('profile_image' => $this->upload->data());
        }
        if (!$this->upload->do_upload('side_photo')) {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);

        } else {
            $side_photo = array('side_photo' => $this->upload->data());
        }

        //info@sensha-world.com
        $send_list = [$this->config->item('contact_email') => $this->config->item('contact_email')];

        $bcc = [];
        $files = ['', ''];
        $msg = "Content:<br>
                PPFカットフィルムの件でお問い合わせがありました。<br>
               下記の内容を確認した上で、ご返答ください。<br>";

        $msg .= "<br>EMAIL: " . $this->input->post('email', TRUE);
        $msg .= "<br>COMPANY NAME: " . $this->input->post('country', TRUE);
        $msg .= "<br>商品名: " . $this->input->post('product_name', TRUE);
        $msg .= "<br>URL: " . $this->input->post('full_url', TRUE);
        $msg .= "<br>グレード: " . $this->input->post('grade', TRUE);
        $msg .= "<br>初年度登録: " . $this->input->post('regis_year', TRUE);
        $msg .= "<br>型式: " . $this->input->post('regis_type', TRUE);

        $msg .= "<br>";
        $msg .= "<br>";
        $msg .= "<img src='" . base_url('uploads/email/' . $side_photo['side_photo']['file_name']) . "'>";
        $msg .= "<br>";
        $msg .= "<img src='" . base_url('uploads/email/' . $profile_image['profile_image']['file_name']) . "'>";

        $respone = mail_to($send_list, "PPFカットフィルムのお問い合わせ", $msg, $bcc, $files);

        //$this->session->unset_userdata('agent');
        $data['page'] = 'email_complete_v';
        $this->load->view('front_template', $data);
    }

    public function global_network()
    {
        $data['global_network'] = $this->datacontrol_model->getAllData('global_network');

        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['htmlTitle'] = "global_network";
        $data['page'] = 'global_network_v';
        $this->load->view('front_template', $data);
    }

    public function terms()
    {
        $data['htmlTitle'] = "Terms of use";
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'terms_v';
        $this->load->view('front_template', $data);
    }

    public function privacy()
    {
        $data['htmlTitle'] = "Privacy Policy";
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'privacy_v';
        $this->load->view('front_template', $data);
    }

    public function contact()
    {
        $data['htmlTitle'] = "Contact Us";
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'contact_v';
        $this->load->view('front_template', $data);
    }

    public function contact_confirm()
    {
        $this->session->set_userdata('contact', $this->input->post());
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $data['page'] = 'contact_confirm_v';
        $this->load->view('front_template', $data);
    }

    public function contact_complete()
    {
        $_POST = $this->session->userdata('contact');
        $msg =
            $this->lang->line('mail_payment_to_customer_dear', FALSE) . " " . $this->input->post('name') . " " . $this->lang->line('mail_payment_to_customer_sama', FALSE) . '<br /><br />';
        $msg .= $this->lang->line('page_mail_P01', FALSE) . $this->input->post('msg') . '<br>';
        $msg .= $this->lang->line('page_mail_P02', FALSE) . '<br>';
        $mail_title = $this->lang->line('page_mail_title', FALSE);
        mail_to(array($this->config->item('contact_email') => "Sensha", $this->input->post('email') => $this->input->post('name')), $mail_title, $msg);
        $this->session->unset_userdata('contact');
        $data['page'] = 'contact_complete_v';
        $this->load->view('front_template', $data);
    }

    public function general($cat, $cat2 = '')
    {
        // $this->output->enable_profiler(TRUE);
        $data['noindex'] = true;

        // update `cluster2` set cluster_url = cluster_name;
        // update `cluster2` set cluster_url = REPLACE(cluster_name, ' ', '-');
        // update `cluster2` set cluster_url = REPLACE(cluster_url, '/', '-');
        // update `cluster2` set cluster_url = REPLACE(cluster_url, '&', '-');
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $cat = urldecode($cat);
        $this->db->where('cluster_url', $cat);
        $this->db->where('country', $this->user_lang);
        $query = $this->db->get('cluster1');
        $cluster1 = $query->row();
        $data['cluster1'] = $cluster1;
        $data['htmlTitle'] = $cluster1->cluster_name;
        // echo $this->db->last_query();
        // print_r($cluster1);

        $data['user_country'] = $this->ion_auth->user()->row()->country;
        $data['nation_lang'] = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data['user_country']));

        if ($cat2 != '') {
            $cat2 = urldecode($cat2);
            $this->db->where('cluster_url', $cat2);
            $this->db->where('country', $this->user_lang);
            $query = $this->db->get('cluster2');
            $cluster2 = $query->row();
            // echo $this->db->last_query();
        }

        $this->db->where('country', $this->user_lang);
        $this->db->where('level1', $cluster1->id);
        $data['sub_cluster'] = $this->datacontrol_model->getAllData('cluster2');


        $data['product_type'] = 'general';

        $this->load->library('pagination');
        $config = array();
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 5;

        // $config["base_url"] = base_url() . "page/general/$cat";
        $config["base_url"] = base_url(uri_string());


        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }
        if (!empty($this->input->post('usage'))) {
            $where_usage = "(usage like '%" . implode("%' or usage like '%", $this->input->post('usage')) . "%')";
            $this->db->where($where_usage);
        }
        if (!empty($this->input->post('feature'))) {
            $where_feature = "(feature like '%" . implode("%' or feature like '%", $this->input->post('feature')) . "%')";
            $this->db->where($where_feature);
        }
        if (!empty($this->input->post('material'))) {
            $where_material = "(material like '%" . implode("%' or material like '%", $this->input->post('material')) . "%')";
            $this->db->where($where_material);
        }
        if (!empty($this->input->post('item_type'))) {
            $this->db->where('item_type', $this->input->post('item_type'));
        }
        //$this->db->where('is_active', 1);
        $this->db->where('cat_1', $cluster1->id);
        if (!empty($cluster2)) {
            $this->db->where('cat_2', $cluster2->id);
        }
        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {

            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }
        $data["products"] = $this->datacontrol_model->getAllData('product_general');

        if (empty($data["products"])) {
            // $this->output->set_status_header('404');
            // $this->load->view('errors/html/error_404');
            // exit();
            // show_404();
        }
        //echo $this->db->last_query();

        $config["total_rows"] = count($data["products"]);
        $config["per_page"] = 12;
        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        // $config['first_link'] = 'First';
        // $config['first_tag_open'] = '<li>';
        // $config['first_tag_close'] = '</li>';

        // $config['last_link'] = 'Last';
        // $config['last_tag_open'] = '<li>';
        // $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = ($this->input->get('page')) ? $this->input->get('page') - 1 : 0;
        $data["links"] = $this->pagination->create_links();


        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }
        if (!empty($this->input->post('usage'))) {
            $where_usage = "(usage like '%" . implode("%' or usage like '%", $this->input->post('usage')) . "%')";
            $this->db->where($where_usage);
        }
        if (!empty($this->input->post('feature'))) {
            $where_feature = "(feature like '%" . implode("%' or feature like '%", $this->input->post('feature')) . "%')";
            $this->db->where($where_feature);
        }

        if (!empty($this->input->post('material'))) {
            $where_material = "(material like '%" . implode("%' or material like '%", $this->input->post('material')) . "%')";
            $this->db->where($where_material);
        }
        if (!empty($this->input->post('item_type'))) {
            $this->db->where('item_type', $this->input->post('item_type'));
        }
        //$this->db->where('is_active', 1);
        $this->db->where('cat_1', $cluster1->id);
        if (!empty($cluster2)) {
            $this->db->where('cat_2', $cluster2->id);
        }
        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {
            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }
        $this->db->limit($config["per_page"], $page * $config["per_page"]);
        $data["products"] = $this->datacontrol_model->getAllData('product_general');

        // print_r($this->input->post());
        //echo $this->db->last_query();
        // exit();
        $data['page'] = 'product/products_general_v';
        $this->load->view('product/products_general_v', $data);

        if ($this->input->post('product_filter')) {
            // echo $msg = $this->load->view('product/products_filter_general_v', $data, false);
        } else {
            $data['page'] = 'product/products_general_v';
            //$this->load->view('product/products_general_v', $data);
        }


        // $this->load->view('front_template', $data);
    }

    public function ppf($cat = '', $cat2 = '', $parts = '')
    {

        $this->load->library('pagination');
        $config = array();
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 5;
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url(uri_string());


        $data['noindex'] = true;
        $data['coutry_iso'] = $this->coutry_iso . "/";


        if ($this->ion_auth->logged_in()) {
            $exclude_group_id = $this->ion_auth->user()->row()->exclude_group;
        }
        if ($this->input->get('cat_1') != '') {
            $this->db->where('id', $this->input->get('cat_1'));
        } else if ($cat != '') {
            $this->db->where('cluster_name', urldecode($cat));
        }
        $this->db->where('country', $this->user_lang);
        $query = $this->db->get('cluster1');
        $cluster1 = $query->row();
        $data['cluster1'] = $cluster1;
        $data['htmlTitle'] = $cluster1->cluster_name;

        $data['user_country'] = $this->ion_auth->user()->row()->country;
        $data['nation_lang'] = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data['user_country']));


        if ($this->input->get('cat_2') != '') {
            $this->db->where('id', $this->input->get('cat_2'));
            $this->db->where('country', $this->user_lang);
            $query = $this->db->get('cluster2');
            $cluster2 = $query->row();
            $data['cluster2'] = $cluster2;
        } else if ($cat2 != '') {
            $this->db->where('cluster_name', urldecode($cat2));
            $this->db->where('country', $this->user_lang);
            $query = $this->db->get('cluster2');
            $cluster2 = $query->row();
            $data['cluster2'] = $cluster2;
        }


        $this->db->where('country', $this->user_lang);
        $this->db->where('level1', $cluster1->id);
        $this->db->order_by('cluster_name', 'asc');
        $data['sub_cluster'] = $this->datacontrol_model->getAllData('cluster2');

        if ($exclude_group_id != '') {
            $this->db->where('id', $exclude_group_id);
            $query = $this->db->get('exclude_group');
            $result = $query->row();

            $this->db->where_not_in('product_id', explode(',', $result->exclude_group_id));
        }
        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }

        //$this->db->where('is_active', 1);
        if (!empty($cluster1)) {
            $this->db->where('cat_1', $cluster1->id);
        }

        if (!empty($cluster2) && empty($this->input->get('s'))) {
            $this->db->where('cat_2', $cluster2->id);
        }

        if (!empty($this->input->get('parts'))) {
            $this->db->where('parts', $this->input->get('parts'));
        }

        if (!empty($parts)) {
            $this->db->where('parts', $parts);
        }
        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {
            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }

        $page = ($this->input->get('page')) ? $this->input->get('page') - 1 : 0;

        $product_count = $this->datacontrol_model->getAllData('product_ppf');
        //echo $this->db->last_query();
        //paging
        $config["total_rows"] = count($product_count);
        $config["per_page"] = 12;
        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = ($this->input->get('page')) ? $this->input->get('page') - 1 : 0;
        $data["links"] = $this->pagination->create_links();

        ////////////////////////////
        if ($exclude_group_id != '') {
            $this->db->where('id', $exclude_group_id);
            $query = $this->db->get('exclude_group');
            $result = $query->row();

            $this->db->where_not_in('product_id', explode(',', $result->exclude_group_id));
        }

        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }

        if (!empty($cluster1)) {
            $this->db->where('cat_1', $cluster1->id);
        }

        if (!empty($cluster2) && empty($this->input->get('s'))) {
            $this->db->where('cat_2', $cluster2->id);
        }

        if (!empty($this->input->get('parts'))) {
            $this->db->where('parts', $this->input->get('parts'));
        }

        if (!empty($parts)) {
            $this->db->where('parts', $parts);
        }

        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {
            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }

        $this->db->limit($config["per_page"], $page * $config["per_page"]);
        $data["products"] = $this->datacontrol_model->getAllData('product_ppf');
        $data['product_type'] = 'ppf';
        /*echo "<br>";
        echo $this->db->last_query();*/

        if ($this->input->post('product_filter')) {
            echo $msg = $this->load->view('product/products_filter_ppf_v', $data, true);
        } else {
            $data['page'] = 'product/products_ppf_v';
            $this->load->view('product/products_ppf_v', $data);
        }

    }

    /*
    public function ppf_bk($cat = '', $cat2 = '')
    {
        // $this->output->enable_profiler(TRUE);
        $data['noindex'] = true;
        $data['coutry_iso'] = $this->coutry_iso . "/";
        if ($this->ion_auth->logged_in()) {
            $exclude_group_id = $this->ion_auth->user()->row()->exclude_group;
        }
        if ($this->input->get('cat_1') != '') {
            $this->db->where('id', $this->input->get('cat_1'));
        } else if ($cat != '') {
            $this->db->where('cluster_name', urldecode($cat));
        }
        $this->db->where('country', $this->user_lang);
        $query = $this->db->get('cluster1');
        $cluster1 = $query->row();
        // echo $this->db->last_query();
        // print_r($cluster1);
        $data['cluster1'] = $cluster1;
        $data['htmlTitle'] = $cluster1->cluster_name;

        $data['user_country'] = $this->ion_auth->user()->row()->country;
        $data['nation_lang'] = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data['user_country']));


        if ($this->input->get('cat_2') != '') {
            $this->db->where('id', $this->input->get('cat_2'));
            $this->db->where('country', $this->user_lang);
            $query = $this->db->get('cluster2');
            $cluster2 = $query->row();
            $data['cluster2'] = $cluster2;
        } else if ($cat2 != '') {
            $this->db->where('cluster_name', urldecode($cat2));
            $this->db->where('country', $this->user_lang);
            $query = $this->db->get('cluster2');
            $cluster2 = $query->row();
            $data['cluster2'] = $cluster2;
        }


        $this->db->where('country', $this->user_lang);
        $this->db->where('level1', $cluster1->id);
        $this->db->order_by('cluster_name', 'asc');
        $data['sub_cluster'] = $this->datacontrol_model->getAllData('cluster2');

        $this->load->library('pagination');
        $config = array();
        $config['page_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';
        $config['num_links'] = 5;
        $config['reuse_query_string'] = true;

        // $uri_segment = 4;
        // $config["base_url"] = base_url() . "page/ppf/$cat";
        $config["base_url"] = base_url(uri_string());


        if ($exclude_group_id != '') {
            $this->db->where('id', $exclude_group_id);
            $query = $this->db->get('exclude_group');
            $result = $query->row();

            $this->db->where_not_in('product_id', explode(',', $result->exclude_group_id));
        }
        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }

        //$this->db->where('is_active', 1);
        if (!empty($cluster1)) {
            $this->db->where('cat_1', $cluster1->id);
        }

        if (!empty($cluster2) && empty($this->input->get('s'))) {
            $this->db->where('cat_2', $cluster2->id);
        }

        if (!empty($this->input->get('parts'))) {
            $this->db->where('parts', $this->input->get('parts'));
        }
        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {
            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }

        $page = ($this->input->get('page')) ? $this->input->get('page') - 1 : 0;

        $data["products"] = $this->datacontrol_model->getAllData('product_ppf');
        //echo $this->db->last_query();
        // echo $cluster1->id;
        $data['product_type'] = 'ppf';

        $config["total_rows"] = count($data["products"]);

        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = ($this->input->get('page')) ? $this->input->get('page') - 1 : 0;
        $data["links"] = $this->pagination->create_links();


        if ($exclude_group_id != '') {
            $this->db->where('id', $exclude_group_id);
            $query = $this->db->get('exclude_group');
            $result = $query->row();

            $this->db->where_not_in('product_id', explode(',', $result->exclude_group_id));
        }

        if ($this->input->get('s')) {
            $this->db->like('product_name', $this->input->get('s'));
        }

        //$this->db->where('is_active', 1);
        if (!empty($cluster1)) {
            $this->db->where('cat_1', $cluster1->id);
        }

        if (!empty($cluster2) && empty($this->input->get('s'))) {
            $this->db->where('cat_2', $cluster2->id);
        }

        if (!empty($this->input->get('parts'))) {
            $this->db->where('parts', $this->input->get('parts'));
        }

        //zun
        if ($this->user_lang == 'Global') {
            $this->db->where('product_country', $this->user_lang);
        } else {
            $this->db->group_start();
            $this->db->where_in('product_country', [$this->user_lang, 'Global']);
            $this->db->group_end();
        }
        $this->db->limit($config["per_page"], $page * $config["per_page"]);
        $data["products"] = $this->datacontrol_model->getAllData('product_ppf');
        $data['product_type'] = 'ppf';


        if ($this->input->post('product_filter')) {
            echo $msg = $this->load->view('product/products_filter_ppf_v', $data, true);
        } else {
            $data['page'] = 'product/products_ppf_v';
            $this->load->view('product/products_ppf_v', $data);
        }
        // $this->load->view('front_template', $data);
    }
    */

    public function product_detail($product_type, $product_id)
    {
        //var_dump($this->session->unset_userdata('sensha_cart'));

        $data["product_id"] = $product_id;
        $cart = $this->session->userdata('sensha_cart');
        $data['cart'] = $cart;

        $data['coutry_iso'] = $this->coutry_iso . "/";
        if ($product_type == 'ppf') {
            $this->db->where('product_id', $product_id);
            //zun
            if ($this->user_lang == 'Global') {
                $this->db->where('product_country', $this->user_lang);
            } else {
                $this->db->group_start();
                $this->db->where_in('product_country', [$this->user_lang, 'Global']);
                $this->db->group_end();
                $this->db->order_by('id', 'DESC');
            }
            $data["products"] = $this->datacontrol_model->getRowData('product_ppf');

        } else if ($product_type == 'general') {
            $this->db->where('product_id', $product_id);
            $this->db->where('product_country', $this->user_lang);
            //zun
            if ($this->user_lang == 'Global') {
                $this->db->where('product_country', $this->user_lang);
            } else {
                $this->db->group_start();
                $this->db->where_in('product_country', [$this->user_lang, 'Global']);
                $this->db->group_end();
            }
            $data["products"] = $this->datacontrol_model->getRowData('product_general');
        }

        $this->db->where('id', $data["products"]->cat_1);
        $data["cat_1"] = $this->datacontrol_model->getRowData('cluster1');
        $data['user_country'] = $this->ion_auth->user()->row()->country;
        $data['nation_lang'] = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data['user_country']));


        $data['product_type'] = $product_type;
        // $this->db->where('product_id', $product_id);
        // $query = $this->db->get('product_general');
        // $data["product"] = $query->row();
        $e = '';
        if (!is_null(get_cookie('recent_viewed_items'))) {
            $c = get_cookie('recent_viewed_items');
            $e = explode(',', $c);
            //array_push($e, "$product_type|$product_id");
            array_push($e, "$product_id");
            set_cookie(array(
                'name' => 'recent_viewed_items',
                'value' => implode(',', $e),
                'expire' => 3600 * 24 * 12
            ));
        } else {
            set_cookie(array(
                'name' => 'recent_viewed_items',
                //'value'  => "$product_type|$product_id",
                'value' => "$product_id",
                'expire' => 3600 * 24 * 12
            ));
            // $e = explode(',', $product_id);
        }

        $this->db->where_in('product_id', $e);
        $product_country = $this->user_lang;
        if ($this->user_lang === 'Global') {
            $product_country = 'Global';
        }
        $this->db->where('product_country', $product_country);

        $this->db->limit(5);


        if ($product_type == 'ppf') {
            $data["recent_product"] = $this->datacontrol_model->getAllData('product_ppf');
        } else {
            $data["recent_product"] = $this->datacontrol_model->getAllData('product_general');
        }
        //echo $this->db->last_query();

        // delete_cookie('recent_viewed_items');

        // $c = get_cookie('recent_viewed_items');
        // print_r($c);

        $this->db->where('id', $data["products"]->cat_1);
        $data["product_cat"] = $this->datacontrol_model->getRowData('cluster1');

        $data['htmlTitle'] = $data['products']->product_name;

        $data['user_country'] = $this->ion_auth->user()->row()->country;
        $data['user_lang'] = $this->user_lang;
        $data['page'] = 'product/product_detail_v';
        $this->load->view('product/product_detail_v', $data);
        // $this->load->view('front_template', $data);
    }

    public function payment()
    {
        $this->session->unset_userdata(array('payWith', 'payFee'));

        $cart = $this->session->userdata('sensha_cart');
        $data = $cart;
        unset($data['coupon_discount']);
        unset($data['coupon_type']);
        unset($data['coupon_code']);
        unset($data['use_credit']);
        $this->session->set_userdata('sensha_cart', $data);

        $payment_mode = $this->session->userdata('sensha_mode');
        $data['payment_mode'] = $payment_mode;
        $data['noindex'] = true;
        $user_id = $this->ion_auth->user()->row()->id;
        $user_country = $this->ion_auth->user()->row()->country;
        $this->db->where('user_id', $user_id);
        $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');

        $data['cart'] = $this->session->userdata('sensha_cart');

        $this->db->where('country', $user_country)->or_where('credit_type', 'BUY');
        $this->db->order_by('id', 'desc');
        $data['sales_history_credit'] = $this->datacontrol_model->getRowData('sales_history_credit');

        $data['user_country'] = $user_country;

        if ($this->session->has_userdata('shipping_domestic')) {
            $shipping_domestic = $this->session->userdata('shipping_domestic');
            $shipping_domestic_name = $shipping_domestic['shipping_name'];
            $shipping_price_domestic = $shipping_domestic['shipping_price'];
        }
        if ($this->session->has_userdata('shipping_oversea')) {
            $shipping_oversea = $this->session->userdata('shipping_oversea');
            $shipping_oversea_name = $shipping_oversea['shipping_name'];
            $shipping_price_oversea = $shipping_oversea['shipping_price'];
        }


        $data['shipping_domestic'] = $shipping_domestic;
        $data['shipping_oversea'] = $shipping_oversea;

        $data['page'] = 'payment_v';
        $this->load->view('front_template', $data);
    }


    public function save_order()
    {
        $this->load->library('encryption');
        // $this->output->enable_profiler(TRUE);
        $data['noindex'] = true;
        $shipping_price_domestic = 0;
        $shipping_price_oversea = 0;

        $payWith = $this->session->userdata('payWith');
        $payFee = $this->session->userdata('payFee');

        $domestic_pay_on_delivery = $this->session->userdata('domestic_pay_on_delivery');

        $data_domestic_sale_total = '';
        $data_oversea_sale_total = '';

        $purchaser = $this->ion_auth->user()->row();

        $purchaser_type = $this->ion_auth->get_users_groups()->row()->name;
        $purchaser_country = $purchaser->country;
        $purchaser_discount = $purchaser->discount_setting;
        // $purchaser_country = 'Thailand';
        $purchaser_id = $purchaser->id;


        $purchaser_email = $purchaser->email;

        if ($this->session->has_userdata('shipping_domestic')) {
            $shipping_domestic = $this->session->userdata('shipping_domestic');
            $shipping_domestic_name = $shipping_domestic['shipping_name'];
            $shipping_price_domestic = $shipping_domestic['shipping_price'];
            if ($this->session->has_userdata('domestic_pay_on_delivery')) {
                //$shipping_price_domestic += $this->session->userdata('domestic_pay_on_delivery');
            }
        }
        if ($this->session->has_userdata('shipping_oversea')) {
            $shipping_oversea = $this->session->userdata('shipping_oversea');
            $shipping_oversea_name = $shipping_oversea['shipping_name'];
            $shipping_price_oversea = $shipping_oversea['shipping_price'];
            if ($this->session->has_userdata('oversea_pay_on_delivery')) {
                $shipping_price_oversea += $this->session->userdata('oversea_pay_on_delivery');
            }
        }

        $cart = $this->session->userdata('sensha_cart');
        // echo "<pre>";
        // print_r($cart);
        // print_r($shipping_domestic);
        // print_r($shipping_oversea);
        $domestic_item = 0;
        $domestic_price = 0;
        $domestic_total = 0;
        $domestic_import_shipping = 0;
        $domestic_import_tax = 0;
        if (isset($cart['domestic'])) {
            foreach ($cart['domestic'] as $key => $value) {
                $domestic_item += $cart['domestic'][$key]['item_amount'];
                $domestic_price += ($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']);
                $domestic_import_shipping += ($cart['domestic'][$key]['import_shipping'] > 0) ? $cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount'] : 0;
                $domestic_import_tax += ($cart['domestic'][$key]['import_tax'] > 0) ? $cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount'] : 0;
            }
            $domestic_total = $domestic_price + $domestic_import_shipping + $domestic_import_tax + $shipping_price_domestic;
        }

        $oversea_item = 0;
        $oversea_price = 0;
        $oversea_total = 0;
        if (isset($cart['oversea'])) {
            foreach ($cart['oversea'] as $key => $value) {
                $oversea_item += $cart['oversea'][$key]['item_amount'];
                $oversea_price += ($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']);
            }
            $oversea_total = $oversea_price + $shipping_price_oversea;
        }

        $shipping_info = $this->datacontrol_model->getRowData('shipping_info', array('user_id' => $purchaser_id));
        //$purchaser_name = $shipping_info->name;
        //$purchaser_company = $shipping_info->company;

        if (isset($cart['use_credit'])) {
            $use_credit = $cart['use_credit'];
        } else {
            $use_credit = 0;
        }

        $domestic_total = 0;
        $domestic_item_sub_total_amount = 0;
        $domestic_user_discount_total_amount = 0;
        $domestic_item_amount = 0;
        $domestic_import_shipping = 0;
        $domestic_import_tax = 0;
        $domestic_coupon_percent_total_amount = 0;

        $coupon_percent_discount = 0;
        $coupon_percent_amount = 0;
        $coupon_fix_amount = 0;
        $user_discount_amount = 0;

        if (isset($cart['domestic']) && !empty($cart['domestic'])) {
            // $shipment_id = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sales_history_total' order by AUTO_INCREMENT desc limit 1)), 6), 'D') as shipment_id")->row()->shipment_id;

            $shipment_id = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT COUNT(id)+1 FROM sales_history_total)), 6), 'D') as shipment_id")->row()->shipment_id;

            foreach ($cart['domestic'] as $key => $value) {
                if ($cart['domestic'][$key]['import_shipping'] == '') {
                    $cart['domestic'][$key]['import_shipping'] = 0;
                }
                if ($cart['domestic'][$key]['import_tax'] == '') {
                    $cart['domestic'][$key]['import_tax'] = 0;
                }
                $user_discount_amount = ($purchaser_discount / 100) * (($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']) + ($cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount']) + ($cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount']));
                if (@$cart['coupon_discount']) {
                    if (@$cart['coupon_type'] == 'fixed') {
                        $coupon_fix_amount = ($cart['coupon_discount']);
                    } else {
                        $coupon_percent_discount = $cart['coupon_discount'];
                        $coupon_percent_amount = (($cart['coupon_discount'] / 100) * ($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']));
                    }
                }

                $domestic_item_sub_total_amount += (($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']));
                $domestic_user_discount_total_amount += $user_discount_amount;
                $domestic_coupon_percent_total_amount += $coupon_percent_amount;
                $domestic_item_amount += $cart['domestic'][$key]['item_amount'];
                $domestic_import_shipping += ($cart['domestic'][$key]['import_shipping'] > 0) ? $cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount'] : 0;
                $domestic_import_tax += ($cart['domestic'][$key]['import_tax'] > 0) ? $cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount'] : 0;

                // sale_history domestic
                $data = array(
                    'shipment_id' => $shipment_id,
                    'country' => @$cart['domestic'][$key]['product_country'],
                    'account_type' => @$cart['domestic'][$key]['sale_account_type'],
                    'account_name' => @$cart['domestic'][$key]['sale_account_name'],
                    'sale_date' => date("Y-m-d"),
                    'sale_item_category' => @$cart['domestic'][$key]['product_category'],
                    'buy_type' => 'domestic',
                    //'sale_item_id' => $key,
                    'sale_item_id' => $cart['domestic'][$key]['product_id'],
                    'product_name' => $cart['domestic'][$key]['product_name'],
                    'item_amount' => $cart['domestic'][$key]['item_amount'],
                    'import_shipping' => ($cart['domestic'][$key]['import_shipping'] > 0) ? $cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount'] : 0,
                    'import_tax' => ($cart['domestic'][$key]['import_tax'] > 0) ? $cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount'] : 0,
                    'product_type' => $cart['domestic'][$key]['product_type'],
                    'item_sub_total_amount' => $cart['domestic'][$key]['price'],
                    'delivery_amount' => $shipping_price_domestic,
                    // discount
                    'discount_ratio' => $purchaser_discount,
                    'user_discount_amount' => round($user_discount_amount),
                    'discount_type' => '',
                    'coupon_discount_type' => (isset($cart['coupon_type'])) ? 'coupon - ' . $cart['coupon_type'] : '',
                    'coupon_code' => @$cart['coupon_code'],
                    'coupon_fix_amount' => $coupon_fix_amount,
                    'coupon_percent_discount' => $coupon_percent_discount,
                    'coupon_percent_amount' => $coupon_percent_amount,
                    'use_credit' => $use_credit,
                    'discount_amount' => round($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit),
                    //'sale_total_amount' => round(($cart['domestic'][$key]['price'] + $shipping_price_domestic) - ($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit)),
                    'sale_total_amount' => round($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']),
                    'purchaser_type' => $purchaser_type,
                    'purchaser_country' => $purchaser_country,
                    'purchaser_id' => $purchaser_id,
                    'purchaser_name' => $shipping_info->name,
                    'purchaser_company' => $shipping_info->company,
                    'shipping_addr' => $shipping_info->address,
                    'payment_status' => '-',
                    'payment_method' => $payWith,
                    'payment_data' => "",
                    'payment_date' => date("Y-m-d"),
                    'shipping_method' => $shipping_domestic_name,
                    'shipping_status' => 'Processing',
                    'shipping_date' => '',
                    'emergency_status' => '',
                    'emergency_date' => '',
                    'total_discount' => 0,
                );

                $domestic_total += $data['sale_total_amount'];
                $this->datacontrol_model->insert('sales_history', $data);
            } // end foreach domestic
            // exit();

            /// sales_history_total domestic
            $data_sale_total = $data;
            $data_sale_total['item_amount'] = $domestic_item;
            $data_sale_total['import_shipping'] = $domestic_import_shipping;
            $data_sale_total['import_tax'] = $domestic_import_tax;
            $data_sale_total['item_sub_total_amount'] = $domestic_item_sub_total_amount + $domestic_import_shipping + $domestic_import_tax;
            $data_sale_total['user_discount_amount'] = round($domestic_user_discount_total_amount);
            $data_sale_total['coupon_percent_amount'] = round($domestic_coupon_percent_total_amount);
            $data_sale_total['discount_amount'] = (round($domestic_user_discount_total_amount) + $coupon_fix_amount + round($domestic_coupon_percent_total_amount) + $use_credit);
            $data_sale_total['sale_total_amount'] = round(($domestic_item_sub_total_amount + $shipping_price_domestic + $domestic_import_shipping + $domestic_import_tax) - round($domestic_user_discount_total_amount));
            if (($coupon_fix_amount + $domestic_coupon_percent_total_amount) >= $data_sale_total['sale_total_amount']) {
                $data_sale_total['discount_amount'] = $data_sale_total['sale_total_amount'];
                $data_sale_total['sale_total_amount'] = 0;
            } else {
                $data_sale_total['sale_total_amount'] = round($data_sale_total['sale_total_amount'] - round($coupon_fix_amount + $domestic_coupon_percent_total_amount));
            }
            if ($use_credit >= $data_sale_total['sale_total_amount']) {
                $use_credit = $data_sale_total['sale_total_amount'];
                $data_sale_total['sale_total_amount'] = 0;
            } else {
                $data_sale_total['sale_total_amount'] = round($data_sale_total['sale_total_amount'] - $use_credit);
            }

            //// commission domestic
            $data_sale_total['sa_name'] = '-';
            $data_sale_total['sa_sale_percent'] = 0;
            $data_sale_total['sa_sale_amount'] = 0;
            $data_sale_total['sa_pay_global'] = 0;
            $data_sale_total['sa_pay_domestic'] = 0;
            $data_sale_total['aa_name'] = '-';
            $data_sale_total['aa_percent'] = 0;
            $data_sale_total['aa_pay'] = 0;

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'SA') {
                $sa = $this->datacontrol_model->getRowData('users', array('email' => $data_sale_total['account_name']));
                $data_sale_total['sa_name'] = $sa->email;
            }

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_global'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']);
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_global'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']);
                }
            }

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'GU') {
            }

            if ($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = 100;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']) + $data_sale_total['delivery_amount'];
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = 100;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']) + $data_sale_total['delivery_amount'];
                }
            }

            if (($data_sale_total['country'] == 'JapanA' || $data_sale_total['country'] == 'JapanB') && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = 50;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']);
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = 50;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']);
                }
            }

            if ($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'GU') {
                $sa = $this->datacontrol_model->getRowData('users', array('email' => $data_sale_total['account_name']));
                $data_sale_total['sa_name'] = $sa->email;
                $data_sale_total['sa_sale_percent'] = 100;
                $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount']) - round($data_sale_total['discount_amount']) + $data_sale_total['delivery_amount'];
            }


            $vat = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data_sale_total['purchaser_country']));
            $data_sale_total['vat_percent'] = 0;
            $data_sale_total['vat_percent'] = $vat->vat;
            $data_sale_total['vat_amount'] = round(($vat->vat / 100) * $data_sale_total['sale_total_amount']);
            $data_sale_total['admin_fee_percent'] = round($payFee);
            $data_sale_total['admin_fee_amount'] = round(($payFee > 10) ? $payFee : ($payFee / 100) * $data_sale_total['sale_total_amount']);
            if ($payFee > 10) {
                $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount'] + $data_sale_total['vat_amount'] + $data_sale_total['admin_fee_amount']);
            } else {
                $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount'] + $data_sale_total['vat_amount'] + $data_sale_total['admin_fee_amount']);
            }

            $data_sale_total['invoice'] = $this->input->post('invoice');
            $json_data = json_encode(array(
                'data_domestic_sale_total' => $data_domestic_sale_total,
                'cart' => $cart
            ));
            $data_sale_total['json_data'] = $json_data;
            $data_domestic_sale_total = $data_sale_total;
            $this->datacontrol_model->insert('sales_history_total', $data_sale_total);

            if ($data_sale_total['aa_name'] != '-') {
                $this->datacontrol_model->update('sales_history', array('aa_name' => $data_sale_total['aa_name']), array('shipment_id' => $shipment_id));
            }


            // sales_history_special domestic
            foreach ($cart['domestic'] as $key => $value) {
                $ac = round(($vat->vat / 100) * $data_sale_total['item_sub_total_amount']);
                $aw = round(($vat->vat / 100) * $shipping_price_domestic);
                $ax = round(($vat->vat / 100) * ($data_sale_total['item_sub_total_amount'] + $shipping_price_domestic - $data_sale_total['discount_amount']));
                $ay = round(($ac + $aw) - $ax);
                $az = $data_sale_total['discount_amount'];
                $data_special = array(
                    "shipment_id" => $shipment_id, //a
                    "product_id" => $cart['domestic'][$key]['product_id'],
                    "blank_a" => "",
                    "product_name" => $cart['domestic'][$key]['product_name'],
                    "item_sub_total_amount" => $cart['domestic'][$key]['price'],
                    "item_amount" => $cart['domestic'][$key]['item_amount'],
                    "sale_date" => date("Y-m-d"),
                    "account_name" => $purchaser_email, //h
                    "email" => $purchaser_email, //i
                    "blank_b" => "",
                    "blank_c" => "",
                    "blank_d" => "",
                    "name" => $shipping_info->company,
                    "shipping_addr" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country",
                    "phone" => $shipping_info->phone,
                    "payment_method" => $payWith,
                    'payment_status' => '-',
                    "blank_e" => "",
                    "blank_f" => "",
                    "blank_g" => "",
                    "blank_h" => "",
                    "blank_i" => "",
                    "blank_j" => "",
                    "blank_k" => "",
                    "blank_l" => "",
                    "blank_m" => "",
                    "item_sub_total_amount2" => $cart['domestic'][$key]['price'], //z
                    "item_amount2" => $cart['domestic'][$key]['item_amount'], // aa
                    "sub_total" => $cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount'], //ab
                    "special_vat" => $ac, //ac
                    "admin_fee_amount" => $data_sale_total['admin_fee_amount'], //ad
                    "delivery_fee" => round((($vat->vat / 100) * $shipping_price_domestic) + $shipping_price_domestic), //ae
                    "payment_total_amount" => $data_sale_total['payment_total_amount'], //af
                    "shipping_full_addr" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country", //ag
                    "company_name" => $shipping_info->company, //ah
                    "shipping_full_addr2" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country", //ai
                    "phone2" => $shipping_info->phone, //aj
                    "blank_n" => "",
                    "blank_o" => "",
                    "email2" => $purchaser_email, //am
                    "blank_p" => "",
                    "blank_q" => "",
                    "item_sub_total_amount3" => $data_sale_total['item_sub_total_amount'], //ap
                    "blank_r" => "",
                    "blank_s" => "",
                    "blank_t" => "",
                    "blank_u" => "",
                    "blank_v" => "",
                    "delivery_amount" => $shipping_price_domestic, //av
                    "delivery_fee_vat" => $aw, //aw
                    "vat_amount" => $ax, //ax
                    "vat_special" => $ay, //ay
                    "discount_amount" => $az, //az
                    "discount_special" => $ay + $az, //ba
                    "blank_w" => "",
                    "country" => @$cart['domestic'][$key]['product_country'],
                    "product_id2" => $cart['domestic'][$key]['product_id'],
                    "item_sub_total_amount4" => $data_sale_total['item_sub_total_amount'],
                    "blank_x" => "",
                    "blank_y" => "",
                    "blank_z" => "",
                    "blank_aa" => "",
                    "blank_ab" => "",
                    "blank_ac" => "",
                    "blank_ad" => "",
                    "blank_ae" => "",
                    "blank_af" => "",
                    "blank_ag" => "",
                    "blank_ah" => "",
                    "blank_ai" => "",
                    "payment_total_amount2" => $data_sale_total['payment_total_amount'], //br
                    "item_sub_total_amount5" => $cart['domestic'][$key]['price'],
                    "blank_aj" => "",
                    "blank_ak" => "",
                    "blank_al" => "",
                    "blank_am" => "",
                    "blank_an" => "",
                    "discount_special2" => $ay + $az, //by
                    "blank_ao" => "",
                    "blank_ap" => "",
                );

                $this->datacontrol_model->insert('sales_history_special', $data_special);
            }

            if (in_array($_GET['use'], array('use all point', 'use point', 'Bank Transfer', 'pay on delivery'))) {
                ///// Credit domestic
                if ($data['account_type'] == 'admin' && $purchaser_type == 'SA') {
                    $credit_type = 'BUY';
                } else {
                    $credit_type = 'SELL';
                }
                $sa_discount = 100;
                if ($data['country'] == 'Global' && $purchaser_type == 'FC') {
                    $sa = $this->ion_auth->user($purchaser_id)->row();
                    $sa_discount = $sa->discount_setting;
                }
                $percent = array($coupon_percent_discount, $purchaser_discount, $sa_discount);
                arsort($percent);
                $total_percent = 0;
                foreach ($percent as $val) {
                    $total_percent = $val - $total_percent;
                }

                $this->db->order_by('id', 'desc');
                $this->db->where('country', $data['purchaser_country'])->or_where('credit_type', 'BUY');
                $balance = $this->datacontrol_model->getRowData('sales_history_credit');
                $credit_added = 0;
                if ($data_sale_total['sa_pay_global'] != 0) {
                    $credit_added = $data_sale_total['sa_pay_global'];
                }
                if ($data_sale_total['sa_pay_domestic'] != 0) {
                    $credit_added = $data_sale_total['sa_pay_domestic'];
                }
                $data_credit = array(
                    'country' => $data['country'],
                    'credit_type' => $credit_type,
                    'shipment_id' => $shipment_id,
                    'item_amount' => $data['item_amount'],
                    'sale_amount' => $domestic_item_sub_total_amount,
                    'shiping_fee' => $shipping_price_domestic,
                    'discount_total' => $data['discount_amount'],
                    'seller' => $data['country'],
                    'purchaser' => $purchaser_type,
                    'purchaser_country' => $purchaser_country,
                    'percent' => $total_percent,
                    'coupon_percent' => $coupon_percent_discount,
                    'fc_percent' => $purchaser_discount,
                    'sa_percent' => $sa_discount,
                    'coupon_fix' => $coupon_fix_amount,
                    'credit_added' => $credit_added,
                    'use_credit' => $use_credit,
                    'release_amount' => 0,
                    'transaction' => $credit_added,
                    'balance' => round($balance->balance + $credit_added),
                );
                if ($credit_type == 'BUY') {
                    $data_credit['transaction'] = -($use_credit);
                    $data_credit['balance'] = round($balance->balance - $use_credit);
                }
                /*comment insert sales_history_credit by zun*/
                //$this->datacontrol_model->insert('sales_history_credit', $data_credit);
            }


            // $json_data = json_encode(array(
            //   'data_domestic_sale_total' => $data_domestic_sale_total,
            //   'cart' => $cart
            // ));
            // $this->datacontrol_model->update('sales_history_total', array('json_data' => $json_data), array('shipment_id' => $shipment_id));
        }


        $oversea_total = 0;
        $oversea_item_sub_total_amount = 0;
        $oversea_user_discount_total_amount = 0;
        $oversea_coupon_percent_total_amount = 0;
        $oversea_item_amount = 0;
        $coupon_percent_discount = 0;
        $coupon_percent_amount = 0;
        $coupon_fix_amount = 0;
        if (isset($cart['oversea']) && !empty($cart['oversea'])) {
            // $shipment_id = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sales_history_total' order by AUTO_INCREMENT desc limit 1)), 6), 'O') as shipment_id")->row()->shipment_id;

            $shipment_id = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT COUNT(id)+1 FROM sales_history_total)), 6), 'O') as shipment_id")->row()->shipment_id;

            foreach ($cart['oversea'] as $key => $value) {
                $user_discount_amount = ($purchaser_discount / 100) * ($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']);

                if ($cart['coupon_discount']) {
                    if ($cart['coupon_type'] == 'fixed') {
                        $coupon_fix_amount = ($cart['coupon_discount']);
                    } else {
                        $coupon_percent_discount = $cart['coupon_discount'];
                        $coupon_percent_amount = (($cart['coupon_discount'] / 100) * ($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']));
                    }
                }

                $oversea_item_sub_total_amount += (($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']) + $cart['oversea'][$key]['import_shipping'] + $cart['oversea'][$key]['import_tax']);
                $oversea_user_discount_total_amount += $user_discount_amount;
                $oversea_coupon_percent_total_amount += $coupon_percent_amount;
                $oversea_item_amount += $cart['oversea'][$key]['item_amount'];

                // sale_history oversea
                $data = array(
                    'shipment_id' => $shipment_id,
                    'country' => 'Global',
                    'account_type' => 'admin',
                    'account_name' => @$cart['oversea'][$key]['sale_account_name'],
                    'sale_date' => date("Y-m-d"),
                    'sale_item_category' => @$cart['oversea'][$key]['product_category'],
                    'buy_type' => 'oversea',
                    //'sale_item_id' => $key,
                    'sale_item_id' => $cart['oversea'][$key]['product_id'],
                    'product_name' => $cart['oversea'][$key]['product_name'],
                    'item_amount' => $cart['oversea'][$key]['item_amount'],
                    // 'import_shipping' => $domestic_import_shipping,
                    // 'import_tax' => $domestic_import_tax,
                    'product_type' => $cart['oversea'][$key]['product_type'],
                    'item_sub_total_amount' => $cart['oversea'][$key]['price'],
                    'delivery_amount' => $shipping_price_oversea,
                    // discount
                    'discount_ratio' => $purchaser_discount,
                    'user_discount_amount' => round($user_discount_amount),
                    'discount_type' => '',
                    'coupon_discount_type' => (isset($cart['coupon_type'])) ? 'coupon - ' . $cart['coupon_type'] : '',
                    'coupon_code' => @$cart['coupon_code'],
                    'coupon_fix_amount' => $coupon_fix_amount,
                    'coupon_percent_discount' => $coupon_percent_discount,
                    'coupon_percent_amount' => $coupon_percent_amount,
                    'use_credit' => $use_credit,
                    'discount_amount' => round($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit),
                    //'sale_total_amount' => round(($cart['oversea'][$key]['price'] + $shipping_price_oversea) - ($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit)),
                    'sale_total_amount' => round($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']),
                    'purchaser_type' => $purchaser_type,
                    'purchaser_country' => $purchaser_country,
                    'purchaser_id' => $purchaser_id,
                    'purchaser_name' => $shipping_info->name,
                    'purchaser_company' => $shipping_info->company,
                    'shipping_addr' => $shipping_info->address,
                    'payment_status' => '-',
                    'payment_method' => $payWith,
                    'payment_data' => "",
                    'payment_date' => date("Y-m-d"),
                    'shipping_method' => $shipping_oversea_name,
                    'shipping_status' => 'Processing',
                    'shipping_date' => '',
                    'emergency_status' => '',
                    'emergency_date' => '',
                    'total_discount' => 0,
                );

                $oversea_total += $data['sale_total_amount'];
                $this->datacontrol_model->insert('sales_history', $data);
            }

            /// sales_history_total oversea
            $data_sale_total = $data;
            $data_sale_total['item_amount'] = $oversea_item;
            $data_sale_total['item_sub_total_amount'] = $oversea_item_sub_total_amount;
            $data_sale_total['user_discount_amount'] = round($oversea_user_discount_total_amount);
            $data_sale_total['coupon_percent_amount'] = round($oversea_coupon_percent_total_amount);
            $data_sale_total['discount_amount'] = (round($oversea_user_discount_total_amount) + $coupon_fix_amount + round($oversea_coupon_percent_total_amount) + $use_credit);
            $data_sale_total['sale_total_amount'] = round(($oversea_item_sub_total_amount + $shipping_price_oversea) - round($oversea_user_discount_total_amount));
            if (($coupon_fix_amount + $oversea_coupon_percent_total_amount) >= $data_sale_total['sale_total_amount']) {
                $data_sale_total['discount_amount'] = $data_sale_total['sale_total_amount'];
                $data_sale_total['sale_total_amount'] = 0;
            } else {
                $data_sale_total['sale_total_amount'] = round($data_sale_total['sale_total_amount'] - round($coupon_fix_amount + $oversea_coupon_percent_total_amount));
            }
            if ($use_credit >= $data_sale_total['sale_total_amount']) {
                $use_credit = $data_sale_total['sale_total_amount'];
                $data_sale_total['sale_total_amount'] = 0;
            } else {
                $data_sale_total['sale_total_amount'] = round($data_sale_total['sale_total_amount'] - $use_credit);

            }

            //// commission oversea
            $data_sale_total['sa_name'] = '-';
            $data_sale_total['sa_sale_percent'] = 0;
            $data_sale_total['sa_sale_amount'] = 0;
            $data_sale_total['sa_pay_global'] = 0;
            $data_sale_total['sa_pay_domestic'] = 0;
            $data_sale_total['aa_name'] = '-';
            $data_sale_total['aa_percent'] = 0;
            $data_sale_total['aa_pay'] = 0;

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'SA') {
                $sa = $this->datacontrol_model->getRowData('users', array('email' => $data_sale_total['account_name']));
                $data_sale_total['sa_name'] = $sa->email;
            }

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_global'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount']);
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_global'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount']);
                }
            }

            if ($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'GU') {
            }

            if ($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = 100;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount']);
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = 100;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount']);
                }
            }

            if (($data_sale_total['country'] == 'JapanA' || $data_sale_total['country'] == 'JapanB') && $data_sale_total['purchaser_type'] == 'FC') {
                $fc = $this->ion_auth->user($data_sale_total['purchaser_id'])->row();
                $sa = $this->ion_auth->user($fc->create_by)->row();
                $data_sale_total['sa_name'] = $sa->email;
                if ($fc->parent_agent > 0) {
                    $aa = $this->ion_auth->user($fc->parent_agent)->row();
                    $sa = $this->ion_auth->user($aa->create_by)->row();

                    $data_sale_total['sa_sale_percent'] = 50;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount']);
                    $data_sale_total['aa_name'] = $aa->email;
                    $data_sale_total['aa_percent'] = $aa->comission_setting;
                    $data_sale_total['aa_pay'] = round((($data_sale_total['aa_percent'] - $fc->discount_setting) / 100) * $data_sale_total['item_sub_total_amount']);
                } else {
                    $data_sale_total['sa_sale_percent'] = 50;
                    $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                    $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount']);
                }
            }

            if ($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'GU') {
                $sa = $this->datacontrol_model->getRowData('users', array('email' => $data_sale_total['account_name']));
                $data_sale_total['sa_name'] = $sa->email;
                $data_sale_total['sa_sale_percent'] = 100;
                $data_sale_total['sa_sale_amount'] = round(($data_sale_total['sa_sale_percent'] / 100) * $data_sale_total['item_sub_total_amount']);
                $data_sale_total['sa_pay_domestic'] = round($data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount']);
            }


            $vat = $this->datacontrol_model->getRowData('nation_lang', array('country' => $data_sale_total['purchaser_country']));
            // $data_sale_total['vat_percent'] = $vat->vat;
            // $data_sale_total['vat_amount'] = ($vat->vat/100)*$data_sale_total['sale_total_amount'];
            $data_sale_total['vat_percent'] = 0;
            $data_sale_total['vat_amount'] = 0;
            $data_sale_total['admin_fee_percent'] = round($payFee);
            $data_sale_total['admin_fee_amount'] = round(($payFee > 10) ? $payFee : ($payFee / 100) * $data_sale_total['sale_total_amount']);
            if ($payFee > 10) {
                $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount'] + $data_sale_total['vat_amount'] + $data_sale_total['admin_fee_amount']);
            } else {
                $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount'] + $data_sale_total['vat_amount'] + $data_sale_total['admin_fee_amount']);
            }

            $data_sale_total['invoice'] = $this->input->post('invoice');
            $json_data = json_encode(array(
                'data_domestic_sale_total' => $data_oversea_sale_total,
                'cart' => $cart
            ));
            $data_sale_total['json_data'] = $json_data;
            $data_oversea_sale_total = $data_sale_total;
            $this->datacontrol_model->insert('sales_history_total', $data_sale_total);

            if ($data_sale_total['aa_name'] != '-') {
                $this->datacontrol_model->update('sales_history', array('aa_name' => $data_sale_total['aa_name']), array('shipment_id' => $shipment_id));
            }

            // sales_history_special oversea
            foreach ($cart['oversea'] as $key => $value) {
                $ac = round($data_sale_total['vat_amount'] * $data_sale_total['item_sub_total_amount']);
                $aw = round($data_sale_total['vat_amount'] * $shipping_price_oversea);
                $ax = round($data_sale_total['vat_amount'] * ($data_sale_total['item_sub_total_amount'] + $shipping_price_oversea - $data_sale_total['discount_amount']));
                $ay = round(($ac + $aw) - $ax);
                $az = $data_sale_total['discount_amount'];
                $data_special = array(
                    "shipment_id" => $shipment_id, //a
                    "product_id" => $cart['oversea'][$key]['product_id'],
                    "blank_a" => "",
                    "product_name" => $cart['oversea'][$key]['product_name'],
                    "item_sub_total_amount" => $cart['oversea'][$key]['price'],
                    "item_amount" => $cart['oversea'][$key]['item_amount'],
                    "sale_date" => date("Y-m-d"),
                    "account_name" => $purchaser_email, //h
                    "email" => $purchaser_email, //i
                    "blank_b" => "",
                    "blank_c" => "",
                    "blank_d" => "",
                    "name" => $shipping_info->name,
                    "shipping_addr" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country",
                    "phone" => $shipping_info->phone,
                    "payment_method" => $payWith,
                    'payment_status' => '-',
                    "blank_e" => "",
                    "blank_f" => "",
                    "blank_g" => "",
                    "blank_h" => "",
                    "blank_i" => "",
                    "blank_j" => "",
                    "blank_k" => "",
                    "blank_l" => "",
                    "blank_m" => "",
                    "item_sub_total_amount2" => $cart['oversea'][$key]['price'], //z
                    "item_amount2" => $cart['oversea'][$key]['item_amount'], // aa
                    "sub_total" => $cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount'], //ab
                    "special_vat" => $ac, //ac
                    "admin_fee_amount" => $data_sale_total['admin_fee_amount'], //ad
                    "delivery_fee" => round(($data_sale_total['vat_amount'] * $shipping_price_oversea) + $shipping_price_oversea), //ae
                    "payment_total_amount" => $data_sale_total['payment_total_amount'], //af
                    "shipping_full_addr" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country", //ag
                    "company_name" => $shipping_info->company, //ah
                    "shipping_full_addr2" => "$shipping_info->zipcode $shipping_info->address $shipping_info->country", //ai
                    "phone2" => $shipping_info->phone, //aj
                    "blank_n" => "",
                    "blank_o" => "",
                    "email2" => $purchaser_email, //am
                    "blank_p" => "",
                    "blank_q" => "",
                    "item_sub_total_amount3" => $data_sale_total['item_sub_total_amount'], //ap
                    "blank_r" => "",
                    "blank_s" => "",
                    "blank_t" => "",
                    "blank_u" => "",
                    "blank_v" => "",
                    "delivery_amount" => $shipping_price_oversea, //av
                    "delivery_fee_vat" => $aw, //aw
                    "vat_amount" => $ax, //ax
                    "vat_special" => $ay, //ay
                    "discount_amount" => $az, //az
                    "discount_special" => $ay + $az, //ba
                    "blank_w" => "",
                    "country" => @$cart['oversea'][$key]['product_country'],
                    "product_id2" => $cart['oversea'][$key]['product_id'],
                    "item_sub_total_amount4" => $data_sale_total['item_sub_total_amount'],
                    "blank_x" => "",
                    "blank_y" => "",
                    "blank_z" => "",
                    "blank_aa" => "",
                    "blank_ab" => "",
                    "blank_ac" => "",
                    "blank_ad" => "",
                    "blank_ae" => "",
                    "blank_af" => "",
                    "blank_ag" => "",
                    "blank_ah" => "",
                    "blank_ai" => "",
                    "payment_total_amount2" => $data_sale_total['payment_total_amount'], //br
                    "item_sub_total_amount5" => $cart['oversea'][$key]['price'],
                    "blank_aj" => "",
                    "blank_ak" => "",
                    "blank_al" => "",
                    "blank_am" => "",
                    "blank_an" => "",
                    "discount_special2" => $ay + $az, //by
                    "blank_ao" => "",
                    "blank_ap" => "",
                );

                $this->datacontrol_model->insert('sales_history_special', $data_special);
            }

            if (in_array($_GET['use'], array('use all point', 'use point', 'Bank Transfer', 'pay on delivery'))) {
                ///// Credit oversea
                if ($data['account_type'] == 'admin' && $purchaser_type == 'SA') {
                    $credit_type = 'BUY';
                } else {
                    $credit_type = 'SELL';
                }
                $sa_discount = 0;
                if ($data['country'] == 'Global' && $purchaser_type == 'FC') {
                    $sa = $this->ion_auth->user($purchaser_id)->row();
                    $sa_discount = $sa->discount_setting;
                }
                $percent = array($coupon_percent_discount, $purchaser_discount, $sa_discount);
                arsort($percent);
                $total_percent = 0;
                foreach ($percent as $val) {
                    $total_percent = $val - $total_percent;
                }

                $this->db->order_by('id', 'desc');
                $this->db->where('country', $data['purchaser_country'])->or_where('credit_type', 'BUY');
                $balance = $this->datacontrol_model->getRowData('sales_history_credit');
                // echo $balance->balance;
                $credit_added = 0;
                if ($data_sale_total['sa_pay_global'] != 0) {
                    $credit_added = $data_sale_total['sa_pay_global'];
                }
                if ($data_sale_total['sa_pay_domestic'] != 0) {
                    $credit_added = $data_sale_total['sa_pay_domestic'];
                }
                $data_credit = array(
                    'country' => $data['country'],
                    'credit_type' => $credit_type,
                    'shipment_id' => $shipment_id,
                    'item_amount' => $data['item_amount'],
                    'sale_amount' => $oversea_item_sub_total_amount,
                    'shiping_fee' => $shipping_price_oversea,
                    'discount_total' => $data['discount_amount'],
                    'seller' => $data['country'],
                    'purchaser' => $purchaser_type,
                    'purchaser_country' => $purchaser_country,
                    'percent' => $total_percent,
                    'coupon_percent' => $coupon_percent_discount,
                    'fc_percent' => $purchaser_discount,
                    'sa_percent' => $sa_discount,
                    'coupon_fix' => $coupon_fix_amount,
                    'credit_added' => $credit_added,
                    'use_credit' => $use_credit,
                    'release_amount' => 0,
                    'transaction' => $credit_added,
                    'balance' => round($balance->balance + $credit_added),
                );
                if ($purchaser_type == 'FC' && $data['country'] == 'Global') {
                    $data_credit['country'] = $purchaser_country;
                }
                if ($credit_type == 'BUY') {
                    $data_credit['transaction'] = -($use_credit);
                    $data_credit['balance'] = round($balance->balance - $use_credit);
                }

                /*comment insert sales_history_credit by zun*/
                //$this->datacontrol_model->insert('sales_history_credit', $data_credit);
            }


        }

        if (in_array($payWith, array("use all point", "use point", "Bank Transfer", "pay on delivery"))
            || (in_array($_GET['use'], array("use all point", "use point")))) {
            if (!empty($data_domestic_sale_total)) {
                $data_domestic_sale_total = arrayToObject($data_domestic_sale_total);
            }
            if (!empty($data_oversea_sale_total)) {
                $data_oversea_sale_total = arrayToObject($data_oversea_sale_total);
            }

            if (strpos($purchaser_email, '@') !== false) {
                $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart, $purchaser);
            } else {
                $this->order_sms($data_domestic_sale_total, $data_oversea_sale_total, $cart, $purchaser);
            }
            echo true;
        }


        // exit();
        // $data['page'] = 'payment_complete_v';
        // $this->load->view('front_template', $data);
        if (!in_array($payWith, array("Bank Transfer", "pay on delivery"))) {
            echo json_encode(
                array(
                    "shipment_id" => $shipment_id,
                    "order_data" => array("data_domestic_sale_total" => $data_domestic_sale_total, "data_oversea_sale_total" => $data_oversea_sale_total, "cart" => $cart)
                ));
        }

    }

    public function payment_callback_paypal()
    {
        // $j = json_encode(array("post" => $_POST, "get" => $_GET));
        // $this->db->where('shipment_id', 'O000594D');
        // $this->db->update('sales_history_total', array('payment_data' => $j, "payment_status"=>"test"));
        // $this->db->where('shipment_id', 'O000578D');
        // $this->db->update('sales_history_total', array('payment_method' => 'test'));
        // $this->payment_complete();

        // {"post":{"mc_gross":"1","invoice":"O000585D","protection_eligibility":"Eligible","address_status":"confirmed","item_number1":"D1","item_number2":"D2","payer_id":"F5XYERM7JYPQU","item_number3":"other3","address_street":"9\/126  BaanKlangMueng Navamin70 Yak 11-2","payment_date":"12:21:07 Feb 20, 2020 PST","payment_status":"Completed","charset":"Shift_JIS","address_zip":"10240","first_name":"Kerapol","mc_fee":"1","address_country_code":"TH","address_name":"Kerapol Borpo","notify_version":"3.9","custom":"O000585D","payer_status":"verified","business":"info@sensha-world.com","address_country":"Thailand","num_cart_items":"3","address_city":"Bungkum","verify_sign":"A53pL.3jUNzWBci5h3TkCOsOFx6zACg48NMbXs98TroTJdo1HbqvsZLT","payer_email":"pinmanfa@hotmail.com","txn_id":"1XV63004K69542736","payment_type":"instant","payer_business_name":"imkb","last_name":"Borpo","address_state":"Bangkok","item_name1":"BODY CLEAN (high concentrated) 18L","receiver_email":"info@sensha-world.com","item_name2":"BODY CLEAN (high concentrated) 4L","payment_fee":"","item_name3":"Other","shipping_discount":"0","quantity1":"1","insurance_amount":"0","quantity2":"1","receiver_id":"LGCR72TXA925S","quantity3":"1","txn_type":"cart","discount":"0","mc_gross_1":"1","mc_currency":"JPY","mc_gross_2":"0","mc_gross_3":"0","residence_country":"TH","shipping_method":"Default","transaction_subject":"","payment_gross":"","ipn_track_id":"173123625718f"},"get":[]}
        $payment_gateway_data = json_encode($_POST);
        $this->payment_update($_POST['invoice'], $_POST['payment_status'], $payment_gateway_data);
    }

    public function payment_callback_amazon()
    {
        // $j = json_encode(array("post" => $_POST, "get" => $_GET));
        // $this->db->where('shipment_id', 'O000598D');
        // $this->db->update('sales_history_total', array('payment_data' => $j, "payment_status"=>"test"));

        $payment_status = ($_POST['status'] == 1) ? "Completed" : "Fail";
        $payment_gateway_data = json_encode($_POST);
        $this->payment_update($_POST['invoice'], $payment_status, $payment_gateway_data);
    }

    public function payment_update($shipment_id, $payment_status, $payment_gateway_data)
    {
        $this->db->where('shipment_id', $shipment_id);
        $data_order = $this->datacontrol_model->getRowData('sales_history_total');
        $purchaser_email = $this->ion_auth->user($data_order->create_by)->row()->email;

        $json_data = json_decode($data_order->json_data);

        // echo "<pre>";
        // print_r($json_data);
        // exit();

        $data_domestic_sale_total = '';
        $data_oversea_sale_total = '';
        $cart = objectToArray($json_data->cart);

        if (!empty($json_data->data_domestic_sale_total)) {
            $data_domestic_sale_total = $json_data->data_domestic_sale_total;
        }
        if (!empty($json_data->data_oversea_sale_total)) {
            $data_oversea_sale_total = $json_data->data_oversea_sale_total;
        }
        // print_r($data_domestic_sale_total);

        // print_r($data_domestic_sale_total);
        // echo $data_oversea_sale_total['payment_method'];
        // exit();

        // if (in_array($data_order->payment_method, array("paypal", "amazon"))) {
        //   $payment_gateway_data = '';
        //   if ($data_order->payment_method == "paypal") {
        //     if (isset($_GET['st'])) {
        //       $payment_status = $_GET['st'];
        //       $payment_gateway_data = json_encode($_GET);
        //     }
        //   } else if ($data_order->payment_method == "amazon") {
        //     if (isset($_GET['status'])) {
        //       $payment_status = ($_GET['status'] == 1) ? "Completed" : "Fail";
        //       $payment_gateway_data = json_encode($_GET);
        //     }
        //   }

        //   // echo $payment_status;
        //   // echo $payment_gateway_data;

        //   // exit();


        // }

        $this->db->where('shipment_id', $shipment_id);
        $this->db->update('sales_history_total', array('payment_status' => $payment_status, 'payment_data' => $payment_gateway_data));

        $this->db->where('shipment_id', $shipment_id);
        $this->db->update('sales_history', array('payment_status' => $payment_status));

        $this->db->where('shipment_id', $shipment_id);
        $this->db->update('sales_history_special', array('payment_status' => $payment_status));

        $user_data = $this->ion_auth->user($data_order->create_by)->row();
        $purchaser_email = $user_data->email;


        if ($data_order->payment_status != 'Completed') {

            if ($payment_status == "Completed") {
                $data_domestic_sale_total = array();
                $data_oversea_sale_total = array();
                if ($data_order->buy_type == "domestic") {
                    $data_domestic_sale_total = $data_order;
                }
                if ($data_order->buy_type == "oversea") {
                    $data_oversea_sale_total = $data_order;
                }
                if (strpos($purchaser_email, '@') !== false) {
                    $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart, $user_data);
                } else {
                    $this->order_sms($data_domestic_sale_total, $data_oversea_sale_total, $cart, $user_data);
                }
            }
        }

        //// credit data prepare
        // $this->db->where('shipment_id', $shipment_id);
        // $data_sale_total = $this->datacontrol_model->getRowData('sales_history_total');
        $data_sale_total = $data_order;

        $purchaser = $this->ion_auth->user($data_sale_total->create_by)->row();

        $purchaser_type = $this->ion_auth->get_users_groups($data_sale_total->purchaser_id)->row()->name;
        $purchaser_country = $purchaser->country;
        $purchaser_discount = $purchaser->discount_setting;

        if ($data_sale_total->account_type == 'admin' && $purchaser_type == 'SA') {
            $credit_type = 'BUY';
        } else {
            $credit_type = 'SELL';
        }
        if ($data_sale_total->buy_type == 'domestic') {
            $sa_discount = 100;
            if ($data_sale_total->country == 'Global' && $purchaser_type == 'FC') {
                $sa = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
                $sa_discount = $sa->discount_setting;
            }
        }
        if ($data_sale_total->buy_type == 'oversea') {
            $sa_discount = 0;
            if ($data_sale_total->country == 'Global' && $purchaser_type == 'FC') {
                $sa = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
                $sa_discount = $sa->discount_setting;
            }
        }


        $percent = array($data_sale_total->coupon_percent_discount, $purchaser_discount, $sa_discount);
        arsort($percent);
        $total_percent = 0;
        foreach ($percent as $val) {
            $total_percent = $val - $total_percent;
        }

        $this->db->order_by('id', 'desc');
        $this->db->where('country', $data_sale_total->purchaser_country)->or_where('credit_type', 'BUY');
        $balance = $this->datacontrol_model->getRowData('sales_history_credit');
        $credit_added = 0;
        if ($data_sale_total->sa_pay_global != 0) {
            $credit_added = $data_sale_total->sa_pay_global;
        }
        if ($data_sale_total->sa_pay_domestic != 0) {
            $credit_added = $data_sale_total->sa_pay_domestic;
        }

        //// credit
        $data_credit = array(
            'country' => $data_sale_total->country,
            'credit_type' => $credit_type,
            'shipment_id' => $shipment_id,
            'item_amount' => $data_sale_total->item_amount,
            'sale_amount' => $data_sale_total->item_sub_total_amount,
            'shiping_fee' => $data_sale_total->delivery_amount,
            'discount_total' => $data_sale_total->discount_amount,
            'seller' => $data_sale_total->country,
            'purchaser' => $purchaser_type,
            'purchaser_country' => $purchaser_country,
            'percent' => $total_percent,
            'coupon_percent' => $data_sale_total->coupon_percent_discount,
            'fc_percent' => $purchaser_discount,
            'sa_percent' => $sa_discount,
            'coupon_fix' => $data_sale_total->coupon_fix_amount,
            'credit_added' => $credit_added,
            'use_credit' => $data_sale_total->use_credit,
            'release_amount' => 0,
            'transaction' => $credit_added,
            'balance' => round($balance->balance + $credit_added),
        );

        if ($purchaser_type == 'FC' && $data_sale_total->country == 'Global') {
            $data_credit['country'] = $purchaser_country;
        }
        if ($credit_type == 'BUY') {
            $data_credit['transaction'] = -($data_sale_total->use_credit);
            $data_credit['balance'] = round($balance->balance - $data_sale_total->use_credit);
        }
        // if ($payment_status == "Completed") {
        //   $this->db->where('shipment_id', $shipment_id);
        //   $check_credit = $this->datacontrol_model->getAllData('sales_history_credit');
        //   if (empty($check_credit)) {
        //     $this->datacontrol_model->insert('sales_history_credit', $data_credit);
        //   }
        // }
    }

    public function payment_complete()
    {
        $shipment_id = '';
        if (isset($_GET['cm']) || isset($_POST['invoice'])) {
            $shipment_id = (isset($_GET['cm'])) ? $_GET['cm'] : $_POST['invoice'];
            $payment_gateway_data = (isset($_GET['cm'])) ? json_encode($_GET) : json_encode($_POST);
            $payment_status = (isset($_GET['st'])) ? $_GET['st'] : $_POST['payment_status'];
            $this->payment_update($shipment_id, $payment_status, $payment_gateway_data);
        }
        if (isset($_GET['invoice'])) {
            $shipment_id = $_GET['invoice'];
            $payment_status = ($_GET['status'] == 1) ? "Completed" : "Fail";
            $payment_gateway_data = json_encode($_GET);
            $this->payment_update($_GET['invoice'], $payment_status, $payment_gateway_data);
        }
        // if (isset($_GET['sensha_pay'])) {
        //   $this->save_order();
        // }


        $this->session->unset_userdata('shipping_domestic');
        $this->session->unset_userdata('shipping_oversea');
        $this->session->unset_userdata('payWith');
        $this->session->unset_userdata('sensha_cart');
        $this->session->unset_userdata('cart_next_page');
        $this->session->unset_userdata('cart');

        $data['page'] = 'payment_complete_v';
        $this->load->view('front_template', $data);
    }

    public function payment_cancel()
    {
        $this->session->unset_userdata('shipping_domestic');
        $this->session->unset_userdata('shipping_oversea');
        $this->session->unset_userdata('payWith');
        $this->session->unset_userdata('sensha_cart');
        $this->session->unset_userdata('cart_next_page');
        $this->session->unset_userdata('cart');
        $data['page'] = 'payment_cancel_v';
        $this->load->view('front_template', $data);
    }


    public function order_sms($data_domestic_sale_total, $data_oversea_sale_total, $cart, $user_data)
    {
        $user = $user_data;

        require_once 'vendor/autoload.php';
        $basic = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
        $client = new \Nexmo\Client($basic);

        $this->db->where('nicename', $user->country);
        $countryCode = $this->datacontrol_model->getRowData('countries');
        $phone = $user->phone;
        $firstCharacter = substr($phone, 0, 1);
        if ($firstCharacter == 0) {
            $phone = substr($phone, 1);
        }

        $text = $user->username . " " . $this->lang->line('shop_sms_greeting', FALSE);
        if (!empty($data_domestic_sale_total)) {
            $text .= $this->lang->line('shop_sms_domestic', FALSE) . $data_domestic_sale_total->shipment_id . "：" . number_format($data_domestic_sale_total->sale_total_amount + $data_domestic_sale_total->admin_fee_amount + $data_domestic_sale_total->vat_amount) . " Yen]";
        }
        if (!empty($data_oversea_sale_total)) {
            $text .= $this->lang->line('shop_sms_international', FALSE) . $data_oversea_sale_total->shipment_id . "：" . number_format($data_oversea_sale_total->sale_total_amount + $data_oversea_sale_total->admin_fee_amount + $data_oversea_sale_total->vat_amount) . " Yen]";
        }
        $message = $client->message()->send([
            'to' => '+' . $countryCode->phonecode . $phone,
            'from' => 'Sensha',
            'text' => $text,
            'type' => 'unicode'
        ]);
        $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart, $user_data);
    }

    public function order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart, $user_data)
    {
        $user = $user_data;
        $this->db->where('user_id', $user->id);
        $shipping_info = $this->datacontrol_model->getRowData('shipping_info');
        $send_list = array();
        $bcc = array();
        if (strpos($user->email, '@') !== false) {
            $send_list[$user->email] = $shipping_info->name;
        }
        $bcc[$this->config->item('contact_email')] = 'Sensha';
        if ($user->country == 'JapanB') {
            $bcc['h-harada@sensha-world.com'] = 'Admin JapanB';
        }

        if (@$data_oversea_sale_total->payment_method == 'Bank Transfer' || @$data_domestic_sale_total->payment_method == 'Bank Transfer') {
            $payment_method = $this->lang->line('page_payment_pay_with_bank_tranfer_forsave', FALSE);
        } elseif (@$data_oversea_sale_total->payment_method == 'pay on delivery' || @$data_domestic_sale_total->payment_method == 'pay on delivery') {
            $payment_method = $this->lang->line('page_payment_pay_on_delivery_forsave', FALSE);
        } elseif (!empty($data_oversea_sale_total->payment_method)) {
            $payment_method = $data_oversea_sale_total->payment_method;
        } else {
            $payment_method = @$data_domestic_sale_total->payment_method;
        }

        if (!empty($user->company_name)) {
            $to_company = $user->company_name;
        } else {
            $to_company = $shipping_info->company;
        }
        if (!empty($user->contact_person)) {
            $to_name = $user->contact_person;
        } else {
            $to_name = $shipping_info->name;
        }

        //$msg = $user->email."様<br /><br />";
        $msg = $this->lang->line('mail_payment_to_customer_dear', FALSE) . " " . $to_company . " " . $to_name . " " . $this->lang->line('mail_payment_to_customer_sama', FALSE) . '<br /><br />';
        //$msg .= "洗車の王国　公式ホームページをご利用いただき、誠にありがとうございます。<br />
        //ご注文を承りましたので、内容/配送/決済のご案内をさせていただきます。<br /><br />";
        $msg .= $this->lang->line('mail_payment_to_customer', FALSE) . '<br /><br />';

        if (!empty($data_domestic_sale_total)) {
            // if($data_domestic_sale_total['account_type'] == 'SA' && $data_domestic_sale_total['purchaser_type'] == 'FC'){
            //   $bcc[$data_domestic_sale_total['account_name']] = $data_domestic_sale_total['account_name'];
            // }
            $shipment_id_show = $data_domestic_sale_total->shipment_id;
            $bcc[$data_domestic_sale_total->account_name] = $data_domestic_sale_total->account_name;
            $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE) . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE) . $data_domestic_sale_total->shipment_id . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE) . date('Y-m-d H:i:s') . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE) . $payment_method . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE) . $data_domestic_sale_total->shipping_method . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE) . "$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->address $shipping_info->country $shipping_info->phone<br /><br />";
            //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'])." Yen<br /><br />";
            //$msg .= $this->lang->line('mail_payment_to_customer_separate_line', FALSE)."<br />";
            foreach ($cart['domestic'] as $key => $value) {
                $msg .= $cart['domestic'][$key]['product_name'] . "<br />";
                $msg .= $cart['domestic'][$key]['product_id'] . "<br />";
                $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE) . $cart['domestic'][$key]['item_amount'] . "<br />";
                $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE) . number_format($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']) . " Yen<br /><br />";
            }
            $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE) . number_format($data_domestic_sale_total->item_sub_total_amount) . " Yen<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE) . number_format($data_domestic_sale_total->delivery_amount) . " Yen<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE) . number_format($data_domestic_sale_total->discount_amount) . " Yen<br />";
           // $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE) . number_format($data_domestic_sale_total->use_credit) . " pt<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE) . number_format($data_domestic_sale_total->sale_total_amount) . " Yen<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE) . number_format($data_domestic_sale_total->admin_fee_amount) . " Yen<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE) . number_format($data_domestic_sale_total->vat_amount) . " Yen<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE) . number_format($data_domestic_sale_total->sale_total_amount + $data_domestic_sale_total->admin_fee_amount + $data_domestic_sale_total->vat_amount) . " Yen<br /><br />";
        }

        if (!empty($data_oversea_sale_total)) {
            $shipment_id_show = $data_oversea_sale_total->shipment_id;
            $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE) . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE) . $data_oversea_sale_total->shipment_id . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE) . date('Y-m-d H:i:s') . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE) . $payment_method . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE) . $data_oversea_sale_total->shipping_method . "<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE) . "$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->address $shipping_info->country $shipping_info->phone<br /><br />";
            //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'])." Yen<br /><br />";
            //$msg .= "============================<br />";
            foreach ($cart['oversea'] as $key => $value) {
                $msg .= $cart['oversea'][$key]['product_name'] . "<br />";
                $msg .= $cart['oversea'][$key]['product_id'] . "<br />";
                $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE) . $cart['oversea'][$key]['item_amount'] . "<br />";
                $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE) . number_format($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']) . " Yen<br /><br />";
            }
            $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE) . number_format($data_oversea_sale_total->item_sub_total_amount) . " Yen<br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE) . number_format($data_oversea_sale_total->delivery_amount) . " Yen<br />";

            $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE) . number_format($data_oversea_sale_total->discount_amount) . " Yen<br />";

           // $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE) . number_format($data_oversea_sale_total->use_credit) . " pt<br /><br />";

            $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE) . number_format($data_oversea_sale_total->sale_total_amount) . " Yen<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE) . number_format($data_oversea_sale_total->admin_fee_amount) . " Yen<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE) . number_format($data_oversea_sale_total->vat_amount) . "<br /><br />";
            $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE) . number_format($data_oversea_sale_total->sale_total_amount + $data_oversea_sale_total->admin_fee_amount + $data_oversea_sale_total->vat_amount) . " Yen<br /><br />";

//            if (in_array($data_oversea_sale_total->shipping_method, array('BUYER TRANSPORTATION', 'SELLER TRANSPORTATION')) || in_array($data_domestic_sale_total->shipping_method, array('BUYER TRANSPORTATION', 'SELLER TRANSPORTATION'))) {
            //               $msg .= $this->lang->line('mail_payment_to_customer_notice_title', FALSE) . "<br />";
//                $msg .= "-----------------------------------------------------------<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE) . number_format($data_oversea_sale_total->sale_total_amount + $data_domestic_sale_total->sale_total_amount + $data_domestic_sale_total->admin_fee_amount + $data_domestic_sale_total->vat_amount + $data_oversea_sale_total->admin_fee_amount + $data_oversea_sale_total->vat_amount) . " Yen<br /><br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_bank', FALSE) . "<br />";
//               $msg .= $this->lang->line('mail_payment_to_customer_bank_account', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_bank2', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_bank2_account', FALSE) . "<br /><br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_caution', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_caution2', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_caution3', FALSE) . "<br />";
//                $msg .= $this->lang->line('mail_payment_to_customer_caution4', FALSE) . "<br /><br />";
//                $msg .= "-----------------------------------------------------------<br /><br />";
//            }
        }

        $msg .= "-----------------------------------------------------------<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice', FALSE) . "<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice2', FALSE) . "<br />";
        $msg .= "<a href='https://store.sensha-world.com/page/order_history'>https://store.sensha-world.com/page/order_history</a><br />";
        $msg .= "-----------------------------------------------------------<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice3', FALSE) . "<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice4', FALSE) . "<br />";
        $msg .= "<a href='https://store.sensha-world.com/page/invoice/" . $shipment_id_show . "'>https://store.sensha-world.com/page/invoice/" . $shipment_id_show . "</a><br />";
        $msg .= "-----------------------------------------------------------<br /><br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice5', FALSE) . "<br /><br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice6', FALSE) . "<br /><br />";
        $msg .= "<a href='https://store.sensha-world.com/page/news'>https://store.sensha-world.com/page/news</a><br /><br />";
        $msg .= "===========================================================<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice7', FALSE) . "<br />";
        $msg .= "===========================================================<br /><br />";
        $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice8', FALSE) . "<br />";

        $mail_title = $this->lang->line('mail_payment_to_customer_mail_title', FALSE);
        mail_to($send_list, $mail_title, $msg, $bcc);
    }

    public function order_history()
    {
        $user_id = $this->ion_auth->user()->row()->id;
        // $this->db->where('buy_type', 'domestic');

        // $order_history_domestic = $this->datacontrol_model->getAllData('sales_history');
        $uri_segment = 3;

        $this->load->library('pagination');
        $config = array();
        $config['page_query_string'] = false;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = TRUE;
        $config['num_links'] = 5;
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "page/order_history";

        $this->db->group_start();
        $this->db->group_start();
        $this->db->where_in('payment_method', array('amazon', 'paypal'));
        $this->db->where('payment_status', 'Completed');
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where_in('payment_method', array('pay on delivery', 'Bank Transfer', Null));
        $this->db->group_end();
        $this->db->group_end();

        $this->db->where('purchaser_id', $user_id);
        $this->db->order_by('id', 'desc');
        $this->db->group_by('shipment_id');
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');
        //echo $this->db->last_query();
        $config["total_rows"] = count($data["order_history"]);
        $config["per_page"] = 5;
        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        // $config['first_link'] = 'First';
        // $config['first_tag_open'] = '<li>';
        // $config['first_tag_close'] = '</li>';
        //
        // $config['last_link'] = 'Last';
        // $config['last_tag_open'] = '<li>';
        // $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
        $data["links"] = $this->pagination->create_links();

        $this->db->group_start();
        $this->db->group_start();
        $this->db->where_in('payment_method', array('amazon', 'paypal'));
        $this->db->where('payment_status', 'Completed');
        $this->db->group_end();

        $this->db->or_group_start();
        $this->db->where_in('payment_method', array('pay on delivery', 'Bank Transfer'));
        $this->db->or_where('payment_method IS NULL');
        $this->db->group_end();
        $this->db->group_end();

        $this->db->where('purchaser_id', $user_id);
        $this->db->order_by('id', 'desc');
        $this->db->group_by('shipment_id');
        $this->db->limit($config["per_page"]);
        $newpage = ($page - 1) < 0 ? 0 : $page - 1;
        $this->db->offset($newpage * $config["per_page"]);
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');

        $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('user_id' => $user_id));

        $data['page'] = 'order_history_v';
        $this->load->view('front_template', $data);
    }

    public function order_history_bk()
    {
        $user_id = $this->ion_auth->user()->row()->id;
        // $this->db->where('buy_type', 'domestic');

        // $order_history_domestic = $this->datacontrol_model->getAllData('sales_history');
        $uri_segment = 3;

        $this->load->library('pagination');
        $config = array();
        $config['page_query_string'] = false;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = TRUE;
        $config['num_links'] = 5;
        $config['reuse_query_string'] = true;
        $config["base_url"] = base_url() . "page/order_history";


        $this->db->where('create_by', $user_id);
        $this->db->order_by('id', 'desc');
        $this->db->group_by('shipment_id');
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');

        $config["total_rows"] = count($data["order_history"]);
        $config["per_page"] = 5;
        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        // $config['first_link'] = 'First';
        // $config['first_tag_open'] = '<li>';
        // $config['first_tag_close'] = '</li>';
        //
        // $config['last_link'] = 'Last';
        // $config['last_tag_open'] = '<li>';
        // $config['last_tag_close'] = '</li>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
        $data["links"] = $this->pagination->create_links();

        $this->db->where('create_by', $user_id);
        $this->db->order_by('id', 'desc');
        $this->db->group_by('shipment_id');
        $this->db->limit($config["per_page"], $page);
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');

        $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('user_id' => $user_id));

        $data['page'] = 'order_history_v';
        $this->load->view('front_template', $data);
    }

    public function claim($shipment_id)
    {
        $user_id = $this->ion_auth->user()->row()->id;

        $this->db->where('shipment_id', $shipment_id);
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history');

        $this->db->where('shipment_id', $shipment_id);
        $data['order'] = $this->datacontrol_model->getRowData('sales_history_total');

        $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('create_by' => $user_id));
        $data['shipment_id'] = $shipment_id;
        $data['page'] = 'claim_v';
        $this->load->view('front_template', $data);
    }

    public function claim_confirm()
    {
        $user_id = $this->ion_auth->user()->row()->id;
        $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('create_by' => $user_id));

        $this->db->where('shipment_id', $this->input->post('shipment_id'));
        $data['item_list'] = $this->datacontrol_model->getAllData('sales_history');

        $this->db->where('shipment_id', $this->input->post('shipment_id'));
        $data['order'] = $this->datacontrol_model->getRowData('sales_history_total');

        $this->session->set_userdata('claim_date', $_POST);
        $data['page'] = 'claim_confirm_v';
        $this->load->view('front_template', $data);
    }

    public function claim_completed()
    {
        $_POST = $this->session->userdata('claim_date');
        $user_id = $this->ion_auth->user()->row()->id;
        $shipping_info = $this->datacontrol_model->getRowData('shipping_info', array('create_by' => $user_id));

        $this->db->where('shipment_id', $this->input->post('shipment_id'));
        $item_list = $this->datacontrol_model->getAllData('sales_history');

        $this->db->where('shipment_id', $this->input->post('shipment_id'));
        $order = $this->datacontrol_model->getRowData('sales_history_total');

        $this->db->where('id', $user_id);
        $user = $this->datacontrol_model->getRowData('users');

        // print_r($this->input->post());
        $subject = $this->lang->line('claim_mail_title', FALSE) . $this->input->post('shipment_id');
        //$subject = "洗車の王国へ配送に関する問題を承りました【洗車の王国】 " . $this->input->post('shipment_id');
        $msg = $this->lang->line('claim_mail_cliant_en', FALSE) . ' ' . $user->company_name . ' ' . $user->contact_person . ' ' . $this->lang->line('claim_mail_cliant_jp', FALSE);
        $msg .= $this->lang->line('claim_mail_paragraph01', FALSE);
        $msg .= $this->lang->line('claim_mail_shipment_id', FALSE) . $this->input->post('shipment_id') . "<br>";
        $msg .= $this->lang->line('claim_mail_order_date', FALSE) . $order->sale_date . "<br>";
        $msg .= $this->lang->line('claim_mail_email', FALSE) . $this->input->post('email') . "<br>";
        $msg .= $this->lang->line('claim_mail_reason', FALSE) . $this->input->post('reason') . "<br>";
        $msg .= $this->lang->line('claim_mail_comment', FALSE) . $this->input->post('comment') . "<br>";
        $msg .= $this->lang->line('claim_mail_paragraph02', FALSE);

        $send_list = array($this->input->post('email') => $shipping_info->name);
        $bcc = array($this->config->item('contact_email') => 'Sensha');
        if ($order->account_type == 'SA' && $order->purchaser_type == 'FC') {
            $fc = $this->ion_auth->user($order->purchaser_id)->row();
            $sa = $this->ion_auth->user($fc->create_by)->row();
            $send_list[$sa->email] = '';
        }

        mail_to($send_list, $subject, $msg, $bcc);
        $data['page'] = 'claim_complete_v';
        $this->load->view('front_template', $data);
    }

    public function cart()
    { //zun
        $this->session->unset_userdata(array('payWith', 'shipping_domestic', 'payFee'));

        $data['noindex'] = true;
        $user_country = $this->ion_auth->user()->row()->country;
        $user_zipcode = $this->ion_auth->user()->row()->zipcode;
        $data['user_zipcode'] = $user_zipcode;

        $cart = $this->session->userdata('sensha_cart');
        $data['cart'] = $cart;

        $user_id = $this->ion_auth->user()->row()->id;


        //$nation_lang  = $this->datacontrol_model->getRowData('nation_lang', array('country' => $user_country ));

        $this->db->where('nicename', $user_country);
        $zone = $this->datacontrol_model->getRowData('countries');
        $zone = "zone" . $zone->zone;

        //check session sensha_cart
        if (!$this->session->has_userdata('sensha_cart')) {
            //delete_cookie('user_language');
            //$this->ion_auth->logout();
            redirect("", 'refresh');
        }

        $delivery_price = 0;
        $delivery_price_inter = 0;
        $data["delivery_local"] = 0;

        $data['payment_mode'] = 'other';

        $this->db->where("user_id", $user_id);
        $shiping_info = $this->datacontrol_model->getRowData('shipping_info');

        if ($shiping_info) {

            //check country
            if ($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB') {
                //get area japan
                $area_japan = $this->get_zipcode($shiping_info->zipcode);

                if ($area_japan) {
                    //domestic general
                    $delivery_price_general = 0;
                    $delivery_price_ppf = 0;
                    $weight_general = 0;
                    $weight_ppf = 0;
                    if ($cart['domestic']) {

                        foreach ($cart['domestic'] as $key => $value) {
                            if ($cart['domestic'][$key]['product_type'] == 'general') {
                                $weight_general += $cart['domestic'][$key]['product_weight'] * $cart['domestic'][$key]['item_amount'];
                            }
                            if ($cart['domestic'][$key]['product_type'] == 'ppf') {
                                $weight_ppf += $cart['domestic'][$key]['product_weight'] * $cart['domestic'][$key]['item_amount'];
                            }
                        }
                    }

                    $this->db->where('weight_max >=', $weight_general);
                    $this->db->where('weight_min <=', $weight_general);
                    $price_general = $this->datacontrol_model->getRowData('delivery_fee_general_japan');

                    $this->db->where('weight_max >=', $weight_ppf);
                    $this->db->where('weight_min <=', $weight_ppf);
                    $price_ppf = $this->datacontrol_model->getRowData('delivery_fee_ppf_japan');

                    if ($price_general) {
                        $delivery_price_general = $price_general->$area_japan;
                    }

                    if ($price_ppf) {
                        if ($area_japan != 'okinawa_others') {
                            $area_japan = 'normal';
                            $delivery_price_ppf = $price_ppf->$area_japan;
                        } else {
                            $delivery_price_ppf = $price_ppf->$area_japan;
                        }
                    }
                    if ($delivery_price_ppf == 9999999 || $delivery_price_general == 9999999) {
                        $data['payment_mode'] = 'bank_trnsfer';
                        $delivery_price_ppf = 0;
                        $delivery_price_general = 0;
                    }

                    $data['ppf_price'] = "ppf : " . $delivery_price_ppf . " , weight : " . $weight_ppf;
                    $data['general_price'] = "general : " . $delivery_price_general . " , weight : " . $weight_general;
                    $delivery_price = $delivery_price_ppf + $delivery_price_general;
                } else {
                    //No zone
                    $data['payment_mode'] = 'no_zone';
                }
            } else {
                //no japan
                //inter
                $weight_general_inter = 0;
                $weight_ppf_inter = 0;
                $delivery_price_general_inter = 0;
                $delivery_price_ppf_inter = 0;

                //print_r($cart);
                if ($cart['oversea']) {
                    foreach ($cart['oversea'] as $key => $value) {
                        if ($cart['oversea'][$key]['product_type'] == 'general') {
                            $weight_general_inter += $cart['oversea'][$key]['product_weight'] * $cart['oversea'][$key]['item_amount'];
                        }
                        if ($cart['oversea'][$key]['product_type'] == 'ppf') {
                            $weight_ppf_inter += $cart['oversea'][$key]['product_weight'] * $cart['oversea'][$key]['item_amount'];
                        }
                    }
                }


                $this->db->where('weight_max >=', $weight_general_inter);
                $this->db->where('weight_min <=', $weight_general_inter);
                $price_general_inter = $this->datacontrol_model->getRowData('delivery_fee_general_ems');
                //echo $this->db->last_query();

                $this->db->where('weight_max >=', $weight_ppf_inter);
                $this->db->where('weight_min <=', $weight_ppf_inter);
                $price_ppf_inter = $this->datacontrol_model->getRowData('delivery_fee_ppt_ems');
                //echo $this->db->last_query();

                //var_dump($price_general_inter);
                if ($price_general_inter) {
                    if (isset($zone)) {
                        if ($price_general_inter->$zone == 9999999) {
                            $delivery_price_general_inter = 0;
                            $data['payment_mode'] = 'bank_trnsfer';
                        } else {
                            $delivery_price_general_inter = $price_general_inter->$zone;
                        }
                    } else {
                        $data['payment_mode'] = 'no_zone';
                    }
                }

                if ($price_ppf_inter) {
                    if (isset($zone)) {
                        if ($price_ppf_inter->$zone == 9999999) {
                            $delivery_price_ppf_inter = 0;
                            $data['payment_mode'] = 'bank_trnsfer';
                        } else {
                            $delivery_price_ppf_inter = $price_ppf_inter->$zone;
                        }
                    } else {
                        $data['payment_mode'] = 'no_zone';
                    }
                }

                $data['ppf_price_ems'] = "ppf : " . $delivery_price_ppf_inter . " , weight : " . $weight_ppf_inter;
                $data['general_price_ems'] = "general : " . $delivery_price_general_inter . " , weight : " . $weight_general_inter;
                $delivery_price_inter = $delivery_price_general_inter + $delivery_price_ppf_inter;
            }

            $data["japan_delivery_fee"] = $delivery_price;
            $data['ems'] = $delivery_price_inter;

            //end domestic
        } else {
            $data['payment_mode'] = 'no_zone';
        }

        $data['delivery_local'] = 0;

        $this->session->set_userdata('sensha_mode', $data['payment_mode']);

        $this->db->where('country', $this->user_lang);
        $data['delivery_domestic'] = $this->datacontrol_model->getRowData('delivery');
        // echo $this->db->last_query();
        // delete_cookie('recent_viewed_items');
        $c = get_cookie('recent_viewed_items');
        $e = explode(',', $c);
        $e = array_reverse($e);

        $i = 1;
        $list_product = array();
        $p_id = array();
        foreach ($e as $item) {
            $s = explode('|', $item);
            if (!in_array($s[1], $p_id)) {
                array_push($p_id, $s[1]);
                if ($i < 6) {
                    if ($s[0] == 'general') {
                        $this->db->select("*, 'general' as product_type");
                        $this->db->where('product_id', $s[1]);
                        $this->db->where('product_country', $user_country);
                        $product = $this->datacontrol_model->getAllData('product_general');
                        $list_product = array_merge($list_product, $product);
                    }
                    if ($s[0] == 'ppf') {
                        $this->db->select("*, 'ppf' as product_type");
                        $this->db->where('product_id', $s[1]);
                        $this->db->where('product_country', $user_country);
                        $product = $this->datacontrol_model->getAllData('product_ppf');
                        $list_product = array_merge($list_product, $product);
                    }
                }
                $i++;
            }
        }

        $data["recent_product"] = $list_product;

        // $this->db->where('buy_type', 'domestic');
        $this->db->where('country', $this->user_lang);
        $this->db->where('create_by', $user_id);
        $this->db->order_by('id', 'desc');
        // $this->db->group_by('shipment_id');
        // $this->db->limit(5);
        $data['order_history'] = $this->datacontrol_model->getAllData('sales_history');

        $data['user_country'] = $user_country;
        $data['coutry_iso'] = $this->coutry_iso . "/";

        $this->db->where("user_id", $user_id);
        $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');


        $data['page'] = 'cart_v';
        $this->load->view('front_template', $data);
    }

    public function place_delivery()
    {
        $data['noindex'] = true;
        $user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('user_id', $user_id);
        $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');

        $data['countries'] = $this->datacontrol_model->getAllData('countries');

        $data['page'] = 'place_delivery_v';
        $this->load->view('front_template', $data);
    }

    public function invoice($shipment_id)
    {
        $this->db->where('shipment_id', $shipment_id);
        $data['sales_history'] = $this->datacontrol_model->getAllData('sales_history');

        $this->db->where('shipment_id', $shipment_id);
        $data['sales_history_total'] = $this->datacontrol_model->getRowData('sales_history_total');

        $user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('user_id', $user_id);
        $data['shipping_info'] = $this->datacontrol_model->getRowData('invoice_info');
        if (empty($data['shipping_info'])) {
            $this->db->where('user_id', $user_id);
            $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info');
        }
        $data['coutry_iso'] = $this->coutry_iso;

        $html = $this->load->view('user/invoice_v', $data, true);

        // Include the main TCPDF library (search for installation path).
        require_once('assets/TCPDF/tcpdf.php');

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        //$font = new TCPDF_FONTS();
        //$pdf->Text(0, 0, "alphabetica ABCDEFG" );
        // フォント：IPAゴシック
        //$font_1 = $font->addTTFfont('assets/fonts/ipag.ttf');
        // $font_1 = $font->addTTFfont('assets/fonts/Pro_w3.otf');Firefly
        // $font_1 = $font->addTTFfont('assets/fonts/Firefly.ttf');
        $pdf->SetFont('kozgopromedium', '', 32, '', true);
        // $pdf->Text(0, 15, "美しい日本語のフォントを表示" );
        // $pdf->Output("cd_cover_template.pdf", "I");
        // exit();

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('SENSHA Invoice');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        // $pdf->SetFont('times', 'BI', 20);
        //$font = new TCPDF_FONTS();
        //$font_1 = $font->addTTFfont('assets/fonts/ipag.ttf');
        $pdf->SetFont('kozgopromedium', '', 14, '', true);

        // add a page
        $pdf->AddPage();


        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // $pdf->Text(0, 15, "美しい日本語のフォントを表示" );

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output("invoice_$shipment_id.pdf", 'D');
        // I: send the file inline to the browser. The PDF viewer is used if available.
        // D: send to the browser and force a file download with the name given by name.
        // F: save to a local file with the name given by name (may include a path).
        // S: return the document as a string.


        //$this->load->view('user/invoice_v', $data);
    }


    public function receipt($shipment_id)
    {
        $this->db->where('shipment_id', $shipment_id);
        $data['sales_history_total'] = $this->datacontrol_model->getRowData('sales_history_total');

        $user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('user_id', $user_id);
        $data['shipping_info'] = $this->datacontrol_model->getRowData('invoice_info');
        if (empty($data['shipping_info'])) {
            $this->db->where('user_id', $user_id);
            $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info');
        }
        $data['coutry_iso'] = $this->coutry_iso;

        $html = $this->load->view('user/receipt_v', $data, true);

        // Include the main TCPDF library (search for installation path).
        require_once('assets/TCPDF/tcpdf.php');

        $pdf = new TCPDF('LANDSCAPE', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->AddPage();
        $pdf->SetFont('kozgopromedium', '', 32, '', true);
        // create new PDF document
        $pdf = new TCPDF('LANDSCAPE', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('SENSHA Receipt');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('kozgopromedium', '', 14, '', true);

        // add a page
        $pdf->AddPage();

        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->Output("Receipt_$shipment_id.pdf", 'D');
    }

    public function news()
    {
        $uri_segment = 3;

        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url() . "page/news";

        $data['htmlTitle'] = "News";
        $this->db->order_by('id', 'desc');
        $data['news_post'] = $this->datacontrol_model->getAllData('news_post');

        $config["total_rows"] = count($data["news_post"]);
        $config["per_page"] = 5;
        $config["uri_segment"] = $uri_segment;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        $config['next_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-next.png") . '">';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '<img src="' . base_url("assets/sensha-theme/images/arrow-p.png") . '">';
        $config['prev_tag_open'] = '<li class="previous">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active">';
        $config['cur_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li class="number">';
        $config['num_tag_close'] = '</li>';


        $this->pagination->initialize($config);
        $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
        $data["links"] = $this->pagination->create_links();

        $data['htmlTitle'] = "News";
        $this->db->order_by('id', 'desc');
        $this->db->limit($config["per_page"], $page);
        $data['news_post'] = $this->datacontrol_model->getAllData('news_post');

        $data['noindex'] = true;
        $data['page'] = 'news_v';
        $this->load->view('front_template', $data);
    }

    public function news_detail($id)
    {
        $this->db->where('id', $id);
        $data['news_post'] = $this->datacontrol_model->getRowData('news_post');
        $data['htmlTitle'] = $data['news_post']->title;
        $data['htmlDes'] = $data['news_post']->content;
        $data['page'] = 'news_detail_v';
        $this->load->view('front_template', $data);
    }

    public function qa()
    {
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $this->db->limit(5);
        $this->db->where('category', 'SENSHA');
        $data['cat_sensha'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Products');
        $data['cat_products'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Order Payment');
        $data['cat_order'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Delivery');
        $data['cat_delivery'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Return product');
        $data['cat_return'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Discount');
        $data['cat_discount'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Account');
        $data['cat_account'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'The others');
        $data['cat_other'] = $this->datacontrol_model->getAllData('qa_post');

        $data['htmlTitle'] = "Q&A: Support for frequent questions";

        $data['page'] = 'qa_v';
        $this->load->view('front_template', $data);
    }

    public function qa_category($cat)
    {
        $user_country = $this->ion_auth->user()->row()->country;
        $data['coutry_iso'] = $this->coutry_iso . "/";

        $cat = urldecode($cat);
        $this->db->where('category', $cat);

        $this->db->where('country', $this->user_lang)->or_where('country', 'Global');

        $data['htmlTitle'] = $cat;

        $data['cat'] = $cat;
        $data['qa_post'] = $this->datacontrol_model->getAllData('qa_post');
        $data['page'] = 'qa_category_v';
        $this->load->view('front_template', $data);
    }

    public function qa_detail($id)
    {
        $data['coutry_iso'] = $this->coutry_iso . "/";
        $this->db->where('id', $id);
        $data['qa'] = $this->datacontrol_model->getRowData('qa_post');

        $data['htmlTitle'] = $data['qa']->title;

        $this->db->limit(5);
        $this->db->where('category', 'SENSHA');
        $data['cat_sensha'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Products');
        $data['cat_products'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Order Payment');
        $data['cat_order'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Delivery');
        $data['cat_delivery'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Return product');
        $data['cat_return'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Discount');
        $data['cat_discount'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'Account');
        $data['cat_account'] = $this->datacontrol_model->getAllData('qa_post');

        $this->db->limit(5);
        $this->db->where('category', 'The others');
        $data['cat_other'] = $this->datacontrol_model->getAllData('qa_post');

        $data['page'] = 'qa_detail_v';
        $this->load->view('front_template', $data);
    }

    public function check_coupon()
    {

        $user = $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups($user->id)->row();

        // $coupon_code = 'KB';
        $coupon_code = $this->input->post('coupon_code');
        $today = date('Y-m-d');
        $this->db->where("start_date <= '$today' AND end_date >= '$today'");
        $this->db->where('coupon_code', $coupon_code);
        $discount_coupon = $this->datacontrol_model->getRowData('discount_coupon');

        $cart = $this->session->userdata('sensha_cart');

        $can_use = false;
        if (!empty($discount_coupon)) {
            $coupon_groups = $this->ion_auth->get_users_groups($discount_coupon->create_by)->row();
            // $coupon_creator = $this->ion_auth->user($discount_coupon->create_by)->row();


            if ($discount_coupon->country == 'Global') {
                if ($user_groups->name == 'GU' && !empty($cart['oversea'])) {
                    $can_use = true;
                }
            } else if ($discount_coupon->country == $user->country) {
                if ($user_groups->name == 'FC' || $user_groups->name == 'GU') {
                    $can_use = true;
                }
            }
        }

        if ($can_use) {
            $data = $cart;
            $data['coupon_discount'] = $discount_coupon->discount;
            $data['coupon_type'] = $discount_coupon->discount_type;
            $data['coupon_code'] = $coupon_code;
            // $data['coupon_discount'] = $discount_coupon->discount;
            // $data['coupon_discount'] = $discount_coupon->discount_type;
            $this->session->set_userdata('sensha_cart', $data);
            echo 1;
        } else {
            $this->session->unset_userdata('payWith');
            $data = $cart;
            unset($data['coupon_discount']);
            unset($data['coupon_type']);
            unset($data['coupon_code']);
            $this->session->set_userdata('sensha_cart', $data);
            echo 0;
        }
    }

    public function use_point()
    {
        $point = $this->input->post('point');
        $cart = $this->session->userdata('sensha_cart');
        $data = $cart;
        $data['use_credit'] = $point;
        $this->session->set_userdata('sensha_cart', $data);
        echo 1;
    }

    public function use_all_point()
    {
        $all_point = $this->input->post('all_point');
        $cart = $this->session->userdata('sensha_cart');
        $data = $cart;
        $data['use_credit'] = $all_point;
        $this->session->set_userdata('sensha_cart', $data);
        echo 1;
    }


    public function change_lang()
    {
        // set_cookie(array(
        //   'name'   => 'user_language',
        //   'value'  => $this->input->post('language'),
        //   'expire' => 3600*24*12
        // ));
        $lang = strtolower($this->input->post('language'));
        $lang = str_replace(" ", "_", $lang);
        $this->session->unset_userdata('user_language');
        $this->session->set_userdata('user_language', $lang);

        $this->db->where('nicename', $this->input->post('language'));
        $r = $this->datacontrol_model->getRowData('countries');
        echo strtolower($r->iso);
    }

    public function checkSerial($v)
    {
        echo file_get_contents('https://ppf-admin.sensha-world.com/service/checkSerial/' . $v);
    }

    public function test_mail()
    {
        echo implode(';', array('kerapol_bor@truecorp.co.th', 'pinmanfa@hotmail.com'));
        echo mail_to(array('kerapol.b@gmail.com' => 'kerapol'), 'test user', 'my test',
            array('kerapol_bor@truecorp.co.th' => 'kerapol', 'pinmanfa@hotmail.com' => 'kerapol'));
        // mail_to(array('kerapol.b@gmail.com'=> 'kerapol'), 'test admin', 'my test');
    }

    public function test_sms()
    {
        require_once 'vendor/autoload.php';
        $basic = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
        $client = new \Nexmo\Client($basic);

        echo strlen('SENSHAサイトでのご注文を承りました。国内注文「O000108D：9,805 Yen」海外注文「：O000108O：9,585 Yen」');

        $message = $client->message()->send([
            'to' => '+66803951444',
            'from' => 'Sensha',
            'text' => 'SENSHAサイトでのご注文を承りました。国内注文「O000108D：9,805 Yen」海外注文「：O000108O：9,585 Yen」',
            'type' => 'unicode'
        ]);
    }

    public function get_zipcode($zipcode)
    {
        $this->db->where('postal_code', $zipcode);
        $code = $this->datacontrol_model->getRowData('postal_code_japan');
        //echo $this->db->last_query();
        if ($code) {
            $area = str_replace("/", "_", $code->area);
            $area = str_replace(" ", "_", $area);
            return strtolower($area);
        } else {
            if (!empty($zipcode)) {

                $str = explode("-", $zipcode);
                $arr1 = str_split($str[0]);

                $n = 9;
                $laste_code = '-xxxx';
                for ($i = 0; $i <= $n; $i++) {
                    if ($arr1[0] == $i) {
                        if (($i == 1 || $i == 2 || $i == 5) && ($i != 0 || $i != 6 || $i != 3 || $i != 4 || $i != 7 || $i != 8 || $i != 9)) {
                            $arr1[1] = 'x';
                            $arr1[2] = 'x';
                            $master_zipcode = $arr1[0] . $arr1[1] . $arr1[2] . $laste_code;
                        } elseif ($i == 0 || $i == 3 || $i == 4 || $i == 6 || $i == 7 || $i == 8 || $i == 9) {
                            $n = 9;
                            for ($j = 0; $j <= $n; $j++) {
                                if ($arr1[1] == $j) {
                                    if ($i == 0) {
                                        $arr1[0] = 'x';
                                        $arr1[1] = $j;
                                    } else {
                                        $arr1[1] = $j;
                                    }
                                    $arr1[2] = 'x';
                                    $master_zipcode = $arr1[0] . $arr1[1] . $arr1[2] . $laste_code;
                                }
                            }
                        } else {
                            $arr1[0] = 'x';
                            $arr1[1] = 'x';
                            $arr1[2] = 'x';
                            $master_zipcode = $arr1[0] . $arr1[1] . $arr1[2] . $laste_code;
                        }
                    }
                }

                $this->db->where('postal_code', $master_zipcode);
                $code = $this->datacontrol_model->getRowData('postal_code_japan');
                //echo $this->db->last_query();
                if ($code) {
                    $area = str_replace("/", "_", $code->area);
                    $area = str_replace(" ", "_", $area);
                    return strtolower($area);
                }
            }
            return false;
        }
    }

    //payment cancle
    public function payment_cancle()
    {
        $data['page'] = 'payment_cancel_v';
        $this->load->view('front_template', $data);
    }
}
