<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
  var $user_lang;
  var $coutry_iso;
  function __construct()
  {
    parent::__construct();
    $this->load->model('database/datacontrol_model');
    // delete_cookie('user_language');
    // $this->user_lang = get_cookie('user_language');
    $this->user_lang = 'Global';
    $this->coutry_iso = "";

    if(!$this->ion_auth->logged_in()){
      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page'){
        $this->db->where('iso', strtoupper($this->uri->segment(1)));
        $r = $this->datacontrol_model->getRowData('countries');
        $this->user_lang = $r->nicename;
        $this->coutry_iso = strtolower($r->iso);
      }
    }
    else{
      if($this->session->has_userdata('user_language')){
        $this->user_lang = $this->session->userdata('user_language');
      }

      $this->db->where('nicename', $this->user_lang);
      $r = $this->datacontrol_model->getRowData('countries');
      $this->coutry_iso = strtolower($r->iso);

      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso){
        $segs = $this->uri->segment_array();
        $segs[1] = $this->coutry_iso;
        // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
        redirect(base_url($segs), 'refresh');
      }

    }






    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
      $this->coutry_iso = "";
    }

    $this->lang->load('set', strtolower($this->user_lang));



  }

  public function index(){
    $user_country =  $this->ion_auth->user()->row()->country;
    if($this->ion_auth->logged_in()){
      $this->db->where('country', $user_country)->or_where('country', 'Global');
    }
    else{
      $this->db->where('country', 'Global');
    }
    $this->db->limit(10);
    $this->db->order_by('id', 'desc');
    $data['news_post'] = $this->datacontrol_model->getAllData('news_post');

    $this->db->where('country', $this->user_lang);
    $data['page_content'] = $this->datacontrol_model->getRowData('page_home');
    $data['htmlTitle'] = $data['page_content']->meta_title;
    $data['htmlDes'] = $data['page_content']->meta_description;

    $this->db->where('is_show_home', 1);
    $this->db->limit(2);
    $data['home_products'] = $this->datacontrol_model->getAllData('product_general');

    $data['coutry_iso'] = $this->coutry_iso."/";

    $data['page'] = 'home_v';
    $this->load->view('front_template', $data);
  }

  public function about(){

    $this->db->where('country', $this->user_lang);
    $data['page_content'] = $this->datacontrol_model->getRowData('page_about');
    $data['htmlTitle'] = $data['page_content']->meta_title;
    $data['htmlDes'] = $data['page_content']->meta_description;

    $data['coutry_iso'] = $this->coutry_iso."/";

    $data['page'] = 'about_v';
    $this->load->view('front_template', $data);
  }

  public function about_ppf(){
    $this->db->where('country', $this->user_lang);
    $data['page_content'] = $this->datacontrol_model->getRowData('page_ppf');
    $data['htmlTitle'] = $data['page_content']->meta_title;
    $data['htmlDes'] = $data['page_content']->meta_description;

    $data['coutry_iso'] = $this->coutry_iso."/";

    $data['page'] = 'ppf_v';
    $this->load->view('front_template', $data);
  }

  public function agent(){

    $this->db->where('country', $this->user_lang);
    $data['page_content'] = $this->datacontrol_model->getRowData('page_agent');
    $data['htmlTitle'] = $data['page_content']->meta_title;
    $data['htmlDes'] = $data['page_content']->meta_description;

    $data['coutry_iso'] = $this->coutry_iso."/";

    $data['page'] = 'agent_v';
    $this->load->view('front_template', $data);
  }

  public function global_network(){
    $data['global_network'] = $this->datacontrol_model->getAllData('global_network');

    $data['coutry_iso'] = $this->coutry_iso."/";
    $data['htmlTitle']  = "global_network";
    $data['page'] = 'global_network_v';
    $this->load->view('front_template', $data);
  }

  public function terms(){
    $data['htmlTitle']  = "Terms of use";
    $data['coutry_iso'] = $this->coutry_iso."/";
    $data['page'] = 'terms_v';
    $this->load->view('front_template', $data);
  }

  public function privacy(){
    $data['htmlTitle']  = "Privacy Policy";
    $data['coutry_iso'] = $this->coutry_iso."/";
    $data['page'] = 'privacy_v';
    $this->load->view('front_template', $data);
  }

  public function contact(){
    $data['htmlTitle']  = "Contact Us";
    $data['coutry_iso'] = $this->coutry_iso."/";
    $data['page'] = 'contact_v';
    $this->load->view('front_template', $data);
  }

  public function contact_confirm(){
    $this->session->set_userdata('contact', $this->input->post());
    $data['coutry_iso'] = $this->coutry_iso."/";
    $data['page'] = 'contact_confirm_v';
    $this->load->view('front_template', $data);
  }

  public function contact_complete(){
    $_POST = $this->session->userdata('contact');
    $msg = "From: ".$this->input->post('name');
    $msg .= "<br />Email: ".$this->input->post('email');
    $msg .= "<br />Message: ".$this->input->post('msg');
    mail_to(array("info@enfete.asia" =>  "Sensha"), "Contact message", $msg);
    $this->session->unset_userdata('contact');
    $data['page'] = 'contact_complete_v';
    $this->load->view('front_template', $data);
  }

  public function general($cat, $cat2=''){
    // update `cluster2` set cluster_url = cluster_name;
    // update `cluster2` set cluster_url = REPLACE(cluster_name, ' ', '-');
    // update `cluster2` set cluster_url = REPLACE(cluster_url, '/', '-');
    // update `cluster2` set cluster_url = REPLACE(cluster_url, '&', '-');
    $data['coutry_iso'] = $this->coutry_iso."/";
    $cat = urldecode($cat);
    $this->db->where('cluster_url', $cat);
    $this->db->where('country', $this->user_lang);
    $query = $this->db->get('cluster1');
    $cluster1 = $query->row();
    $data['cluster1'] = $cluster1;
    $data['htmlTitle']  = $cluster1->cluster_name;
    // echo $this->db->last_query();
    // print_r($cluster1);

    $data['user_country'] = $this->ion_auth->user()->row()->country;


    if($cat2 != ''){
      $cat2 = urldecode($cat2);
      $this->db->where('cluster_url', $cat2);
      $this->db->where('country', $this->user_lang);
      $query = $this->db->get('cluster2');
      $cluster2 = $query->row();
      // echo $this->db->last_query();
    }

    $this->db->where('country', $this->user_lang);
    $this->db->where('level1', $cluster1->id);
    $data['sub_cluster'] = $this->datacontrol_model->getAllData('cluster2');



    $data['product_type'] = 'general';

    $this->load->library('pagination');
    $config = array();
    $config['page_query_string'] = TRUE;
    $config['use_page_numbers'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['num_links']=5;

    // $config["base_url"] = base_url() . "page/general/$cat";
    $config["base_url"] = base_url(uri_string());

    if($this->input->get('s')){
      $this->db->like('product_name', $this->input->get('s'));
    }
    if(!empty($this->input->post('usage'))){
      $this->db->where_in('usage', $this->input->post('usage'));
    }
    if(!empty($this->input->post('feature'))){
      $this->db->where_in('feature', $this->input->post('feature'));
    }
    if(!empty($this->input->post('material'))){
      $this->db->where_in('material', $this->input->post('material'));
    }
    if(!empty($this->input->post('item_type'))){
      $this->db->where('item_type', $this->input->post('item_type'));
    }
    $this->db->where('is_active', 1);
    $this->db->where('cat_1', $cluster1->id);
    if(!empty($cluster2)){
      $this->db->where('cat_2', $cluster2->id);
    }
    $this->db->where('product_country', $this->user_lang);
    $data["products"] = $this->datacontrol_model->getAllData('product_general');

    $config["total_rows"] = count($data["products"]);
    $config["per_page"] = 12;
    $config["uri_segment"] = $uri_segment;

    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';

    // $config['first_link'] = 'First';
    // $config['first_tag_open'] = '<li>';
    // $config['first_tag_close'] = '</li>';

    // $config['last_link'] = 'Last';
    // $config['last_tag_open'] = '<li>';
    // $config['last_tag_close'] = '</li>';

    $config['next_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-next.png").'">';
    $config['next_tag_open'] = '<li class="next">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-p.png").'">';
    $config['prev_tag_open'] = '<li class="previous">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active">';
    $config['cur_tag_close'] = '</li>';

    $config['num_tag_open'] = '<li class="number">';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
    $page = ($this->input->get('page')) ? $this->input->get('page')-1 : 0;
    $data["links"] = $this->pagination->create_links();

    if($this->input->get('s')){
      $this->db->like('product_name', $this->input->get('s'));
    }
    if(!empty($this->input->post('usage'))){
      $this->db->where_in('usage', $this->input->post('usage'));
    }
    if(!empty($this->input->post('feature'))){
      $this->db->where_in('feature', $this->input->post('feature'));
    }
    if(!empty($this->input->post('material'))){
      $this->db->where_in('material', $this->input->post('material'));
    }
    if(!empty($this->input->post('item_type'))){
      $this->db->where('item_type', $this->input->post('item_type'));
    }
    $this->db->where('is_active', 1);
    $this->db->where('cat_1', $cluster1->id);
    if(!empty($cluster2)){
      $this->db->where('cat_2', $cluster2->id);
    }
    $this->db->where('product_country', $this->user_lang);
    $this->db->limit($config["per_page"], $page*$config["per_page"]);
    $data["products"] = $this->datacontrol_model->getAllData('product_general');

    // print_r($this->input->post());
    // echo $this->db->last_query();
    // exit();

    if($this->input->post('product_filter')){
      echo $msg = $this->load->view('product/products_filter_general_v', $data, false);
    }
    else{
      $data['page'] = 'product/products_general_v';
      $this->load->view('product/products_general_v', $data);
    }



    // $this->load->view('front_template', $data);
  }

  public function ppf($cat='', $cat2=''){
    $data['noindex'] = true;
    $data['coutry_iso'] = $this->coutry_iso."/";
    if($this->input->get('cat_1') != ''){
      $this->db->where('id', $this->input->get('cat_1'));
    }
    else if($cat != ''){
      $this->db->where('cluster_name', urldecode($cat));
    }
    $this->db->where('country', $this->user_lang);
    $query = $this->db->get('cluster1');
    $cluster1 = $query->row();
    // echo $this->db->last_query();
    // print_r($cluster1);
    $data['cluster1'] = $cluster1;
    $data['htmlTitle']  = $cluster1->cluster_name;

    $data['user_country'] = $this->ion_auth->user()->row()->country;


    if($this->input->get('cat_2') != ''){
      $this->db->where('id', $this->input->get('cat_2'));
      $this->db->where('country', $this->user_lang);
      $query = $this->db->get('cluster2');
      $cluster2 = $query->row();
      $data['cluster2'] = $cluster2;
    }
    else if($cat2 != ''){
      $this->db->where('cluster_name', $cat2);
      $this->db->where('country', $this->user_lang);
      $query = $this->db->get('cluster2');
      $cluster2 = $query->row();
      $data['cluster2'] = $cluster2;
    }


    $this->db->where('country', $this->user_lang);
    $this->db->where('level1', $cluster1->id);
    $data['sub_cluster'] = $this->datacontrol_model->getAllData('cluster2');

    $this->load->library('pagination');
    $config = array();
    $config['page_query_string'] = TRUE;
    $config['use_page_numbers'] = TRUE;
    $config['query_string_segment'] = 'page';
    $config['num_links']=5;
    $config['reuse_query_string'] = true;

    // $uri_segment = 4;
    // $config["base_url"] = base_url() . "page/ppf/$cat";
    $config["base_url"] = base_url(uri_string());

    $this->db->where('is_active', 1);
    if(!empty($cluster1)){
      $this->db->where('cat_1', $cluster1->id);
    }

    if(!empty($cluster2)){
      $this->db->where('cat_2', $cluster2->id);
    }

    if(!empty($this->input->post('part'))){
      $this->db->where('parts', $this->input->post('part'));
    }
    $this->db->where('product_country', $this->user_lang);
    $data["products"] = $this->datacontrol_model->getAllData('product_ppf');
    // echo $this->db->last_query();
    // echo $cluster1->id;
    $data['product_type'] = 'ppf';


    $config["total_rows"] = count($data["products"]);
    $config["per_page"] = 12;
    $config["uri_segment"] = $uri_segment;

    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';

    // $config['first_link'] = 'First';
    // $config['first_tag_open'] = '<li>';
    // $config['first_tag_close'] = '</li>';

    // $config['last_link'] = 'Last';
    // $config['last_tag_open'] = '<li>';
    // $config['last_tag_close'] = '</li>';

    $config['next_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-next.png").'">';
    $config['next_tag_open'] = '<li class="next">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-p.png").'">';
    $config['prev_tag_open'] = '<li class="previous">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active">';
    $config['cur_tag_close'] = '</li>';

    $config['num_tag_open'] = '<li class="number">';
    $config['num_tag_close'] = '</li>';

    $this->pagination->initialize($config);
    $page = ($this->input->get('page')) ? $this->input->get('page')-1 : 0;
    $data["links"] = $this->pagination->create_links();


    $this->db->where('is_active', 1);
    if(!empty($cluster1)){
      $this->db->where('cat_1', $cluster1->id);
    }

    if(!empty($cluster2)){
      $this->db->where('cat_2', $cluster2->id);
    }

    if(!empty($this->input->post('part'))){
      $this->db->where('parts', $this->input->post('part'));
    }

    if(!empty($this->input->post('part'))){
      $this->db->where('parts', $this->input->post('part'));
    }

    $this->db->where('product_country', $this->user_lang);
    $this->db->limit($config["per_page"], $page*$config["per_page"]);
    $data["products"] = $this->datacontrol_model->getAllData('product_ppf');
    $data['product_type'] = 'ppf';
    // echo $this->db->last_query();


    if($this->input->post('product_filter')){
      echo $msg = $this->load->view('product/products_filter_ppf_v', $data, true);
    }
    else{
      $data['page'] = 'product/products_ppf_v';
      $this->load->view('product/products_ppf_v', $data);
    }
    // $this->load->view('front_template', $data);
  }

  public function product_detail($product_type, $product_id){
    // $this->session->unset_userdata('sensha_cart');
    $data["product_id"] = $product_id;
    $cart = $this->session->userdata('sensha_cart');
    $data['cart'] = $cart;

    $data['coutry_iso'] = $this->coutry_iso."/";
    if($product_type == 'ppf'){
      $this->db->where('product_id', $product_id);
      $this->db->where('product_country', 'Global');
      $data["products"] = $this->datacontrol_model->getRowData('product_ppf');
    }
    else if($product_type == 'general'){
      $this->db->where('product_id', $product_id);
      $this->db->where('product_country', 'Global');
      $data["products"] = $this->datacontrol_model->getRowData('product_general');
    }
    $data['product_type'] = $product_type;
    // $this->db->where('product_id', $product_id);
    // $query = $this->db->get('product_general');
    // $data["product"] = $query->row();
    $e = '';
    if(!is_null(get_cookie('recent_viewed_items'))){
      $c = get_cookie('recent_viewed_items');
      $e = explode(',', $c);
      array_push($e, "$product_type|$product_id");
      set_cookie(array(
        'name'   => 'recent_viewed_items',
        'value'  => implode(',', $e),
        'expire' => 3600*24*12
      ));
    }
    else{
      set_cookie(array(
        'name'   => 'recent_viewed_items',
        'value'  => "$product_type|$product_id",
        'expire' => 3600*24*12
      ));
      // $e = explode(',', $product_id);
    }

    $this->db->where_in('product_id', $e);
    $this->db->where('product_country', $this->user_lang);
    $this->db->limit(5);
    if($product_type == 'ppf'){
      $data["recent_product"] = $this->datacontrol_model->getAllData('product_ppf');
    }
    else{
      $data["recent_product"] = $this->datacontrol_model->getAllData('product_general');
    }
    // echo $this->db->last_query();

    // delete_cookie('recent_viewed_items');

    // $c = get_cookie('recent_viewed_items');
    // print_r($c);

    $this->db->where('id', $data["products"]->cat_1);
    $data["product_cat"] = $this->datacontrol_model->getRowData('cluster1');

    $data['htmlTitle']  = $data['products']->product_name;

    $data['user_country'] = $this->ion_auth->user()->row()->country;
    $data['user_lang'] = $this->user_lang;
    $data['page'] = 'product/product_detail_v';
    $this->load->view('product/product_detail_v', $data);
    // $this->load->view('front_template', $data);
  }

  public function payment(){
    $data['noindex'] = true;
    $user_id = $this->ion_auth->user()->row()->id;
    $user_country = $this->ion_auth->user()->row()->country;
    $this->db->where('user_id', $user_id);
    $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');

    $data['cart'] = $this->session->userdata('sensha_cart');

    $this->db->where('country', $user_country)->or_where('credit_type', 'BUY');
    $this->db->order_by('id', 'desc');
    $data['sales_history_credit'] = $this->datacontrol_model->getRowData('sales_history_credit');

    $data['user_country'] = $user_country;

    if($this->session->has_userdata('shipping_domestic')){
      $shipping_domestic = $this->session->userdata('shipping_domestic');
      $shipping_domestic_name = $shipping_domestic['shipping_name'];
      $shipping_price_domestic = $shipping_domestic['shipping_price'];
    }
    if($this->session->has_userdata('shipping_oversea')){
      $shipping_oversea = $this->session->userdata('shipping_oversea');
      $shipping_oversea_name = $shipping_oversea['shipping_name'];
      $shipping_price_oversea = $shipping_oversea['shipping_price'];
    }


    $data['shipping_domestic'] = $shipping_domestic;
    $data['shipping_oversea'] = $shipping_oversea;

    $data['page'] = 'payment_v';
    $this->load->view('front_template', $data);
  }

  public function payment_complete(){
    $data['noindex'] = true;
    $shipping_price_domestic = 0;
    $shipping_price_oversea = 0;

    $payWith = $this->session->userdata('payWith');
    $payFee = $this->session->userdata('payFee');
    $domestic_pay_on_delivery = $this->session->userdata('domestic_pay_on_delivery');

    $payment_gateway_data = '';
    if($payWith == 'paypal'){
      $payment_gateway_data = json_encode($_POST);
    }
    if($payWith == 'amazon'){
      $payment_gateway_data = json_encode($_GET);
    }
    // echo $payment_gateway_data;


    $data_domestic_sale_total = '';
    $data_oversea_sale_total = '';


    $purchaser = $this->ion_auth->user()->row();

    $purchaser_type = $this->ion_auth->get_users_groups()->row()->name;
    $purchaser_country = $purchaser->country;
    $purchaser_discount = $purchaser->discount_setting;
    // $purchaser_country = 'Thailand';
    $purchaser_id = $purchaser->id;

    $purchaser_email = $purchaser->email;

    if($this->session->has_userdata('shipping_domestic')){
      $shipping_domestic = $this->session->userdata('shipping_domestic');
      $shipping_domestic_name = $shipping_domestic['shipping_name'];
      $shipping_price_domestic = $shipping_domestic['shipping_price'];
      if($this->session->has_userdata('domestic_pay_on_delivery')){
        $shipping_price_domestic += $this->session->userdata('domestic_pay_on_delivery');
      }
    }
    if($this->session->has_userdata('shipping_oversea')){
      $shipping_oversea = $this->session->userdata('shipping_oversea');
      $shipping_oversea_name = $shipping_oversea['shipping_name'];
      $shipping_price_oversea = $shipping_oversea['shipping_price'];
      if($this->session->has_userdata('oversea_pay_on_delivery')){
        $shipping_price_oversea += $this->session->userdata('oversea_pay_on_delivery');
      }
    }




    $cart = $this->session->userdata('sensha_cart');
    // echo "<pre>";
    // print_r($cart);
    // print_r($shipping_domestic);
    // print_r($shipping_oversea);
    $domestic_item = 0;
    $domestic_price = 0;
    $domestic_total = 0;
    $domestic_import_shipping = 0;
    $domestic_import_tax = 0;
    foreach ($cart['domestic'] as $key => $value) {
      $domestic_item += $cart['domestic'][$key]['item_amount'];
      $domestic_price += ($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']);
      $domestic_import_shipping += ($cart['domestic'][$key]['import_shipping']>0)?$cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount']:0;
      $domestic_import_tax += ($cart['domestic'][$key]['import_tax']>0)?$cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount']:0;
    }
    $domestic_total = $domestic_price+$domestic_import_shipping+$domestic_import_tax+$shipping_price_domestic;

    $oversea_item = 0;
    $oversea_price = 0;
    $oversea_total = 0;
    foreach ($cart['oversea'] as $key => $value) {
      $oversea_item += $cart['oversea'][$key]['item_amount'];
      $oversea_price += ($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']);
    }
    $oversea_total = $oversea_price+$shipping_price_oversea;

    $shipment_id_domestic = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sales_history' order by AUTO_INCREMENT desc limit 1)), 6), 'D') as shipment_id")->row()->shipment_id;
    $shipment_id_oversea = $this->db->query("select CONCAT('O', RIGHT(CONCAT('000000',(SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sales_history' order by AUTO_INCREMENT desc limit 1)), 6), 'O') as shipment_id")->row()->shipment_id;

    $shipping_info = $this->datacontrol_model->getRowData('shipping_info', array('create_by'=>$purchaser_id));

    if(isset($cart['use_credit'])){
      $use_credit = $cart['use_credit'];
    }
    else{
      $use_credit = 0;
    }

    $domestic_total = 0;
    $domestic_item_sub_total_amount = 0;
    $domestic_user_discount_total_amount = 0;
    $domestic_item_amount = 0;

    $coupon_percent_discount = 0;
    $coupon_percent_amount = 0;
    $coupon_fix_amount = 0;

    if(isset($cart['domestic']) && !empty($cart['domestic'])){
      foreach ($cart['domestic'] as $key => $value) {
        $user_discount_amount = ($purchaser_discount/100)*(($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']) + ($cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount']) + ($cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount']));
        if($cart['coupon_discount']){
          if($cart['coupon_type'] == 'fixed'){
            $coupon_fix_amount = ($cart['coupon_discount']);
          }
          else{
            $coupon_percent_discount = $cart['coupon_discount'];
            $coupon_percent_amount = (($cart['coupon_discount']/100)*($cart['domestic'][$key]['price']* $cart['domestic'][$key]['item_amount']));
          }
        }

        $domestic_item_sub_total_amount += (($cart['domestic'][$key]['price']*$cart['domestic'][$key]['item_amount']) + $cart['domestic'][$key]['import_shipping'] + $cart['domestic'][$key]['import_tax']);
        $domestic_user_discount_total_amount += $user_discount_amount;
        $domestic_coupon_percent_total_amount += $coupon_percent_amount;
        $domestic_item_amount += $cart['domestic'][$key]['item_amount'];
        $data = array(
          // information
          'shipment_id' => $shipment_id_domestic,
          'country' => @$cart['domestic'][$key]['product_country'],
          'account_type' => @$cart['domestic'][$key]['sale_account_type'],
          'account_name' => @$cart['domestic'][$key]['sale_account_name'],
          'sale_date' => date("Y-m-d"),
          'sale_item_category' => @$cart['domestic'][$key]['product_category'],
          'buy_type' => 'domestic',
          'sale_item_id' => $key,
          'product_name' => $cart['domestic'][$key]['product_name'],
          'item_amount' => $cart['domestic'][$key]['item_amount'],
          'import_shipping' => ($cart['domestic'][$key]['import_shipping']>0)?$cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount']:0,
          'import_tax' => ($cart['domestic'][$key]['import_tax']>0)?$cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount']:0,
          'product_type' => $cart['domestic'][$key]['product_type'],
          'item_sub_total_amount' => $cart['domestic'][$key]['price'],
          'delivery_amount' => $shipping_price_domestic,
          // discount
          'discount_ratio' => $purchaser_discount,
          'user_discount_amount' => round($user_discount_amount),
          'discount_type' => '',
          'coupon_discount_type' => (isset($cart['coupon_type']))?'coupon - '.$cart['coupon_type']:'',
          'coupon_code' => $cart['coupon_code'],
          'coupon_fix_amount' => $coupon_fix_amount,
          'coupon_percent_discount' => $coupon_percent_discount,
          'coupon_percent_amount' => $coupon_percent_amount,
          'use_credit' => $use_credit,
          'discount_amount' => round($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit),
          'sale_total_amount' => round(($cart['domestic'][$key]['price'] + $shipping_price_domestic) - ($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit)),
          'purchaser_type' => $purchaser_type,
          'purchaser_country' => $purchaser_country,
          'purchaser_id' => $purchaser_id,
          'shipping_addr' => $shipping_info->address,
          'payment_status' => 'success',
          'payment_method' => $payWith,
          'payment_data' => $payment_gateway_data,
          'payment_date' => date("Y-m-d"),
          'shipping_method' => $shipping_domestic_name,
          'shipping_status' => 'Processing',
          'shipping_date' => '',
          'emergency_status' => '',
          'emergency_date' => '',
          'total_discount' => 0,
        );
        if(in_array($data['payment_method'], array('paypal', 'amazon', 'alibaba'))){
          $data['shipping_status'] = "Paid";
        }

        $domestic_total += $data['sale_total_amount'];
        // echo "<pre>";
        // print_r($data);


        $this->datacontrol_model->insert('sales_history', $data);
      }// end foreach domestic
      // exit();

      /// sales_history_total
      $data_sale_total = $data;
      $data_sale_total['item_amount'] = $domestic_item;
      $data_sale_total['item_sub_total_amount'] = $domestic_item_sub_total_amount;
      $data_sale_total['user_discount_amount'] = round($domestic_user_discount_total_amount);
      $data_sale_total['coupon_percent_amount'] = round($domestic_coupon_percent_total_amount);
      $data_sale_total['discount_amount'] = round($domestic_user_discount_total_amount + $coupon_fix_amount + $domestic_coupon_percent_total_amount + $use_credit);
      $data_sale_total['sale_total_amount'] = round(($domestic_item_sub_total_amount + $shipping_price_domestic) - ($domestic_user_discount_total_amount + $coupon_fix_amount + $domestic_coupon_percent_total_amount + $use_credit));

      //// commission
      $data_sale_total['sa_name'] = '-';
      $data_sale_total['sa_sale_percent'] = 0;
      $data_sale_total['sa_sale_amount'] = 0;
      $data_sale_total['sa_pay_global'] = 0;
      $data_sale_total['sa_pay_domestic'] = 0;
      $data_sale_total['aa_name'] = '-';
      $data_sale_total['aa_percent'] = 0;
      $data_sale_total['aa_pay'] = 0;

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'SA'){
        $sa = $this->datacontrol_model->getRowData('users', array('email'=>$data_sale_total['account_name']));
        $data_sale_total['sa_name'] = $sa->email;
      }

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_global'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'];
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = round(($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_global'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'];
        }
      }

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'GU'){

      }

      if($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = 100;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = round(($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = 100;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
        }
      }

      if(($data_sale_total['country'] == 'JapanA' || $data_sale_total['country'] == 'JapanB') && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = 50;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] ;
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = (($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = 50;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'];
        }
      }

      if($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'GU'){
        $sa = $this->datacontrol_model->getRowData('users', array('email'=>$data_sale_total['account_name']));
        $data_sale_total['sa_name'] = $sa->email;
        $data_sale_total['sa_sale_percent'] = 100;
        $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
        $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
      }


      $vat = $this->datacontrol_model->getRowData('nation_lang', array('country'=> $data_sale_total['purchaser_country']));
      $data_sale_total['vat_percent'] = $vat->vat;
      $data_sale_total['vat_amount'] = round(($vat->vat/100)*$data_sale_total['sale_total_amount']);
      $data_sale_total['admin_fee_percent'] = round($payFee);
      $data_sale_total['admin_fee_amount'] = round(($payFee>10)?$payFee:($payFee/100)*$data_sale_total['sale_total_amount']);
      if($payFee>10){
        $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount']+$data_sale_total['vat_amount']+$data_sale_total['admin_fee_amount']);
      }
      else{
        $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount']+$data_sale_total['vat_amount']+$data_sale_total['admin_fee_amount']);
      }



      // echo "<pre>";
      // print_r($data_sale_total);
      // exit();
      $data_domestic_sale_total = $data_sale_total;
      $this->datacontrol_model->insert('sales_history_total', $data_sale_total);
      // $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart);


      ///// Credit
      if($data['account_type'] == 'admin' && $purchaser_type == 'SA'){
        $credit_type = 'BUY';
      }
      else{
        $credit_type = 'SELL';
      }
      $sa_discount = 100;
      if($data['country'] == 'Global' && $purchaser_type == 'FC'){
        $sa = $this->ion_auth->user($purchaser_id)->row();
        $sa_discount = $sa->discount_setting;
      }
      $percent = array($coupon_percent_discount, $purchaser_discount, $sa_discount);
      arsort($percent);
      $total_percent = 0;
      foreach ($percent as $val) {
        $total_percent = $val - $total_percent;
      }

      $this->db->order_by('id', 'desc');
      $this->db->where('country', $data['purchaser_country'])->or_where('credit_type', 'BUY');
      $balance = $this->datacontrol_model->getRowData('sales_history_credit');
      $credit_added = 0;
      if($data_sale_total['sa_pay_global'] != 0){
        $credit_added = $data_sale_total['sa_pay_global'];
      }
      if($data_sale_total['sa_pay_domestic'] != 0){
        $credit_added = $data_sale_total['sa_pay_domestic'];
      }
      $data = array(
        'country' => $data['country'],
        'credit_type' => $credit_type,
        'shipment_id' => $shipment_id_domestic,
        'item_amount' => $data['item_amount'],
        'sale_amount' => $domestic_item_sub_total_amount,
        'shiping_fee' => $shipping_price_domestic,
        'discount_total' => $data['discount_amount'],
        'seller' => $data['country'],
        'purchaser' => $purchaser_type,
        'purchaser_country' => $purchaser_country,
        'percent' => $total_percent,
        'coupon_percent' => $coupon_percent_discount,
        'fc_percent' => $purchaser_discount,
        'sa_percent' => $sa_discount,
        'coupon_fix' => $coupon_fix_amount,
        'credit_added' => $credit_added,
        'use_credit' => $use_credit,
        'release_amount' => 0,
        'transaction' => $credit_added,
        'balance' => $balance->balance+$credit_added,
      );
      if($credit_type == 'BUY'){
        $data['transaction'] = -($use_credit);
        $data['balance'] = $balance->balance-$use_credit;
      }
      // echo "<pre>";
      // print_r($data);

      $this->datacontrol_model->insert('sales_history_credit', $data);
    }





    $oversea_total = 0;
    $oversea_item_sub_total_amount = 0;
    $oversea_user_discount_total_amount = 0;
    $oversea_coupon_percent_total_amount = 0;
    $oversea_item_amount = 0;
    $coupon_percent_discount = 0;
    $coupon_percent_amount = 0;
    $coupon_fix_amount = 0;
    if(isset($cart['oversea']) && !empty($cart['oversea'])){
      foreach ($cart['oversea'] as $key => $value) {
        $user_discount_amount = ($purchaser_discount/100)*($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']);

        if($cart['coupon_discount']){
          if($cart['coupon_type'] == 'fixed'){
            $coupon_fix_amount = ($cart['coupon_discount']);
          }
          else{
            $coupon_percent_discount = $cart['coupon_discount'];
            $coupon_percent_amount = (($cart['coupon_discount']/100)*$cart['oversea'][$key]['price']);
          }
        }


        $oversea_item_sub_total_amount += (($cart['oversea'][$key]['price']*$cart['oversea'][$key]['item_amount']) + $cart['oversea'][$key]['import_shipping'] + $cart['oversea'][$key]['import_tax']);
        $oversea_user_discount_total_amount += $user_discount_amount;
        $oversea_coupon_percent_total_amount += $coupon_percent_amount;
        $oversea_item_amount += $cart['oversea'][$key]['item_amount'];

        $data = array(
          // information
          'shipment_id' => $shipment_id_oversea,
          'country' => 'Global',
          'account_type' => 'admin',
          'account_name' => @$cart['oversea'][$key]['sale_account_name'],
          'sale_date' => date("Y-m-d"),
          'sale_item_category' => @$cart['oversea'][$key]['product_category'],
          'buy_type' => 'oversea',
          'sale_item_id' => $key,
          'product_name' => $cart['oversea'][$key]['product_name'],
          'item_amount' => $cart['oversea'][$key]['item_amount'],
          // 'import_shipping' => $domestic_import_shipping,
          // 'import_tax' => $domestic_import_tax,
          'product_type' => $cart['oversea'][$key]['product_type'],
          'item_sub_total_amount' => $cart['oversea'][$key]['price'],
          'delivery_amount' => $shipping_price_oversea,
          // discount
          'discount_ratio' => $purchaser_discount,
          'user_discount_amount' => round($user_discount_amount),
          'discount_type' => '',
          'coupon_discount_type' => (isset($cart['coupon_type']))?'coupon - '.$cart['coupon_type']:'',
          'coupon_code' => $cart['coupon_code'],
          'coupon_fix_amount' => $coupon_fix_amount,
          'coupon_percent_discount' => $coupon_percent_discount,
          'coupon_percent_amount' => $coupon_percent_amount,
          'use_credit' => $use_credit,
          'discount_amount' => round($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit),
          'sale_total_amount' => round(($cart['oversea'][$key]['price'] + $shipping_price_oversea) - ($user_discount_amount + $coupon_fix_amount + $coupon_percent_amount + $use_credit)),
          'purchaser_type' => $purchaser_type,
          'purchaser_country' => $purchaser_country,
          'purchaser_id' => $purchaser_id,
          'shipping_addr' => $shipping_info->address,
          'payment_status' => 'success',
          'payment_method' => $payWith,
          'payment_data' => $payment_gateway_data,
          'payment_date' => date("Y-m-d"),
          'shipping_method' => $shipping_oversea_name,
          'shipping_status' => 'Processing',
          'shipping_date' => '',
          'emergency_status' => '',
          'emergency_date' => '',
          'total_discount' => 0,
        );
        if(in_array($data['payment_method'], array('paypal', 'amazon', 'alibaba'))){
          $data['shipping_status'] = "Paid";
        }
        $oversea_total += $data['sale_total_amount'];
        // echo "<pre>";
        // print_r($data);
        $this->datacontrol_model->insert('sales_history', $data);
      }


      /// sales_history_total
      $data_sale_total = $data;
      $data_sale_total['item_amount'] = $oversea_item;
      $data_sale_total['item_sub_total_amount'] = $oversea_item_sub_total_amount;
      $data_sale_total['user_discount_amount'] = round($oversea_user_discount_total_amount);
      $data_sale_total['coupon_percent_amount'] = round($oversea_coupon_percent_total_amount);
      $data_sale_total['discount_amount'] = round($oversea_user_discount_total_amount + $coupon_fix_amount + $oversea_coupon_percent_total_amount + $use_credit);
      $data_sale_total['sale_total_amount'] = round(($oversea_item_sub_total_amount + $shipping_price_oversea) - ($oversea_user_discount_total_amount + $coupon_fix_amount + $oversea_coupon_percent_total_amount + $use_credit));

      //// commission
      $data_sale_total['sa_name'] = '-';
      $data_sale_total['sa_sale_percent'] = 0;
      $data_sale_total['sa_sale_amount'] = 0;
      $data_sale_total['sa_pay_global'] = 0;
      $data_sale_total['sa_pay_domestic'] = 0;
      $data_sale_total['aa_name'] = '-';
      $data_sale_total['aa_percent'] = 0;
      $data_sale_total['aa_pay'] = 0;

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'SA'){
        $sa = $this->datacontrol_model->getRowData('users', array('email'=>$data_sale_total['account_name']));
        $data_sale_total['sa_name'] = $sa->email;
      }

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_global'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'];
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = round(($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = $sa->discount_setting;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_global'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'];
        }
      }

      if($data_sale_total['country'] == 'Global' && $data_sale_total['purchaser_type'] == 'GU'){

      }

      if($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = 100;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = round(($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = 100;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
        }
      }

      if(($data_sale_total['country'] == 'JapanA' || $data_sale_total['country'] == 'JapanB') && $data_sale_total['purchaser_type'] == 'FC'){
        $fc = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
        $sa = $this->ion_auth->user($fc->create_by)->row();
        $data_sale_total['sa_name'] = $sa->email;
        if($fc->parent_agent > 0){
          $aa = $this->ion_auth->user($fc->parent_agent)->row();
          $sa = $this->ion_auth->user($aa->create_by)->row();

          $data_sale_total['sa_sale_percent'] = 50;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
          $data_sale_total['aa_name'] = $aa->email;
          $data_sale_total['aa_percent'] = $aa->comission_setting;
          $data_sale_total['aa_pay'] = round(($data_sale_total['aa_percent']/100)*$data_sale_total['item_sub_total_amount']);
        }
        else{
          $data_sale_total['sa_sale_percent'] = 50;
          $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
          $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
        }
      }

      if($data_sale_total['account_type'] == 'SA' && $data_sale_total['purchaser_type'] == 'GU'){
        $sa = $this->datacontrol_model->getRowData('users', array('email'=>$data_sale_total['account_name']));
        $data_sale_total['sa_name'] = $sa->email;
        $data_sale_total['sa_sale_percent'] = 100;
        $data_sale_total['sa_sale_amount'] = ($data_sale_total['sa_sale_percent']/100)*$data_sale_total['item_sub_total_amount'];
        $data_sale_total['sa_pay_domestic'] = $data_sale_total['sa_sale_amount'] - $data_sale_total['discount_amount'] + $data_sale_total['delivery_amount'];
      }



      $vat = $this->datacontrol_model->getRowData('nation_lang', array('country'=> $data_sale_total['purchaser_country']));
      // $data_sale_total['vat_percent'] = $vat->vat;
      // $data_sale_total['vat_amount'] = ($vat->vat/100)*$data_sale_total['sale_total_amount'];
      $data_sale_total['vat_percent'] = 0;
      $data_sale_total['vat_amount'] = 0;
      $data_sale_total['admin_fee_percent'] = round($payFee);
      $data_sale_total['admin_fee_amount'] = round(($payFee>10)?$payFee:($payFee/100)*$data_sale_total['sale_total_amount']);
      if($payFee>10){
        $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount']+$data_sale_total['vat_amount']);
      }
      else{
        $data_sale_total['payment_total_amount'] = round($data_sale_total['sale_total_amount']+$data_sale_total['vat_amount']+$data_sale_total['admin_fee_amount']);
      }

      // echo "<pre>";
      // print_r($data_sale_total);
      $data_oversea_sale_total = $data_sale_total;
      $this->datacontrol_model->insert('sales_history_total', $data_sale_total);





      ///// Credit
      if($data['account_type'] == 'admin' && $purchaser_type == 'SA'){
        $credit_type = 'BUY';
      }
      else{
        $credit_type = 'SELL';
      }
      $sa_discount = 0;
      if($data['country'] == 'Global' && $purchaser_type == 'FC'){
        $sa = $this->ion_auth->user($purchaser_id)->row();
        $sa_discount = $sa->discount_setting;
      }
      $percent = array($coupon_percent_discount, $purchaser_discount, $sa_discount);
      arsort($percent);
      $total_percent = 0;
      foreach ($percent as $val) {
        $total_percent = $val - $total_percent;
      }

      $this->db->order_by('id', 'desc');
      $this->db->where('country', $data['purchaser_country'])->or_where('credit_type', 'BUY');
      $balance = $this->datacontrol_model->getRowData('sales_history_credit');
      // echo $balance->balance;
      $credit_added = 0;
      if($data_sale_total['sa_pay_global']!=0){
        $credit_added = $data_sale_total['sa_pay_global'];
      }
      if($data_sale_total['sa_pay_domestic']!=0){
        $credit_added = $data_sale_total['sa_pay_domestic'];
      }
      $data = array(
        'country' => $data['country'],
        'credit_type' => $credit_type,
        'shipment_id' => $shipment_id_oversea,
        'item_amount' => $data['item_amount'],
        'sale_amount' => $oversea_item_sub_total_amount,
        'shiping_fee' => $shipping_price_oversea,
        'discount_total' => $data['discount_amount'],
        'seller' => $data['country'],
        'purchaser' => $purchaser_type,
        'purchaser_country' => $purchaser_country,
        'percent' => $total_percent,
        'coupon_percent' => $coupon_percent_discount,
        'fc_percent' => $purchaser_discount,
        'sa_percent' => $sa_discount,
        'coupon_fix' => $coupon_fix_amount,
        'credit_added' => $credit_added,
        'use_credit' => $use_credit,
        'release_amount' => 0,
        'transaction' => $credit_added,
        'balance' => $balance->balance+$credit_added,
      );
      if($purchaser_type == 'FC' && $data['country'] == 'Global'){
        $data['country'] = $purchaser_country;
      }
      if($credit_type == 'BUY'){
        $data['transaction'] = -($use_credit);
        $data['balance'] = $balance->balance-$use_credit;
      }
      // echo "<pre>";
      // print_r($data);
      $this->datacontrol_model->insert('sales_history_credit', $data);


    }


    //
    $this->session->unset_userdata('shipping_domestic');
    $this->session->unset_userdata('shipping_oversea');
    $this->session->unset_userdata('payWith');
    $this->session->unset_userdata('sensha_cart');
    $this->session->unset_userdata('cart_next_page');
    $this->session->unset_userdata('cart');

    // exit();

    if (strpos($purchaser_email, '@') !== false) {
      $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart);
    }
    else{
      $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart);
    }
    // exit();
    $data['page'] = 'payment_complete_v';
    $this->load->view('front_template', $data);
  }

  public function order_sms($data_domestic_sale_total, $data_oversea_sale_total, $cart){
    $user = $this->ion_auth->user()->row();

    require_once 'vendor/autoload.php';
    $basic  = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
    $client = new \Nexmo\Client($basic);

    $this->db->where('nicename', $user->country);
    // $this->db->where('nicename', 'Thailand');
    $countryCode = $this->datacontrol_model->getRowData('countries');
    $phone = $user->phone;
    // $phone = '0803951444';
    // $phone = '0823251212';
    $firstCharacter = substr($phone, 0, 1);
    if($firstCharacter == 0){
      $phone = substr($phone, 1);
    }

    $text = $user->username."SENSHAサイトでのご注文を承りました。";
    if(!empty($data_domestic_sale_total)){
      $text .= "国内注文「".$data_domestic_sale_total['shipment_id']."：".number_format($data_domestic_sale_total['sale_total_amount'] + $data_domestic_sale_total['admin_fee_amount'] + $data_domestic_sale_total['vat_amount'])." Yen」";
    }
    if(!empty($data_oversea_sale_total)){
      $text .= "海外注文「".$data_oversea_sale_total['shipment_id']."：".number_format($data_oversea_sale_total['sale_total_amount'] + $data_oversea_sale_total['admin_fee_amount'] + $data_oversea_sale_total['vat_amount'])." Yen」";
    }
    // echo strlen($text);
    $message = $client->message()->send([
      'to' => '+'.$countryCode->phonecode.$phone,
      'from' => 'Sensha',
      'text' => $text,
      'type' => 'unicode'
    ]);
    $this->order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart);
  }

  public function order_mail($data_domestic_sale_total, $data_oversea_sale_total, $cart){
    // echo "<pre>";
    // print_r($data_domestic_sale_total);
    // print_r($cart['domestic']);
    $user = $this->ion_auth->user()->row();
    $this->db->where('user_id', $user->id);
    $shipping_info = $this->datacontrol_model->getRowData('shipping_info');
    $send_list = array();
    if (strpos($user->email, '@') !== false) {
      $send_list[$user->email] = $shipping_info->name;
    }
    $send_list['info@enfete.asia'] = 'Sensha';

    // $send_list = array($user->email =>  $shipping_info->name, 'info@enfete.asia' => 'Sensha');

    if($data_oversea_sale_total['payment_method']=='Bank Transfer'||$data_domestic_sale_total['payment_method']=='Bank Transfer'){
      $payment_method=$this->lang->line('page_payment_pay_with_bank_tranfer_forsave', FALSE);
    }elseif($data_oversea_sale_total['payment_method']=='pay on delivery'||$data_domestic_sale_total['payment_method']=='pay on delivery'){
      $payment_method=$this->lang->line('page_payment_pay_on_delivery_forsave', FALSE);
    }elseif(!empty($data_oversea_sale_total['payment_method'])){
      $payment_method=$data_oversea_sale_total['payment_method'];
    }else{
      $payment_method=$data_domestic_sale_total['payment_method'];
    }

    $msg = $user->email."様<br /><br />";
    //$msg .= "洗車の王国　公式ホームページをご利用いただき、誠にありがとうございます。<br />
    //ご注文を承りましたので、内容/配送/決済のご案内をさせていただきます。<br /><br />";
    $msg .= $this->lang->line('mail_payment_to_customer', FALSE).'<br /><br />';

    if(!empty($data_domestic_sale_total)){
      if($data_domestic_sale_total['account_type'] == 'SA' && $data_domestic_sale_total['purchaser_type'] == 'FC'){
        $send_list[$data_domestic_sale_total['account_name']] = $data_domestic_sale_total['account_name'];
      }
      $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE).$data_domestic_sale_total['shipment_id']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE).date('Y-m-d H:i:s')."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE).$payment_method."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE).$data_domestic_sale_total['shipping_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE)."$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->addr $shipping_info->country $shipping_info->phone<br /><br />";
      //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_separate_line', FALSE)."<br />";
      foreach ($cart['domestic'] as $key => $value) {
        $msg .= $cart['domestic'][$key]['product_name']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE).$cart['domestic'][$key]['item_amount']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE).number_format($cart['domestic'][$key]['price']*$cart['domestic'][$key]['item_amount'])." Yen<br /><br />";
      }
      $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE).number_format($data_domestic_sale_total['item_sub_total_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE).number_format($data_domestic_sale_total['delivery_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE).number_format($data_domestic_sale_total['discount_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE).number_format($data_domestic_sale_total['use_credit'])." pt<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE).number_format($data_domestic_sale_total['admin_fee_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE).number_format($data_domestic_sale_total['vat_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'] + $data_domestic_sale_total['admin_fee_amount'] + $data_domestic_sale_total['vat_amount'])." Yen<br /><br />";

    }

    if(!empty($data_oversea_sale_total)){
      $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE).$data_oversea_sale_total['shipment_id']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE).date('Y-m-d H:i:s')."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE).$data_oversea_sale_total['payment_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE).$data_oversea_sale_total['shipping_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE)."$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->addr $shipping_info->country $shipping_info->phone<br /><br />";
      //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= "============================<br />";
      foreach ($cart['oversea'] as $key => $value) {
        $msg .= $cart['oversea'][$key]['product_name']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE).$cart['oversea'][$key]['item_amount']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE).number_format($cart['oversea'][$key]['price']*$cart['oversea'][$key]['item_amount'])." Yen<br /><br />";
      }
      $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE).number_format($data_oversea_sale_total['item_sub_total_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE).number_format($data_oversea_sale_total['delivery_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE)." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE)." pt<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE).number_format($data_oversea_sale_total['admin_fee_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE).number_format($data_oversea_sale_total['vat_amount'])."<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'] + $data_oversea_sale_total['admin_fee_amount'] + $data_oversea_sale_total['vat_amount'])." Yen<br /><br />";
    }

    if(in_array($data_oversea_sale_total['shipping_method'], array('EXW', 'CIF')) || in_array($data_domestic_sale_total['shipping_method'])){
      $msg .= $this->lang->line('mail_payment_to_customer_notice_title', FALSE)."<br />";
      $msg .= "-----------------------------------------------------------<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE).number_format($data_oversea_sale_total['sale_total_amount'] + $data_domestic_sale_total['sale_total_amount'] + $data_domestic_sale_total['admin_fee_amount'] + $data_domestic_sale_total['vat_amount'] + $data_oversea_sale_total['admin_fee_amount'] + $data_oversea_sale_total['vat_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank_account', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank2', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank2_account', FALSE)."<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution2', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution3', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution4', FALSE)."<br /><br />";
      $msg .= "-----------------------------------------------------------<br /><br />";
    }

    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice', FALSE)."<br />";
    $msg .= "-----------------------------------------------------------<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice2', FALSE)."<br />";
    $msg .= "https://sensha-world.com/page/order_history<br /><br />";
    $msg .= "-----------------------------------------------------------<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice3', FALSE)."<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice4', FALSE)."<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice5', FALSE)."<br />";
    $msg .= "-----------------------------------------------------------<br />";
    // print_r($send_list);
    // exit();
    $mail_title = $this->lang->line('mail_payment_to_customer_mail_title', FALSE);
    mail_to($send_list, $mail_title, $msg);
    // mail_to(array('kerapol.b@gmail.com'=>'kerapol'), 'test user', $msg);
    // mail_to(array('kerapol.b@hotmail.com'=>'kerapol'), 'test user2', $msg);



    //send to admin (info@enfete.asia) Hide
    $msg = $user->email."様<br /><br />";
    $msg .= $this->lang->line('mail_payment_to_customer', FALSE).'<br /><br />';

    if(!empty($data_domestic_sale_total)){
      if($data_domestic_sale_total['account_type'] == 'SA' && $data_domestic_sale_total['purchaser_type'] == 'FC'){
        $send_list[$data_domestic_sale_total['account_name']] = $data_domestic_sale_total['account_name'];
      }
      $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE).$data_domestic_sale_total['shipment_id']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE).date('Y-m-d H:i:s')."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE).$data_domestic_sale_total['payment_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE).$data_domestic_sale_total['shipping_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE)."$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->addr $shipping_info->country $shipping_info->phone<br /><br />";
      //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_separate_line', FALSE)."<br />";
      foreach ($cart['domestic'] as $key => $value) {
        $msg .= $cart['domestic'][$key]['product_name']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE).$cart['domestic'][$key]['item_amount']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE).number_format($cart['domestic'][$key]['price']*$cart['domestic'][$key]['item_amount'])." Yen<br /><br />";
      }
      $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE).number_format($data_domestic_sale_total['item_sub_total_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE).number_format($data_domestic_sale_total['delivery_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE).number_format($data_domestic_sale_total['discount_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE).number_format($data_domestic_sale_total['use_credit'])." pt<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE).number_format($data_domestic_sale_total['admin_fee_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE).number_format($data_domestic_sale_total['vat_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE).number_format($data_domestic_sale_total['sale_total_amount'] + $data_domestic_sale_total['admin_fee_amount'] + $data_domestic_sale_total['vat_amount'])." Yen<br /><br />";

    }

    if(!empty($data_oversea_sale_total)){
      $msg .= $this->lang->line('mail_payment_to_customer_content', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipment_id', FALSE).$data_oversea_sale_total['shipment_id']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_order_date', FALSE).date('Y-m-d H:i:s')."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_payment_method', FALSE).$data_oversea_sale_total['payment_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_method', FALSE).$data_oversea_sale_total['shipping_method']."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_shipping_place', FALSE)."$shipping_info->zipcode $shipping_info->name $shipping_info->company $shipping_info->addr $shipping_info->country $shipping_info->phone<br /><br />";
      //$msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= "============================<br />";
      foreach ($cart['oversea'] as $key => $value) {
        $msg .= $cart['oversea'][$key]['product_name']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_amount', FALSE).$cart['oversea'][$key]['item_amount']."<br />";
        $msg .= $this->lang->line('mail_payment_to_customer_item_price', FALSE).number_format($cart['oversea'][$key]['price']*$cart['oversea'][$key]['item_amount'])." Yen<br /><br />";
      }
      $msg .= $this->lang->line('mail_payment_to_customer_item_sub_total_amount', FALSE).number_format($data_oversea_sale_total['item_sub_total_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_delivery_amount', FALSE).number_format($data_oversea_sale_total['delivery_amount'])." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_discount_amount', FALSE)." Yen<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_use_credit', FALSE)." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_subtotal_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_admin_fee_amount', FALSE).number_format($data_oversea_sale_total['admin_fee_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_vat_amount', FALSE).number_format($data_oversea_sale_total['vat_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_item_sale_total_amount', FALSE).number_format($data_oversea_sale_total['sale_total_amount'] + $data_oversea_sale_total['admin_fee_amount'] + $data_oversea_sale_total['vat_amount'])." Yen<br /><br />";
    }

    if(in_array($data_oversea_sale_total['shipping_method'], array('EXW', 'CIF')) || in_array($data_domestic_sale_total['shipping_method'])){
      $msg .= $this->lang->line('mail_payment_to_customer_notice_title', FALSE)."<br />";
      $msg .= "-----------------------------------------------------------<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE).number_format($data_oversea_sale_total['sale_total_amount'] + $data_domestic_sale_total['sale_total_amount'] + $data_domestic_sale_total['admin_fee_amount'] + $data_domestic_sale_total['vat_amount'] + $data_oversea_sale_total['admin_fee_amount'] + $data_oversea_sale_total['vat_amount'])." Yen<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_sale_total_amount_to_transfer', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank_account', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank2', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_bank2_account', FALSE)."<br /><br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution2', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution3', FALSE)."<br />";
      $msg .= $this->lang->line('mail_payment_to_customer_caution4', FALSE)."<br /><br />";
      $msg .= "-----------------------------------------------------------<br /><br />";
    }

    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice', FALSE)."<br />";
    $msg .= "-----------------------------------------------------------<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice2', FALSE)."<br />";
    $msg .= "https://sensha-world.com/page/order_history<br /><br />";
    $msg .= "-----------------------------------------------------------<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice3', FALSE)."<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice4', FALSE)."<br />";
    $msg .= $this->lang->line('mail_payment_to_customer_confirm_notice5', FALSE)."<br />";
    $msg .= "-----------------------------------------------------------<br />";
    // print_r($send_list);
    // exit();
    $mail_title = $this->lang->line('mail_payment_to_customer_mail_title', FALSE);

    mail_to(array('info@enfete.asia' => 'Sensha'), $mail_title, $msg);
    // mail_to(array('kerapol.b@hotmail.com'=>'kerapol'), 'test admin', $msg);
  }

  public function order_history(){
    $user_id = $this->ion_auth->user()->row()->id;
    // $this->db->where('buy_type', 'domestic');

    // $order_history_domestic = $this->datacontrol_model->getAllData('sales_history');
    $uri_segment = 3;

    $this->load->library('pagination');
    $config = array();
    $config["base_url"] = base_url() . "page/order_history";


    $this->db->where('create_by', $user_id);
    $this->db->order_by('id', 'desc');
    $this->db->group_by('shipment_id');
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');

    $config["total_rows"] = count($data["order_history"]);
    $config["per_page"] = 10;
    $config["uri_segment"] = $uri_segment;

    $config['full_tag_open'] = '<ul>';
    $config['full_tag_close'] = '</ul>';

    // $config['first_link'] = 'First';
    // $config['first_tag_open'] = '<li>';
    // $config['first_tag_close'] = '</li>';
    //
    // $config['last_link'] = 'Last';
    // $config['last_tag_open'] = '<li>';
    // $config['last_tag_close'] = '</li>';

    $config['next_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-next.png").'">';
    $config['next_tag_open'] = '<li class="next">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '<img src="'.base_url("assets/sensha-theme/images/arrow-p.png").'">';
    $config['prev_tag_open'] = '<li class="previous">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="active">';
    $config['cur_tag_close'] = '</li>';

    $config['num_tag_open'] = '<li class="number">';
    $config['num_tag_close'] = '</li>';


    $this->pagination->initialize($config);
    $page = ($this->uri->segment($uri_segment)) ? $this->uri->segment($uri_segment) : 0;
    $data["links"] = $this->pagination->create_links();

    $this->db->where('create_by', $user_id);
    $this->db->order_by('id', 'desc');
    $this->db->group_by('shipment_id');
    $this->db->limit($config["per_page"], $page);
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');


    $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('user_id'=>$user_id));

    // print_r($data['shipping_info']);

    // $this->db->where('buy_type', 'oversea');
    // $this->db->where('create_by', $user_id);
    // $order_history_oversea = $this->datacontrol_model->getAllData('sales_history');
    // print_r($sale_history);
    $this->db->where('create_by', $user_id);
    $this->db->order_by('id', 'desc');
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history_total');
    $data['page'] = 'order_history_v';
    $this->load->view('front_template', $data);
  }

  public function claim($shipment_id){
    $user_id = $this->ion_auth->user()->row()->id;

    $this->db->where('shipment_id', $shipment_id);
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history');

    $this->db->where('shipment_id', $shipment_id);
    $data['order'] = $this->datacontrol_model->getRowData('sales_history_total');

    $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('create_by'=>$user_id));
    $data['shipment_id'] = $shipment_id;
    $data['page'] = 'claim_v';
    $this->load->view('front_template', $data);
  }

  public function claim_confirm(){
    $user_id = $this->ion_auth->user()->row()->id;
    $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('create_by'=>$user_id));

    $this->db->where('shipment_id', $this->input->post('shipment_id'));
    $data['item_list'] = $this->datacontrol_model->getAllData('sales_history');

    $this->db->where('shipment_id', $this->input->post('shipment_id'));
    $data['order'] = $this->datacontrol_model->getRowData('sales_history_total');

    $this->session->set_userdata('claim_date', $_POST);
    $data['page'] = 'claim_confirm_v';
    $this->load->view('front_template', $data);
  }

  public function claim_completed(){
    $_POST = $this->session->userdata('claim_date');
    $user_id = $this->ion_auth->user()->row()->id;
    $shipping_info = $this->datacontrol_model->getRowData('shipping_info', array('create_by'=>$user_id));

    $this->db->where('shipment_id', $this->input->post('shipment_id'));
    $item_list = $this->datacontrol_model->getAllData('sales_history');

    $this->db->where('shipment_id', $this->input->post('shipment_id'));
    $order = $this->datacontrol_model->getRowData('sales_history_total');
    // print_r($this->input->post());

    $subject = "ウェブサイトからクレームが届きました ".$this->input->post('shipment_id');
    $msg = "ユーザーから下記のクレームが届きました。<br />内容を確認の上、速やかに対応してください。<br /><br />";
    if($order->aa_name != '-'){
      $subject = 'Got a claim from user '.$this->input->post('shipment_id');
      $msg = "You recieved the claim from SENSHA user.  Please find the below.  And quicly respond to the user. ";
    }
    $msg .= '============================<br />';
    $msg .= 'Claim: '.$this->input->post('shipment_id').'<br /><br />';
    $msg .= 'Shipment ID : '.$this->input->post('shipment_id').'<br />';
    $msg .= 'Date of order : '.$order->sale_date.'<br />';
    $msg .= 'Deliver address : '."$shipping_info->name $shipping_info->address $shipping_info->zipcode $shipping_info->country $shipping_info->phone".'<br />';
    $msg .= 'Ordered items :<br />';
    foreach($item_list as $product_item){
      $msg .= $product_item->product_name."<br />";
      $msg .= "Amount: ".$product_item->item_amount."<br />";
      $msg .= "Subtotal: ".number_format($product_item->item_sub_total_amount, 2)."<br /><br />";
    }
    // $msg .= 'Subtotal : '.$order->item_sub_total_amount.'<br />';
    $msg .= 'Total amount : '.$order->sale_total_amount.'<br />';
    $msg .= 'Deliver type : '.$order->buy_type.'<br />';
    $msg .= 'Deliver method : '.$order->shipping_method.'<br />';
    $msg .= 'Email : '.$this->input->post('email').'<br />';
    $msg .= 'Reason : '.$this->input->post('reason').'<br />';
    $msg .= 'Claim : '.$this->input->post('comment').'<br />';
    $msg .= '============================<br />';

    $send_list = array($this->input->post('email') =>  $shipping_info->name, 'info@enfete.asia' => 'Sensha');
    if($order->account_type == 'SA' && $order->purchaser_type == 'FC'){
      $fc = $this->ion_auth->user($order->purchaser_id)->row();
      $sa = $this->ion_auth->user($fc->create_by)->row();
      $send_list[$sa->email] = '';
    }
    // echo $msg;
    mail_to($send_list, $subject, $msg);
    // mail_to(array('kerapol.b@gmail.com'=>''), $subject, $msg);
    $data['page'] = 'claim_complete_v';
    $this->load->view('front_template', $data);
  }

  public function cart(){
    $data['noindex'] = true;
    $user_country = $this->ion_auth->user()->row()->country;
    // $this->session->unset_userdata('sensha_cart');
    $cart = $this->session->userdata('sensha_cart');
    $data['cart'] = $cart;

    $user_id =  $this->ion_auth->user()->row()->id;

    $this->db->where('nicename', $this->user_lang);
    $zone = $this->datacontrol_model->getRowData('countries');
    $zone = "zone".$zone->zone;

    $cart = $this->session->userdata('sensha_cart');

    $weight = 0;

    if($cart['oversea']){
      foreach ($cart['oversea'] as $key => $value) {
        $weight += $cart['oversea'][$key]['product_weight']*$cart['oversea'][$key]['item_amount'];
      }
    }

    $this->db->where('weight_max >=', $weight);
    $this->db->where('weight_max <=', $weight);
    $ems_price = $this->datacontrol_model->getRowData('ems_fee');
    // echo $this->db->last_query();

    $data['ems'] = $ems_price->$zone;


    $data["delivery_local"] = 0;

    $this->db->where("user_id", $user_id);
    $shiping_info = $this->datacontrol_model->getRowData('shipping_info');
    if($shiping_info){
      $this->db->where('postal_code_min >=', $shiping_info->zipcode);
      $this->db->where('postal_code_max <=', $shiping_info->zipcode);
      $data["japan_delivery_fee"] = $this->datacontrol_model->getRowData('japan_delivery_fee');
    }



    $data['delivery_local'] = 0;


    $this->db->where('country', $this->user_lang);
    $data['delivery_domestic'] = $this->datacontrol_model->getRowData('delivery');
    // echo $this->db->last_query();
    // delete_cookie('recent_viewed_items');
    $c = get_cookie('recent_viewed_items');
    $e = explode(',', $c);

    $i=1;
    $list_product = array();
    $p_id = array();
    foreach($e as $item){
      $s = explode('|', $item);
      if(!in_array($s[1], $p_id)){
        array_push($p_id, $s[1]);
        if($i<=5){
          if($s[0] == 'general'){
            $this->db->select("*, 'general' as product_type");
            $this->db->where('product_id', $s[1]);
            $this->db->where('product_country', $user_country);
            $product = $this->datacontrol_model->getAllData('product_general');
            $list_product = array_merge($list_product, $product);
          }
          if($s[0] == 'ppf'){
            $this->db->select("*, 'ppf' as product_type");
            $this->db->where('product_id', $s[1]);
            $this->db->where('product_country', $user_country);
            $product = $this->datacontrol_model->getAllData('product_ppf');
            $list_product = array_merge($list_product, $product);
          }
        }
        $i++;
      }
    }

    $data["recent_product"] = $list_product;

    // $this->db->where('buy_type', 'domestic');
    $this->db->where('create_by', $user_id);
    $this->db->order_by('id', 'desc');
    // $this->db->group_by('shipment_id');
    // $this->db->limit(5);
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history');

    $data['user_country'] = $user_country;
    $data['coutry_iso'] = $this->coutry_iso."/";

    $data['page'] = 'cart_v';
    $this->load->view('front_template', $data);
  }

  public function place_delivery(){
    $data['noindex'] = true;
    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->where('user_id', $user_id);
    $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');

    $data['countries'] = $this->datacontrol_model->getAllData('countries');

    $data['page'] = 'place_delivery_v';
    $this->load->view('front_template', $data);
  }

  public function invoice($shipment_id){
    $this->db->where('shipment_id', $shipment_id);
    $data['sales_history'] = $this->datacontrol_model->getAllData('sales_history');

    $this->db->where('shipment_id', $shipment_id);
    $data['sales_history_total'] = $this->datacontrol_model->getRowData('sales_history_total');

    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->where('user_id', $user_id);
    $data['shipping_info'] = $this->datacontrol_model->getRowData('invoice_info');
    if(empty($data['shipping_info'])){
      $this->db->where('user_id', $user_id);
      $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info');
    }

    $html = $this->load->view('user/invoice_v', $data, true);
    // echo $html;

    // $this->load->library('pdf');
    // $this->pdf->loadHtml($html, 'UTF-8');
    // // $this->pdf->loadHtml("<h1>宅配便ff</h1>", 'UTF-8');
    // $this->pdf->setPaper('A4', 'portrait');
    // $this->pdf->render();
    // $this->pdf->stream("invoice_$shipment_id.pdf", array("Attachment"=>0));
    // // $this->pdf->stream();
    // // 1= download
    // // 0= preview
    //
    // // exit();


    // Include the main TCPDF library (search for installation path).
    require_once('assets/TCPDF/tcpdf.php');

    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $pdf->AddPage();
    // $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

    //$font = new TCPDF_FONTS();
    //$pdf->Text(0, 0, "alphabetica ABCDEFG" );
    // フォント：IPAゴシック
    //$font_1 = $font->addTTFfont('assets/fonts/ipag.ttf');
    // $font_1 = $font->addTTFfont('assets/fonts/Pro_w3.otf');Firefly
    // $font_1 = $font->addTTFfont('assets/fonts/Firefly.ttf');
    $pdf->SetFont('kozgopromedium' , '', 32,'',true);
    // $pdf->Text(0, 15, "美しい日本語のフォントを表示" );
    // $pdf->Output("cd_cover_template.pdf", "I");
    // exit();

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Nicola Asuni');
    $pdf->SetTitle('TCPDF Example 002');
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set font
    // $pdf->SetFont('times', 'BI', 20);
    //$font = new TCPDF_FONTS();
    //$font_1 = $font->addTTFfont('assets/fonts/ipag.ttf');
    $pdf->SetFont('kozgopromedium', '', 14,'',true);

    // add a page
    $pdf->AddPage();


    // Print text using writeHTMLCell()
    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    // $pdf->Text(0, 15, "美しい日本語のフォントを表示" );

    // ---------------------------------------------------------

    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output("invoice_$shipment_id.pdf", 'D');
     // I: send the file inline to the browser. The PDF viewer is used if available.
     // D: send to the browser and force a file download with the name given by name.
     // F: save to a local file with the name given by name (may include a path).
     // S: return the document as a string.


    //$this->load->view('user/invoice_v', $data);
  }


  public function receipt($shipment_id){
    $this->db->where('shipment_id', $shipment_id);
    $data['sales_history_total'] = $this->datacontrol_model->getRowData('sales_history_total');


    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->where('user_id', $user_id);
    $data['shipping_info'] = $this->datacontrol_model->getRowData('invoice_info');
    if(empty($data['shipping_info'])){
      $this->db->where('user_id', $user_id);
      $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info');
    }

    $this->load->library('pdf');
    $html = $this->load->view('user/receipt_v', $data, true);
    // echo $html;
    $this->pdf->set_paper('A4', 'landscape');
    $this->pdf->loadHtml($html, 'UTF-8');
    $this->pdf->render();
    // $this->pdf->stream();
    $this->pdf->stream("receipt_$shipment_id.pdf");

    // $this->load->view('user/receipt_v', $data);
  }

  public function news(){
    $data['htmlTitle']  = "News";
    $data['news_post'] = $this->datacontrol_model->getAllData('news_post');
    $data['noindex'] = true;
    $data['page'] = 'news_v';
    $this->load->view('front_template', $data);
  }

  public function news_detail($id){
    $this->db->where('id', $id);
    $data['news_post'] = $this->datacontrol_model->getRowData('news_post');
    $data['htmlTitle'] = $data['news_post']->title;
    $data['htmlDes'] = $data['news_post']->content;
    $data['page'] = 'news_detail_v';
    $this->load->view('front_template', $data);
  }

  public function qa(){
    $this->db->limit(5);
    $this->db->where('category', 'SENSHA');
    $data['cat_sensha'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Products');
    $data['cat_products'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Order Payment');
    $data['cat_order'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Delivery');
    $data['cat_delivery'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Return product');
    $data['cat_return'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Discount');
    $data['cat_discount'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Account');
    $data['cat_account'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'The others');
    $data['cat_other'] = $this->datacontrol_model->getAllData('qa_post');

    $data['htmlTitle']  = "Q&A: Support for frequent questions";

    $data['page'] = 'qa_v';
    $this->load->view('front_template', $data);
  }

  public function qa_category($cat){
    $user_country =  $this->ion_auth->user()->row()->country;

    $cat = urldecode($cat);
    $this->db->where('category', $cat);

    if($this->ion_auth->logged_in()){
      $this->db->where('country', $user_country)->or_where('country', 'Global');
    }
    else{
      $this->db->where('country', 'Global');
    }

    $data['htmlTitle']  = $cat;

    $data['cat'] = $cat;
    $data['qa_post'] = $this->datacontrol_model->getAllData('qa_post');
    $data['page'] = 'qa_category_v';
    $this->load->view('front_template', $data);
  }

  public function qa_detail($id){
    $this->db->where('id', $id);
    $data['qa'] = $this->datacontrol_model->getRowData('qa_post');

    $data['htmlTitle']  = $data['qa']->title;

    $this->db->limit(5);
    $this->db->where('category', 'SENSHA');
    $data['cat_sensha'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Products');
    $data['cat_products'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Order Payment');
    $data['cat_order'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Delivery');
    $data['cat_delivery'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Return product');
    $data['cat_return'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Discount');
    $data['cat_discount'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'Account');
    $data['cat_account'] = $this->datacontrol_model->getAllData('qa_post');

    $this->db->limit(5);
    $this->db->where('category', 'The others');
    $data['cat_other'] = $this->datacontrol_model->getAllData('qa_post');

    $data['page'] = 'qa_detail_v';
    $this->load->view('front_template', $data);
  }

  public function check_coupon(){
    $coupon_code = $this->input->post('coupon_code');
    $this->db->where('coupon_code', $coupon_code);
    $discount_coupon = $this->datacontrol_model->getRowData('discount_coupon');
    // print_r($discount_coupon);
    if($discount_coupon){
      $cart = $this->session->userdata('sensha_cart');
      $data = $cart;
      $data['coupon_discount'] = $discount_coupon->discount;
      $data['coupon_type'] = $discount_coupon->discount_type;
      $data['coupon_code'] = $coupon_code;
      // $data['coupon_discount'] = $discount_coupon->discount;
      // $data['coupon_discount'] = $discount_coupon->discount_type;
      $this->session->set_userdata('sensha_cart', $data);
      echo 1;
    }
    else{
      echo 0;
    }
  }

  public function use_point(){
    $point = $this->input->post('point');
    $cart = $this->session->userdata('sensha_cart');
    $data = $cart;
    $data['use_credit'] = $point;
    $this->session->set_userdata('sensha_cart', $data);
    echo 1;
  }

  public function use_all_point(){
    $all_point = $this->input->post('all_point');
    $cart = $this->session->userdata('sensha_cart');
    $data = $cart;
    $data['use_credit'] = $all_point;
    $this->session->set_userdata('sensha_cart', $data);
    echo 1;
  }


  public function change_lang(){
    // set_cookie(array(
    //   'name'   => 'user_language',
    //   'value'  => $this->input->post('language'),
    //   'expire' => 3600*24*12
    // ));
    $this->session->unset_userdata('user_language');
    $this->session->set_userdata('user_language', $this->input->post('language'));

    $this->db->where('nicename', $this->input->post('language'));
    $r = $this->datacontrol_model->getRowData('countries');
    echo strtolower($r->iso);
  }

  public function checkSerial($v){
    echo file_get_contents('https://sensha-world.com/admin/service/checkSerial/'.$v);
  }

  public function test_mail(){
    mail_to(array('kerapol.b@hotmail.com'=> 'kerapol'), 'test user', 'my test');
    mail_to(array('kerapol.b@gmail.com'=> 'kerapol'), 'test admin', 'my test');
  }

  public function test_sms(){
    require_once 'vendor/autoload.php';
    $basic  = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
    $client = new \Nexmo\Client($basic);

    echo strlen('SENSHAサイトでのご注文を承りました。国内注文「O000108D：9,805 Yen」海外注文「：O000108O：9,585 Yen」');

    $message = $client->message()->send([
      'to' => '+66803951444',
      'from' => 'Sensha',
      'text' => 'SENSHAサイトでのご注文を承りました。国内注文「O000108D：9,805 Yen」海外注文「：O000108O：9,585 Yen」',
      'type' => 'unicode'
    ]);
  }

  // public function 404(){
  //   $data['page'] = 'page404/404';
  //   $this->load->view('page/front_template', $data);
  // }

}
