<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    if(!$this->ion_auth->logged_in()){
      // redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    $this->coutry_iso = "";

    if(!$this->ion_auth->logged_in()){
      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page'){
        $this->db->where('iso', strtoupper($this->uri->segment(1)));
        $r = $this->datacontrol_model->getRowData('countries');
        $this->user_lang = $r->nicename;
        $this->coutry_iso = strtolower($r->iso);
      }
    }
    else{
      if($this->session->has_userdata('user_language')){
        $this->user_lang = $this->session->userdata('user_language');
      }

      $this->db->where('nicename', $this->user_lang);
      $r = $this->datacontrol_model->getRowData('countries');
      $this->coutry_iso = strtolower($r->iso);

      if($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso){
        $segs = $this->uri->segment_array();
        $segs[1] = $this->coutry_iso;
        // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
        redirect(base_url($segs), 'refresh');
      }

    }


    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
      $this->coutry_iso = "";
    }

    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function doLogin(){
    $this->session->unset_userdata('shipping_domestic');
    $this->session->unset_userdata('shipping_oversea');
    $this->session->unset_userdata('payWith');
    $this->session->unset_userdata('sensha_cart');
    $this->session->unset_userdata('cart_next_page');
    $this->session->unset_userdata('cart');

    $this->ion_auth->set_message_delimiters('','');
    $this->ion_auth->set_error_delimiters('','');
    $email = $this->input->post('email');
		$pass = $this->input->post('password');
		$remember = (bool)$this->input->post('remember');

		$identity_column = $this->config->item('identity', 'ion_auth');
		$getUser = $this->ion_auth->where($identity_column, $email)->users()->row();

		if($getUser){
			if(!$getUser->active){
				echo json_encode(array('error' => 1));
			}else{
				if($this->ion_auth->login($email, $pass, $remember)){

                    if(!$this->ion_auth->in_group(array('admin', 'SA', 'AA','FC','GU'))){
                        $this->ion_auth->logout();
                        echo json_encode(array('error' => 2, 'msg' => 'You not have authorization.'));
                        exit();
                    }

					unset($getUser->password);
					$this->session->set_userdata('user_data', $getUser);
          $user_country = $this->ion_auth->user()->row()->country;
          $this->session->set_userdata('user_language', $user_country);
					@keep_log("login", $getUser->id);
					echo json_encode(array('error' => 0));
				}else{

         $cookie_name = "login_false";
         $cookie_value =  $_COOKIE['login_false'] + 1;
         setcookie($cookie_name, $cookie_value, time() + (60 * 60), "/"); 

					echo json_encode(array('error' => 2, 'msg' => $this->ion_auth->errors(),'lock' => $cookie_value));
				}
			}
		}
		else{

      $cookie_name = "login_false";
      $cookie_value =  $_COOKIE['login_false'] + 1;
      setcookie($cookie_name, $cookie_value, time() + (60 * 60), "/");

			echo json_encode(array('error' => 3,'lock' => $cookie_value));
		}
  }

  public function dashboard(){
    $data['noindex'] = true;
    // $this->session->unset_userdata('sensha_cart');
    $cart = $this->session->userdata('sensha_cart');
    $data['cart'] = $cart;

    $user_country = 'Global';
    if($this->ion_auth->logged_in()){
      $user_country = $this->ion_auth->user()->row()->country;
    }
    $user_id =  $this->ion_auth->user()->row()->id;

    $c = get_cookie('recent_viewed_items');
    $e = explode(',', $c);
    $e = array_reverse($e);

    $i=1;
    $list_product = array();
    $p_id = array();
    foreach($e as $item){
      $s = explode('|', $item);
      if(!in_array($s[1], $p_id)){
        array_push($p_id, $s[1]);
        if($i<6){
          if($s[0] == 'general'){
            $this->db->select("*, 'general' as product_type");
            $this->db->where('product_id', $s[1]);
            $this->db->where('product_country', $user_country);
            $product = $this->datacontrol_model->getAllData('product_general');
            $list_product = array_merge($list_product, $product);
          }
          if($s[0] == 'ppf'){
            $this->db->select("*, 'ppf' as product_type");
            $this->db->where('product_id', $s[1]);
            $this->db->where('product_country', $user_country);
            $product = $this->datacontrol_model->getAllData('product_ppf');
            $list_product = array_merge($list_product, $product);
          }
        }
        $i++;
      }
    }

    $data["recent_product"] = $list_product;

    $this->db->order_by('id', 'desc');
    $this->db->where('country', $user_country)->or_where('credit_type', 'BUY');
    $data['balance'] = $this->datacontrol_model->getRowData('sales_history_credit');

    // $this->db->where('buy_type', 'domestic');
    // $this->db->select('sale_item_id, product_type, buy_type, item_sub_total_amount');
	$this->db->where('country', $this->user_lang);
    $this->db->where('create_by', $user_id);
    $this->db->order_by('id', 'desc');
    // $this->db->group_by('shipment_id');
    // $this->db->limit(5);
    $data['order_history'] = $this->datacontrol_model->getAllData('sales_history');
    // echo $this->db->last_query();
    // exit();

    $data['user_country'] = $user_country;

    $data['page'] = 'user/dashboard_v';
    $this->load->view('front_template', $data);
  }

  public function personal_info(){
    $data['noindex'] = true;
    if(!$this->ion_auth->logged_in()){
      redirect('', 'refresh');
    }
    $user_id = $this->ion_auth->user()->row()->id;
    $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('user_id'=>$user_id));
    $data['invoice_info'] = $this->datacontrol_model->getRowData('invoice_info', array('user_id'=>$user_id));
    $data['page'] = 'user/personal_info_v';
    $this->load->view('front_template', $data);
  }

  public function edit_personal_info(){
    $data['noindex'] = true;
    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    $data['page'] = 'user/edit_personal_info_v';
    $this->load->view('front_template', $data);
  }

  public function confirm_personal_info(){
    $data['noindex'] = true;
    $this->session->set_userdata('edit_personal_info', $this->input->post());
    $data['page'] = 'user/confirm_personal_info_v';
    $this->load->view('front_template', $data);
  }

  public function complete_personal_info(){
    $data['noindex'] = true;
    $_POST = $this->session->userdata('edit_personal_info');
    $user_id = $this->ion_auth->user()->row()->id;
    $data = array(
      'phone' => $this->input->post('phone', TRUE),
      'country' => $this->input->post('country', TRUE),
      'state' => $this->input->post('state', TRUE),
    );
    if($this->input->post('password', TRUE) != ''){
      $data['password'] = $this->input->post('password', TRUE);
    }

    $this->ion_auth->update($user_id, $data);

    $data['page'] = 'user/complete_personal_info_v';
    $this->load->view('front_template', $data);

    // if ($this->ion_auth->update(11, $data)){
    //   $messages = $this->ion_auth->messages_array();
    //   foreach ($messages as $message){
    //     echo $message;
    //   }
    // }
    // else{
    //   $errors = $this->ion_auth->errors_array();
    //   foreach ($errors as $error){
    //     echo $error;
    //   }
    // }
  }

  public function edit_shipping_info(){
    $data['noindex'] = true;
    if(isset($_GET['redirect'])){
      $this->session->set_userdata('redirect', $_GET['redirect']);
    }
    if(isset($_GET['iso'])){
      $this->session->set_userdata('redirect_iso', $_GET['iso']);
    }
    $user_id = $this->ion_auth->user()->row()->id;
    $data['shipping_info'] = $this->datacontrol_model->getRowData('shipping_info', array('user_id'=>$user_id));
    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    // $user_id = $this->ion_auth->user()->row()->id;
    // $this->db->where('user_id', $user_id);
    // $shipping_information = $this->datacontrol_model->getAllData('shipping_info');
    // $data['shipping_information'] = $shipping_information[0];
    $data['page'] = 'user/edit_shipping_info_v';
    $this->load->view('front_template', $data);
  }

  public function confirm_shipping_info(){
    $data['noindex'] = true;
    $this->session->set_userdata('edit_shipping_info', $this->input->post());
    $data['page'] = 'user/confirm_shipping_info_v';
    $this->load->view('front_template', $data);
  }

  public function complete_shipping_info(){
    $data['noindex'] = true;

    $_POST = $this->session->userdata('edit_shipping_info');
    $user_id = $this->ion_auth->user()->row()->id;
    $data = array(
      'user_id' => $user_id,
      'name' => $this->input->post('name', TRUE),
      'company' => $this->input->post('company', TRUE),
      'zipcode' => $this->input->post('zipcode', TRUE),
      'address' => $this->input->post('address', TRUE),
      'country' => $this->input->post('country', TRUE),
      'state' => $this->input->post('state', TRUE),
      'phone' => $this->input->post('phone', TRUE),
    );

    $this->db->where('user_id', $user_id);
    $shipping_information = $this->datacontrol_model->getAllData('shipping_info');
    if($shipping_information){
      $this->datacontrol_model->update('shipping_info', $data, array('user_id' => $user_id));

      $user_data = array(
        'contact_person' => $this->input->post('name'),
        'company_name' => $this->input->post('company'),
        'company_addr' => $this->input->post('address'),
        'company_phone' => $this->input->post('phone'),
        'zipcode' => $this->input->post('zipcode'),
      );
      $this->ion_auth->update($user_id, $user_data);
    }
    else{
      $this->datacontrol_model->insert('shipping_info', $data);
    }

    if($this->session->has_userdata('redirect')){
      redirect($this->session->userdata('redirect_iso').'/page/'.$this->session->userdata('redirect'), 'refresh');
    }


    $data['page'] = 'user/complete_shipping_info_v';
    $this->load->view('front_template', $data);
  }

  public function ajax_edit_shipping_info(){
    $user_id = $this->ion_auth->user()->row()->id;
    $data = array(
      'user_id' => $user_id,
      'name' => $this->input->post('name', TRUE),
      'company' => $this->input->post('company', TRUE),
      'zipcode' => $this->input->post('zipcode', TRUE),
      'address' => $this->input->post('address', TRUE),
      'country' => $this->input->post('country', TRUE),
      'phone' => $this->input->post('phone', TRUE),
    );

    $this->db->where('user_id', $user_id);
    $shipping_information = $this->datacontrol_model->getAllData('shipping_info');
    if($shipping_information){
      $this->datacontrol_model->update('shipping_info', $data, array('user_id' => $user_id));
    }
    else{
      $this->datacontrol_model->insert('shipping_info', $data);
    }
  }

  public function edit_invoice_info(){
    $data['noindex'] = true;
    $user_id = $this->ion_auth->user()->row()->id;
    $data['invoice_info'] = $this->datacontrol_model->getRowData('invoice_info', array('user_id'=>$user_id));
    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    // $user_id = $this->ion_auth->user()->row()->id;
    // $this->db->where('user_id', $user_id);
    // $invoice_information = $this->datacontrol_model->getAllData('invoice_info');
    // $data['invoice_information'] = $invoice_information[0];
    $data['page'] = 'user/edit_invoice_info_v';
    $this->load->view('front_template', $data);
  }

  public function confirm_invoice_info(){
    $data['noindex'] = true;
    $this->session->set_userdata('edit_invoice_info', $this->input->post());
    $data['page'] = 'user/confirm_invoice_info_v';
    $this->load->view('front_template', $data);
  }

  public function complete_invoice_info(){
    $data['noindex'] = true;
    $_POST = $this->session->userdata('edit_invoice_info');
    $user_id = $this->ion_auth->user()->row()->id;
    $data = array(
      'user_id' => $user_id,
      'name' => $this->input->post('name', TRUE),
      'company' => $this->input->post('company', TRUE),
      'zipcode' => $this->input->post('zipcode', TRUE),
      'address' => $this->input->post('address', TRUE),
      'country' => $this->input->post('country', TRUE),
      'state' => $this->input->post('state', TRUE),
      'phone' => $this->input->post('phone', TRUE),
      'taxid' => $this->input->post('taxid', TRUE),
    );

    $this->db->where('user_id', $user_id);
    $invoice_information = $this->datacontrol_model->getAllData('invoice_info');
    if($invoice_information){
      $this->datacontrol_model->update('invoice_info', $data, array('user_id' => $user_id));
    }
    else{
      $this->datacontrol_model->insert('invoice_info', $data);
    }


    $data['page'] = 'user/complete_invoice_info_v';
    $this->load->view('front_template', $data);
  }


  public function forgotPassword(){
    $data['noindex'] = true;
    $data['page'] = 'user/forgotPassword_v';
    $this->load->view('front_template', $data);
  }

  public function newPassword(){
    $data['noindex'] = true;
    $data['page'] = 'user/new_password_v';
    $this->load->view('front_template', $data);
  }

  public function wishlist(){
    $data['noindex'] = true;
    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->where('create_by', $user_id);
    $wishlist = $this->datacontrol_model->getAllData('wishlist');
    $wishlist_id = array(0);
    $list_product = array();
    foreach($wishlist as $item){
      // array_push($wishlist_id, $item->product_id);
      if($item->product_type == 'general'){
        $this->db->select("*, 'general' as product_type");
        $this->db->where('id', $item->product_id);
        $product = $this->datacontrol_model->getAllData('product_general');
        $list_product = array_merge($list_product, $product);
      }
      if($item->product_type == 'ppf'){
        $this->db->select("*, 'ppf' as product_type");
        $this->db->where('id', $item->product_id);
        $product = $this->datacontrol_model->getAllData('product_ppf');
        $list_product = array_merge($list_product, $product);
      }
    }

    $data['products'] = $list_product;

    // print_r($list_product);
    // exit();

    // $this->db->where_in('id', $wishlist_id);
    // $data['products'] = $this->datacontrol_model->getAllData('product_general');

    $data['page'] = 'user/wishlist_v';
    $this->load->view('front_template', $data);
  }

  public function delete_wishlist($product_id){
    echo $product_id;
    $user_id = $this->ion_auth->user()->row()->id;
    echo $user_id;
    $this->datacontrol_model->delete('wishlist', array('product_id'=>$product_id, 'create_by'=>$user_id));
  }

  public function logout(){
    delete_cookie('user_language');
    // @keep_log("logout", $this->ion_auth->user()->row()->id);
		$this->ion_auth->logout();
		redirect("", 'refresh');
  }

}
