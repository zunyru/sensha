<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('database/datacontrol_model');
        $this->user_lang = 'Global';
        $this->coutry_iso = "";

        if (!$this->ion_auth->logged_in()) {
            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page') {
                $this->db->where('iso', strtoupper($this->uri->segment(1)));
                $r = $this->datacontrol_model->getRowData('countries');
                $this->user_lang = $r->nicename;
                $this->coutry_iso = strtolower($r->iso);
            }
        } else {
            if ($this->session->has_userdata('user_language')) {
                $this->user_lang = $this->session->userdata('user_language');
            }

            $this->db->where('nicename', $this->user_lang);
            $r = $this->datacontrol_model->getRowData('countries');
            $this->coutry_iso = strtolower($r->iso);

            if ($this->uri->segment(1) != '' && $this->uri->segment(1) != 'page' && $this->uri->segment(1) != $this->coutry_iso) {
                $segs = $this->uri->segment_array();
                $segs[1] = $this->coutry_iso;
                // echo $new_uri = str_replace("/".$this->uri->segment(1), $this->coutry_iso."/", uri_string());
                // redirect(base_url($segs), 'refresh');
            }

        }


        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
            $this->coutry_iso = "";
        }

        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $data['page'] = 'home_v';
        $this->load->view('front_template', $data);
    }

    public function add_cart($product_type, $product_id, $type, $amount = 1)
    {
        $this->db->where('id', $product_id);
        if ($product_type == 'ppf') {
            $query = $this->db->get('product_ppf');
        }
        if ($product_type == 'general') {
            $query = $this->db->get('product_general');
        }

        $product = $query->row();
        $sale_account_type = $this->ion_auth->get_users_groups($product->create_by)->row()->name;
        $sale_account_name = $this->ion_auth->user($product->create_by)->row()->email;
        if ($type == 'domestic') {
            $sa_list = $this->ion_auth->users('SA')->result();
            foreach ($sa_list as $item) {
                if ($item->country == $product->product_country) {
                    $sale_account_type = 'SA';
                    $sale_account_name = $item->email;
                }
            }
        }
        if ($type == 'oversea') {
            $sale_account_type = 'Global';
            $sale_account_name = $this->ion_auth->users('admin')->row()->email;
        }
        if (!$this->session->has_userdata('sensha_cart')) {
            $import_shipping = ($type == 'domestic') ? $product->import_shipping : 0;
            $import_tax = ($type == 'domestic') ? $product->import_tax : 0;
            $price = ($type == 'oversea') ? $product->global_price : $product->sa_price;
            $data[$type][$product_id] = array(
                'item_amount' => $amount,
                'id' => $product->id,
                'product_type' => $product_type,
                'product_id' => $product->product_id,
                'product_name' => $product->product_name,
                'product_weight' => $product->product_weight,
                'product_category' => $product->cat_1,
                'product_country' => $product->product_country,
                'sale_account_type' => $sale_account_type,
                'sale_account_name' => $sale_account_name,
                'image' => $product->image,
                'price' => $price,
                'import_shipping' => $import_shipping,
                'import_tax' => $import_tax,
                'total' => (($product->$type * $amount) + $import_shipping + $import_tax),
            );
            $this->session->set_userdata('sensha_cart', $data);
            // echo "<pre>";
            // print_r($data);
        } else {
            $cart = $this->session->userdata('sensha_cart');
            $data = $cart;
            //zun
            if ((!empty($data['domestic'])) && (!empty($data['oversea']))) {
                echo 'not_add_cart';
                exit();
            } else {

                if ((empty($data['domestic'])) && (empty($data['oversea']))) {
                    $import_shipping = ($type == 'domestic') ? $product->import_shipping : 0;
                    $import_tax = ($type == 'domestic') ? $product->import_tax : 0;
                    $price = ($type == 'oversea') ? $product->global_price : $product->sa_price;

                    $data[$type][$product_id] = array(
                        'item_amount' => $amount,
                        'id' => $product->id,
                        'product_type' => $product_type,
                        'product_id' => $product->product_id,
                        'product_name' => $product->product_name,
                        'product_weight' => $product->product_weight,
                        'product_category' => $product->cat_1,
                        'product_country' => $product->product_country,
                        'sale_account_type' => $sale_account_type,
                        'sale_account_name' => $sale_account_name,
                        'image' => $product->image,
                        'price' => $price,
                        'import_shipping' => $import_shipping,
                        'import_tax' => $import_tax,
                        'total' => ((!empty($product->$type) ?? 1 * $amount) + $import_shipping + $import_tax),
                    );
                }

                if (!empty($data['domestic']) && $type == 'domestic') {
                    $add_amount = $cart[$type][$product_id]['item_amount'] + $amount;
                    $import_shipping = ($type == 'domestic') ? $product->import_shipping : 0;
                    $import_tax = ($type == 'domestic') ? $product->import_tax : 0;
                    $price = ($type == 'oversea') ? $product->global_price : $product->sa_price;
                    $data[$type][$product_id] = array(
                        'item_amount' => $add_amount,
                        'id' => $product->id,
                        'product_type' => $product_type,
                        'product_id' => $product->product_id,
                        'product_name' => $product->product_name,
                        'product_weight' => $product->product_weight,
                        'product_category' => $product->cat_1,
                        'product_country' => $product->product_country,
                        'sale_account_type' => $sale_account_type,
                        'sale_account_name' => $sale_account_name,
                        'image' => $product->image,
                        'price' => $price,
                        'import_shipping' => $import_shipping,
                        'import_tax' => $import_tax,
                        'total' => (($product->$type * $add_amount) + $import_shipping + $import_tax),
                    );
                }
                if (!empty($data['oversea']) && $type == 'oversea') {
                    $add_amount = $cart[$type][$product_id]['item_amount'] + $amount;
                    $import_shipping = ($type == 'domestic') ? $product->import_shipping : 0;
                    $import_tax = ($type == 'domestic') ? $product->import_tax : 0;
                    $price = ($type == 'oversea') ? $product->global_price : $product->sa_price;
                    $data[$type][$product_id] = array(
                        'item_amount' => $add_amount,
                        'id' => $product->id,
                        'product_type' => $product_type,
                        'product_id' => $product->product_id,
                        'product_name' => $product->product_name,
                        'product_weight' => $product->product_weight,
                        'product_category' => $product->cat_1,
                        'product_country' => $product->product_country,
                        'sale_account_type' => $sale_account_type,
                        'sale_account_name' => $sale_account_name,
                        'image' => $product->image,
                        'price' => $price,
                        'import_shipping' => $import_shipping,
                        'import_tax' => $import_tax,
                        'total' => ((!empty($product->$type) ?? 1 * $add_amount) + $import_shipping + $import_tax),
                    );
                }

                $this->session->set_userdata('sensha_cart', $data);

                if ((!empty($data['domestic'])) && $type == 'oversea') {
                    echo 'not_add_cart';
                    exit();
                }
                if ((!empty($data['oversea'])) && $type == 'domestic') {
                    echo 'not_add_cart';
                    exit();
                }

                /*
                if (isset($cart[$type][$product_id])) {
                  $add_amount = $cart[$type][$product_id]['item_amount'] + $amount;
                  $import_shipping = ($type == 'domestic')?$product->import_shipping:0;
                  $import_tax = ($type == 'domestic')?$product->import_tax:0;
                  $price = ($type == 'oversea')?$product->global_price:$product->sa_price;
                  $data[$type][$product_id] = array(
                    'item_amount' => $add_amount,
                    'id' => $product->id,
                    'product_type' => $product_type,
                    'product_id' => $product->product_id,
                    'product_name' => $product->product_name,
                    'product_weight' => $product->product_weight,
                    'product_category' => $product->cat_1,
                    'product_country' => $product->product_country,
                    'sale_account_type' => $sale_account_type,
                    'sale_account_name' => $sale_account_name,
                    'image' => $product->image,
                    'price' => $price,
                    'import_shipping' => $import_shipping,
                    'import_tax' => $import_tax,
                    'total' => (($product->$type*$add_amount) + $import_shipping + $import_tax),
                  );
                  $this->session->set_userdata('sensha_cart', $data);
                   //print_r($data);
                }else{
                  $import_shipping = ($type == 'domestic')?$product->import_shipping:0;
                  $import_tax = ($type == 'domestic')?$product->import_tax:0;
                  $price = ($type == 'oversea')?$product->global_price:$product->sa_price;
                  $data[$type][$product_id] = array(
                    'item_amount' => $amount,
                    'id' => $product->id,
                    'product_type' => $product_type,
                    'product_id' => $product->product_id,
                    'product_name' => $product->product_name,
                    'product_weight' => $product->product_weight,
                    'product_category' => $product->cat_1,
                    'product_country' => $product->product_country,
                    'sale_account_type' => $sale_account_type,
                    'sale_account_name' => $sale_account_name,
                    'image' => $product->image,
                    'price' => $price,
                    'import_shipping' => $import_shipping,
                    'import_tax' => $import_tax,
                    'total' => (($product->$type*$amount) + $import_shipping + $import_tax),
                  );

                  $this->session->set_userdata('sensha_cart', $data);
                }
                */
            }
        }
        echo json_encode($this->session->userdata('sensha_cart'));
    }

    public function delete_cart($product_id, $type)
    {
        $cart = $this->session->userdata('sensha_cart');
        $data = $cart;
        if (isset($cart[$type][$product_id])) {
            unset($data[$type][$product_id]);
        }
        $this->session->set_userdata('sensha_cart', $data);
    }

    public function get_product_detail_cart_v()
    {
        $user_id = $this->ion_auth->user()->row()->id;
        $this->db->where('user_id', $user_id);
        $data['shipping_information'] = $this->datacontrol_model->getRowData('shipping_info');

        $this->load->view('page/product/product_detail_cart_v', $data);
    }

    public function add_shipping()
    {
        if ($this->input->post('type') == 'domestic') {
            $this->session->set_userdata('shipping_domestic', $this->input->post());
        }
        if ($this->input->post('type') == 'oversea') {
            $this->session->set_userdata('shipping_oversea', $this->input->post());
        }
    }

    public function add_payWith()
    {
        $this->session->set_userdata('payWith', $this->input->post('name'));
        // $this->session->set_userdata('payFee', $this->input->post('fee'));
    }

    public function add_wishlist()
    {
        // print_r($this->input->post());
        $data = array(
            'product_id' => $this->input->post('id'),
            'product_type' => $this->input->post('product_type'),
            'sku' => $this->input->post('sku'),
        );
        $this->datacontrol_model->insert('wishlist', $data);
    }

    public function get_parts()
    {
        $cat1 = $this->input->post('cat1');
        $cat2 = $this->input->post('cat2');

        $this->db->select('parts');
        $this->db->where('cat_1', $cat1);
        $this->db->where('cat_2', $cat2);
        $this->db->group_by('parts');
        $query = $this->db->get('product_ppf');
        $parts = $query->result();
        echo json_encode($parts);
    }

}
