<div class="clr inner">
    <div id="breadcrumbs" style="margin:15px 0;">
        <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo "Email Send"; ?></span></span>
    </div>
</div>
<div class="clr box_apply">
    <div class="wrap">
        <div class="topic2">
            <p class="title-page"><?php echo "Send to SENSHA"; ?></p>
        </div>
        <div class="inner-apply">
            <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;">
                <?php echo "お問い合わせは SENSHA.CO.,LTD.に送信されました。"; ?></h2>
            <?php echo "近日中にご返答させていただきます。" ?>
        </div>
    </div>
</div>
