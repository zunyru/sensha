<div class="top-slide">
    <section id="video" class="clr">
        <div id="youtube-wrap">
            <div id="youtube" data-property="{
      videoURL: 'R0ZXZz2uzL0',
      containment: '#youtube-wrap',
      autoPlay: true,
      loop: 1,
      mute: true,
      startAt: 0,
	      stopMovieOnBlur: false,
      opacity: 1,
      showControls: false,
      showYTLogo: false,
    onReady: function( player ) {
      $('.loader').hide('slow');
    }
		  }"></div>
            <div class="load-container">
                <div class="loader">Loading...</div>
            </div>
        </div>
        <div class="caption-banner">
            <p class="copy_big"><?php echo htmlspecialchars_decode($page_content->content_a); ?></p>
            <h2><?php echo htmlspecialchars_decode($page_content->content_b); ?></h2>
        </div>
    </section>
    <div class="clr box-s-product">
        <div class="inner">
            <ul class="bxslider">
                <?php foreach ($home_products as $item): ?>
                    <?php $product_images = explode(',', $item->image); ?>
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/product_detail/general/$item->product_id"); ?>">
                            <img src="<?php echo base_url("uploads/product_image/$product_images[0]"); ?>">
                            <div class="product-slider">
                                <h2><?php echo $item->product_name; ?></h2>
                                <p><?php echo $item->caution; ?></p>
                                <?php if ($this->ion_auth->logged_in() && $item->sa_price > 0 && !$this->ion_auth->in_group(array('SA'))): ?>
                                    <div class="clr p-amount">
                                        <span class="b-gray" style="background:#004e9f;color:#fff;">Domestic</span>
                                        <div class="price" style="color:#000;">
                                            <?php echo number_format($item->sa_price); ?> Yen
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="clr p-amount">
                                    <span class="b-gray" style="background:#004e9f;color:#fff;">Oversea</span>
                                    <div class="price" style="color:#000;">
                                        <?php echo number_format($item->global_price); ?> Yen
                                    </div>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php endforeach; ?>
                <!-- <li>
        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/product.png">
        <div class="product-slider">
        <h2>Product Name Product</h2>
        <p>50ml Boxed + accessories</p>
        <div class="clr p-amount">
        <span class="b-gray" style="background:#004e9f;color:#fff;">Domestic</span>
        <div class="price" style="color:#000;">
        1,990 Yen
      </div>
    </div>
    <div class="clr p-amount">
    <span class="b-gray" style="background:#004e9f;color:#fff;">Oversea</span>
    <div class="price" style="color:#000;">
    1,990 Yen
  </div>
</div>
</div>
</li> -->
            </ul>
        </div>
    </div>
</div>
<!-- .top-slide -->
<div class="outline">
    <section class="brand-list">
        <div class="inner">
            <div class="brand-list-in">
                <div class="headline">
                    <p class="title"><?php echo $this->lang->line('page_top_section1_title', FALSE); ?></p>
                    <h1><?php echo $this->lang->line('page_top_section1_subtitle', FALSE); ?></h1>
                </div>
                <ul class="brand-card">
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/about"); ?>"></a>
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/img_brand01.jpg" alt="">
                        <div class="des">
                            <p class="ttl"><?php echo $this->lang->line('page_top_section1_banner1_title', FALSE); ?></p>
                            <p class="txt"><?php echo $this->lang->line('page_top_section1_banner1_desc', FALSE); ?></p>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/about_ppf"); ?>"></a>
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/img_brand02.jpg" alt="">
                        <div class="des">
                            <p class="ttl"><?php echo $this->lang->line('page_top_section1_banner2_title', FALSE); ?></p>
                            <p class="txt"><?php echo $this->lang->line('page_top_section1_banner2_desc', FALSE); ?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- .brand-list -->
    <section class="products-lineup">
        <div class="inner">
            <div class="products-lineup-in">
                <div class="headline">
                    <p class="title"><?php echo $this->lang->line('page_top_section2_title', FALSE); ?></p>
                    <h2><?php echo $this->lang->line('page_top_section2_subtitle', FALSE); ?></h2>
                </div>

                <div class="product-list-wrap">
                    <ul class="products-list">
                        <!--
            <li>
              <a href="<?php echo base_url("$coutry_iso" . "page/general/PPF"); ?>">
                <figure class="hvr-float-shadow">
                  <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products01.png" alt="PPF  (18)">
                </figure>
                <span>PPF  (<?php echo get_count('PPF'); ?>)</span>
              </a>
            </li>
-->
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Body"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products02.png"
                                         alt="Body  (16)" class="hvr-bob">
                                </figure>
                                <span>Body  (<?php echo get_count('Body'); ?>)</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Window"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products03.png"
                                         alt="Window  (10)" class="hvr-bob">
                                </figure>
                                <span>Window  (<?php echo get_count('Window'); ?>)</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Wheel---Tire"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products04.png"
                                         alt="Wheel / Tire  (12)" class="hvr-bob">
                                </figure>
                                <span>Wheel / Tire  (<?php echo get_count('Wheel / Tire'); ?>)</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Mall-Bumper"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products05.png"
                                         alt="Mall/Bumper  (8)" class="hvr-bob">
                                </figure>
                                <span>Molding / Bumper  (<?php echo get_count('Molding / Bumper'); ?>)</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="products-list products-list2">
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Engine-room"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products06.png"
                                         alt="Engine room  (6)" class="hvr-bob">
                                </figure>
                                <span>Engine room  (<?php echo get_count('Engine room'); ?>)</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/interior"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products07.png"
                                         alt="Interior  (10)" class="hvr-bob">
                                </figure>
                                <span>Interior  (<?php echo get_count('Interior'); ?>)</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso" . "page/general/Miscellaneous"); ?>">
                                <figure class="hvr-float-shadow">
                                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/ico-products08.png"
                                         alt="The other  (20)" class="hvr-bob">
                                </figure>
                                <span>Miscellaneous  (<?php echo get_count('Miscellaneous'); ?>)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- .products-lineup -->
    <section class="select-films">
        <div class="inner">
            <div class="select-films-in">
                <div class="headline">
                    <p class="title"><?php echo $this->lang->line('page_top_section3_title', FALSE); ?></p>
                    <h2><?php echo $this->lang->line('page_top_section3_subtitle', FALSE); ?></h2>
                </div>

                <?php
                $country = 'Global';
                if ($this->ion_auth->logged_in()) {
                    $country = $this->ion_auth->user()->row()->country;
                    $exclude_group_id = $this->ion_auth->user()->row()->exclude_group;
                } else {
                    $country = $this->user_lang;
                }

                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('id', explode(',', $result->category));
                }
                $this->db->where('country', $country);
                $this->db->where('cluster_type', 'ppf');
                $query = $this->db->get('cluster1');
                $cluster1 = $query->result();


                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('level1', explode(',', $result->category));
                }
                $this->db->where('country', $country);
                $this->db->where('cluster_type', 'ppf');
                $this->db->order_by('cluster_name', 'asc');
                $query = $this->db->get('cluster2');
                $cluster2 = $query->result();

                ?>

                <div class="select-list-wrap cf">
                    <form id="ppf_form_home" method="post" action="#">
                        <ul class="select-list">
                            <li class="maker">
                                <span class="icon-maker"></span>
                                <select name="cat_1" required>
                                    <option value=""><?php echo $this->lang->line('navi_cut_film_select_maker', FALSE); ?></option>
                                    <?php foreach ($cluster1 as $item): ?>
                                        <option value="<?php echo $item->id; ?>" <?php ($this->input->post('cat_1') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="model">
                                <span class="icon-model"></span>
                                <select name="cat_2">
                                    <option value=""><?php echo $this->lang->line('navi_cut_film_select_model', FALSE); ?></option>
                                    <?php foreach ($cluster2 as $item): ?>
                                        <option data-level1="<?php echo $item->level1; ?>"
                                                value="<?php echo $item->id; ?>"><?php echo $item->cluster_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="part">
                                <span class="icon-part"></span>
                                <select name="parts">
                                    <option value=""><?php echo $this->lang->line('navi_cut_film_select_part', FALSE); ?></option>
                                    <option value="headlight">Headlight</option>
                                    <option value="f-bumper">F-bumper</option>
                                    <option value="r-bumper">R-bumper</option>
                                    <option value="bumper-set">Bumper set</option>
                                    <option value="luggage">Luggage</option>
                                    <option value="doorcup">Doorcup</option>
                                    <option value="aluminium-mold">Aluminium Mold</option>
                                </select>
                            </li>
                        </ul>
                        <div class="serch-btn">
                            <button type="submit" id="sbtn2">
                                <span><?php echo $this->lang->line('navi_cut_film_btn_search', FALSE); ?></span>
                                <span class="icon-search02 ico-glass"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- .select-films -->

    <section class="news-content">
        <div class="inner">
            <div class="news-content-in">
                <div class="headline">
                    <p class="title"><?php echo $this->lang->line('page_top_section4_title', FALSE); ?></p>
                    <h2><?php echo $this->lang->line('page_top_section4_subtitle', FALSE); ?></h2>
                </div>

                <ul class="news-content-list">
                    <?php foreach ($news_post as $item): ?>
                        <li>
                            <time datetime=""><?php echo date('d.m.Y', strtotime($item->create_date)); ?></time>
                            <a href="<?php echo base_url("$coutry_iso" . "page/news_detail/$item->id"); ?>"><?php echo $item->title; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="news-btn">
                    <a href="<?php echo base_url("$coutry_iso" . "page/news"); ?>"
                       class="btn btn-seeall"><?php echo $this->lang->line('page_top_section4_seeall', FALSE); ?></a>
                </div>
            </div>
        </div>
    </section>
    <!-- .news-content -->
    <section class="content-list">
        <div class="inner">
            <div class="content-list-in">
                <ul class="content-card">
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/global_network"); ?>"></a>
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/img_contents01.jpg" alt="">
                        <div class="des">
                            <p class="ttl"><?php echo $this->lang->line('page_top_section5_banner1_title', FALSE); ?></p>
                            <p class="txt"><?php echo $this->lang->line('page_top_section5_banner1_desc', FALSE); ?></p>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/agent"); ?>"></a>
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/img_contents02.jpg" alt="">
                        <div class="des">
                            <p class="ttl"><?php echo $this->lang->line('page_top_section5_banner2_title', FALSE); ?></p>
                            <p class="txt"><?php echo $this->lang->line('page_top_section5_banner2_desc', FALSE); ?></p>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo base_url("$coutry_iso" . "page/qa"); ?>"></a>
                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/top/img_contents03.jpg" alt="">
                        <div class="des">
                            <p class="ttl"><?php echo $this->lang->line('page_top_section5_banner3_title', FALSE); ?></p>
                            <p class="txt"><?php echo $this->lang->line('page_top_section5_banner3_desc', FALSE); ?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- .content-list -->
</div>
<!-- .outline -->

<section class="magazine-content">
    <div class="inner">
        <div class="magazin-content-in cf">
            <div class="headline">
                <span class="icon-mail"></span>
                <p class="title"><?php echo $this->lang->line('mail_mag_title', FALSE); ?></p>
                <h2 class="txt"><?php echo $this->lang->line('mail_mag_subtitle', FALSE); ?></h2>
            </div>
            <div class="magazine-input">
                <!-- Begin Mailchimp Signup Form -->
                <form action="https://sensha-world.us19.list-manage.com/subscribe/post?u=2c9a060b1bad8aee66a259d33&amp;id=0297a383d1"
                      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                      target="_blank" novalidate>
                    <input type="email" value="" name="EMAIL" class="magazine-input_input" id="mce-EMAIL"
                           placeholder="email address" required>
                    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                              name="b_2c9a060b1bad8aee66a259d33_0297a383d1"
                                                                                              tabindex="-1" value="">
                    </div>
                    <div class="clear"><input type="submit"
                                              value="<?php echo $this->lang->line('mail_mag_btn', FALSE); ?>"
                                              name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </form>
                <!--End mc_embed_signup-->
            </div>
        </div>
    </div>
</section>
<!-- .mail-content -->


</div>
<!-- .content -->

<script>
    $(function () {
        $('.bxslider').bxSlider({
            auto: false,
            autoControls: false,
            pager: false,
            slideWidth: 600,
            touchEnabled: false
        });
    })

    //IE
    /*var isIE11 = !!navigator.userAgent.match(/Trident.*rv\:11\./);
    if(isIE11) {



        function ppf_form_home_cat_1(e) {
            document.getElementById('ppf_form_home_cat_1').innerHTML('');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_mobile select[name="cat_2"]').append('<option value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
        }
    }*/

</script>
