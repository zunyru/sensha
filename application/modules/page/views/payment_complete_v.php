<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_complete', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="box-progress">
        <div class="progress-row">
          <div class="progress-step">
            <button type="button" class="btn-circle">1</button>
            <p>Cart item& Delivery</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">2</button>
            <p>Shipping Address</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">3</button>
            <p>Confirmation& Payment</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle step-complete">4</button>
            <p>Complete Order</p>
          </div>
        </div>
      </div>
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_complete_order_confirmation', FALSE); ?></p>
      </div>
      <div class="clr box_complete">
        <div class="left">
            <img src="<?php echo base_url("assets/sensha-theme/");?>images/img-complete.png">
        </div>
        <div class="right">
          <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_complete_your_order', FALSE); ?></h2>
          <?php echo $this->lang->line('page_complete_thank_you', FALSE); ?>
          <div style="text-align:left;margin-top:20px;">
            <a href="<?php echo base_url("page/order_history");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width: 20px;margin-right: 5px;"><?php echo $this->lang->line('page_complete_order_history', FALSE); ?></a>
          </div>
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->
