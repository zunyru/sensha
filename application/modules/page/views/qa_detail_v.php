<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs"style="margin:15px 0;">
      <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><a href="<?php echo base_url("$coutry_iso" . "page/qa");?>"><?php echo $this->lang->line('breadcrumb_qa', FALSE); ?></a></span><span><?php echo $qa->title;?></span>
    </div>
  </div>
  <div class="clr page-qa">
    <div class="clr inner">
      <div class="topic2">
        <p class="title-page">Question aout SENSHA</p>
      </div>
      <div class="clr inner-qa">
        <aside class="col-left">
          <div class="box_menu">
            <div class="a_menu">
              <div class="topic">
                <p>Q&A category</p>
              </div>
              <ul>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/SENSHA");?>">SENSHA</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Products");?>">Products</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Order Payment");?>">Order / Payment</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Delivery");?>">Delivery</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Return product");?>">Return product</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Discount");?>">Discount</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Account");?>">Account</a></li>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/The others");?>">The others</a></li>
              </ul>
            </div>
          </div>
        </aside>
        <div class="content">
          <div class="answer-list">
            <div class="topic-qa">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-qa.png"></figure>
              <h3><?php echo $qa->title;?></h3>
            </div>
            <div class="inner-answer">
              <?php echo htmlspecialchars_decode($qa->content);?>
            </div>
          </div>
        </div>
      </div>
      <div class="clr box-frequent">
        <div class="topic">
          <p class="title-page">Frequent question</p>
        </div>
        <div class="clr inner-frequent">
          <div class="col-4">
            <h3>SENSHA</h3>
            <ul>
              <?php foreach($cat_sensha as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Products</h3>
            <ul>
              <?php foreach($cat_products as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Order / Payment</h3>
            <ul>
              <?php foreach($cat_order as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Delivery</h3>
            <ul>
              <?php foreach($cat_delivery as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Return</h3>
            <ul>
              <?php foreach($cat_return as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Discount</h3>
            <ul>
              <?php foreach($cat_discount as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>Account</h3>
            <ul>
              <?php foreach($cat_account as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3>The others</h3>
            <ul>
              <?php foreach($cat_other as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>
