<div class="clr inner">
<div id="breadcrumbs" style="margin:15px 0;">
  <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo $this->lang->line('breadcrumb_about_sensha', FALSE); ?></span></span>
</div>
</div>
  <div class="top-banner b-about">
    <div class="caption-b">
  <p class="head-banner"><?php echo htmlspecialchars_decode($page_content->content_a);?></p>
  <h2><?php echo htmlspecialchars_decode($page_content->content_b);?></h2>
  <p><?php echo htmlspecialchars_decode($page_content->content_c);?></p>
</div>
  </div>
<div class="box_section2">
  <div class="clr wrap">
      <h2><?php echo htmlspecialchars_decode($page_content->content_d);?></h2>
    <p><?php echo htmlspecialchars_decode($page_content->content_e);?></p>
    <ul class="clr">
      <li>
        <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/a1.jpg"></figure>
      </li>
      <li>
        <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/a2.jpg"></figure>
      </li>
      <li>
        <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/a3.jpg"></figure>
      </li>
      <li>
        <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/a4.jpg"></figure>
      </li>
    </ul>
    <div class="car">
        <img src="<?php echo base_url("assets/sensha-theme/");?>images/car.png">
    </div>
  </div>
</div>
  <div class="box_section3">
  <div class="clr wrap">
    <div class="topic">
    <p class="title-page"><?php echo $this->lang->line('page_about_section2_title', FALSE); ?></p>
    </div>
    <h2><?php echo $this->lang->line('page_about_section2_img1_title', FALSE); ?></h2>
      <div class="clr b-service">
      <div class="left">
        <figure class="img-about">
	      <div class="img-about-title1"><?php echo $this->lang->line('page_about_section2_img1_title', FALSE); ?></div>
		  <div class="img-about-desc1">
			      <?php echo $this->lang->line('page_about_section2_img1_txt', FALSE); ?>
		  </div>
	      <div class="img-about-title2"><?php echo $this->lang->line('page_about_section2_img2_title', FALSE); ?></div>
		  <div class="img-about-desc2">
			      <?php echo $this->lang->line('page_about_section2_img2_txt', FALSE); ?>
		  </div>
	      <div class="img-about-title3"><?php echo $this->lang->line('page_about_section2_img3_title', FALSE); ?></div>
		  <div class="img-about-desc3">
				  <?php echo $this->lang->line('page_about_section2_img3_txt', FALSE); ?>
		  </div>
          <img src="<?php echo base_url("assets/sensha-theme/");?>images/about01.png">
        	</figure>
        </div>
      <div class="right">
        <?php echo $this->lang->line('page_about_section2_content', FALSE); ?>
      </div>
    </div>
  </div>
  </div>
<div class="clr box-product ">
  <div class="left">
    <div class="topic">
    <p class="title-page"><?php echo $this->lang->line('page_about_section3_title', FALSE); ?></p>
    </div>
    <p class="title"><?php echo $this->lang->line('page_about_section3_topic1_title', FALSE); ?></p>
    <p><?php echo $this->lang->line('page_about_section3_topic1_content', FALSE); ?></p>
    <p class="title"><?php echo $this->lang->line('page_about_section3_topic2_title', FALSE); ?></p>
    <p><?php echo $this->lang->line('page_about_section3_topic2_content', FALSE); ?></p>
  </div>
    <div class="right">
    <div class="product-category">
      <ul>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-1.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/Body");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat1_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat1_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-2.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/Window");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat2_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat2_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-3.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/Wheel---Tire");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat3_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat3_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-4.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/Mall-Bumper");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat4_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat4_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-5.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/Engine-room");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat5_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat5_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
        <li>
          <figure>
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/c-p-6.jpg">
          </figure>
          <a href="<?php echo base_url("$coutry_iso"."page/general/interior");?>">
          <figcaption>
             <h3><?php echo $this->lang->line('page_about_section3_cat6_title', FALSE); ?></h3>
             <p><?php echo $this->lang->line('page_about_section3_cat6_desc', FALSE); ?></p>
          </figcaption>
        </a>
        </li>
      </ul>
    </div>
  </div>
</div>
  <div class="clr box-contact">
  <div class="clr wrap">
    <div class="left">
      <h2><?php echo $this->lang->line('page_about_section4_title', FALSE); ?></h2>
      <p><?php echo $this->lang->line('page_about_section4_desc', FALSE); ?></p>
    </div>
    <div class="right">
    <div class="topic">
      <p class="title-page"><?php echo $this->lang->line('page_about_section4_contact', FALSE); ?></p>
      </div>
      <ul>
        <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i1.png"></figure>
        <div class="detail">
          <p class="title"><?php echo $this->lang->line('page_about_section4_head_office', FALSE); ?></p>
          <p>SENSHA CO., LTD</p>
        </div>
        </li>
      <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i2.png"></figure>
        <div class="detail">
          <p class="title"><?php echo $this->lang->line('page_about_section4_office_address', FALSE); ?></p>
          <p><?php echo $this->lang->line('page_about_section4_office_address_content', FALSE); ?></p>
        </div>
        </li>
      <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i3.png"></figure>
        <div class="detail">
          <p class="title"><?php echo $this->lang->line('page_about_section4_phone', FALSE); ?></p>
          <p><?php echo $this->lang->line('page_about_section4_phone_content', FALSE); ?></p>
        </div>
        </li>
      </ul>
        <div class="news-btn" style="text-align: center;">
          <a style="display: inline-block;margin: 40px auto 10px;" href="<?php echo base_url("$coutry_iso"."page/contact");?>" class="btn btn-seeall"><?php echo $this->lang->line('page_about_section4_link_to_contact', FALSE); ?></a>
        </div>
    </div>
  </div>
</div>