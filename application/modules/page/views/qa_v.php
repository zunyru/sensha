<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs" style="margin:15px 0;">
      <span><a href="<?php echo base_url();?>">HOME</a><span><?php echo $this->lang->line('breadcrumb_qa', FALSE); ?></span></span>
    </div>
  </div>
  <div class="page-qa">
    <div class="inner">
      <div class="topic2">
        <p class="title-page"><?php echo $this->lang->line('page_qa_title', FALSE); ?></p>
      </div>
      <p class="title-detail"><?php echo $this->lang->line('page_qa_desc', FALSE); ?></p>
      <div class="clr box_category_qa">
        <ul>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/SENSHA");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q1.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_sensha', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa1', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Products");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q2.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa2', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa3', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Order Payment");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q3.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa4', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa5', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Delivery");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q4.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa6', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa7', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Return product");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q5.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa8', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa9', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Discount");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q6.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa10', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa11', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Account");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q7.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa12', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa13', FALSE); ?></p>
              </div>
            </a>
          </li>
          <li>
            <a href="<?php echo base_url("$coutry_iso" . "page/qa_category/The others");?>">
              <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-q8.jpg"></figure>
              <div class="detail">
                <p class="name-category"><?php echo $this->lang->line('page_qa_qa14', FALSE); ?></p>
                <p><?php echo $this->lang->line('page_qa_qa15', FALSE); ?></p>
              </div>
            </a>
          </li>
        </ul>
      </div>
      <div class="clr box-frequent">
        <div class="topic">
          <p class="title-page"><?php echo $this->lang->line('page_qa_qa16', FALSE); ?></p>
        </div>
        <div class="clr inner-frequent">
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_sensha', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_sensha as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa2', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_products as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa4', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_order as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa6', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_delivery as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa8', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_return as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa10', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_discount as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa12', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_account as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
          <div class="col-4">
            <h3><?php echo $this->lang->line('page_qa_qa14', FALSE); ?></h3>
            <ul>
              <?php foreach($cat_other as $item):?>
                <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
              <?php endforeach;?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
