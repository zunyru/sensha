<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_privacy', FALSE); ?></span>
  </div>
</div>
<div class="inner privacy">
      <div class="clr single-page">
  <div class="topic">
    <p class="title-page"><?php echo $this->lang->line('page_privacy_title', FALSE); ?></p>
  </div>    
   <?php echo $this->lang->line('page_privacy_subtitle', FALSE); ?>
   <?php echo $this->lang->line('page_privacy_desc', FALSE); ?>
   </div>
</div>
<script src="js/main.js"></script>
<?php include('include/footer.php')?>
</div>
