<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_contact_confirm', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('breadcrumb_contact_confirm', FALSE); ?></p>
				</div>
				<div class="box-inner">

					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_contact_name', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('name');?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_contact_email', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('email');?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_contact_message', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('msg');?></p>
						</div>
					</div>
					<div class="row-btn">
						<a href="<?php echo base_url("page/contact_complete");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_contact_submit', FALSE); ?></a>
					</div>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->
</div><!--container-->
