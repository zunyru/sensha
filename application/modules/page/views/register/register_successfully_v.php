<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_register_success', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="box-content">
			<div class="layout-contain">
				<div class="clr box_form">
				<?php if($this->input->post('register_by') == 'email'):?>
					<div class="topic">
						<p class="title-page"><?php echo $this->lang->line('page_register_success_title', FALSE); ?></p>
					</div>
					<div class="clr box-success">
						<div class="left">
							<img src="<?php echo base_url("assets/sensha-theme/");?>images/img-mail.png">
						</div>
						<div class="right">
							<h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_register_success_message1', FALSE); ?></h2>
							<p><?php echo $this->lang->line('page_register_success_message2', FALSE); ?></p>
						</div>
					</div>
				<?php else:?>
					<div class="topic">
						<p class="title-page"><?php echo $this->lang->line('page_register_complete_title', FALSE); ?></p>
					</div>
					<div class="clr box-success">
						<div class="left">
							<img src="<?php echo base_url("assets/sensha-theme/");?>images/img-complete-r.png">
						</div>
						<div class="right">
							<h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_register_complete_message1', FALSE); ?></h2>
							<p><?php echo $this->lang->line('page_register_complete_message2', FALSE); ?></p>
<!--
								<div class="row-button">
									<a href="<?php echo base_url("page/user/personal_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" style="width: 20px;margin-right: 5px;"><?php echo $this->lang->line('page_register_complete_message3', FALSE); ?></a>
								</div>
-->
						</div>
					</div>
				<?php endif;?>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
