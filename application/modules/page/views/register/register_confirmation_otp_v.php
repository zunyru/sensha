<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url(); ?>">Home</a><span><?php echo $this->lang->line('breadcrumb_register_otp', FALSE); ?></span>
		</div>
	</div>
	<!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_register_otp_title', FALSE); ?></p>
				</div>
				<div class="box-paragraph">
					<p><?php echo $this->lang->line('page_register_otp_message1', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/08.png"><?php echo $this->lang->line('page_register_otp_message2', FALSE); ?></label>
						<div class="r-input">
							<input type="text" placeholder="<?php echo $this->lang->line('page_register_otp_message3', FALSE); ?>" class="form-control" name="otp" required>
						</div>
					</div>

					<div class="row-btn">
						<a href="<?php echo base_url("$coutry_iso" . "page/register/register_otp"); ?>" class="c-gray"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/mail.png" style="width:18px;margin-right:8px;"><?php echo $this->lang->line('page_register_otp_send_again', FALSE); ?></a>
						<a href="javascript:void(0);" class="b-blue conf"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_register_otp_confirm', FALSE); ?></a>
					</div>
				</div>
			</div>
		</div>
		<!--layout-contain-->
	</div>
	<!--inner-->
</div>
<!--container-->


<script>
	$(function() {
		$('.conf').click(function() {
			var otp = $('input[name="otp"]').val();
			$.post("<?php echo base_url("page/register/check_otp"); ?>", {
					otp: otp
				})
				.done(function(data) {
					var r = JSON.parse(data);
					if (r.err == 0) {
						window.location = '<?php echo base_url("$coutry_iso" . "page/register/register_successfully"); ?>'
					}
					else{
						alert("OTP is not match.");
					}
					// 
				});

		});
	})
</script>