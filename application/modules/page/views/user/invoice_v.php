<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>COMMERCIAL Invoice</title>
</head>
<style type="text/css">
	body {
		padding: 0px;
		font-size:14px;
		margin: 0;
		width: 100%;
		color: #000;
		letter-spacing: 1.2rem;
	}
	.container{width:100%; margin:10px auto; max-width:900px;}
	.row{margin-bottom:10px;}
	p{padding: 0px;margin:0px 0 10px 0;}
	.txt-red{color:#FF0000}
	table{border:1}
	table {
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  padding:5px;
	}
	.table td{border: 1px solid #000;}
	ul{margin:0;padding: 0px;}
	li{list-style: none;margin-bottom: 10px;line-height: 20px;}
	.list{}
	.list ul li{margin-bottom: 10px;list-style: decimal;}
</style>
<body>
	<div class="container" style="overflow:hidden">
		<table>
			<tbody>
				<tr>
				  <td colspan="2"><img src="<?php echo base_url('assets/sensha-theme/images/logo.png');?>" width="220" height="73" alt=""/></td>
				  <td colspan="2" style="font-size: 40px; font-weight: bold;color:#aaaaaa;"><?php echo $this->lang->line('pdf_title', FALSE); ?></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="4" style="font-style: italic;"><strong><?php echo $this->lang->line('pdf_description', FALSE); ?></strong> </td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2"><?php echo $this->lang->line('pdf_invoice_no', FALSE); ?><?php echo date('Ymd', strtotime($sales_history[0]->sale_date));?> - <?php echo $sales_history[0]->create_by;?></td>
					<td colspan="2" align="right"><strong><?php echo $this->lang->line('pdf_date', FALSE); ?> <?php echo $sales_history[0]->sale_date;?></strong></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_company_name', FALSE); ?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_company_address', FALSE); ?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_company_tel', FALSE); ?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_company_mail', FALSE); ?></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_shipment_terms', FALSE); ?><?php echo $sales_history_total->shipping_method;?></td>
					<td></td>
				</tr>
				<?php if($coutry_iso=='jp'){ ?>
				<tr>
					<td colspan="3"><?php echo $shipping_info->company;?>　<?php echo $shipping_info->name;?><?php echo $this->lang->line('pdf_to_jp', FALSE); ?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">〒<?php echo $shipping_info->zipcode;?> <?php echo $shipping_info->address;?></td>
					<td></td>
				</tr>
				<?php }else{ ?>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_to', FALSE); ?><?php echo $shipping_info->name;?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_customer_name', FALSE); ?><?php echo $shipping_info->company;?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_customer_address', FALSE); ?><?php echo $shipping_info->address;?></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3"><?php echo $this->lang->line('pdf_customer_zip', FALSE); ?><?php echo $shipping_info->zipcode;?></td>
					<td></td>
				</tr>
				<?php } ?>
				<tr>
				  <td colspan="3"><?php echo $this->lang->line('pdf_customer_tel', FALSE); ?><?php echo $shipping_info->phone;?></td>
				  <td></td>
			  </tr>
			  <tr>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td></td>
			  </tr>
			</tbody>
		</table>
		<table style="border: 1px solid #000;" class="table">
			<tbody>
				<tr style="background-color:#cccccc;font-weight:bold; border:1px solid #000;">
					  <td align="center"><?php echo $this->lang->line('pdf_products_ordered', FALSE); ?> (<?php echo $sales_history_total->shipment_id;?>)</td>
					  <td align="center"><?php echo $this->lang->line('pdf_qty', FALSE); ?></td>
					  <td align="center"><?php echo $this->lang->line('pdf_unit_price', FALSE); ?></td>
					  <td align="center"><?php echo $this->lang->line('pdf_amount', FALSE); ?></td>
				  </tr>
				<?php foreach($sales_history as $item):?>
					<?php
					$shipment_fee = $item->delivery_amount;
					?>
					<tr style="border: 1px solid #000;">
					  <td ><?php echo $item->product_name;?></td>
					  <td style="text-align: center;"><?php echo $item->item_amount;?></td>
					  <td style="text-align: right;"><?php echo number_format($item->item_sub_total_amount);?></td>
					  <td style="text-align: right;"><?php echo number_format($item->item_sub_total_amount * $item->item_amount);?></td>
				  </tr>
				<?php endforeach;?>
				<tr>
					<td><?php echo $this->lang->line('pdf_shipping_fee', FALSE); ?></td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($shipment_fee);?></td>
				</tr>
				<tr>
					<td><?php echo $this->lang->line('pdf_discount', FALSE); ?></td>
					<td></td>
					<td></td>
					<td style="text-align: right;">-<?php echo number_format($sales_history_total->discount_amount);?></td>
				</tr>
				<tr style="background-color:#ccc;">
					<td><?php echo $this->lang->line('pdf_sub_total', FALSE); ?></td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->sale_total_amount);?></td>
				</tr>
				<tr>
					<td><?php echo $this->lang->line('pdf_admin_fee', FALSE); ?></td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->admin_fee_amount);?></td>
				</tr>
				<tr>
					<td><?php echo $this->lang->line('pdf_tax', FALSE); ?> <?php echo number_format($sales_history_total->vat_percent);?>% </td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->vat_amount);?></td>
				</tr>
				<tr style="background-color:#222;font-weight:bold;">
					<td  style="border-right:1px solid #fff; color: #ffffff;"><?php echo $this->lang->line('pdf_grand_total', FALSE); ?></td>
					<td style="border-right:1px solid #fff; color: #fff;"></td>
					<td style="border-right:1px solid #fff; color: #fff;"></td>
					<td style="color: #fff; text-align: right;"><?php echo number_format($sales_history_total->sale_total_amount + $sales_history_total->vat_amount + $sales_history_total->admin_fee_amount);?></td>
				</tr>
		</tbody>
		</table>
		<table style="margin-top:20px;">
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><strong><?php echo $this->lang->line('pdf_payment_options', FALSE); ?></strong></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<?php
					if($sales_history_total->payment_method=='pay on delivery'){
						$payment_method = $this->lang->line('pdf_sensha_pay_on_delivery', FALSE);
				?>
				<tr>
					<td><?php echo $payment_method;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>				
				<?php
					}else{
						$payment_method = $sales_history_total->payment_method;
				?>
				<tr>
					<td><?php echo $payment_method;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<!--tr>
					<td>※ JPY 3000 additional remittance fee is charged to payer.</td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><strong>$ 30.00</strong></td>
				</tr-->
			<?php if($sales_history[0]->buy_type=='domestic' && ($sales_history[0]->purchaser_country=='Japan'||$sales_history[0]->purchaser_country=='JapanA'||$sales_history[0]->purchaser_country=='JapanB')){ ?>
				<tr>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_bank', FALSE); ?></td>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_account', FALSE); ?></td>
				</tr>
				<tr>
					<?php if($coutry_iso!='jp'){ ?>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_swift', FALSE); ?></td>
					<?php }// end if($coutry_iso!='jp') ?>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_account_holder', FALSE); ?></td>

				</tr>
			<?php }elseif($sales_history[0]->buy_type=='oversea'){ ?>
				<tr>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_bank_oversea', FALSE); ?></td>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_account_oversea', FALSE); ?></td>
				</tr>
				<tr>
					<?php if($coutry_iso!='jp'){ ?>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_swift_oversea', FALSE); ?></td>
					<?php }// end if($coutry_iso!='jp') ?>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_account_holder_oversea', FALSE); ?></td>

				</tr>				
				<?php if($coutry_iso!='jp'){ ?>
				<tr>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_branch', FALSE); ?></td>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_address', FALSE); ?></td>
				</tr>
				<tr>
					<td colspan="2"><?php echo $this->lang->line('pdf_sensha_tel', FALSE); ?><?php //echo $this->lang->line('pdf_sensha_branch_bank', FALSE); ?></td>
					<td colspan="2"><?php //echo $this->lang->line('pdf_sensha_branch_tel', FALSE); ?></td>
				</tr>
				<tr>
					<td colspan="2"><?php //echo $this->lang->line('pdf_sensha_branch_bank_code', FALSE); ?></td>
					<td></td>
					<td></td>
				</tr>
				<?php }//end if($coutry_iso!='jp') ?>
			<?php }//end if($sales_history[0]->buy_type=='domestic' ?>
				<?php }//end if($sales_history_total->payment_method=='pay on delivery')  ?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="4" align="center" style="font-size:16px;"><strong><?php echo $this->lang->line('pdf_thanks', FALSE); ?></strong></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- <script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
	<script>
		$(function(){
			var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

		doc.fromHTML($('html').html(), 15, 15, {
				'width': 170,
						'elementHandlers': specialElementHandlers
		});
		// doc.addImage(blob, 'PNG', 0, 0, $('.container').width(), $('.container').height());
		doc.save('sample-file.pdf');

		})
	</script> -->
</body>
</html>
