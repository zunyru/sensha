<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a><span><?php echo $this->lang->line('breadcrumb_edit_account', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<form method="post" action="confirm_personal_info">
		<div class="clr inner">
			<div class="layout-contain">
				<div class="clr box_form">
					<div class="topic">
						<p class="title-page"><?php echo $this->lang->line('page_account_info_edit_account_info', FALSE); ?></p>
					</div>
					<div class="box-inner">
						<div class="r-inline" style="display: none;">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_account_info_id', FALSE); ?></label>
							<div class="r-input">
								<p></p>
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_account_info_email', FALSE); ?></label>
							<div class="r-input">
								<!-- <input type="text" placeholder="Please input your name" class="form-control"> -->
								<p><?php echo $this->ion_auth->user()->row()->email;?></p>
							</div>
						</div>
						
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_account_info_password', FALSE); ?></label>
							<div class="r-input">
								<input type="password" placeholder="" class="form-control" name="password">
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_account_info_country', FALSE); ?></label>
							<div class="r-input">
								<select class="form-control" name="country" required>
									<?php foreach($countries as $item):?>
										<option value="<?php echo $item->nicename;?>" <?php echo ($this->ion_auth->user()->row()->country == $item->nicename)?"selected":"disabled";?>><?php echo ucwords(str_replace("_"," ", $item->nicename))?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="row-btn">
							<button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-save.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_edit_account_update', FALSE); ?></a>
						</div>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</form>

</div><!--container-->

<script>
// $('select[name="country"]').change(function(){
// 	if($(this).val() == 'Japan'){
// 		$('.japan-only').show();
// 	}
// 	else{
// 		$('.japan-only').hide();
// 	}
// });
</script>
