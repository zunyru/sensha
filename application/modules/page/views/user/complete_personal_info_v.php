<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_complete_change_account', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="box-content">
			<div class="layout-contain">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_complete_change_account_confirmed', FALSE); ?></p>
				</div>
				<div class="clr box_complete">
					<div class="left">
						<img src="<?php echo base_url("assets/sensha-theme/");?>images/img-personal-info.png">
					</div>
					<div class="right">
						<h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"> <?php echo $this->lang->line('page_complete_change_account_updated', FALSE); ?> </h2>
						<p><?php echo $this->lang->line('page_complete_change_account_your_account_informaion', FALSE); ?></p>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--box-content-->
	</div><!--inner-->
</div><!--container-->
