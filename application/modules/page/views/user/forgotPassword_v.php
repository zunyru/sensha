<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_forgot_pass', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<form method="post" action="<?php echo base_url("page/user/newPassword");?>">
		<div class="clr inner">
			<div class="layout-contain">
				<div class="clr box_form">
					<div class="topic">
						<p class="title-page"><?php echo $this->lang->line('page_forgot_pass_title', FALSE); ?></p>
					</div>
					<div class="box-paragraph">
						<p><?php echo $this->lang->line('page_forgot_pass_message1', FALSE); ?></p>
					</div>
					<div class="box-inner">
						<div class="clr inline-radio">
							<div class="left">
								<label class="">
									<input type="radio" id="01" name="reset_by" checked> <?php echo $this->lang->line('page_register_email', FALSE); ?>
								</label>
							</div>
							<div class="left">
								<label class="">
									<input type="radio" id="01" name="reset_by" disabled> <?php echo $this->lang->line('page_register_sms', FALSE); ?>
								</label>
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png">Email</label>
							<div class="r-input">
								<input type="text" placeholder="Required if choose email" class="form-control" name="email" required>
							</div>
						</div>
						<!-- <div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png">Telephone</label>
							<div class="r-input">
								<input type="text" placeholder="Required if choose SMS" class="form-control" name="phone" required>
							</div>
						</div> -->
						<div class="row-btn">
							<button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-save.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_forgot_pass_message2', FALSE); ?></button>
						</div>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</form>
</div><!--container-->
