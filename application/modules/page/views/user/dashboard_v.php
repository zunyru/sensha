<div class="clr inner">
	<div id="breadcrumbs">
		<span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></span></span>
	</div>
</div><!--inner-->
<div class="container-page">
	<div class="clr inner">
		<?php if($this->ion_auth->in_group(array('SA'))):?>
			<div class="topic">
				<p class="title-page"><?php echo $this->lang->line('page_dashboard_credit', FALSE); ?></p>
			</div>
			<p class="txt-green"><?php echo $this->lang->line('page_dashboard_use_credit', FALSE); ?></p>
			<p><?php echo $this->lang->line('page_dashboard_use_credit_desc1', FALSE); ?> <?php echo number_format($balance->balance);?> <?php echo $this->lang->line('page_dashboard_use_credit_desc2', FALSE); ?></p>
			<div class="line"><div class="b-yellow">Shop admin</div></div>
		<?php endif;?>
		<div class="box-shop-admin">


			<ul class="clr">
				<li>
					<a href="<?php echo base_url("page/user/personal_info");?>">
						<figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-user.png"></figure>
						<p class="title"><?php echo $this->lang->line('page_dashboard_edit_account', FALSE); ?></p>
						<p><?php echo $this->lang->line('page_dashboard_edit_account_desc', FALSE); ?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url("page/order_history");?>">
						<figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-history.png"></figure>
						<p class="title"><?php echo $this->lang->line('page_dashboard_shopping_history', FALSE); ?></p>
						<p><?php echo $this->lang->line('page_dashboard_shopping_history_desc', FALSE); ?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url("page/cart");?>">
						<figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-cart2.png"></figure>
						<p class="title"><?php echo $this->lang->line('page_dashboard_cart', FALSE); ?></p>
						<p><?php echo $this->lang->line('page_dashboard_cart_desc', FALSE); ?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url("page/user/wishlist");?>">
						<figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-wishlist.png"></figure>
						<p class="title"><?php echo $this->lang->line('page_dashboard_wishlist', FALSE); ?></p>
						<p><?php echo $this->lang->line('page_dashboard_wishlist_desc', FALSE); ?></p>
					</a>
				</li>
			</ul>
		</div>
		<div class="clr box-question">
			<div class="topic">
				<p class="title-page"><?php echo $this->lang->line('page_dashboard_faq', FALSE); ?></p>
			</div>
			<div class="clr inner-q">
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_sensha', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'SENSHA');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa2', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Products');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa4', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Order Payment');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa6', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Delivery');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa8', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Return');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa10', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Discount');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa12', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'Avgount');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
				<div class="col">
					<h3><?php echo $this->lang->line('page_qa_qa14', FALSE); ?></h3>
					<ul>
						<?php
						$this->db->where('category', 'The others');
						$qa_post = $this->datacontrol_model->getAllData('qa_post');
						?>
						<?php foreach($qa_post as $item):?>
							<li><a href="<?php echo base_url("page/qa_detail/$item->id");?>"><?php echo $item->title;?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		</div><!--box-question-->
		<?php if($this->ion_auth->in_group(array('FC', 'GU')) || ($this->ion_auth->in_group(array('SA')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB'))) ):?>
		<div class="clr box_recent_purchased">
			<div class="inner">
				<div class="topic">
					<p><?php echo $this->lang->line('page_dashboard_recommend', FALSE); ?></p>
					<h2><?php echo $this->lang->line('page_dashboard_recommend_desc', FALSE); ?></h2>
				</div>
				<div class="clr list-recent">

				</div>
			</div>
		</div>
		<div class="clr box_recent_purchased">
			<div class="inner">
				<div class="topic">
					<p><?php echo $this->lang->line('page_dashboard_recent_purchase', FALSE); ?></p>
					<h2><?php echo $this->lang->line('page_dashboard_recent_purchase_desc', FALSE); ?></h2>
				</div>
				<div class="clr list-recent">
					<ul class="owl-carousel owl-theme">
						<?php $product_id = ''; $p_id = array(); $i=1; foreach($order_history as $order):?>
							<?php

								if(!in_array($order->sale_item_id, $p_id)):
					        array_push($p_id, $order->sale_item_id);
									if($i<=5):
							?>
							<?php
							$product = $this->datacontrol_model->getRowData('product_general', array('id'=> $order->sale_item_id));
							$product_images = explode(',', $product->image);
							?>
							<?php if($product_id != $product->product_id): $product_id = $product->product_id;?>
								<li class="item">
									<figure>
										<a href="<?php echo base_url("page/product_detail/$order->product_type/$product->product_id");?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
									</figure>
									<div class="detail">
										<p class="name-p"><?php echo $product->product_name;?></p>
										<p class="quantity"><?php echo $product->caution;?></p>
										<?php if($order->buy_type == 'domestic'):?>
											<div class="clr p-amount">
												<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
												<div class="price">
													<?php echo number_format($order->item_sub_total_amount);?> Yen
												</div>
												<div class="clr line-btn">
													<?php if(empty($cart['oversea'])):?>
														<a href="javascript:void(0)" class="b-cart" onclick="add_cart('domestic', '<?php echo $order->product_type;?>', '<?php echo $order->sale_item_id;?>', '<?php echo base_url("page/product_detail/$order->product_type/$product->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
													<?php endif;?>
												</div>
											</div>
										<?php endif;?>
										<?php if($order->buy_type == 'oversea'):?>
											<div class="clr p-amount">
												<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
												<div class="price">
													<?php echo number_format($order->item_sub_total_amount);?> Yen
												</div>
												<div class="clr line-btn">
													<?php if(empty($cart['domestic'])):?>
														<a href="javascript:void(0)" class="b-cart" onclick="add_cart('oversea', '<?php echo $order->product_type;?>', '<?php echo $item->id;?>', '<?php echo base_url("page/product_detail/$order->product_type/$product->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
													<?php endif;?>
												</div>
											</div>
										<?php endif;?>
									</div>
								</li>
							<?php endif;?>
							<?php endif;?>
							<?php $i++; endif;?>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr box_recent_view">
			<div class="inner">
				<div class="topic">
					<p><?php echo $this->lang->line('page_dashboard_recently_viewed', FALSE); ?></p>
					<h2><?php echo $this->lang->line('page_dashboard_recently_viewed_desc', FALSE); ?></h2>
				</div>
				<div class="clr list-recent">
					<ul class="owl-carousel owl-theme">
						<?php foreach($recent_product as $item):?>
							<?php $product_images = explode(',', $item->image);?>
							<li class="item">
								<figure>
									<a href="<?php echo base_url("page/product_detail/$item->product_type/$item->product_id");?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
								</figure>
								<div class="detail">
									<p class="name-p"><?php echo $item->product_name;?></p>
									<p class="quantity"><?php echo $item->caution;?></p>
									<?php if($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')) )):?>
										<?php
										$this->db->where('product_country', $user_country);
										$this->db->where('product_id', $item->product_id);
										$product_domestic = $this->datacontrol_model->getRowData('product_general');
										?>
										<?php if($product_domestic->is_active == 1):?>
											<div class="clr p-amount">
												<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
												<div class="price">
													<?php echo number_format($product_domestic->sa_price);?> Yen
												</div>
												<div class="clr line-btn">
													<?php if(empty($cart['oversea'])):?>
														<a href="javascript:void(0)" class="b-cart" onclick="add_cart('domestic', 'general', '<?php echo $product_domestic->id;?>', '<?php echo base_url("page/product_detail/$item->product_type/$item->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
													<?php endif;?>
												</div>
											</div>
										<?php endif;?>
									<?php endif;?>
									<?php if($this->ion_auth->in_group(array('SA')) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')) )):?>
										<div class="clr p-amount">
											<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
											<div class="price">
												<?php echo number_format($item->global_price);?> Yen
											</div>
											<div class="clr line-btn">
												<?php if(empty($cart['domestic'])):?>
													<a href="javascript:void(0)" class="b-cart" onclick="add_cart('oversea', 'general', '<?php echo $item->id;?>', '<?php echo base_url("page/product_detail/$item->product_type/$item->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
												<?php endif;?>
											</div>
										</div>
									<?php endif;?>
								</div>
							</li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
		</div>
        <?php endif;?>
	</div>
</div><!--container-->

<script>
$(document).ready(function() {
	$('.owl-carousel').owlCarousel({
		loop:false,
		margin: 15,
		responsiveClass: true,
		responsive: {
			0: {
				items: 2,
				nav: false
			},
			600: {
				items: 4,
				nav: false
			},
			1000: {
				items: 5,
				nav: true,
				loop: false,
				margin: 20
			}
		}
	});
});
function add_cart(type, product_type, id, url){
	var amount = 1;
	// if(type == 'domestic'){
	// 	amount = $('#domestic_no').val();
	// }
	// if(type == 'oversea'){
	// 	amount = $('#oversea_no').val();
	// }
	if(product_type == ''){
		product_type = 'general';
	}

	$.ajax({
		url: '<?php echo base_url("page/product/add_cart/");?>'+product_type+'/'+id+'/'+type+'/'+amount,
		type:'GET',
		success: function(data){
			window.location = url;
			// $.ajax({
			// 	url:'<?php echo base_url("page/product/get_product_detail_cart_v");?>',
			// 	type:'GET',
			// 	success: function(data){
			// 		$('#product_detail_cart').html('<div class="p-shipping">'+$(data).html()+'</div>');
			// 	}
			// });
		}
	});
}
</script>
