<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_confirm_shipping_info', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_confirm_shipping_info', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_shipping_info_name', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('name', TRUE);?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_shipping_info_company', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('company', TRUE);?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_shipping_info_zip', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('zipcode', TRUE);?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_shipping_info_address', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('address', TRUE);?></p>
						</div>
					</div>
					<!--<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png"><?php echo $this->lang->line('page_shipping_info_state', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('state', TRUE);?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_shipping_info_country', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('country', TRUE);?></p>
						</div>
					</div>-->
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_shipping_info_telephone', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('phone', TRUE);?></p>
						</div>
					</div>
					<div class="row-btn">
						<a href="<?php echo base_url("page/user/complete_shipping_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_check_account_confirm', FALSE); ?></a>
					</div>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->
</div><!--container-->
