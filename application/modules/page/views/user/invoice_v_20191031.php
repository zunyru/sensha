<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>COMMERCIAL Invoice</title>
</head>
<style type="text/css">
/* @import url('https://fonts.googleapis.com/css?family=Kosugi&display=swap');

*{
	font-family: 'Kosugi', sans-serif;
} */

@font-face {
    font-family: 'pro_w3';
    src: url('<?=base_url("assets/font/Pro_w3.otf")?>');
}
@font-face {
    font-family: 'pro_w6';
    src: url('<?=base_url("assets/font/Pro_w6.otf")?>');
}
@font-face {
    font-family: 'ipag';
    src: url('<?=base_url("assets/font/ipag.ttf")?>') format('truetype');
}
@font-face {
    font-family: 'Firefly';
    src: url('<?=base_url("assets/font/Firefly.ttf")?>');
}

*{
	font-family: 'ipag', "sans-serif", Helvetica;
}
	body {
		padding: 0px;
		font-size:14px;
		margin: 0;
		width: 100%;
		color: #000;

		/* font-family: "Gill Sans", "Gill Sans MT", "Myriad Pro", "DejaVu Sans Condensed", Helvetica, Arial, "sans-serif" */
	}
	.container{width:100%; margin:10px auto; max-width:900px;}
	.row{margin-bottom:10px;}
	p{padding: 0px;margin:0px 0 10px 0;}
	.txt-red{color:#FF0000}
	table{border:1}
	table {
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  padding:5px;
	}
	.table td{border: 1px solid #000;}
	ul{margin:0;padding: 0px;}
	li{list-style: none;margin-bottom: 10px;line-height: 20px;}
	.list{}
	.list ul li{margin-bottom: 10px;list-style: decimal;}
</style>
<body>
	<div class="container" style="overflow:hidden">
		<table>
			<tbody>
				<tr>
				  <td colspan="2"><img src="<?php echo base_url('assets/sensha-theme/images/logo.png');?>" width="220" height="73" alt=""/></td>
				  <td colspan="2" style="font-size: 40px; font-weight: bold;color:#aaaaaa;">INVOICE</td>
				</tr>
				<tr>
					<td colspan="4" style="font-style: italic;"><strong>Contribute to "safe drive and traffic safety "over the world through car care business. It's our Dream.</strong> </td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>INVOICE NO. : <?php echo date('Ymd', strtotime($sales_history[0]->sale_date));?> - <?php echo $sales_history[0]->create_by;?></td>
					<td></td>
					<td align="right"><strong>DATE <?php echo $sales_history[0]->sale_date;?></strong></td>
					<td align="right"></td>
				</tr>
				<tr>
					<td>SENSHA Co., Ltd.</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>1007-3 Kamikasuya, Isehara, Kanagawa 259-1141</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Phone 81(463) 94-5106<br>Fax 81(463) 94-5108</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Mail Address: info@cleanyourcar.jp</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><strong>Shipment Terms: <?php echo $sales_history_total->shipping_method;?></strong></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>To: <?php echo $shipping_info->name;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Company Name: <?php echo $shipping_info->company;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Address: <?php echo $shipping_info->address;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>ZIP Code(Postal Code): <?php echo $shipping_info->zipcode;?></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
				  <td>Phone: <?php echo $shipping_info->phone;?></td>
				  <td></td>
				  <td></td>
				  <td></td>
			  </tr>
			  <tr>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td></td>
			  </tr>
			</tbody>
		</table>
		<table style="border: 1px solid #000;" class="table">
			<tbody>
				<tr style="background:#cccccc;font-weight:bold; border:1px solid #000;">
					  <td align="center">Products ordered (<?php echo $sales_history_total->shipment_id;?>)</td>
					  <td align="center">Qty</td>
					  <td align="center">Unit price (JPY)</td>
					  <td align="center">AMOUNT (JPY)</td>
				  </tr>
				<?php foreach($sales_history as $item):?>
					<?php
					$shipment_fee = $item->delivery_amount;
					?>
					<tr style="border: 1px solid #000;">
					  <td ><?php echo $item->product_name;?></td>
					  <td style="text-align: center;"><?php echo $item->item_amount;?></td>
					  <td style="text-align: right;"><?php echo number_format($item->item_sub_total_amount);?></td>
					  <td style="text-align: right;"><?php echo number_format($item->item_sub_total_amount * $item->item_amount);?></td>
				  </tr>
				<?php endforeach;?>
				<tr>
					<td>SHIPPING FEE</td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($shipment_fee);?></td>
				</tr>
				<tr>
					<td>DISCOUNT</td>
					<td></td>
					<td></td>
					<td style="text-align: right;">-<?php echo number_format($sales_history_total->discount_amount);?></td>
				</tr>
				<tr>
					<td>AFTER DISCOUNT</td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->sale_total_amount);?></td>
				</tr>
				<tr>
					<td>ADMIN FEE</td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->admin_fee_amount);?></td>
				</tr>
				<tr>
					<td>TAX <?php echo number_format($sales_history_total->vat_percent);?>% </td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><?php echo number_format($sales_history_total->vat_amount);?></td>
				</tr>
				<tr style="background:#222;font-weight:bold; border:1px solid #fff; color: #fff;">
					<td  style="border:1px solid #fff; color: #ffffff;">GRAND TOTAL</td>
					<td style="border:1px solid #fff; color: #fff;"></td>
					<td style="border:1px solid #fff; color: #fff;"></td>
					<td style="border:1px solid #fff; color: #fff; text-align: right;"><?php echo number_format($sales_history_total->sale_total_amount + $sales_history_total->vat_amount + $sales_history_total->admin_fee_amount);?></td>
				</tr>
		</tbody>
		</table>
		<table style="margin-top:20px;">
			<tbody>
				<tr>
					<td><strong>Payment Options</strong></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Bank Transfer</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<!--tr>
					<td>※ JPY 3000 additional remittance fee is charged to payer.</td>
					<td></td>
					<td></td>
					<td style="text-align: right;"><strong>$ 30.00</strong></td>
				</tr-->
				<tr>
					<td>Bank: RESONA BANK, LTD.</td>
					<td colspan="3">Account Number: 1299037</td>
				</tr>
				<tr>
					<td>Swift Code: DIWAJPJT</td>
					<td colspan="3">Account Holder: SENSHA Co., Ltd. 			 </td>

				</tr>
				<tr>
					<td>Branch: Isehara</td>
					<td colspan="3">Address: 1-3-6 Isehara, Isehara-City, Kanagawa, 259-1131 JAPAN</td>

				</tr>
				<tr>
					<td>Zengin Bank Code: 0010</td>
					<td colspan="3">Telephone: ＋81-(0)463-92-1511			</td>
				</tr>
				<tr>
					<td>Zengin Branch Code: 646</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="4" align="center" style="font-size:16px;"><strong>THANK YOU FOR YOUR BUSINESS!</strong></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- <script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
	<script>
		$(function(){
			var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };

		doc.fromHTML($('html').html(), 15, 15, {
				'width': 170,
						'elementHandlers': specialElementHandlers
		});
		// doc.addImage(blob, 'PNG', 0, 0, $('.container').width(), $('.container').height());
		doc.save('sample-file.pdf');

		})
	</script> -->
</body>
</html>
