<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_account_info', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_account_info_account_info', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_account_info_id', FALSE); ?></label>
						<div class="r-input">
							<p>MD7318TS</p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_account_info_email', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->ion_auth->user()->row()->email;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_account_info_telephone', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->ion_auth->user()->row()->phone;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png"><?php echo $this->lang->line('page_account_info_password', FALSE); ?></label>
						<div class="r-input">
							<p>xxxxxxxxxxxxxxxxxxxxxxxxxxxx</p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_account_info_country', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->ion_auth->user()->row()->country;?></p>
						</div>
					</div>
					<?php if($this->ion_auth->user()->row()->country == 'Japan'):?>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png"><?php echo $this->lang->line('page_account_info_state', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->ion_auth->user()->row()->state;?></p>
						</div>
					</div>
				<?php endif;?>
					<div class="row-btn">
						<a href="<?php echo base_url("page/user/edit_personal_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_account_info_edit', FALSE); ?></a>
					</div>
				</div>
			</div>
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_shipping_info_shipping_info', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_shipping_info_name', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->name;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_shipping_info_company', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->company;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_shipping_info_zip', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->zipcode;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_shipping_info_address', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->address;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png"><?php echo $this->lang->line('page_shipping_info_state', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->state;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_shipping_info_country', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->country;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_shipping_info_telephone', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$shipping_info->phone;?></p>
						</div>
					</div>
					<div class="row-btn">
						<a href="<?php echo base_url("page/user/edit_shipping_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_account_info_edit', FALSE); ?></a>
					</div>
				</div>
			</div>
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_invoice_info_change_invoice', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_invoice_info_name', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->name;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_invoice_info_company', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->company;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_invoice_info_zip', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->zipcode;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_invoice_info_address', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->address;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/06.png"><?php echo $this->lang->line('page_invoice_info_state', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->state;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_invoice_info_country', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->country;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_invoice_info_telephone', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->phone;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/09.png"><?php echo $this->lang->line('page_invoice_info_tax', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo @$invoice_info->taxid;?></p>
						</div>
					</div>
					<div class="row-btn">
						<a href="<?php echo base_url("page/user/edit_invoice_info");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_account_info_edit', FALSE); ?></a>
					</div>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->

</div><!--container-->
