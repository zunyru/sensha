<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url();?>">Home</a><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_shipping_info', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_shipping_info_shipping_info', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<form method="post" action="<?php echo base_url('page/user/confirm_shipping_info');?>">
						<input type="hidden" name="country" value="<?php echo $this->ion_auth->user()->row()->country;?>">
						<?php
						  $zipcode_japan = "";
                          if(($this->ion_auth->user()->row()->country == 'Japan') || ($this->ion_auth->user()->row()->country == 'JapanA') || ($this->ion_auth->user()->row()->country == 'JapanB')){
                          	$zipcode_japan = "zipcode_japan";
                          }
						?>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_shipping_info_name', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control" name="name" value="<?php echo @$shipping_info->name;?>" required />
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_shipping_info_company', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your company" class="form-control" name="company" value="<?php echo @$shipping_info->company;?>" />
							</div>
						</div>
						<div class="r-inline">
							<label  class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_shipping_info_zip', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your zipcode" class="form-control masked" name="zipcode" value="<?php echo @$shipping_info->zipcode;?>"  id="<?=$zipcode_japan?>" required>
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_shipping_info_address', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your Address" class="form-control" name="address" value="<?php echo @$shipping_info->address;?>" required>
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_shipping_info_telephone', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your Telephone" class="form-control" name="phone" value="<?php echo @$shipping_info->phone;?>" required>
							</div>
						</div>
						<div class="row-btn">
							<button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-save.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_edit_account_update', FALSE); ?></button>
						</div>
					</form>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->
</div><!--container-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/jquery.inputmask.bundle.min.js"></script>
<script>
$(document).ready(function(){
    $("#zipcode_japan").inputmask("999-9999");
});
</script>
