<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a></span><span><?php echo $this->lang->line('breadcrumb_invoice_info', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_invoice_info_change_invoice', FALSE); ?></p>
				</div>
				<div class="box-inner">
					<form method="post" action="<?php echo base_url('page/user/confirm_invoice_info');?>">
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_invoice_info_name', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your name" class="form-control" name="name" value="<?php echo @$invoice_info->name;?>" required />
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_invoice_info_company', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your company" class="form-control" name="company" value="<?php echo @$invoice_info->company;?>" />
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_invoice_info_zip', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your zipcode" class="form-control" name="zipcode" value="<?php echo @$invoice_info->zipcode;?>" required>
							</div>
						</div>
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_invoice_info_address', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your Address" class="form-control" name="address" value="<?php echo @$invoice_info->address;?>" required>
							</div>
						</div>
						
						<div class="r-inline">
							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_invoice_info_telephone', FALSE); ?></label>
							<div class="r-input">
								<input type="text" placeholder="Please input your Telephone" class="form-control" name="phone" value="<?php echo @$invoice_info->phone;?>" required>
							</div>
						</div>
						<div class="row-btn">
							<button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-save.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_edit_account_update', FALSE); ?></button>
						</div>
					</form>
				</div>
			</div>
		</div><!--layout-contain-->
	</div><!--inner-->
</div><!--container-->
