<!DOCTYPE HTML>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php echo $htmlTitle; ?></title>
	<link rel="canonical" href="<?php echo base_url(uri_string()); ?>" />
	<meta name="description" content="<?php echo $htmlDes; ?>">
	<meta name="keywords" content="">
	<?php if (!$this->ion_auth->logged_in()) : ?>
		<?php
		$uri = uri_string();
		if ($this->coutry_iso != '') {
			$uri = str_replace("$this->coutry_iso", "", $uri);
		}

		$this->db->where('is_active', 1);
		$nation_lang = $this->datacontrol_model->getAllData('nation_lang');
		?>
		<?php foreach ($nation_lang as $item) : ?>
			<?php
			$this->db->where('nicename', $item->country);
			$c = $this->datacontrol_model->getRowData('countries');
			?>

			<?php if ($item->country == "Global") : ?>
				<link rel="alternate" href="<?php echo base_url($uri); ?>" hreflang="en" />
			<?php else : ?>
				<link rel="alternate" href="<?php echo base_url(strtolower($c->iso) . $uri); ?>" hreflang="<?php echo strtolower($c->iso); ?>" />
			<?php endif; ?>

		<?php endforeach; ?>
	<?php else : ?>
		<link rel="alternate" href="<?php echo base_url(uri_string()); ?>" hreflang="<?php echo $this->coutry_iso; ?>" />
	<?php endif; ?>

	<?php if ($noindex) : ?>
		<meta name=”robots” content="noindex">
	<?php endif; ?>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/animation.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/home.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/common.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/page.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/menu-mobile.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="https://www.at-aroma.com/js/common/html5.js"></script>
	<![endif]-->
</head>

<body class>
	<div id="wrapper">
		<?php $this->load->view('header_v'); ?>
		<div class="container">
			<div class="clr inner">
				<div id="breadcrumbs">
					<span><a href="<?php echo base_url("$coutry_iso"); ?>">Home</a></span>
					<?php if (!$cluster1->cluster_name) { ?>
						<span>404 PRODUCT NOT FOUND</span>
					<?php } else { ?>
						<span><?php echo $cluster1->cluster_name; ?></span>
					<?php } ?>
				</div>
				<!-----------------menu-category-product-mobile------------------------>
				<div id="stick-here"></div>
				<div id="stickThis">
					<div class="menu-mobile">
						<header id="toggle">
							<h3><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/list.png"> Product Menu </h3><span class="arrow"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/chev.png"></span>
						</header>
						<div id="content">
							<aside class="col-left">
								<div class="search">
									<form method="get" action="">
										<input type="text" placeholder="Keyword Search" name="s" value="<?php echo (isset($_GET['s'])) ? $_GET['s'] : ''; ?>">
										<button type="submit"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-search.png" width="25"></button>
									</form>
								</div>
								<div class="box_menu">
									<div class="a_menu">
										<h3><?php echo $cluster1->cluster_name; ?></h3>
										<ul>
											<?php foreach ($sub_cluster as $item) : ?>
												<li><a href="<?php echo base_url("$coutry_iso" . "page/general/$cluster1->cluster_url/$item->cluster_url"); ?>/"><?php echo $item->cluster_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<form id="product_filter_mobile" method="post" action="<?php echo base_url("$coutry_iso" . "page/general/$cluster1->cluster_url"); ?>">
										<div class="a_menu m-checkbox">
											<h3><?php echo $this->lang->line('page_product_general_material', FALSE); ?></h3>
											<ul>
												<li><label><input type="checkbox" name="material[]" value="Painting" <?php echo (!empty($this->input->post("material")) && (in_array("Painting", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_painting', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Emblem" <?php echo (!empty($this->input->post("material")) && (in_array("Emblem", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_emblem', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Glass" <?php echo (!empty($this->input->post("material")) && (in_array("Glass", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_glass', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Tire" <?php echo (!empty($this->input->post("material")) && (in_array("Tire", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tire', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Tire house" <?php echo (!empty($this->input->post("material")) && (in_array("Tire house", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tire_house', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Plating" <?php echo (!empty($this->input->post("material")) && (in_array("Plating", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_plating', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Shaving-polish" <?php echo (!empty($this->input->post("material")) && (in_array("Shaving-polish", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_shaving', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Unpainted plastic" <?php echo (!empty($this->input->post("material")) && (in_array("Unpainted plastic", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_unpainted_plastic', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Gum" <?php echo (!empty($this->input->post("material")) && (in_array("Gum", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_gum', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Engine" <?php echo (!empty($this->input->post("material")) && (in_array("Engine", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_engine', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Hose" <?php echo (!empty($this->input->post("material")) && (in_array("Hose", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_hose', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Fabric" <?php echo (!empty($this->input->post("material")) && (in_array("Fabric", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_fabric', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Leather" <?php echo (!empty($this->input->post("material")) && (in_array("Leather", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_leather', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="material[]" value="Meter panel" <?php echo (!empty($this->input->post("material")) && (in_array("Meter panel", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_meter_panel', FALSE); ?></label></li>
											</ul>
										</div>
										<div class="a_menu m-checkbox">
											<h3><?php echo $this->lang->line('page_product_general_usage', FALSE); ?></h3>
											<ul>
												<li><label><input type="checkbox" name="usage[]" value="Protection" <?php echo (!empty($this->input->post("usage")) && (in_array("Protection", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_protection', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Glazing" <?php echo (!empty($this->input->post("usage")) && (in_array("Glazing", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_glazing', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Self-healing" <?php echo (!empty($this->input->post("usage")) && (in_array("Self-healing", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_self_healing', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Water repellent" <?php echo (!empty($this->input->post("usage")) && (in_array("Water repellent", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_water_repellent', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Oil repellent" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil repellent", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil_repellent', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Yellowing resistance" <?php echo (!empty($this->input->post("usage")) && (in_array("Yellowing resistance", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_yellowing', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Cracks of the surface" <?php echo (!empty($this->input->post("usage")) && (in_array("Cracks of the surface", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_cracks', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="For rainy day" <?php echo (!empty($this->input->post("usage")) && (in_array("For rainy day", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_rainy', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Night" <?php echo (!empty($this->input->post("usage")) && (in_array("Night", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_night', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Dust" <?php echo (!empty($this->input->post("usage")) && (in_array("Dust", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dust', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Dirty dirt" <?php echo (!empty($this->input->post("usage")) && (in_array("Dirty dirt", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dirty_dirt', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Dirt (of scale)" <?php echo (!empty($this->input->post("usage")) && (in_array("Dirt (of scale)", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dirt', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Oil film" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil film", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil_film', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Scratches" <?php echo (!empty($this->input->post("usage")) && (in_array("Scratches", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_scratches', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Kusumi" <?php echo (!empty($this->input->post("usage")) && (in_array("Kusumi", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_kusumi', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Ring stain" <?php echo (!empty($this->input->post("usage")) && (in_array("Ring stain", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_ring_stain', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Stain" <?php echo (!empty($this->input->post("usage")) && (in_array("Stain", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_stain', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Oil stains" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil stains", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Tar" <?php echo (!empty($this->input->post("usage")) && (in_array("Tar", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tar', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Iron powder" <?php echo (!empty($this->input->post("usage")) && (in_array("Iron powder", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_iron_powder', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Brake dust" <?php echo (!empty($this->input->post("usage")) && (in_array("Brake dust", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_brake_dust', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Insects" <?php echo (!empty($this->input->post("usage")) && (in_array("Insects", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_insects', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Clouded window" <?php echo (!empty($this->input->post("usage")) && (in_array("Clouded window", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_clouded', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Washer" <?php echo (!empty($this->input->post("usage")) && (in_array("Washer", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_washer', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Deodorize" <?php echo (!empty($this->input->post("usage")) && (in_array("Deodorize", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_deodorize', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Sterilization" <?php echo (!empty($this->input->post("usage")) && (in_array("Sterilization", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_sterilization', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="usage[]" value="Antibacterial" <?php echo (!empty($this->input->post("usage")) && (in_array("Antibacterial", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_antibacterial', FALSE); ?></label></li>
											</ul>
										</div>
										<div class="a_menu m-checkbox">
											<h3><?php echo $this->lang->line('page_product_general_item_type', FALSE); ?></h3>
											<ul>
												<li><label><input type="radio" name="item_type" value="Basic wash" <?php echo ($this->input->post("item_type") == "Basic wash") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_basic_wash', FALSE); ?></label></li>
												<li><label><input type="radio" name="item_type" value="Coating" <?php echo ($this->input->post("item_type") == "Coating") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_coating', FALSE); ?></label></li>
												<li><label><input type="radio" name="item_type" value="Special wash" <?php echo ($this->input->post("item_type") == "Special wash") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_special_wash', FALSE); ?></label></li>
												<li><label><input type="radio" name="item_type" value="PPF" <?php echo ($this->input->post("item_type") == "PPF") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_ppf', FALSE); ?></label></li>
												<li><label><input type="radio" name="item_type" value="The others" <?php echo ($this->input->post("item_type") == "The others") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_others', FALSE); ?></label></li>
											</ul>
										</div>
										<div class="a_menu m-checkbox">
											<h3><?php echo $this->lang->line('page_product_general_feature', FALSE); ?></h3>
											<ul>
												<li><label><input type="checkbox" name="feature[]" value="Domestic delivery" <?php echo (!empty($this->input->post("feature")) && (in_array("Domestic delivery", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_domestic_delivery', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="feature[]" value="Free sample" <?php echo (!empty($this->input->post("feature")) && (in_array("Free sample", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_free_sample', FALSE); ?></label></li>
												<li><label><input type="checkbox" name="feature[]" value="Bottle" <?php echo (!empty($this->input->post("feature")) && (in_array("Bottle", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_bottle', FALSE); ?></label></li>
											</ul>
										</div>
										<input type="hidden" name="product_filter" value="1" />
									</form>
								</div>
							</aside>
						</div>
					</div>
					<!--menu-mobile-->
				</div>
				<!-----------------end-menu-category-product-mobile------------------------>

				<aside class="col-left">
					<div class="search">
						<form method="get" action="">
							<input type="text" placeholder="Keyword Search" name="s" value="<?php echo (isset($_GET['s'])) ? $_GET['s'] : ''; ?>">
							<button type="submit"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-search.png" width="25"></button>
						</form>
					</div>
					<div class="box_menu">
						<div class="a_menu">
							<h3><?php echo $cluster1->cluster_name; ?></h3>
							<ul>
								<?php foreach ($sub_cluster as $item) : ?>
									<li><a href="<?php echo base_url("$coutry_iso" . "page/general/$cluster1->cluster_url/$item->cluster_url"); ?>/"><?php echo $item->cluster_name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<form id="product_filter" method="post" action="<?php echo base_url("$coutry_iso" . "page/general/$cluster1->cluster_url"); ?>">
							<div class="a_menu m-checkbox">
								<h3><?php echo $this->lang->line('page_product_general_material', FALSE); ?></h3>
								<ul>
									<li><label><input type="checkbox" name="material[]" value="Painting" <?php echo (!empty($this->input->post("material")) && (in_array("Painting", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_painting', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Emblem" <?php echo (!empty($this->input->post("material")) && (in_array("Emblem", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_emblem', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Glass" <?php echo (!empty($this->input->post("material")) && (in_array("Glass", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_glass', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Tire" <?php echo (!empty($this->input->post("material")) && (in_array("Tire", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tire', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Tire house" <?php echo (!empty($this->input->post("material")) && (in_array("Tire house", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tire_house', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Plating" <?php echo (!empty($this->input->post("material")) && (in_array("Plating", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_plating', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Shaving-polish" <?php echo (!empty($this->input->post("material")) && (in_array("Shaving-polish", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_shaving', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Unpainted plastic" <?php echo (!empty($this->input->post("material")) && (in_array("Unpainted plastic", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_unpainted_plastic', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Gum" <?php echo (!empty($this->input->post("material")) && (in_array("Gum", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_gum', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Engine" <?php echo (!empty($this->input->post("material")) && (in_array("Engine", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_engine', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Hose" <?php echo (!empty($this->input->post("material")) && (in_array("Hose", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_hose', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Fabric" <?php echo (!empty($this->input->post("material")) && (in_array("Fabric", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_fabric', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Leather" <?php echo (!empty($this->input->post("material")) && (in_array("Leather", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_leather', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="material[]" value="Meter panel" <?php echo (!empty($this->input->post("material")) && (in_array("Meter panel", $this->input->post("material")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_meter_panel', FALSE); ?></label></li>
								</ul>
							</div>
							<div class="a_menu m-checkbox">
								<h3><?php echo $this->lang->line('page_product_general_usage', FALSE); ?></h3>
								<ul>
									<li><label><input type="checkbox" name="usage[]" value="Protection" <?php echo (!empty($this->input->post("usage")) && (in_array("Protection", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_protection', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Glazing" <?php echo (!empty($this->input->post("usage")) && (in_array("Glazing", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_glazing', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Self-healing" <?php echo (!empty($this->input->post("usage")) && (in_array("Self-healing", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_self_healing', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Water repellent" <?php echo (!empty($this->input->post("usage")) && (in_array("Water repellent", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_water_repellent', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Oil repellent" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil repellent", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil_repellent', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Yellowing resistance" <?php echo (!empty($this->input->post("usage")) && (in_array("Yellowing resistance", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_yellowing', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Cracks of the surface" <?php echo (!empty($this->input->post("usage")) && (in_array("Cracks of the surface", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_cracks', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="For rainy day" <?php echo (!empty($this->input->post("usage")) && (in_array("For rainy day", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_rainy', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Night" <?php echo (!empty($this->input->post("usage")) && (in_array("Night", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_night', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Dust" <?php echo (!empty($this->input->post("usage")) && (in_array("Dust", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dust', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Dirty dirt" <?php echo (!empty($this->input->post("usage")) && (in_array("Dirty dirt", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dirty_dirt', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Dirt (of scale)" <?php echo (!empty($this->input->post("usage")) && (in_array("Dirt (of scale)", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_dirt', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Oil film" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil film", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil_film', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Scratches" <?php echo (!empty($this->input->post("usage")) && (in_array("Scratches", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_scratches', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Kusumi" <?php echo (!empty($this->input->post("usage")) && (in_array("Kusumi", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_kusumi', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Ring stain" <?php echo (!empty($this->input->post("usage")) && (in_array("Ring stain", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_ring_stain', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Stain" <?php echo (!empty($this->input->post("usage")) && (in_array("Stain", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_stain', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Oil stains" <?php echo (!empty($this->input->post("usage")) && (in_array("Oil stains", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_oil', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Tar" <?php echo (!empty($this->input->post("usage")) && (in_array("Tar", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_tar', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Iron powder" <?php echo (!empty($this->input->post("usage")) && (in_array("Iron powder", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_iron_powder', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Brake dust" <?php echo (!empty($this->input->post("usage")) && (in_array("Brake dust", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_brake_dust', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Insects" <?php echo (!empty($this->input->post("usage")) && (in_array("Insects", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_insects', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Clouded window" <?php echo (!empty($this->input->post("usage")) && (in_array("Clouded window", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_clouded', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Washer" <?php echo (!empty($this->input->post("usage")) && (in_array("Washer", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_washer', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Deodorize" <?php echo (!empty($this->input->post("usage")) && (in_array("Deodorize", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_deodorize', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Sterilization" <?php echo (!empty($this->input->post("usage")) && (in_array("Sterilization", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_sterilization', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="usage[]" value="Antibacterial" <?php echo (!empty($this->input->post("usage")) && (in_array("Antibacterial", $this->input->post("usage")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_antibacterial', FALSE); ?></label></li>
								</ul>
							</div>
							<div class="a_menu m-checkbox">
								<h3><?php echo $this->lang->line('page_product_general_item_type', FALSE); ?></h3>
								<ul>
									<li><label><input type="radio" name="item_type" value="Basic wash" <?php echo ($this->input->post("item_type") == "Basic wash") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_basic_wash', FALSE); ?></label></li>
									<li><label><input type="radio" name="item_type" value="Coating" <?php echo ($this->input->post("item_type") == "Coating") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_coating', FALSE); ?></label></li>
									<li><label><input type="radio" name="item_type" value="Special wash" <?php echo ($this->input->post("item_type") == "Special wash") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_special_wash', FALSE); ?></label></li>
									<li><label><input type="radio" name="item_type" value="PPF" <?php echo ($this->input->post("item_type") == "PPF") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_ppf', FALSE); ?></label></li>
									<li><label><input type="radio" name="item_type" value="The others" <?php echo ($this->input->post("item_type") == "The others") ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_others', FALSE); ?></label></li>
								</ul>
							</div>
							<div class="a_menu m-checkbox">
								<h3><?php echo $this->lang->line('page_product_general_feature', FALSE); ?></h3>
								<ul>
									<li><label><input type="checkbox" name="feature[]" value="Domestic delivery" <?php echo (!empty($this->input->post("feature")) && (in_array("Domestic delivery", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_domestic_delivery', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="feature[]" value="Free sample" <?php echo (!empty($this->input->post("feature")) && (in_array("Free sample", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_free_sample', FALSE); ?></label></li>
									<li><label><input type="checkbox" name="feature[]" value="Bottle" <?php echo (!empty($this->input->post("feature")) && (in_array("Bottle", $this->input->post("feature")))) ? "checked" : ""; ?>><?php echo $this->lang->line('page_product_general_bottle', FALSE); ?></label></li>
								</ul>
							</div>
							<input type="hidden" name="product_filter" value="1" />
						</form>
					</div>
				</aside>
				<div class="content">
					<div class="headline">
						<p class="title"><?php echo $cluster1->cluster_name; ?><?php echo $this->lang->line('page_cat_title', FALSE); ?></p>
						<?php if (!$cluster1->cluster_name) { ?>
							<div class="clr inner">
								<div class="box-content">
									<div class="layout-contain">
										<div class="clr box_form">
											<div class="topic">
												<p class="title-page">Sorry, this page isn't available.</p>
											</div>
											<div class="clr box-success" style="text-align: center;margin-top:50px;">
												<p style="font-size: 16px;font-weight: 700;margin-bottom: 40px;">The link you followed may be broken, or the page may have been removed.</p>
												<h3 style="font-size:98px;font-weight: 700;font-family: 'Oswald', sans-serif;color:#004ea1;">404 </h3>
												<p style="font-size: 35px;font-weight: 700;margin-bottom:30px;">PAGE NOT FOUND</p>
											</div>
										</div>
									</div>
									<!--layout-contain-->
								</div>
								<!--inner-->
							<?php } else { ?>
								<h1 style="text-align:left;"><?php echo $cluster1->cluster_name; ?><?php echo $this->lang->line('page_cat_subtitle', FALSE); ?></h1>
							<?php } ?>
							<product>
								<div class="clr product-list">
									<ul>
										<?php $i = 0;
										foreach ($products as $item) : ?>
											<?php $product_images = explode(',', $item->image); ?>

											<li>
												<a href="<?php echo base_url("$coutry_iso" . "page/product_detail/$product_type/$item->product_id"); ?>">
													<figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]"); ?>" alt="" /></figure>
													<div class="detail">
														<p class="name-p"><?php echo $item->product_name; ?></p>
														<?php if ($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')))) : ?>
															<?php
															$this->db->where('product_id', $item->product_id);
															$this->db->where('product_country', $this->user_lang);
															$product_domestic = $this->datacontrol_model->getRowData('product_general');
															?>
															<?php 
															if ($product_domestic->is_active == 1) : 
																if ($nation_lang->is_active == 1) : 
															?>
																<div class="clr p-amount">
																	<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
																	<div class="price">
																		<?php echo number_format($product_domestic->sa_price); ?> Yen
																	</div>
																</div>
															<?php endif; endif;?>
														<?php endif; ?>
														<?php //if($this->ion_auth->in_group(array('SA')) || (!$this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')) )):
														?>
														<?php if ($this->ion_auth->in_group(array('SA')) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')))) : ?>
															<div class="clr p-amount">
																<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
																<div class="price">
																	<?php echo number_format($item->global_price); ?> Yen
																</div>
															</div>
														<?php endif; ?>

													</div>
												</a>
											</li>
										<?php $i++;
										endforeach; ?>
									</ul>
								</div>
								<section id="pagination" >
									<?php echo $links; ?>
									<!-- <ul>

									<li class="previous"><a href=""><img src="<?php //echo base_url("assets/sensha-theme/"); ?>images/arrow-p.png"></a></li>
									<li class="active"><span>1</span></li>
									<li class="number"><a href="">2</a></li>
									<li class="number"><a href="">3</a></li>
									<li class="number"><a href="">4</a></li>
									<li class="number"><a href="">5</a></li>
									<li class="next"><a href=""><img src="<?php //echo base_url("assets/sensha-theme/"); ?>images/arrow-next.png"></a></li>
								</ul> -->
								</section>
							</product>

							</div>
							<div>
							</div>
					</div>
				</div>
				<!--inner-->
			</div>
			<script src="<?php echo base_url("assets/sensha-theme/"); ?>js/main.js"></script>
			<?php $this->load->view('footer_v'); ?>
			<script>
				function sticktothetop() {
					var window_top = $(window).scrollTop();
					var top = $('#stick-here').offset().top;
					if (window_top > top) {
						$('#stickThis').addClass('stick');
						$('#stick-here').height($('#stickThis').outerHeight());
					} else {
						$('#stickThis').removeClass('stick');
						$('#stick-here').height(0);
					}
				}
				$(function() {
					$(window).scroll(sticktothetop);
					sticktothetop();

					$('#product_filter input').click(function() {
						$('#product_filter').attr('action', $(this).attr('href'));
						$('#product_filter').submit();
						
					});
					$('#product_filter_mobile input').click(function() {
						$('#product_filter_mobile').attr('action', $(this).attr('href'));
						$('#product_filter_mobile').submit();
						
					});

					$('#pagination li.number a').click(function() {
						$('#product_filter').attr('action', $(this).attr('href'));
						$('#product_filter').submit();
						return false;
					});

                    $('#pagination li.previous a').click(function() {
                        $('#product_filter').attr('action', $(this).attr('href'));
                        $('#product_filter').submit();
                        return false;
                    });

                    $('#pagination li.next a').click(function() {
                        $('#product_filter').attr('action', $(this).attr('href'));
                        $('#product_filter').submit();
                        return false;
                    });


				});
			</script>
		</div>
		<!-- .wrapper -->