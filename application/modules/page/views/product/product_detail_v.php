<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $htmlTitle; ?></title>
    <link rel="canonical" href="<?php echo base_url(uri_string()); ?>"/>
    <meta name="description" content="<?php echo $htmlDes; ?>">
    <meta name="keywords" content="">
    <link rel="stylesheet" type="text/css"
          href="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.css">

    <?php if (!$this->ion_auth->logged_in()): ?>
        <?php
        $uri = uri_string();
        if ($this->coutry_iso != '') {
            $uri = str_replace("$this->coutry_iso", "", $uri);
        }

        $this->db->where('is_active', 1);
        $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
        ?>
        <?php foreach ($nation_lang as $item): ?>
            <?php
            $this->db->where('nicename', $item->country);
            $c = $this->datacontrol_model->getRowData('countries');
            ?>

            <?php if ($item->country == "Global"): ?>
                <link rel="alternate" href="<?php echo base_url($uri); ?>" hreflang="en"/>
            <?php else: ?>
                <link rel="alternate" href="<?php echo base_url(strtolower($c->iso) . $uri); ?>"
                      hreflang="<?php echo strtolower($c->iso); ?>"/>
            <?php endif; ?>

        <?php endforeach; ?>
    <?php else: ?>
        <link rel="alternate" href="<?php echo base_url(uri_string()); ?>" hreflang="<?php echo $this->coutry_iso; ?>"/>
    <?php endif; ?>


    <?php if (!$product_cat->cluster_name || !$products->product_name) { ?>
        <meta name=”robots” content="noindex">
    <?php } ?>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/animation.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/home.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/common.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/page.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/menu-mobile.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.bxslider.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://www.at-aroma.com/js/common/html5.js"></script>
    <![endif]-->
</head>
<body class>
<div id="wrapper">
    <?php $this->load->view('header_v'); ?>
    <div class="container">
        <div class="clr inner">
            <div id="breadcrumbs">
                <span><a href="<?php echo base_url("$coutry_iso"); ?>">Home</a></span>
                <?php if (!$product_cat->cluster_name || !$products->product_name) { ?>
                    <span>404 PRODUCT NOT FOUND</span>
                <?php } else { ?>
                    <span><a href="<?php echo base_url($coutry_iso . "page/$product_type/$product_cat->cluster_url"); ?>"><?php echo $product_cat->cluster_name; ?></a></span>
                    <span> <?php echo $products->product_name; ?></span>
                <?php } ?>
            </div>
            <!-----------------menu-category-product-mobile------------------------>
            <div id="stick-here"></div>
            <div id="stickThis">
                <div class="menu-mobile">
                    <div class="clr m-inner">
                        <h3><i class="fas fa-clipboard-list"
                               style="font-size:17px;margin-right: 5px;"></i> <?php echo $this->lang->line('page_product_detail_right_proceed_payment', FALSE); ?>
                        </h3>
                        <div class="right">
                            <!-- <a href="cart.php"><i class="fas fa-shopping-cart"></i> &nbsp;<?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?></a> -->
                            <!-- 								<a href="<?php echo base_url('page/cart'); ?>" style="background:#f9bd00;"><i class="fas fa-money-bill-alt"></i>&nbsp; <?php echo $this->lang->line('page_product_detail_right_buy_now', FALSE); ?></a> -->
                            <a href="#cart-target" class="mobile-buy-btn" style="background:#f9bd00;"><i
                                        class="fas fa-money-bill-alt"></i>&nbsp; <?php echo $this->lang->line('page_product_detail_right_buy_now', FALSE); ?>
                            </a>
                        </div>
                    </div>
                </div><!--menu-mobile-->
            </div>
            <!-----------------end-menu-category-product-mobile-------------------->

            <?php if (!$product_cat->cluster_name || !$products->product_name){ ?>
            <div class="clr inner">
                <div class="box-content">
                    <div class="layout-contain">
                        <div class="clr box_form">
                            <div class="topic">
                                <p class="title-page">Sorry, this page isn't available.</p>
                            </div>
                            <div class="clr box-success" style="text-align: center;margin-top:50px;">
                                <p style="font-size: 16px;font-weight: 700;margin-bottom: 40px;">The link you followed
                                    may be broken, or the page may have been removed.</p>
                                <h3 style="font-size:98px;font-weight: 700;font-family: 'Oswald', sans-serif;color:#004ea1;">
                                    404 </h3>
                                <p style="font-size: 35px;font-weight: 700;margin-bottom:30px;">PAGE NOT FOUND</p>
                            </div>
                        </div>
                    </div><!--layout-contain-->
                </div><!--inner-->
                <?php } else { ?>
                    <div class="box-content">
                        <div class="clr box-detail-product">
                            <div class="left">
                                <div class="gallery-container">
                                    <div id="gallery" class="gallery-images">
                                        <?php
                                        $product_images = explode(',', $products->image);
                                        for ($i = 0; $i < count($product_images); $i++) {
                                            ?>
                                            <img src="<?php echo base_url("uploads/product_image/$product_images[$i]"); ?>"
                                                 alt=""/>
                                        <?php } ?>
                                    </div>
                                    <div class="gallery-thumbs-container">
                                        <ul id="gallery-thumbs" class="gallery-thumbs-list">
                                            <?php for ($i = 0; $i < count($product_images); $i++) { ?>
                                                <li class="thumb-item">
                                                    <div class="thumb pager-active">
                                                        <a href=""><img
                                                                    src="<?php echo base_url("uploads/product_image/$product_images[$i]"); ?>"
                                                                    alt=""/></a>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clr vdo">
                                    <div class="videoWrapper">
                                        <iframe width="560" height="315" src="<?php echo $products->youtube; ?>"
                                                frameborder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div><!--left-->
                            <div class="right">
                                <div class="detail-product">
                                    <h1><?php echo $products->product_name; ?></h1>
                                    <p class="id">Product ID : <?php echo $products->product_id; ?></p>
                                    <?php echo $products->item_description; ?>
                                    <div class="box-detail02">
                                        <p class="head"><?php echo $this->lang->line('page_product_detail_contents', FALSE); ?></p>
                                        <p><?php echo $products->content; ?></p>
                                        <p style="color: red;font-size: 0.8em;margin-top: 10px;line-height: 1.6em;"><?php echo $products->caution; ?></p>
                                    </div>
                                </div>
                                <div class="clr price01">
                                    <ul class="clr">
                                        <?php if ($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')))): ?>
                                            <?php
                                            $user_country = $this->ion_auth->user()->row()->country;
                                            // if($user_country == 'JapanA' || $user_country == 'JapanB'){
                                            // 	$user_country = 'Japan';
                                            // }
                                            $this->db->where('product_country', $user_country);
                                            $this->db->where('product_id', $products->product_id);
                                            $product_domestic = $this->datacontrol_model->getRowData("product_$product_type");
                                            ?>
                                            <?php
                                            if ($product_domestic->is_active == 1) :
                                                if ($nation_lang->is_active == 1) :
                                                    ?>
                                                    <li>
                                                        <div class="clr p-amount">
                                                            <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
                                                            <div class="price">
                                                                <?php echo number_format($product_domestic->sa_price); ?>
                                                                Yen
                                                            </div>
                                                        </div>
                                                        <?php if (!empty($products->import_shipping)): ?>
                                                            <div class="row"><span
                                                                        class="import"><?php echo $this->lang->line('page_product_detail_import_shipping', FALSE); ?></span><span
                                                                        class="tax"><?php echo @number_format($products->import_shipping); ?> Yen</span>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if (!empty($products->import_tax)): ?>
                                                            <div class="row"><span
                                                                        class="import"><?php echo $this->lang->line('page_product_detail_import_duty', FALSE); ?></span><span
                                                                        class="tax"><?php echo @number_format($products->import_tax); ?> Yen</span>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="amount01">
												<span>
													<?php echo $this->lang->line('page_product_detail_number', FALSE); ?>
													<select id="domestic_no">
														<option value="1">1</option>
														<?php if ($cat_1->cluster_url != 'Sample') { ?>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        <?php } ?>
													</select>
												</span>
                                                            <div class="col-cart">
                                                                <?php if (empty($cart['oversea'])): ?>
                                                                    <a href="javascript:void(0)"
                                                                       class="b-cart add_cart cart_domestic"
                                                                       onclick="add_cart('domestic','<?php echo $product_domestic->id; ?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?>
                                                                        <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/i-cart.png"></a>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endif; endif; ?>
                                        <?php if (($this->ion_auth->in_group(array('SA')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB'))) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')))): ?>
                                            <li>
                                                <div class="clr p-amount">
                                                    <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
                                                    <div class="price">
                                                        <?php echo number_format($products->global_price); ?> Yen
                                                    </div>
                                                </div>
                                                <div class="amount01">
												<span>
													<?php echo $this->lang->line('page_product_detail_number', FALSE); ?>
													<select id="oversea_no">
														<option value="1">1</option>
                                                        <?php if ($cat_1->cluster_url != 'Sample') { ?>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        <?php } ?>
													</select>
												</span>
                                                    <div class="col-cart">
                                                        <?php if (empty($cart['domestic'])): ?>
                                                            <a href="javascript:void(0)"
                                                               class="b-cart add_cart cart_oversea"
                                                               onclick="add_cart('oversea','<?php echo $products->id; ?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?>
                                                                <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/i-cart.png"></a>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="clr"></div>

                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="added">
                                        <p><?php echo $this->lang->line('page_product_detail_added', FALSE); ?></p>
                                        <p class="added-caution"><?php echo $this->lang->line('page_product_detail_added_caution', FALSE); ?></p>
                                    </div>
                                </div>
                                <div class="addtowishlist">
                                    <?php if ($this->ion_auth->logged_in()) : ?>
                                        <span id="not_add_wishlist"><?php echo $this->lang->line('page_product_detail_add_your_produts_to', FALSE); ?> <a
                                                    href="javascript:void(0);" class="wishlist add-wishlist"
                                                    data-id="<?php echo $products->id; ?>"><!-- &nbsp;<i class="far fa-heart"></i>  --><?php echo $this->lang->line('page_product_detail_wishlist', FALSE); ?></a></span>
                                        <span id="added_wishlist"
                                              style="display:none; color:green;">Added wishlist</span>

                                    <?php else: ?>
                                        <span><a href="" data-src="#modal01" class="login">
			                          <?php echo $this->lang->line('page_cart_add_to_wishlist_login', FALSE); ?>
			                          <span class="wishlist"><?php echo $this->lang->line('page_product_detail_wishlist', FALSE); ?></span>
			                        </a></span>
                                    <?php endif; ?>
                                </div>
                                <div class="product-information">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td> <?php echo $this->lang->line('page_product_detail_origin', FALSE); ?> </td>
                                                <td>
                                                    Japan
                                                    <?php //echo $products->product_country;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> <?php echo $this->lang->line('page_product_detail_list_price', FALSE); ?> </td>
                                                <td>
                                                    <?php echo number_format($products->global_price); ?> Yen
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> <?php echo $this->lang->line('page_product_detail_no_of_use', FALSE); ?> </td>
                                                <td>
                                                    <?php echo number_format($products->no_of_use); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td> <?php echo $this->lang->line('page_product_detail_cost_per_car', FALSE); ?> </td>
                                                <td>
                                                    <?php
                                                    $cost_per_car = $products->global_price / ($products->no_of_use == 0 ? 1 : $products->no_of_use);
                                                    echo number_format($cost_per_car);
                                                    ?> Yen
                                                </td>
                                            </tr>
                                            <!-- <tr>
											<td> <?php echo $this->lang->line('page_product_detail_right_side_minimum_quantity', FALSE); ?> </td>
											<td>
											1
										</td>
									</tr> -->
                                            <!--
									<tr>
										<td> <?php echo $this->lang->line('page_product_detail_shipping_method', FALSE); ?> </td>
										<td>
											Domestic, Oversea
										</td>
									</tr>
									<tr>
										<td> <?php echo $this->lang->line('page_product_detail_payment_method', FALSE); ?> </td>
										<td>
											Paypal, Amazon, Alipay
										</td>
									</tr>
-->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div><!--right-->
                        </div><!--box-detail-product-->
                        <div class="clr box-about-product">
                            <?php
                            // $this->db->where('country', $user_lang);
                            $this->db->where('id', $products->cat_2);
                            $cluster = $this->datacontrol_model->getRowData('cluster2');

                            $this->db->where('country', $user_lang);
                            $this->db->where('cluster_name', $cluster->cluster_name);
                            $cluster2 = $this->datacontrol_model->getRowData('cluster2');

                            ?>
                            <?php echo htmlspecialchars_decode($cluster2->html_content); ?>
                        </div>
                        <?php
                        if (($this->ion_auth->in_group(array('FC')) && in_array($user_country, array('Japan', 'JapanA', 'JapanB')))) {
                            if ($product_type == 'ppf') { ?>
                                <div class="clr box-contact-product ">
                                    <h3 class="text-center mb-2 contact-header">カットフィルム照合用お問い合わせ</h3>
                                    <p class="text-center">当商品がお客様の車に利用できるかどうかを確認されたい場合は、</p>
                                    <p class="text-center">下記のフォームに必要情報を入力の上、送信ボタンを押してください</p>
                                    <br>
                                    <div class="box-center">
                                        <form action="<?php echo base_url('page/send_mail_ppf') ?>" method="post"
                                              enctype="multipart/form-data" id="uploadForm">
                                            <div class="form-group row">
                                                <label for="staticEmail" class="col-sm-2 col-form-label">商品名
                                                </label>
                                                <div class="col-sm-10 col-form-label">
                                                    <p><?php echo $products->product_name; ?></p>
                                                    <input type="hidden" name="product_name"
                                                           value="<?php echo $products->product_name; ?>">
                                                    <input type="hidden" name="country"
                                                           value="<?php echo $this->ion_auth->user()->row()->company_name; ?>">
                                                    <input type="hidden" name="email"
                                                           value="<?php echo $this->ion_auth->user()->row()->username; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">URL</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <p><?php echo current_url(); ?></p>
                                                    <input type="hidden" name="full_url"
                                                           value="<?php echo current_url(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">グレード</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <input type="text" class="form-control" name="grade" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">初年度登録</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <input type="text" class="form-control" name="regis_year" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">型式</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <input type="text" class="form-control" name="regis_type" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">正面画像
                                                    (最大7MB)</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <input type="file" name="profile_image" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-2 col-form-label">斜め画像
                                                    (最大7MB)</label>
                                                <div class="col-sm-10 col-form-label">
                                                    <input type="file" name="side_photo" required>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <button style="margin: auto;border-radius: 20px;padding: 3px 20px 3px 20px;display: block;"
                                                    class="b-cart cart_oversea" type="submit">
                                                送信する
                                            </button>
                                            <br>
                                        </form>
                                    </div>
                                </div>
                            <?php }
                        } ?>

                    </div><!--box-content-->
                <?php } ?>
                <aside class="col-right" id="cart-target">
                    <div id="product_detail_cart">
                        <?php echo $this->load->view('page/product/product_detail_cart_v', array('cart_next_page' => 'page/cart')); ?>
                    </div>
                    <div class="box-recent-items">
                        <h3><?php echo $this->lang->line('page_product_detail_right_side_recently', FALSE); ?></h3>
                        <ul>
                            <?php foreach ($recent_product as $item): ?>
                                <?php $product_images = explode(',', $item->image); ?>
                                <li>
                                    <a href="<?php echo base_url("page/product_detail/$product_type/$item->product_id"); ?>">
                                        <figure><img
                                                    src="<?php echo base_url("uploads/product_image/$product_images[0]"); ?>">
                                        </figure>
                                        <p class="name-p">
                                            <?php echo $item->product_name; ?>
                                        </p>
                                    </a>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                    <div class="product-cart-list">
                        <div class="p-shipping">
                            <p class="name-p">
                                <?php echo $products->product_name; ?>
                            </p>
                            <?php if ($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')))): ?>
                                <?php if ($nation_lang->is_active == 1) : ?>
                                    <div class="price-shipping">
                                        <div class="clr p-amount">
                                            <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
                                            <div class="price"><?php echo number_format($products->sa_price); ?>Yen
                                            </div>
                                        </div>
                                        <ul class="list-items">
                                            <?php if (!empty($products->import_shipping)): ?>
                                                <li>
                                                    <p><?php echo $this->lang->line('page_product_detail_import_shipping', FALSE); ?></p>
                                                    <span><?php echo @number_format($products->import_shipping); ?> Yen</span>
                                                </li>
                                            <?php endif; ?>
                                            <?php if (!empty($products->import_tax)): ?>
                                                <li>
                                                    <p><?php echo $this->lang->line('page_product_detail_import_duty', FALSE); ?></p>
                                                    <span><?php echo @number_format($products->import_tax); ?>  Yen</span>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="line-btn">
                                        <?php if ($this->ion_auth->logged_in() && !$this->ion_auth->in_group(array('SA'))): ?>
                                            <a href="javascript:void(0)" class="b-cart cart_domestic"
                                               onclick="add_cart('domestic','<?php echo $product_domestic->id; ?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?>
                                                <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/i-cart.png"></a>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php if (($this->ion_auth->in_group(array('SA')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB'))) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')))): ?>
                                <div class="price-shipping">
                                    <div class="clr p-amount">
                                        <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
                                        <div class="price"><?php echo number_format($products->global_price); ?>Yen
                                        </div>
                                    </div>
                                </div>
                                <div class="line-btn" style="border-radius:0px 0px 15px 15px;">
                                    <?php if (empty($cart['domestic'])): ?>
                                        <a href="javascript:void(0)" class="b-cart cart_oversea"
                                           onclick="add_cart('oversea','<?php echo $products->id; ?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?>
                                            <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/i-cart.png"></a>
                                    <?php endif; ?>
                                    <p class="added"><?php echo $this->lang->line('page_product_detail_added', FALSE); ?></p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </aside>
            </div><!--inner-->
        </div><!--container-->
    </div>
    <!-- .wrapper -->
    <script src="https://unpkg.com/file-upload-with-preview@4.0.2/dist/file-upload-with-preview.min.js"></script>
    <script src="<?php echo base_url("assets/sensha-theme/"); ?>js/main.js"></script>
    <?php $this->load->view('footer_v'); ?>
    <script>
        function add_cart(type, id) {
            var amount = 0;
            if (type == 'domestic') {
                amount = $('#domestic_no').val();
                $('.cart_oversea').hide();
            }
            if (type == 'oversea') {
                amount = $('#oversea_no').val();
                $('.cart_domestic').hide();
            }

            $.ajax({
                url: '<?php echo base_url("page/product/add_cart/");?>' + '<?php echo $product_type;?>/' + id + '/' + type + '/' + amount,
                type: 'GET',
                success: function (data) {
                    //console.log(data);
                    if (data == 'not_add_cart') {
                        alert("Adding an item into another cart is not allowed.");
                        setTimeout(location.reload.bind(location), 300);
                    } else {

                        $.ajax({
                            url: '<?php echo base_url("page/product/get_product_detail_cart_v");?>',
                            type: 'GET',
                            success: function (data) {
                                $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                                $(".mobile-buy-btn").css({"display": "block"});
                            }
                        });

                    }
                }
            });
            $('.added').hide().fadeIn();
        }

        function add_cart2(type, id) {
            var amount = 1;

            $.ajax({
                url: '<?php echo base_url("page/product/add_cart/");?>' + '<?php echo $product_type;?>/' + id + '/' + type + '/' + amount,
                type: 'GET',
                success: function (data) {
                    $.ajax({
                        url: '<?php echo base_url("page/product/get_product_detail_cart_v");?>',
                        type: 'GET',
                        success: function (data) {
                            $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                            $(".mobile-buy-btn").css({"display": "block"});
                        }
                    });
                }
            });
        }

        $(function () { // document ready
            if ($('.product-cart-list').length) { // make sure "#sticky" element exists
                var el = $('.product-cart-list');
                var stickyTop = $('.product-cart-list').offset().top; // returns number
                var stickyHeight = $('.product-cart-list').height();

                $(window).scroll(function () { // scroll event
                    var limit = $('#footer').offset().top - stickyHeight - 20;

                    var windowTop = $(window).scrollTop(); // returns number

                    if (stickyTop < windowTop) {
                        el.css({
                            position: 'fixed',
                            top: 20
                        });
                    } else {
                        el.css('position', 'static');
                    }

                    if (limit < windowTop) {
                        var diff = limit - windowTop;
                        el.css({
                            top: 20
                        });
                    }
                });
            }

        });
    </script>
    <script>
        function sticktothetop() {
            var window_top = $(window).scrollTop();
            var top = $('#stick-here').offset().top;
            if (window_top > top) {
                $('#stickThis').addClass('stick');
                $('#stick-here').height($('#stickThis').outerHeight());
            } else {
                $('#stickThis').removeClass('stick');
                $('#stick-here').height(0);
            }
        }

        $(function () {
            $(window).scroll(sticktothetop);
            sticktothetop();

            $('.add-wishlist').click(function () {
                var id = $(this).data('id');
                var sku = '<?php echo $product_id;?>';
                var product_type = '<?php echo $product_type;?>';
                $.ajax({
                    url: '<?php echo base_url("page/product/add_wishlist");?>',
                    type: 'POST',
                    data: {id: id, product_type: product_type, sku: sku},
                    success: function (data) {
                        console.log(data);
                        $('#added_wishlist').show();
                        $('#not_add_wishlist').hide();
                    }
                });
                return false;
            });
        });

        var FrontPhoto = new FileUploadWithPreview('FrontPhoto');
        var SidePhoto = new FileUploadWithPreview('SidePhoto');

    </script>
