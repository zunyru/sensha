<div class="clr product-list">
  <ul>
    <?php $i=0; foreach($products as $item):?>
      <?php $product_images = explode(',', $item->image);?>

      <li>
        <a href="<?php echo base_url("page/product_detail/$product_type/$item->product_id");?>">
          <figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>" alt=""/></figure>
          <div class="detail">
            <p class="name-p"><?php echo $item->product_name;?></p>
            <p class="quantity"><?php echo $item->caution;?></p>
            <?php if($this->ion_auth->logged_in() && $item->sa_price > 0 && !$this->ion_auth->in_group(array('SA'))):?>
            <div class="clr p-amount">
              <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
              <div class="price">
                <?php echo number_format($item->sa_price);?> Yen
              </div>
            </div>
          <?php endif;?>
            <div class="clr p-amount">
              <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
              <div class="price">
                <?php echo number_format($item->global_price);?> Yen
              </div>
            </div>
          </div>
        </a>
      </li>
    <?php $i++; endforeach;?>
  </ul>
</div>
<section id="pagination" class="row">
  <?php echo $links; ?>
  <!-- <ul>

    <li class="previous"><a href=""><img src="<?php echo base_url("assets/sensha-theme/");?>images/arrow-p.png"></a></li>
    <li class="active"><span>1</span></li>
    <li class="number"><a href="">2</a></li>
    <li class="number"><a href="">3</a></li>
    <li class="number"><a href="">4</a></li>
    <li class="number"><a href="">5</a></li>
    <li class="next"><a href=""><img src="<?php echo base_url("assets/sensha-theme/");?>images/arrow-next.png"></a></li>
  </ul> -->
</section>
