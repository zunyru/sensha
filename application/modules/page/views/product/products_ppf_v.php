<!DOCTYPE HTML>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php echo $htmlTitle; ?></title>
	<link rel="canonical" href="<?php echo base_url(uri_string()); ?>" />
	<meta name="description" content="<?php echo $htmlDes; ?>">
	<meta name="keywords" content="">
	<?php if (!$this->ion_auth->logged_in()) : ?>
		<?php
		$uri = uri_string();
		if ($this->coutry_iso != '') {
			$uri = str_replace("$this->coutry_iso", "", $uri);
		}

		$this->db->where('is_active', 1);
		$nation_lang = $this->datacontrol_model->getAllData('nation_lang');
		?>
		<?php foreach ($nation_lang as $item) : ?>
			<?php
			$this->db->where('nicename', $item->country);
			$c = $this->datacontrol_model->getRowData('countries');
			?>

			<?php if ($item->country == "Global") : ?>
				<link rel="alternate" href="<?php echo base_url($uri); ?>" hreflang="en" />
			<?php else : ?>
				<link rel="alternate" href="<?php echo base_url(strtolower($c->iso) . $uri); ?>" hreflang="<?php echo strtolower($c->iso); ?>" />
			<?php endif; ?>

		<?php endforeach; ?>
	<?php else : ?>
		<link rel="alternate" href="<?php echo base_url(uri_string()); ?>" hreflang="<?php echo $this->coutry_iso; ?>" />
	<?php endif; ?>

	<?php if ($noindex) : ?>
		<meta name=”robots” content="noindex">
	<?php endif; ?>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/animation.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/home.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/common.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/page.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/menu-mobile.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
	<!--[if lt IE 9]-->
	<script src="https://www.at-aroma.com/js/common/html5.js"></script>
	<!--[endif]-->
</head>

<body class>
	<div id="wrapper">
		<?php $this->load->view('header_v'); ?>
		<div class="container">
			<div class="clr inner">
				<div id="breadcrumbs">
					<span><a href="<?php echo base_url("$coutry_iso"); ?>">Home</a></span>
					<?php if (!$cluster1->cluster_name) { ?>
						<span>404 PRODUCT NOT FOUND</span>
					<?php } else { ?>
						<span><?php echo $cluster1->cluster_name; ?></span>
					<?php } ?>
				</div>
				<!-----------------menu-category-product-mobile------------------------>
				<div id="stick-here"></div>
				<div id="stickThis">
					<div class="menu-mobile">
						<header id="toggle">
							<h3><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/list.png"> Product Menu </h3><span class="arrow"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/chev.png"></span>
						</header>
						<div id="content">
							<aside class="col-left">
								<div class="search">
									<form method="post" action="">
										<input type="text" placeholder="Keyword Search" name="s" value="<?php echo (isset($_POST['s'])) ? $_POST['s'] : ''; ?>">
										<input type="hidden" name="cat_1" value="<?php echo $_POST['cat_1']; ?>">
										<input type="hidden" name="cat_2" value="<?php echo $_POST['cat_2']; ?>">
										<button type="submit"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-search.png" width="25"></button>
									</form>
								</div>
								<div class="box_menu">
									<div class="a_menu"> 
										<h3><?php echo $cluster1->cluster_name; ?></h3>
										<ul>
											<?php foreach ($sub_cluster as $item) : ?>
												<li><a href="<?php echo base_url("$coutry_iso" . "page/ppf/$cluster1->cluster_name/$item->cluster_name"); ?>/"><?php echo $item->cluster_name; ?></a></li>
											<?php endforeach; ?>
										</ul>
									</div>
									<form id="product_filter_mobile" method="post" action="<?php echo current_url(); ?>">
										<input type="hidden" name="cat_1" value="<?php echo $_POST['cat_1']; ?>">
										<input type="hidden" name="cat_2" value="<?php echo $_POST['cat_2']; ?>">
										<div class="a_menu m-checkbox">
											<h3>Parts</h3>
											<ul>
												<li><label><input type="radio" name="parts" value="headlight" <?php echo ($this->input->post("parts") == "headlight") ? "checked" : ""; ?>> headlight</label></li>
												<li><label><input type="radio" name="parts" value="f-bumper" <?php echo ($this->input->post("parts") == "f-bumper") ? "checked" : ""; ?>> f-bumper</label></li>
												<li><label><input type="radio" name="parts" value="r-bumper" <?php echo ($this->input->post("parts") == "r-bumper") ? "checked" : ""; ?>> r-bumper</label></li>
												<li><label><input type="radio" name="parts" value="bumper-set" <?php echo ($this->input->post("parts") == "bumper-set") ? "checked" : ""; ?>> bumper-set</label></li>
												<li><label><input type="radio" name="parts" value="luggage" <?php echo ($this->input->post("parts") == "luggage") ? "checked" : ""; ?>> luggage</label></li>
												<li><label><input type="radio" name="parts" value="doorcup" <?php echo ($this->input->post("parts") == "doorcup") ? "checked" : ""; ?>> doorcup</label></li>
											</ul>
										</div>
										<input type="hidden" name="product_filter" value="1" />
									</form>
								</div>
							</aside>
						</div>
					</div>
					<!--menu-mobile-->
				</div>
				<!-----------------end-menu-category-product-mobile------------------------>

				<aside class="col-left">
					<div class="search">
						<form method="get" action="">
							<input type="text" placeholder="Keyword Search" name="s" value="<?php echo (isset($_GET['s'])) ? $_GET['s'] : ''; ?>">
							<input type="hidden" name="cat_1" value="<?php echo $_GET['cat_1']; ?>">
							<input type="hidden" name="cat_2" value="<?php echo $_GET['cat_2']; ?>">
                            <input type="hidden" name="parts" value="<?php echo $_GET['parts']; ?>">
							<button type="submit"><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-search.png" width="25"></button>
						</form>
					</div>
					<div class="box_menu">
						<div class="a_menu">
							<h3><?php echo $cluster1->cluster_name; ?></h3>
							<ul>
								<?php foreach ($sub_cluster as $item) : ?>
									<li><a href="<?php echo base_url("$coutry_iso" . "page/ppf/$cluster1->cluster_name/$item->cluster_name"); ?>/"><?php echo $item->cluster_name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<form id="product_filter" method="get" action="<?php echo current_url(); ?>">
							<input type="hidden" name="cat_1" value="<?php echo $_POST['cat_1']; ?>">
							<input type="hidden" name="cat_2" value="<?php echo $_POST['cat_2']; ?>">
							<div class="a_menu m-checkbox">
								<h3>Parts</h3>
								<ul>
									<li><label><input type="radio" name="parts" value="headlight" <?php echo ($this->input->get("parts") == "headlight") ? "checked" : ""; ?>> headlight</label></li>
									<li><label><input type="radio" name="parts" value="f-bumper" <?php echo ($this->input->get("parts") == "f-bumper") ? "checked" : ""; ?>> f-bumper</label></li>
									<li><label><input type="radio" name="parts" value="r-bumper" <?php echo ($this->input->get("parts") == "r-bumper") ? "checked" : ""; ?>> r-bumper</label></li>
									<li><label><input type="radio" name="parts" value="bumper-set" <?php echo ($this->input->get("parts") == "bumper-set") ? "checked" : ""; ?>> bumper-set</label></li>
									<li><label><input type="radio" name="parts" value="luggage" <?php echo ($this->input->get("parts") == "luggage") ? "checked" : ""; ?>> luggage</label></li>
									<li><label><input type="radio" name="parts" value="doorcup" <?php echo ($this->input->get("parts") == "doorcup") ? "checked" : ""; ?>> doorcup</label></li>
								</ul>
							</div>
							<input type="hidden" name="product_filter" value="1" />
						</form>
					</div>
				</aside>
				<div class="content">
					<div class="headline">
						<p class="title"><?php echo $this->lang->line('page_filter_search_title', FALSE); ?></p>
						<h1 style="text-align:left;"><?php echo $this->lang->line('page_filter_search_subtitle', FALSE); ?></h1>
						<product>
							<div class="clr product-list">
								<ul>
									<?php $i = 0;
									foreach ($products as $item) : ?>
										<?php $product_images = explode(',', $item->image); ?>

										<li>
											<a href="<?php echo base_url("$coutry_iso" . "page/product_detail/$product_type/$item->product_id"); ?>">
												<figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]"); ?>" alt="" /></figure>
												<div class="detail">
													<p class="name-p"><?php echo $item->product_name; ?></p>
													<?php if ($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')))) : ?>
														<?php
														$this->db->where('product_id', $item->product_id);
														$this->db->where('product_country', $this->user_lang);
														$product_domestic = $this->datacontrol_model->getRowData('product_ppf');
														?>
														<?php if ($product_domestic->is_active == 1) :
																if ($nation_lang->is_active == 1) : 
														 ?>
															<div class="clr p-amount">
																<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
																<div class="price">
																	<?php echo number_format($product_domestic->sa_price); ?> Yen
																</div>
															</div>
														<?php endif; endif; ?>
													<?php endif; ?>
													<?php if ($this->ion_auth->in_group(array('SA')) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')))) : ?>
														<div class="clr p-amount">
															<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
															<div class="price">
																<?php echo number_format($item->global_price); ?> Yen
															</div>
														</div>
													<?php endif; ?>
												</div>
											</a>
										</li>
									<?php $i++;
									endforeach; ?>
								</ul>
							</div>
							<section id="pagination" >
								<?php echo $links; ?>
								<!-- <ul>

									<li class="previous"><a href=""><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/arrow-p.png"></a></li>
									<li class="active"><span>1</span></li>
									<li class="number"><a href="">2</a></li>
									<li class="number"><a href="">3</a></li>
									<li class="number"><a href="">4</a></li>
									<li class="number"><a href="">5</a></li>
									<li class="next"><a href=""><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/arrow-next.png"></a></li>
								</ul> -->
							</section>
						</product>

					</div>
					<div>
					</div>
				</div>
			</div>
			<!--inner-->
		</div>
		<script src="<?php echo base_url("assets/sensha-theme/"); ?>js/main.js"></script>
		<?php $this->load->view('footer_v'); ?>
		<script>
			function sticktothetop() {
				var window_top = $(window).scrollTop();
				var top = $('#stick-here').offset().top;
				if (window_top > top) {
					$('#stickThis').addClass('stick');
					$('#stick-here').height($('#stickThis').outerHeight());
				} else {
					$('#stickThis').removeClass('stick');
					$('#stick-here').height(0);
				}
			}
			$(function() {
				$(window).scroll(sticktothetop);
				sticktothetop();

				$('#product_filter input').click(function() {
					/*var formData = new FormData($('#product_filter')[0]);
					$.ajax({
							method: "POST",
							url: $('#product_filter').attr('action'),
							data: formData,
							cache: false,
							contentType: false,
							processData: false,
						})
						.done(function(msg) {
							// console.log(msg);
							$('product').html(msg);
							// r = JSON.parse(msg);
							// console.log(r);

						})
						.fail(function(msg) {
							console.log(msg);
						});*/
                    $('#product_filter').attr('action', $(this).attr('href'));
                    $('#product_filter').submit();
				});

				$('#product_filter_mobile input').click(function() {
					/*var formData = new FormData($('#product_filter_mobile')[0]);

					$.ajax({
							method: "POST",
							url: $('#product_filter_mobile').attr('action'),
							data: formData,
							cache: false,
							contentType: false,
							processData: false,
						})
						.done(function(msg) {
							 console.log(msg);
							$('product').html(msg);
							// r = JSON.parse(msg);
							// console.log(r);

						})
						.fail(function(msg) {
							console.log(msg);
						});*/
                    $('#product_filter_mobile').attr('action', $(this).attr('href'));
                    $('#product_filter_mobile').submit();
				});



			});
		</script>
	</div>
	<!-- .wrapper -->