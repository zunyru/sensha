<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="<?php echo base_url("");?>">Home</a></span><span><a href="<?php echo base_url("page/news");?>">News</a></span><span><?php echo $news_post->title;?></span>
    </div>
  </div><!--inner-->
  <div class="clr inner">
    <div class="clr single-page">
      <div class="topic none">
        <h2><?php echo $news_post->title;?></h2>
        <span class="date"><?php echo date('d.m.Y', strtotime($news_post->create_date));?></span>
      </div>
      <?php echo htmlspecialchars_decode($news_post->content);?>
    </div><!--layout-contain-->
  </div><!--inner-->
</div><!--container-->
