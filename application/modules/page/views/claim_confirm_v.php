<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="index.php">Home</a><span><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a><span><?php echo $this->lang->line('breadcrumb_delivery_claim_confirm', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="layout-contain">
			<div class="clr box_form">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_delivery_claim_confirm_order_number', FALSE); ?>: <?php echo $this->input->post('shipment_id');?></p>
				</div>
				<div class="box-inner">
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_order_history_shipment_id', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('shipment_id');?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="far fa-calendar-alt"></i> <?php echo $this->lang->line('page_order_history_data_of_order', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $order->sale_date;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="far fa-address-card"></i> <?php echo $this->lang->line('page_order_history_delivery_address', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $shiping_info->name;?></p>
							<p class="f500"><?php echo "$shipping_info->address $shipping_info->zipcode $shipping_info->country";?> <br>Tel : <?php echo $shipping_info->phone;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="fas fa-file-alt"></i> <?php echo $this->lang->line('page_delivery_claim_confirm_order_number', FALSE); ?></label>
						<div class="r-input">
							<?php foreach($item_list as $product_item):?>
								<div class="confirm">
									<p class="f500"><?php echo $product_item->product_name;?></p>
									<p style="margin-top:10px;"><?php echo $this->lang->line('page_order_history_amount', FALSE); ?>: <?php echo $product_item->item_amount;?></p>
									<p class="txt-red"><?php echo $this->lang->line('page_order_history_sub_total', FALSE); ?>: <?php echo number_format($product_item->item_sub_total_amount, 2);?> Yen</p>
								</div>
							<?php endforeach;?>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="far fa-clipboard"></i> <?php echo $this->lang->line('page_order_history_total', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo number_format($order->sale_total_amount, 2);?> Yen</p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="fas fa-truck"></i> <?php echo $this->lang->line('page_delivery_claim_confirm_delivery_type', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $order->buy_type;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/10.png"> <?php echo $this->lang->line('page_order_history_delivery_mothod', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $order->shipping_method;?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_delivery_claim_email', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('email');?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="fas fa-cubes"></i> <?php echo $this->lang->line('page_delivery_claim_confirm_reason', FALSE); ?></label>
						<div class="r-input">
							<p><?php echo $this->input->post('reason');?></p>
						</div>
					</div>
					<div class="r-inline">
						<label class="label"><i class="fas fa-exclamation-triangle"></i> <?php echo $this->lang->line('page_delivery_claim_confirm_claim', FALSE); ?></label>
						<div class="r-input">
							<p class="f500"><?php echo $this->input->post('comment');?></p>
							</div>
						</div>
						<div class="line-checkbox">
							<!--<input type="checkbox" id="01" name="selector">
							<label style="font-weight:600;color:#000;">
								<?php echo $this->lang->line('page_confirm_claim_check_box', FALSE); ?>
							</label>-->
						</div>
						<div class="row-btn">
							<a href="<?php echo base_url("page/claim_completed");?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_confirm_claim_confirm', FALSE); ?></a>
						</div>
					</div>
				</div>
			</div><!--layout-contain-->
		</div><!--inner-->
	</div><!--container-->
	<script src="js/main.js"></script>
