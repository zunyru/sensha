<?php
	//nobu
$cart = $this->session->userdata('sensha_cart');
$domestic_item = 0;
$domestic_price = 0;
$domestic_total = 0;
$domestic_import_shipping = 0;
$domestic_import_tax = 0;
if(is_array($cart['domestic'])){
  foreach ($cart['domestic'] as $key => $value) {
    $domestic_item += $cart['domestic'][$key]['item_amount'];
    $domestic_price += ($cart['domestic'][$key]['price'] * $cart['domestic'][$key]['item_amount']);
    $domestic_import_shipping = $cart['domestic'][$key]['import_shipping'];
    $domestic_import_tax = $cart['domestic'][$key]['import_tax'];
  }
}
$domestic_total = $domestic_price+$domestic_import_shipping+$domestic_import_tax;

$oversea_item = 0;
$oversea_price = 0;
$oversea_total = 0;
if(is_array($cart['oversea'])){
  foreach ($cart['oversea'] as $key => $value) {
    $oversea_item += $cart['oversea'][$key]['item_amount'];
    $oversea_price += ($cart['oversea'][$key]['price'] * $cart['oversea'][$key]['item_amount']);
  }
}
$oversea_total = $oversea_price;
$user_country = $this->ion_auth->user()->row()->country;
?>

<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_cart', FALSE); ?></span>
    </div>
    <!-----------------menu-category-product-mobile------------------------>
    <div id="stick-here"></div>
    <div id="stickThis">
      <div class="menu-mobile">
        <div class="clr m-inner">
          <h3><i class="fas fa-clipboard-list" style="font-size:17px;margin-right: 5px;"></i> <?php echo $oversea_item + $domestic_item;?> <?php echo $this->lang->line('page_product_detail_right_side_items', FALSE); ?></h3>
          <div class="right">
            <a href="<?php echo base_url('page/place_delivery');?>" style="background:#f9bd00;"><i class="fas fa-money-bill-alt"></i>&nbsp; <?php echo $this->lang->line('page_product_detail_right_buy_now', FALSE); ?></a>
          </div>
        </div>
      </div><!--menu-mobile-->
    </div>
    <!-----------------end-menu-category-product-mobile-------------------->
    <div class="box-content">
      <?php if(isset($cart['domestic']) && !empty($cart['domestic'])):?>
      <div class="box-domestic">
        <div class="title-cart">
          <ul>
            <li>
              <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
            </li>
            <li><span class="txt-b1"><?php echo $this->lang->line('page_cart_shopping_cart', FALSE); ?></span></li>
            <li><span class="txt-b2"><?php echo $this->lang->line('page_cart_quantity', FALSE); ?></span></li>
            <li><span class="txt-b2"><?php echo $this->lang->line('page_cart_price', FALSE); ?></span></li>
          </ul>
        </div>
        <div class="clr list-cart">
          <ul>
            <?php foreach ($cart['domestic'] as $key => $value):?>
              <?php
              $product_images = explode(',', $cart['domestic'][$key]['image']);
              $product_type = $cart['domestic'][$key]['product_type'];
              ?>
              <li>
                <div class="clr c1">
                  <div class="pic-product">
                    <a href="<?php echo base_url("page/product_detail/$product_type/").$cart['domestic'][$key]['product_id'];?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
                  </div>
                </div>
                <div class="clr c2">
                  <div class="product-name">
                    <h1 class="name-p"><?php echo $cart['domestic'][$key]['product_name'];?></h1>
                    <div class="available">
                      <p><?php echo $this->lang->line('page_cart_available', FALSE); ?></p>
                       <?php if ($this->ion_auth->logged_in()) : ?>
                      <span><a href="javascript:void(0);" class="wishlist add-wishlist" data-id="<?php echo $cart['domestic'][$key]['id'];?>"> &nbsp;<i class="far fa-heart"></i>  <?php echo $this->lang->line('page_cart_add_to_wishlist', FALSE); ?></a></span> | <span><a href="" onclick="delete_cart('domestic','<?php echo $cart['domestic'][$key]['id'];?>'); return false;"><?php echo $this->lang->line('page_cart_delete', FALSE); ?></a></span>
                      <?php else: ?>
                        <span><a href="" data-src="#modal01" class="login">
                                <?php echo $this->lang->line('page_cart_add_to_wishlist_login', FALSE); ?>
                                <span class="wishlist"><?php echo $this->lang->line('page_product_detail_wishlist', FALSE); ?></span>
                          </a></span>
                      <?php endif; ?>  
                    </div>
                  </div>
                  <div class="amount">
                    <p>
                      <?php echo $cart['domestic'][$key]['item_amount'];?>
                    </p>
                  </div>
                  <div class="box-price">
                    <p class="txt-red"><?php echo number_format($cart['domestic'][$key]['price']);?> Yen</p>
                    <?php if(!empty($cart['domestic'][$key]['import_shipping'])):?>
                      <div class="row">
                       <span class="import"><?php echo $this->lang->line('page_product_detail_import_shipping', FALSE); ?></span>
                       <span class="tax"><?php echo number_format($cart['domestic'][$key]['import_shipping'] * $cart['domestic'][$key]['item_amount']);?> Yen</span>
                     </div>
                   <?php endif;?>
                   <?php if(!empty($cart['domestic'][$key]['import_tax'])):?>
                    <div class="row">
                     <span class="import"><?php echo $this->lang->line('page_product_detail_import_duty', FALSE); ?></span>
                     <span class="tax"><?php echo number_format($cart['domestic'][$key]['import_tax'] * $cart['domestic'][$key]['item_amount']);?> Yen</span>
                   </div>
                 <?php endif;?>
               </div>
             </div>
           </li>
         <?php endforeach;?>
       </ul>
       <div class="clr subtotal">
        <span><?php echo $domestic_item?> <?php echo $this->lang->line('page_cart_items_subtotal', FALSE); ?></span>
        <p class="txt-red"><?php echo number_format($domestic_price);?> Yen</p>
      </div>
    </div>
    <div class="clr box-select-delivery">
      <div class="d1">
        <span class="b-domestic"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
        <span class="txt-delivery"><?php echo $this->lang->line('page_cart_delivery', FALSE); ?></span>
      </div>
      <div class="clr select-delivery">
<?php
      $user_country = $this->ion_auth->user()->row()->country;
      if(!$user_country||(($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB')&&!$shipping_information->zipcode)){
?>
      <div class="clr box-shipping-address">
        <div class="left">
          <h3><?php echo $this->lang->line('page_place_delivery_please_update_delivery_address', FALSE); ?></h3>
        </div>
          <div class="right">
            <div class="row">
            <a href="<?php echo base_url("page/user/edit_shipping_info?redirect=cart&iso=$this->coutry_iso");?>" class="btn-gray"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" width="31" style="width: 20px;margin-right: 10px;">
              <?php echo $this->lang->line('page_place_delivery_change_address', FALSE); ?>
            </a>
          </div>
        </div>
      </div>
<?php      
      }else{
?>
        <ul>
          <?php
          $fixed_price = 0;
          if($delivery_domestic->fixed_type == 'fixed'){
            $fixed_price = $delivery_domestic->fixed_amount;
          }
          else{

          }
          if($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB'){
            $fixed_price  = $japan_delivery_fee;
          }
          ?>
          <li onclick="add_shipping('domestic', '<?php echo $this->lang->line('page_cart_fix_price', FALSE); ?>', <?php echo $fixed_price;?>,'<?php echo $domestic_price;?>')">
            <input type="radio" id="domestic-shipping1" name="domestic_delivery" value="Fix Price">
            <p data-value="ppf_price" style="display: none;"><?=$ppf_price?></p>
            <p data-value="general_price" style="display: none;"><?=$general_price?></p>
            <label for="domestic-shipping1">
            
              <p><?php echo $this->lang->line('page_cart_fix_price', FALSE); ?></p>
             
              <!-- <span>Appomixately 3-4 days</span> -->
              <?php if($payment_mode != 'bank_trnsfer' && $payment_mode != 'no_zone'): ?>
              <p class="txt-red"><?php echo number_format($fixed_price);?> Yen</p>
               <?php endif; ?>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
                <p class="txt-red"><?php echo $this->lang->line('page_cart_estimates', FALSE); ?></p>
              <?php endif; ?>
            </label>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
            <span style="font-size: 0.8em; line-height: 1.3em; display: block; padding-left: 10px;"><?php echo $this->lang->line('page_cart_estimates_notice', FALSE); ?></span>
              <?php endif; ?>
          </li>
        </ul>
<?php }// end if(!$user_country||(($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB')&&!$shipping_information->zipcode)) ?>
      </div>
    </div>
  </div><!--box-domestic-->
<?php endif;?>
<?php if(isset($cart['oversea']) && !empty($cart['oversea'])):?>
<div class="box-oversea">
  <div class="title-cart">
    <ul>
      <li>
        <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
      </li>
      <li><span class="txt-b1"><?php echo $this->lang->line('page_cart_shopping_cart', FALSE); ?></span></li>
      <li><span class="txt-b2"><?php echo $this->lang->line('page_cart_quantity', FALSE); ?></span></li>
      <li><span class="txt-b2"><?php echo $this->lang->line('page_cart_price', FALSE); ?></span></li>
    </ul>
  </div>
  <div class="clr list-cart">
    <ul>
      <?php foreach ($cart['oversea'] as $key => $value):?>
        <?php
        $product_images = explode(',', $cart['oversea'][$key]['image']);
        $product_type = $cart['oversea'][$key]['product_type'];
        ?>
        <li>
          <div class="clr c1">
            <div class="pic-product">
              <a href="<?php echo base_url("page/product_detail/$product_type/").$cart['oversea'][$key]['product_id'];?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
            </div>
          </div>
          <div class="clr c2">
            <div class="product-name">
              <h1 class="name-p"><?php echo $cart['oversea'][$key]['product_name'];?></h1>
              <div class="available">
                <p>Available</p>
                <?php if ($this->ion_auth->logged_in()) : ?>
                <span class="not_add_wishlist"><a href="javascript:void(0);" class="wishlist add-wishlist" data-id="<?php echo $cart['oversea'][$key]['id'];?>"> &nbsp;<i class="far fa-heart"></i>  <?php echo $this->lang->line('page_product_detail_wishlist', FALSE); ?></a></span> | <span><a href="" onclick="delete_cart('oversea','<?php echo $cart['oversea'][$key]['id'];?>'); return false;"><?php echo $this->lang->line('page_cart_delete', FALSE); ?></a></span>
                <?php else:?>
                  <span><a href="" data-src="#modal01" class="login">
                      <?php echo $this->lang->line('page_cart_add_to_wishlist_login', FALSE); ?>
                      <span class="wishlist"><?php echo $this->lang->line('page_product_detail_wishlist', FALSE); ?></span>
                    </a></span>
                     | <span><a href="" onclick="delete_cart('oversea','<?php echo $cart['oversea'][$key]['id'];?>'); return false;"><?php echo $this->lang->line('page_cart_delete', FALSE); ?></a></span>
                 <?php endif; ?> 
              </div>
            </div>
            <div class="amount">
              <p>
                <?php echo $cart['oversea'][$key]['item_amount'];?>
              </p>
            </div>
            <div class="box-price">
              <p class="txt-red"><?php echo number_format($cart['oversea'][$key]['price']);?> Yen</p>
            </div>
          </div>
        </li>
      <?php endforeach;?>
    </ul>
    <div class="clr subtotal">
      <span><?php echo $oversea_item?> <?php echo $this->lang->line('page_cart_items_subtotal', FALSE); ?></span>
      <p class="txt-red"><?php echo number_format($oversea_price);?> Yen</p>
    </div>
  </div>
  <div class="clr box-select-delivery">
    <div class="d1">
      <span class="b-domestic"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
      <span class="txt-delivery"><?php echo $this->lang->line('page_cart_delivery', FALSE); ?></span>
    </div>
    <div class="clr select-delivery">
<?php
      $user_country = $this->ion_auth->user()->row()->country;
      if(!$user_country||!$shipping_information->address){
?>
      <div class="clr box-shipping-address">
        <div class="left">
          <h3><?php echo $this->lang->line('page_place_delivery_please_update_delivery_address', FALSE); ?></h3>
        </div>
          <div class="right">
            <div class="row">
            <a href="<?php echo base_url("page/user/edit_shipping_info?redirect=cart&iso=$this->coutry_iso");?>" class="btn-gray"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" width="31" style="width: 20px;margin-right: 10px;">
              <?php echo $this->lang->line('page_place_delivery_change_address', FALSE); ?>
            </a>
          </div>
        </div>
      </div>
<?php      
      }else{
?>
      <ul>
        <?php
        $user_country = $this->ion_auth->user()->row()->country;
        if($user_country != 'Japan' && $user_country != 'JapanA' && $user_country != 'JapanB'){
          ?>
          <li onclick="add_shipping('oversea', '<?php echo $this->lang->line('page_order_history_ems', FALSE); ?>', '<?php echo $ems;?>', '<?php echo $oversea_price;?>')">
            <input type="radio" id="oversea-shipping1" name="oversea_delivery" value="EMS">
            <p data-value="ppf_price" style="display: none;"><?=$ppf_price_ems?></p>
            <p data-value="general_price" style="display: none;"><?=$general_price_ems?></p>
            <label for="oversea-shipping1">
            
              <p><?php echo $this->lang->line('page_order_history_ems', FALSE); ?></p>
              
              <!-- <span>Appomixately 3-4 days</span> -->
              <?php if($payment_mode != 'bank_trnsfer' && $payment_mode != 'no_zone'): ?>
              <p class="txt-red"><?php echo number_format($ems);?> Yen</p>
              <?php endif; ?>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
                <p class="txt-red"><?php echo $this->lang->line('page_cart_estimates', FALSE); ?></p>
              <?php endif; ?>
            </label>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
            <span style="font-size: 0.8em; line-height: 1.3em; display: block; padding-left: 10px;"><?php echo $this->lang->line('page_cart_estimates_notice', FALSE); ?></span>
              <?php endif; ?>
          </li>
          <?php if($this->ion_auth->in_group(array('SA', 'FC'))):?>
            <li onclick="add_shipping('oversea', '<?php echo $this->lang->line('page_order_history_exw', FALSE); ?>', 0, '<?php echo $oversea_price;?>')">
              <input type="radio" id="oversea-shipping2" name="oversea_delivery" value="EXW">
              <label for="oversea-shipping2">
                <p><?php echo $this->lang->line('page_order_history_exw', FALSE); ?></p>
                <!-- <span>Appomixately 3-4 days</span> -->
                <p class="txt-red"><?php echo $this->lang->line('page_cart_estimates', FALSE); ?></p>
              </label>
              <span style="font-size: 0.8em; line-height: 1.3em; display: block; padding-left: 10px;"><?php echo $this->lang->line('page_order_history_exw_notice', FALSE); ?></span>
            </li>
            <li onclick="add_shipping('oversea', '<?php echo $this->lang->line('page_order_history_cif', FALSE); ?>', 0, '<?php echo $oversea_price;?>')">
              <input type="radio" id="oversea-shipping3" name="oversea_delivery" value="CIF">
              <label for="oversea-shipping3">
                <p><?php echo $this->lang->line('page_order_history_cif', FALSE); ?></p>
                <!-- <span>Appomixately 3-4 days</span> -->
                <p class="txt-red"><?php echo $this->lang->line('page_cart_estimates', FALSE); ?></p>
              </label>
              <span style="font-size: 0.8em; line-height: 1.3em; display: block; padding-left: 10px;"><?php echo $this->lang->line('page_order_history_cif_notice', FALSE); ?></span>
            </li>
          <?php endif;?>
        <?php } //end if($user_country != 'Japan' ?>
        <?php
        if($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB'){
          $fixed_price  = $japan_delivery_fee;
          ?>
          <li onclick="add_shipping('oversea', '<?php echo $this->lang->line('page_cart_fix_price', FALSE); ?>', <?php echo $fixed_price;?>,'<?php echo $oversea_price;?>')">
            <input type="radio" id="oversea-shipping4" name="oversea_delivery" value="Fix Price">
            <p data-value="ppf_price" style="display: none;"><?=$ppf_price?></p>
            <p data-value="general_price" style="display: none;"><?=$general_price?></p>
            <label for="oversea-shipping4">
                <p><?php echo $this->lang->line('page_cart_fix_price', FALSE); ?></p>
                            <!-- <span>Appomixately 3-4 days</span> -->
              <?php if($payment_mode != 'bank_trnsfer' && $payment_mode != 'no_zone'): ?>
              <p class="txt-red"><?php echo number_format($fixed_price);?> Yen</p>
              <?php endif; ?>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
                <p class="txt-red"><?php echo $this->lang->line('page_cart_estimates', FALSE); ?></p>
              <?php endif; ?>
            </label>
              <?php if($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone'): ?>
            <span style="font-size: 0.8em; line-height: 1.3em; display: block; padding-left: 10px;"><?php echo $this->lang->line('page_cart_estimates_notice', FALSE); ?></span>
              <?php endif; ?>
          </li>
        <?php } ?>
      </ul>
<?php }// end if(!$user_country||!$shipping_information->address) ?>
    </div>

  </div>
</div><!--box-domestic-->
<?php endif;?>
</div><!--box-content-->
<aside class="col-right" id="cart-target">
  <div id="product_detail_cart">
    <?php echo $this->load->view('page/product/product_detail_cart_v', array('cart_next_page'=>'page/place_delivery'));?>
  </div>
</aside>
</div><!--inner-->

<?php if($this->ion_auth->in_group(array('FC', 'GU')) || ($this->ion_auth->in_group(array('SA')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB'))) ):?>
<div class="clr box_recent_purchased">
  <div class="inner">
    <div class="topic">
      <p><?php echo $this->lang->line('page_dashboard_recent_purchase', FALSE); ?></p>
      <h2><?php echo $this->lang->line('page_dashboard_recent_purchase_desc', FALSE); ?></h2>
    </div>
    <div class="clr list-recent">
      <ul class="owl-carousel owl-theme">
        <?php $product_id = ''; $p_id = array(); $i=1; foreach($order_history as $order):?>
        <?php

        if(!in_array($order->sale_item_id, $p_id)):
          array_push($p_id, $order->sale_item_id);
          if($i<=5):
            ?>
            <?php
            $product = $this->datacontrol_model->getRowData('product_general', array('id'=> $order->sale_item_id));
            $product_images = explode(',', $product->image);
            ?>
            <?php if($product_id != $product->product_id): $product_id = $product->product_id;?>
              <li class="item">
                <figure>
                  <a href="<?php echo base_url("$coutry_iso"."page/product_detail/$order->product_type/$product->product_id");?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
                </figure>
                <div class="detail">
                  <p class="name-p"><?php echo $product->product_name;?></p>
                  <p class="quantity"><?php echo $product->caution;?></p>
                  <?php if($order->buy_type == 'domestic'):?>
                    <div class="clr p-amount">
                      <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
                      <div class="price">
                        <?php echo number_format($order->item_sub_total_amount);?> Yen
                      </div>
                      <div class="clr line-btn">
                        <?php if(empty($cart['oversea'])):?>
                          <a href="javascript:void(0)" class="b-cart" onclick="add_cart('domestic', '<?php echo $order->product_type;?>', '<?php echo $order->sale_item_id;?>', '<?php echo base_url("page/product_detail/$order->product_type/$product->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                        <?php endif;?>
                      </div>
                    </div>
                  <?php endif;?>
                  <?php if($order->buy_type == 'oversea'):?>
                    <div class="clr p-amount">
                      <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
                      <div class="price">
                        <?php echo number_format($order->item_sub_total_amount);?> Yen
                      </div>
                      <div class="clr line-btn">
                        <?php if(empty($cart['domestic'])):?>
                          <a href="javascript:void(0)" class="b-cart" onclick="add_cart('oversea', '<?php echo $order->product_type;?>', '<?php echo $item->id;?>', '<?php echo base_url("page/product_detail/$order->product_type/$product->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                        <?php endif;?>
                      </div>
                    </div>
                  <?php endif;?>
                </div>
              </li>
            <?php endif;?>
          <?php endif;?>
          <?php $i++; endif;?>
        <?php endforeach;?>
      </ul>
    </div>
  </div>
</div>
<div class="clr box_recent_view">
  <div class="inner">
    <div class="topic">
      <p><?php echo $this->lang->line('page_dashboard_recently_viewed', FALSE); ?></p>
      <h2><?php echo $this->lang->line('page_dashboard_recently_viewed_desc', FALSE); ?></h2>
    </div>
    <div class="clr list-recent">
      <ul class="owl-carousel owl-theme">
        <?php foreach($recent_product as $item):?>
          <?php $product_images = explode(',', $item->image);?>
          <li class="item">
            <figure>
              <a href="<?php echo base_url("$coutry_iso"."page/product_detail/$item->product_type/$item->product_id");?>"><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></a>
            </figure>
            <div class="detail">
              <p class="name-p"><?php echo $item->product_name;?></p>
              <p class="quantity"><?php echo $item->caution;?></p>
              <?php if($this->ion_auth->logged_in() && (!$this->ion_auth->in_group(array('admin', 'SA', 'AA')) )):?>
              <?php
			 $this->db->where('product_country', $user_country);
              $this->db->where('product_id', $item->product_id);
              $product_domestic = $this->datacontrol_model->getRowData('product_general');
              ?>
              <?php if($product_domestic->is_active == 1):?>
                <div class="clr p-amount">
                  <span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', FALSE); ?></span>
                  <div class="price">
                    <?php echo number_format($product_domestic->sa_price);?> Yen
                  </div>
                  <div class="clr line-btn">
                    <?php if(empty($cart['oversea'])):?>
                      <a href="javascript:void(0)" class="b-cart" onclick="add_cart('domestic', 'general', '<?php echo $product_domestic->id;?>', '<?php echo base_url("page/product_detail/$item->product_type/$item->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                    <?php endif;?>
                  </div>
                </div>
              <?php endif;?>
            <?php endif;?>
            <?php if($this->ion_auth->in_group(array('SA')) || !$this->ion_auth->logged_in() || ($this->ion_auth->in_group(array('FC', 'GU')) && !in_array($user_country, array('Japan', 'JapanA', 'JapanB')) )):?>
            <div class="clr p-amount">
              <span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', FALSE); ?></span>
              <div class="price">
                <?php echo number_format($item->global_price);?> Yen
              </div>
              <div class="clr line-btn">
                <?php if(empty($cart['domestic'])):?>
                  <a href="javascript:void(0)" class="b-cart" onclick="add_cart('oversea', 'general', '<?php echo $item->id;?>', '<?php echo base_url("page/product_detail/$item->product_type/$item->product_id");?>'); return false;"><?php echo $this->lang->line('page_product_detail_add_to_cart', FALSE); ?> <img src="<?php echo base_url("assets/sensha-theme/");?>images/i-cart.png"></a>
                <?php endif;?>
              </div>
            </div>
          <?php endif;?>
        </div>
      </li>
    <?php endforeach;?>
  </ul>
</div>
</div>
</div>
<?php endif;?>

</div><!--container-->

<script>
  function sticktothetop() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
      $('#stickThis').addClass('stick');
      $('#stick-here').height($('#stickThis').outerHeight());
    } else {
      $('#stickThis').removeClass('stick');
      $('#stick-here').height(0);
    }
  }
  $(function() {
    $(window).scroll(sticktothetop);
    sticktothetop();
  });

  function checkDelivery(next_page){
    if($('input[name="domestic_delivery"]').length > 0){
      var checked = 0;
      $('input[name="domestic_delivery"]').each(function(){
        if($(this).prop('checked')){
          checked++;
        }
      });

      if(checked==0){
        alert('<?php echo $this->lang->line('page_cart_error', FALSE); ?>');
        return false;
      }
    }
    if($('input[name="oversea_delivery"]').length > 0){
      var checked = 0;
      $('input[name="oversea_delivery"]').each(function(){
        if($(this).prop('checked')){
          checked++;
        }
      });

      if(checked==0){
        alert('<?php echo $this->lang->line('page_cart_error', FALSE); ?>');
        return false;
      }
    }


    window.location = next_page;

  }

  function add_shipping(type, shipping_name, shipping_price, sub_price){
    var total = (shipping_price*1)+(sub_price*1);
  // console.log(total);
  $('.'+type+'_total').text(total.toLocaleString()+" Yen");
  $.ajax({
    url:'<?php echo base_url("page/product/add_shipping");?>',
    type:'POST',
    data: {type:type, shipping_name:shipping_name, shipping_price:shipping_price, sub_price:sub_price},
    success: function(data){
      // console.log(data);
      $.ajax({
        url:'<?php echo base_url("page/product/get_product_detail_cart_v");?>',
        type:'GET',
        success: function(data){
          $('#product_detail_cart').html('<div class="p-shipping">'+$(data).html()+'</div>');
        }
      });
    }
  });
}

function delete_cart(type, id){
  var amount = 0;
  if(type == 'domestic'){
    amount = $('#domestic_no').val();
  }
  if(type == 'oversea'){
    amount = $('#oversea_no').val();
  }

  $.ajax({
    url: '<?php echo base_url("page/product/delete_cart/");?>'+id+'/'+type,
    type:'GET',
    success: function(data){
      // $.ajax({
      // 	url:'<?php //echo base_url("page/product/get_product_detail_cart_v");?>',
      // 	type:'GET',
      // 	success: function(data){
      // 		$('#product_detail_cart').html('<div class="p-shipping">'+$(data).html()+'</div>');
      // 	}
      // });
      window.location = '';
    }
  });
}

$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
    loop:false,
    margin: 15,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: false
      },
      600: {
        items: 4,
        nav: false
      },
      1000: {
        items: 5,
        nav: true,
        loop: false,
        margin: 20
      }
    }
  });

  /*
  $('.add-wishlist').click(function(){
    var id = $(this).data('id');
    var t = $(this);
    $.ajax({
      url:'<?php echo base_url("page/product/add_wishlist");?>',
      type:'POST',
      data:{id:id},
      success: function(data){
        console.log(data);
        t.parent().attr('style', 'color:green;');
        t.parent().text('Added wishlist');
      }
    });
    return false;
  });*/

  $('.add-wishlist').click(function(){
    var id = $(this).data('id');
    var sku = '<?php echo $product_id;?>';
    var product_type = '<?php echo $product_type;?>';
    var t = $(this);
    $.ajax({
      url:'<?php echo base_url("page/product/add_wishlist");?>',
      type:'POST',
      data:{id:id, product_type:product_type, sku:sku},
      success: function(data){
        console.log(data);
        t.parent().attr('style', 'color:green;');
        t.parent().text('Added wishlist');
      }
    });
    return false;
  });

});

// function add_cart(type, id){
//   var amount = 1;
//   if(type == 'domestic'){
// 		// amount = $('#domestic_no').val();
// 		$('.cart_oversea').hide();
// 	}
// 	if(type == 'oversea'){
// 		// amount = $('#oversea_no').val();
// 		$('.cart_domestic').hide();
// 	}
//
//   $.ajax({
//     url: '<?php echo base_url("page/product/add_cart/general/");?>'+id+'/'+type+'/'+amount,
//     type:'GET',
//     success: function(data){
//       $.ajax({
//         url:'<?php echo base_url("page/product/get_product_detail_cart_v");?>',
//         type:'GET',
//         success: function(data){
//           $('#product_detail_cart').html('<div class="p-shipping">'+$(data).html()+'</div>');
//           window.location = '';
//         }
//       });
//     }
//   });
// }

function add_cart(type, product_type, id, url){
 var amount = 1;
 // if(type == 'domestic'){
 // 	amount = $('#domestic_no').val();
 // }
 // if(type == 'oversea'){
 // 	amount = $('#oversea_no').val();
 // }
 if(product_type == ''){
   product_type = 'general';
 }

 $.ajax({
   url: '<?php echo base_url("page/product/add_cart/");?>'+product_type+'/'+id+'/'+type+'/'+amount,
   type:'GET',
   success: function(data){
     window.location = url;
     // $.ajax({
     // 	url:'<?php echo base_url("page/product/get_product_detail_cart_v");?>',
     // 	type:'GET',
     // 	success: function(data){
     // 		$('#product_detail_cart').html('<div class="p-shipping">'+$(data).html()+'</div>');
     // 	}
     // });
   }
 });
}

</script>
