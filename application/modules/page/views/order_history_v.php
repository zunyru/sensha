<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><a href="<?php echo base_url("page/user/dashboard");?>"> <?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a><span><?php echo $this->lang->line('breadcrumb_order_history', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_order_history_order_history', FALSE); ?></p>
      </div>
      <?php  foreach($order_history as $order):?>
        <?php
          $order_user = $order->create_by;
           if ((in_array($order->payment_method, array('amazon', 'paypal')) && $order->payment_status == 'Completed') || in_array($order->payment_method, array('pay on delivery', 'Bank Transfer', Null))) :
        ?>
        <div class="clr box-detail-history">
              <?php $sale_history_total = $this->datacontrol_model->getRowData('sales_history_total', array('shipment_id'=>$order->shipment_id));?>
          <div class="clr">
            <div class="left">
              <div class="col1">
                <span><?php echo $this->lang->line('page_order_history_shipment_id', FALSE); ?>:</span>
                <p><?php echo $order->shipment_id;?></p>
                <div style="margin-top: 10px;">
                  <span><?php echo $this->lang->line('page_order_history_data_of_order', FALSE); ?>:</span>
                  <p><?php echo date('Y/m/d', strtotime($order->create_date));?></p>
                </div>
              </div>
              <div class="col2">
                <div>
                  <p><?php echo $sale_history_total->purchaser_company;?> <?php echo $sale_history_total->purchaser_name;?></p>
                  <span><?php echo $sale_history_total->shipping_addr;?></span>
                </div>
                <!--div class="address">
                  <?php // echo "$shipping_info->address $shipping_info->zipcode $shipping_info->country";?> <br>Tel :  <?php // echo $shipping_info->phone;?>
                </div-->
              </div>
            </div>
            <div class="clr right">
              <div class="col1">
                <span><?php echo $this->lang->line('page_order_history_total', FALSE); ?>:</span>
                <p class="txt-red"><?php echo number_format($sale_history_total->sale_total_amount+$sale_history_total->admin_fee_amount+$sale_history_total->vat_amount);?> Yen</p>
                <span><?php echo $this->lang->line('page_order_history_item_sub_total_amount', FALSE); ?>:</span>
                <p class="txt-12"><?php echo number_format($sale_history_total->item_sub_total_amount);?> Yen</p>
                <span><?php echo $this->lang->line('page_order_history_delivery_amount', FALSE); ?>:</span>
                <p class="txt-12"><?php echo number_format($sale_history_total->delivery_amount);?> Yen</p>
                <span><?php echo $this->lang->line('page_order_history_discount_amount', FALSE); ?>:</span>
                <p class="txt-12"><?php echo number_format($sale_history_total->discount_amount);?> Yen</p>
                <span><?php echo $this->lang->line('page_product_detail_delivery_fee', FALSE); ?>:</span>
                <p class="txt-12"><?php echo number_format($sale_history_total->admin_fee_amount);?> Yen</p>
                <span><?php echo $this->lang->line('page_product_detail_tax', FALSE); ?> (<?php echo $sale_history_total->vat_percent;?>%):</span>
                <p class="txt-12"><?php echo number_format($sale_history_total->vat_amount);?> Yen</p>
              </div>
              <div class="col2">
                <span><?php echo $this->lang->line('page_order_history_delivery_mothod', FALSE); ?>:</span>
                <p><?php echo $order->shipping_method;?></p>
                <span class="b-gray"><?php echo $order->buy_type; ?></span>
              </div>
            </div>
          </div>
            <div class="status-delivery">
            <h2>Status : <span><?php echo $sale_history_total->shipping_status;?></span></h2>
            <div class="c-left">
              <?php
              $this->db->where('shipment_id', $order->shipment_id);
              $this->db->where('create_by', $order->create_by);
              $item_list = $this->datacontrol_model->getAllData('sales_history');
              $i=0;
              ?>
              <ul class="ul">
                <?php foreach($item_list as $product_item):?>
                  <?php
                    $product = $this->datacontrol_model->getRowData('product_'.$product_item->product_type, array('product_id'=> $product_item->sale_item_id));
                    $product_images = explode(',', $product->image);
                    if($i>1){ $showmore=1;}
                  ?>
                  <li <?php if($i>1){ echo "class='open_more'";} ?>>
                    <figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></figure>
                    <div class="detail">
                      <p class="name-p"><?php echo $product_item->product_name;?></p>
                      <div class="amount-status">
                        <span><?php echo $this->lang->line('page_order_history_amount', FALSE); ?>: <?php echo number_format($product_item->item_amount);?></span>
                        <p class="txt-red"><?php echo $this->lang->line('page_order_history_sub_total', FALSE); ?>: <?php echo number_format($product_item->item_sub_total_amount*$product_item->item_amount);?> Yen</p>
					  <?php if(!empty($product_item->import_shipping)):?>
                        <span><?php echo $this->lang->line('page_product_detail_import_shipping', FALSE); ?>: <?php echo @number_format($product_item->import_shipping);?> Yen</span>
						<?php endif;?>
						<?php if(!empty($product_item->import_tax)):?>
                        <span><?php echo $this->lang->line('page_product_detail_import_duty', FALSE); ?>: <?php echo @number_format($product_item->import_tax);?> Yen</span>
                        <?php endif;?>
                      </div>
                    </div>
                  </li>
                  <?php $i++; ?>
                <?php endforeach;?>
              </ul>
             <?php if($showmore==1){ ?>
              <div class="show_btn">
	              <a class="show_more "><span>+ <?php echo $this->lang->line('page_order_history_show_more', FALSE); ?></span></a>
	              <a class="show_less" style="display: none;"><span>- <?php echo $this->lang->line('page_order_history_show_less', FALSE); ?></span></a>
              </div>
              <?php } ?>
            </div>
            <div class="c-right">
              <ul>
			  <?php if($this->ion_auth->in_group(array('SA', 'FC'))):?>
                <li>
                  <a href="<?php echo base_url("page/invoice/$order->shipment_id");?>" target="_blank" class="b-blue"><?php echo $this->lang->line('page_order_history_invoice_download', FALSE); ?></a>
                </li>
			   <?php endif;?>
			   <?php  if ((in_array($sale_history_total->shipping_status, array('Paid', 'Completed')))) :  ?>
                <li>
                  <a href="<?php echo base_url("page/receipt/$order->shipment_id");?>" target="_blank" class="b-blue"><?php echo $this->lang->line('page_order_history_receipt_download', FALSE); ?></a>
                </li>
			   <?php endif;?>
                <li>
                  <a href="<?php echo base_url("page/claim/$order->shipment_id");?>" class="b-blue"><?php echo $this->lang->line('page_order_history_claim_shipment', FALSE); ?></a>
                </li>
              </ul>
              </div>
          </div>
        </div>
      <?php $showmore=0; ?>
      <?php endif; ?>
      <?php endforeach;?>
      <section id="pagination">
        <?php echo $links; ?>

       </section>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->

<!--<script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.showmore.js"></script>-->
<script>
$('.show_more').click(function(){
	$(this).hide();
	$(this).parent().find('.show_less').show();
	$(this).parent().parent().find('.open_more').fadeIn();
});
$('.show_less').click(function(){
	$(this).parent().find('.show_more').show();
	$(this).hide();
	$(this).parent().parent().find('.open_more').fadeOut();
});
</script>
