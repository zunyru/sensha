
<div class="container">
	<div class="clr inner">
		<div id="breadcrumbs">
			<span><a href="index.php">Home</a><a href="<?php echo base_url("page/user/dashboard");?>"><?php echo $this->lang->line('breadcrumb_dashboard', FALSE); ?></a><span><?php echo $this->lang->line('breadcrumb_delivery_claim', FALSE); ?></span>
		</div>
	</div><!--inner-->
	<div class="clr inner">
		<div class="box-content">
			<div class="layout-contain">
				<div class="topic">
					<p class="title-page"><?php echo $this->lang->line('page_delivery_claim_order_number', FALSE); ?> : <?php echo $shipment_id;?></p>
				</div>

				<div class="clr box-detail-history">
					<div class="clr">
						<div class="left">
							<div class="col1">
								<span><?php echo $this->lang->line('page_order_history_shipment_id', FALSE); ?>:</span>
								<p><?php echo $order->shipment_id;?></p>
								<div style="margin-top: 10px;">
									<span><?php echo $this->lang->line('page_order_history_data_of_order', FALSE); ?>:</span>
									<p><?php echo date('d/m/Y', strtotime($order->create_date));?></p>
								</div>
							</div>
							<div class="col2">
								<div>
									<span><?php echo $this->lang->line('page_order_history_delivery_address', FALSE); ?></span>
									<p><?php echo $shipping_info->name;?></p>
								</div>
								<div class="address">
									<?php echo "$shipping_info->address $shipping_info->zipcode $shipping_info->country";?> <br>Tel :  <?php echo $shipping_info->phone;?>
								</div>
							</div>
						</div>
						<div class="clr right">
							<?php $sale_history_total = $this->datacontrol_model->getRowData('sales_history_total', array('shipment_id'=>$order->shipment_id));?>
							<div class="col1">
								<span><?php echo $this->lang->line('page_order_history_total', FALSE); ?>:</span>
								<p class="txt-red"><?php echo number_format($sale_history_total->sale_total_amount+$sale_history_total->admin_fee_amount+$sale_history_total->vat_amount);?> Yen</p>
								<span><?php echo $this->lang->line('page_order_history_item_sub_total_amount', FALSE); ?>:</span>
								<p class="txt-12"><?php echo number_format($sale_history_total->item_sub_total_amount, 2);?> Yen</p>
								<span><?php echo $this->lang->line('page_order_history_delivery_amount', FALSE); ?>:</span>
								<p class="txt-12"><?php echo number_format($sale_history_total->delivery_amount, 2);?> Yen</p>
								<span><?php echo $this->lang->line('page_order_history_discount_amount', FALSE); ?>:</span>
								<p class="txt-12"><?php echo number_format($sale_history_total->discount_amount, 2);?> Yen</p>
				                <span><?php echo $this->lang->line('page_product_detail_delivery_fee', FALSE); ?>:</span>
				                <p class="txt-12"><?php echo number_format($sale_history_total->admin_fee_amount);?> Yen</p>
				                <span><?php echo $this->lang->line('page_product_detail_tax', FALSE); ?> (<?php echo $sale_history_total->vat_percent;?>%):</span>
				                <p class="txt-12"><?php echo number_format($sale_history_total->vat_amount);?> Yen</p>
							</div>
							<div class="col2">
								<span><?php echo $this->lang->line('page_order_history_delivery_mothod', FALSE); ?>:</span>
								<p><?php echo $order->shipping_method;?></p>
								<span class="b-gray"><?php echo $order->buy_type; ?></span>
							</div>
						</div>
					</div>
					<div class="status-delivery">
						<h2>Status : <span><?php echo $order->shipping_status;?></span></h2>
						<ul>
							<?php foreach($order_history as $product_item):?>
								<?php
								$product = $this->datacontrol_model->getRowData('product_'.$product_item->product_type, array('id'=> $product_item->sale_item_id));
								$product_images = explode(',', $product->image);
								if($i>1){ $showmore=1;}
								?>
								<li <?php if($i>1){ echo "class='open_more'";} ?>>
									<figure><img src="<?php echo base_url("uploads/product_image/$product_images[0]");?>"></figure>
									<div class="detail">
										<p class="name-p"><?php echo $product_item->product_name;?></p>
										<div class="amount-status">
											<span><?php echo $this->lang->line('page_order_history_amount', FALSE); ?>: <?php echo number_format($product_item->item_amount);?></span>
											<p class="txt-red"><?php echo $this->lang->line('page_order_history_sub_total', FALSE); ?>: <?php echo number_format($product_item->item_sub_total_amount);?> Yen</p>
										</div>
									</div>
								</li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
				<form method="post" action="<?php echo base_url("page/claim_confirm");?>">
					<div class="clr box-claim-form">
						<div class="topic">
							<p class="title-page"><?php echo $this->lang->line('page_delivery_claim_about_issue', FALSE); ?></p>
						</div>
						<ul>
							<p class="head-bold" style="margin-bottom:15px;"><?php echo $this->lang->line('page_delivery_claim_issue_details', FALSE); ?></p>
							<li>
								<div class="radio">
									<input type="radio" name="reason" class="" value="<?php echo $this->lang->line('page_delivery_claim_not_obtained_products', FALSE); ?>">
								</div>
								<label for="01" class="detail-label">
									<p class="head-bold"><?php echo $this->lang->line('page_delivery_claim_not_obtained_products', FALSE); ?></p>
									<span><?php echo $this->lang->line('page_delivery_claim_not_obtained_any_products', FALSE); ?></span>
								</label>
							</li>
							<li>
								<div class="radio">
									<input type="radio" name="reason" class="" value="<?php echo $this->lang->line('page_delivery_claim_missing_products', FALSE); ?>">
								</div>
								<label for="01" class="detail-label">
									<p class="head-bold"><?php echo $this->lang->line('page_delivery_claim_missing_products', FALSE); ?></p>
									<span><?php echo $this->lang->line('page_delivery_claim_obtained_products_but_missing', FALSE); ?></span>
								</label>
							</li>
							<li>
								<div class="radio">
									<input type="radio" name="reason" class="" value="<?php echo $this->lang->line('page_delivery_claim_obtained_wrong_products', FALSE); ?>">
								</div>
								<label for="01" class="detail-label">
									<p class="head-bold"><?php echo $this->lang->line('page_delivery_claim_obtained_wrong_products', FALSE); ?></p>
									<span><?php echo $this->lang->line('page_delivery_claim_case_obtained_wrong_products', FALSE); ?></span>
								</label>
							</li>
							<li>
								<div class="radio">
									<input type="radio" name="reason" class="" value="<?php echo $this->lang->line('page_delivery_claim_others', FALSE); ?>">
								</div>
								<div class="detail-label w-30">
									<input type="text" placeholder="<?php echo $this->lang->line('page_delivery_claim_others', FALSE); ?>" class="form-control" name="text_other">
								</div>
							</li>
							<li>
								<p class="head-bold" style="margin-bottom:15px;"><?php echo $this->lang->line('page_delivery_claim_email', FALSE); ?></p>
								<div class="c-form">
									<img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png" style="width:20px; display:inline-block;">
									<input type="text" placeholder="" class="form-control" name="email">
								</div>
							</li>
							<li>
								<p class="head-bold" style="margin-bottom:15px;"><?php echo $this->lang->line('page_delivery_claim_describe_issues', FALSE); ?></p>
								<div class="c-form">
									<img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit-gray.png" style="width:20px; display:inline-block;vertical-align:top">
									<textarea class="form-control" rows="8" placeholder="<?php echo $this->lang->line('page_delivery_claim_comments', FALSE); ?>" name="comment"></textarea>
								</div>
							</li>
						</ul>
						<div style="padding-left:30%;"><button type="submit" class="b-blue"><?php echo $this->lang->line('page_delivery_claim_send', FALSE); ?></button></div>
					</div>
					<input type="hidden" name="shipment_id" value="<?php echo $shipment_id;?>">
				</form>
			</div><!--layout-contain-->

		</div><!--box-content-->

	</div><!--inner-->

</div><!--container-->
<script src="js/main.js"></script>
<!-- .wrapper -->
