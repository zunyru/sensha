<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('page_place_delivery_use_registered', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="box-progress">
        <div class="progress-row">
          <div class="progress-step">
            <button type="button" class="btn-circle">1</button>
            <p>Cart item& Delivery</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle step-active">2</button>
            <p>Shipping Address</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle c-blue">3</button>
            <p>Confirmation& Payment</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle c-blue">4</button>
            <p>Complete Order</p>
          </div>
        </div>
      </div>
      <?php if($shipping_information):?>
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_place_delivery_use_registered', FALSE); ?></p>
      </div>
      <?php if(empty($shipping_information->address)){ ?>
      <div class="clr box-shipping-address">
        <div class="left">
          <h3><?php echo $this->lang->line('page_place_delivery_please_update_delivery_address', FALSE); ?></h3>
        </div>
          <div class="right">
          <?php if($this->ion_auth->user()->row()->country == 'Japan'):?>
            <div class="row">
            <a href="<?php echo base_url("page/user/edit_shipping_info?redirect=cart&iso=$this->coutry_iso");?>" class="btn-gray"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" width="31" style="width: 20px;margin-right: 10px;">
              <?php echo $this->lang->line('page_place_delivery_change_address', FALSE); ?>
            </a>
          </div>
        <?php endif; ?>
        </div>
      </div>
      <?php }else{  //end if(!empty($shipping_information->name)): ?>
      <div class="clr box-shipping-address">
        <div class="left">
          <h3><?php echo ($shipping_information->company == '') ? $shipping_information->name : $shipping_information->company; ?></h3>
          <p><?php echo $shipping_information->address;?> <?php echo $shipping_information->zipcode;?> <?php echo ucwords(str_replace("_"," ", $shipping_information->country))?></p>
          <p>Tel : <?php echo $shipping_information->phone;?></p>
        </div>
          <div class="right">
            <div class="row">

            <a href="<?php echo base_url('page/payment');?>" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" width="31" style="width: 20px;margin-right: 10px;"><?php echo $this->lang->line('page_place_delivery_use_this', FALSE); ?></a>

          </div>
          <?php // if($this->ion_auth->user()->row()->country == 'Japan'):?>
            <div class="row">
            <a href="<?php echo base_url("page/user/edit_shipping_info?redirect=cart&iso=$this->coutry_iso");?>" class="btn-gray"><img src="<?php echo base_url("assets/sensha-theme/");?>images/i-edit.png" width="31" style="width: 20px;margin-right: 10px;">
              <?php echo $this->lang->line('page_place_delivery_edit_address', FALSE); ?>
            </a>
          </div>
        <?php // endif;?>
        </div>
      </div>
      <?php } ?>
      <?php endif;?>
      <?php /*  if($this->ion_auth->user()->row()->country != 'Japan'):?>
        <form id="ajax_edit_shipping_info" method="post" action="<?php echo base_url('page/user/ajax_edit_shipping_info');?>">
          <div class="clr box_form">
            <div class="topic">
              <p class="title-page"><?php echo $this->lang->line('page_place_delivery_change_address', FALSE); ?> <span></span></p>
              </div>
            <div class="box-inner">
              <div class="r-inline">
                <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png">Country</label>
                <div class="r-input">
                  <select class="form-control" name="country" required>
  									<option value="">Select</option>
  									<?php //foreach($countries as $item):?>
  										<option value="<?php echo $item->nicename;?>" <?php echo (@$shipping_information->country == $item->nicename)?"selected":"disabled";?> ><?php echo ucwords(str_replace("_"," ", $item->nicename))?></option>
  									<?php //endforeach;?>
  								</select>
                </div>
                </div>
                <div class="r-inline">
    							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_shipping_info_name', FALSE); ?></label>
    							<div class="r-input">
    								<input type="text" placeholder="Please input your name" class="form-control" name="name" required />
    							</div>
    						</div>
    						<div class="r-inline">
    							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/03.png"><?php echo $this->lang->line('page_shipping_info_company', FALSE); ?></label>
    							<div class="r-input">
    								<input type="text" placeholder="Please input your company" class="form-control" name="company"  />
    							</div>
    						</div>
    						<div class="r-inline">
    							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_shipping_info_zip', FALSE); ?></label>
    							<div class="r-input">
    								<input type="text" placeholder="Please input your zipcode" class="form-control" name="zipcode"  required>
    							</div>
    						</div>
    						<div class="r-inline">
    							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/05.png"><?php echo $this->lang->line('page_shipping_info_address', FALSE); ?></label>
    							<div class="r-input">
    								<input type="text" placeholder="Please input your Address" class="form-control" name="address" required>
    							</div>
    						</div>
    						<div class="r-inline">
    							<label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_shipping_info_telephone', FALSE); ?></label>
    							<div class="r-input">
    								<input type="text" placeholder="Please input your Telephone" class="form-control" name="phone" required>
    							</div>
    						</div>
              <div style="text-align: center;margin:40px 0 0 0">
                <button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" width="31" style="width: 20px;margin-right: 10px;"><?php echo $this->lang->line('page_place_delivery_over_write', FALSE); ?></button>
              </div>
             </div>
          </div>
        </form>
      <?php endif;*/ ?>

    </div><!--layout-contain-->

  </div><!--box-content-->

</div><!--inner-->

</div><!--container-->

<script>
  $(function(){
    $('#ajax_edit_shipping_info').submit(function(){
      $.ajax({
        method: "POST",
        url: $(this).attr('action'),
        data: $( this ).serialize()
      })
      .done(function( msg ) {
        location.reload();
      })
      .fail(function( msg ) {
        Swal.fire(
          'Oops...',
          msg,
          'error'
        );
      });
      return false;
    });
  });
</script>
