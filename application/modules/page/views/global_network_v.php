<div class="clr inner">
  <div id="breadcrumbs" style="margin:15px 0;">
    <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo $this->lang->line('breadcrumb_global', FALSE); ?></span></span>
  </div>
</div>
<div class="clr box-network">
  <div class="inner">
    <div class="topic2">
      <p class="title-page"><?php echo $this->lang->line('page_global_section1_title', FALSE); ?></p>
    </div>
    <p align="center"><?php echo $this->lang->line('page_global_section1_desc', FALSE); ?></p>
    <div class="box-map">
      <div id="map" style="height: 500px;"></div>
      <!-- <img src="<?php echo base_url("assets/sensha-theme/");?>images/map.jpg"> -->
      <!-- <div class="popup">
        <div class="inner-popup">
          <h3>Japan</h3>
          <p class="company-name">SENSHA CO., LTD</p>
          <p>OFFICE ADDRESS <br>
            1007-3, Kami-kasuya, Isehara-shi, Japan<br>
            PHONE : (+81) 463-94-5106</p><br>
          </div>
        </div> -->
      </div>
      <div class="clr box_location">
        <ul id="branchs">
          <?php foreach($global_network as $item):?>
            <li>
              <figure><img src="<?php echo base_url("uploads/global_network/$item->branch_image");?>"></figure>
              <div class="clr name-location">
                <div class="l-inner">
                  <h3><?php echo $item->country;?></h3>
                  <p class="company-name"><?php echo $item->company;?></p>
                  <p><?php echo $this->lang->line('page_global_address', FALSE); ?> <br>
                    <?php echo $item->addr;?><br>
                    <?php echo $this->lang->line('page_global_phone', FALSE); ?> : <?php echo $item->phone;?></p><br>
                  </div>
                  <div class="btn">
                    <a href="https://maps.google.com?saddr=Current+Location&amp;daddr=<?php echo $item->latitude;?>,<?php echo $item->longitude;?>" target="_blank" class="b-map"><?php echo $this->lang->line('page_global_see_google', FALSE); ?></a>
                  </div>
                </div>
              </li>
            <?php endforeach;?>
          </ul>
        </div>
      </div>
    </div>

    <div style="display: none;">
      <?php $i=0; foreach($global_network as $item):?>
        <div id="p<?php echo $i;?>">
        <div id="p<?php echo $i;?>" class="popupx">
          <div class="inner-popup">
            <h3><?php echo $item->country;?></h3>
            <p class="company-name"><?php echo $item->company;?></p>
            <p><?php echo $this->lang->line('page_global_address', FALSE); ?> <br>
              <?php echo $item->addr;?><br>
              <?php echo $this->lang->line('page_global_phone', FALSE); ?> : <?php echo $item->phone;?></p><br>
            </div>
          </div>
          </div>
      <?php $i++; endforeach;?>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjVxfyjjLbo4IDH5rHbCpW7watylFE8J4&callback=initMap" async defer></script>
    <!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0B8RG67OOvNICV_4EQr6G3WVAyhKqvho&callback=initMap" async defer></script-->
    <script>
    var map;
    var infoWindow;
    var markers = [];
    var marker;
    var locations = [];
    var branchs = JSON.parse('<?php echo json_encode($global_network);?>');

    function initBranchs(){
      var x=1;
      for(i=0; i<branchs.length; i++){
        //
        var li = `<li>
        <figure><img src="http://localhost/new_sensha/assets/sensha-theme/images/n1.jpg"></figure>
        <div class="clr name-location">
        <div class="l-inner">
        <h3>Global</h3>
        <p class="company-name">KB</p>
        <p>OFFICE ADDRESS <br>
        9/126 บ้านกลางเมืองเกษตร-นวมินทร์ ซอยนวมินทร์70 แยก11-2 แขวงคลองกุ่ม เขตบึงกุ่ม กทม 10240<br>
        PHONE : 803951444</p><br>
        </div>
        <div class="btn">
        <a href="" class="b-map">See google map</a>
        </div>
        </div>
        </li>`;

        // $('ul#branchs').append(li);
        locations.push({lat: parseFloat(branchs[i].latitude), lng: parseFloat(branchs[i].longitude)});
        x++;
      }
    }

    initBranchs();
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: parseFloat(branchs[0].latitude), lng: parseFloat(branchs[0].longitude)},
        zoom: 2
      });
      infoWindow = new google.maps.InfoWindow();

      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: locations[i],
          map: map,
          // animation: google.maps.Animation.BOUNCE,
        });

        markers.push(marker);

        google.maps.event.addListener(marker,'click', (function(marker, content, infowindow, i){
          return function(branchs) {

            infowindow.setContent(content);
            // infowindow.setContent(branchs[i].latitude, branchs[i].longitude);
            infowindow.open(map,marker);
          };
        })(marker, $('#p' + i ).html(), infoWindow, i));

      }




    }



    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
      // infoWindow.setPosition(pos);
      // infoWindow.setContent(browserHasGeolocation ?
      //   'Error: The Geolocation service failed.' :
      //   'Error: Your browser doesn\'t support geolocation.');
      //   infoWindow.open(map);
      // $('ul li').eq(60).click();
    }

    function distance(lat1, lon1, lat2, lon2, unit) {
      if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
      }
      else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
          dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return dist;
      }
    }



    $('#search_addr').click(function(){
      if($('#address').val() == ''){
        $('#loc-list li').remove();
        initBranchs();
      }
      var data_list = [];
      $('#loc-list li ' ).each(function(){
        var str = $(this).find('.loc-addr').text();
        // console.log(str);
        var res = str.match($('#address').val());
        // console.log(res);
        if(res != null){
          data_list.push($(this).data('markerid'));
        }
      });

      if(data_list.length == 0){
        return false;
      }
      $('#loc-list li').remove();
      for(i=0; i<data_list.length; i++){
        var li = `<li data-markerid="`+i+`">
        <div class="list-label">`+(i+1)+`</div>
        <div class="list-details">
        <div class="list-content">
        <div class="loc-name">`+branchs[data_list[i]].name+`</div>
        <div class="loc-addr">`+branchs[data_list[i]].address+`</div>
        <div class="loc-business_times">`+branchs[data_list[i]].business_times+`</div>
        <div class="loc-tel">`+branchs[data_list[i]].tel+`</div>
        <div class="loc-remark">`+branchs[data_list[i]].remark+`</div>
        </div>
        </div>
        </li>`;

        $('ul#list').append(li);
      }
    })


    </script>
