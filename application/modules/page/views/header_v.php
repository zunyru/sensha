<header id="header">
    <div class="header-wrap">
        <div class="header-main">
            <div class="inner">
                <a class="header-logo" href="<?php echo base_url("$this->coutry_iso"); ?>">
                    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/logo.png" alt="SENSHA">
                </a>
                <ul class="header-menu">
                    <?php if (!$this->ion_auth->logged_in()) : ?>
                        <!--
            <li>
              <a href="<?php echo base_url("$coutry_iso" . "page/register"); ?>" class="">
                <span class="icon-person"></span>
                <?php echo $this->lang->line('header_menu_sign_up', FALSE); ?>
              </a>
            </li>
-->
                        <li>
                            <a href="" data-src="#modal01" class="login">
                                <span class="icon-lock"></span>
                                <?php echo $this->lang->line('header_menu_login', FALSE); ?>
                            </a>
                        </li>
                    <?php else : ?>
                        <li>
                            <a href="<?php echo base_url("/page/user/dashboard"); ?>" class="">
                                <span class="icon-person"></span>
                                <?php echo $this->ion_auth->user()->row()->email; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("/page/user/logout"); ?>" class="">
                                <span class="icon-lock"></span>
                                <?php echo $this->lang->line('header_menu_logout', FALSE); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="">
                        <a href="<?php echo base_url("$coutry_iso" . "page/cart"); ?>" class="">
                            <span class="icon-cart02"></span>
                            <?php echo $this->lang->line('header_menu_cart', FALSE); ?>
                        </a>
                        <!--<div class="box-cart">
                        <h2><span class="icon-cart02"></span> Current Cart</h2>
                        <div class="clr">
                        <div class="box_domestic">
                        <div class="inner_box_domestic">
                        <span class="c01">Domestic</span>
                        <p class="items">5 items in cart</p>
                        <p class="sub">Subtotal 5,180 Yen</p>
                        <ul class="list-items">
                        <li>
                        <p>Import shipping</p>
                        <span>1,200 Yen</span>
                      </li>
                      <li>
                      <p>Import Tax</p>
                      <span>820  Yen</span>
                    </li>
                    <li>
                    <p>Minimum Shipping</p>
                    <span>900  Yen</span>
                  </li>
                </ul>
              </div>
              <div class="total clr">
              <p>Total</p>
              <span>8,180 Yen</span>
            </div>
            </div>
            <div class="box_oversea">
            <div class="inner_box_domestic">
            <span class="c01">Oversea</span>
            <p class="items">5 items in cart</p>
            <p class="sub">Subtotal 5,180 Yen</p>
            <ul class="list-items">
            <li>
            <p>Import shipping</p>
            <span>1,200 Yen</span>
            </li>
            <li>
            <p>Import Tax</p>
            <span>820  Yen</span>
            </li>
            <li>
            <p>Minimum Shipping</p>
            <span>900  Yen</span>
            </li>
            </ul>
            </div>
            <div class="total clr">
            <p>Total</p>
            <span>8,180 Yen</span>
            </div>
            </div>
            </div>
            <div class="line-btn">
            <input type="button" class="buy btn" onclick="location.href='cart.php';" value="ADD TO CART" />
            </div>
            </div>-->
                    </li>
                    <li class="lang">
                        <a href="" class="">
                            <span class="icon-lang"></span>
                            <?php echo ucwords(str_replace("_", " ", $this->user_lang)) ?>
                        </a>
                        <div class="language-select">
                            <?php
                            $user_country = $this->ion_auth->user()->row()->country;
                            if ($this->ion_auth->logged_in()) {
                                $this->db->where('country', $user_country);
                                $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                                if (empty($nation_lang)) {
                                    $this->db->where('country', 'Global');
                                    $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                                }
                            } else {
                                $this->db->where('is_active', 1);
                                $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                            }

                            ?>
                            <ul>
                                <?php foreach ($nation_lang as $item) : ?>
                                    <?php
                                    $this->db->where('nicename', '!=', 'JapanA');
                                    $this->db->where('nicename', '!=', 'JapanB');
                                    $this->db->where('nicename', $item->country);
                                    $c = $this->datacontrol_model->getRowData('countries');
                                    if (($item->country != 'JapanA') && ($item->country != 'JapanB')):
                                        ?>
                                        <li><input <?php echo ($user_country == $item->country) ? 'selected' : ''; ?>
                                                    type="radio" name="language"
                                                    data-iso="<?php echo strtolower($c->iso); ?>"
                                                    value="<?php echo $item->country; ?>" data-label="language-select"
                                                    data-value="<?php echo $item->country; ?>"><label
                                                    for="language01"><?php echo ucwords(str_replace("_", " ", $item->country)) ?></label>
                                        </li>
                                    <?php endif; endforeach; ?>
                                <!-- <li><input type="radio" id="language01" name="language" value="Global" data-label="language-select" data-value="Global" checked="checked"><label for="language01">Global - English</label></li> -->
                                <!-- <li><input type="radio" id="language02" name="language" value="Japan" data-label="language-select" data-value="Japan"><label for="language02">Japan - 日本語</label></li> -->
                                <!-- <li><input type="radio" id="language03" name="language" value="Chinese - Mandalin" data-label="language-select" data-value="Chinese - Mandalin"><label for="language03">Chinese - Mandalin</label></li> -->
                                <!-- <li><input type="radio" id="language04" name="language" value="Chinese - Cantonese" data-label="language-select" data-value="Chinese - Cantonese"><label for="language04">Chinese - Cantonese</label></li> -->
                                <!-- <li><input type="radio" id="language05" name="language" value="English - Us" data-label="language-select" data-value="English - Us"><label for="language05">English - Us</label></li> -->
                                <!-- <li><input type="radio" id="language06" name="language" value="Thai" data-label="language-select" data-value="Thai"><label for="language06">Thai</label></li> -->
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- .header-main -->
        <div id="hamburger">
            <div id="hamburger-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
        </div>
        <?php
        $country = 'Global';
        if ($this->ion_auth->logged_in()) {
            $country = $this->ion_auth->user()->row()->country;
        } else {
            $country = $this->user_lang;
        }
        if ($country == 'JapanA' || $country == 'JapanB') {
            $country = 'Japan';
        }

        //zun
        $nation_lang = $this->datacontrol_model->getRowData('nation_lang', array('country' => $country));

        if (!empty($nation_lang)) {
            $this->db->where('country', $country);
        } else {
            $this->db->where('country', 'Global');
        }

        $this->db->where('cluster_type', 'general');
        $query = $this->db->get('cluster1');
        $cluster1 = $query->result();

        //echo $this->db->last_query();
        ?>
        <div class="header-nav">
            <div class="inner">
                <nav>
                    <ul>
                        <?php if ($this->ion_auth->logged_in()) : ?>
                            <li class="welcome">
                                <a href="<?php echo base_url("/page/user/dashboard"); ?>" class="">
                                    Hello
                                    <span class="icon-person"></span>
                                    <?php echo $this->ion_auth->user()->row()->email; ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php foreach ($cluster1 as $item) : ?>
                            <?php if ((!$this->ion_auth->logged_in() && $item->cluster_url == 'Sample') || ($this->ion_auth->in_group(array('GU')) && $item->cluster_url == 'Sample')) : ?>

                            <?php else : ?>
                                <li>
                                    <a href="<?php echo base_url("$coutry_iso" . "page/general/$item->cluster_url"); ?>"><?php echo $item->cluster_name; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <?php
                $country = 'Global';
                if ($this->ion_auth->logged_in()) {
                    $country = $this->ion_auth->user()->row()->country;
                    $exclude_group_id = $this->ion_auth->user()->row()->exclude_group;
                } else {
                    $country = $this->user_lang;
                }

                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('id', explode(',', $result->category));
                }

                $nation_lang = $this->datacontrol_model->getRowData('nation_lang', array('country' => $country));
                if (!empty($nation_lang)) {
                    $this->db->where('country', $country);
                } else {
                    $this->db->where('country', 'Global');
                }

                $this->db->where('cluster_type', 'ppf');
                $query = $this->db->get('cluster1');
                $cluster1 = $query->result();

                if ($exclude_group_id != '') {
                    $this->db->where('id', $exclude_group_id);
                    $query = $this->db->get('exclude_group');
                    $result = $query->row();

                    $this->db->where_not_in('level1', explode(',', $result->category));
                }
                if (!empty($nation_lang)) {
                    $this->db->where('country', $country);
                } else {
                    $this->db->where('country', 'Global');
                }
                $this->db->select('id,country,cluster_type,cluster_name,level1', 'cluster_url');
                $this->db->where('cluster_type', 'ppf');
                $this->db->order_by('cluster_name', 'asc');
                $query = $this->db->get('cluster2');
                $cluster2 = $query->result();
                //echo $this->db->last_query();


                ?>
                <!-- Mobile -->
                <form id="ppf_form_mobile" method="post" action="<?php echo base_url("page/ppf"); ?>">
                    <div class="select-list-block sp_view">
                        <p class="ttl">PPF Search</p>
                        <ul class="select-list">
                            <li class="maker">
                                <span class="icon-maker"></span>
                                <select name="cat_1" required>
                                    <option value="Select Maker">Maker</option>
                                    <?php foreach ($cluster1 as $item) : ?>
                                        <option value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_1') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="model">
                                <span class="icon-model"></span>
                                <select name="cat_2">
                                    <option value="">Select Model</option>
                                    <?php foreach ($cluster2 as $item) : ?>
                                        <?php if (empty($this->input->post('cat_1')) || $this->input->post('cat_1') == $item->level1) { ?>
                                            <option data-level1="<?php echo $item->level1; ?>"
                                                    value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_2') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                        <?php } else { ?>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                            <li class="part">
                                <span class="icon-part"></span>
                                <select name="parts">
                                    <option value="" <?php if ($_REQUEST['part'] == '') {
                                        echo 'selected';
                                    } ?>>Select Part
                                    </option>
                                    <option value="headlight" <?php if ($_REQUEST['part'] == 'headlight') {
                                        echo 'selected';
                                    } ?>>headlight
                                    </option>
                                    <option value="f-bumper" <?php if ($_REQUEST['part'] == 'f-bumper') {
                                        echo 'selected';
                                    } ?>>f-bumper
                                    </option>
                                    <option value="r-bumper" <?php if ($_REQUEST['part'] == 'r-bumper') {
                                        echo 'selected';
                                    } ?>>r-bumper
                                    </option>
                                    <option value="bumper-set" <?php if ($_REQUEST['part'] == 'bumper-set') {
                                        echo 'selected';
                                    } ?>>bumper-set
                                    </option>
                                    <option value="luggage" <?php if ($_REQUEST['part'] == 'luggage') {
                                        echo 'selected';
                                    } ?>>luggage
                                    </option>
                                    <option value="doorcup" <?php if ($_REQUEST['part'] == 'doorcup') {
                                        echo 'selected';
                                    } ?>>doorcup
                                    </option>
                                </select>
                            </li>
                            <!-- <li class="year">
                        <span class="icon-year"></span>
                        <select name="year">
                        <option value="Select Year">Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                      </select>
                    </li> -->
                        </ul>
                        <div class="search-btn">
                            <button type="submit">
                                <span>SEARCH</span>
                                <span class="icon-search02 ico-glass"></span>
                            </button>
                        </div>
                    </div>
                </form>


                <form id="ppf_form_desktop" method="get" action="#">
                    <input type="hidden" name="ppf" value="1"/>
                    <div class="header-search">
                        <a data-src=""
                           class="search-button"><?php echo $this->lang->line('navi_cut_film_title', FALSE); ?>
                            <span class="icon-search02 ico-glass"></span>
                        </a>
                        <div class="header-search-pop">
                            <div class="search-content">
                                <p class="ttl"><?php echo $this->lang->line('navi_cut_film_title_search', FALSE); ?></p>
                                <p class="lead"><?php echo $this->lang->line('navi_cut_film_easy_to', FALSE); ?></p>
                                <ul class="search-list">
                                    <li class="maker">
                                        <span class="icon-maker"><?php echo $this->lang->line('navi_cut_film_select_maker', FALSE); ?></span>
                                        <select name="cat_1" required>
                                            <option value="">Select Maker</option>
                                            <?php foreach ($cluster1 as $item) : ?>
                                                <option value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_1') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </li>
                                    <li class="model">
                                        <span class="icon-model"><?php echo $this->lang->line('navi_cut_film_select_model', FALSE); ?></span>
                                        <select name="cat_2">
                                            <option value="">Select Model</option>
                                            <?php foreach ($cluster2 as $item) : ?>
                                                <?php if (empty($this->input->post('cat_1')) || $this->input->post('cat_1') == $item->level1) { ?>
                                                    <option data-level1="<?php echo $item->level1; ?>"
                                                            value="<?php echo $item->id; ?>" <?php echo ($this->input->post('cat_2') == $item->id) ? 'selected' : ''; ?>><?php echo $item->cluster_name; ?></option>
                                                <?php } else { ?>
                                                <?php } ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </li>
                                    <li class="part">
                                        <span class="icon-part"><?php echo $this->lang->line('navi_cut_film_select_part', FALSE); ?></span>
                                        <select name="parts">
                                            <option value="" <?php if ($_REQUEST['parts'] == '') {
                                                echo 'selected';
                                            } ?>>Select Part
                                            </option>
                                            <option value="headlight" <?php if ($_REQUEST['parts'] == 'headlight') {
                                                echo 'selected';
                                            } ?>>headlight
                                            </option>
                                            <option value="f-bumper" <?php if ($_REQUEST['parts'] == 'f-bumper') {
                                                echo 'selected';
                                            } ?>>f-bumper
                                            </option>
                                            <option value="r-bumper" <?php if ($_REQUEST['parts'] == 'r-bumper') {
                                                echo 'selected';
                                            } ?>>r-bumper
                                            </option>
                                            <option value="bumper-set" <?php if ($_REQUEST['parts'] == 'bumper-set') {
                                                echo 'selected';
                                            } ?>>bumper-set
                                            </option>
                                            <option value="luggage" <?php if ($_REQUEST['parts'] == 'luggage') {
                                                echo 'selected';
                                            } ?>>luggage
                                            </option>
                                            <option value="doorcup" <?php if ($_REQUEST['parts'] == 'doorcup') {
                                                echo 'selected';
                                            } ?>>doorcup
                                            </option>
                                            <option value="aluminium-mold" <?php if ($_REQUEST['parts'] == 'aluminium-mold') {
                                                echo 'selected';
                                            } ?>>Aluminium Mold
                                            </option>

                                        </select>
                                    </li>
                                    <!-- <li class="year">
                            <span class="icon-year">Select Year</span>
                            <select name="year">
                            <option value="Select Year">Select Year</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                          </select>
                        </li> -->
                                </ul>
                                <div class="serch-btn">
                                    <button type="submit" id="sbtn2">
                                        <span><?php echo $this->lang->line('navi_cut_film_btn_search', FALSE); ?></span>
                                        <span class="icon-search02 ico-glass"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="clear: both"></div>
                <div class="lang mobile">
                    <div class="current_lang">
                        <span>Select Language</span>
                        <span class="icon-lang" id="lang-btn-sp"></span>
                        <?php echo (get_cookie('user_language') == '') ? "Global" : get_cookie('user_language'); ?>
                    </div>
                    <div class="language-select">
                        <?php
                        $user_country = $this->ion_auth->user()->row()->country;
                        if ($this->ion_auth->logged_in()) {
                            $this->db->where('country', $user_country);
                            $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                            if (empty($nation_lang)) {
                                $this->db->where('country', 'Global');
                                $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                            }
                        } else {
                            $this->db->where('is_active', 1);
                            $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
                        }

                        ?>
                        <ul id="lang-content-sp">
                            <?php foreach ($nation_lang as $item) : ?>
                                <?php
                                $this->db->where('nicename', '!=', 'JapanA');
                                $this->db->where('nicename', '!=', 'JapanB');
                                $this->db->where('nicename', $item->country);
                                $c = $this->datacontrol_model->getRowData('countries');
                                if (($item->country != 'JapanA') && ($item->country != 'JapanB')):
                                    ?>
                                    <li><input <?php echo ($user_country == $item->country) ? 'selected' : ''; ?>
                                                type="radio" name="language"
                                                data-iso="<?php echo strtolower($c->iso); ?>"
                                                value="<?php echo $item->country; ?>" data-label="language-select"
                                                data-value="<?php echo $item->country; ?>"><label
                                                for="language01"><?php echo ucwords(str_replace("_", " ", $item->country)) ?></label>
                                    </li>
                                <?php endif; endforeach; ?>
                        </ul>


                    </div>
                </div>
            </div>
        </div>
        <?php if (!$this->ion_auth->logged_in()) : ?>
            <div class="sp-login">
                <a href="" data-src="#modal01" class="sp-login-btn">
                    <span class="icon-person"></span>
                    Login
                </a>
            </div>
        <?php else : ?>
            <div class="sp-login">
                <a href="<?php echo base_url("/page/user/logout"); ?>">
                    <span class="icon-person"></span>
                    Logout
                </a>
            </div>
        <?php endif; ?>
    </div>
</header>
<script>
    var cat1 = "";
    var cat2 = "";
    var cat3 = "";
    var ppf_cluster1 = JSON.parse('<?php echo json_encode($cluster1); ?>');
    var ppf_cluster2 = JSON.parse('<?php echo json_encode($cluster2); ?>');

    $("#lang-btn-sp").click(function (event) {
        $("#lang-content-sp").slideToggle();
        $("html,body").animate({
            scrollTop: $('#hamburger').offset().top
        }, 1000);
    });

    $(function () {
        $('#ppf_form_desktop select[name="cat_1"]').change(function () {
            $('#ppf_form_desktop select[name="cat_2"]').html('');
            $('#ppf_form_desktop select[name="parts"]').html('<option value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_desktop select[name="cat_2"]').append('<option value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option value="">Select Model</option>';
            $('#ppf_form_desktop select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_desktop select[name="cat_2"]').prop("selectedIndex", 0);

            cat1 = $('#ppf_form_desktop select[name="cat_1"] option:selected').text();
            cat1_id = $('#ppf_form_desktop select[name="cat_1"] option:selected').val();
            //cat1 = cat1.replace(" ", "-");
            //cat1 = cat1.replace("/", " ");

        });

        $('#ppf_form_desktop select[name="cat_2"]').change(function () {
            cat2 = $('#ppf_form_desktop select[name="cat_2"] option:selected').text();
            cat2_id = $('#ppf_form_desktop select[name="cat_2"] option:selected').val();
            $('#ppf_form_desktop select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: { cat1 : cat1_id, cat2: cat2_id },
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_desktop select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_desktop select[name="parts"]').append('<option value="' + response[i].parts + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });
        });

        $('#ppf_form_desktop select[name="parts"]').change(function () {

            cat3 = $('#ppf_form_desktop select[name="parts"] option:selected').text();
            //cat3 = cat3.replace(" ", "-");
            //cat3 = cat3.replace("/", " ");
        });

        $(document).ready(function () {
            $("#ppf_form_desktop").submit(function () {
                if (cat1 != 'Select-Maker') {
                    str = cat1;

                    if (cat2 != 'Select-Model') {
                        str = cat1 + '/' + cat2;
                    }

                    if (cat3 != 'Select-Part') {
                        str = cat1 + '/' + cat2 + "/" + cat3;
                    }
                }
                str = str.replace("//", "/");
                str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
                //str_url = 'https://<?php //echo $_SERVER['SERVER_NAME'].$coutry_iso; ?>//page/ppf/'+str.toLowerCase();
                console.log(str_url)
                window.location.replace(str_url);
                return false;
            });
        });

        $('#ppf_form_mobile select[name="cat_1"]').change(function () {

            $('#ppf_form_mobile select[name="cat_2"]').html('');
            $('#ppf_form_mobile select[name="parts"]').html('<option value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_mobile select[name="cat_2"]').append('<option value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option value=">Select Model</option>';
            $('#ppf_form_mobile select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_mobile select[name="cat_2"]').prop("selectedIndex", 0);

            cat1 = $('#ppf_form_mobile select[name="cat_1"] option:selected').text();
            cat1_id = $('#ppf_form_mobile select[name="cat_1"] option:selected').val();
            //cat1 = cat1.replace(" ", "-");
            //cat1 = cat1.replace("/", " ");
        });

        $('#ppf_form_mobile select[name="cat_2"]').change(function () {
            cat2 = $('#ppf_form_mobile select[name="cat_2"] option:selected').text();
            cat2_id = $('#ppf_form_mobile select[name="cat_2"] option:selected').val();
            $('#ppf_form_mobile select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: { cat1 : cat1_id, cat2: cat2_id },
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_mobile select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_mobile select[name="parts"]').append('<option value="' + response[i].parts + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });
        });

        $('#ppf_form_mobile select[name="parts"]').change(function () {
            cat3 = $('#ppf_form_mobile select[name="parts"] option:selected').text();
            //cat3 = cat3.replace(" ", "-");
            //cat3 = cat3.replace("/", " ");
        });

        $(document).ready(function () {
            $("#ppf_form_mobile").submit(function () {
                if (cat1 != 'Select-Maker') {
                    str = cat1;

                    if (cat2 != 'Select-Model') {
                        str = cat1 + '/' + cat2;
                    }

                    if (cat3 != 'Select-Part') {
                        str = cat1 + '/' + cat2 + "/" + cat3;
                    }
                }
                str = str.replace("//", "");
                str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
                //console.log(str_url)
                window.location.replace(str_url);
                return false;
            });
        });


        $('#ppf_form_home select[name="cat_1"]').change(function () {
            $('#ppf_form_home select[name="cat_2"]').html('');
            $('#ppf_form_home select[name="parts"]').html('<option value="">Select Part</option>');
            for (i = 0; i < ppf_cluster2.length; i++) {
                if (ppf_cluster2[i].level1 == $(this).val()) {
                    $('#ppf_form_home select[name="cat_2"]').append('<option value="' + ppf_cluster2[i].id + '">' + ppf_cluster2[i].cluster_name + '</option>');
                }
            }
            var option_none = '<option value="">Select Model</option>';
            $('#ppf_form_home select[name="cat_2"]').prepend(option_none);
            $('#ppf_form_home select[name="cat_2"]').prop("selectedIndex", 0);

            cat1 = $('#ppf_form_home select[name="cat_1"] option:selected').text();
            cat1_id = $('#ppf_form_home select[name="cat_1"] option:selected').val();
        });

        $('#ppf_form_home select[name="cat_2"]').change(function () {
            cat2 = $('#ppf_form_home select[name="cat_2"] option:selected').text();
            cat2_id = $('#ppf_form_home select[name="cat_2"] option:selected').val();
            $('#ppf_form_home select[name="parts"]').html('');

            $.ajax({
                method: "POST",
                url: '<?php echo base_url("page/product/get_parts") ?>',
                data: { cat1 : cat1_id, cat2: cat2_id },
                success: function (result) {
                    var option_none = '<option value="">Select Part</option>';
                    var response = JSON.parse(result);
                    $('#ppf_form_home select[name="parts"]').prepend(option_none);
                    for (i = 0; i < response.length; i++) {
                        $('#ppf_form_home select[name="parts"]').append('<option value="' + response[i].parts + '">'
                            + response[i].parts + '</option>');
                    }
                }
            });

        });

        $('#ppf_form_home select[name="parts"]').change(function () {
            cat3 = $('#ppf_form_home select[name="parts"] option:selected').text();

        });
    });
    $(document).ready(function () {
        $("#ppf_form_home").submit(function () {
            if (cat1 != 'Select-Model') {
                str = cat1;

                if (cat2 != 'Select-Model') {
                    str = cat1 + '/' + cat2;
                }

                if (cat3 != 'Select-Model') {
                    str = cat1 + '/' + cat2 + "/" + cat3;
                }
            }
            str = str.replace("//", "/");
            str_url = '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>' + str.toLowerCase();
            window.location.replace(str_url);
            return false;
        });
    });

    function submitFilter(cat1, cat2) {
        if (cat1 != "" && cat2 != "") {
            console.log("cat2")
        }
        $('#ppf_form_desktop').attr('action', '<?php echo base_url("$coutry_iso" . "page/ppf/"); ?>');


    }
</script>