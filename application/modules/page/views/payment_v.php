<?php
$cart = $this->session->userdata('sensha_cart');
$shipping_method = $_SESSION['shipping_oversea']['shipping_name'];
if ($shipping_method == 'BUYER TRANSPORTATION') {
  $shipping_method = 'EXW';
} elseif ($shipping_method == 'SELLER TRANSPORTATION') {
  $shipping_method = 'CIF';
}
?>
<style>
  .no_method,
  .need_method,
  .clr.payment {
    display: none;
  }

  .has-error {
    display: none;
    color: red;
  }
</style>
<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="<?php echo base_url("$coutry_iso"); ?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_payment', false); ?></span>
    </div>
    <div class="box-content">
      <div class="layout-contain">
        <div class="box-progress">
          <div class="progress-row">
            <div class="progress-step">
              <button type="button" class="btn-circle">1</button>
              <p>Cart item&amp; Delivery</p>
            </div>
            <div class="progress-step">
              <button type="button" class="btn-circle">2</button>
              <p>Shipping Address</p>
            </div>
            <div class="progress-step">
              <button type="button" class="btn-circle step-active">3</button>
              <p>Confirmation&amp; Payment</p>
            </div>
            <div class="progress-step">
              <button type="button" class="btn-circle c-blue">4</button>
              <p>Complete Order</p>
            </div>
          </div>
        </div>

        <div class="clr choose-discount">
          <div class="topic">
            <p class="title-page"><?php echo $this->lang->line('page_payment_credit_point', false); ?></p>
          </div>
          <div class="row-discount">
            <ul>
              <li>
                <div>
                  <!-- <input type="radio" id="01" name="selector"> -->
                  <label for="01">
                    <input type="radio" name="discount_type" value="none_discount" id="select_none_discount">
                    <p><?php echo $this->lang->line('page_payment_without_coupon', false); ?></p>
                  </label>
                </div>
              </li>
            </ul>
          </div>
          <?php if ($this->ion_auth->in_group(array('SA'))) : ?>
            <div class="row-discount">
              <ul>
                <li>
                  <div>
                    <!-- <input type="radio" id="01" name="selector"> -->
                    <label for="01">
                      <input type="radio" name="discount_type" value="use_point" id="select_use_point">
                      <p><?php echo $this->lang->line('page_payment_use_credit', false); ?></p>
                      <span><?php echo $this->lang->line('page_payment_your_credit1', false); ?> <?php echo number_format($sales_history_credit->balance); ?> <?php echo $this->lang->line('page_payment_your_credit2', false); ?></span>
                    </label>
                  </div>
                </li>
                <li>
                  <input type="number" id="point" placeholder="<?php echo $this->lang->line('page_payment_enter_points', false); ?>" class="form-control">
                  <span class="has-error error_point">Fail</span>
                </li>
                <li>
                  <a href="javascript:void(0)" id="use_point" class="b-blue">Apply</a>
                </li>
              </ul>
            </div>
            <div class="row-discount">
              <ul>
                <li>
                  <div>
                    <!-- <input type="radio " id="01" name="selector"> -->
                    <label for="01">
                      <input type="radio" name="discount_type" value="use_all_point" id="select_use_all_point">
                      <p><?php echo $this->lang->line('page_payment_use_all', false); ?></p>
                      <span><?php echo $this->lang->line('page_payment_you_can_use', false); ?></span>
                    </label>
                  </div>
                </li>
                <li>
                  <input type="text" id="all_point" placeholder="S-3.2-PC" class="form-control" value="<?php echo number_format($sales_history_credit->balance); ?>" readonly>
                  <span class="has-error error_all_point">Fail</span>
                </li>
                <li>
                  <a href="javascript:void(0)" id="use_all_point" class="b-blue">Apply</a>
                </li>
              </ul>
            </div>
          <?php endif; ?>
          <?php if ((isset($cart['domestic']) && !empty($cart['domestic'])) || ($this->ion_auth->in_group(array('GU')))) : ?>
            <div class="row-discount">
              <ul>
                <li>
                  <div>
                    <!-- <input type="radio" id="01" name="selector"> -->
                    <label for="01">
                      <input type="radio" name="discount_type" value="use_coupon" id="select_use_coupon">
                      <p><?php echo $this->lang->line('page_payment_coupon', false); ?></p>
                      <span><?php echo $this->lang->line('page_payment_apply_coupon', false); ?></span>
                    </label>
                  </div>
                </li>
                <li>
                  <input type="text" id="coupon_code" placeholder="<?php echo $this->lang->line('page_payment_enter_coupon', false); ?>" class="form-control">
                  <span class="has-error error_coupon"><?php echo $this->lang->line('page_payment_can_not_use', false); ?></span>
                </li>
                <li>
                  <a href="javascript:void(0)" id="use_coupon" class="b-blue">Apply</a>
                </li>
              </ul>
            </div>
          <?php endif; ?>
        </div>


        <div class="clr payment">
          <div class="topic">
            <p class="title-page"><?php echo $this->lang->line('page_payment_choose_payment', false); ?></p>
          </div>
          <div class="no_method" style="color:red;">
            <?php echo $this->lang->line('you_donot_need_to_pay', false); ?>
          </div>
          <div class="need_method">
            <ul>

              <!-- japan -->
              <?php if ($payment_mode == 'bank_trnsfer' || $payment_mode == 'no_zone') : ?>

                <li onclick="add_payWith('Bank Transfer')">
                  <input type="radio" id="r-five" name="payment_method">
                  <label for="r-five">
                    <p>
                      Bank Transfer
                    </p>
                    <span><?php echo $this->lang->line('page_payment_pay_with_bank_tranfer', false); ?></span>
                  </label>
                </li>

              <?php else : ?>
                <?php if ($shipping_method != 'EXW' && $shipping_method != 'CIF') { ?>
                  <li onclick="add_payWith('amazon')">
                    <label for="r-one">
                      <input type="radio" id="r-one" name="payment_method">
                      <p>Amazon <span><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/logo-amazon.png"></span></p>
                      <span><?php echo $this->lang->line('page_payment_pay_with_amazon', false); ?></span>
                    </label>
                  </li>
                  <li onclick="add_payWith('paypal')">
                    <input type="radio" id="r-two" name="payment_method">
                    <label for="r-two">
                      <p>Paypal <span><img src="<?php echo base_url("assets/sensha-theme/"); ?>images/logo-paypal.png"></span></p>
                      <span><?php echo $this->lang->line('page_payment_pay_with_paypal', false); ?></span>
                    </label>
                  </li>
                <?php } ?>

                <?php
                if ($user_country == 'Japan' || $user_country == 'JapanA' || $user_country == 'JapanB' || $shipping_method == 'EXW' || $shipping_method == 'CIF') : ?>
                  <li onclick="add_payWith('Bank Transfer')">
                    <input type="radio" id="r-five" name="payment_method">
                    <label for="r-five">
                      <p>
                        Bank Transfer
                      </p>
                      <span><?php echo $this->lang->line('page_payment_pay_with_bank_tranfer', false); ?></span>
                    </label>
                  </li>
                <?php endif; ?>
                <?php if ($user_country == 'Japan' && ($shipping_domestic['shipping_name'] == '宅配便') || ($user_country == 'JapanA' || $user_country == 'JapanB')) : ?>
                  <?php if ($shipping_method != 'EXW' && $shipping_method != 'CIF') { ?>
                    <li onclick="add_payWith('pay on delivery')">
                      <input type="radio" id="r-four" name="payment_method">
                      <label for="r-four">
                        <p>
                          Pay On Delivery
                        </p>
                        <span><?php echo $this->lang->line('page_payment_pay_on_delivery', false); ?></span>
                      </label>
                    </li>
                  <?php } ?>

                <?php endif; ?>
                <!-- ZUN update -->
              <?php endif; ?>
            </ul>
          </div>
        </div>
        <!--<div class="line-p">
    <a href="complete.php" class="b-yellow"><?php echo $this->lang->line('page_product_detail_right_side_confirm_pay', false); ?>
    <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-check.png" style="width:20px;margin-left:5px;">
  </a>
</div>-->
        <!--<div class="clr box_order_confirm">
<div class="topic">
<p class="title-page"><?php echo $this->lang->line('page_payment_pay', false); ?></p>
</div>
<div class="total-domestic">
<div class="d2">
<span class="b-gray"><?php echo $this->lang->line('page_product_detail_domestic', false); ?></span>
<p><span style="font-size:18px;font-weight:600;">5</span> <?php echo $this->lang->line('page_product_detail_right_side_items', false); ?></p>
<p><?php echo $this->lang->line('page_product_detail_right_side_subtotal', false); ?> <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
</div>
<div style="background:#fff;">
<ul class="list-items">
<li>
<p><?php echo $this->lang->line('page_product_detail_import_shipping', false); ?></p>
<span>1,200 Yen</span>
</li>
<li>
<p><?php echo $this->lang->line('page_product_detail_import_duty', false); ?></p>
<span>820  Yen</span>
</li>
<li>
<p><?php echo $this->lang->line('page_product_detail_right_side_minimum_cost', false); ?></p>
<span>900  Yen</span>
</li>
</ul>
<div class="total clr">
<p><?php echo $this->lang->line('page_product_detail_right_side_total', false); ?></p>
<span class="txt-red">8,180 Yen</span>
</div>
</div>
</div>
<div class="total-oversea">
<div class="d2">
<span class="b-gray"><?php echo $this->lang->line('page_product_detail_international', false); ?></span>
<p><span style="font-size:18px;font-weight:600;">5</span> <?php echo $this->lang->line('page_product_detail_right_side_items', false); ?></p>
<p><?php echo $this->lang->line('page_product_detail_right_side_subtotal', false); ?> <span class="txt-red" style="font-style:normal;">5,180 Yen</span></p>
</div>
<div style="background:#fff;">
<ul class="list-items">
<li>
<p><?php echo $this->lang->line('page_product_detail_import_shipping', false); ?></p>
<span>1,200 Yen</span>
</li>
<li>
<p><?php echo $this->lang->line('page_product_detail_import_duty', false); ?></p>
<span>820  Yen</span>
</li>
<li>
<p><?php echo $this->lang->line('page_product_detail_right_side_minimum_cost', false); ?></p>
<span>900  Yen</span>
</li>
</ul>
<div class="total-price">
<div class="total clr">
<p><?php echo $this->lang->line('page_product_detail_right_side_total', false); ?></p>
<span class="txt-red">8,180 Yen</span>
</div>
<div class="total clr">
<p style="float: none;"><?php echo $this->lang->line('page_product_detail_right_side_total', false); ?>Grand Total</p>
<span class="txt-red" style="display: block;float: none;text-align:right;font-size:16px;font-weight: 700;margin-top: 5px;">16,360 Yen</span>
</div>
<div class="total clr">
<p>Discount</p>
<span class="txt-red">6,360 Yen</span>
</div>
</div>
<div class="total">
<p>Net to pay</p>
<span class="txt-red" style="display: block;float: none;text-align:right;font-size:16px;font-weight: 700;margin-top: 5px;">10,000 Yen</span>
</div>
</div>
</div>
<div class="line-p">
<a href="complete.php" class="b-yellow"><?php echo $this->lang->line('page_product_detail_right_side_confirm_pay', false); ?>
<img src="<?php echo base_url("assets/sensha-theme/"); ?>images/icon-check.png" style="width:20px;margin-left:5px;">
</a>
</div>

</div>-->
      </div>
    </div>
    <!--box-content-->
    <aside class="col-right">
      <div id="product_detail_cart">
        <?php echo $this->load->view('page/product/product_detail_cart_v', array('cart_next_page' => 'page/payment_complete')); ?>
      </div>
    </aside>
  </div>
  <!--inner-->
</div>
<!--container-->
<?php
// print_r($cart);
?>

<!-- <form action="https://secure.paypal.com/uk/cgi-bin/webscr" method="post" name="paypal" id="form_paypal" > -->


<script>
  $(function() {
    // if($('mytotal').data('total') == 0){
    //   $('.need_method').hide();
    //   $('.no_method').show();
    // }
    // else {
    //   $('.need_method').show();
    //   $('.no_method').hide();
    // }

    $('input[name="discount_type"]').change(function() {
      if ($(this).val() == 'none_discount') {
        $('.clr.payment, .need_method').show();
      } else {
        $('.clr.payment, .need_method').hide();
      }
    });

    $('#point').click(function() {
      $('#select_use_point').prop('checked', true);
    });

    $('#coupon_code').click(function() {
      $('#select_use_coupon').prop('checked', true);
    });

    $('#point').click(function() {
      $('#select_use_point').eq(1).prop('checked', true);
    });

    $('#coupon_code').click(function() {
      $('#select_use_all_point').eq(1).prop('checked', true);
    });
  });


  function add_payWith2(name) {
    console.log(name);
  }

  var pay_with = '';

  function add_payWith(name) {
    //console.log(name);

    pay_with = name;
    $.ajax({
      url: '<?php echo base_url("page/product/add_payWith/"); ?>',
      type: 'POST',
      data: {
        name: name
      },
      success: function(data) {
        $.ajax({
          url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
          type: 'GET',
          success: function(data) {
            $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
          }
        });
      }
    });
  }



  $('#use_coupon').click(function() {
    $('.has-error').hide();
    if ($('#coupon_code').val() != '') {
      $.ajax({
          method: "POST",
          url: '<?php echo base_url("page/check_coupon"); ?>',
          data: {
            coupon_code: $('#coupon_code').val()
          }
        })
        .done(function(msg) {
          // console.log(msg);
          // location.reload();
          if (msg == 1) {
            $('input[name="discount_type"]').attr('disabled', true);
            $.ajax({
              url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
              type: 'GET',
              success: function(data) {
                $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                if ($('mytotal').data('total') == 0) {
                  $('.need_method').hide();
                  $('.clr.payment, .no_method').show();
                } else {
                  $('.clr.payment, .need_method').show();
                  $('.no_method').hide();
                }
              }
            });
            pay_with = 'use coupon';
          } else {
            // alert('not found coupon');
            $.ajax({
              url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
              type: 'GET',
              success: function(data) {
                $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
              }
            });

            pay_with = '';
            $('.clr.payment').hide();
            $('input[name="discount_type"]').removeAttr('disabled');
            $('.has-error.error_coupon').show();
          }
        })
        .fail(function(msg) {
          console.log(msg);
        });
    } else {
      alert('<?php echo $this->lang->line('page_payment_no_found_coupon', false); ?>');
    }
  });

  $('#use_point').click(function() {
    $('.has-error').hide();
    var p = '<?php echo round($sales_history_credit->balance); ?>'.replace(/\,/g, '');

    if ($('#point').val() != '') {
      if (parseInt($('#point').val()) > parseInt(p)) {
        // console.log(p);
        // console.log($('#point').val());
        alert("Can't use point more than point you have");
        return false;
      } else {
        $.ajax({
            method: "POST",
            url: '<?php echo base_url("page/use_point"); ?>',
            data: {
              point: $('#point').val()
            }
          })
          .done(function(msg) {
            // console.log(msg);
            // location.reload();
            if (msg == 1) {
              $('input[name="discount_type"]').attr('disabled', true);
              $.ajax({
                url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
                type: 'GET',
                success: function(data) {
                  $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                  if ($('mytotal').data('total') == 0) {
                    $('.need_method').hide();
                    $('.clr.payment, .no_method').show();
                  } else {
                    $('.clr.payment, .need_method').show();
                    $('.no_method').hide();
                  }
                }
              });
              pay_with = 'use point';
            } else {
              // alert('not found coupon');
              $.ajax({
                url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
                type: 'GET',
                success: function(data) {
                  $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                }
              });

              pay_with = '';
              $('.clr.payment').hide();
              $('input[name="discount_type"]').removeAttr('disabled');
              $('.has-error.error_point').show();
            }
          })
          .fail(function(msg) {
            console.log(msg);
          });
      }

    } else {
      alert('input you point');
    }
  });

  $('#use_all_point').click(function() {
    $('#select_use_all_point').prop('checked', true);
    $('.has-error').hide();
    if ($('#all_point').val() != '') {
      $.ajax({
          method: "POST",
          url: '<?php echo base_url("page/use_all_point"); ?>',
          data: {
            all_point: $('#all_point').val().replace(/\,/g, '')
          }
        })
        .done(function(msg) {
          // console.log(msg);
          // location.reload();
          if (msg == 1) {
            $('input[name="discount_type"]').attr('disabled', true);
            $.ajax({
              url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
              type: 'GET',
              success: function(data) {
                $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
                if ($('mytotal').data('total') == 0) {
                  $('.need_method').hide();
                  $('.clr.payment, .no_method').show();
                } else {
                  $('.clr.payment, .need_method').show();
                  $('.no_method').hide();
                }
              }
            });
            pay_with = 'use all point';
          } else {
            // alert('fail');
            $.ajax({
              url: '<?php echo base_url("page/product/get_product_detail_cart_v"); ?>',
              type: 'GET',
              success: function(data) {
                $('#product_detail_cart').html('<div class="p-shipping">' + $(data).html() + '</div>');
              }
            });

            pay_with = '';
            $('.clr.payment').hide();
            $('input[name="discount_type"]').removeAttr('disabled');
            $('.has-error.error_all_point').show();
          }
        })
        .fail(function(msg) {
          console.log(msg);
        });
    } else {
      alert('<?php echo $this->lang->line('page_payment_no_found_coupon', false); ?>');
    }
  });

  function payWith() {
    $('#pay_btn').attr('disabled', 'disabled');
    var checked = 0;
    $('input[name="payment_method"]').each(function() {
      if ($(this).prop('checked')) {
        checked++;
      }
    });

    if (!$('input[name="discount_type"]').is(':checked')) {
      alert("Please choose discount");
      $('#pay_btn').removeAttr('disabled');
      return false;
    }

    var pass = true;

    if ($('mytotal').data('total') > 0) {
      if (checked == 0) {
        alert('Please choose payment method');
        $('#pay_btn').removeAttr('disabled');
        pass = false;
      }
    }

    if (pass) {
      var payments_url = '<?php echo $this->config->item('payments_url'); ?>';
      var sensha_pay = ['pay on delivery', 'Bank Transfer', 'use all point', 'use point'];

      if (pay_with == 'paypal') {
        //$('#form_payment_gateway').attr('action', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
        // $('#form_payment_gateway').attr('action', 'https://www.paypal.com/cgi-bin/webscr');
        // $('#form_payment_gateway').submit();
        $.ajax({
          url: '<?php echo base_url("page/save_order"); ?>',
          type: 'GET',
          success: function(data) {
            r = JSON.parse(data);
            console.log(r.order_data)
              var amount =  window.atob($('#form_payment_gateway input[name="amount_1"]').val());
              var check = true;
              if(r.order_data.data_domestic_sale_total != ''){
                   if(amount != r.order_data.data_domestic_sale_total.payment_total_amount){
                       check = false;
                   }
              }else{
                  if(amount != r.order_data.data_oversea_sale_total.payment_total_amount){
                      check = false;
                  }
              }
            if(check) {
                if (r.shipment_id) {
                    var amount = window.atob($('#form_payment_gateway input[name="amount_1"]').val())

                    $('#form_payment_gateway input[name="amount_1"]').val(amount);
                    $('#form_payment_gateway input[name="invoice"]').val(r.shipment_id);
                    $('#form_payment_gateway input[name="custom"]').val(r.shipment_id);
                    $('#form_payment_gateway').attr('action', payments_url);
                    $('#form_payment_gateway').submit();
                } else {
                    alert("No invoice ID");
                }
            }else{
                window.location.replace('<?php echo base_url("page/payment_cancel"); ?>');
            }
          }
        });
      } else if (pay_with == 'amazon') {
        // $('#form_payment_gateway').attr('action', 'https://<?php echo $_SERVER['SERVER_NAME']; ?>/payment_sensha/payment');
        // $('#form_payment_gateway').submit();
        // alert('Payment method not available');
        $.ajax({
          url: '<?php echo base_url("page/save_order"); ?>',
          type: 'GET',
          success: function(data) {
            r = JSON.parse(data);
            
            $('#form_payment_gateway input[name="invoice"]').val(r.shipment_id);
            $('#form_payment_gateway input[name="custom"]').val(r.shipment_id);
            $('#form_payment_gateway').attr('action', 'https://<?php echo $_SERVER['SERVER_NAME']; ?>/payment_sensha/payment');
            $('#form_payment_gateway').submit();
          }
        });
      } else if (pay_with == 'alibaba') {
        alert('Payment method not available');
      } else if (sensha_pay.indexOf(pay_with) != -1) {
        $.ajax({
          url: '<?php echo base_url("page/save_order?use="); ?>' + pay_with,
          type: 'GET',
          success: function(data) {
              
                if(data) {
                    window.location = '<?php echo base_url("page/payment_complete"); ?>';
                }
          }
        });
      }
    }

    return false;
  }
</script>