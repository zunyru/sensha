<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="index.php">Home</a></span><span>Password reset</span>
    </div>
  </div><!--inner-->
  <div class="clr inner">
    <div class="box-content">
      <div class="layout-contain">
        <div class="clr box_form">
          <div class="topic">
            <p class="title-page">Authentication email sent successfully</p>
          </div>
          <div class="clr box-success">
            <div class="left">
              <img src="<?php echo base_url("assets/sensha-theme/");?>images/img-mail.png">
            </div>
            <div class="right">
              <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;">The email for authentication was successfully sent</h2>
              <p>Please check your email / SMS and click the link to authenticate your account.</p>
            </div>
          </div>
        </div>
      </div><!--layout-contain-->
    </div><!--inner-->
  </div><!--container-->
</div>
