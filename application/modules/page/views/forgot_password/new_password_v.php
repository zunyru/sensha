<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="index.php">Home</a><span><?php echo $this->lang->line('breadcrumb_new_pass', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
  <div class="layout-contain">
     <div class="clr box_form">
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_new_pass_title', FALSE); ?></p>
      </div>
      <div class="box-paragraph">
        <p><?php echo $this->lang->line('page_new_pass_message1', FALSE); ?></p>
      </div>
      <form method="post" action="<?php echo base_url("page/forgot_password/reset_password");?>">
        <input type="hidden" name="code" value="<?php echo $code;?>">
        <div class="box-inner">
          <div class="r-inline">
             <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png">Password</label>
             <div class="r-input">
              <input type="password" placeholder="Please input your new password" class="form-control" name="new">
             </div>
            </div>
          <div class="r-inline">
             <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/08.png">Password</label>
             <div class="r-input">
               <input type="password" placeholder="For confirmation" class="form-control" name="old">
             </div>
          </div>
          <div class="row-btn">
             <button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_new_pass_message2', FALSE); ?></button>
          </div>
         </div>
      </form>
    </div>
  </div><!--layout-contain-->
</div><!--inner-->
</div><!--container-->
