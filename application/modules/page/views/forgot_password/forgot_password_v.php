<div class="container">
  <div class="clr inner">
    <div id="breadcrumbs">
      <span><a href="index.php">Home</a><span><?php echo $this->lang->line('breadcrumb_forgot_pass', FALSE); ?></span>
    </div>
  </div><!--inner-->
  <div class="clr inner">
    <div class="layout-contain">
      <div class="clr box_form">
        <div class="topic">
          <p class="title-page"><?php echo $this->lang->line('page_forgot_pass_title', FALSE); ?></p>
        </div>
        <div class="box-paragraph">
          <p><?php echo $this->lang->line('page_forgot_pass_message1', FALSE); ?></p>
        </div>
        <form method="POST" action="<?php echo base_url("page/forgot_password/request_new_password");?>">
          <div class="box-inner">
            <div class="clr inline-radio">
              <div class="left">
                <label class="">
                  <input type="radio" name="register_by" value="email" checked required> <?php echo $this->lang->line('page_register_email', FALSE); ?>
                </label>
              </div>
              <div class="left">
                <label class="">
                  <input type="radio" name="register_by" value="SMS" required> <?php echo $this->lang->line('page_register_sms', FALSE); ?>
                </label>
              </div>
            </div>
            <div class="r-inline email">
              <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png">Email</label>
              <div class="r-input">
                <input type="email" placeholder="Required if choose email" class="form-control" name="email">
              </div>
            </div>
            <div class="r-inline sms">
              <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png">Telephone</label>
              <div class="r-input">
                <input type="text" placeholder="Please input your Telephone" class="form-control" name="phone">
              </div>
            </div>
            <div class="row-btn">
              <button type="submit" class="b-blue"><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-save.png" style="width:16px;margin-right:5px;"><?php echo $this->lang->line('page_forgot_pass_message2', FALSE); ?></button>
            </div>
          </div>
        </form>
      </div>
    </div><!--layout-contain-->
  </div><!--inner-->
</div><!--container-->
<!-- <script src="js/main.js"></script>  -->

<script>
$(function(){
	$('.sms').hide();
	$('input[name="register_by"]').change(function(){
		if($(this).val() == 'email'){
			$('.sms').hide();
			$('.email').show();

			$('input[name="phone"]').removeAttr('required','')
			$('input[name="email"]').attr('required','')
		}
		if($(this).val() == 'SMS'){
			$('.sms').show();
			$('.email').hide();

			$('input[name="phone"]').attr('required','')
			$('input[name="email"]').removeAttr('required','')
		}
	});
});

</script>
