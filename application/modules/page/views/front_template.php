<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $htmlTitle; ?></title>
    <link rel="canonical" href="<?php echo base_url(uri_string()); ?>"/>
    <meta name="description" content="<?php echo $htmlDes; ?>">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NJ68PFT');</script>
    <!-- End Google Tag Manager -->

    <?php if (!$this->ion_auth->logged_in()): ?>
        <?php
        $uri = uri_string();
        if ($this->coutry_iso != '') {
            $uri = str_replace("$this->coutry_iso", "", $uri);
        }

        $this->db->where('is_active', 1);
        $nation_lang = $this->datacontrol_model->getAllData('nation_lang');
        ?>
        <?php foreach ($nation_lang as $item): ?>
            <?php
            $this->db->where('nicename', $item->country);
            $c = $this->datacontrol_model->getRowData('countries');
            ?>

            <?php if ($item->country == "Global"): ?>
                <link rel="alternate" href="<?php echo base_url($uri); ?>" hreflang="en"/>
            <?php else: ?>
                <link rel="alternate" href="<?php echo base_url(strtolower($c->iso) . $uri); ?>"
                      hreflang="<?php echo strtolower($c->iso); ?>"/>
            <?php endif; ?>

        <?php endforeach; ?>
    <?php else: ?>
        <link rel="alternate" href="<?php echo base_url(uri_string()); ?>" hreflang="<?php echo $this->coutry_iso; ?>"/>
    <?php endif; ?>

    <?php if ($noindex): ?>
        <meta name=”robots” content="noindex">
    <?php endif; ?>

    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/animation.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/home.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/common.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/page.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/menu-mobile.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/jquery.bxslider.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/"); ?>css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://www.at-aroma.com/js/common/html5.js"></script>
    <![endif]-->
    <script type="text/javascript"
            src="<?php echo base_url("assets/sensha-theme/"); ?>js/jquery.mb.YTPlayer.min.js"></script>
    <!-- youtube video player -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/packages/icons/css/icons.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/css/youtube-video-player.min.css"/>
    <script type="text/javascript"
            src="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/js/youtube-video-player.jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.css"/>
    <script type="text/javascript"
            src="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url("assets/sensha-theme/"); ?>youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js"></script>
    <!-- youtube video player end -->
    <?php if (get_cookie('user_language') == 'Japan'): ?>
        <link href="<?php echo base_url("assets/sensha-theme/"); ?>css/japanese.css" rel="stylesheet">
    <?php endif; ?>
    <script>
        // YTPlayer実行
        $(function () {
            $('#youtube').YTPlayer();
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#myList').youtube_video({
                playlist: 'PLZUp1F67_y_cs43qqJc_Y3-c09SWY-ZVe',
                playlist_type: 'horizontal',
                api_key: 'AIzaSyCjVxfyjjLbo4IDH5rHbCpW7watylFE8J4',
            });
        });
    </script>

</head>
<body <?php echo (get_cookie('user_language') == 'Japan') ? 'id="japanese"' : ''; ?> class>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJ68PFT"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="wrapper">
    <div id="content">
        <?php $this->load->view('header_v'); ?>
        <?php $this->load->view($page); ?>
        <?php $this->load->view('footer_v'); ?>
    </div>
    <!-- .wrapper -->
