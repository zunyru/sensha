<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url();?>">Home</a><span><?php echo $this->lang->line('breadcrumb_cancel', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="box-progress">
        <div class="progress-row">
          <div class="progress-step">
            <button type="button" class="btn-circle">1</button>
            <p>Cart item& Delivery</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">2</button>
            <p>Shipping Address</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">3</button>
            <p>Confirmation& Payment</p>
          </div>
          <div class="progress-step">
            <button type="button" class="btn-circle">4</button>
            <p>Cancel Order</p>
          </div>
        </div>
      </div>
      <div class="topic">
        <p class="title-page"><?php echo $this->lang->line('page_cancel_order_confirmation', FALSE); ?></p>
      </div>
      <div class="clr box_complete">
        <div class="left">
            <img src="<?php echo base_url("assets/sensha-theme/");?>images/img-complete.png">
        </div>
        <div class="right">
          <h2><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-check-g.png" style="width: 20px;margin-right:5px;"><?php echo $this->lang->line('page_cancel_your_order', FALSE); ?></h2>
          <?php echo $this->lang->line('page_cancel_thank_you', FALSE); ?>
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->
