<div class="clr inner">
<div id="breadcrumbs" style="margin:15px 0;">
  <span><a href="<?php echo base_url();?>">HOME</a><span> <?php echo $this->lang->line('breadcrumb_sole_agent', FALSE); ?></span></span>
</div>
</div>
  <div class="top-banner b-sole-agent">
     <div class="caption-b">
  <h2><?php echo htmlspecialchars_decode($page_content->content_a);?></h2>
  <p><?php echo htmlspecialchars_decode($page_content->content_b);?></p>
 </div>
  </div>
<div class="clr box-sole">
  <div class="wrap">
    <div class="topic2">
    <p class="title-page"><?php echo htmlspecialchars_decode($page_content->content_c);?></p>
    </div>
    <p><?php echo htmlspecialchars_decode($page_content->content_d);?></p>
  </div>
</div>
<div class="page-s-agent">
  <div class="wrap">
    <div class="clr box-benefits">
      <div class="topic2">
      <p class="title-page"><?php echo $this->lang->line('page_sole_agent_section2_title', FALSE); ?></p>
      </div>
      <ul>
        <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/b1.jpg"></figure>
          <div class="detail">
          <p class="b-title"><?php echo $this->lang->line('page_sole_agent_section2_item1_title', FALSE); ?></p>
          <p><?php echo $this->lang->line('page_sole_agent_section2_item1_desc', FALSE); ?></p>
          </div>
        </li>
         <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/b2.jpg"></figure>
          <div class="detail">
          <p class="b-title"><?php echo $this->lang->line('page_sole_agent_section2_item2_title', FALSE); ?></p>
          <p><?php echo $this->lang->line('page_sole_agent_section2_item2_desc', FALSE); ?></p>
          </div>
        </li>
         <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/b3.jpg"></figure>
          <div class="detail">
          <p class="b-title"><?php echo $this->lang->line('page_sole_agent_section2_item3_title', FALSE); ?></p>
          <p><?php echo $this->lang->line('page_sole_agent_section2_item3_desc', FALSE); ?></p>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <div class="clr box-flow">
    <div class="wrap">
      <div class="topic2">
      <p class="title-page"><?php echo $this->lang->line('page_sole_agent_section3_title', FALSE); ?></p>
      </div>
      <ul>
        <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/s1.jpg"></figure>
          <div class="detail">
            <p class="step"><?php echo $this->lang->line('page_sole_agent_section3_step1_title', FALSE); ?></p>
            <h2><?php echo $this->lang->line('page_sole_agent_section3_step1_subtitle', FALSE); ?></h2>
            <p><?php echo $this->lang->line('page_sole_agent_section3_step1_desc', FALSE); ?></p>
          </div>
        </li>
        <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/s2.jpg"></figure>
          <div class="detail">
            <p class="step"><?php echo $this->lang->line('page_sole_agent_section3_step2_title', FALSE); ?></p>
            <h2><?php echo $this->lang->line('page_sole_agent_section3_step2_subtitle', FALSE); ?></h2>
            <p><?php echo $this->lang->line('page_sole_agent_section3_step2_desc', FALSE); ?></p>
          </div>
        </li>
         <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/s3.jpg"></figure>
          <div class="detail">
            <p class="step"><?php echo $this->lang->line('page_sole_agent_section3_step3_title', FALSE); ?></p>
            <h2><?php echo $this->lang->line('page_sole_agent_section3_step3_subtitle', FALSE); ?></h2>
            <p><?php echo $this->lang->line('page_sole_agent_section3_step3_desc', FALSE); ?></p>
          </div>
        </li>
         <li>
          <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/s4.jpg"></figure>
          <div class="detail">
            <p class="step"><?php echo $this->lang->line('page_sole_agent_section3_step4_title', FALSE); ?></p>
            <h2><?php echo $this->lang->line('page_sole_agent_section3_step4_subtitle', FALSE); ?></h2>
            <p><?php echo $this->lang->line('page_sole_agent_section3_step4_desc', FALSE); ?></p>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
  <div class="clr box_apply">
  <div class="wrap">
    <div class="topic2">
    <p class="title-page"><?php echo $this->lang->line('page_sole_agent_section4_title', FALSE); ?></p>
    </div>
    <p style="font-weight:600;text-align:center;"><?php echo $this->lang->line('page_sole_agent_section4_subtitle', FALSE); ?></p>
    <div class="inner-apply">
      <form method="post" action="<?php echo base_url("page/agent_confirm");?>">
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_contact_name', FALSE); ?></label>
	      <div class="r-input">
	        <input type="text" placeholder="Please input your Name" class="form-control" name="name">
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_contact_email', FALSE); ?></label>
	      <div class="r-input">
	        <input type="text" placeholder="Please input your E-mail" class="form-control" name="email">
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/07.png"><?php echo $this->lang->line('page_contact_tel', FALSE); ?></label>
	      <div class="r-input">
	        <input type="text" placeholder="Please input your Telephone" class="form-control" name="tel">
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/01.png"><?php echo $this->lang->line('page_contact_country', FALSE); ?></label>
	      <div class="r-input">
	        <select class="form-control" name="country">
				<?php foreach($countries as $item):?>
					<?php if($item->nicename!='JapanA'&&$item->nicename!='JapanB'){ ?>
					<option value="<?php echo $item->nicename;?>" <?php echo ($this->ion_auth->user()->row()->country == $item->nicename)?"selected":"";?>><?php echo $item->nicename;?></option>
					<?php } ?>
				<?php endforeach;?>
	        </select>
	      </div>
	      </div>
	      <div class="r-inline">
	      <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/11.png"><?php echo $this->lang->line('page_contact_message', FALSE); ?></label>
	      <div class="r-input">
	        <textarea class="form-control" rows="3" placeholder="Comment" name="msg"></textarea>
	      </div>
	      </div>
	      
	      <div class="row-btn"><button type="submit" class="b-blue"><?php echo $this->lang->line('page_contact_submit', FALSE); ?></button></div>
      </form>
  </div>
  </div>
</div>
