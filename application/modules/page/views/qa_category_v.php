<div class="container">
  <div class="clr inner">
   <div id="breadcrumbs" style="margin:15px 0;">
     <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_qa', FALSE); ?></span> / <span><?php echo $cat;?></span>
   </div>
  </div>
  <div class="clr page-qa">
 <div class="clr inner">
   <div class="topic2">
   <p class="title-page"><?php echo $this->lang->line('page_qa_cat_title1', FALSE); ?> <?php echo $cat;?> <?php echo $this->lang->line('page_qa_cat_title2', FALSE); ?></p>
   </div>
   <p class="title-detail"><?php echo $this->lang->line('page_qa_cat_desc1', FALSE); ?> <?php echo $cat;?> <?php echo $this->lang->line('page_qa_cat_desc2', FALSE); ?></p>
   <div class="clr inner-qa">
   <aside class="col-left">
     <div class="box_menu">
       <div class="a_menu">
         <div class="topic">
           <p><?php echo $this->lang->line('page_qa_cat_qa_cat', FALSE); ?></p>
             </div>
         <ul>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/SENSHA");?>">SENSHA</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Products");?>">Products</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Order Payment");?>">Order / Payment</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Delivery");?>">Delivery</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Return product");?>">Return product</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Discount");?>">Discount</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/Account");?>">Account</a></li>
           <li><a href="<?php echo base_url("$coutry_iso" . "page/qa_category/The others");?>">The others</a></li>
         </ul>
       </div>
     </div>
   </aside>
   <div class="content">
     <div class="question-list">
       <ul>
         <?php foreach($qa_post as $item):?>
           <li class="active">
             <a href="<?php echo base_url("$coutry_iso" . "page/qa_detail/$item->id");?>">
               <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/icon-qa.png"></figure>
               <div class="detail">
                 <h3><?php echo $item->title;?></h3>
                 <?php echo mb_substr(htmlspecialchars_decode($item->content), 0, 50);?>
                 <div class="see-more">
                   <?php echo $this->lang->line('page_qa_cat_see_more', FALSE); ?>
                 </div>
               </div>
             </a>
           </li>
         <?php endforeach;?>
       </ul>
     </div>
   </div>
   </div>
  </div>
</div>
</div>
