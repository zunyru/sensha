<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url("");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_news', FALSE); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
  <div class="clr page-news">
  <div class="layout-contain">
      <div class="news-content-in">
      <div class="headline">
      <p class="title"><?php echo $this->lang->line('page_news_title', FALSE); ?></p>
      <h2 align="center"><?php echo $this->lang->line('page_news_subtitle', FALSE); ?></h2>
      </div>
      <ul class="news-content-list">
        <?php foreach($news_post as $item):?>
          <li>
            <time datetime=""><?php echo date('d.m.Y', strtotime($item->create_date));?></time>
            <a href="<?php echo base_url("page/news_detail/$item->id");?>"><?php echo $item->title;?></a>
          </li>
        <?php endforeach;?>
      </ul>
      <section id="pagination" class="row">
        <?php echo $links; ?>
        <!-- <ul>
        <li class="previous"><a href=""><img src="<?php echo base_url("assets/sensha-theme/");?>images/arrow-p.png"></a></li>
        <li class="active"><span>1</span></li>
        <li class="number"><a href="">2</a></li>
        <li class="number"><a href="">3</a></li>
        <li class="number"><a href="">4</a></li>
        <li class="number"><a href="">5</a></li>
        <li class="next"><a href=""><img src="<?php echo base_url("assets/sensha-theme/");?>images/arrow-next.png"></a></li>
       </ul> -->
       </section>
        </div>
  </div><!--layout-contain-->
</div><!--inner-->
</div><!--container-->
