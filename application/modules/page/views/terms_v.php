<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_terms', FALSE); ?></span>
  </div>
</div>
<div class="inner terms">
      <div class="clr single-page">
  <div class="topic">
    <p class="title-page"><?php echo $this->lang->line('page_terms_title', FALSE); ?></p>
  </div>    
         <?php echo $this->lang->line('page_terms_subtitle', FALSE); ?>
         <?php echo $this->lang->line('page_terms_desc', FALSE); ?>
   </div>
</div>
<script src="js/main.js"></script>
<?php include('include/footer.php')?>
</div>
