<footer id="footer">
    <div class="footer-wrap">
        <div class="inner">
            <nav class="footer-nav">
                <?php
                $country = 'Global';
                if($this->ion_auth->logged_in()){
                    $country = $this->ion_auth->user()->row()->country;
                }
                $this->db->where('country', $country);
                $this->db->where('cluster_type', 'general');
                $query = $this->db->get('cluster1');
                $cluster1 = $query->result();
                ?>
                <section>
                    <h4>SENSHA Products</h4>
                    <ul>
                        <?php foreach($cluster1 as $item):?>
        		<?php if((!$this->ion_auth->logged_in()&&$item->cluster_url=='Sample') || ($this->ion_auth->in_group(array('GU')) && $item->cluster_url == 'Sample'))   :?>
        		<?php else: ?>
                            <li>
                                <a href="<?php echo base_url("$coutry_iso"."page/general/$item->cluster_url");?>"><?php echo $item->cluster_name;?></a>
                            </li>
          	<?php endif;?>
                        <?php endforeach;?>
                        <!-- <li><a href="">PPF</a></li>
                        <li><a href="">Body</a></li>
                        <li><a href="">Window</a></li>
                        <li><a href="">Wheel / Tire</a></li>
                        <li><a href="">Mall/Bumper</a></li>
                        <li><a href="">Enine room</a></li>
                        <li><a href="">Interior</a></li>
                        <li><a href="">The other</a></li> -->
                    </ul>
                </section>
                <?php
                $country = 'Global';
                if($this->ion_auth->logged_in()){
                    $country = $this->ion_auth->user()->row()->country;
                }
                $this->db->where('country', $country);
                $this->db->where('cluster_type', 'ppf');
                $query = $this->db->get('cluster1');
                $cluster1 = $query->result();
                ?>
                <section>
                    <h4>Syncshield Cut Films</h4>
                    <ul>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>Toyota">TOYOTA</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>HONDA">HONDA</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MAZDA">MAZDA</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MERCEDES%20BENZ">MERCEDES BENZ</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MITSUBISHI">MITSUBISHI</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>NISSAN">NISSAN</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>SUZUKI">SUZUKI</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>BMW">BMW</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>ISUZU">ISUZU</a>
                        </li>

                        <!-- <li><a href="">Content 01</a></li>
                        <li><a href="">Content 02</a></li>
                        <li><a href="">Content 03</a></li>
                        <li><a href="">Content 04</a></li>
                        <li><a href="">Content 05</a></li>
                        <li><a href="">Content 06</a></li> -->
                    </ul>
                    <!-- <ul> -->
                    <?php //foreach($cluster1 as $item):?>
                        <!-- <li>
                        <a href="<?php echo base_url("page/product_category/$item->cluster_url");?>"><?php echo $item->cluster_name;?></a>
                    </li> -->
                    <?php //endforeach;?>
                    <!-- <li><a href="">Content 01</a></li>
                    <li><a href="">Content 02</a></li>
                    <li><a href="">Content 03</a></li>
                    <li><a href="">Content 04</a></li>
                    <li><a href="">Content 05</a></li>
                    <li><a href="">Content 06</a></li> -->
                    <!-- </ul> -->
                </section>
                <section>
                    <h4>About SENSHA</h4>
                    <ul>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/about");?>">About Sensha</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/about_ppf");?>">About PPF</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/global_network");?>">About Network</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/agent");?>">About Sole Agent</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/terms");?>">Terms of use</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/privacy");?>">Privacy</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/contact");?>">Contact</a></li>
                    </ul>
                </section>
                <section>
                    <h4>Q&A</h4>
                    <ul>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/SENSHA");?>">SENSHA</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Products");?>">Products</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Order Payment");?>">Order / Payment</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Delivery");?>">Delivery</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Return product");?>">Return product</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Discount");?>">Discount</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Account");?>">Account</a></li>
                        <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/The others");?>">The others</a></li>
                    </ul>
                </section>
            </nav>
            <section class="footer-sns">
                <ul>
                    <li class="facebook">
                        <a href="https://www.facebook.com/SENSHAJAPAN" target="_blank">
                            <span class="icon-facebook"></span>
                            <span>Facebook</span>
                        </a>
                    </li>
                    <li class="youtube-sns">
                        <a href="https://www.youtube.com/channel/UCIyoGIHMmbgBRi5o8Hd8E2g" target="_blank">
                            <span><img src="<?php echo base_url("assets/sensha-theme/");?>images/youtube.svg" style="width: 25px;"></span>
                            <span>Youtube</span>
                        </a>
                    </li>
                    <li class="instagram">
                        <a href="https://www.instagram.com/sensha_world" target="_blank">
                            <span><img src="<?php echo base_url("assets/sensha-theme/");?>images/instagram.svg" style="width: 20px;"></span>
                            <span>Instagram</span>
                        </a>
                    </li>
                    <!--<li class="line">
                        <a href="">
                            <span class="icon-line"></span>
                            <span>Line id</span>
                        </a>
                    </li>
                    <li class="wechat">
                        <a href="">
                            <span class="icon-wechat"></span>
                            <span>WeChat</span>
                        </a>
                    </li>
                    <li class="whatsapp">
                        <a href="">
                            <span class="icon-whats-app"></span>
                            <span>What’s App</span>
                        </a>
                    </li>-->
                </ul>
            </section>
        </div>
    </div>
    <div class="copyright">
        <small>Copyright © 2018 Sensha. All Rights Reserved.</small>
    </div>
</footer>
<!--------------------------modal--------------------------------------->
<div class="header-form" style="display: none;"id="modal01">
    <ul class="cl-group">
        <li class="cl active"><span class="icon-lock"></span><span><?php echo $this->lang->line('login_title', FALSE); ?></span></li>
    </ul>
    <div class="form-content" >
        <div id="login" class="form-content-inner">
            <?php
                if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) {
                   echo "<p style='color: red'>Lock !!!</p>";
                } 
            ?>
            <p class="lead"><?php echo $this->lang->line('login_text1', FALSE); ?></p>
            <form id="formLogin" method="post" action="<?php echo base_url("page/user/doLogin");?>">
                <div class="form-block">
                    <label for="CustomerEmail" class="hidden-label"><?php echo $this->lang->line('login_text6', FALSE); ?></label>
                    <input type="text" id="CustomerEmail" class="user-input user-email" name="email" placeholder=""
                     <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) { echo 'style="background-color: #ccc"'; echo ' disabled'; }?>>
                    <label for="CustomerPassword" class="hidden-label"><?php echo $this->lang->line('login_text7', FALSE); ?></label>
                    <input type="password" id="CustomerPassword" class="user-input user-password" name="password" placeholder="" <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 3) { echo 'style="background-color: #ccc"'; echo ' disabled'; }?>>
                    <input type="submit" class="sign-in-btn btn" value="LOGIN" <?php if(isset($_COOKIE['login_false']) && $_COOKIE['login_false'] > 5) { echo 'style="background: #614804"'; echo ' disabled'; }?>/>
                </div>
            </form>
        </div>

    </div>
    <p class="note"><?php echo $this->lang->line('login_text2', FALSE); ?> <a href="<?php echo base_url("page/register");?>"><?php echo $this->lang->line('login_text3', FALSE); ?></a></p>
    <p class="note"><?php echo $this->lang->line('login_text4', FALSE); ?><a href="<?php echo base_url("page/forgot_password");?>"><?php echo $this->lang->line('login_text5', FALSE); ?></a></p>
</div>
<!----------------------------------------------------------------->
<!--<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>-->
<script src="<?php echo base_url("assets/sensha-theme/");?>js/slick.min.js"></script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/home.js"></script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/action.js"></script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.bxslider.js"></script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/owl.carousel.min.js"></script>

<!-- sweetalert2 -->
<script src="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.all.min.js")?>"></script>
<link rel="stylesheet" href="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.css")?>">
<script>
	;( function( $, window, document, undefined ){
    'use strict';
    var $list       = $( '.list' ),
        $items      = $list.find( '.list__item' ),
        setHeights  = function()
        {
            $items.css( 'height', 'auto' );

            var perRow = Math.floor( $list.width() / $items.width() );
            if( perRow == null || perRow < 1 ) return true;

            for( var i = 0, j = $items.length; i < j; i += perRow )
            {
                var maxHeight   = 0,
                    $row        = $items.slice( i, i + perRow );

                $row.each( function()
                {
                    var itemHeight = parseInt( $( this ).outerHeight() );
                    if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                });
                $row.css( 'height', maxHeight );
            }
        };
    setHeights();
    $( window ).on( 'resize', setHeights );

	})( jQuery, window, document );
</script>
<script>
	$( ".how-use ul" ).addClass( "list" );
</script>
<script>
	$( ".how-use ul li" ).addClass( "list__item" );
</script>
<script type="text/javascript">
$(document).ready(function() {
    $(".search-button-m").fancybox();
    $(".sign-up").fancybox();
    $(".login").fancybox();
    $(".language").fancybox();
    $(".sp-login-btn").fancybox();

    $('#formLogin').submit(function(){
        // Swal.fire({
        //     title: 'Processing',
        //     allowOutsideClick:false,
        //     onBeforeOpen: () => {
        //         swal.showLoading();
        //     }
        // });

        $.ajax({
            method: "POST",
            url: $(this).attr('action'),
            data: $( this ).serialize()
        })
        .done(function( msg ) {
            r = JSON.parse(msg);
            if(r.error == 0){
                // Swal.fire({
                //     type: 'success',
                //     title: 'Login Success',
                //     showConfirmButton: false,
                //     allowOutsideClick:false
                // });
                setTimeout(function(){ window.location = ''; }, 1500);
            }
            else if(r.error == 1){
                // Swal.fire({
                //     type: 'error',
                //     title: 'Please verify your email',
                // });
                alert('Please verify your email');
            }
            if(r.error == 2){
                // Swal.fire({
                //     type: 'error',
                //     title: r.msg,
                // });
                var lock = r.lock;
                if(lock > 5){
                    $('#CustomerEmail').prop("disabled", true);
                    $('#CustomerPassword').prop("disabled", true);
                    $('.sign-in-btn').prop("disabled", true);
                    $("#CustomerEmail").css("background-color","#ccc");
                    $("#CustomerPassword").css("background-color","#ccc");
                    $('.sign-in-btn').css("background","#614804");

                }
               
                

                alert(r.msg);

            }

            if(r.error == 3){
                var lock = r.lock;
                console.log(lock)
                if(lock > 5){
                   $('#CustomerEmail').prop("disabled", true);
                   $('#CustomerPassword').prop("disabled", true);
                   $('.sign-in-btn').prop("disabled", true);
                   $("#CustomerEmail").css("background-color","#ccc");
                   $("#CustomerPassword").css("background-color","#ccc");
                   $('.sign-in-btn').css("background","#614804");
                }

                alert('Not found you account.');
            }
        })
        .fail(function( msg ) {
            Swal.fire(
                'Oops...',
                msg,
                'error'
            );
        });

        return false;
    });
});
</script>

<script>

if ($('#gallery-thumbs').length > 0) {

    // Cache the thumb selector for speed
    var thumb = $('#gallery-thumbs').find('.thumb');

    // How many thumbs do you want to show & scroll by
    var visibleThumbs = 5;

    // Put slider into variable to use public functions
    var gallerySlider = $('#gallery').bxSlider({
        controls: false,
        pager: false,
        easing: 'easeInOutQuint',
        infiniteLoop: true,
        speed: 500,
        touchEnabled: true,
        adaptiveHeight:true,
		auto: true,
        onSlideAfter: function (currentSlideNumber) {
            var currentSlideNumber = gallerySlider.getCurrentSlide();
            thumb.removeClass('pager-active');
            thumb.eq(currentSlideNumber).addClass('pager-active');
        },

        onSlideNext: function () {
            var currentSlideNumber = gallerySlider.getCurrentSlide();
            slideThumbs(currentSlideNumber, visibleThumbs);
        },

        onSlidePrev: function () {
            var currentSlideNumber = gallerySlider.getCurrentSlide();
            slideThumbs(currentSlideNumber, visibleThumbs);
        }
    });

    // When clicking a thumb
    thumb.click(function (e) {

        // -6 as BX slider clones a bunch of elements
        gallerySlider.goToSlide($(this).closest('.thumb-item').index());

        // Prevent default click behaviour
        e.preventDefault();
    });

    // Thumbnail slider
    var thumbsSlider = $('#gallery-thumbs').bxSlider({
        controls:true,
        pager:false,
        touchEnabled: false,
        adaptiveHeight:true,
        easing: 'easeInOutQuint',
        infiniteLoop: false,
        minSlides: 5,
        maxSlides: 2,
        slideWidth: 360,
        slideMargin: 10
    });

    // Function to calculate which slide to move the thumbs to
    function slideThumbs(currentSlideNumber, visibleThumbs) {

        // Calculate the first number and ignore the remainder
        var m = Math.floor(currentSlideNumber / visibleThumbs);

        // Multiply by the number of visible slides to calculate the exact slide we need to move to
        var slideTo = m * visibleThumbs;

        // Tell the slider to move
        thumbsSlider.goToSlide(m);
    }

    // When you click on a thumb
    $('#gallery-thumbs').find('.thumb').click(function () {

        // Remove the active class from all thumbs
        $('#gallery-thumbs').find('.thumb').removeClass('pager-active');

        // Add the active class to the clicked thumb
        $(this).addClass('pager-active');

    });
}
$(function(){
    var lang = '<?php echo (get_cookie('user_language') == '')?"Global":get_cookie('user_language');?>'

    $('input[value="'+lang+'"]').prop('checked', true);
    $('input[name="language"]').change(function(){
        var iso = $(this).data('iso');
        var old_iso = '<?php echo $this->coutry_iso;?>';
        var my_url = "<?php echo $this->uri->uri_string();?>";
        $.ajax({
            method: "POST",
            url: '<?php echo base_url("page/change_lang");?>',
            data: {'language': $(this).val()}
        })
        .done(function( msg ) {
            if(msg == ''){
                 console.log(iso);
                 console.log(old_iso);
                 console.log(my_url);
                 console.log(msg);
                var goto = my_url.replace(old_iso+'/', '');
                if(iso == msg){
                    var goto = '';
                }
                if(iso == ""){
                    var goto = my_url.replace(old_iso+'/', '');
                }
                 console.log(goto);
                 if(lang != 'Global'){
                    window.location = '<?php echo base_url();?>'+goto;
                 }else{
                    if(iso == ''){
                        goto = my_url.replace(old_iso+"/","")
                        if(goto == ''){
                           window.location = '<?php echo base_url();?>';
                        }else{
                            window.location = '<?php echo base_url();?>'+goto;
                        }
                    }else{
                        window.location = '<?php echo base_url();?>'+goto;
                    }
                 }
            }
            else{

                var goto = my_url.replace(old_iso+'/', msg+'/');
                if(old_iso == ''){
                    var goto = msg+'/'+my_url;
                }
                if(my_url == old_iso){
                    var goto = msg;
                }
                 //console.log(goto);
                window.location = '<?php echo base_url();?>'+goto;
            }
            // window.location = '<?php base_url();?>'+"/"+iso+"/page";
            // window.location = '';
            // $('product').html(msg);
            // r = JSON.parse(msg);
            // console.log(r);

        })
        .fail(function( msg ) {
            console.log(msg);
        });
    });
});

$(function(){
	$('.lang.mobile .current_lang').click(function(){
		$('.lang.mobile .language-select ul').slideToggle();
	});
});

</script>
<script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.showmore.js"></script>
</body>
</html>
