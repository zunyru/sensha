<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="<?php echo base_url("$coutry_iso");?>">Home</a></span><span><?php echo $this->lang->line('breadcrumb_contact', FALSE); ?></span>
  </div>
</div>
<div class="inner">
<div class="clr page-contact">
  <div class="topic">
    <p class="title-page"><?php echo $this->lang->line('page_contact_title', FALSE); ?></p>
  </div>
  <div class="clr box-contact">
         <div class="right">
        <ul>
          <li>
            <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i1.png"></figure>
            <div class="detail">
              <p class="title"><?php echo $this->lang->line('page_about_section4_head_office', FALSE); ?></p>
              <p>SENSHA CO., LTD</p>
            </div>
          </li>
          <li>
            <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i2.png"></figure>
            <div class="detail">
              <p class="title"><?php echo $this->lang->line('page_about_section4_office_address', FALSE); ?></p>
              <p><?php echo $this->lang->line('page_about_section4_office_address_content', FALSE); ?></p>
            </div>
          </li>
          <li>
            <figure><img src="<?php echo base_url("assets/sensha-theme/");?>images/i3.png"></figure>
            <div class="detail">
              <p class="title"><?php echo $this->lang->line('page_about_section4_phone', FALSE); ?></p>
              <p><?php echo $this->lang->line('page_about_section4_phone_content', FALSE); ?></p>
            </div>
          </li>
        </ul>
        </div>
        <div class="left">
          <form method="post" action="<?php echo base_url("page/contact_confirm");?>">
            <div class="r-inline">
              <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/02.png"><?php echo $this->lang->line('page_contact_name', FALSE); ?></label>
              <div class="r-input">
              <input type="text" placeholder="Please input your Name" class="form-control" name="name">
              </div>
            </div>
            <div class="r-inline">
              <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/04.png"><?php echo $this->lang->line('page_contact_email', FALSE); ?></label>
              <div class="r-input">
              <input type="email" placeholder="Please input your Email" class="form-control" name="email">
              </div>
            </div>
            <div class="r-inline">
              <label class="label"><img src="<?php echo base_url("assets/sensha-theme/");?>images/11.png"><?php echo $this->lang->line('page_contact_message', FALSE); ?></label>
              <div class="r-input">
              <textarea rows="5" placeholder="Please input your Message" class="form-control" name="msg"></textarea>

              </div>
            </div>
               <div class="row-btn">
              <button type="submit" class="b-blue"><?php echo $this->lang->line('page_contact_submit', FALSE); ?></button>
            </div>
          </form>
        </div>
  </div>
</div>
</div>
