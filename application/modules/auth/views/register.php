<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css")?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css") ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/square/blue.css") ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- jQuery -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js") ?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/icheck.min.js") ?>"></script>

  <!-- sweetalert2 -->
  <script src="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.js")?>"></script>
  <link rel="stylesheet" href="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.css")?>">
</head>
<body class="hold-transition login-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="../../index2.html"><b>Admin</b>LTE</a>
    </div>

    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Register a new membership</p>

        <form id="formUserRegister" action="<?=base_url("auth/doRegister")?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="First Name" name="first_name" required="yes">
            <!-- <span class="fa fa-user form-control-feedback"></span> -->
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Last Name" name="last_name" required="yes">
            <!-- <span class="fa fa-user form-control-feedback"></span> -->
          </div>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" required="yes">
            <!-- <span class="fa fa-envelope form-control-feedback"></span> -->
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password" required="yes">
            <!-- <span class="fa fa-lock form-control-feedback"></span> -->
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password" name="conf_pass" required="yes">
            <!-- <span class="fa fa-lock form-control-feedback"></span> -->
          </div>
          <div class="row">
            <div class="col-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> I agree to the <a href="#">terms</a>
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <a href="<?php echo base_url("auth/login")?>" class="text-center">I already have a membership</a>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>


  <script>
  $(function(){
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    });

    $("#formUserRegister").submit(function(){
      Swal.fire({
        title: 'Processing',
        allowOutsideClick:false,
        onBeforeOpen: () => {
          swal.showLoading();
        }
      });

      $.ajax({
        method: "POST",
        url: $(this).attr('action'),
        data: $( this ).serialize()
      })
      .done(function( msg ) {
        r = JSON.parse(msg);
        if(r.error == 0){
          Swal.fire({
            type: 'success',
            title: 'Signup success.<br /> Please confirm your email.',
            allowOutsideClick:false
          }).then((result) => {
            if (result.value) {
              window.location = '<?=base_url("auth/login")?>';
            }
          });

        }
        else if(r.error == 1){
          Swal.fire({
            type: 'error',
            title: r.msg,
          });
        }
      })
      .fail(function( msg ) {
        Swal.fire(
          'Oops...',
          msg,
          'error'
        );
      });

      return false;
    });

  });

</script>
</body>
</html>
