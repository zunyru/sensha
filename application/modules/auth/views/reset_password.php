<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/font-awesome/css/font-awesome.min.css")?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/dist/css/adminlte.min.css") ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/square/blue.css") ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- jQuery -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/jquery/jquery.min.js") ?>"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/iCheck/icheck.min.js") ?>"></script>

  <!-- sweetalert2 -->
  <script src="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.js")?>"></script>
  <link rel="stylesheet" href="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.css")?>">
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Admin</b>LTE</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Reset Password</p>

        <form id="reset_pass" action="<?=base_url("auth/doResetUserPassword")?>" method="post">
          <input type="hidden" name="code" value="<?=$code?>">
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="New password" id="new_password" name="new_password" required="yes">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Confirm Password" id="confpass" name="confpass" required="yes">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-8">
              <a href="<?=base_url("auth/login")?>" class="text-center">Go to login</a>
            </div>
            <div class="col-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>


  <script>
  $(function(){
    $('#reset_pass').submit(function(){
      if($('#new_password').val() != $('#confpass').val()){
        Swal.fire({
          title: 'Password not match!',
          type: 'error',
        });
      }
      else{
        $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $( this ).serialize()
        })
        .done(function( msg ) {
          r = JSON.parse(msg);
          if(r.error == 0){
            Swal.fire({
              type: 'success',
              title: 'Reset password success',
              showConfirmButton: false,
              allowOutsideClick:false
            });
            setTimeout(function(){ window.location = '<?=base_url("admin")?>'; }, 1500);
          }
          else if(r.error == 1){
            Swal.fire(
              'Oops...',
              r.msg,
              'error'
            );
          }
        })
        .fail(function( msg ) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
      }
      return false;
    });
  });

  <?php if(!$is_have_user):?>
  $('.login-box').remove();
  Swal.fire({
    title: 'Oops...',
    html: "Sory something went wrong. please contact to <br /> webmaster",
    type: 'error',
    showCancelButton: false,
    allowOutsideClick: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ok',
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger',
    buttonsStyling: false,
    reverseButtons: true
  }).then((result) => {
    if (result.value) {
      window.location = '<?=base_url("auth/login");?>';
    }
  });

  <?php endif;?>
  </script>
</body>
</html>
