<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function login(){
		$this->load->view('auth/login');
	}

	public function doLogin(){
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');
		$remember = (bool)$this->input->post('remember');

		$identity_column = $this->config->item('identity', 'ion_auth');
		$getUser = $this->ion_auth->where($identity_column, $email)->users()->row();

		if($getUser){
			if(!$getUser->active){
				echo json_encode(array('error' => 1));
			}else{
				if($this->ion_auth->login($email, $pass, $remember)){
					if(!$this->ion_auth->in_group(array('admin', 'SA', 'AA'))){
						$this->ion_auth->logout();
						echo json_encode(array('error' => 2, 'msg' => 'You not have authorization.'));
						exit();
					}
					unset($getUser->password);
					$this->session->set_userdata('user_data', $getUser);
					@keep_log("login", $getUser->id);
					echo json_encode(array('error' => 0));
				}else{
                    $cookie_name = "login_false";
                    $cookie_value =  $_COOKIE['login_false'] + 1;
                    setcookie($cookie_name, $cookie_value, time() + (60 * 60), "/");
					echo json_encode(array('error' => 2, 'msg' => $this->ion_auth->errors(),'lock' => $cookie_value));
				}
			}
		}
		else{
            $cookie_name = "login_false";
            $cookie_value =  $_COOKIE['login_false'] + 1;
            setcookie($cookie_name, $cookie_value, time() + (60 * 60), "/");
			echo json_encode(array('error' => 3,'lock' => $cookie_value));
		}
	}

	public function doLoginBypass(){
		$email = $this->input->post('email');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$identity = $this->ion_auth->where($identity_column, $email)->users()->row();

		$new_data = array();

		if($identity){
			$user = $identity;
			$new_data['active'] = 1;
			$new_data['activation_code'] = "";

			$this->db->where('id', $user->id);
			$this->db->update('users', $new_data);
			$this->ion_auth->set_session($user);
			$this->ion_auth->update_last_login($user->id);
			$this->ion_auth->clear_login_attempts($email);
			$this->ion_auth->trigger_events(array('post_login', 'post_login_successful'));
			$this->ion_auth->set_message('login_successful');
			echo json_encode(array('error' => 0));
		}
		else{
			if($this->ion_auth->register($email, $this->generateRandomString(15), $email, $new_data)){
				$getUser = $this->ion_auth->where($identity_column, $email)->users()->row();
				$this->ion_auth->set_session($getUser);
				$this->ion_auth->update($getUser->id, array('active' => 1, 'activation_code' => ''));
				$this->ion_auth->update_last_login($getUser->id);
				$this->ion_auth->clear_login_attempts($email);
				$this->ion_auth->trigger_events(array('post_login', 'post_login_successful'));
				$this->ion_auth->set_message('login_successful');
				echo json_encode(array('error' => 0));
			}
			else{
				echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->error()));
			}
		}

	}

	public function register(){
		$this->load->view('auth/register');
	}

	public function doRegister(){
		$email = strtolower($this->input->post('email'));
		$password = $this->input->post('password');

		$additional_data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
		);

		$new_account = $this->ion_auth->register($email, $password, $email, $additional_data);

		$id = $new_account['id'];

		if($new_account){
			// @keep_log('signup', '', $email);
			$activate_code = $this->ion_auth->user($id)->row()->activation_code;
			$msg = 'Please activate your account <a href="'.base_url("auth/activateAccount/$id/$activate_code").'">Click here</a>';
			mail_to(array("$email" =>  $this->input->post('name')), "Activate to your account.", $msg);
			echo json_encode(array('error' => 0, 'msg' => 'Signup success. Please confirm your email'));
		}
		else{
			echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
		}
	}

	public function activateAccount($id, $code = FALSE, $send_back = true){
		if ($code !== FALSE){
			$activation = $this->ion_auth->activate($id, $code);
		}

		$data['activation'] = $activation;
		$this->load->view('auth/activate_v', $data);
	}

	public function createUser(){
		$email = strtolower($this->input->post('email'));
		$identity_column = $this->config->item('identity', 'ion_auth');
		$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
		$password = $this->input->post('password');

		$additional_data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
		);

		$is_create = $this->ion_auth->register($identity, $password, $email, $additional_data);

		if($is_create){
			$uid = $is_create['id'];
			$code = $is_create['activation'];

			$this->activate($uid, $code, false);
			// $this->ion_auth->update($uid, array('uid' => $uid, 'active' => 1, 'activation_code' => ''));

			// Only allow updating groups if user is admin
			if ($this->ion_auth->is_admin()){
				// Update the groups user belongs to
				$groupData = $this->input->post('groups');

				if (isset($groupData) && !empty($groupData)){
					$this->ion_auth->remove_from_group('', $uid);

					foreach ($groupData as $grp){
						$this->ion_auth->add_to_group($grp, $uid);
					}

				}
			}

			// $subject = "Registration Confirmation";
			// $mes = '<a href="'.base_url("auth/activate/$uid/$code").'" target="_blank">Activate Your account</a>';
			// mail_to(array($email => ''), $subject, $mes);
			echo json_encode(array('error' => 0));
		}
		else{
			echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
		}
		// if($this->ion_auth->register($identity, $password, $email, $additional_data)){
		// 	echo json_encode(array('error' => 0));
		// }
		// else{
		// 	echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
		// }
	}

	public function edit_user(){
		$id = $this->input->post('edit_id');
		$user = $this->ion_auth->user($id)->row();
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
		);

		// update the password if it was posted
		if ($this->input->post('password'))
		{
			$data['password'] = $this->input->post('password');
		}

		if ($this->ion_auth->is_admin()){
			// Update the groups user belongs to
			$groupData = $this->input->post('groups');

			if (isset($groupData) && !empty($groupData))
			{
				$this->ion_auth->remove_from_group('', $id);
				foreach ($groupData as $grp)
				{
					$this->ion_auth->add_to_group($grp, $id);
				}
			}
		}

		if ($this->ion_auth->update($user->id, $data)){
			// Only allow updating groups if user is admin

			echo json_encode(array('error' => 0));
		}
		else{
			echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
		}
	}

	public function activate($id, $code = FALSE, $send_back = true){
		if ($code !== FALSE){
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin()){
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation){
			// redirect them to the auth page
			// $this->session->set_flashdata('message', $this->ion_auth->messages());
			// redirect("auth", 'refresh');

			if($send_back){
				echo json_encode(array('error' => 0));
			}
		}
		else{
			// redirect them to the forgot password page
			// $this->session->set_flashdata('message', $this->ion_auth->errors());
			// redirect("auth/forgot_password", 'refresh');

			if($send_back){
				echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
			}
		}
	}

	public function create_group(){
		if($this->ion_auth->is_admin()){
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if ($new_group_id){
				echo json_encode(array('error' => 0));
			}
			else{
				echo json_encode(array('error' => 1));
			}
		}
		else{
			echo json_encode(array('error' => 1));
		}
	}

	public function edit_group(){
		$id = $this->input->post('edit_id');
		$group_update = $this->ion_auth->update_group($id, $this->input->post('group_name'), $this->input->post('description'));
		if ($group_update){
			echo json_encode(array('error' => 0));
		}else{
			echo json_encode(array('error' => 1));

		}
	}

	public function forgot_password(){
		$identity_column = $this->config->item('identity', 'ion_auth');
		$identity = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();

		if (!empty($identity)){
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				// $this->session->set_flashdata('message', $this->ion_auth->messages());
				// redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
				$user = $this->ion_auth->where($identity_column, $this->input->post('email'))->users()->row();

				$name = $user->first_name.' '.$user->last_name;

				$mes = '
				Dear '.$name.':<br/>
				Please click the link below and follow the instruction to set your new password:<br><br>

				<a href="'.base_url("auth/reset_password/$user->forgotten_password_code").'" target="_blank"> Reset Password </a><br><br>

				Then you can log in using your email and new password.<br><br>

				';

				mail_to(array("$user->email"=>"$name"), 'Forgot Password TrueInnovationCenter.Com', $mes);
				echo json_encode(array('error' => 0));
			}
			else
			{
				// $this->session->set_flashdata('message', $this->ion_auth->errors());
				// redirect("auth/forgot_password", 'refresh');
				echo json_encode(array('error' => 1));
			}
		}
		else{
			echo json_encode(array('error' => 1, 'msg' => "Forgot password email not found."));
		}
	}

	public function reset_password($code = NULL){
		$user = $this->ion_auth->forgotten_password_check($code);

		if($user){
			$data['is_have_user'] = 1;
		}
		else{
			$data['is_have_user'] = 0;
		}

		if($code == NULL){
			$data['is_have_user'] = 0;
		}

		$data['uid'] = @$user->id;
		$data['code'] = $code;

		$this->load->view('auth/reset_password', $data);
	}

	public function doResetUserPassword(){
		$user = $this->ion_auth->forgotten_password_check($this->input->post('code'));
		if ($user){
			// finally change the password
			$identity = $user->{$this->config->item('identity', 'ion_auth')};

			$change = $this->ion_auth->reset_password($identity, $this->input->post('new_password'));

			if ($change)
			{
				// if the password was successfully changed
				echo json_encode(array('error' => 0));
			}
			else
			{
				echo json_encode(array('error' => 1));
			}
		}
		else{
			echo json_encode(array('error' => 1));
		}


		// $this->users_model->setUserPassword($this->input->post('uid'), $this->input->post('new_password'));
		// echo json_encode(array('error' => 0, 'redirect' => base_url("")));
	}

	public function logout(){
		@keep_log("logout", $this->ion_auth->user()->row()->id);
		$this->ion_auth->logout();
		redirect("auth/login", 'refresh');
	}


	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}


}
