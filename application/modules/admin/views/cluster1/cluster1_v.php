<style>
/* .modal-lg{
  width: 80%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_product_cat_1st_cluster', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_product_cat_1st_cluster', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <?php if($this->ion_auth->in_group(array('admin'))):?>
      <div class="row">
        <div class="col-md-4">
<!--          <form method="post" action="--><?php //echo base_url("sensha-admin/cluster1");?><!--">-->
            <div class="form-group">
              <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
              <select name="country" id="filter_country">
                <option value="">--- Choose Country ---</option>
                <?php foreach($nation_lang as $item):?>
                  <option value="<?php echo $item->country;?>" <?php echo ($this->input->post('country') == $item->country)?'selected':'';?>><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                <?php endforeach;?>
              </select>
              <button type="submit" class="btn btn-primary btn-sm" id="filter"><?php echo $this->lang->line('global_search', FALSE); ?></button>
            </div>
<!--          </form>-->
        </div>
      </div>
    <?php endif;?>


    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <a href="<?php echo base_url("sensha-admin/cluster1");?>" class="btn btn-outline-secondary btn-flat disabled" ><?php echo $this->lang->line('admin_product_cat_1st_cluster', FALSE); ?></a>
          <a href="<?php echo base_url("sensha-admin/cluster2");?>" class="btn btn-outline-info btn-flat" ><?php echo $this->lang->line('admin_product_cat_2nd_cluster', FALSE); ?></a>
          <!-- <a href="<?php //echo base_url("sensha-admin/cluster3");?>" class="btn btn-outline-info btn-flat" ><?php //echo $this->lang->line('admin_product_cat_3rd_cluster', FALSE); ?></a> -->
        </div>
      </div>
    </div>
    <?php if($this->ion_auth->in_group(array('admin'))):?>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
        <br><br>
      </div>
    </div>
    <?php endif;?>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $this->lang->line('admin_product_cat_1st_cluster', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">ID</th>
                  <th><?php echo $this->lang->line('admin_product_cat_name', FALSE); ?> (EN)</th>
                  <th><?php echo $this->lang->line('admin_product_cat_name', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_product_cat_type', FALSE); ?></th>
                  <th width="10%"></th>
                </tr>
              </thead>

              </table>
            </div>
          </div>
        </div>
      </div>

      <div id="modal-add" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_product_cat_create_1st_cluster', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/cluster1/add");?>">
                <div class="card-body">
                  <!-- <div class="form-group">
                    <label>Country</label>
                    <select class="form-control " name="country" style="width: 100%;" required >
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo $item->country;?></option>
                      <?php endforeach;?>
                    </select>
                  </div> -->
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_type', FALSE); ?></label>
                    <select class="form-control " name="cluster_type" style="width: 100%;" required >
                      <option selected="selected" value="">-- Choose --</option>
                      <option value="general"><?php echo $this->lang->line('admin_product_cat_general', FALSE); ?></option>
                      <option value="ppf"><?php echo $this->lang->line('admin_product_cat_ppf', FALSE); ?></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_cluster_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="cluster_name" required />
                    <input type="hidden" name="cluster_level" value="1" />
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>



      <div id="modal-edit" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_product_cat_edit_1st_cluster', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/cluster1/edit");?>">
                <input type="hidden" id="edit_id" name="edit_id" />
                <div class="card-body">
                  <!-- <div class="form-group">
                    <label>Country</label>
                    <select class="form-control select2" name="country" style="width: 100%;" required>
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo $item->country;?></option>
                      <?php endforeach;?>
                    </select>
                  </div> -->
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_type', FALSE); ?></label>
                    <select class="form-control " name="cluster_type" style="width: 100%;" <?php echo ($this->ion_auth->in_group(array('SA')))?'disabled':'';?> required disabled>
                      <option selected="selected" value="">-- Choose --</option>
                      <option value="general"><?php echo $this->lang->line('admin_product_cat_general', FALSE); ?></option>
                      <option value="ppf"><?php echo $this->lang->line('admin_product_cat_ppf', FALSE); ?></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_cluster_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="cluster_name" <?php echo ($this->ion_auth->in_group(array('SA')))?'disabled':'';?> required />
                    <input type="hidden" name="cluster_level" value="1" />
                  </div>
                  <?php if($this->ion_auth->in_group(array('SA'))):?>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_new_cluster_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="cluster_name" required />
                    <input type="hidden" name="cluster_level" value="1" />
                  </div>
                  <?php endif;?>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>



    </section>


    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>

    <!-- Select2 -->

    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js")?>"></script>

    <script>
    function edit_item(t){
      // $('#modal-edit select[name="country"]').select2("val", $(t).data('country'));
      $('#modal-edit select[name="country"]').val($(t).data('country'));
      $('#modal-edit select[name="cluster_type"]').val($(t).data('cluster_type'));
      $('#modal-edit input[name="cluster_name"]').val($(t).data('cluster_name'));

      $('#edit_id').val($(t).data('id'));
      $('#modal-edit').modal();
    }

    function delete_item(t){
      Swal.fire({
        title: 'Are you sure to delete <br /> "'+$(t).data('cluster_name')+'"?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: "POST",
            url: '<?=base_url("sensha-admin/cluster1/delete/")?>'+$(t).data('id'),
          })
          .done(function( msg ) {
             r = JSON.parse(msg);
            if(r.error == 0){
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
           }else{
            Swal.fire({
              type: 'error',
              title: 'fail',
            });

           }
            setTimeout(function(){ window.location = ''; }, 1400);
          })
          .fail(function( msg ) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
        }
      });
    }

    $(function(){
      /*$('#list_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength": 30
      });*/
        fill_datatable();

        function fill_datatable(filter_country = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/cluster1/data_index" ?>`;
            var dataList = $('#list_table');
            dataList.DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data :{
                        filter_country: filter_country
                    }
                },
                order: [[0, "asc"]],
                pageLength: 100,
                lengthMenu: [100, 150, 200,300],
                columns: [
                    {data: "id",width: "5%", orderable: false},
                    {data: "cluster_url",width: "5%", orderable: false},
                    {data: "cluster_name", orderable: false},
                    {data: "country", orderable: false},
                    {data: "cluster_type", orderable: false},
                    {data: "action", orderable: false},
                ]
            });
        }

        $(document).ready(function () {

            $('#filter').click(function () {
                var filter_country = $('#filter_country').val();
                if (filter_country != '' ) {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country);
                } else {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country);
                }
            });
        });

      // $('.select2').select2();

      $('#form_add, #form_edit').submit(function(){
        $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $( this ).serialize()
        })
        .done(function( msg ) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if(r.error == 0){
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick:false
            });
            setTimeout(function(){ location.reload(); }, 1400);
          }
          else{
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function( msg ) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
        return false;
      });
    });
  </script>
