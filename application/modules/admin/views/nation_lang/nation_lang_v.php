<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_nation_lang_title', FALSE); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_nation_lang_title', FALSE); ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i
                            class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"><?php echo $this->lang->line('admin_nation_lang_title', FALSE); ?></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover no-margin" id="list_table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_nation_lang_language', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_nation_lang_currency_name', FALSE); ?></th>
                                <th>VAT</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                        </table>

                    </div>

                </div>
            </div>
        </div>

        <div id="modal-add" class="modal" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Nation & Lang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/nation_lang/add"); ?>">
                            <div class="card-body">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></label>
                                    <select class="form-control select2" name="country" style="width: 100%;" required>
                                        <option selected="selected" value="">-- Choose --</option>
                                        <?php foreach ($countries_cut as $item) : ?>
                                            <option value="<?php echo $item->nicename; ?>"><?php echo ucwords(str_replace("_", " ", $item->nicename)) ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_language', FALSE); ?></label>
                                    <input type="text" class="form-control" name="language" required>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_reading', FALSE); ?></label>
                                    <select class="form-control select2" name="reading" style="width: 100%;">
                                        <!-- <option value="">-- Choose --</option> -->
                                        <option selected="selected" value="ltr">LTR</option>
                                        <option value="rtl">RTL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_currency_name', FALSE); ?></label>
                                    <input type="text" class="form-control" name="currency_name" required>
                                </div>

                                <div class="form-group">
                                    <label>VAT</label>
                                    <input type="number" class="form-control" name="vat" required>
                                </div>
                                <div class="form-group">
                                    <label>Show</label>
                                    <div class="checkbox">
                                        <label style="margin-right: 15px;"><input type="checkbox" name="is_active"
                                                                                  value="1"> Active</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div id="modal-edit" class="modal" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Nation & Lang</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_edit" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/nation_lang/edit"); ?>">
                            <input type="hidden" id="edit_id" name="edit_id"/>
                            <input type="hidden" id="old_country" name="old_country"/>
                            <div class="card-body">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_country', FALSE); ?></label>
                                    <select class="form-control select2" name="country" style="width: 100%;" required
                                            disabled>
                                        <option selected="selected" value="">-- Choose --</option>
                                        <option value="Global">Global</option>
                                        <?php foreach ($countries as $item) : ?>
                                            <option value="<?php echo $item->nicename; ?>"><?php echo ucwords(str_replace("_", " ", $item->nicename)) ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_language', FALSE); ?></label>
                                    <input type="text" class="form-control" name="language" required>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_country_flag', FALSE); ?></label>
                                    <input type="file" class="form-control" name="country_flag"
                                           placeholder="Enter email">
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_reading', FALSE); ?></label>
                                    <select class="form-control select2" name="reading" style="width: 100%;">
                                        <!--<option value="">-- Choose --</option> -->
                                        <option selected="selected" value="ltr">LTR</option>
                                        <option value="rtl">RTL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_nation_lang_currency_name', FALSE); ?></label>
                                    <input type="text" class="form-control" name="currency_name" required>
                                </div>
                                <div class="form-group">
                                    <label>VAT</label>
                                    <input type="number" class="form-control" name="vat" required>
                                </div>
                                <div class="form-group">
                                    <label>Show</label>
                                    <div class="checkbox">
                                        <label style="margin-right: 15px;"><input type="checkbox" name="is_active"
                                                                                  value="1"> Active</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


</section>


<!-- DataTables -->
<link rel="stylesheet"
      href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<!--<script src="--><?php //echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?><!--"></script>-->

<script>
    function edit_item(t) {
        console.log($(t).data('country'))
        $('#modal-edit input[name="old_country"]').val($(t).data('country'));
        $('#modal-edit select[name="country"]').val($(t).data('country')).trigger("change");
        $('#modal-edit select[name="reading"]').val($(t).data('reading')).trigger("change");
        //$('#modal-edit select[name="reading"]').select2("val", $(t).data('reading'));
        $('#modal-edit input[name="language"]').val($(t).data('language'));
        $('#modal-edit input[name="font"]').val($(t).data('font'));
        $('#modal-edit input[name="currency_name"]').val($(t).data('currency_name'));
        $('#modal-edit input[name="currency_rate_exchange"]').val($(t).data('currency_rate_exchange'));
        $('#modal-edit input[name="vat"]').val($(t).data('vat'));
        $("#modal-edit input[name='is_active']").prop('checked', false);
        if ($(t).data('is_active') == 1) {
            $("#modal-edit input[name='is_active']").prop('checked', true);
        }
        // $('#modal-edit #description').val($(t).data('des'));

        $('#edit_id').val($(t).data('id'));
        $('#modal-edit').modal();
    }

    function delete_item(t) {
        Swal.fire({
            title: 'Are you sure to delete <br /> "' + $(t).data('country') + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?= base_url("sensha-admin/nation_lang/delete/") ?>' + $(t).data('id'),
                })
                    .done(function (msg) {
                        r = JSON.parse(msg);
                        if (r.error == 3) {
                            Swal.fire({
                                title: 'Fail!',
                                text: "Can't to delete.",
                                type: 'warning',
                                showConfirmButton: false,
                            })
                        } else {
                            Swal.fire({
                                title: 'Deleted!',
                                text: 'Your data has been deleted.',
                                type: 'success',
                                showConfirmButton: false,
                            })
                            setTimeout(function () {
                                window.location = '';
                            }, 1400);
                        }
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    $(function () {
        /*$('#list_table').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "pageLength": 30
        });*/
        var base_url = `<?php echo base_url()."sensha-admin/nation_lang/data_index" ?>`;
        var dataList = $('#list_table');
        dataList.DataTable({
            serverSide: true,
            processing:true,
            ajax: {
                url:  base_url,
                type: 'POST',
            },
            order: [[0, "asc"]],
            pageLength: 10,
            columns: [
                {data: "id", orderable: false},
                {data: "country", orderable: false},
                {data: "language", orderable: false},
                {data: "currency_name", orderable: false},
                {data: "vat",  orderable: false},
                {data: "action",  orderable: false},
            ]
        });


        $('.select2').select2();

        $('#form_add, #form_edit').submit(function () {
            Swal.fire({
                title: 'Processing',
                onBeforeOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getContent()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                onClose: () => {
                    clearInterval(timerInterval)
                }
            });
            var formData = new FormData($(this)[0]);
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        Swal.fire({
                            type: 'success',
                            title: 'success',
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1400);
                    } else if (r.error == 3) {
                        Swal.fire(
                            'Oops...',
                            'Country is duplicate',
                            'warning'
                        );
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });
    });
</script>