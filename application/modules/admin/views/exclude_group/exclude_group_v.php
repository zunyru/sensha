<style>
/* .modal-lg{
  width: 80%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_exclude_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_exclude_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <!-- <div class="row">
      <div class="col-md-4">
        <form method="post" action="<?php echo base_url("sensha-admin/exclude_group");?>">
          <div class="form-group">
            <label>Filter</label>
            <select name="country">
              <option value="">--- Choose Country ---</option>
              <?php foreach($nation_lang as $item):?>
                <option value="<?php echo $item->country;?>" <?php echo ($this->input->post('country') == $item->country)?'selected':'';?>><?php echo $item->country;?></option>
              <?php endforeach;?>
            </select>
            <button type="submit" class="btn btn-primary btn-sm">Search</button>
          </div>
        </form>
      </div>
    </div> -->

    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
        <br><br>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $this->lang->line('admin_exclude_title', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th><?php echo $this->lang->line('admin_exclude_name', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_exclude_category', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_exclude_id', FALSE); ?></th>
                  <th width="10%"></th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($exclude_group)):?>
                <?php $n = 1; foreach($exclude_group as $item):?>
                  <tr>
                    <td><?php echo $n;?></td>
                    <td><?php echo $item->name;?></td>
                    <td><?php echo $item->category;?></td>
                    <td><?php echo $item->exclude_group_id;?></td>
                    <td>
                      <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?php echo $item->id?>" data-name="<?php echo $item->name?>" data-exclude_group_id="<?php echo $item->exclude_group_id?>" data-category="<?php echo $item->category?>"><i class="fa fa-pencil-square-o"></i></button>
                      <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?php echo $item->id?>" data-name="<?php echo $item->name?>" ><i class="fa fa-trash-o"></i></button>
                    </td>
                  </tr>
                  <?php $n++; endforeach;?>
                <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div id="modal-add" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_exclude_create_exclude_group', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/exclude_group/add");?>">
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="name" >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_category', FALSE); ?></label>
                    <input type="text" class="form-control" name="category" >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_id', FALSE); ?></label>
                    <input type="text" class="form-control" name="exclude_group_id" required >
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

      <div id="modal-edit" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_exclude_edit_exclude_group', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/exclude_group/edit");?>">
                <input type="hidden" id="edit_id" name="edit_id" />
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="name" >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_category', FALSE); ?></label>
                    <input type="text" class="form-control" name="category" >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_exclude_id', FALSE); ?></label>
                    <input type="text" class="form-control" name="exclude_group_id" required >
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>



    </section>


    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>

    <!-- Select2 -->

    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js")?>"></script>

    <script>
    function edit_item(t){
      $('#modal-edit input[name="name"]').val($(t).data('name'));
      $('#modal-edit input[name="category"]').val($(t).data('category'));
      $('#modal-edit input[name="exclude_group_id"]').val($(t).data('exclude_group_id'));

      $('#edit_id').val($(t).data('id'));
      $('#modal-edit').modal();
    }

    function delete_item(t){
      Swal.fire({
        title: 'Are you sure to delete <br /> "'+$(t).data('name')+'"?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: "POST",
            url: '<?=base_url("sensha-admin/exclude_group/delete/")?>'+$(t).data('id'),
          })
          .done(function( msg ) {
            // r = JSON.parse(msg);
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
            setTimeout(function(){ window.location = ''; }, 1400);
          })
          .fail(function( msg ) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
        }
      });
    }

    $(function(){
      $('#list_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength": 30
      });

      $('.select2').select2();

      $('#form_add, #form_edit').submit(function(){
        $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $( this ).serialize()
        })
        .done(function( msg ) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if(r.error == 0){
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick:false
            });
            setTimeout(function(){ location.reload(); }, 1400);
          }
          else{
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function( msg ) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
        return false;
      });
    });
  </script>
