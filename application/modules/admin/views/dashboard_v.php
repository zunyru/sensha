<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_dashboard_title', FALSE); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_dashboard_title', FALSE); ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <form method="post" action="<?php echo base_url("sensha-admin/dashboard"); ?>">
                    <div class="form-group">
                        <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                            <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
                            <select name="country">
                                <option value="">--- Choose Country ---</option>
                                <?php foreach ($nation_lang as $item): ?>
                                    <?php if ($item->country != 'Global'): ?>
                                        <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <select name="month">
                            <option value="">--- Month ---</option>
                            <?php for ($i = 1; $i <= 12; $i++): ?>
                                <option value="<?php echo $i; ?>" <?php echo ($this->input->post('month') == $i) ? 'selected' : ''; ?>><?php echo date('F', strtotime("2019-$i")); ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="year">
                            <option value="">--- Year ---</option>
                            <?php $y = date('Y');
                            for ($i = $y; $i >= 2019; $i--): ?>
                                <option value="<?php echo $i; ?>" <?php echo ($this->input->post('year') == $i) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <button type="submit"
                                class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<?php if ($this->ion_auth->in_group(array('admin'))): ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo $this->lang->line('admin_dashboard_table_title', FALSE); ?>
                                : HQ account</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>

                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?php echo number_format($hq->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($hq->discount_amount); ?></td>
                                            <td><?php echo number_format($hq->delivery_amount); ?></td>

                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format($hq->sa_pay_global); ?></td>
                                            <td><?php echo number_format($hq->sale_total_amount - $hq->sa_pay_global); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>

                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            <td><?php echo number_format($domestic->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->discount_amount); ?></td>
                                            <td><?php echo number_format($domestic->delivery_amount); ?></td>
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->sa_pay_domestic); ?></td>
                                            <td><?php echo number_format($domestic->sale_total_amount - $domestic->sa_pay_domestic); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <br/>
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section4_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_grand_total', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></th>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_section3_title', FALSE); ?></th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?php echo number_format(($hq->sale_total_amount + $domestic->sale_total_amount) - ($release->release_amount + $buy->use_credit)); ?></td>
                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <!-- <td><?php echo number_format($release->release_amount + $buy->use_credit); ?></td> -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($this->ion_auth->in_group(array('SA'))): ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo $this->lang->line('admin_dashboard_table_title', FALSE); ?>
                                : SA account</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!-- <th></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <!-- <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_product', FALSE); ?></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_term', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_total', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_no_of_item', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_average', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <!-- <td><?php echo $month; ?></td> -->
                                            <!-- <td><?php echo number_format($sum_sale_amount + $sum_discount + $sum_shiping_fee); ?></td> -->
                                            <td><?php echo number_format($hq->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($hq->discount_amount); ?></td>
                                            <td><?php echo number_format($hq->delivery_amount); ?></td>
                                            <!-- <td><?php echo number_format($sum_item); ?></td> -->
                                            <!-- <td><?php echo number_format(($sum_sale_amount - $sum_discount) / $sum_item); ?></td> -->
                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format($hq->aa_pay); ?></td>
                                            <td><?php echo number_format((($hq->sa_pay_global != 0) ? $hq->sa_pay_global : $hq->sa_pay_domestic) - $hq->aa_pay); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!-- <th></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <!-- <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_product', FALSE); ?></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_term', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_total', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_no_of_item', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_average', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <!-- <td><?php echo date('F', strtotime($domestic->the_month)); ?></td> -->
                                            <!-- <td><?php echo number_format($sum_sale_amount + $sum_discount + $sum_shiping_fee); ?></td> -->
                                            <td><?php echo number_format($domestic->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->discount_amount); ?></td>
                                            <td><?php echo number_format($domestic->delivery_amount); ?></td>
                                            <!-- <td><?php echo number_format($sum_item); ?></td> -->
                                            <!-- <td><?php echo number_format(($sum_sale_amount - $sum_discount) / $sum_item); ?></td> -->
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->aa_pay); ?></td>
                                            <td><?php echo number_format((($domestic->sa_pay_global != 0) ? $domestic->sa_pay_global : $domestic->sa_pay_domestic) - $domestic->aa_pay); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br/>
                            <!-- <div class="row">
                <div class="col">
                  <h4><?php echo $this->lang->line('admin_dashboard_section3_title', FALSE); ?></h4>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        
                        <th colspan="3"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                      </tr>
                      <tr>
                      
                        <th><?php echo $this->lang->line('admin_dashboard_table_total_payment', FALSE); ?></th>
                        <th><?php echo $this->lang->line('admin_dashboard_table_release', FALSE); ?></th>
                        <th><?php echo $this->lang->line('admin_dashboard_table_buy_products', FALSE); ?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        
                        <td><?php echo number_format($release->release_amount + $buy->use_credit); ?></td>
                        <td><?php echo number_format($release->release_amount); ?></td>
                        <td><?php echo number_format($buy->use_credit); ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <br /> -->
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section4_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_grand_total', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></th>
                                            <!--  <th><?php echo $this->lang->line('admin_dashboard_section3_title', FALSE); ?></th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?php echo number_format(($hq->sale_total_amount + $domestic->sale_total_amount) - ($release->release_amount + $buy->use_credit)); ?></td>
                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <!-- <td><?php echo number_format($release->release_amount + $buy->use_credit); ?></td> -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($this->ion_auth->in_group(array('AA'))): ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"><?php echo $this->lang->line('admin_dashboard_table_title', FALSE); ?>
                                : AA account</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!-- <th></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <!-- <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_product', FALSE); ?></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_term', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_total', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_no_of_item', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_average', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <!-- <td><?php echo $month; ?></td> -->
                                            <!-- <td><?php echo number_format($sum_sale_amount + $sum_discount + $sum_shiping_fee); ?></td> -->
                                            <td><?php echo number_format($hq->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($hq->discount_amount); ?></td>
                                            <td><?php echo number_format($hq->delivery_amount); ?></td>
                                            <!-- <td><?php echo number_format($sum_item); ?></td> -->
                                            <!-- <td><?php echo number_format(($sum_sale_amount - $sum_discount) / $sum_item); ?></td> -->
                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format(0); ?></td>
                                            <td><?php echo number_format($hq->aa_pay); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <!-- <th></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_basic_info', FALSE); ?></th>
                                            <!-- <th colspan="2"><?php echo $this->lang->line('admin_dashboard_table_product', FALSE); ?></th> -->
                                            <th colspan="4"><?php echo $this->lang->line('admin_dashboard_table_income', FALSE); ?></th>
                                        </tr>
                                        <tr>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_term', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_total', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_product_sales', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_discount_amount', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_delivery_fee', FALSE); ?></th>
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_no_of_item', FALSE); ?></th> -->
                                            <!-- <th><?php echo $this->lang->line('admin_dashboard_table_average', FALSE); ?></th> -->
                                            <th><?php echo $this->lang->line('admin_dashboard_table_income2', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_pay_to_agent', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_expected_income', FALSE); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <!-- <td><?php echo date('F', strtotime($domestic->the_month)); ?></td> -->
                                            <!-- <td><?php echo number_format($sum_sale_amount + $sum_discount + $sum_shiping_fee); ?></td> -->
                                            <td><?php echo number_format($domestic->item_sub_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->discount_amount); ?></td>
                                            <td><?php echo number_format($domestic->delivery_amount); ?></td>
                                            <!-- <td><?php echo number_format($sum_item); ?></td> -->
                                            <!-- <td><?php echo number_format(($sum_sale_amount - $sum_discount) / $sum_item); ?></td> -->
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <td><?php echo number_format(0); ?></td>
                                            <td><?php echo number_format($domestic->aa_pay); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col">
                                    <h4><?php echo $this->lang->line('admin_dashboard_section4_title', FALSE); ?></h4>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th><?php echo $this->lang->line('admin_dashboard_table_grand_total', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section1_title', FALSE); ?></th>
                                            <th><?php echo $this->lang->line('admin_dashboard_section2_title', FALSE); ?></th>
                                            <!--  <th><?php //echo $this->lang->line('admin_dashboard_section3_title', FALSE); ?></th> -->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?php echo number_format(($hq->sale_total_amount + $domestic->sale_total_amount) - ($release->release_amount + $buy->use_credit)); ?></td>
                                            <td><?php echo number_format($hq->sale_total_amount); ?></td>
                                            <td><?php echo number_format($domestic->sale_total_amount); ?></td>
                                            <!--  <td><?php //echo number_format($release->release_amount+$buy->use_credit);?></td> -->
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
