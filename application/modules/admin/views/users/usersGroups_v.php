<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Users Groups</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Users Groups v2</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add pull-right"><i class="fa fa-plus"></i> Add</button>
        <br><br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <!-- <div class="card-header">
          <h3 class="card-title">Hover Data Table</h3>
        </div> -->
        <div class="card-body">
          <table class="table table-bordered table-hover no-margin" id="list_table">
            <thead>
              <tr>
                <th width="5%">#</th>
                <th>Name</th>
                <th>Description</th>
                <th width="10%"></th>
              </tr>
            </thead>
            <tbody>
              <?php $n=1; foreach($groups_list as $item):?>
                <tr>
                  <td><?=$n?></td>
                  <td><?=$item->name?></td>
                  <td><?=$item->description?></td>
                  <td>
                    <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?=$item->id?>" data-name="<?=$item->name?>" data-des="<?=$item->description?>"><i class="fa fa-pencil-square-o"></i></button>
                  </td>
                </tr>
                <?php $n++; endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="modal-add" class="modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add User Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_add" method="post" enctype="multipart/form-data" action="<?=base_url("auth/create_group")?>">
          <div class="form-group">
            <input type="text" class="form-control" name="group_name" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="description" placeholder="Description">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<div id="modal-edit" class="modal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit User Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form_edit" method="post" enctype="multipart/form-data" action="<?=base_url('auth/edit_group')?>">
          <input type="hidden" id="edit_id" name="edit_id">
          <div class="form-group">
            <input type="text" class="form-control" id="desname" name="group_name" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="description" name="description" placeholder="Description">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>




<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>


<script>

function edit_item(t){
  $('#modal-edit #desname').val($(t).data('name'));
  $('#modal-edit #description').val($(t).data('des'));

  $('#edit_id').val($(t).data('id'));
  $('#modal-edit').modal();
}

$(function(){
  $('#list_table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "pageLength": 30
  });

  $('.btn-add').click(function(){
    $('#modal-add').modal();
  });


  $('#form_add').submit(function(){
    $.ajax({
      method: "POST",
      url: $(this).attr('action'),
      data: $( this ).serialize()
    })
    .done(function( msg ) {
      // console.log(msg);
      r = JSON.parse(msg);
      console.log(r);
      if(r.error == 0){
        Swal.fire({
          type: 'success',
          title: 'Add data complete',
          showConfirmButton: false,
          allowOutsideClick:false
        });
        setTimeout(function(){ location.reload(); }, 1400);
      }
      else{
        Swal.fire(
          'Oops...',
          'Something went wrong!',
          'error'
        );
      }
    })
    .fail(function( msg ) {
      Swal.fire(
        'Oops...',
        msg,
        'error'
      );
    });
    return false;
  });

  $('#form_edit').submit(function(){
    $.ajax({
      method: "POST",
      url: $(this).attr('action'),
      data: $( this ).serialize()
    })
    .done(function( msg ) {
      // console.log(msg);
      r = JSON.parse(msg);
      console.log(r);
      if(r.error == 0){
        Swal.fire({
          type: 'success',
          title: 'Update data complete',
          showConfirmButton: false,
          allowOutsideClick:false
        });
        setTimeout(function(){ location.reload(); }, 1400);
      }
      else{
        Swal.fire(
          'Oops...',
          'Something went wrong!',
          'error'
        );
      }
    })
    .fail(function( msg ) {
      Swal.fire(
        'Oops...',
        msg,
        'error'
      );
    });
    return false;
  });
});

</script>
