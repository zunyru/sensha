<style>
    /* .modal-lg{
    max-width: 80%;
    } */
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_product_management_title', FALSE); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_product_management_title', FALSE); ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <?php if ($this->ion_auth->in_group(array('admin'))): ?>
            <div class="row">
                <div class="col-md-4">
                    <!--        <form method="post" action="-->
                    <?php //echo base_url("sensha-admin/product_management/ppf");?><!--">-->
                    <div class="form-group">
                        <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
                        <select name="country" id="filter_country">
                            <option value="">--- Choose Country ---</option>
                            <?php foreach ($nation_lang as $item): ?>
                                <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button type="submit" class="btn btn-primary btn-sm"
                                id="filter"><?php echo $this->lang->line('global_search', FALSE); ?></button>
                    </div>
                    <!--        </form>-->
                </div>
            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <a href="<?php echo base_url("sensha-admin/product_management"); ?>"
                       class="btn btn-outline-info btn-flat"><?php echo $this->lang->line('admin_product_cat_general', FALSE); ?></a>
                    <a href="<?php echo base_url("sensha-admin/product_management/ppf"); ?>"
                       class="btn btn-outline-secondary btn-flat disabled"><?php echo $this->lang->line('admin_product_cat_ppf', FALSE); ?></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add">
                        <i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
                <?php endif; ?>
                <a href="<?php echo base_url("sensha-admin/product_management/export_ppf"); ?>"
                   class="btn btn-info"> <?php echo $this->lang->line('global_export', FALSE); ?></a>
                <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                    <button type="button" class="btn btn-info" data-toggle="modal"
                            data-target="#modal-import"> <?php echo $this->lang->line('global_import', FALSE); ?></button>
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal"
                            data-target="#modal-upload_multi_image"><i
                                class="fa fa-upload"></i> <?php echo $this->lang->line('admin_product_management_upload', FALSE); ?>
                    </button>
                <?php endif; ?>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Products PPF</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover no-margin" id="list_table">
                            <thead>
                            <tr>
                                <th width="5%">SKU</th>
                                <th><?php echo $this->lang->line('admin_product_management_product_name', FALSE); ?></th>
                                <th>Country</th>
                                <th><?php echo $this->lang->line('admin_product_management_global_price', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_product_management_domestic_price', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_product_management_active', FALSE); ?></th>
                                <th width="10%"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-add" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Product Management</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/product_management/add_ppf"); ?>">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_product_id', FALSE); ?></label>
                                            <input type="text" class="form-control" name="product_id" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_product_management_product_name', FALSE); ?></label>
                                    <input type="text" class="form-control" name="product_name" required>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_1', FALSE); ?></label>
                                            <select class="form-control" name="cat_1" style="width: 100%;" required>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster1 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level1="<?php echo $item->id; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_2', FALSE); ?></label>
                                            <select class="form-control" name="cat_2" style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster2 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level2="<?php echo $item->id; ?>"
                                                            data-level1="<?php echo $item->level1; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_3', FALSE); ?></label>
                                            <select class="form-control" name="cat_3" style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster3 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level2="<?php echo $item->level2; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_product_weight', FALSE); ?></label>
                                            <input type="text" class="form-control" name="product_weight">
                                        </div>
                                    </div>
                                    <!-- <div class="col">
                      <div class="form-group">
                        <label>Production Country</label>
                        <select class="form-control select2" name="product_country" style="width: 100%;" required>
                          <option selected="selected" value="">-- Choose --</option>
                          <?php foreach ($nation_lang as $item): ?>
                            <option value="<?php echo $item->country; ?>"><?php echo $item->country; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_global_price', FALSE); ?></label>
                                            <input type="text" class="form-control" name="global_price">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <!--
                        <label><?php echo $this->lang->line('admin_product_management_fixed_price', FALSE); ?></label>
                        <input type="text" class="form-control" name="fixed_price"  >
-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_sa_price', FALSE); ?></label>
                                            <input type="text" class="form-control" name="sa_price">
                                        </div>
                                    </div>
                                    <!--<div class="col">
                                        <div class="form-group">
                                            <label><?php /*echo $this->lang->line('admin_product_management_import_shipping', FALSE); */?></label>
                                            <input type="text" class="form-control" name="import_shipping">
                                        </div>
                                    </div>-->
                                </div>
                                <div class="row">
                                   <!-- <div class="col">
                                        <div class="form-group">
                                            <label><?php /*echo $this->lang->line('admin_product_management_import_tax', FALSE); */?></label>
                                            <input type="text" class="form-control" name="import_tax">
                                        </div>
                                    </div>-->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_no_of_use', FALSE); ?></label>
                                            <input type="text" class="form-control" name="no_of_use">
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php /*echo $this->lang->line('admin_product_management_cost_per_use', FALSE); */ ?></label>
                        <input type="text" class="form-control" name="cost_per_use"  >
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label><?php /*echo $this->lang->line('admin_product_management_fixed_delivery_price', FALSE); */ ?></label>
                        <input type="text" class="form-control" name="fixed_delivery_price"  >
                      </div>
                    </div>
                  </div>-->
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_parts', FALSE); ?></label>
                                            <!--<input type="text" class="form-control" name="parts" value="Headlight" readonly>-->
                                            <select class="form-control" name="parts">
                                                <option value="headlight">Headlight</option>
                                                <option value="f-bumper">F-bumper</option>
                                                <option value="r-bumper">R-bumper</option>
                                                <option value="bumper-set">Bumper set</option>
                                                <option value="luggage">Luggage</option>
                                                <option value="doorcup">Doorcup</option>
                                                <option value="side-spoiler">Side spoiler</option>
                                                <option value="aluminium-mold">Aluminium Mold</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="col">
                      <div class="form-group">
                        <label><?php /*echo $this->lang->line('admin_product_management_feature', FALSE); */ ?></label>
                        <input type="text" class="form-control" name="feature" readonly >
                      </div>
                    </div>-->
                                </div>
                                <div class="row">
                                    <!-- <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_product_management_no_of_stock', FALSE); ?></label>
                        <input type="text" class="form-control" name="no_of_stock"  >
                      </div>
                    </div> -->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_item_description', FALSE); ?></label>
                                            <input type="text" class="form-control" name="item_description">
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php /*echo $this->lang->line('admin_product_management_car_production_id', FALSE); */ ?></label>
                        <input type="text" class="form-control" name="car_production_id"  >
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label><?php /*echo $this->lang->line('admin_product_management_begining_term_to_use', FALSE); */ ?></label>
                        <input type="text" class="form-control" name="begining_term_to_use"  >
                      </div>
                    </div>
                  </div>-->
                                <!-- <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_product_management_ending_term_to_use', FALSE); ?></label>
                        <input type="text" class="form-control" name="ending_term_to_use"  >
                      </div>
                    </div>
                  </div> -->
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_content', FALSE); ?></label>
                                            <input type="text" class="form-control" name="content">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_caution', FALSE); ?></label>
                                            <input type="text" class="form-control" name="caution">
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="form-group">
                    <label><?php /*echo $this->lang->line('admin_product_management_html_content', FALSE); */ ?></label>
                    <textarea id="content1" rows="5" class="form-control" name="html_content"  ></textarea>
                  </div>-->
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_image', FALSE); ?></label>
                                            <!-- <input type="file" class="form-control" name="image"  > -->
                                            <div class="preview_image">

                                            </div>
                                            <button type="button"
                                                    class="btn btn-success form-control btn-add-image"><?php echo $this->lang->line('admin_product_management_add_image', FALSE); ?></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_youtube_videos', FALSE); ?></label>
                                            <input type="text" class="form-control" name="youtube">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <!--
                        <label><?php echo $this->lang->line('admin_product_management_unpurchasable_user', FALSE); ?></label>
                        <input type="text" class="form-control" name="unpurchasable_user"  >
-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_active', FALSE); ?></label>
                                            <select class="form-control" name="is_active">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                <div class="col">
                                <div class="form-group">
                                <label>Registration Date</label>
                                <input type="text" class="form-control" name="registration_date"  >
                              </div>
                            </div>
                            <div class="col">
                            <div class="form-group">
                            <label>Last Modification Date</label>
                            <input type="text" class="form-control" name="last_modification_date"  >
                          </div>
                        </div>
                      </div> -->
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                            <input type="hidden" name="set_product_image" value="">
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div id="modal-edit" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Product Management</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_edit" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/product_management/edit_ppf"); ?>">
                            <input type="hidden" id="edit_id" name="edit_id"/>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_product_id', FALSE); ?></label>
                                            <input type="text" class="form-control" name="product_id" readonly required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_product_management_product_name', FALSE); ?></label>
                                    <input type="text" class="form-control" name="product_name" required>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_1', FALSE); ?></label>
                                            <select class="form-control" name="cat_1"
                                                    style="width: 100%;" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'enabled' : ''; ?>
                                                    required>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster1 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level1="<?php echo $item->id; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_2', FALSE); ?></label>
                                            <select class="form-control" name="cat_2"
                                                    style="width: 100%;" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'enabled' : ''; ?>>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster2 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level2="<?php echo $item->id; ?>"
                                                            data-level1="<?php echo $item->level1; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_cat_3', FALSE); ?></label>
                                            <select class="form-control" name="cat_3"
                                                    style="width: 100%;" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'enabled' : ''; ?>>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($cluster3 as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"
                                                            data-country="<?php echo $item->country; ?>"
                                                            data-level2="<?php echo $item->level2; ?>"><?php echo $item->cluster_name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_product_weight', FALSE); ?></label>
                                            <input type="text" class="form-control"
                                                   name="product_weight" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'readonly' : ''; ?>>
                                        </div>
                                    </div>
                                    <!-- <div class="col">
                <div class="form-group">
                  <label>Production Country</label>
                  <select class="form-control select2" name="product_country" style="width: 100%;" required>
                    <option selected="selected" value="">-- Choose --</option>
                    <?php foreach ($nation_lang as $item): ?>
                      <option value="<?php echo $item->country; ?>"><?php echo $item->country; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div> -->
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_global_price', FALSE); ?></label>
                                            <input type="text" class="form-control"
                                                   name="global_price" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'readonly' : ''; ?> >
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <!--
                  <label><?php echo $this->lang->line('admin_product_management_fixed_price', FALSE); ?></label>
                  <input type="text" class="form-control" name="fixed_price"  >
-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_sa_price', FALSE); ?></label>
                                            <input type="text" class="form-control" name="sa_price">
                                        </div>
                                    </div>
                                    <!--<div class="col">
                                        <div class="form-group">
                                            <label><?php /*echo $this->lang->line('admin_product_management_import_shipping', FALSE); */?></label>
                                            <input type="text" class="form-control" name="import_shipping">
                                        </div>
                                    </div>-->
                                </div>
                                <div class="row">
                                    <!--<div class="col">
                                        <div class="form-group">
                                            <label><?php /*echo $this->lang->line('admin_product_management_import_tax', FALSE); */?></label>
                                            <input type="text" class="form-control" name="import_tax">
                                        </div>
                                    </div>-->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_no_of_use', FALSE); ?></label>
                                            <input type="text" class="form-control"
                                                   name="no_of_use" <?php echo ($this->ion_auth->in_group(array('SA'))) ? 'readonly' : ''; ?> >
                                        </div>
                                    </div>
                                </div>
                                <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                                    <!--<div class="row">
              <div class="col">
                <div class="form-group">
                  <label><?php /*echo $this->lang->line('admin_product_management_cost_per_use', FALSE); */ ?></label>
                  <input type="text" class="form-control" name="cost_per_use"  >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label><?php /*echo $this->lang->line('admin_product_management_fixed_delivery_price', FALSE); */ ?></label>
                  <input type="text" class="form-control" name="fixed_delivery_price"  >
                </div>
              </div>
            </div>-->
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label><?php echo $this->lang->line('admin_product_management_parts', FALSE); ?></label>
                                                <!--<input type="text" class="form-control" name="parts" value="Headlight" readonly>-->
                                                <select class="form-control" name="parts">
                                                    <option value="headlight">Headlight</option>
                                                    <option value="f-bumper">F-bumper</option>
                                                    <option value="r-bumper">R-bumper</option>
                                                    <option value="bumper-set">Bumper set</option>
                                                    <option value="luggage">Luggage</option>
                                                    <option value="doorcup">Doorcup</option>
                                                    <option value="side-spoiler">Side spoiler</option>
                                                    <option value="aluminium-mold">Aluminium Mold</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col">
                <div class="form-group">
                  <label><?php /*echo $this->lang->line('admin_product_management_feature', FALSE); */ ?></label>
                  <input type="text" class="form-control" name="feature" readonly >
                </div>
              </div>-->
                                    </div>
                                <?php endif; ?>
                                <div class="row">
                                    <!-- <div class="col">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_product_management_no_of_stock', FALSE); ?></label>
                  <input type="text" class="form-control" name="no_of_stock"  >
                </div>
              </div> -->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_item_description', FALSE); ?></label>
                                            <input type="text" class="form-control" name="item_description">
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="row">
              <div class="col">
                <div class="form-group">
                  <label><?php /*echo $this->lang->line('admin_product_management_car_production_id', FALSE); */ ?></label>
                  <input type="text" class="form-control" name="car_production_id"  >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label><?php /*echo $this->lang->line('admin_product_management_begining_term_to_use', FALSE); */ ?></label>
                  <input type="text" class="form-control" name="begining_term_to_use"  >
                </div>
              </div>
            </div>-->
                                <!-- <div class="row">
              <div class="col-6">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_product_management_ending_term_to_use', FALSE); ?></label>
                  <input type="text" class="form-control" name="ending_term_to_use"  >
                </div>
              </div>
            </div> -->
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_content', FALSE); ?></label>
                                            <input type="text" class="form-control" name="content">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_caution', FALSE); ?></label>
                                            <input type="text" class="form-control" name="caution">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display: none">
                                    <label><?php echo $this->lang->line('admin_product_management_html_content', FALSE); ?></label>
                                    <textarea id="content2" rows="5" class="form-control"
                                              name="html_content"></textarea>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_image', FALSE); ?></label>
                                            <!-- <input type="file" class="form-control" name="image"  > -->
                                            <div class="preview_image">

                                            </div>
                                            <button type="button"
                                                    class="btn btn-success form-control btn-add-image"><?php echo $this->lang->line('admin_product_management_add_image', FALSE); ?></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_youtube_videos', FALSE); ?></label>
                                            <input type="text" class="form-control" name="youtube">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <!--
                  <label><?php echo $this->lang->line('admin_product_management_unpurchasable_user', FALSE); ?></label>
                  <input type="text" class="form-control" name="unpurchasable_user"  >
-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_management_active', FALSE); ?></label>
                                            <select class="form-control" name="is_active">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                <div class="col">
                                <div class="form-group">
                                <label>Registration Date</label>
                                <input type="text" class="form-control" name="registration_date"  >
                              </div>
                            </div>
                            <div class="col">
                            <div class="form-group">
                            <label>Last Modification Date</label>
                            <input type="text" class="form-control" name="last_modification_date"  >
                          </div>
                        </div>
                      </div> -->
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                            <input type="hidden" name="set_product_image" value="">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-import" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Import Product Management</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_import" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/product_management/import_ppf"); ?>">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Choose File import</label>
                                    <input type="file" class="form-control" name="file_import" required/>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

</section>

<form class="hide" role="form" id="form_add_product_image" method="post" enctype="multipart/form-data"
      action="<?php echo base_url("sensha-admin/product_management/add_product_image_ppf"); ?>">
    <input type="file" name="product_image" onchange="readURL(this)"/>
</form>

<div id="modal-upload_multi_image" class="modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo $this->lang->line('admin_product_management_upload', FALSE); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                      action="<?php echo base_url("sensha-admin/account_setting/add"); ?>">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Choose image</label>
                            <input type="text" class="form-control" name="upload_multi_image" data-max-file-size="3MB"
                                   multiple/>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <!-- <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div> -->
                </form>
            </div>

        </div>
    </div>
</div>


<!-- DataTables -->
<link rel="stylesheet"
      href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>

<!-- filepond-plugin-file-validate-size -->
<script src="<?php echo base_url("node_modules/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js") ?>"></script>

<!-- filepond-plugin-image-preview -->
<link href="<?php echo base_url("node_modules/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css") ?>"
      rel="stylesheet">
<script src="<?php echo base_url("node_modules/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js") ?>"></script>

<!-- filepond -->
<link rel="stylesheet" href="<?php echo base_url("node_modules/filepond/dist/filepond.css") ?>">
<script src="<?php echo base_url("node_modules/filepond/dist/filepond.js") ?>"></script>

<!-- ckeditor -->
<!-- <script src="https://cdn.ckeditor.com/4.11.2/standard-all/ckeditor.js"></script> -->
<script src="<?php echo base_url("assets/ckeditor/ckeditor.js") ?>"></script>


<script>
    const inputElement = document.querySelector('input[name="upload_multi_image"]');
    const pond = FilePond.create(inputElement);
    // FilePond.registerPlugin(
    //   FilePondPluginImagePreview,
    //   // FilePondPluginImageExifOrientation,
    //   FilePondPluginFileValidateSize
    // );

    FilePond.setOptions({
        server: {
            process: '<?php echo base_url("sensha-admin/product_management/upload_product_image_ppf")?>',
            fetch: null,
            revert: '<?php echo base_url("sensha-admin/product_management/delete_product_image_ppf")?>'
        }
    });

    function readURL(input) {
        // if($('#form_add .preview_image img, #form_edit .preview_image img').length >= 4){
        //   return false;
        // }
        // else if(input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var html = '<div style="display: inline-block; padding: 5px;"><img src="' + e.target.result + '" alt="your image" width="150" data-image="" /><button type="button" class="close" onclick="delete_product_image(this)" style="position: relative; float: right; top: 0px; right: 12px;"><span aria-hidden="true">×</span></button></div>';
            $('.preview_image').append(html);
            $('#form_add_product_image').submit();
            // $(input).parent().find('.preview_image').attr('src', e.target.result);
            // $(input).parent().find('.preview_image').show();
        }
        reader.readAsDataURL(input.files[0]);
        // }
    }

    function edit_item(t, c) {

        // $('#modal-edit select[name="cat_1"]').select2("val", $(t).data('cat_1'));
        // $('#modal-edit select[name="cat_2"]').select2("val", $(t).data('cat_2'));
        // $('#modal-edit select[name="cat_3"]').select2("val", $(t).data('cat_3'));
        $('select[name="cat_1"]').children('option:gt(0)').hide();
        <?php if($this->ion_auth->in_group(array('admin'))){ ?>
        $('select[name="cat_1"]').children("option[data-country='" + $(t).data('product_country') + "']").show()
        <?php } ?>

        $('select[name="cat_2"]').children('option:gt(0)').hide();
        <?php if($this->ion_auth->in_group(array('admin'))){ ?>
        $('select[name="cat_2"]').children("option[data-country='" + $(t).data('product_country') + "']").show()
        <?php } ?>

        $('select[name="cat_3"]').children('option:gt(0)').hide();
        <?php if($this->ion_auth->in_group(array('admin'))){ ?>
        $('select[name="cat_3"]').children("option[data-country='" + $(t).data('product_country') + "']").show()
        <?php } ?>

        $('#modal-edit select[name="cat_1"]').val($(t).data('cat_1'));
        $('#modal-edit select[name="cat_2"]').val($(t).data('cat_2'));
        $('#modal-edit select[name="cat_3"]').val($(t).data('cat_3'));
        // $('#modal-edit select[name="product_country"]').select2("val", $(t).data('product_country'));
        $('#modal-edit select[name="is_active"]').val($(t).data('is_active'));

        $('#modal-edit input[name="product_id"]').val($(t).data('product_id'));
        $('#modal-edit input[name="product_name"]').val($(t).data('product_name'));
        $('#modal-edit input[name="product_weight"]').val($(t).data('product_weight'));
        $('#modal-edit input[name="global_price"]').val($(t).data('global_price'));
        $('#modal-edit input[name="fixed_price"]').val($(t).data('fixed_price'));
        $('#modal-edit input[name="sa_price"]').val($(t).data('sa_price'));
        $('#modal-edit input[name="import_shipping"]').val($(t).data('import_shipping'));
        $('#modal-edit input[name="import_tax"]').val($(t).data('import_tax'));
        $('#modal-edit input[name="no_of_use"]').val($(t).data('no_of_use'));
        $('#modal-edit input[name="cost_per_use"]').val($(t).data('cost_per_use'));
        $('#modal-edit input[name="fixed_delivery_price"]').val($(t).data('fixed_delivery_price'));
        $('#modal-edit select[name="parts"]').val($(t).data('parts'));
        // $('#modal-edit input[name="parts"]').val($(t).data('parts'));
        // $('#modal-edit input[name="feature"]').val($(t).data('feature'));
        $('#modal-edit input[name="domestic"]').val($(t).data('domestic'));
        $('#modal-edit input[name="oversea"]').val($(t).data('oversea'));
        $('#modal-edit input[name="no_of_stock"]').val($(t).data('no_of_stock'));
        $('#modal-edit input[name="item_description"]').val($(t).data('item_description'));
        $('#modal-edit input[name="car_production_id"]').val($(t).data('car_production_id'));
        $('#modal-edit input[name="begining_term_to_use"]').val($(t).data('begining_term_to_use'));
        $('#modal-edit input[name="ending_term_to_use"]').val($(t).data('ending_term_to_use'));
        $('#modal-edit input[name="content"]').val($(t).data('content'));
        $('#modal-edit input[name="caution"]').val($(t).data('caution'));
        // $('#modal-edit input[name="html_content"]').val($(t).data('html_content'));
        //CKEDITOR.instances['content2'].setData($(t).data('html_content'));
        $('#modal-edit input[name="youtube"]').val($(t).data('youtube'));
        $('#modal-edit input[name="unpurchasable_user"]').val($(t).data('unpurchasable_user'));
        $('#modal-edit input[name="registration_date"]').val($(t).data('registration_date'));
        $('#modal-edit input[name="last_modification_date"]').val($(t).data('last_modification_date'));
        $('.preview_image').html('');
        var s = $(t).data('image').split(',');
        for (i = 0; i < s.length; i++) {
            var html = '<div style="display: inline-block; padding: 5px;"><img src="' + '<?php echo base_url("uploads/product_image/")?>' + s[i] + '" alt="your image" width="150" data-image="' + s[i] + '" /><button type="button" class="close" onclick="delete_product_image(this)" style="position: relative; float: right; top: 0px; right: 12px;"><span aria-hidden="true">×</span></button></div>';
            $('.preview_image').append(html);
        }


        $('#edit_id').val($(t).data('id'));
        $('#modal-edit').modal();
    }

    function delete_item(t) {
        Swal.fire({
            title: 'Are you sure to delete <br /> "' + $(t).data('product_name') + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?=base_url("sensha-admin/product_management/delete_ppf/")?>' + $(t).data('id'),
                })
                    .done(function (msg) {
                        // r = JSON.parse(msg);
                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your data has been deleted.',
                            type: 'success',
                            showConfirmButton: false,
                        })
                        setTimeout(function () {
                            window.location = '';
                        }, 1400);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    var product_image = [];

    function delete_product_image(t) {
        $(t).parent().remove();
        // var index = product_image.indexOf(t);
        // if (index > -1) {
        //   product_image.splice(index, 1);
        // }
    }

    $(function () {
        /*$('#list_table').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "pageLength": 30
        });*/

        fill_datatable();

        function fill_datatable(filter_country = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/product_management/data_index_ppf" ?>`;
            var dataList = $('#list_table');
            dataList.DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data: {
                        filter_country: filter_country
                    }
                },
                order: [[0, "asc"]],
                pageLength: 100,
                lengthMenu: [100, 150, 200, 300, 500, 1000],
                columns: [
                    {data: "product_id", width: "5%", orderable: false},
                    {data: "product_name", width: "30%", orderable: false},
                    {data: "product_country", orderable: false},
                    {data: "global_price", orderable: false},
                    {data: "sa_price", orderable: false},
                    {data: "is_active", orderable: false},
                    {data: "action", orderable: false},
                ]
            });
        }

        $(document).ready(function () {

            $('#filter').click(function () {
                var filter_country = $('#filter_country').val();
                if (filter_country != '') {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country);
                } else {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country);
                }
            });
        });

        // $('.select2').select2();
        $('#modal-add').on('shown.bs.modal', function (e) {
            $('select[name="cat_1"]').children('option:gt(0)').hide();
            $('select[name="cat_1"]').children('option').eq(0).show();
            $('select[name="cat_1"]').children("option[data-country='Global']").show();

            $('select[name="cat_2"]').children('option:gt(0)').hide();
            $('select[name="cat_2"]').children('option').eq(0).show();
            $('select[name="cat_2"]').children("option[data-country='Global']").show();

            $('select[name="cat_3"]').children('option:gt(0)').hide();
            $('select[name="cat_3"]').children('option').eq(0).show();
            $('select[name="cat_3"]').children("option[data-country='Global']").show();
        });

        $('#modal-edit input[name="no_of_use"]').change(function () {
            var global_price = $('#modal-edit input[name="global_price"]').val() * 1;
            var no_of_use = $('#modal-edit input[name="no_of_use"]').val() * 1;
            $('#modal-edit input[name="cost_per_use"]').val(Math.ceil(global_price / no_of_use));
        });

        $('#modal-add input[name="no_of_use"]').change(function () {
            var global_price = $('#modal-add input[name="global_price"]').val() * 1;
            var no_of_use = $('#modal-add input[name="no_of_use"]').val() * 1;
            $('#modal-add input[name="cost_per_use"]').val(Math.ceil(global_price / no_of_use));
        });

        $('select[name="cat_2"]').children('option:gt(0)').hide();
        $('select[name="cat_3"]').children('option:gt(0)').hide();
        $('select[name="cat_1"]').change(function () {
            $('select[name="cat_2"]').children('option:gt(0)').hide();
            $('select[name="cat_2"]').val('');
            $('select[name="cat_3"]').val('');
            $('select[name="cat_2"]').children("option[data-level1='" + $(this).find(':selected').data('level1') + "']").show()
        });

        $('select[name="cat_2"]').change(function () {
            $('select[name="cat_3"]').children('option:gt(0)').hide();
            $('select[name="cat_3"]').val('');
            $('select[name="cat_3"]').children("option[data-level2='" + $(this).find(':selected').data('level2') + "']").show()
        });

        $('.btn-add').click(function () {
            $('.preview_image').html('');
        });

        $('#form_add, #form_edit, #form_import').submit(function () {
            var img_list = [];
            $(this).find('.preview_image img').each(function () {
                img_list.push($(this).data('image'));
            });
            $(this).find('input[name="set_product_image"]').val(img_list.join());

            /*if($(this).attr('id') == 'form_add'){
              $('#content1').val(CKEDITOR.instances.content1.getData());
            }
            if($(this).attr('id') == 'form_edit'){
              $('#content2').val(CKEDITOR.instances.content2.getData());
            }*/

            var formData = new FormData($(this)[0]);
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        Swal.fire({
                            type: 'success',
                            title: 'success',
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1400);
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });

        $('.btn-add-image').click(function () {
            if ($('#form_add .preview_image img').length >= 4 && $('#form_edit .preview_image img').length >= 4) {
                Swal.fire({
                    type: 'warning',
                    title: 'You can choose only 4 images.',
                    showConfirmButton: true,
                    allowOutsideClick: false
                });
            } else {
                $('input[name="product_image"]').click();
            }
        });


        $('#form_add_product_image').submit(function () {
            Swal.fire({
                title: 'Processing',
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    swal.showLoading();
                }
            });
            var formData = new FormData($(this)[0]);
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        // Swal.fire({
                        //   type: 'success',
                        //   title: 'success',
                        //   showConfirmButton: false,
                        //   allowOutsideClick:false
                        // });
                        // setTimeout(function(){ location.reload(); }, 1400);

                        // console.log(r.file_name);
                        product_image.push(r.file_name);
                        // $('input[name="set_product_image"]').val(product_image.join());
                        $('#form_add .preview_image img').last().data('image', r.file_name);
                        $('#form_edit .preview_image img').last().data('image', r.file_name);
                        swal.close();
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });

    });

    /*CKEDITOR.replace( 'content1',{
      extraPlugins: 'uploadimage',
      uploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '/apps/ckfinder/3.4.4/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url("uploads/webpage_setting")?>',
  filebrowserUploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl: '<?php echo base_url("sensha-admin/webpage_setting/editorUploadImage")?>',
  // removeDialogTabs: 'image:advanced;image:Link;image:Image Info;link:advanced;'
});

CKEDITOR.replace( 'content2',{
  extraPlugins: 'uploadimage',
  uploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
  // Configure your file manager integration. This example uses CKFinder 3 for PHP.
  filebrowserBrowseUrl: '/apps/ckfinder/3.4.4/ckfinder.html',
  filebrowserImageBrowseUrl: '<?php echo base_url("uploads/webpage_setting")?>',
  filebrowserUploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl: '<?php echo base_url("sensha-admin/webpage_setting/editorUploadImage")?>',
  // removeDialogTabs: 'image:advanced;image:Link;image:Image Info;link:advanced;'
});*/

    /*CKEDITOR.on('dialogDefinition', function( ev )
    {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;

      switch (dialogName) {
        case 'image': //Image Properties dialog
        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');
        // dialogDefinition.removeContents('info');
        break;
        case 'link': //image Properties dialog
        break;
      }
    });*/


</script>
