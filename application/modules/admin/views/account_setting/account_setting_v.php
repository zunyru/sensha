<style>
    /* .modal-lg{
    width: 80%;
    } */

    .parent_agent {
        display: none;
    }

    .comission_setting {
        display: none;
    }

    .discount_setting {
        display: none;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_account_setting_title', FALSE); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_account_setting_title', FALSE); ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <?php if ($this->ion_auth->in_group(array('admin'))): ?>
            <div class="row">
                <div class="col-md-4">
                    <!--          <form method="post" action="-->
                    <?php //echo base_url("sensha-admin/account_setting");?><!--">-->
                    <div class="form-group">
                        <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
                        <select name="country" id="filter_country">
                            <option value="">--- Choose Country ---</option>
                            <?php foreach ($nation_lang as $item): ?>
                                <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button type="submit" class="btn btn-primary btn-sm"
                                id="filter"><?php echo $this->lang->line('global_search', FALSE); ?></button>
                    </div>
                    <!--          </form>-->
                </div>
            </div>
        <?php endif; ?>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add">
                        <i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
                    <a href="<?php echo base_url("sensha-admin/account_setting/export") ?>"
                       class="btn btn-info"> <?php echo $this->lang->line('global_export', FALSE); ?></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"><?php echo $this->lang->line('admin_account_setting_title', FALSE); ?></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover no-margin" id="list_table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th><?php echo $this->lang->line('admin_account_setting_email', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_user_id', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_country', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_account_type', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_company_name', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_company_phone', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_discount_setting', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_account_setting_comission_setting', FALSE); ?></th>
                                <th width="10%"></th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-add" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Account Setting</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/account_setting/add"); ?>">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_email', FALSE); ?></label>
                                            <input type="text" class="form-control" name="email" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_password', FALSE); ?></label>
                                            <input type="password" class="form-control" name="password" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_country', FALSE); ?></label>
                                            <select class="form-control select2" name="country" style="width: 100%;"
                                                    required>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($nation_lang as $item): ?>
                                                    <option value="<?php echo $item->country; ?>"><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_account_type', FALSE); ?></label>
                                            <select class="form-control select2" name="account_type"
                                                    style="width: 100%;" required>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($groups_list as $item): ?>
                                                    <?php if ($this->ion_auth->in_group(array('admin'))): ?>
                                                        <?php if ($item->name == "SA"): ?>
                                                            <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php if ($this->ion_auth->in_group(array('SA'))): ?>
                                                        <?php if ($item->name == "AA" || $item->name == "FC"): ?>
                                                            <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row parent_agent">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Parent Agent</label>
                                            <select class="form-control select2" name="parent_agent"
                                                    style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($user_aa as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"><?php echo $item->email; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">

                                    </div>
                                </div>

                                <div class="row">
                                    <!--<div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_id', FALSE); ?></label>
                        <input type="text" class="form-control" readonly="readonly">
                      </div>
                    </div>-->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_company_name', FALSE); ?></label>
                                            <input type="text" class="form-control" name="company_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_contact_person', FALSE); ?></label>
                                            <input type="text" class="form-control" name="contact_person">
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_zipcode', FALSE); ?></label>
                                            <input type="text" class="form-control" name="zipcode">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label style="width: 100%;"><?php echo $this->lang->line('admin_account_setting_company_address', FALSE); ?></label>
                                            <textarea rows="3" class="form-control" name="company_addr"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!--<div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_tax_id', FALSE); ?></label>
                        <input type="text" class="form-control" name="tax_id"  >
                      </div>
                    </div>-->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_company_phone', FALSE); ?></label>
                                            <input type="text" class="form-control" name="company_phone">
                                        </div>
                                    </div>
                                    <!--div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_exclude_group', FALSE); ?></label>
                        <select class="form-control select2" name="exclude_group" style="width: 100%;" >
                          <option selected="selected" value="">-- Choose --</option>
                          <?php foreach ($exclude_group as $item): ?>
                            <option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div-->
                                </div>

                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_name', FALSE); ?></label>
                        <input type="text" class="form-control" name="shipment_name"  >
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_phone', FALSE); ?></label>
                        <input type="text" class="form-control" name="shipment_phone"  >
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_address', FALSE); ?></label>
                        <textarea rows="3" class="form-control" name="shipment_addr"  ></textarea>
                      </div>
                    </div>
                  </div>-->

                                <div class="row">
                                    <div class="col-6 discount_setting">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_discount_setting', FALSE); ?></label>
                                            <input type="number" class="form-control" name="discount_setting" min="0"
                                                   max="50">
                                        </div>
                                    </div>
                                    <div class="col-6 comission_setting">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_comission_setting', FALSE); ?></label>
                                            <input type="number" class="form-control" name="comission_setting" min="0"
                                                   max="50">
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_exclude_group', FALSE); ?></label>
                        <select class="form-control select2" name="exclude_group" style="width: 100%;" >
                          <option selected="selected" value="">-- Choose --</option>
                          <?php foreach ($exclude_group as $item): ?>
                            <option value="<?php echo $item->name; ?>"><?php echo $item->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="col">

                    </div>
                  </div>-->
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div id="modal-edit" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Account Setting</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_edit" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/account_setting/edit"); ?>">
                            <input type="hidden" id="edit_id" name="edit_id"/>
                            <input type="hidden" id="old_country" name="old_country"/>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_email', FALSE); ?></label>
                                            <input type="text" class="form-control" name="email" readonly>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_password', FALSE); ?></label>
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="Input only change to new password">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_country', FALSE); ?></label>
                                            <select class="form-control select2" name="country" style="width: 100%;"
                                                    disabled>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($nation_lang as $item): ?>
                                                    <option value="<?php echo $item->country; ?>"><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_account_type', FALSE); ?></label>
                                            <select class="form-control select2" name="account_type"
                                                    style="width: 100%;" disabled>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($groups_list as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row parent_agent">
                                    <div class="col">
                                        <div class="form-group">
                                            <?php
                                            $disabled = '';
                                            if ($this->ion_auth->in_group(array('admin'))) {
                                                $disabled = 'disabled';
                                            }
                                            ?>
                                            <label>Parent Agent</label>
                                            <select class="form-control select2" name="parent_agent"
                                                    style="width: 100%;" <?= $disabled ?>>
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($user_aa as $item): ?>
                                                    <option value="<?php echo $item->id; ?>"><?php echo $item->email; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_id', FALSE); ?></label>
                                            <input type="text" class="form-control" name="user_id" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_company_name', FALSE); ?></label>
                                            <input type="text" class="form-control" name="company_name">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_contact_person', FALSE); ?></label>
                                            <input type="text" class="form-control" name="contact_person">
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_zipcode', FALSE); ?></label>
                                            <input type="text" class="form-control" name="zipcode">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label style="width: 100%;"><?php echo $this->lang->line('admin_account_setting_company_address', FALSE); ?></label>
                                            <textarea rows="3" class="form-control" name="company_addr"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <!--<div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_tax_id', FALSE); ?></label>
                        <input type="text" class="form-control" name="tax_id"  >
                      </div>
                    </div>-->
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_company_phone', FALSE); ?></label>
                                            <input type="text" class="form-control" name="company_phone">
                                        </div>
                                    </div>
                                    <!--div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_exclude_group', FALSE); ?></label>
                        <select class="form-control select2" name="exclude_group" style="width: 100%;" >
                          <option selected="selected" value="">-- Choose --</option>
                          <?php foreach ($exclude_group as $item): ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div-->
                                </div>

                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_name', FALSE); ?></label>
                        <input type="text" class="form-control" name="shipment_name"  >
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_phone', FALSE); ?></label>
                        <input type="text" class="form-control" name="shipment_phone"  >
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_shipment_address', FALSE); ?></label>
                        <textarea rows="3" class="form-control" name="shipment_addr"  ></textarea>
                      </div>
                    </div>
                  </div>-->

                                <div class="row">
                                    <div class="col-6 discount_setting">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_discount_setting', FALSE); ?></label>
                                            <input type="number" class="form-control" name="discount_setting" min="0"
                                                   max="50" <?php echo ($this->ion_auth->in_group(array('admin','SA'))) ? '' : 'disabled' ?>>
                                        </div>
                                    </div>
                                    <div class="col-6 comission_setting">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_comission_setting', FALSE); ?></label>
                                            <input type="number" class="form-control" name="comission_setting" min="0"
                                                   max="50">
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label><?php echo $this->lang->line('admin_account_setting_exclude_group', FALSE); ?></label>
                        <select class="form-control select2" name="exclude_group" style="width: 100%;" >
                          <option selected="selected" value="">-- Choose --</option>
                          <?php foreach ($exclude_group as $item): ?>
                            <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="col">

                    </div>
                  </div>-->
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

</section>


<!-- DataTables -->
<link rel="stylesheet"
      href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>

<script>
    $('#modal-add').on('shown.bs.modal', function (e) {
        $('.parent_agent').css('display', 'none');
    });

    function edit_item(t) {
        $('#modal-edit input[name="old_country"]').val($(t).data('country'));
        // $('#modal-edit select[name="country"]').select2("val", $(t).data('country'));
        // $('#modal-edit select[name="account_type"]').select2("val", $(t).data('account_type'));
        // $('#modal-edit select[name="parent_agent"]').select2("val", $(t).data('parent_agent'));
        // $('#modal-edit select[name="exclude_group"]').select2("val", $(t).data('exclude_group'));

        $('#modal-edit select[name="country"]').val($(t).data('country'));
        $('#modal-edit select[name="account_type"]').val($(t).data('account_type'));
        $('#modal-edit input[name="user_id"]').val($(t).data('user_id'));

        $('#modal-edit select[name="exclude_group"]').val($(t).data('exclude_group'));

        $('#modal-edit textarea[name="company_addr"]').val($(t).data('company_addr'));
        $('#modal-edit textarea[name="shipment_addr"]').val($(t).data('shipment_addr'));

        $('#modal-edit input[name="email"]').val($(t).data('email'));
        $('#modal-edit input[name="company_name"]').val($(t).data('company_name'));
        $('#modal-edit input[name="company_phone"]').val($(t).data('company_phone'));
        $('#modal-edit input[name="tax_id"]').val($(t).data('tax_id'));
        $('#modal-edit input[name="contact_person"]').val($(t).data('contact_person'));
        $('#modal-edit input[name="shipment_name"]').val($(t).data('shipment_name'));
        $('#modal-edit input[name="shipment_phone"]').val($(t).data('shipment_phone'));
        $('#modal-edit input[name="zipcode"]').val($(t).data('zipcode'));
        var discount_setting_input = $('#modal-edit input[name="discount_setting"]').val($(t).data('discount_setting'));
        $('#modal-edit input[name="comission_setting"]').val($(t).data('comission_setting'));

        $('.comission_setting, .discount_setting, .parent_agent').css('display', 'none');

        if ($('#modal-edit select[name="account_type"] option:selected').text() == 'AA') {
            $('.comission_setting').css('display', 'inline-block');
        } else {
            $('.discount_setting').css('display', 'inline-block');
        }

        if ($('#modal-edit select[name="account_type"] option:selected').text() == 'FC') {
            $('.parent_agent').css('display', 'inline-block');
            genAA($(t).data('parent_agent'));
        }

        //zun
        var auth_sa = <?php echo ($this->ion_auth->in_group(array('SA'))) ? 1 : 0; ?>;
        discount_setting_input.prop("disabled", false);
        if(auth_sa) {
            if ($(t).data('account_type') == 3) {
                discount_setting_input.prop("disabled", true);
            }
        }

        $('#edit_id').val($(t).data('id'));
        $('#modal-edit').modal();
    }

    function delete_item(t) {
        Swal.fire({
            title: 'Are you sure to delete <br /> "' + $(t).data('email') + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?=base_url("sensha-admin/account_setting/delete/")?>' + $(t).data('id'),
                })
                    .done(function (msg) {
                        // r = JSON.parse(msg);
                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your data has been deleted.',
                            type: 'success',
                            showConfirmButton: false,
                        })
                        setTimeout(function () {
                            window.location = '';
                        }, 1400);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    function genAA(select) {
        var country = $('select[name="country"]').val();

        $.get("<?php echo base_url("sensha-admin/account_setting/getUserType/AA");?>", function (data) {
            var r = JSON.parse(data);
            $('select[name="parent_agent"] option').remove();

            var o = new Option("-- Choose --", "");
            $(o).html("-- Choose --");
            $('select[name="parent_agent"]').append(o);

            for (i = 0; i < r.length; i++) {
                var o = new Option(r[i].email, r[i].id);
                $(o).html(r[i].email);
                $('select[name="parent_agent"]').append(o);
            }
            if (select != '') {
                $('#modal-edit select[name="parent_agent"]').val(select);
            }
        });
    }

    $(function () {
        /*$('#list_table').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": true,
          "ordering": false,
          "info": true,
          "autoWidth": false,
          "pageLength": 30
        });*/
        fill_datatable();

        function fill_datatable(filter_country = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/account_setting/data_index" ?>`;
            var dataList = $('#list_table');
            dataList.DataTable({
                serverSide: true,
                processing:true,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data: {
                        filter_country: filter_country
                    },
                },
                order: [[0, "asc"]],
                pageLength: 30,
                lengthMenu: [ 30 , 50, 75, 100 ],
                columns: [
                    {data: "DT_RowId", orderable: false},
                    {data: "email", orderable: false},
                    {data: "id", orderable: false},
                    {data: "country", orderable: false},
                    {data: "name", orderable: false},
                    {data: "company_name", orderable: false},
                    {data: "company_phone", orderable: false},
                    {data: "discount_setting", orderable: false},
                    {data: "comission_setting", orderable: false},
                    {data: "action", orderable: false}
                ]
            });
        }

        $(document).ready(function () {
            $('#filter').click(function () {
                var filter_country = $('#filter_country').val();
                if (filter_country != '') {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country);
                } else {
                    $('#list_table').DataTable().destroy();
                    fill_datatable();
                }
            });
        });

        // $('.select2').select2();
        $('select[name="account_type"]').on('change', function () {
            var data = $(this).find("option:selected").text();
            if (data == 'FC') {
                $('.parent_agent').css('display', 'inline-block');
                genAA('');
            } else {
                $('.parent_agent').hide();
            }

            $('.comission_setting, .discount_setting').css('display', 'none');

            if (data == 'AA') {
                $('.comission_setting').css('display', 'inline-block');
            } else {
                $('.discount_setting').css('display', 'inline-block');
            }
        })

        $('#form_add, #form_edit').submit(function () {
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize()
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        Swal.fire({
                            type: 'success',
                            title: 'success',
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1400);
                    } else if (r.error == 3) {
                        Swal.fire(
                            'Oops...',
                            'SA account is duplicate in country',
                            'warning'
                        );
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });
    });

    $('#form_edit').each(function () {

    });
</script>
