<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_financial_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_financial_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <form method="post" action="<?php echo base_url("sensha-admin/financial_manager");?>">
          <div class="form-group">
            <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
            <select name="country">
              <option value="">--- Choose Country ---</option>
              <?php foreach($nation_lang as $item):?>
                <?php if($item->country != 'Global'):?>
                  <option value="<?php echo $item->country;?>" <?php echo ($this->input->post('country') == $item->country)?'selected':'';?>><?php echo $item->country;?></option>
                <?php endif;?>
              <?php endforeach;?>
            </select>
            <select name="month">
              <option value="">--- Month ---</option>
              <?php for($i=1; $i<=12; $i++):?>
                <option value="<?php echo $i;?>" <?php echo ($this->input->post('month') == $i)?'selected':'';?>><?php echo date('F', strtotime("2019-$i"));?></option>
              <?php endfor;?>
            </select>
            <button type="submit" class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <!-- <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> Add</button> -->
          <!-- <a href="<?php echo base_url("sensha-admin/financial_manager/export")?>" class="btn btn-info"> Export</a> -->
          <?php if($this->ion_auth->in_group(array('admin'))):?>
            <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-pay-credit"><?php echo $this->lang->line('admin_financial_pay_credit', FALSE); ?></button>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills ml-left">
          <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"><?php echo $this->lang->line('admin_financial_tab1', FALSE); ?></a></li>
          <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"><?php echo $this->lang->line('admin_financial_tab2', FALSE); ?> </a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_1">
            <div class="card card-primary">
              <!-- <div class="card-header">
              <h3 class="card-title">Financial Manager</h3>
            </div> -->
            <div class="card-body table-responsive">
              <table class="table table-bordered table-hover no-margin">
                <thead>
                  <tr>
                    <th colspan="2" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_basic_info', FALSE); ?></th>
                    <th class="text-center"><?php echo $this->lang->line('admin_financial_tab1_saller_info', FALSE); ?></th>
                    <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_agent_info', FALSE); ?></th>
                    <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_purchase_info', FALSE); ?></th>
                    <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_sales_info', FALSE); ?></th>
                    <th colspan="3" class="text-center">Total</th>
                  </tr>
                  <tr>
                    <th><?php echo $this->lang->line('admin_financial_tab1_shipment_id', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_date_of_payment', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_country', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_sa_name', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_sa_discount_ratio', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_aa_name', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_aa_discount_ratio', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_country2', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_user_type', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_user_id', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_discount_ratio', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_no_of_item', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_sales_amount', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_shipping_fee', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_discount_total', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_sales_total', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_pay_to_agent', FALSE); ?></th>
                    <th><?php echo $this->lang->line('admin_financial_tab1_expected_income', FALSE); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum_item = 0;
                  $sum_sale_amount = 0;
                  $sum_shiping_fee = 0;
                  $sum_discount = 0;
                  $sum_sale_total = 0;
                  $sum_pay_to_agent = 0;
                  $sum_income_total = 0;

                  $sa_sum_item = 0;
                  $sa_sum_sale_amount = 0;
                  $sa_sum_shiping_fee = 0;
                  $sa_sum_discount = 0;
                  $sa_sum_sale_total = 0;
                  $sa_sum_pay_to_agent = 0;
                  $sa_sum_income_total = 0;

                  $credit=array();
                  ?>
                  <?php if($this->ion_auth->in_group(array('admin'))):?>
                    <?php foreach($sales_history as $item):
                          $purchaser = $this->ion_auth->user($item->purchaser_id)->row();
                          $user_groups = $this->ion_auth->get_users_groups($purchaser->id)->row();
                          ?>
                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'SA'):?>
                        <?php
                        $sale_amount = $item->item_sub_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $pay_to_agent = -0;
                        $income_total = $sale_total+$pay_to_agent;

                        $sum_item += $item->item_amount;
                        $sum_sale_amount += $sale_amount;
                        $sum_shiping_fee += $shipping_fee;
                        $sum_discount += $discount;
                        $sum_sale_total += $sale_total;
                        $sum_pay_to_agent += $pay_to_agent;
                        $sum_income_total += $income_total;
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sale_amount);?></td>
                          <td><?php echo number_format($shipping_fee);?></td>
                          <td><?php echo number_format($discount);?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <td><?php echo number_format($pay_to_agent);?></td>
                          <td><?php echo number_format($income_total);?></td>
                        </tr>
                      <?php endif;?>

                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();
                          $sa = $this->ion_auth->user($aa->create_by)->row();

                          $sa_discount = $sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting;
                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $pay_to_agent = -(($sa_discount + $aa->comission_setting)/100)*$sale_amount;
                          $income_total = $sale_total+$pay_to_agent;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;

                          if(!isset($credit[$purchaser->country])){
                            $credit[$purchaser->country] = array();
                          }
                          array_push($credit[$purchaser->country], $pay_to_agent);
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?>dfdf</td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                            <td><?php echo $sa->email;?></td>
                            <td><?php echo $sa_discount;?></td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php else:?>
                          <?php
                          $sa = $this->ion_auth->user($fc->create_by)->row();

                          $sa_discount = $sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting;
                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $pay_to_agent = -(($sa_discount + $aa->comission_setting)/100)*$sale_amount;
                          $income_total = $sale_total+$pay_to_agent;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;

                          if(!isset($credit[$purchaser->country])){
                            $credit[$purchaser->country] = array();
                          }
                          array_push($credit[$purchaser->country], $pay_to_agent);
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                            <td><?php echo $sa->email;?></td>
                            <td><?php echo $sa->discount_setting-$purchaser->discount_setting;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'GU' && $item->discount_type == 'coupon'):?>
                        <?php $sa_id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($sa_id)->row(); ?>
                        <?php
                        $sale_amount = $item->item_sub_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $pay_to_agent = -0;
                        $income_total = $sale_total+$pay_to_agent;

                        $sum_item += $item->item_amount;
                        $sum_sale_amount += $sale_amount;
                        $sum_shiping_fee += $shipping_fee;
                        $sum_discount += $discount;
                        $sum_sale_total += $sale_total;
                        $sum_pay_to_agent += $pay_to_agent;
                        $sum_income_total += $income_total;
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sale_amount);?></td>
                          <td><?php echo number_format($shipping_fee);?></td>
                          <td><?php echo number_format($discount);?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <td><?php echo number_format($pay_to_agent);?></td>
                          <td><?php echo number_format($income_total);?></td>
                        </tr>

                      <?php elseif($item->account_type == 'admin' && $item->purchaser_type == 'GU'):?>
                        <?php $sa_id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($sa_id)->row(); ?>
                        <?php
                        $sale_amount = $item->item_sub_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -$item->discount_amount;
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $pay_to_agent = -0;
                        $income_total = $sale_total+$pay_to_agent;

                        $sum_item += $item->item_amount;
                        $sum_sale_amount += $sale_amount;
                        $sum_shiping_fee += $shipping_fee;
                        $sum_discount += $discount;
                        $sum_sale_total += $sale_total;
                        $sum_pay_to_agent += $pay_to_agent;
                        $sum_income_total += $income_total;
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sale_amount);?></td>
                          <td><?php echo number_format($shipping_fee);?></td>
                          <td><?php echo number_format($discount);?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <td><?php echo number_format($pay_to_agent);?></td>
                          <td><?php echo number_format($income_total);?></td>
                        </tr>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();

                          $sa_sale_amount = $item->item_sub_total_amount;
                          $sa_shipping_fee = $item->delivery_amount;
                          $sa_discount = -($item->discount_amount);
                          $sa_sale_total = $sa_sale_amount+$sa_shipping_fee+$sa_discount;
                          $sa_pay_to_agent = -$sa_sale_total;
                          $sa_income_total = $sa_sale_total+$sa_pay_to_agent;

                          $sa_sum_item += $item->item_amount;
                          $sa_sum_sale_amount += $sa_sale_amount;
                          $sa_sum_shiping_fee += $sa_shipping_fee;
                          $sa_sum_discount += $sa_discount;
                          $sa_sum_sale_total += $sa_sale_total;
                          $sa_sum_pay_to_agent += $sa_pay_to_agent;
                          $sa_sum_income_total += $sa_income_total;

                          if(!isset($credit[$purchaser->country])){
                            $credit[$purchaser->country] = array();
                          }
                          array_push($credit[$purchaser->country], $sa_pay_to_agent);

                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo $item->country;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sa_sale_amount);?></td>
                            <td><?php echo number_format($sa_shipping_fee);?></td>
                            <td><?php echo number_format($sa_discount);?></td>
                            <td><?php echo number_format($sa_sale_total);?></td>
                            <td><?php echo number_format($sa_pay_to_agent);?></td>
                            <td><?php echo number_format($sa_income_total);?></td>
                          </tr>
                        <?php else:?>
                          <?php
                          $sa = $this->ion_auth->user($fc->create_by)->row();

                          $sa_sale_amount = $item->item_sub_total_amount;
                          $sa_shipping_fee = $item->delivery_amount;
                          $sa_discount = -($item->discount_amount);
                          $sa_sale_total = $sa_sale_amount+$sa_shipping_fee+$sa_discount;
                          $sa_pay_to_agent = -$sa_sale_total;
                          $sa_income_total = $sa_sale_total+$sa_pay_to_agent;

                          $sa_sum_item += $item->item_amount;
                          $sa_sum_sale_amount += $sa_sale_amount;
                          $sa_sum_shiping_fee += $sa_shipping_fee;
                          $sa_sum_discount += $sa_discount;
                          $sa_sum_sale_total += $sa_sale_total;
                          $sa_sum_pay_to_agent += $sa_pay_to_agent;
                          $sa_sum_income_total += $sa_income_total;

                          if(!isset($credit[$purchaser->country])){
                            $credit[$purchaser->country] = array();
                          }
                          array_push($credit[$purchaser->country], $sa_sum_pay_to_agent);
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo $item->country;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sa_sale_amount);?></td>
                            <td><?php echo number_format($sa_shipping_fee);?></td>
                            <td><?php echo number_format($sa_discount);?></td>
                            <td><?php echo number_format($sa_sale_total);?></td>
                            <td><?php echo number_format($sa_pay_to_agent);?></td>
                            <td><?php echo number_format($sa_income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'GU' && $item->discount_type != ''):?>
                        <?php $sa_id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($sa_id)->row(); ?>
                        <?php
                        $sa_sale_amount = $item->item_sub_total_amount;
                        $sa_shipping_fee = $item->delivery_amount;
                        $sa_discount = -($item->discount_amount);
                        $sa_sale_total = $sa_sale_amount+$sa_shipping_fee+$sa_discount;
                        $sa_pay_to_agent = -$sa_sale_total;
                        $sa_income_total = $sa_sale_total+$sa_pay_to_agent;

                        $sa_sum_item += $item->item_amount;
                        $sa_sum_sale_amount += $sa_sale_amount;
                        $sa_sum_shiping_fee += $sa_shipping_fee;
                        $sa_sum_discount += $sa_discount;
                        $sa_sum_sale_total += $sa_sale_total;
                        $sa_sum_pay_to_agent += $sa_pay_to_agent;
                        $sa_sum_income_total += $sa_income_total;

                        if(!isset($credit[$purchaser->country])){
                          $credit[$purchaser->country] = array();
                        }
                        array_push($credit[$purchaser->country], $sa_sum_pay_to_agent);
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo $item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sa_sale_amount);?></td>
                          <td><?php echo number_format($sa_shipping_fee);?></td>
                          <td><?php echo number_format($sa_discount);?></td>
                          <td><?php echo number_format($sa_sale_total);?></td>
                          <td><?php echo number_format($sa_pay_to_agent);?></td>
                          <td><?php echo number_format($sa_income_total);?></td>
                        </tr>

                      <?php elseif($item->account_type == 'SA' && $item->purchaser_type == 'GU'):?>
                        <?php $sa_id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($sa_id)->row(); ?>
                        <?php
                        $sa_sale_amount = $item->item_sub_total_amount;
                        $sa_shipping_fee = $item->delivery_amount;
                        $sa_discount = -($item->discount_amount);
                        $sa_sale_total = $sa_sale_amount+$sa_shipping_fee+$sa_discount;
                        $sa_pay_to_agent = -$sa_sale_total;
                        $sa_income_total = $sa_sale_total+$sa_pay_to_agent;

                        $sa_sum_item += $item->item_amount;
                        $sa_sum_sale_amount += $sa_sale_amount;
                        $sa_sum_shiping_fee += $sa_shipping_fee;
                        $sa_sum_discount += $sa_discount;
                        $sa_sum_sale_total += $sa_sale_total;
                        $sa_sum_pay_to_agent += $sa_pay_to_agent;
                        $sa_sum_income_total += $sa_income_total;

                        if(!isset($credit[$purchaser->country])){
                          $credit[$purchaser->country] = array();
                        }
                        array_push($credit[$purchaser->country], $sa_sum_pay_to_agent);
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo $item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sa_sale_amount);?></td>
                          <td><?php echo number_format($sa_shipping_fee);?></td>
                          <td><?php echo number_format($sa_discount);?></td>
                          <td><?php echo number_format($sa_sale_total);?></td>
                          <td><?php echo number_format($sa_pay_to_agent);?></td>
                          <td><?php echo number_format($sa_income_total);?></td>
                        </tr>
                      <?php endif;?>
                    <?php endforeach;?>
                    <!-- <tr>
                      <td colspan="11"></td>
                      <td><?php echo number_format($sum_item);?></td>
                      <td><?php echo number_format($sum_sale_amount);?></td>
                      <td><?php echo number_format($sum_shiping_fee);?></td>
                      <td><?php echo number_format($sum_discount);?></td>
                      <td><?php echo number_format($sum_sale_total);?></td>
                      <td><?php echo number_format($sum_pay_to_agent);?></td>
                      <td><?php echo number_format($sum_income_total);?></td>
                    </tr>
                    <tr>
                      <td colspan="11"></td>
                      <td><?php echo number_format($sa_sum_item);?></td>
                      <td><?php echo number_format($sa_sum_sale_amount);?></td>
                      <td><?php echo number_format($sa_sum_shiping_fee);?></td>
                      <td><?php echo number_format($sa_sum_discount);?></td>
                      <td><?php echo number_format($sa_sum_sale_total);?></td>
                      <td><?php echo number_format($sa_sum_pay_to_agent);?></td>
                      <td></td>
                    </tr> -->
                  <?php endif;?>

                  <?php if($this->ion_auth->in_group(array('SA'))):?>
                    <?php foreach($sales_history as $item): $purchaser = $this->ion_auth->user($item->purchaser_id)->row(); $user_groups = $this->ion_auth->get_users_groups($purchaser->id)->row();?>
                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();
                          $sa = $this->ion_auth->user($aa->create_by)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          // $sale_total = ((($sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting) + $aa->comission_setting)/100)*$sale_amount;
                          $sale_total = (((($sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting)+ $aa->comission_setting)/100) * $sale_amount);
                          $pay_to_agent = (($aa->comission_setting/100)*$sale_amount);
                          $income_total = (($sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting)/100)*$sale_amount;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                            <td><?php echo $sa->email;?></td>
                            <td><?php echo $sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting;?></td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format(((($sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting)+$aa->comission_setting)/100)*$sale_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php else:?>
                          <?php
                          $sa = $this->ion_auth->user($fc->create_by)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = (($sa->discount_setting-$purchaser->discount_setting)/100)*$sale_amount;
                          $pay_to_agent = 0;
                          $income_total = (($purchaser->discount_setting)/100)*$sale_amount;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                            <td><?php echo $sa->email;?></td>
                            <td><?php echo $sa->discount_setting-$purchaser->discount_setting;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format(($sa->discount_setting/100)*$sale_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $pay_to_agent = ($purchaser->discount_setting/100)*$sale_amount;
                          $income_total = $sale_total-$pay_to_agent;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo $item->country;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php else:?>
                          <?php
                          $sa = $this->ion_auth->user($fc->create_by)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $pay_to_agent = 0;
                          $income_total = $sale_total;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo $item->country;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'GU' && $item->discount_type == 'coupon'):?>
                        <?php $id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($id)->row(); ?>
                        <?php
                        $sale_amount = $item->item_sub_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $pay_to_agent = 0;
                        $income_total = $sale_total+$pay_to_agent;

                        $sum_item += $item->item_amount;
                        $sum_sale_amount += $sale_amount;
                        $sum_shiping_fee += $shipping_fee;
                        $sum_discount += $discount;
                        $sum_sale_total += $sale_total;
                        $sum_pay_to_agent += $pay_to_agent;
                        $sum_income_total += $income_total;
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo $item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sale_amount);?></td>
                          <td><?php echo number_format($shipping_fee);?></td>
                          <td><?php echo number_format($discount);?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <td><?php echo number_format($pay_to_agent);?></td>
                          <td><?php echo number_format($income_total);?></td>
                        </tr>


                      <?php elseif($item->account_type == 'SA' && $item->purchaser_type == 'GU'):?>
                        <?php $id = $this->ion_auth->user($purchaser->id)->row()->create_by; ?>
                        <?php $sa = $this->ion_auth->user($id)->row(); ?>
                        <?php
                        $sale_amount = $item->item_sub_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $pay_to_agent = 0;
                        $income_total = $sale_total+$pay_to_agent;

                        $sum_item += $item->item_amount;
                        $sum_sale_amount += $sale_amount;
                        $sum_shiping_fee += $shipping_fee;
                        $sum_discount += $discount;
                        $sum_sale_total += $sale_total;
                        $sum_pay_to_agent += $pay_to_agent;
                        $sum_income_total += $income_total;
                        ?>
                        <tr>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->create_date;?></td>
                          <td><?php echo $item->country;?></td>
                          <td>-</td>
                          <td>0</td>
                          <td>-</td>
                          <td>0</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td><?php echo $user_groups->name;?></td>
                          <td><?php echo $purchaser->id;?></td>
                          <td><?php echo $purchaser->discount_setting;?></td>
                          <td><?php echo $item->item_amount;?></td>
                          <td><?php echo number_format($sale_amount);?></td>
                          <td><?php echo number_format($shipping_fee);?></td>
                          <td><?php echo number_format($discount);?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <td><?php echo number_format($pay_to_agent);?></td>
                          <td><?php echo number_format($income_total);?></td>
                        </tr>
                      <?php endif;?>

                    <?php endforeach;?>
                    <!-- <tr>
                      <td colspan="15"></td>
                      <td><?php echo number_format($sum_sale_total);?></td>
                      <td><?php echo number_format($sum_pay_to_agent);?></td>
                      <td></td>
                    </tr> -->
                  <?php endif;?>

                  <?php if($this->ion_auth->in_group(array('AA'))):?>
                    <?php foreach($sales_history as $item): $purchaser = $this->ion_auth->user($item->purchaser_id)->row(); $user_groups = $this->ion_auth->get_users_groups($purchaser->id)->row();?>
                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();
                          $sa = $this->ion_auth->user($aa->create_by)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = ($aa->comission_setting/100)*$sale_amount;
                          $pay_to_agent = 0;
                          $income_total = ($aa->comission_setting/100)*$sale_amount;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo ($item->account_type == 'admin')?'Global':$item->country;?></td>
                            <td><?php echo $sa->email;?></td>
                            <td><?php echo $sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting;?></td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($income_total);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'FC'):?>
                        <?php $fc = $this->ion_auth->user($purchaser->id)->row(); ?>
                        <?php if($fc->parent_agent > 0):?>
                          <?php
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();

                          $sale_amount = $item->item_sub_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $pay_to_agent = 0;
                          $income_total = ($aa->comission_setting/100)*$sale_amount;

                          $sum_item += $item->item_amount;
                          $sum_sale_amount += $sale_amount;
                          $sum_shiping_fee += $shipping_fee;
                          $sum_discount += $discount;
                          $sum_sale_total += $sale_total;
                          $sum_pay_to_agent += $pay_to_agent;
                          $sum_income_total += $income_total;
                          ?>
                          <tr>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->create_date;?></td>
                            <td><?php echo $item->country;?></td>
                            <td>-</td>
                            <td>0</td>
                            <td><?php echo $aa->email;?></td>
                            <td><?php echo $aa->comission_setting;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td><?php echo $user_groups->name;?></td>
                            <td><?php echo $purchaser->id;?></td>
                            <td><?php echo $purchaser->discount_setting;?></td>
                            <td><?php echo $item->item_amount;?></td>
                            <td><?php echo number_format($sale_amount);?></td>
                            <td><?php echo number_format($shipping_fee);?></td>
                            <td><?php echo number_format($discount);?></td>
                            <td><?php echo number_format($income_total);?></td>
                            <td><?php echo number_format($pay_to_agent);?></td>
                            <td><?php echo number_format($income_total);?></td>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                    <?php endforeach;?>
                    <!-- <tr>
                      <td colspan="15"></td>
                      <td><?php echo $sum_sale_total;?></td>
                      <td></td>
                      <td></td>
                    </tr> -->
                  <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_2">
          <div class="card card-primary">
            <!-- <div class="card-header">
            <h3 class="card-title">Financial Manager</h3>
          </div> -->
          <div class="card-body table-responsive">
            <table class="table table-bordered table-hover no-margin">
              <thead>
                <tr>
                  <th colspan="3" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_basic_info', FALSE); ?></th>
                  <th colspan="2" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_payer_info', FALSE); ?></th>
                  <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_detailed_info', FALSE); ?></th>
                </tr>
                <tr>
                  <th><?php echo $this->lang->line('admin_financial_tab2_date_of_payment', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_pay_type', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_shipment_id', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_account_type', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_sa_country', FALSE); ?></th>
                  <!-- <th><?php echo $this->lang->line('admin_financial_tab2_sa_name', FALSE); ?></th> -->
                  <!-- <th><?php echo $this->lang->line('admin_financial_tab2_credit_before_process', FALSE); ?></th> -->
                  <th><?php echo $this->lang->line('admin_financial_tab2_description', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_amount_to_pay', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_financial_tab2_credit_after_process', FALSE); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $credit_paid = array();
                ?>
                <?php if($this->ion_auth->in_group(array('admin'))):?>
                  <?php
                  $credit_before_process = 0;
                  $credit_after_process = 0;
                  $credit_balance = 0;
                  $credit_before_process = array_sum($credit[$item->country]);
                  $credit_before_process = str_replace('-', '', $credit_before_process);

                  ?>

                  <?php foreach($sales_history as $item):?>
                    <?php if($item->payment_method == 'release'):?>
                      <?php
                      $credit_after_process = $credit_before_process-$item->sale_total_amount;

                      if(!isset($credit_paid[$item->country])){
                        $credit_paid[$item->country] = array();
                      }
                      array_push($credit_paid[$item->country], $item->sale_total_amount);

                      ?>
                      <tr>
                        <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                        <td>Release</td>
                        <td>-</td>
                        <td>Global</td>
                        <td>HQ</td>
                        <td><?php echo $item->country;?></td>
                        <!-- <td><?php echo $item->account_name;?></td> -->
                        <!-- <td><?php echo number_format($credit_before_process);?></td> -->
                        <td>Release payment on <?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                        <td><?php echo number_format($item->sale_total_amount);?></td>
                        <th><?php echo number_format($credit_after_process);?></th>
                      </tr>
                      <?php   $credit_before_process = $credit_after_process; endif;?>
                    <?php endforeach;?>
                  <?php endif;?>


                  <?php if($this->ion_auth->in_group(array('SA'))):?>
                    <?php
                    $sale_amount = 0;
                    $shipping_fee = 0;
                    $discount = 0;
                    $sale_total = 0;
                    $balance = 0;
                    ?>
                    <?php foreach($sales_history as $item):?>
                      <?php
                      $purchaser = $this->ion_auth->user($item->purchaser_id)->row();
                      $user_groups = $this->ion_auth->get_users_groups($purchaser->id)->row();
                      ?>
                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'SA'):?>
                        <?php
                        $sale_amount = $item->sale_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = -($sale_amount+$shipping_fee+$discount);
                        $balance = $sale_total+$balance;
                        ?>
                        <tr>
                          <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                          <td>Buy</td>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->country;?></td>
                          <td>HQ</td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <!-- <td><?php echo $sa->email;?></td> -->
                          <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                          <td>To buy <?php echo $item->item_amount;?> items</td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <th><?php echo number_format($balance);?></th>
                        </tr>
                      <?php endif;?>

                      <?php if($item->payment_method == 'release'):


                        $sale_total = -($item->sale_total_amount);
                        $income_total = $sale_total+$pay_to_agent;

                        $balance = $sale_total+$balance;

                        ?>
                        <tr>
                          <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                          <td>Release</td>
                          <td>-</td>
                          <td>Global</td>
                          <td>HQ</td>
                          <td><?php echo $item->country;?></td>
                          <!-- <td><?php echo $item->account_name;?></td> -->
                          <!-- <td><?php echo number_format($credit_before_process);?></td> -->
                          <td>Release payment on <?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <th><?php echo number_format($balance);?></th>
                        </tr>
                      <?php endif;?>

                      <?php if($item->account_type == 'admin' && $item->purchaser_type == 'FC'):
                        $fc = $this->ion_auth->user($purchaser->id)->row();
                        if($fc->parent_agent > 0):
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();
                          $sa = $this->ion_auth->user($aa->create_by)->row();

                          $sale_amount = $item->sale_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = (((($sa->discount_setting - $aa->comission_setting - $purchaser->discount_setting)+ $aa->comission_setting)/100) * $sale_amount);
                          $balance += $sale_total;
                          ?>
                          <tr>
                            <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                            <td>Sell</td>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td>FC</td>
                            <td><?php echo $item->country;?></td>
                            <!-- <td><?php echo $sa->email;?></td> -->
                            <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                            <td>Global sales <?php echo $item->item_amount;?> items</td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <th><?php echo number_format($balance);?></th>
                          </tr>
                        <?php else:
                          $sa = $this->ion_auth->user($fc->create_by)->row();
                          $sale_amount = $item->sale_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          // $sale_total = $sale_amount+$shipping_fee+$discount;
                          $sale_total = (($sa->discount_setting-$purchaser->discount_setting)/100)*$sale_amount;
                          $balance += $sale_total;
                          ?>
                          <tr>
                            <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                            <td>Sell</td>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td>FC</td>
                            <td><?php echo $item->country;?></td>
                            <!-- <td><?php echo $sa->email;?></td> -->
                            <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                            <td>Global sales <?php echo $item->item_amount;?> items</td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <th><?php echo number_format($balance);?></th>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'FC'):
                        $fc = $this->ion_auth->user($purchaser->id)->row();
                        if($fc->parent_agent > 0):
                          $aa = $this->ion_auth->user($fc->parent_agent)->row();

                          $sale_amount = $item->sale_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $balance += $sale_total;
                          ?>
                          <tr>
                            <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                            <td>Sell</td>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td>FC</td>
                            <td><?php echo $item->country;?></td>
                            <!-- <td><?php echo $sa->email;?></td> -->
                            <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                            <td>Domestic sales <?php echo $item->item_amount;?> items</td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <th><?php echo number_format($balance);?></th>
                          </tr>
                        <?php else:
                          $sa = $this->ion_auth->user($fc->create_by)->row();

                          $sale_amount = $item->sale_total_amount;
                          $shipping_fee = $item->delivery_amount;
                          $discount = -($item->discount_amount);
                          $sale_total = $sale_amount+$shipping_fee+$discount;
                          $balance += $sale_total;
                          ?>
                          <tr>
                            <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                            <td>Sell</td>
                            <td><?php echo $item->shipment_id;?></td>
                            <td><?php echo $item->purchaser_country;?></td>
                            <td>FC</td>
                            <td><?php echo $item->country;?></td>
                            <!-- <td><?php echo $sa->email;?></td> -->
                            <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                            <td>Domestic sales <?php echo $item->item_amount;?> items</td>
                            <td><?php echo number_format($item->sale_total_amount);?></td>
                            <th><?php echo number_format($balance);?></th>
                          </tr>
                        <?php endif;?>
                      <?php endif;?>

                      <?php if($item->account_type == 'SA' && $item->purchaser_type == 'GU'):
                        $sale_amount = $item->sale_total_amount;
                        $shipping_fee = $item->delivery_amount;
                        $discount = -($item->discount_amount);
                        $sale_total = $sale_amount+$shipping_fee+$discount;
                        $balance += $sale_total;
                        ?>
                        <tr>
                          <td><?php echo date('Y-m-d', strtotime($item->create_date));?></td>
                          <td>Sell</td>
                          <td><?php echo $item->shipment_id;?></td>
                          <td><?php echo $item->purchaser_country;?></td>
                          <td>GU</td>
                          <td><?php echo $item->country;?></td>
                          <!-- <td><?php echo $sa->email;?></td> -->
                          <!-- <td><?php echo number_format($global_sum_sale_total);?></td> -->
                          <td>Domestic sales <?php echo $item->item_amount;?> items</td>
                          <td><?php echo number_format($item->sale_total_amount);?></td>
                          <th><?php echo number_format($balance);?></th>
                        </tr>
                      <?php endif;?>

                    <?php endforeach;?>
                  <?php endif;?>
                  <tr>
                    <th colspan="7" class="text-right">Current credit</th>
                    <td><?php echo number_format($balance);?></td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
  ?>


</section>

<div id="modal-add" class="modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create Financial Manager</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/financial_manager/add");?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Month</label>
                  <select class="form-control select2" name="month" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <?php for($i=1; $i<=12; $i++):?>
                      <option value="<?php echo date('m', strtotime('2017-'.$i))?>"><?php echo date('m', strtotime('2017-'.$i))?></option>
                    <?php endfor;?>]
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Shop Type</label>
                  <select class="form-control select2" name="shop_type" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <option value="HQ">HQ</option>
                    <option value="SA">SA</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Agent Type</label>
                  <select class="form-control select2" name="agent_type" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <option value="none">None</option>
                    <option value="SA">SA</option>
                    <option value="AA">AA</option>
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Sales Amount</label>
                  <input type="text" class="form-control" name="sale_amount" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Shipment Amount</label>
                  <input type="text" class="form-control" name="shipment_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Discount Amount</label>
                  <input type="text" class="form-control" name="discount_amount" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Total Amount</label>
                  <input type="text" class="form-control" name="total_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>No. Of Transaction</label>
                  <input type="text" class="form-control" name="no_transaction" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Average Sales Amount</label>
                  <input type="text" class="form-control" name="avg_sale_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Amount To Pay</label>
                  <input type="text" class="form-control" name="amount_to_pay" >
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>

<div id="modal-edit" class="modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Financial Manager</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/financial_manager/edit");?>">
          <input type="hidden" id="edit_id" name="edit_id" />
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Month</label>
                  <select class="form-control select2" name="month" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <?php for($i=1; $i<=12; $i++):?>
                      <option value="<?php echo date('m', strtotime('2017-'.$i))?>"><?php echo date('m', strtotime('2017-'.$i))?></option>
                    <?php endfor;?>]
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Shop Type</label>
                  <select class="form-control select2" name="name" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <option value="HQ">HQ</option>
                    <option value="SA">SA</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Agent Type</label>
                  <select class="form-control select2" name="name" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <option value="none">None</option>
                    <option value="SA">SA</option>
                    <option value="AA">AA</option>
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Sales Amount</label>
                  <input type="text" class="form-control" name="sale_amount" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Shipment Amount</label>
                  <input type="text" class="form-control" name="shipment_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Discount Amount</label>
                  <input type="text" class="form-control" name="discount_amount" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Total Amount</label>
                  <input type="text" class="form-control" name="total_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>No. Of Transaction</label>
                  <input type="text" class="form-control" name="no_transaction" >
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label>Average Sales Amount</label>
                  <input type="text" class="form-control" name="avg_sale_amount" >
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Amount To Pay</label>
                  <input type="text" class="form-control" name="amount_to_pay" >
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div id="modal-pay-credit" class="modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo $this->lang->line('admin_financial_pay_credit', FALSE); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form role="form" id="form_pay_credit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/financial_manager/pay_credit");?>">
          <div class="card-body">
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_financial_country', FALSE); ?></label>
                  <select class="form-control select2" name="country" style="width: 100%;" >
                    <option selected="selected" value="">-- Choose --</option>
                    <?php foreach($nation_lang as $item):?>
                      <?php if($item->country != 'Global'):?>
                        <option value="<?php echo $item->country;?>"><?php echo $item->country;?></option>
                      <?php endif;?>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="col">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_financial_credit', FALSE); ?></label>
                  <input type="text" class="form-control" name="credit" readonly />
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label><?php echo $this->lang->line('admin_financial_amount_to_release', FALSE); ?></label>
                  <input type="text" class="form-control" name="amount_of_release" />
                </div>
              </div>
              <div class="col">

              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('admin_financial_submit', FALSE); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php foreach($credit as $key => $item):?>
  <div id="c-<?php echo str_replace(' ', '-', $key);?>" data-val="<?php echo str_replace('-', '', array_sum($item)) - array_sum($credit_paid[$key]);?>"></div>
<?php endforeach;?>


<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
<script src="<?=base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js")?>"></script>

<script>
function edit_item(t){
  $('#modal-edit select[name="month"]').select2("val", $(t).data('month'));
  $('#modal-edit input[name="font"]').val($(t).data('font'));
  // $('#modal-edit #description').val($(t).data('des'));

  $('#edit_id').val($(t).data('id'));
  $('#modal-edit').modal();
}

function delete_item(t){
  Swal.fire({
    title: 'Are you sure to delete <br /> "'+$(t).data('firstname')+' '+$(t).data('lastname')+'"?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        method: "POST",
        url: '<?=base_url("sensha-admin/financial_manager/delete/")?>'+$(t).data('id'),
      })
      .done(function( msg ) {
        // r = JSON.parse(msg);
        Swal.fire({
          title: 'Deleted!',
          text: 'Your data has been deleted.',
          type: 'success',
          showConfirmButton: false,
        })
        setTimeout(function(){ window.location = ''; }, 1400);
      })
      .fail(function( msg ) {
        Swal.fire({
          type: 'error',
          // text: msg.responseText,
          title: msg.statusText,
          // showConfirmButton: false
        });
      });
    }
  });
}

$(function(){
  $('#list_table').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": true,
    "ordering": false,
    "info": true,
    "autoWidth": false,
    "pageLength": 30
  });

  // $('.select2').select2();

  $('#modal-pay-credit select[name="country"]').change(function(){
    var data = '<?php $credit;?>'
    var country = $(this).val().replace(' ', '-');
    $('#modal-pay-credit input[name="credit"]').val($('#c-'+country).data('val'));
  });

  $('#form_pay_credit').submit(function(){
    $.ajax({
      method: "POST",
      url: $(this).attr('action'),
      data: $( this ).serialize()
    })
    .done(function( msg ) {
      Swal.fire({
        type: 'success',
        title: 'success',
        showConfirmButton: false,
        allowOutsideClick:false
      });
      setTimeout(function(){ location.reload(); }, 1400);
      // // console.log(msg);
      // r = JSON.parse(msg);
      // // console.log(r);
      // if(r.error == 0){
      //   Swal.fire({
      //     type: 'success',
      //     title: 'success',
      //     showConfirmButton: false,
      //     allowOutsideClick:false
      //   });
      //   setTimeout(function(){ location.reload(); }, 1400);
      // }
      // else{
      //   Swal.fire(
      //     'Oops...',
      //     'Something went wrong!',
      //     'error'
      //   );
      // }
    })
    .fail(function( msg ) {
      Swal.fire(
        'Oops...',
        msg,
        'error'
      );
    });
    return false;
  });

  $('#form_add, #form_edit').submit(function(){
    $.ajax({
      method: "POST",
      url: $(this).attr('action'),
      data: $( this ).serialize()
    })
    .done(function( msg ) {
      // console.log(msg);
      r = JSON.parse(msg);
      // console.log(r);
      if(r.error == 0){
        Swal.fire({
          type: 'success',
          title: 'success',
          showConfirmButton: false,
          allowOutsideClick:false
        });
        setTimeout(function(){ location.reload(); }, 1400);
      }
      else{
        Swal.fire(
          'Oops...',
          'Something went wrong!',
          'error'
        );
      }
    })
    .fail(function( msg ) {
      Swal.fire(
        'Oops...',
        msg,
        'error'
      );
    });
    return false;
  });
});
</script>
