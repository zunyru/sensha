<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_financial_title', false); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_financial_title', false); ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <!--        <form method="post" action="-->
                <?php //echo base_url("sensha-admin/financial_manager"); ?><!--">-->
                <div class="form-group">
                    <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
                        <label><?php echo $this->lang->line('global_filter', false); ?></label>
                        <select name="country" id="filter_country">
                            <option value="">--- Choose Country ---</option>
                            <?php foreach ($nation_lang as $item) : ?>

                                <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>

                            <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                    <select name="month" id="filter_month">
                        <option value="">--- Month ---</option>
                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                            <option value="<?php echo $i; ?>" <?php echo ($this->input->post('month') == $i) ? 'selected' : ''; ?>><?php echo date('F', strtotime("2019-$i")); ?></option>
                        <?php endfor; ?>
                    </select>

                    <select name="year" id="filter_year">
                        <option value="">--- Year ---</option>
                        <?php $y = date('Y');
                        for ($i = $y; $i >= 2019; $i--) : ?>
                            <option value="<?php echo $i; ?>" <?php echo ($this->input->post('year') == $i) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                        <?php endfor; ?>
                    </select>
                    <button type="submit" class="btn btn-primary btn-sm"
                            id="filter"><?php echo $this->lang->line('global_search', false); ?></button>
                </div>
                <!--        </form>-->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> Add</button> -->
                    <a href="<?php echo base_url("sensha-admin/financial_manager/export") ?>"
                       class="btn btn-info"> <?php echo $this->lang->line('global_export', false); ?></a>
                    <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
                        <a href="<?php echo base_url("sensha-admin/financial_manager/export_special") ?>"
                           class="btn btn-info"> <?php echo $this->lang->line('global_tuhan_gate', false); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> Add</button> -->
                    <!-- <a href="<?php echo base_url("sensha-admin/financial_manager/export") ?>" class="btn btn-info"> Export</a> -->
                    <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
                        <button type="button" class="btn btn-success btn-add" data-toggle="modal"
                                data-target="#modal-pay-credit"><?php echo $this->lang->line('admin_financial_pay_credit', false); ?></button>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-pills ml-left">
                    <li class="nav-item"><a class="nav-link active" href="#tab_1"
                                            data-toggle="tab"><?php echo $this->lang->line('admin_financial_tab1', false); ?></a>
                    </li>
                    <?php if (!$this->ion_auth->in_group(array('AA'))) : ?>
                        <li class="nav-item"><a class="nav-link" href="#tab_2"
                                                data-toggle="tab"><?php echo $this->lang->line('admin_financial_tab2', false); ?> </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="card card-primary">
                            <!-- <div class="card-header">
                            <h3 class="card-title">Financial Manager</h3>
                          </div> -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-hover no-margin datatabel1" width="100%">
                                    <thead>
                                    <tr>
                                        <th colspan="2" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_basic_info', FALSE); ?></th>
                                        <th class="text-center"><?php echo $this->lang->line('admin_financial_tab1_saller_info', FALSE); ?></th>
                                        <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_agent_info', FALSE); ?></th>
                                        <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_purchase_info', FALSE); ?></th>
                                        <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab1_sales_info', FALSE); ?></th>
                                    </tr>
                                    <tr>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_shipment_id', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_date_of_payment', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_country', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_sales_history_company_name', FALSE); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_user_type', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_user_id', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_discount_ratio', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_no_of_item', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_sales_amount', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_shipping_fee', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_discount_total', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_sales_total', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_pay_to_agent', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab1_expected_income', false); ?></th>
                                        <th>Confirm Credit</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="card card-primary">
                            <!-- <div class="card-header">
                          <h3 class="card-title">Financial Manager</h3>
                        </div> -->
                            <div class="card-body table-responsive">
                                <table class="table table-bordered table-hover no-margin datatabel2" width="100%">
                                    <thead>
                                    <tr>
                                        <th colspan="3" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_basic_info', false); ?></th>
                                        <th colspan="2" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_payer_info', false); ?></th>
                                        <th colspan="4" class="text-center"><?php echo $this->lang->line('admin_financial_tab2_detailed_info', false); ?></th>
                                    </tr>
                                    <tr>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_date_of_payment', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_pay_type', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_shipment_id', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_country', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_account_type', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_sa_country', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_description', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_amount_to_pay', false); ?></th>
                                        <th><?php echo $this->lang->line('admin_financial_tab2_credit_after_process', false); ?></th>
                                    </tr>
                                    </thead>

                                </table>
                                <?php if ($this->ion_auth->in_group(array('SA'))) { ?>
                                    <div style="margin: 20px 0; max-width: 500px;">
                                        <table class="table table-bordered table-hover no-margin" width="100%"
                                               style="width: 100%">
                                            <thead>
                                            <tr>
                                                <th colspan="3" style="text-align: center">Credit Release Request (YEN)</th>
                                            </tr>
                                            <tr>
                                                <th style="text-align: center">SA Name</th>
                                                <th style="text-align: center">Amount</th>
                                                <th style="text-align: center">Confirm to send</th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <form action="<?php echo base_url('sensha-admin/financial_manager/send_credit_release_request')?>" method="post">
                                                    <td>
                                                        <?php echo $this->ion_auth->user()->row()->company_name ?></td>
                                                    <input type="hidden" name="sa_name" value="<?php echo $this->ion_auth->user()->row()->company_name ?>">
                                                    <input type="hidden" name="sa_country" value="<?php echo $this->ion_auth->user()->row()->country ?>">
                                                    <input type="hidden" name="sa_current_credit" value="<?php echo $current_credit->balance ?>">
                                                    <td>
                                                        <input type="number" name="amount" style="width: 100%" required>
                                                    </td>
                                                    <td style="text-align: center">
                                                        <button type="submit" class="btn btn-warning">SEND</button>
                                                    </td>
                                                </form>
                                            </tr>
                                        </table>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        ?>


</section>

<div id="modal-add" class="modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Financial Manager</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                      action="<?php echo base_url("sensha-admin/financial_manager/add"); ?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Month</label>
                                    <select class="form-control select2" name="month" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                                            <option value="<?php echo date('m', strtotime('2017-' . $i)) ?>"><?php echo date('m', strtotime('2017-' . $i)) ?></option>
                                        <?php endfor; ?>]
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Shop Type</label>
                                    <select class="form-control select2" name="shop_type" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <option value="HQ">HQ</option>
                                        <option value="SA">SA</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Agent Type</label>
                                    <select class="form-control select2" name="agent_type" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <option value="none">None</option>
                                        <option value="SA">SA</option>
                                        <option value="AA">AA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Sales Amount</label>
                                    <input type="text" class="form-control" name="sale_amount">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Shipment Amount</label>
                                    <input type="text" class="form-control" name="shipment_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Discount Amount</label>
                                    <input type="text" class="form-control" name="discount_amount">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Total Amount</label>
                                    <input type="text" class="form-control" name="total_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>No. Of Transaction</label>
                                    <input type="text" class="form-control" name="no_transaction">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Average Sales Amount</label>
                                    <input type="text" class="form-control" name="avg_sale_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Amount To Pay</label>
                                    <input type="text" class="form-control" name="amount_to_pay">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div id="modal-edit" class="modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Financial Manager</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" id="form_edit" method="post" enctype="multipart/form-data"
                      action="<?php echo base_url("sensha-admin/financial_manager/edit"); ?>">
                    <input type="hidden" id="edit_id" name="edit_id"/>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Month</label>
                                    <select class="form-control select2" name="month" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <?php for ($i = 1; $i <= 12; $i++) : ?>
                                            <option value="<?php echo date('m', strtotime('2017-' . $i)) ?>"><?php echo date('m', strtotime('2017-' . $i)) ?></option>
                                        <?php endfor; ?>]
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Shop Type</label>
                                    <select class="form-control select2" name="name" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <option value="HQ">HQ</option>
                                        <option value="SA">SA</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Agent Type</label>
                                    <select class="form-control select2" name="name" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <option value="none">None</option>
                                        <option value="SA">SA</option>
                                        <option value="AA">AA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Sales Amount</label>
                                    <input type="text" class="form-control" name="sale_amount">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Shipment Amount</label>
                                    <input type="text" class="form-control" name="shipment_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Discount Amount</label>
                                    <input type="text" class="form-control" name="discount_amount">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Total Amount</label>
                                    <input type="text" class="form-control" name="total_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>No. Of Transaction</label>
                                    <input type="text" class="form-control" name="no_transaction">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label>Average Sales Amount</label>
                                    <input type="text" class="form-control" name="avg_sale_amount">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Amount To Pay</label>
                                    <input type="text" class="form-control" name="amount_to_pay">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="modal-pay-credit" class="modal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo $this->lang->line('admin_financial_pay_credit', false); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" id="form_pay_credit" method="post" enctype="multipart/form-data"
                      action="<?php echo base_url("sensha-admin/financial_manager/pay_credit"); ?>">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_financial_country', false); ?></label>
                                    <select class="form-control select2" name="country" style="width: 100%;">
                                        <option selected="selected" value="">-- Choose --</option>
                                        <?php foreach ($nation_lang as $item) : ?>
                                            <?php if ($item->country != 'Global') : ?>
                                                <option value="<?php echo $item->country; ?>"><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_financial_credit', false); ?></label>
                                    <input type="text" class="form-control" name="credit" readonly/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label><?php echo $this->lang->line('admin_financial_amount_to_release', false); ?></label>
                                    <input type="text" class="form-control" name="amount_of_release"/>
                                </div>
                            </div>
                            <div class="col">

                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit"
                                class="btn btn-primary"><?php echo $this->lang->line('admin_financial_submit', false); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php foreach ($credit_history as $key => $item) : ?>
    <div id="c-<?php echo $item->country; ?>" data-val="<?php echo $item->balance; ?>"></div>
<?php endforeach; ?>


<!-- DataTables -->
<link rel="stylesheet"
      href="<?= base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?= base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?= base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>
<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>

<script>

    <?php if($this->session->flashdata('msg_email')) {?>
    Swal.fire({
        title: 'Send Email!',
        text: 'Your Send Email success.',
        type: 'success',
        showConfirmButton: false,
    })
    <?php } ?>
    function edit_item(t) {
        $('#modal-edit select[name="month"]').select2("val", $(t).data('month'));
        $('#modal-edit input[name="font"]').val($(t).data('font'));
        // $('#modal-edit #description').val($(t).data('des'));

        $('#edit_id').val($(t).data('id'));
        $('#modal-edit').modal();
    }

    function delete_item(t) {
        Swal.fire({
            title: 'Are you sure to delete <br /> "' + $(t).data('firstname') + ' ' + $(t).data('lastname') + '"?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?= base_url("sensha-admin/financial_manager/delete/") ?>' + $(t).data('id'),
                })
                    .done(function (msg) {
                        // r = JSON.parse(msg);
                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your data has been deleted.',
                            type: 'success',
                            showConfirmButton: false,
                        })
                        setTimeout(function () {
                            window.location = '';
                        }, 1400);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    function conf_credit(shipment_id, t) {
        Swal.fire({
            title: 'Are you sure to confirm credit for shipment ' + shipment_id,
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?= base_url("sensha-admin/financial_manager/add_credit/") ?>' + shipment_id,
                })
                    .done(function (msg) {
                        r = JSON.parse(msg);
                        if (r.error == 0) {
                            Swal.fire({
                                title: 'Success',
                                text: 'Your credit is added.',
                                type: 'success',
                                // showConfirmButton: false,
                            })

                            $(t).text('Confirmed');
                            $(t).removeClass('btn-warning');
                            $(t).addClass('btn-default');
                            $(t).attr('disabled', 'disabled');
                        } else if (r.error == 1) {
                            Swal.fire({
                                title: 'Error',
                                text: "Credit can't add",
                                type: 'warning',
                                // showConfirmButton: false,
                            })
                        }
                        setTimeout(function(){location.reload()}, 300);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    function cancle_credit(shipment_id, t) {
        Swal.fire({
            title: 'Are you sure to cancla credit for shipment ' + shipment_id,
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?= base_url("sensha-admin/financial_manager/cancle/") ?>' + shipment_id,
                })
                    .done(function (msg) {
                        r = JSON.parse(msg);
                        if (r.error == 0) {
                            Swal.fire({
                                title: 'Success',
                                text: 'Your credit is added.',
                                type: 'success',
                                // showConfirmButton: false,
                            })

                            $(t).text('Cancled');
                            $(t).removeClass('btn-warning');
                            $('#conf-'+shipment_id).addClass('hide');
                            $(t).addClass('btn-default');
                            $(t).attr('disabled', 'disabled');
                        } else if (r.error == 1) {
                            Swal.fire({
                                title: 'Error',
                                text: "Credit can't add",
                                type: 'warning',
                                // showConfirmButton: false,
                            })
                        }
                        setTimeout(function(){location.reload()}, 300);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    $(function () {

        function fill_datatable(filter_country = '', filter_month = '', filter_year = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/financial_manager/data_index_sales_total" ?>`;
            var dataList = $('.datatabel1');
            dataList.DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data: {
                        filter_country: filter_country,
                        filter_month: filter_month,
                        filter_year: filter_year
                    }
                },
                order: [[0, "asc"]],
                pageLength: 100,
                lengthMenu: [100, 150, 200, 500],
                columns: [
                    {data: "shipment_id", orderable: false},
                    {data: "create_date", orderable: false},
                    {data: "country", orderable: false},
                    {data: "purchaser_company" , orderable: false},
                    {data: "purchaser_type" , orderable: false},
                    {data: "create_by", orderable: false},
                    {data: "ion_auth", orderable: false},
                    {data: "no_of_item", orderable: false},
                    {data: "sales_amount", orderable: false},
                    {data: "shipping_fee", orderable: false},
                    {data: "discount_total", orderable: false},
                    {data: "sales_total", orderable: false},
                    {data: "pay_to_agent", orderable: false},
                    {data: "expected_income", orderable: false},
                    {data: "action",width:"15%", orderable: false},
                ]
            });
        }

        function fill_datatable_credit(filter_country = '', filter_month = '', filter_year = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/financial_manager/data_index_sales_history_credit" ?>`;
            var dataList = $('.datatabel2');
            dataList.DataTable({
                serverSide: true,
                processing: false,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data: {
                        filter_country: filter_country,
                        filter_month: filter_month,
                        filter_year: filter_year
                    }
                },
                order: [[0, "asc"]],
                pageLength: 100,
                lengthMenu: [100, 150, 200, 500],
                columns: [
                    {data: "create_date", orderable: false},
                    {data: "credit_type", orderable: false},
                    {data: "shipment_id", orderable: false},
                    {data: "purchaser_country" ,orderable: false},
                    {data: "purchaser", orderable: false},
                    {data: "country", orderable: false},
                    {data: "item_amount", orderable: false},
                    {data: "transaction", orderable: false},
                    {data: "balance", orderable: false},
                ]
            });
        }

        $(document).ready(function () {

            var filter_month = $('#filter_month').val();
            var filter_year = $('#filter_year').val();
            var filter_country = $('#filter_country').val();
            fill_datatable(filter_country, filter_month, filter_year);
            fill_datatable_credit(filter_country, filter_month, filter_year);
            $('#filter').click(function () {
                var filter_country = $('#filter_country').val();
                var filter_month = $('#filter_month').val();
                var filter_year = $('#filter_year').val();
                if (filter_country != '' || filter_month != '' || filter_year != '') {
                    $('.datatabel1').DataTable().destroy();
                    $('.datatabel2').DataTable().destroy();
                    fill_datatable(filter_country, filter_month, filter_year);
                    fill_datatable_credit(filter_country, filter_month, filter_year);
                } else {
                    $('.datatabel1').DataTable().destroy();
                    $('.datatabel2').DataTable().destroy();
                    fill_datatable(filter_country, filter_month, filter_year);
                    fill_datatable_credit(filter_country, filter_month, filter_year);
                }
            });
        });

        // $('.select2').select2();

        $('#modal-pay-credit select[name="country"]').change(function () {
            var country = $(this).val().replace(' ', '-');
            $('#modal-pay-credit input[name="credit"]').val($('#c-' + country).data('val'));
        });

        $('#form_pay_credit').submit(function () {
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize()
            })
                .done(function (msg) {
                    Swal.fire({
                        type: 'success',
                        title: 'success',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1400);
                    // // console.log(msg);
                    // r = JSON.parse(msg);
                    // // console.log(r);
                    // if(r.error == 0){
                    //   Swal.fire({
                    //     type: 'success',
                    //     title: 'success',
                    //     showConfirmButton: false,
                    //     allowOutsideClick:false
                    //   });
                    //   setTimeout(function(){ location.reload(); }, 1400);
                    // }
                    // else{
                    //   Swal.fire(
                    //     'Oops...',
                    //     'Something went wrong!',
                    //     'error'
                    //   );
                    // }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });

        $('#form_add, #form_edit').submit(function () {
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize()
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        Swal.fire({
                            type: 'success',
                            title: 'success',
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1400);
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });
    });
</script>