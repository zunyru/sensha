<style>
/* .modal-lg{
width: 80%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_post_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_post_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <?php if($this->ion_auth->in_group(array('admin'))):?>
      <div class="row">
        <div class="col-md-4">
          <form method="post" action="<?php echo base_url("sensha-admin/news_post");?>">
            <div class="form-group">
              <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
              <select name="country">
                <option value="">--- Choose Country ---</option>
                <?php foreach($nation_lang as $item):?>
                  <option value="<?php echo $item->country;?>" <?php echo ($this->input->post('country') == $item->country)?'selected':'';?>><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                <?php endforeach;?>
              </select>
              <button type="submit" class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
            </div>
          </form>
        </div>
      </div>
    <?php endif;?>


    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
        <br><br>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $this->lang->line('admin_post_title', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_post_title', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_post_create_date', FALSE); ?></th>
                  <th width="10%"></th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($news_post)):?>
                  <?php $n = 1; foreach($news_post as $item):?>
                    <tr>
                      <td><?php echo $n;?></td>
                      <td><?php echo ucwords(str_replace("_"," ", $item->country))?></td>
                      <td><?php echo $item->title;?></td>
                      <td><?php echo $item->create_date;?></td>
                      <td>
                        <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?php echo $item->id?>" data-country="<?php echo $item->country?>" data-title="<?php echo $item->title?>" data-content="<?php echo $item->content?>" data-img="<?php echo $item->img?>" ><i class="fa fa-pencil-square-o"></i></button>
                        <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?php echo $item->id?>" data-title="<?php echo $item->title?>"><i class="fa fa-trash-o"></i></button>
                      </td>
                    </tr>
                    <?php $n++; endforeach;?>
                  <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div id="modal-add" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_post_create_new_post', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/news_post/add");?>">
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                    <select class="form-control select2" name="country" style="width: 100%;" >
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_post_post_title', FALSE); ?></label>
                    <input type="text" class="form-control" name="title"  >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_post_post_content', FALSE); ?></label>
                    <textarea id="content1" rows="10" class="form-control" name="content"  ></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

      <div id="modal-edit" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_post_edit_post', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/news_post/edit");?>">
                <input type="hidden" id="edit_id" name="edit_id" />
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                    <select class="form-control select2" name="country" style="width: 100%;" >
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_post_post_title', FALSE); ?></label>
                    <input type="text" class="form-control" name="title"  >
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_post_post_content', FALSE); ?></label>
                    <textarea id="content2" rows="10" class="form-control" name="content"  ></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </section>




    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js")?>"></script>

    <!-- ckeditor -->
    <!-- <script src="https://cdn.ckeditor.com/4.11.2/standard-all/ckeditor.js"></script> -->
<!--     <script src="https://cdn.ckeditor.com/4.10.1/standard-all/ckeditor.js"></script> -->
<script src="<?php echo base_url("assets/ckeditor/ckeditor.js")?>"></script>

    <script>
    function edit_item(t){
      $('#modal-edit select[name="country"]').select2("val", $(t).data('country'));

      $('#modal-edit input[name="title"]').val($(t).data('title'));
      CKEDITOR.instances['content2'].setData($(t).data('content'));

      $('#edit_id').val($(t).data('id'));
      $('#modal-edit').modal();
    }

    function delete_item(t){
      Swal.fire({
        title: 'Are you sure to delete <br /> "'+$(t).data('title')+'"?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: "POST",
            url: '<?=base_url("sensha-admin/news_post/delete/")?>'+$(t).data('id'),
          })
          .done(function( msg ) {
            // r = JSON.parse(msg);
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
            setTimeout(function(){ window.location = ''; }, 1400);
          })
          .fail(function( msg ) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
        }
      });
    }

    $(function(){
      $('#list_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength": 30
      });

      $('.select2').select2();

      // $('.wysihtml5-editor').wysihtml5({
      //   toolbar: { fa: true }
      // });

      // var editor = new wysihtml.Editor("wysihtml-textarea", { // id of textarea element
      //   toolbar:      "wysihtml-toolbar", // id of toolbar element
      //   parserRules:  wysihtml5ParserRules // defined in parser rules set
      // });

      $('#form_add, #form_edit').submit(function(){
        if($(this).attr('id') == 'form_add'){
          $('#modal-add textarea[name="content"]').val(CKEDITOR.instances.content1.getData())
        }
        if($(this).attr('id') == 'form_edit'){
          $('#modal-edit textarea[name="content"]').val(CKEDITOR.instances.content2.getData())
        }
        $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $( this ).serialize()
        })
        .done(function( msg ) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if(r.error == 0){
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick:false
            });
            setTimeout(function(){ location.reload(); }, 1400);
          }
          else{
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function( msg ) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
        return false;
      });
    });

    CKEDITOR.replace( 'content1',{
      extraPlugins: 'uploadimage',
      uploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '/apps/ckfinder/3.4.4/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url("uploads/news_post")?>',
      filebrowserUploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '<?php echo base_url("sensha-admin/news_post/editorUploadImage")?>',
      // removeDialogTabs: 'image:advanced;image:Link;image:Image Info;link:advanced;'
    });

    CKEDITOR.replace( 'content2',{
      extraPlugins: 'uploadimage',
      uploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '/apps/ckfinder/3.4.4/ckfinder.html',
      filebrowserImageBrowseUrl: '<?php echo base_url("uploads/news_post")?>',
      filebrowserUploadUrl: '/apps/ckfinder/3.4.4/core/connector/php/connector.php?command=QuickUpload&type=Files',
      filebrowserImageUploadUrl: '<?php echo base_url("sensha-admin/news_post/editorUploadImage")?>',
      // removeDialogTabs: 'image:advanced;image:Link;image:Image Info;link:advanced;'
    });

    CKEDITOR.on('dialogDefinition', function( ev )
    {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;

      switch (dialogName) {
        case 'image': //Image Properties dialog
        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');
        // dialogDefinition.removeContents('info');
        break;
        case 'link': //image Properties dialog
        break;
      }
    });
    </script>
