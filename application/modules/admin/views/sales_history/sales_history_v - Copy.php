<style>
  /* .modal-lg{
  width: 90%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-4">
        <form method="post" action="<?php echo base_url("sensha-admin/sales_history"); ?>">
          <div class="form-group">
            <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
              <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
              <select name="country">
                <option value="">--- Choose Country ---</option>
                <?php foreach ($nation_lang as $item) : ?>s
                    <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_"," ", $item->country)) ?></option>
                <?php endforeach; ?>
              </select>
            <?php endif; ?>
            <select name="month">
              <option value="">--- Month ---</option>
              <?php for ($i = 1; $i <= 12; $i++) : ?>
                <option value="<?php echo date('m', strtotime("2019-$i")); ?>" <?php echo ($this->input->post('month') == $i) ? 'selected' : ''; ?>><?php echo date('F', strtotime("2019-$i")); ?></option>
              <?php endfor; ?>
            </select>
            <select name="year">
              <option value="">--- Year ---</option>
              <?php $y = date('Y');
              for ($i = $y; $i >= 2019; $i--) : ?>
                <option value="<?php echo $i; ?>" <?php echo ($this->input->post('year') == $i) ? 'selected' : ''; ?>><?php echo $i; ?></option>
              <?php endfor; ?>
            </select>
            <button type="submit" class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
          </div>
        </form>
      </div>
    </div>



    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <?php if(!empty($this->session->userdata('admin_sql_export'))){ ?>
          <a href="<?php echo base_url("sensha-admin/sales_history/export") ?>" class="btn btn-info"> <?php echo $this->lang->line('global_export', FALSE); ?></a>
          <?php } ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-Sales History"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th><?php echo $this->lang->line('admin_financial_tab1_shipment_id', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_account_type', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_company_name', FALSE); ?></th>
                  <!-- <th>Account Name</th> -->
                  <!-- <th>Sales Item</th> -->
                  <!-- <th>Product Name</th> -->
                  <th><?php echo $this->lang->line('admin_sales_history_sales_date', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_item_id', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_item_name', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_item_amount', FALSE); ?></th>
                  <!-- <th>Item Sub Total Amount</th> -->
                  <th><?php echo $this->lang->line('admin_sales_history_sales_total_amount', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_sales_history_payment_method', FALSE); ?></th>
                  <!--th><?php echo $this->lang->line('admin_sales_history_payment_status', FALSE); ?></th-->
                  <th><?php echo $this->lang->line('admin_sales_history_shipping_status', FALSE); ?></th>
                  <!-- <th width="10%"></th> -->
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($sales_history)) : ?>
                  <?php $n = 1;
                  foreach ($sales_history as $item) : ?>
                    <?php 
	                     // if (!(in_array($item->payment_method, array('amazon', 'paypal')) && !($item->payment_status == 'Completed'))) : 
	                    if ((in_array($item->payment_method, array('amazon', 'paypal')) && $item->payment_status == 'Completed') || in_array($item->payment_method, array('pay on delivery', 'Bank Transfer', Null))) : 
	                    //if (!(in_array($item->payment_method, array('amazon', 'paypal')) && ($item->payment_status == '-'))) : 
	                    ?>
                      <tr>
                        <td><?php echo $n; ?></td>
                        <td><?php echo $item->shipment_id; ?></td>
                        <td><?php echo ucwords(str_replace("_"," ", $item->country)) ?></td>
                        <td><?php echo $item->account_type; ?></td>
                        <td><?php echo ($item->purchaser_company == '') ? $item->purchaser_name : $item->purchaser_company; ?></td>
                        <!-- <td><?php echo $item->account_name; ?></td> -->
                        <!-- <td><?php echo $item->sale_item_id; ?></td> -->
                        <!-- <td><?php echo $item->product_name; ?></td> -->
                        <td><?php echo $item->sale_date; ?></td>
                        <td><?php echo $item->sale_item_id; ?></td>
                        <td><?php echo $item->product_name; ?></td>
                        <td><?php echo $item->item_amount; ?></td>
                        <!-- <td><?php echo number_format($item->item_sub_total_amount); ?></td> -->
                        <td><?php echo number_format($item->sale_total_amount); ?></td>
                        <td><?php echo $item->payment_method; ?></td>
                        <!--td><?php echo $item->payment_status; ?></td-->
                        <td>
                          <select name="shipping_status" onchange="change_shipping_status(this);" data-shipment_id="<?php echo $item->shipment_id; ?>" class="form-control" <?php if (($this_country == $item->country && $this->ion_auth->in_group(array('SA'))) || $this->ion_auth->in_group(array('admin'))) {
                                                                                                                                                                            } else {
                                                                                                                                                                              echo 'disabled';
                                                                                                                                                                            } ?>>
                            <option value="Processing" <?php echo ($item->shipping_status == 'Processing') ? 'selected' : '';; ?>>Processing</option>
                            <option value="Paid" <?php echo ($item->shipping_status == 'Paid') ? 'selected' : '';; ?>>Paid</option>
                            <option value="Unpaid" <?php echo ($item->shipping_status == 'Unpaid') ? 'selected' : '';; ?>>Unpaid</option>
                            <option value="Credit sale" <?php echo ($item->shipping_status == 'Credit sale') ? 'selected' : '';; ?>>Credit sale</option>
                            <option value="Completed" <?php echo ($item->shipping_status == 'Completed') ? 'selected' : '';; ?>>Completed</option>
                          </select>
                        </td>
                        <!-- <td>
                      <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?php echo $item->id ?>" data-country="<?php echo $item->country ?>" data-account_type="<?php echo $item->account_type ?>" data-account_name="<?php echo $item->account_name ?>" data-sale_date="<?php echo $item->sale_date ?>" data-sale_item_category="<?php echo $item->sale_item_category ?>" data-sale_item_id="<?php echo $item->sale_item_id ?>" data-item_amount="<?php echo $item->item_amount ?>" data-item_sub_total_amount="<?php echo $item->item_sub_total_amount ?>" data-delivery_amount="<?php echo $item->delivery_amount ?>" data-discount_amount="<?php echo $item->discount_amount ?>" data-sale_total_amount="<?php echo $item->sale_total_amount ?>" data-purchaser_type="<?php echo $item->purchaser_type ?>" data-purchaser_id="<?php echo $item->purchaser_id ?>" data-shipping_addr="<?php echo $item->shipping_addr ?>" data-payment_status="<?php echo $item->payment_status ?>" data-payment_method="<?php echo $item->payment_method ?>" data-payment_date="<?php echo $item->payment_date ?>" data-shipping_method="<?php echo $item->shipping_method ?>" data-shipping_status="<?php echo $item->shipping_status ?>" data-shipping_date="<?php echo $item->shipping_date ?>" data-emergency_status="<?php echo $item->emergency_status ?>" data-emergency_date="<?php echo $item->emergency_date ?>" data-create_by="<?php echo $item->create_by ?>" data-update_by="<?php echo $item->update_by ?>" data-create_date="<?php echo $item->create_date ?>" data-update_date="<?php echo $item->update_date ?>"><i class="fa fa-pencil-square-o"></i></button>
                      <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?php echo $item->id ?>" ><i class="fa fa-trash-o"></i></button>
                    </td> -->
                      </tr>
                    <?php endif; ?>

                  <?php $n++;
                  endforeach; ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div id="modal-add" class="modal" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-Sales History"><?php echo $this->lang->line('admin_sales_history_create_sales_hitory', FALSE); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/sales_history/add"); ?>">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                      <select class="form-control select2" name="country" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <?php foreach ($countries as $item) : ?>
                          <option value="<?php echo $item->nicename; ?>"><?php echo $item->nicename; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_account_setting_account_type', FALSE); ?></label>
                      <select class="form-control select2" name="account_type" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <?php foreach ($groups_list as $item) : ?>
                          <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_sales_history_account_name', FALSE); ?></label>
                      <input type="text" class="form-control" name="account_name" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_sales_history_sales_date', FALSE); ?></label>
                      <input type="text" class="form-control datepicker" name="sale_date" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label><?php echo $this->lang->line('admin_sales_history_sale_item_category', FALSE); ?></label>
                      <input type="text" class="form-control" name="sale_item_category" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Sale Item ID</label>
                      <input type="text" class="form-control" name="sale_item_id" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Item Amount</label>
                      <input type="text" class="form-control" name="item_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Item Sub Total item_amount</label>
                      <input type="text" class="form-control" name="item_sub_total_amount" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Delivery Amount</label>
                      <input type="text" class="form-control" name="delivery_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Discount Amount</label>
                      <input type="text" class="form-control" name="discount_amount" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Sales Total Amount</label>
                      <input type="text" class="form-control" name="sale_total_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Purchaser Type</label>
                      <input type="text" class="form-control" name="purchaser_type" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Purchaser ID</label>
                      <input type="text" class="form-control" name="purchaser_id" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Address</label>
                      <input type="text" class="form-control" name="shipping_addr" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Status</label>
                      <input type="text" class="form-control" name="payment_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Method</label>
                      <input type="text" class="form-control" name="payment_method" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Date</label>
                      <input type="text" class="form-control" name="payment_date" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Method</label>
                      <input type="text" class="form-control" name="shipping_method" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Status</label>
                      <input type="text" class="form-control" name="shipping_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Date</label>
                      <input type="text" class="form-control datepicker" name="shipping_date" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Emergency Status</label>
                      <input type="text" class="form-control" name="emergency_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Emergency Date</label>
                      <input type="text" class="form-control datepicker" name="emergency_date" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
              </div>
            </form>
          </div>

        </div>
      </div>
    </div>

    <div id="modal-edit" class="modal" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-Sales History">Edit Sales History</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/sales_history/edit"); ?>">
              <input type="hidden" id="edit_id" name="edit_id" />
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Country</label>
                      <select class="form-control select2" name="country" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <?php foreach ($countries as $item) : ?>
                          <option value="<?php echo $item->nicename; ?>"><?php echo $item->nicename; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Account Type</label>
                      <select class="form-control select2" name="account_type" style="width: 100%;">
                        <option selected="selected" value="">-- Choose --</option>
                        <?php foreach ($groups_list as $item) : ?>
                          <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Account Name</label>
                      <input type="text" class="form-control" name="account_name" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Sale Date</label>
                      <input type="text" class="form-control datepicker" name="sale_date" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Sale Item Category</label>
                      <input type="text" class="form-control" name="sale_item_category" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Sale Item ID</label>
                      <input type="text" class="form-control" name="sale_item_id" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Item Amount</label>
                      <input type="text" class="form-control" name="item_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Item Sub Total item_amount</label>
                      <input type="text" class="form-control" name="item_sub_total_amount" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Delivery Amount</label>
                      <input type="text" class="form-control" name="delivery_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Discount Amount</label>
                      <input type="text" class="form-control" name="discount_amount" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Sales Total Amount</label>
                      <input type="text" class="form-control" name="sale_total_amount" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Purchaser Type</label>
                      <input type="text" class="form-control" name="purchaser_type" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Purchaser ID</label>
                      <input type="text" class="form-control" name="purchaser_id" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Address</label>
                      <input type="text" class="form-control" name="shipping_addr" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Status</label>
                      <input type="text" class="form-control" name="payment_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Method</label>
                      <input type="text" class="form-control" name="payment_method" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Payment Date</label>
                      <input type="text" class="form-control" name="payment_date" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Method</label>
                      <input type="text" class="form-control" name="shipping_method" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Status</label>
                      <input type="text" class="form-control" name="shipping_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Shipping Date</label>
                      <input type="text" class="form-control datepicker" name="shipping_date" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label>Emergency Status</label>
                      <input type="text" class="form-control" name="emergency_status" placeholder="">
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-group">
                      <label>Emergency Date</label>
                      <input type="text" class="form-control datepicker" name="emergency_date" placeholder="">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>



</section>


<!-- DataTables -->
<link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>

<!-- Date picker -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/datetimepicker-master/jquery.datetimepicker.css") ?>" />
<script src="<?php echo base_url("assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js") ?>"></script>

<script>
  function edit_item(t) {
    $('#modal-edit select[name="country"]').val($(t).data('country'));
    $('.select2-selection__rendered').text($(t).data('country'));
    $('#modal-edit input[name="language"]').val($(t).data('language'));
    $('#modal-edit input[name="font"]').val($(t).data('font'));
    // $('#modal-edit #description').val($(t).data('des'));

    $('#edit_id').val($(t).data('id'));
    $('#modal-edit').modal();
  }

  function delete_item(t) {
    Swal.fire({
      title: 'Are you sure to delete?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
            method: "POST",
            url: '<?= base_url("sensha-admin/sales_history/delete/") ?>' + $(t).data('id'),
          })
          .done(function(msg) {
            // r = JSON.parse(msg);
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
            setTimeout(function() {
              window.location = '';
            }, 1400);
          })
          .fail(function(msg) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
      }
    });
  }

  function change_shipping_status(t) {
    $.post("<?php echo base_url("sensha-admin/sales_history/changeShippingStatus"); ?>", {
        shipping_status: $(t).val(),
        shipment_id: $(t).data('shipment_id')
      })
      .done(function(data) {
        // console.log(data);
        // alert( "Data Loaded: " + data );
        window.location = '';
      });
  }

  $(function() {

    $('#list_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "pageLength": 30
    });

    $('.datepicker').datetimepicker({
      timepicker: false,
      format: 'Y-m-d',
    });

    // $('.select2').select2();
    // $('select[name="shipping_status"]').change(function() {
    //   $.post("<?php echo base_url("sensha-admin/sales_history/changeShippingStatus"); ?>", {
    //       shipping_status: $(this).val(),
    //       shipment_id: $(this).data('shipment_id')
    //     })
    //     .done(function(data) {
    //       // alert( "Data Loaded: " + data );
    //     });
    // });

    $('#form_add, #form_edit').submit(function() {
      $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $(this).serialize()
        })
        .done(function(msg) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if (r.error == 0) {
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick: false
            });
            setTimeout(function() {
              location.reload();
            }, 1400);
          } else {
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function(msg) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
      return false;
    });
  });
</script>