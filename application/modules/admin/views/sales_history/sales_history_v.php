card-Sales History<style>
    /* .modal-lg{
    width: 90%;
  } */
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-4">
<!--                <form method="post" action="--><?php //echo base_url("sensha-admin/sales_history"); ?><!--">-->
                    <div class="form-group">
                        <?php if ($this->ion_auth->in_group(array('admin'))) : ?>
                            <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
                            <select name="country" id="filter_country">
                                <option value="">--- Choose Country ---</option>
                                <?php foreach ($nation_lang as $item) : ?>s
                                    <option value="<?php echo $item->country; ?>" <?php echo ($this->input->post('country') == $item->country) ? 'selected' : ''; ?>><?php echo ucwords(str_replace("_", " ", $item->country)) ?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                        <select name="month" id="filter_month">
                            <option value="">--- Month ---</option>
                            <?php for ($i = 1; $i <= 12; $i++) : ?>
                                <option value="<?php echo date('m', strtotime("2019-$i")); ?>" <?php echo ($this->input->post('month') == $i) ? 'selected' : ''; ?>><?php echo date('F', strtotime("2019-$i")); ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="year" id="filter_year">
                            <option value="">--- Year ---</option>
                            <?php $y = date('Y');
                            for ($i = $y; $i >= 2019; $i--) : ?>
                                <option value="<?php echo $i; ?>" <?php echo ($this->input->post('year') == $i) ? 'selected' : ''; ?>><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <button type="submit"
                                class="btn btn-primary btn-sm" id="filter"><?php echo $this->lang->line('global_search', FALSE); ?></button>
                    </div>
<!--                </form>-->
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

                        <a href="<?php echo base_url("sensha-admin/sales_history/export") ?>"
                           class="btn btn-info"> <?php echo $this->lang->line('global_export', FALSE); ?></a>
                    
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-Sales History"><?php echo $this->lang->line('admin_sales_history_title', FALSE); ?></h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-hover no-margin" id="list_table">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th><?php echo $this->lang->line('admin_financial_tab1_shipment_id', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_account_type', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_company_name', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_sales_date', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_item_id', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_item_name', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_item_amount', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_sales_total_amount', FALSE); ?></th>
                                <th><?php echo $this->lang->line('admin_sales_history_payment_method', FALSE); ?></th>
                                <th style="min-width: 100px;"><?php echo $this->lang->line('admin_sales_history_shipping_status', FALSE); ?></th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal-add" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-Sales History"><?php echo $this->lang->line('admin_sales_history_create_sales_hitory', FALSE); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_add" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/sales_history/add"); ?>">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                                            <select class="form-control select2" name="country" style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($countries as $item) : ?>
                                                    <option value="<?php echo $item->nicename; ?>"><?php echo $item->nicename; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_account_setting_account_type', FALSE); ?></label>
                                            <select class="form-control select2" name="account_type"
                                                    style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($groups_list as $item) : ?>
                                                    <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_sales_history_account_name', FALSE); ?></label>
                                            <input type="text" class="form-control" name="account_name" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_sales_history_sales_date', FALSE); ?></label>
                                            <input type="text" class="form-control datepicker" name="sale_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label><?php echo $this->lang->line('admin_sales_history_sale_item_category', FALSE); ?></label>
                                            <input type="text" class="form-control" name="sale_item_category"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sale Item ID</label>
                                            <input type="text" class="form-control" name="sale_item_id" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Item Amount</label>
                                            <input type="text" class="form-control" name="item_amount" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Item Sub Total item_amount</label>
                                            <input type="text" class="form-control" name="item_sub_total_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Delivery Amount</label>
                                            <input type="text" class="form-control" name="delivery_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Discount Amount</label>
                                            <input type="text" class="form-control" name="discount_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sales Total Amount</label>
                                            <input type="text" class="form-control" name="sale_total_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Purchaser Type</label>
                                            <input type="text" class="form-control" name="purchaser_type"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Purchaser ID</label>
                                            <input type="text" class="form-control" name="purchaser_id" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Address</label>
                                            <input type="text" class="form-control" name="shipping_addr" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Status</label>
                                            <input type="text" class="form-control" name="payment_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Method</label>
                                            <input type="text" class="form-control" name="payment_method"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Date</label>
                                            <input type="text" class="form-control" name="payment_date" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Method</label>
                                            <input type="text" class="form-control" name="shipping_method"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Status</label>
                                            <input type="text" class="form-control" name="shipping_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Date</label>
                                            <input type="text" class="form-control datepicker" name="shipping_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Emergency Status</label>
                                            <input type="text" class="form-control" name="emergency_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Emergency Date</label>
                                            <input type="text" class="form-control datepicker" name="emergency_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div id="modal-edit" class="modal" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-Sales History">Edit Sales History</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="form_edit" method="post" enctype="multipart/form-data"
                              action="<?php echo base_url("sensha-admin/sales_history/edit"); ?>">
                            <input type="hidden" id="edit_id" name="edit_id"/>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control select2" name="country" style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($countries as $item) : ?>
                                                    <option value="<?php echo $item->nicename; ?>"><?php echo $item->nicename; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Account Type</label>
                                            <select class="form-control select2" name="account_type"
                                                    style="width: 100%;">
                                                <option selected="selected" value="">-- Choose --</option>
                                                <?php foreach ($groups_list as $item) : ?>
                                                    <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Account Name</label>
                                            <input type="text" class="form-control" name="account_name" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sale Date</label>
                                            <input type="text" class="form-control datepicker" name="sale_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sale Item Category</label>
                                            <input type="text" class="form-control" name="sale_item_category"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sale Item ID</label>
                                            <input type="text" class="form-control" name="sale_item_id" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Item Amount</label>
                                            <input type="text" class="form-control" name="item_amount" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Item Sub Total item_amount</label>
                                            <input type="text" class="form-control" name="item_sub_total_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Delivery Amount</label>
                                            <input type="text" class="form-control" name="delivery_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Discount Amount</label>
                                            <input type="text" class="form-control" name="discount_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Sales Total Amount</label>
                                            <input type="text" class="form-control" name="sale_total_amount"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Purchaser Type</label>
                                            <input type="text" class="form-control" name="purchaser_type"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Purchaser ID</label>
                                            <input type="text" class="form-control" name="purchaser_id" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Address</label>
                                            <input type="text" class="form-control" name="shipping_addr" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Status</label>
                                            <input type="text" class="form-control" name="payment_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Method</label>
                                            <input type="text" class="form-control" name="payment_method"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Payment Date</label>
                                            <input type="text" class="form-control" name="payment_date" placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Method</label>
                                            <input type="text" class="form-control" name="shipping_method"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Status</label>
                                            <input type="text" class="form-control" name="shipping_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Shipping Date</label>
                                            <input type="text" class="form-control datepicker" name="shipping_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Emergency Status</label>
                                            <input type="text" class="form-control" name="emergency_status"
                                                   placeholder="">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Emergency Date</label>
                                            <input type="text" class="form-control datepicker" name="emergency_date"
                                                   placeholder="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit"
                                        class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


</section>


<!-- DataTables -->
<link rel="stylesheet"
      href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css") ?>">
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js") ?>"></script>
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js") ?>"></script>

<!-- Select2 -->
<script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js") ?>"></script>

<!-- Date picker -->
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url("assets/datetimepicker-master/jquery.datetimepicker.css") ?>"/>
<script src="<?php echo base_url("assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js") ?>"></script>

<script>
    function edit_item(t) {
        $('#modal-edit select[name="country"]').val($(t).data('country'));
        $('.select2-selection__rendered').text($(t).data('country'));
        $('#modal-edit input[name="language"]').val($(t).data('language'));
        $('#modal-edit input[name="font"]').val($(t).data('font'));
        // $('#modal-edit #description').val($(t).data('des'));

        $('#edit_id').val($(t).data('id'));
        $('#modal-edit').modal();
    }

    function delete_item(t) {
        Swal.fire({
            title: 'Are you sure to delete?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    method: "POST",
                    url: '<?= base_url("sensha-admin/sales_history/delete/") ?>' + $(t).data('id'),
                })
                    .done(function (msg) {
                        // r = JSON.parse(msg);
                        Swal.fire({
                            title: 'Deleted!',
                            text: 'Your data has been deleted.',
                            type: 'success',
                            showConfirmButton: false,
                        })
                        setTimeout(function () {
                            window.location = '';
                        }, 1400);
                    })
                    .fail(function (msg) {
                        Swal.fire({
                            type: 'error',
                            // text: msg.responseText,
                            title: msg.statusText,
                            // showConfirmButton: false
                        });
                    });
            }
        });
    }

    function change_shipping_status(t) {
        $.post("<?php echo base_url("sensha-admin/sales_history/changeShippingStatus"); ?>", {
            shipping_status: $(t).val(),
            shipment_id: $(t).data('shipment_id')
        })
            .done(function (data) {
                // console.log(data);
                // alert( "Data Loaded: " + data );
                window.location = '';
            });
    }

    $(function () {

        /*$('#list_table').DataTable({
              "paging": true,
              "lengthChange": false,
              "searching": true,
              "ordering": false,
              "info": true,
              "autoWidth": false,
              "pageLength": 30
          });*/
        function fill_datatable(filter_country = '',filter_month = '',filter_year = '') {
            var base_url = `<?php echo base_url() . "sensha-admin/sales_history/data_index" ?>`;
            var dataList = $('#list_table');
            dataList.DataTable({
                serverSide: true,
                processing: true,
                ajax: {
                    url: base_url,
                    type: 'POST',
                    data :{
                        filter_country: filter_country,
                        filter_month:filter_month,
                        filter_year:filter_year
                    }
                },
                order: [[0, "asc"]],
                pageLength: 100,
                lengthMenu: [100, 150, 200,500],
                columns: [
                    {data: "DT_RowId",width: "5%", orderable: false},
                    {data: "shipment_id",width: "5%", orderable: false},
                    {data: "country", orderable: false},
                    {data: "account_type", orderable: false},
                    {data: "purchaser_company", orderable: false},
                    {data: "sale_date", width: "10%",orderable: false},
                    {data: "sale_item_id",width: "7%", orderable: false},
                    {data: "product_name", orderable: false},
                    {data: "item_amount", orderable: false},
                    {data: "sale_total_amount", orderable: false},
                    {data: "payment_method", orderable: false},
                    {data: "action", orderable: false},
                ]
            });
        }


        $(document).ready(function () {

            var filter_month = $('#filter_month').val();
            var filter_year = $('#filter_year').val();
            fill_datatable('',filter_month,filter_year);
            $('#filter').click(function () {
                var filter_country = $('#filter_country').val();
                var filter_month = $('#filter_month').val();
                var filter_year = $('#filter_year').val();
                if (filter_country != '' || filter_month != '' || filter_year != '') {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country,filter_month,filter_year);
                } else {
                    $('#list_table').DataTable().destroy();
                    fill_datatable(filter_country,filter_month,filter_year);
                }
            });
        });

        $('.datepicker').datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
        });

        // $('.select2').select2();
        // $('select[name="shipping_status"]').change(function() {
        //   $.post("<?php echo base_url("sensha-admin/sales_history/changeShippingStatus"); ?>", {
        //       shipping_status: $(this).val(),
        //       shipment_id: $(this).data('shipment_id')
        //     })
        //     .done(function(data) {
        //       // alert( "Data Loaded: " + data );
        //     });
        // });

        $('#form_add, #form_edit').submit(function () {
            $.ajax({
                method: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize()
            })
                .done(function (msg) {
                    // console.log(msg);
                    r = JSON.parse(msg);
                    // console.log(r);
                    if (r.error == 0) {
                        Swal.fire({
                            type: 'success',
                            title: 'success',
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1400);
                    } else {
                        Swal.fire(
                            'Oops...',
                            'Something went wrong!',
                            'error'
                        );
                    }
                })
                .fail(function (msg) {
                    Swal.fire(
                        'Oops...',
                        msg,
                        'error'
                    );
                });
            return false;
        });
    });
</script>