<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 23px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 15px;
        width: 15px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(15px);
        -ms-transform: translateX(15px);
        transform: translateX(15px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo 'Login Setting'; ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo base_url("admin"); ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo 'Login Setting'; ?></li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title"><?php echo 'Login Lock Active'; ?></h3>
                    </div>
                    <div class="card-body">
                        <label class="switch">
                            <input type="checkbox" name="lock" <?php if ($lock[0]->active != 0) echo 'checked'; ?>>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.toggle').click(function (e) {
        e.preventDefault(); // The flicker is a codepen thing
        $(this).toggleClass('toggle-on');
    });

    $("input").change(function () {
        $.ajax({
            url: '<?=base_url("sensha-admin/login_setting/locked")?>',
            type: "POST",
            data: {lock: $(this).is(':checked')},
            success: function (result) {
                Swal.fire({
                    title: 'success !',
                    text: 'setting Locked',
                    type: 'success',
                    showConfirmButton: false,
                })
            }
        });
    });
</script>