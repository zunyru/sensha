<div class="container">
<div class="clr inner">
  <div id="breadcrumbs">
    <span><a href="index.php">Home</a><span><?php echo $this->lang->line('breadcrumb_complete', false); ?></span>
  </div>
</div><!--inner-->
<div class="clr inner">
    <div class="box-content">
    <div class="layout-contain">
      <div class="topic">
        <p class="title-page">Credit Confirm</p>
      </div>
      <div class="clr box_complete">
        <div class="left">
            <img src="<?php echo base_url("assets/sensha-theme/"); ?>images/img-complete.png">
        </div>
        <div class="right">
          <?php if ($is_get_credit): ?>
            <p style="color:red;">You already get credit</p>
          <?php else: ?>
          <p>Amount: <?php echo number_format($credit->release_amount); ?></p>
          <p>Country: <?php echo $credit->country; ?></p>
          <form role="form" id="form_pay_credit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/sensha_service/success_pay_credit/$encode"); ?>">
          <div class="form-group">
            <label>Your OTP</label>
            <input type="text" class="form-control" name="otp" />
            <button type="submit" class="b-blue">Confirm OTP to get credit</button>
          </div>
          </form>
          <?php endif;?>
        </div>
      </div>
    </div><!--layout-contain-->
  </div><!--box-content-->
</div><!--inner-->
</div><!--container-->

<script>
$('#form_pay_credit').submit(function(){
    $.ajax({
      method: "POST",
      url: $(this).attr('action'),
      data: $( this ).serialize()
    })
    .done(function( msg ) {
      if(msg == 1){
        Swal.fire({
        type: 'success',
        title: 'success',
        showConfirmButton: false,
        allowOutsideClick:false
      });
      setTimeout(function(){ location.reload(); }, 1400);
      }
      else if(msg == 0){
        Swal.fire({
          type: 'warning',
          title: 'You already get credit',
          showConfirmButton: true,
          allowOutsideClick:false
        });
      }
      else if(msg == 2){
        Swal.fire({
          type: 'warning',
          title: 'OTP not match',
          showConfirmButton: true,
          allowOutsideClick:false
        });
      }

    })
    .fail(function( msg ) {
      Swal.fire(
        'Oops...',
        msg,
        'error'
      );
    });
    return false;
  });
</script>

