<style>
/* .modal-lg{
  width: 80%;
} */
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?php echo $this->lang->line('admin_webpage_title', FALSE); ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url("admin");?>">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $this->lang->line('admin_webpage_title', FALSE); ?></li>
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="container-fluid">
    <div class="row hide">
      <div class="col-md-4">
        <form method="post" action="">
          <div class="form-group">
            <label><?php echo $this->lang->line('global_filter', FALSE); ?></label>
            <select name="country">
              <option value="">--- Choose Country ---</option>
              <?php foreach($nation_lang as $item):?>
                <option value="<?php echo $item->country;?>" <?php echo ($this->input->post('country') == $item->country)?'selected':'';?>><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
              <?php endforeach;?>
            </select>
            <button type="submit" class="btn btn-primary btn-sm"><?php echo $this->lang->line('global_search', FALSE); ?></button>
          </div>
        </form>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <a href="<?php echo base_url("sensha-admin/webpage_setting");?>" class="btn btn-outline-secondary btn-flat disabled"><?php echo $this->lang->line('admin_webpage_home', FALSE); ?></a>
          <a href="<?php echo base_url("sensha-admin/webpage_setting/about");?>" class="btn btn-outline-info btn-flat"><?php echo $this->lang->line('admin_webpage_about', FALSE); ?></a>
          <a href="<?php echo base_url("sensha-admin/webpage_setting/ppf");?>" class="btn btn-outline-info btn-flat"><?php echo $this->lang->line('admin_webpage_ppf', FALSE); ?></a>
          <a href="<?php echo base_url("sensha-admin/webpage_setting/agent");?>" class="btn btn-outline-info btn-flat"><?php echo $this->lang->line('admin_webpage_agent', FALSE); ?></a>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> <?php echo $this->lang->line('global_add', FALSE); ?></button>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title"><?php echo $this->lang->line('admin_webpage_home', FALSE); ?></h3>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover no-margin" id="list_table">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_webpage_meta_name', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_webpage_meta_desc', FALSE); ?></th>
                  <th><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> A</th>
                  <th><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> B</th>
                  <th width="10%"></th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($page_home)):?>
                <?php $n = 1; foreach($page_home as $item):?>
                  <tr>
                    <td><?php echo $n;?></td>
                    <td><?php echo ucwords(str_replace("_"," ", $item->country))?></td>
                    <td><?php echo $item->meta_title;?></td>
                    <td><?php echo $item->meta_description;?></td>
                    <td><?php echo $item->content_a;?></td>
                    <td><?php echo $item->content_b;?></td>
                    <td>
                      <button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit" data-id="<?php echo $item->id?>" data-country="<?php echo $item->country?>" data-meta_title="<?php echo $item->meta_title?>" data-meta_description="<?php echo $item->meta_description?>" data-name="<?php echo $item->name?>" data-content_a="<?php echo $item->content_a?>" data-content_b="<?php echo $item->content_b?>"><i class="fa fa-pencil-square-o"></i></button>
                      <button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" data-id="<?php echo $item->id?>"><i class="fa fa-trash-o"></i></button>
                    </td>
                  </tr>
                  <?php $n++; endforeach;?>
                <?php endif;?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div id="modal-add" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_webpage_create_webpage_setting', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_add" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/webpage_setting/add_home");?>">
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                    <select class="form-control select2" name="country" style="width: 100%;" >
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_meta_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="meta_title" />
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_meta_desc', FALSE); ?></label>
                    <input type="text" class="form-control" name="meta_description" />
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> A</label>
                    <textarea rows="4" class="form-control" name="content_a"></textarea>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> B</label>
                    <textarea rows="4" class="form-control" name="content_b"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>

          </div>
        </div>
      </div>

      <div id="modal-edit" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><?php echo $this->lang->line('admin_webpage_edit_webpage_setting', FALSE); ?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form role="form" id="form_edit" method="post" enctype="multipart/form-data" action="<?php echo base_url("sensha-admin/webpage_setting/edit_home");?>">
                <input type="hidden" id="edit_id" name="edit_id" />
                <div class="card-body">
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_product_cat_country', FALSE); ?></label>
                    <select class="form-control select2" name="country" style="width: 100%;" >
                      <option selected="selected" value="">-- Choose --</option>
                      <?php foreach($nation_lang as $item):?>
                        <option value="<?php echo $item->country;?>"><?php echo ucwords(str_replace("_"," ", $item->country))?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_meta_name', FALSE); ?></label>
                    <input type="text" class="form-control" name="meta_title" />
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_meta_desc', FALSE); ?></label>
                    <input type="text" class="form-control" name="meta_description" />
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> A</label>
                    <textarea rows="4" class="form-control" name="content_a"></textarea>
                  </div>
                  <div class="form-group">
                    <label><?php echo $this->lang->line('admin_webpage_content', FALSE); ?> B</label>
                    <textarea rows="4" class="form-control" name="content_b"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('global_submit', FALSE); ?></button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>



    </section>


    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.css")?>">
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/jquery.dataTables.js")?>"></script>
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/datatables/dataTables.bootstrap4.js")?>"></script>

    <!-- Select2 -->
    <script src="<?php echo base_url("assets/AdminLTE-3.0.0-alpha.2/plugins/select2/select2.full.min.js")?>"></script>

    <!-- ckeditor -->
    <!-- <script src="https://cdn.ckeditor.com/4.11.2/standard-all/ckeditor.js"></script> -->
    <!-- <script src="https://cdn.ckeditor.com/4.10.1/standard-all/ckeditor.js"></script> -->

    <script>
    function edit_item(t){
      $('#modal-edit input[name="meta_title"]').val($(t).data('meta_title'));
      $('#modal-edit input[name="meta_description"]').val($(t).data('meta_description'));
      $('#modal-edit textarea[name="content_a"]').val($(t).data('content_a'));
      $('#modal-edit textarea[name="content_b"]').val($(t).data('content_b'));
      //$('#modal-edit select[name="country"]').select2("val", $(t).data('country'));
      $('#modal-edit select[name="country"]').val($(t).data('country'));

      $('#edit_id').val($(t).data('id'));
      $('#modal-edit').modal();
    }

    function delete_item(t){
      Swal.fire({
        title: 'Are you sure to delete?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            method: "POST",
            url: '<?=base_url("sensha-admin/webpage_setting/delete_home/")?>'+$(t).data('id'),
          })
          .done(function( msg ) {
            // r = JSON.parse(msg);
            Swal.fire({
              title: 'Deleted!',
              text: 'Your data has been deleted.',
              type: 'success',
              showConfirmButton: false,
            })
            setTimeout(function(){ window.location = ''; }, 1400);
          })
          .fail(function( msg ) {
            Swal.fire({
              type: 'error',
              // text: msg.responseText,
              title: msg.statusText,
              // showConfirmButton: false
            });
          });
        }
      });
    }

    $(function(){
      $('#list_table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "pageLength": 30
      });

      //$('.select2').select2();

      $('#form_add, #form_edit').submit(function(){
          console.log($( this ).serialize())
        $.ajax({
          method: "POST",
          url: $(this).attr('action'),
          data: $( this ).serialize()
        })
        .done(function( msg ) {
          // console.log(msg);
          r = JSON.parse(msg);
          // console.log(r);
          if(r.error == 0){
            Swal.fire({
              type: 'success',
              title: 'success',
              showConfirmButton: false,
              allowOutsideClick:false
            });
            setTimeout(function(){ location.reload(); }, 1400);
          }
          else{
            Swal.fire(
              'Oops...',
              'Something went wrong!',
              'error'
            );
          }
        })
        .fail(function( msg ) {
          Swal.fire(
            'Oops...',
            msg,
            'error'
          );
        });
        return false;
      });
    });


  </script>
