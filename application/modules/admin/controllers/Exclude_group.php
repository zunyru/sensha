<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exclude_group extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    if($this->input->post('country')){
      $this->db->where('country', $this->input->post('country'));
    }
    $data['exclude_group'] = $this->datacontrol_model->getAllData('exclude_group');

    $data["content_view"] = 'admin/exclude_group/exclude_group_v';
    $data["menu"] = 'exclude_group';
    $data["htmlTitle"] = "Exclude Group";

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $data = array(
      'name' => $this->input->post('name', TRUE),
      'category' => $this->input->post('category', TRUE),
      'exclude_group_id' => $this->input->post('exclude_group_id', TRUE),
    );
    $affected_rows = $this->datacontrol_model->insert('exclude_group', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $data = array(
      'name' => $this->input->post('name', TRUE),
      'category' => $this->input->post('category', TRUE),
      'exclude_group_id' => $this->input->post('exclude_group_id', TRUE),
    );
    $affected_rows = $this->datacontrol_model->update('exclude_group', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('exclude_group', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
