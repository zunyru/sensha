<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account_setting extends CI_Controller
{
    var $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');
        $this->load->model('admin/account_setting_model', 'account_setting_m');

        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user = $this->ion_auth->user()->row();
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('create_by', $user->id)->or_where('id', $user->id);
        }
        $data['users'] = $this->ion_auth->users()->result();
        //$this->session->set_userdata('admin_sql_export', $this->db->last_query());

        $data['groups_list'] = $this->datacontrol_model->getAllData('groups');
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user->country);
        }
        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
        $data['exclude_group'] = $this->datacontrol_model->getAllData('exclude_group');

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user->country);
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('create_by', $user->id);
        }

        $data['user_aa'] = $this->datacontrol_model->getAllData('users');


        $data["content_view"] = 'admin/account_setting/account_setting_v';
        $data["menu"] = 'account_setting';
        $data["htmlTitle"] = "Nation & Lang";

        $this->load->view('admin_template', $data);
    }

    public function data_index()
    {
        $input = $this->input->post();

        $info = $this->account_setting_m->get_rows($input);
        $this->session->set_userdata('admin_sql_export', $this->db->last_query());
        $infoCount = $this->account_setting_m->get_count($input);

        foreach ($info->result() as $key => $item) {
            $column[$key]['DT_RowId'] = $key + 1;
            $column[$key]['email'] = $item->email;
            $column[$key]['id'] = $item->id;
            $column[$key]['country'] = ucwords(str_replace("_", " ", $item->country));
            $column[$key]['name'] = $this->ion_auth->get_users_groups_mode($item->id)->row()->name;;
            $column[$key]['company_name'] = $item->company_name;
            $column[$key]['company_phone'] = $item->company_phone;
            $column[$key]['discount_setting'] = $item->discount_setting;
            $column[$key]['comission_setting'] = $item->comission_setting;
            $column[$key]['action'] = '<button type="button" onclick="edit_item(this);" class="btn btn-warning btn-edit"
                                            data-id="' . $item->id . '"  data-email="' . $item->email . '" 
                                            data-first_name="' . $item->first_name . '" data-last_name="' . $item->last_name . '" 
                                            data-country="' . $item->country . '" 
                                            data-account_type="' . $this->ion_auth->get_users_groups($item->id)->row()->id . '" 
                                            data-parent_agent="' . $item->parent_agent . '" 
                                            data-company_name="' . $item->company_name . '" 
                                            data-company_addr="' . $item->company_addr . '" 
                                            data-company_phone="' . $item->company_phone . '" 
                                            data-tax_id="' . $item->tax_id . '" 
                                            data-contact_person="' . $item->contact_person . '" 
                                            data-shipment_name="' . $item->shipment_name . '" 
                                            data-shipment_addr="' . $item->shipment_addr . '" 
                                            data-shipment_phone="' . $item->shipment_phone . '" 
                                            data-zipcode="' . $item->zipcode . '" 
                                            data-discount_setting="' . $item->discount_setting . '" 
                                            data-comission_setting="' . $item->comission_setting . '" 
                                            data-exclude_group="' . $item->exclude_group . '" 
                                            data-create_by="' . $item->create_by . '" 
                                            data-update_by="' . $item->update_by . '" 
                                            data-create_date="' . $item->create_date . '" 
                                            data-update_date="' . $item->update_date . '" 
                                            data-user_id="' . $item->user_id . '"><i class="fa fa-pencil-square-o"></i></button>' .

                '<button type="button" onclick="delete_item(this);" class="btn btn-danger btn-delete" 
                                            data-id="' . $item->id . '" 
                                            data-email="' . $item->email . '">
                                            <i class="fa fa-trash-o"></i></button>';

        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));

    }

    public function getUserType($type)
    {
        $user = $this->ion_auth->user()->row();

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user->country);
        }

        $user_list = $this->ion_auth->users($type)->result();
        echo json_encode($user_list);
    }

    public function add()
    {
        $this->db->where('id', $this->input->post('account_type'));
        $query = $this->db->get('groups');
        $check_group = $query->row();

        if ($check_group->name == 'SA') {
            $sa_users = $this->ion_auth->users('SA')->result();
            foreach ($sa_users as $item) {
                if ($item->country == $this->input->post('country')) {
                    echo json_encode(array('error' => 3));
                    exit();
                }
            }
        }

        $admin_id = $this->ion_auth->user()->row()->id;
        $email = strtolower($this->input->post('email'));
        $identity_column = $this->config->item('identity', 'ion_auth');
        $identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
        $password = $this->input->post('password');

        $additional_data = array(
            'country' => $this->input->post('country'),
            'parent_agent' => $this->input->post('parent_agent'),
            'company_name' => $this->input->post('company_name'),
            'company_addr' => $this->input->post('company_addr'),
            'company_phone' => $this->input->post('company_phone'),
            // 'tax_id' => $this->input->post('tax_id'),
            'contact_person' => $this->input->post('contact_person'),
            // 'shipment_name' => $this->input->post('shipment_name'),
            // 'shipment_addr' => $this->input->post('shipment_addr'),
            // 'shipment_phone' => $this->input->post('shipment_phone'),
            'zipcode' => $this->input->post('zipcode'),
            'discount_setting' => $this->input->post('discount_setting'),
            'comission_setting' => $this->input->post('comission_setting'),
            //'exclude_group' => $this->input->post('exclude_group'),
            'create_by' => $admin_id,
            'update_by' => $admin_id,
            'create_date' => date('Y/m/d H:i:s'),
        );
        // echo "<pre>";
        // print_r($additional_data);
        // print_r($this->input->post());

        $group = array($this->input->post('account_type'));
        $is_create = $this->ion_auth->register($identity, $password, $email, $additional_data, $group);
        // echo $this->db->last_query();

        if ($is_create) {
            $uid = $is_create['id'];
            $code = $is_create['activation'];

            // $this->activate($uid, $code, false);
            $this->ion_auth->update($uid, array('active' => 1, 'activation_code' => ''));

            $data = array(
                'user_id' => $uid,
                'name' => $this->input->post('contact_person', TRUE),
                'company' => $this->input->post('company_name', TRUE),
                'address' => $this->input->post('company_addr', TRUE),
                'country' => $this->input->post('country', TRUE),
                'phone' => $this->input->post('company_phone', TRUE),
                'zipcode' => $this->input->post('zipcode', TRUE),
            );

            $this->db->where('user_id', $uid);
            $shipping_information = $this->datacontrol_model->getAllData('shipping_info');
            if ($shipping_information) {
                $this->datacontrol_model->update('shipping_info', $data, array('user_id' => $uid));
            } else {
                $this->datacontrol_model->insert('shipping_info', $data);
            }

            // Only allow updating groups if user is admin
            // if ($this->ion_auth->is_admin()){
            //   // Update the groups user belongs to
            //   $this->ion_auth->remove_from_group('', $uid);
            //   $this->ion_auth->add_to_group($this->input->post('account_type'), $uid);
            // }

            // $subject = "Registration Confirmation";
            // $mes = '<a href="'.base_url("auth/activate/$uid/$code").'" target="_blank">Activate Your account</a>';
            // mail_to(array($email => ''), $subject, $mes);
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1, 'msg' => $this->ion_auth->errors()));
        }

    }

    public function edit()
    {
        $this->db->where('id', $this->input->post('account_type'));
        $query = $this->db->get('groups');
        $check_group = $query->row();

        if ($check_group->name == 'SA') {
            if ($this->input->post('country') != $this->input->post('old_country')) {
                $sa_users = $this->ion_auth->users('SA')->result();
                foreach ($sa_users as $item) {
                    if ($item->country == $this->input->post('country')) {
                        echo json_encode(array('error' => 3));
                        exit();
                    }
                }
            }
        }

        $admin_id = $this->ion_auth->user()->row()->id;
        $data = array(
            // 'country' => $this->input->post('country'),
            'parent_agent' => $this->input->post('parent_agent'),
            'company_name' => $this->input->post('company_name'),
            'company_addr' => $this->input->post('company_addr'),
            'company_phone' => $this->input->post('company_phone'),
            'tax_id' => $this->input->post('tax_id'),
            'contact_person' => $this->input->post('contact_person'),
            'shipment_name' => $this->input->post('shipment_name'),
            'shipment_addr' => $this->input->post('shipment_addr'),
            'shipment_phone' => $this->input->post('shipment_phone'),
            'zipcode' => $this->input->post('zipcode'),
            'discount_setting' => $this->input->post('discount_setting'),
            'comission_setting' => $this->input->post('comission_setting'),
            // 'exclude_group' => $this->input->post('exclude_group'),
            'update_by' => $admin_id,
        );
        if ($this->input->post('password') != '') {
            $data['password'] = $this->input->post('password');
        }

        // print_r($this->input->post());

        // print_r($data);

        // exit();

        $this->ion_auth->update($this->input->post('edit_id', TRUE), $data);

        if ($this->input->post('account_type') != '') {
            // Update the groups user belongs to
            $this->ion_auth->remove_from_group('', $this->input->post('edit_id', TRUE));
            $this->ion_auth->add_to_group($this->input->post('account_type'), $this->input->post('edit_id', TRUE));
        }

        $data = array(
            'user_id' => $this->input->post('edit_id'),
            'name' => $this->input->post('contact_person', TRUE),
            'company' => $this->input->post('company_name', TRUE),
            'address' => $this->input->post('company_addr', TRUE),
            'country' => $this->input->post('country', TRUE),
            'phone' => $this->input->post('company_phone', TRUE),
            'zipcode' => $this->input->post('zipcode', TRUE),
        );

        $this->db->where('user_id', $this->input->post('edit_id'));
        $shipping_information = $this->datacontrol_model->getAllData('shipping_info');
        if ($shipping_information) {
            $this->datacontrol_model->update('shipping_info', $data, array('user_id' => $this->input->post('edit_id')));
        } else {
            $this->datacontrol_model->insert('shipping_info', $data);
        }


        echo json_encode(array('error' => 0));

        // $affected_rows = $this->datacontrol_model->update('users', $data, array('id'=>$this->input->post('edit_id', TRUE)));
        // if($affected_rows > 0){
        //   echo json_encode(array('error' => 0));
        // }
        // else{
        //   echo json_encode(array('error' => 1));
        // }
    }

    // public function delete($id){
    //   $this->datacontrol_model->delete('discount_coupon', array('id'=>$id));
    // }


    // public function userDeactivate(){
    //   $this->ion_auth->deactivate(19);
    // }


    public function delete($id)
    {
        // $this->ion_auth->delete_user($id)
        // $affected_rows = $this->datacontrol_model->delete('cluster1', array('id'=>$id));
        if ($this->ion_auth->delete_user($id)) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function export()
    {
        // $account_setting = $this->datacontrol_model->getAllData('users');
        $selected = 'username, active, country, parent_agent, company_name, company_addr, company_phone, tax_id, zipcode, discount_setting, comission_setting, create_date	update_date';
        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export'));


        $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
        //$new_query =  $query_limit." LIMIT 15";

        $query = $this->db->query($query_none_limit);
        //echo $query->num_rows(); exit();
        $count_row_limit = 3000;
        $files_csv = [];
        if ($query->num_rows() <= 3000) {
            $export = $query->result();
            array_push($files_csv,$this->new_export($export,0,"General"));
        } else {
            $new_query = [];
            $newquery = [];
            $export = [];
            $file_number = 0;
            for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
                $file_number++;
                $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
                $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

                $newquery[$i] = $this->db->query($new_query[$i]);
                $export[$i] = $newquery[$i]->result();

                array_push($files_csv,$this->new_export($export[$i],$file_number));

            }
        }

        $data["files_csv"] = $files_csv;
        $this->session->set_userdata('account_setting_download', $data);

        redirect('admin/account_setting/download');
    }

    public function new_export($data,$file_number = 0)
    {
        $table_columns = array();
        foreach ($data[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        // echo date('H:i:s') , " Set document properties" , EOL;
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Account Setting" . date('Y-m-d'))
            ->setSubject("Account Setting" . date('Y-m-d'))
            ->setDescription("Export Account Setting")
            ->setKeywords("Export Account Setting")
            ->setCategory("Export Account Setting");

        // Add some data
        // echo date('H:i:s') , " Add some data" , EOL;
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        // print_r($product_management[0]->id);

        for ($i = 0; $i < count($data); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet
        // echo date('H:i:s') , " Rename worksheet" , EOL;
        $this->excel->getActiveSheet()->setTitle('Export Account Setting' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');
        if($file_number == 0){
            $newFileName = "Export-Account-Setting" . "_" . date('Y-m-d') . '_' . time(). '.csv';
        }else{
            $newFileName = "Export-Account-Setting" . "_" . date('Y-m-d') . '_' . time(). "(".$file_number.")" . '.csv';
        }

       /* // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $newFileName . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0*/


        if (!is_dir('uploads/'.'export/')) {
            mkdir('./uploads/export/', 0777, TRUE);

        }
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save("uploads/export/".$newFileName);
        //$objWriter->save('php://output');
        return "uploads/export/".$newFileName;

    }

    public function download()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'account_setting';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("account_setting_download");
        $this->load->view('admin_template', $data);
    }

}
