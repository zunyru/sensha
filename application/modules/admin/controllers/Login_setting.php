<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_setting extends CI_Controller
{
    var $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');

        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $data['page_home'] = $this->datacontrol_model->getAllData('page_home');
        $data['lock'] = $this->datacontrol_model->getAllData('lock_seeting');

        $data["content_view"] = 'admin/login_setting/home_v';
        $data["menu"] = 'login_setting';
        $data["htmlTitle"] = "Login Setting";

        $this->load->view('admin_template', $data);
    }

    public function locked()
    {
        $lock = $this->input->post('lock');
        if($lock =='true'){
            $lock = true;
            $g_lock = false;
        }else{
            $lock = false;
            $g_lock = true;
        }
        $this->db->set('active', $lock);
        $this->db->where('id', 1);
        $this->db->update('lock_seeting');


        $this->db->set('active', $g_lock);
        $this->db->where('id !=', 1);
        $this->db->update('groups');

    }
}
