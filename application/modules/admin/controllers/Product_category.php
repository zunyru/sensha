<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_category extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){

    // $data['product_category'] = $this->datacontrol_model->getAllData('product_category');

    $data["content_view"] = 'admin/product_category/product_category_v';
    $data["menu"] = 'product_category';
    $data["htmlTitle"] = "Product Category";

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $data = array(
      '' => $this->input->post('', TRUE),
    );
    $affected_rows = $this->datacontrol_model->insert($table, $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $data = array(
      '' => $this->input->post('', TRUE),
    );
    $affected_rows = $this->datacontrol_model->update($table, $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $affected_rows = $this->datacontrol_model->delete($table, array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
