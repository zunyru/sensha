<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster2 extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');
    $this->load->model('admin/cluster2_model','cluster2_m');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    $user_country = $this->ion_auth->user()->row()->country;

    // $this->db->where('country', $user_country);
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');
    $data['cluster1_add'] =  $this->db->get_where('cluster1', array('country' => 'Global'))->result();

    $this->db->select('c2.id, c2.country, c2.cluster_name, c2.level1, c2.cluster_type, c1.cluster_name as c1_name, c2.html_content,c2.cluster_url');
    $this->db->from('cluster2 c2');
    $this->db->join('cluster1 c1', 'c2.level1 = c1.id', 'inner');
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('c2.country', $user_country);
    }
    if($this->input->post('country')){
      $this->db->where('c2.country', $this->input->post('country'));
    }

    $query = $this->db->get('');
    $data['cluster2'] = $query->result();

    $data["content_view"] = 'admin/cluster2/cluster2_v';
    $data["menu"] = 'product_category';
    $data["htmlTitle"] = "2nd Cluster";

    $this->load->view('admin_template', $data);
  }

    public function data_index()
    {
        $input = $this->input->post();
        $info = $this->cluster2_m->get_rows($input);
        $infoCount = $this->cluster2_m->get_count($input);

        foreach ($info->result() as $key => $item) {

            $action =  "<button type=\"button\" onclick=\"edit_item(this);\" class=\"btn btn-warning btn-edit\" 
                        data-id=\"{$item->id}\" 
                        data-cluster_name=\"{$item->cluster_name}\" 
                        data-country=\"{$item->country}\" 
                        data-html_content=\"{$item->html_content}\" 
                        data-cluster_type=\"{$item->cluster_type}\">
                        <i class=\"fa fa-pencil-square-o\"></i></button>";
            if($item->country == 'Global') {
                $action.= "<button type=\"button\" onclick=\"delete_item(this);\" class=\"btn btn-danger btn-delete\" 
                        data-id=\"{$item->id}\" data-cluster_name=\"{$item->cluster_name}\">
                        <i class=\"fa fa-trash-o\"></i></button>";
            }

            $column[$key]['id'] = $item->id;
            $column[$key]['cluster_url'] = $item->cluster_url;
            $column[$key]['cluster_name'] = $item->cluster_name;
            $column[$key]['country'] = ucwords(str_replace("_"," ", $item->country));
            $column[$key]['cluster_type'] = $item->cluster_type;
            $column[$key]['c1_name'] = $item->c1_name;
            $column[$key]['action'] = $action;
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

  public function add(){
    $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
    $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);

    $country = $this->datacontrol_model->getAllData('nation_lang');
    foreach($country as $item){
       $cluster1 =  $this->datacontrol_model->getRowData('cluster1',['id'=>$this->input->post('level1', TRUE)]);

       $cluster1All =  $this->datacontrol_model->getRowData('cluster1',
          [
            'cluster_url'=> $cluster1->cluster_url,
            'country' => $item->country,
          ]
        );

       $data = array(
      // 'country' => $this->input->post('country', TRUE),
      'country' => 'Global',
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'level1' => $cluster1All->id,
      'cluster_type' => $this->input->post('cluster_type', TRUE),
      'cluster_url'  => $cluster_url,
      'html_content' => htmlspecialchars($this->input->post('html_content')),
    );

      $data['country'] = $item->country;
      $affected_rows = $this->datacontrol_model->insert('cluster2', $data);
    }
    // $affected_rows = $this->datacontrol_model->insert('cluster2', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
  
   $data = $this->getCountryCluster1($this->input->post('level1', TRUE),$this->input->post());
 
   $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
   $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
   if(!empty($this->input->post('cluster_type', TRUE) && !empty($this->input->post('level1', TRUE)))){
    $data = array(
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'level1' => $this->input->post('level1', TRUE),
      'cluster_type' => $this->input->post('cluster_type', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content')),
    );
  }else if(!empty($this->input->post('cluster_type', TRUE))){
    $data = array(
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'cluster_type' => $this->input->post('cluster_type', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content')),
    );
  }elseif(!empty($this->input->post('level1', TRUE))){
     $data = array(
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'level1' => $this->input->post('level1', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content')),
    );
   }else{
      $data = array(
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content'))
     );
   }
    $affected_rows = $this->datacontrol_model->update('cluster2', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function getCountryCluster1($id,$input){

    $cluster1 =  $this->datacontrol_model->getRowData('cluster1',['id'=>$id]);

    if($cluster1->country == 'Global'){

       $cluster1->cluster_url;

       $cluster2 =  $this->datacontrol_model->getRowData('cluster2',['cluster_name'=> $input['cluster_name']]);
       

      $cluster2_all=  $this->db->get_where('cluster2', array('cluster_url' => $cluster2->cluster_url))->result();
    
      foreach ( $cluster2_all as $item) {
        $cluster1All =  $this->datacontrol_model->getRowData('cluster1',
          [
            'cluster_url'=> $cluster1->cluster_url,
            'country' => $item->country,
          ]
        );
        $id_cluster1 = $cluster1All->id;
 
        //update 
        $this->db->set('level1', $id_cluster1);
        $this->db->where('id', $item->id);
        $this->db->update('cluster2');
      } 
      return true;
    }else{
      return true;
    }
  }

 /* public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('cluster2', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }*/
 
  public function delete($id){
    $rows = $this->datacontrol_model->getRowData('cluster2', array('id'=>$id));

    $affected_rows = $this->datacontrol_model->delete('cluster2', array('cluster_url'=>$rows->cluster_url));

    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function editorUploadImage(){
    if (!file_exists('uploads/cluster2/')) {
      mkdir("uploads/cluster2/", 0777);
    }

    $tempName = $_FILES['upload']['tmp_name'];
    $fileName = uniqid() . $_FILES['upload']['name'];
    $uploadPath = 'uploads/cluster2/' . $fileName;
    $imageUrl = base_url("uploads/cluster2/") . $fileName;

    $success = move_uploaded_file($tempName, $uploadPath);
    echo json_encode([
      'uploaded' => $success,
      'fileName' => $fileName,
      'url' => $imageUrl,
    ]);
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
