<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster3 extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $user_country = $this->ion_auth->user()->row()->country;
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    $this->db->where('country', $user_country);
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');

    $this->db->where('country', $user_country);
    $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');

    $this->db->select('c3.id, c3.country, c3.cluster_name, c3.level1, c3.level2, c3.cluster_type, c2.cluster_name as c2_name');
    $this->db->from('cluster3 c3');
    $this->db->join('cluster2 c2', 'c3.level2 = c2.id', 'inner');
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('c3.country', $user_country);
    }
    if($this->input->post('country')){
      $this->db->where('country', $this->input->post('country'));
    }
    if($this->input->post('country')){
      $this->db->where('c3.country', $this->input->post('country'));
    }
    $query = $this->db->get();
    $data['cluster3'] = $query->result();

    $data["content_view"] = 'admin/cluster3/cluster3_v';
    $data["menu"] = 'product_category';
    $data["htmlTitle"] = "3nd Cluster";

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
    $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
    $data = array(
      // 'country' => $this->input->post('country', TRUE),
      'country' => 'Global',
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'level1' => $this->input->post('level1', TRUE),
      'level2' => $this->input->post('level2', TRUE),
      'cluster_type' => $this->input->post('cluster_type', TRUE),
      'cluster_url' => $cluster_url,
    );

    $country = $this->datacontrol_model->getAllData('nation_lang');
    foreach($country as $item){
      $data['country'] = $item->country;
      $affected_rows = $this->datacontrol_model->insert('cluster3', $data);
    }
    // $affected_rows = $this->datacontrol_model->insert('cluster3', $data);

    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
    $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
    $data = array(
      // 'country' => $this->input->post('country', TRUE),
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'level1' => $this->input->post('level1', TRUE),
      'level2' => $this->input->post('level2', TRUE),
      'cluster_type' => $this->input->post('cluster_type', TRUE),
      'cluster_url' => $cluster_url,
    );
    $affected_rows = $this->datacontrol_model->update('cluster3', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('cluster3', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }




  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
