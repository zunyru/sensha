<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_post extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $user_country = $this->ion_auth->user()->row()->country;
     if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('country', $user_country);
    }   
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    if($this->input->post('country')){
      $this->db->where('country', $this->input->post('country'));
    }
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('country', $user_country);
    }
    $data['news_post'] = $this->datacontrol_model->getAllData('news_post');

    $data["content_view"] = 'admin/news_post/news_post_v';
    $data["menu"] = 'news_post';
    $data["htmlTitle"] = "News Post";

    $this->load->view('admin_template', $data);
  }

  public function add(){
    $data = array(
      'country' => $this->input->post('country', TRUE),
      'title' => $this->input->post('title', TRUE),
      'content' => htmlspecialchars($this->input->post('content', TRUE)),
    );
    $affected_rows = $this->datacontrol_model->insert('news_post', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $data = array(
      'country' => $this->input->post('country', TRUE),
      'title' => $this->input->post('title', TRUE),
      'content' => htmlspecialchars($this->input->post('content', TRUE)),
    );
    $affected_rows = $this->datacontrol_model->update('news_post', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('news_post', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function editorUploadImage(){
    if (!file_exists('uploads/news_post/')) {
      mkdir("uploads/news_post/", 0777);
    }

    $tempName = $_FILES['upload']['tmp_name'];
    $fileName = uniqid() . $_FILES['upload']['name'];
    $uploadPath = 'uploads/news_post/' . $fileName;
    $imageUrl = base_url("uploads/news_post/") . $fileName;

    $success = move_uploaded_file($tempName, $uploadPath);
    echo json_encode([
      'uploaded' => $success,
      'fileName' => $fileName,
      'url' => $imageUrl,
    ]);
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
