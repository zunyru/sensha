<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nation_lang extends CI_Controller
{
    var $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');
        $this->load->model('admin/nation_lang_model','nation_lang_m');

        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        $this->db->where('name not in (select country from nation_lang)');
        $data['countries_cut'] = $this->datacontrol_model->getAllDataWhere('countries', ['zone is NOT NULL' => NULL]);


        $data["content_view"] = 'admin/nation_lang/nation_lang_v';
        $data["menu"] = 'nation_lang';
        $data["htmlTitle"] = "Nation & Lang";

        $this->load->view('admin_template', $data);
    }

    public function add()
    {
        $this->db->where('country', $this->input->post('country'));
        $query = $this->db->get('nation_lang');
        $check_group = $query->row();
        if (count($check_group) > 0) {
            echo json_encode(array('error' => 3));
            exit();
        }

        if (!file_exists('uploads/lang/')) {
            mkdir("uploads/lang/", 0777);
        }

        if (!file_exists('uploads/country_flag/')) {
            mkdir("uploads/country_flag/", 0777);
        }
        mkdir(APPPATH . "language/" . strtolower($this->input->post('country')), 0755);
        copy(APPPATH . "language/global/set_lang.php", APPPATH . "language/" . strtolower($this->input->post('country')) . "/set_lang.php");

        $country_flag = '';

        if (!empty($_FILES['country_flag']['name'])) {
            $config['upload_path'] = 'uploads/country_flag/';
            $config['allowed_types'] = 'gif|jpg|png';
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            if (!$this->upload->do_upload('country_flag')) {
                echo $error = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $country_flag = $data['file_name'];
            }
        }

        $data = array();
        if (!empty($_FILES['file_lang']['name'])) {
            // Set preference
            $config['upload_path'] = 'uploads/lang/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '1000'; // max_size in kb
            // $config['file_name'] = $_FILES['file_lang']['name'];
            $config['file_name'] = $this->ion_auth->user()->row()->id . "_" . date("Ymdhis") . ".csv";


            // Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            // File upload
            if ($this->upload->do_upload('file_lang')) {
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];

                // Reading file
                $file = fopen("uploads/lang/" . $filename, "r");
                $i = 1;

                $importData_arr = array();
                $text = "";
                $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {

                    if ($i > 1) {
                        // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';
                        $data = array(
                            'key' => $filedata[0],
                            'language' => $filedata[1],
                            'set' => $filedata[2],
                            'text' => $filedata[3],
                        );
                        $this->datacontrol_model->insert('language', $data);
                    }
                    $i++;
                }
                fclose($file);
                unlink("uploads/lang/" . $filename);
            } else {
                echo $this->upload->display_errors();
            }

        } else {

        }

        $affected_rows = $this->datacontrol_model->insert('nation_lang', array(
            'country' => $this->input->post('country', TRUE),
            'language' => $this->input->post('language', TRUE),
            'country_flag' => $country_flag,
            'font' => $this->input->post('font', TRUE),
            'reading' => $this->input->post('reading', TRUE),
            'currency_name' => $this->input->post('currency_name', TRUE),
            'currency_rate_exchange' => $this->input->post('currency_rate_exchange', TRUE),
            'vat' => $this->input->post('vat', TRUE),
            'is_active' => ($this->input->post('is_active') == 1) ? 1 : 0,
        ));

        if ($affected_rows > 0) {
            $this->create_webpage();
            $this->create_cluster();
            $this->create_product();
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }

    }

    public function edit()
    {
        if ($this->input->post('country') != $this->input->post('old_country')) {
            $this->db->where('country', $this->input->post('country'));
            $query = $this->db->get('nation_lang');
            $check_group = $query->row();
            if (count($check_group) > 0) {
                echo json_encode(array('error' => 3));
                exit();
            }
        }

        if (!file_exists('uploads/lang/')) {
            mkdir("uploads/lang/", 0777);
        }

        if (!file_exists('uploads/country_flag/')) {
            mkdir("uploads/country_flag/", 0777);
        }

        $country_flag = '';

        if (!empty($_FILES['country_flag']['name'])) {
            $config['upload_path'] = 'uploads/country_flag/';
            $config['allowed_types'] = 'gif|jpg|png';
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            if (!$this->upload->do_upload('country_flag')) {
                echo $error = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $country_flag = $data['file_name'];
            }
        }

        $data = array();
        if (!empty($_FILES['file_lang']['name'])) {
            // Set preference
            $config['upload_path'] = 'uploads/lang/';
            $config['allowed_types'] = 'csv';
            $config['max_size'] = '1000'; // max_size in kb
            // $config['file_name'] = $_FILES['file_lang']['name'];
            $config['file_name'] = $this->ion_auth->user()->row()->id . "_" . date("Ymdhis") . ".csv";


            // Load upload library
            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            // File upload
            if ($this->upload->do_upload('file_lang')) {
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];

                // Reading file
                $file = fopen("uploads/lang/" . $filename, "r");
                $i = 1;

                $importData_arr = array();
                $text = "";
                $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {

                    if ($i > 1) {
                        // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';
                        $data = array(
                            'key' => $filedata[0],
                            'language' => $filedata[1],
                            'set' => $filedata[2],
                            'text' => $filedata[3],
                        );
                        $this->datacontrol_model->insert('language', $data);
                    }
                    $i++;
                }
                fclose($file);
                unlink("uploads/lang/" . $filename);
            } else {
                echo $this->upload->display_errors();
            }

        } else {

        }
        if (!empty($this->input->post('country', TRUE))) {
            $edit_data = array(
                'country' => $this->input->post('country', TRUE),
                'language' => $this->input->post('language', TRUE),
                'font' => $this->input->post('font', TRUE),
                'reading' => $this->input->post('reading', TRUE),
                'currency_name' => $this->input->post('currency_name', TRUE),
                'currency_rate_exchange' => $this->input->post('currency_rate_exchange', TRUE),
                'vat' => $this->input->post('vat', TRUE),
                'is_active' => ($this->input->post('is_active') == 1) ? 1 : 0,
            );
        } else {
            $edit_data = array(
                'language' => $this->input->post('language', TRUE),
                'font' => $this->input->post('font', TRUE),
                'reading' => $this->input->post('reading', TRUE),
                'currency_name' => $this->input->post('currency_name', TRUE),
                'currency_rate_exchange' => $this->input->post('currency_rate_exchange', TRUE),
                'vat' => $this->input->post('vat', TRUE),
                'is_active' => ($this->input->post('is_active') == 1) ? 1 : 0,
            );
        }
        if ($country_flag != '') {
            $edit_data['country_flag'] = $country_flag;
        }

        $affected_rows = $this->datacontrol_model->update('nation_lang', $edit_data, array('id' => $this->input->post('edit_id')));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        }
        // redirect("admin/nation_lang", "refresh");

    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('nation_lang');
        $country = $query->row()->country;
        $is_delete = true;

        $this->db->where('country', $country);
        $query = $this->db->get('cluster1');
        $result = $query->result();
        if (count($result) > 0) {
            $is_delete = false;
        }

        $this->db->where('country', $country);
        $query = $this->db->get('cluster2');
        $result = $query->result();
        if (count($result) > 0) {
            $is_delete = false;
        }

        $this->db->where('product_country', $country);
        $query = $this->db->get('product_general');
        $result = $query->result();
        if (count($result) > 0) {
            $is_delete = false;
        }

        $this->db->where('product_country', $country);
        $query = $this->db->get('product_ppf');
        $result = $query->result();
        if (count($result) > 0) {
            $is_delete = false;
        }

        if ($is_delete) {
            $affected_rows = $this->datacontrol_model->delete('nation_lang', array('id' => $id));
            if ($affected_rows > 0) {
                echo json_encode(array('error' => 0));
            } else {
                echo json_encode(array('error' => 1));
            }
        } else {
            echo json_encode(array('error' => 3));
        }

    }

    function create_webpage()
    {
        $this->db->where('country', 'Global');
        $query = $this->db->get('webpage_setting');
        $page = $query->result();
        // $page_list = array(
        //   'About SENSHA',
        //   'About PPF',
        //   'Sole agent wanted',
        //   'Wolrd network',
        //   'Term of use',
        //   'Privacy policy',
        //   'Contact'
        // );
        foreach ($page as $item) {
            $data = array(
                'name' => $item->name,
                'country' => $this->input->post('country', TRUE),
            );
            $this->datacontrol_model->insert('webpage_setting', $data);
        }

    }

    function create_cluster()
    {
        $this->db->where('country', 'Global');
        $cluster1 = $this->datacontrol_model->getAllData('cluster1');
        foreach ($cluster1 as $item) {
            $cluster_url = str_replace(' ', '-', trim($item->cluster_name));
            $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
            $data = array(
                'country' => $this->input->post('country', TRUE),
                'cluster_type' => $item->cluster_type,
                'cluster_name' => $item->cluster_name,
                'cluster_url' => $cluster_url,
            );
            $this->datacontrol_model->insert('cluster1', $data);
        }

        $this->db->where('country', 'Global');
        $cluster2 = $this->datacontrol_model->getAllData('cluster2');
        foreach ($cluster2 as $item) {
            $cluster_url = str_replace(' ', '-', trim($item->cluster_name));
            $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
            $this->db->where('id', $item->level1);
            $data_lv1 = $this->datacontrol_model->getRowData('cluster1');
            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $data_lv1->cluster_name);
            $lv1 = $this->datacontrol_model->getRowData('cluster1');
            $data = array(
                'country' => $this->input->post('country', TRUE),
                'cluster_name' => $item->cluster_name,
                'level1' => $lv1->id,
                'cluster_type' => $item->cluster_type,
                'cluster_url' => $cluster_url,
            );
            $this->datacontrol_model->insert('cluster2', $data);
        }

        $this->db->where('country', 'Global');
        $cluster3 = $this->datacontrol_model->getAllData('cluster3');
        foreach ($cluster3 as $item) {
            $cluster_url = str_replace(' ', '-', trim($item->cluster_name));
            $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
            $this->db->where('id', $item->level1);
            $data_lv1 = $this->datacontrol_model->getRowData('cluster1');
            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $data_lv1->cluster_name);
            $lv1 = $this->datacontrol_model->getRowData('cluster1');
            $this->db->where('id', $item->level2);
            $data_lv2 = $this->datacontrol_model->getRowData('cluster2');
            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $data_lv2->cluster_name);
            $lv2 = $this->datacontrol_model->getRowData('cluster2');
            $data = array(
                'country' => $this->input->post('country', TRUE),
                'cluster_name' => $item->cluster_name,
                'level1' => $lv1->id,
                'level2' => $lv2->id,
                'cluster_type' => $item->cluster_type,
                'cluster_url' => $cluster_url,
            );
            $this->datacontrol_model->insert('cluster3', $data);
        }


    }

    function create_product()
    {
        $this->db->where('product_country', 'Global');
        $product_general = $this->datacontrol_model->getAllData('product_general');
        foreach ($product_general as $item) {
            $this->db->where('id', $item->cat_1);
            $cat = $this->datacontrol_model->getRowData('cluster1');

            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $cat->cluster_name);
            $cat_1 = $this->datacontrol_model->getRowData('cluster1');


            $this->db->where('id', $item->cat_2);
            $cat = $this->datacontrol_model->getRowData('cluster2');

            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $cat->cluster_name);
            $cat_2 = $this->datacontrol_model->getRowData('cluster2');


            $this->db->where('id', $item->cat_3);
            $cat = $this->datacontrol_model->getRowData('cluster3');

            $this->db->where('country', $this->input->post('country', TRUE));
            $this->db->where('cluster_name', $cat->cluster_name);
            $cat_3 = $this->datacontrol_model->getRowData('cluster3');

            $data = array(
                'product_id' => $item->product_id,
                'product_name' => $item->product_name,
                'cat_1' => $cat_1->id,
                'cat_2' => $cat_2->id,
                'cat_3' => $cat_3->id,
                'product_country' => $this->input->post('country', TRUE),
                'global_price' => $item->global_price,
                'fixed_price' => $item->fixed_price,
                'sa_price' => $item->sa_price,
                'import_shipping' => $item->import_shipping,
                'import_tax' => $item->import_tax,
                'no_of_use' => $item->no_of_use,
                'cost_per_use' => $item->cost_per_use,
                'fixed_delivery_price' => $item->fixed_delivery_price,
                'material' => $item->material,
                'usage' => $item->usage,
                'item_type' => $item->item_type,
                'feature' => $item->feature,
                'no_of_stock' => $item->no_of_stock,
                'item_description' => $item->item_description,
                'caution' => $item->caution,
                'content' => $item->content,
                'html_content' => $item->html_content,
                'youtube' => $item->youtube,
                'unpurchasable_user' => $item->unpurchasable_user,
                'is_active' => 0,
                'image' => $item->image,
                'product_weight' => $item->product_weight,
            );

            $this->datacontrol_model->insert('product_general', $data);
        }

        $this->db->where('product_country', 'Global');
        $product_ppf = $this->datacontrol_model->getAllData('product_ppf');
        foreach ($product_ppf as $item) {
            $data = array(
                'product_id' => $item->product_id,
                'product_name' => $item->product_name,
                'cat_1' => $item->cat_1,
                'cat_2' => $item->cat_2,
                'cat_3' => $item->cat_3,
                'product_country' => $this->input->post('country', TRUE),
                'global_price' => $item->global_price,
                'fixed_price' => $item->fixed_price,
                'sa_price' => $item->sa_price,
                'import_shipping' => $item->import_shipping,
                'import_tax' => $item->import_tax,
                'no_of_use' => $item->no_of_use,
                'cost_per_use' => $item->cost_per_use,
                'fixed_delivery_price' => $item->fixed_delivery_price,
                'parts' => $item->parts,
                'feature' => $item->feature,
                'no_of_stock' => $item->no_of_stock,
                'item_description' => $item->item_description,
                'car_production_id' => $item->car_production_id,
                'begining_term_to_use' => $item->begining_term_to_use,
                'ending_term_to_use' => $item->ending_term_to_use,
                'caution' => $item->caution,
                'content' => $item->content,
                'html_content' => $item->html_content,
                'youtube' => $item->youtube,
                'unpurchasable_user' => $item->unpurchasable_user,
                'is_active' => $item->is_active,
                'image' => $item->image,
                'product_weight' => $item->product_weight,
            );

            $this->datacontrol_model->insert('product_ppf', $data);
        }

    }

    function test()
    {
        $page_list = array(
            'About SENSHA',
            'About PPF',
            'Sole agent wanted',
            'Wolrd network',
            'Term of use',
            'Privacy policy',
            'Contact'
        );
        foreach ($page_list as $item) {
            $data = array(
                'name' => $item,
                'country' => 'Japan',
            );
            $this->datacontrol_model->insert('webpage_setting', $data);
        }
    }

    public function data_index()
    {
        $input = $this->input->post();
        $info = $this->nation_lang_m->get_rows($input);
        $infoCount = $this->nation_lang_m->get_count($input);

        foreach ($info->result() as $key => $item) {
            $column[$key]['id'] = $key+1;
            $column[$key]['country'] = ucwords(str_replace("_", " ", $item->country));
            $column[$key]['language'] = $item->language;
            $column[$key]['currency_name'] = $item->currency_name;
            $column[$key]['vat'] = $item->vat;
            if($item->nicename == ''){
                $country_nicename = 'Global';
            }else{
                $country_nicename = $item->nicename;
            }
            $column[$key]['action'] = '<button type="button" onclick="edit_item(this)"
                                    class="btn btn-warning btn-edit" data-id="'.$item->id.'"
                                    data-country="'.$country_nicename.'"
                                    data-language="'.$item->language.'"
                                    data-font="'.$item->font.'"
                                    data-reading="'.$item->reading.'"
                                    data-currency_name="'.$item->currency_name.'"
                                    data-currency_rate_exchange="'.$item->currency_rate_exchange.'"
                                    data-vat="'.$item->vat.'"
                                    data-is_active="'.$item->is_active.'"><i
                                    class="fa fa-pencil-square-o"></i></button>'.

                                    '<button type="button" onclick="delete_item(this);"
                                    class="btn btn-danger btn-delete" data-id="'.$item->id.'"
                                    data-country="'.$item->country.'"><i
                                    class="fa fa-trash-o"></i></button>';
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));

    }


}
