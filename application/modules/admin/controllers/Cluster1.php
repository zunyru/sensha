<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster1 extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');
    $this->load->model('admin/cluster1_model','cluster1_m');


    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    if($this->ion_auth->in_group(array('SA'))){
      $user_country = $this->ion_auth->user()->row()->country;
      $this->db->where('country', $user_country);
    }
    if($this->input->post('country')){
      $this->db->where('country', $this->input->post('country'));
    }
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');

    $data["content_view"] = 'admin/cluster1/cluster1_v';
    $data["menu"] = 'product_category';
    $data["htmlTitle"] = "1st Cluster";

    $this->load->view('admin_template', $data);
  }

  public function data_index()
  {
      $input = $this->input->post();
      $info = $this->cluster1_m->get_rows($input);
      $infoCount = $this->cluster1_m->get_count($input);

      foreach ($info->result() as $key => $item) {

          $action =  "<button type=\"button\" onclick=\"edit_item(this);\" class=\"btn btn-warning btn-edit\" 
                        data-id=\"{$item->id}\" 
                        data-cluster_name=\"{$item->cluster_name}\" 
                        data-country=\"{$item->country}\" 
                        data-cluster_type=\"{$item->cluster_type}\"><i class=\"fa fa-pencil-square-o\"></i></button>";
          if($item->country == 'Global') {
              $action.= "<button type=\"button\" onclick=\"delete_item(this);\" class=\"btn btn-danger btn-delete\" 
                        data-id=\"{$item->id}\" data-cluster_name=\"{$item->cluster_name}\">
                        <i class=\"fa fa-trash-o\"></i></button>";
          }

          $column[$key]['id'] = $item->id;
          $column[$key]['cluster_url'] = $item->cluster_url;
          $column[$key]['cluster_name'] = $item->cluster_name;
          $column[$key]['country'] = ucwords(str_replace("_"," ", $item->country));
          $column[$key]['cluster_type'] = $item->cluster_type;
          $column[$key]['action'] = $action;
      }

      $data['data'] = $column ?? [];
      $data['recordsTotal'] = $info->num_rows();
      $data['recordsFiltered'] = $infoCount;
      $data['draw'] = $input['draw'];
      $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($data));
  }

  public function add(){
    $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
    $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
    $data = array(
      // 'country' => $this->input->post('country', TRUE),
      'country' => 'Global',
      'cluster_name' => $this->input->post('cluster_name', TRUE),
      'cluster_url' => $cluster_url,
      'cluster_type' => $this->input->post('cluster_type', TRUE),
    );
    $country = $this->datacontrol_model->getAllData('nation_lang');
    foreach($country as $item){
      $data['country'] = $item->country;
      $affected_rows = $this->datacontrol_model->insert('cluster1', $data);
    }
    // $affected_rows = $this->datacontrol_model->insert('cluster1', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit(){
    $cluster_url = str_replace(' ', '-', trim($this->input->post('cluster_name', TRUE)));
    $cluster_url = preg_replace("/[^a-z-\d\p{L}\p{N}\p{Katakana}\p{Hiragana}\p{Han}]/iu", '', $cluster_url);
    if(!empty($this->input->post('cluster_type', TRUE))){
      $data = array(
        // 'country' => $this->input->post('country', TRUE),
        'cluster_name' => $this->input->post('cluster_name', TRUE),
        //'cluster_url' => $cluster_url,
        'cluster_type' => $this->input->post('cluster_type', TRUE),
      );
    }else{
      $data = array('cluster_name' => $this->input->post('cluster_name', TRUE));
    }
    if($this->ion_auth->in_group(array('SA'))){
      $data['cluster_new_name'] = $this->input->post('cluster_new_name', TRUE);
      $affected_rows = $this->datacontrol_model->update('cluster1', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    }
    else{
      $affected_rows = $this->datacontrol_model->update('cluster1', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    }


    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  /*public function delete($id){
    $affected_rows = $this->datacontrol_model->delete('cluster1', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }*/

  public function delete($id){
    $affected_rows = $this->datacontrol_model->getRowData('cluster2', array('level1'=>$id));

    if(empty($affected_rows)){
      $rows = $this->datacontrol_model->getRowData('cluster1', array('id'=>$id));
      $affected_rows = $this->datacontrol_model->delete('cluster1', array('cluster_url'=>$rows->cluster_url));
      echo json_encode(array('error' => 0));
    } else{
      echo json_encode(array('error' => 1));
    }
  }


  // public function userDeactivate(){
  //   $this->ion_auth->deactivate(19);
  // }


}
