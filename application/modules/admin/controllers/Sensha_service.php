<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sensha_service extends CI_Controller
{

    public $user_lang;
    public $coutry_iso;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('database/datacontrol_model');
        // delete_cookie('user_language');
        // $this->user_lang = get_cookie('user_language');
        $this->user_lang = 'Global';
        if (get_cookie('user_language')) {
            $this->user_lang = get_cookie('user_language');
        } else {
            set_cookie(array(
                'name' => 'user_language',
                'value' => 'Global',
                'expire' => 3600 * 24 * 12,
            ));
        }

        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }

        $this->lang->load('set', strtolower($this->user_lang));
        $this->db->where('nicename', $this->user_lang);
        $r = $this->datacontrol_model->getRowData('countries');
        $this->coutry_iso = strtolower($r->iso);
        //
        // $this->db->where('country', $this->user_lang);
        // $lang = $this->datacontrol_model->getRowData('nation_lang');

        // $this->lang->load('set', 'japanese');
    }

    public function confirm_pay_credit($encode = '')
    {
        $decode = json_decode(base64_decode(urldecode($encode)));

        $this->db->where('country', $decode->country);
        $this->db->where('otp', $decode->otp);
        $got_credit = $this->datacontrol_model->getRowData('sales_history_credit');
        if (empty($got_credit)) {
            $data['is_get_credit'] = 0;
        } else {
            $data['is_get_credit'] = 1;
        }

        $data['credit'] = $decode;
        $data['encode'] = $encode;
        $data['page'] = 'admin/confirm_credit_v';
        $this->load->view('page/front_template', $data);

    }

    public function success_pay_credit($encode = '')
    {
        $decode = json_decode(base64_decode(urldecode($encode)));

        $this->db->where('country', $decode->country);
        $this->db->where('otp', $decode->otp);
        $got_credit = $this->datacontrol_model->getRowData('sales_history_credit');

        if ($decode->otp == $this->input->post('otp')) {
            if (empty($got_credit)) {
                $decode->create_by = 1;
                $decode->update_by = 1;
                $this->datacontrol_model->insert('sales_history_credit', $decode);
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }

    }

}
