<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Financial_manager extends CI_Controller
{
    public $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');
        $this->load->model('admin/sales_history_total_model', 'sales_history_total_m');
        $this->load->model('admin/Sales_history_credit_model', 'sales_history_credit_m');


        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user = $this->ion_auth->user()->row();
        // $this->db->where('country', $user->country);

        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->input->post('month')) {
            $this->db->where('month(create_date)', $this->input->post('month'));
        } else {
            $this->db->where('month(create_date)', date('m'));
            $_POST['month'] = date('m');
        }
        if ($this->input->post('year')) {
            $this->db->where('year(create_date)', $this->input->post('year'));
        } else {
            $this->db->where('year(create_date)', date('Y'));
            $_POST['year'] = date('Y');
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            //$this->db->where('country', $user->country);
            $this->db->where('purchaser_country', $user->country);
            $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
            //$this->db->where('purchaser_type', 'FC');
            $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
        }
        if ($this->ion_auth->in_group(array('AA'))) {
            $this->db->where('aa_name', $user->email);
            // $this->db->where('aa_name <> ', '-');
            // $this->db->where('country', $user->country);
        }
        $data['sales_history'] = $this->datacontrol_model->getAllData('sales_history_total');

        // echo $this->db->last_query();
        $this->session->set_userdata('admin_sql_export', $this->db->last_query());

        if ($this->ion_auth->in_group(array('admin'))) {
            //  $this->db->where('credit_type', 'release');
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            //$this->db->where('country', $user->country);
            $this->db->where("(country = '" . $user->country . "' OR purchaser_country = '" . $user->country . "')");
            //$this->db->where('country <> ', 'Global')->or_where('credit_type', 'BUY');
            //$this->db->where('purchaser <> ', 'GU');
            //$this->db->where("((purchaser = 'SA') OR (purchaser = 'FC') OR (purchaser = 'GU' AND seller = '" . $user->country . "'))");
            //$this->db->or_where('credit_type', 'release');
            $this->db->where("(credit_added != 0 OR release_amount != 0 OR use_credit != 0)");
        }
        if ($this->input->post('month')) {
            $this->db->where('month(create_date)', $this->input->post('month'));
        } else {
            $this->db->where('month(create_date)', date('m'));
            $_POST['month'] = date('m');
        }
        if ($this->input->post('year')) {
            $this->db->where('year(create_date)', $this->input->post('year'));
        } else {
            $this->db->where('year(create_date)', date('Y'));
            $_POST['year'] = date('Y');
        }
        $data['sales_history_credit'] = $this->datacontrol_model->getAllData('sales_history_credit');
        //echo $this->db->last_query();

        // echo "<pre>";
        // print_r($data['sales_history']);
        // exit();

        $this->db->select("account_name, country, create_date, month(create_date) as the_month, year(create_date) as the_year, sum(sale_total_amount) as sale_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(item_amount) as item_amount");
        $this->db->where('account_type', 'SA');
        $this->db->group_by('country, YEAR(create_date), MONTH(create_date)');
        $query = $this->db->get('sales_history');
        $data['credit_transaction_hq'] = $query->result();

        $this->db->where('account_type', 'SA');
        $data['credit_transaction_sa'] = $this->datacontrol_model->getAllData('sales_history');

        $query = $this->db->query("SELECT id, country, balance FROM sales_history_credit WHERE id IN ( SELECT MAX(id) FROM sales_history_credit GROUP BY country)");
        $data['credit_history'] = $query->result();

        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

        $data["content_view"] = 'admin/financial/financial_manager_v';
        $data["menu"] = 'financial_manager';
        $data["htmlTitle"] = "Financial Manager";

        $current_credit = $this->sales_history_credit_m->get_current_credit();
        $data['current_credit'] = $current_credit[0];

        $this->load->view('admin_template', $data);
    }

    public function data_index_sales_total()
    {
        $input = $this->input->post();
        $info = $this->sales_history_total_m->get_rows($input);
        $this->session->set_userdata('admin_sql_export_sales_total', $this->db->last_query());
        $infoCount = $this->sales_history_total_m->get_count($input);

        $user = $this->ion_auth->user()->row();

        foreach ($info->result() as $key => $item) {

            //if((in_array($item->payment_method, ['amazon', 'paypal']) && $item->payment_status == 'Completed') || in_array($item->payment_method, ['pay on delivery', 'Bank Transfer', Null])) {

            $this->db->where('shipment_id', $item->shipment_id);
            $credit = $this->datacontrol_model->getRowData('sales_history_credit');

            if ($this->ion_auth->in_group(array('admin'))) {
                if (empty($credit)) {
                    if (!$item->cancle) {
                        $action = "<button id=\"conf-{$item->shipment_id}\" class=\"btn btn-warning\" onclick=\"conf_credit('{$item->shipment_id}', this)\">Confirm</button>";
                        $action .= "&nbsp;<button class=\"btn btn-outline-danger\" onclick=\"cancle_credit('{$item->shipment_id}', this)\">Cancel</button>";
                    } else {
                        $action = "<button class=\"btn btn-outline-danger btn-default\" disabled>Canceled</button>";
                    }
                } else {
                    $action = "<button class=\"btn btn-default\" disabled>Confirmed</button>";
                }
            } else {
                if (empty($credit)) {
                    if ($item->cancle) {
                        $action = "<p class='text-danger'>Canceled</p>";
                    }else {
                        $action = "<p class='text-default'>Processing</p>";
                    }
                } else {
                    $action = "<p class='text-default'>Confirmed</p>";
                }
            }

            if ($this->ion_auth->in_group(array('admin'))) {
                $sales_total = number_format(($item->item_sub_total_amount + $item->delivery_amount - $item->discount_amount));
                $pay_to_agent = number_format(($item->sa_pay_global != 0) ? $item->sa_pay_global : $item->sa_pay_domestic);
                $expected_income = number_format(($item->item_sub_total_amount + $item->delivery_amount - $item->discount_amount) - (($item->sa_pay_global != 0) ? $item->sa_pay_global : $item->sa_pay_domestic));
            }
            if ($this->ion_auth->in_group(array('SA'))) {
                $sales_total = number_format(($item->sa_pay_global != 0) ? $item->sa_pay_global : $item->sa_pay_domestic);
                $pay_to_agent = number_format($item->aa_pay);;
                $expected_income = number_format((($item->sa_pay_global != 0) ? $item->sa_pay_global : $item->sa_pay_domestic) - $item->aa_pay);
            }
            if ($this->ion_auth->in_group(array('AA'))) {
                $sales_total = number_format($item->discount_amount);
                $pay_to_agent = number_format(0);
                $expected_income = number_format($item->aa_pay);
            }

            $column[$key]['shipment_id'] = $item->shipment_id;
            $column[$key]['create_date'] = $item->create_date;
            $column[$key]['country'] = ucwords(str_replace("_", " ", $item->country));
            $column[$key]['purchaser_company'] = ($item->purchaser_company == '') ? $item->purchaser_name : $item->purchaser_company;
            $column[$key]['purchaser_type'] = $item->purchaser_type;
            $column[$key]['create_by'] = $item->create_by;
            $column[$key]['ion_auth'] = $this->ion_auth->user($item->create_by)->row()->discount_setting;
            $column[$key]['no_of_item'] = $item->item_amount;
            $column[$key]['sales_amount'] = number_format($item->item_sub_total_amount);
            $column[$key]['shipping_fee'] = number_format($item->delivery_amount);
            $column[$key]['discount_total'] = number_format($item->discount_amount);
            $column[$key]['sales_total'] = $sales_total;
            $column[$key]['pay_to_agent'] = $pay_to_agent;
            $column[$key]['expected_income'] = $expected_income;
            $column[$key]['action'] = $action;
        }
        //}

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function data_index_sales_history_credit()
    {
        $input = $this->input->post();
        $info = $this->sales_history_credit_m->get_rows($input);
        $this->session->set_userdata('admin_sql_export', $this->db->last_query());
        $infoCount = $this->sales_history_credit_m->get_count($input);

        foreach ($info->result() as $key => $item) {
            $column[$key]['create_date'] = date('Y-m-d', strtotime($item->create_date));
            $column[$key]['credit_type'] = $item->credit_type;
            $column[$key]['shipment_id'] = $item->shipment_id;
            $column[$key]['purchaser_country'] = ucwords(str_replace("_", " ", $item->purchaser_country));
            $column[$key]['purchaser'] = $item->purchaser;
            $column[$key]['country'] = ucwords(str_replace("_", " ", $item->country));
            $column[$key]['item_amount'] = "sales " . $item->item_amount . " items";
            $column[$key]['transaction'] = number_format($item->transaction);
            $column[$key]['balance'] = "<b>" . number_format($item->balance) . "</b>";

        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    public function x()
    {
        $financial = $this->datacontrol_model->getAllData('financial');
        foreach ($financial[0] as $key => $value) {
            echo "<td>php echo item->$key;?></td><br />";
            echo 'data-' . $key . '="php echo $item->' . $key . '?>" ';
        }

    }

    public function add()
    {
        $data = array(
            'month' => $this->input->post('month', true),
            'shop_type' => $this->input->post('shop_type', true),
            'agent_type' => $this->input->post('agent_type', true),
            'sale_amount' => $this->input->post('sale_amount', true),
            'shipment_amount' => $this->input->post('shipment_amount', true),
            'discount_amount' => $this->input->post('discount_amount', true),
            'total_amount' => $this->input->post('total_amount', true),
            'no_transaction' => $this->input->post('no_transaction', true),
            'avg_sale_amount' => $this->input->post('avg_sale_amount', true),
            'amount_to_pay' => $this->input->post('amount_to_pay', true),
        );
        $affected_rows = $this->datacontrol_model->insert('financial', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit()
    {
        $data = array(
            'month' => $this->input->post('month', true),
            'shop_type' => $this->input->post('shop_type', true),
            'agent_type' => $this->input->post('agent_type', true),
            'sale_amount' => $this->input->post('sale_amount', true),
            'shipment_amount' => $this->input->post('shipment_amount', true),
            'discount_amount' => $this->input->post('discount_amount', true),
            'total_amount' => $this->input->post('total_amount', true),
            'no_transaction' => $this->input->post('no_transaction', true),
            'avg_sale_amount' => $this->input->post('avg_sale_amount', true),
            'amount_to_pay' => $this->input->post('amount_to_pay', true),
        );
        $affected_rows = $this->datacontrol_model->update('financial', $data, array('id' => $this->input->post('edit_id', true)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete($id)
    {
        $affected_rows = $this->datacontrol_model->delete('financial', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function export()
    {
        if ($this->ion_auth->in_group(array('AA'))) {
            $selected = 'shipment_id, country, account_type, account_name, sale_date, buy_type, item_amount, item_sub_total_amount, import_shipping, import_tax, delivery_amount, discount_ratio, user_discount_amount, coupon_percent_amount, use_credit, discount_amount, coupon_code, sale_total_amount, vat_percent, vat_amount, admin_fee_amount, payment_total_amount, purchaser_type, purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method, shipping_method, shipping_status, aa_name, aa_pay, create_date';
        } else {
            $selected = 'shipment_id, country, account_type, account_name, sale_date, buy_type, item_amount, item_sub_total_amount, import_shipping, import_tax, delivery_amount, discount_ratio, user_discount_amount, coupon_percent_amount, use_credit, discount_amount, coupon_code, sale_total_amount, vat_percent, vat_amount, admin_fee_amount, payment_total_amount, purchaser_type, purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method, shipping_method, shipping_status, sa_name, sa_pay_global, sa_pay_domestic, aa_name, aa_pay, create_date';
        }
        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export_sales_total'));
        $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
        $query = $this->db->query($query_none_limit);
        //echo $query->num_rows(); exit();
        $count_row_limit = 3000;
        $files_csv = [];
        if ($query->num_rows() <= 3000) {
            $export = $query->result();
            array_push($files_csv, $this->new_export($export, 0));
        } else {
            $new_query = [];
            $newquery = [];
            $export = [];
            $file_number = 0;
            for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
                $file_number++;
                $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
                $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

                $newquery[$i] = $this->db->query($new_query[$i]);
                $export[$i] = $newquery[$i]->result();

                array_push($files_csv, $this->new_export($export[$i], $file_number));

            }

        }

        $data["files_csv"] = $files_csv;
        $this->session->set_userdata('financial_manager_download', $data);

        redirect('admin/financial_manager/download');

    }

    public function new_export($data, $file_number = 0)
    {
        $table_columns = array();
        foreach ($data[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Account Setting" . date('Y-m-d'))
            ->setSubject("Account Setting" . date('Y-m-d'))
            ->setDescription("Export Account Setting")
            ->setKeywords("Export Account Setting")
            ->setCategory("Export Account Setting");

        // Add some data
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        for ($i = 0; $i < count($data); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet

        $this->excel->getActiveSheet()->setTitle('Export Financial Manager' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        $callStartTime = microtime(true);

        if ($file_number == 0) {
            $newFileName = "Export-Financial-Manager" . "_" . date('Y-m-d') . '_' . time() . '.csv';
        } else {
            $newFileName = "Export-Financial-Manager" . "_" . date('Y-m-d') . '_' . time() . "(" . $file_number . ")" . '.csv';
        }

        if (!is_dir('uploads/' . 'export/')) {
            mkdir('./uploads/export/', 0777, TRUE);

        }
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save("uploads/export/" . $newFileName);
        //$objWriter->save('php://output');
        return "uploads/export/" . $newFileName;

    }

    public function download()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'financial_manager';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("financial_manager_download");
        $this->load->view('admin_template', $data);
    }

    public function export_special()
    {
        $selected = 'sp.shipment_id,sp.product_id,sp.blank_a,sp.product_name,sp.item_sub_total_amount, sp.item_amount, sp.sale_date, sp.account_name, sp.email, sp.blank_b, sp.blank_c, sp.blank_d, sp.name, sp.shipping_addr, sp.phone, sp.payment_method, sp.blank_e, sp.blank_f, sp.blank_g, sp.blank_h, sp.blank_i, sp.blank_j, sp.blank_k, sp.blank_l, sp.blank_m, sp.item_sub_total_amount2, sp.item_amount2, sp.sub_total, sp.special_vat, sp.admin_fee_amount, sp.delivery_fee, sp.payment_total_amount, sp.shipping_full_addr, sp.company_name, sp.shipping_full_addr2, sp.phone2, sp.blank_n, sp.blank_o, sp.email2, sp.blank_p, sp.blank_q, sp.item_sub_total_amount3, sp.blank_r, blank_s, blank_t, blank_u, sp.blank_v, sp.delivery_amount, sp.delivery_fee_vat, sp.vat_amount, sp.vat_special, sp.discount_amount, sp.discount_special, sp.blank_w, sp.country, sp.product_id2, sp.item_sub_total_amount4, sp.blank_x, sp.blank_y, blank_z, blank_aa, blank_ab, blank_ac, blank_ad, sp.blank_ae, sp.blank_af, sp.blank_ag, sp.blank_ah, sp.blank_ai, sp.payment_total_amount2, sp.item_sub_total_amount5, sp.blank_aj, sp.blank_ak, sp.blank_al, sp.blank_am, sp.blank_an, discount_special2, sp.blank_ao, sp.blank_ap, sp.create_by, sp.update_by, sp.create_date, sp.update_date,
sales_history_total.`country` as country_seller ';

        $varFrom = " FROM sales_history_special sp RIGHT JOIN sales_history_total ON `sales_history_total`.`shipment_id` = `sp`.`shipment_id`";

        $varSetWhere = "WHERE  (sales_history_total.`country` IN ('Global','Japan','JapanA','JapanB') AND ((sp.payment_method  IN ('amazon','paypal') AND sp.payment_status = 'Completed') OR (sp.payment_method  IN ('Bank Transfer','pay on delivery')) OR (sp.payment_method  IS NULL)  ))";


        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export_sales_total'));

        $_set_value_export = str_replace('FROM `sales_history_total`', $varFrom, $_set_value_export);

        $_set_value_export = str_replace('(create_date)', '(sp.create_date)', $_set_value_export);

        $_set_value_export = str_replace('`country` =', 'sp.country =', $_set_value_export);

        $_set_value_export = str_replace('`payment_method`', 'sp.payment_method', $_set_value_export);

        $_set_value_export = str_replace('`payment_status`', 'sp.payment_status', $_set_value_export);


        $isWhere = strpos($_set_value_export, 'WHERE');

        if ($isWhere) {
            $_set_value_export = str_replace('WHERE', $varSetWhere . ' AND ', $_set_value_export);
        } else {
            $_set_value_export = $_set_value_export . ' WHERE ' . $varSetWhere;
        }

        $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
        $query = $this->db->query($query_none_limit);
        //echo $this->db->last_query();exit();
        //echo $query->num_rows(); exit();
        $count_row_limit = 3000;
        $files_csv = [];
        if ($query->num_rows() <= 3000) {
            $export = $query->result();
            array_push($files_csv, $this->new_export_special($export, 0));
        } else {
            $new_query = [];
            $newquery = [];
            $export = [];
            $file_number = 0;
            for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
                $file_number++;
                $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
                $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

                $newquery[$i] = $this->db->query($new_query[$i]);
                $export[$i] = $newquery[$i]->result();

                array_push($files_csv, $this->new_export_special($export[$i], $file_number));

            }

        }

        $data["files_csv"] = $files_csv;
        $this->session->set_userdata('financial_manager_special_download', $data);

        redirect('admin/financial_manager/download_special');

    }

    public function new_export_special($data, $file_number = 0)
    {
        $table_columns = array();
        foreach ($data[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Account Setting" . date('Y-m-d'))
            ->setSubject("Account Setting" . date('Y-m-d'))
            ->setDescription("Export Account Setting")
            ->setKeywords("Export Account Setting")
            ->setCategory("Export Account Setting");

        // Add some data
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        for ($i = 0; $i < count($data); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet

        $this->excel->getActiveSheet()->setTitle('Export Financial Manager' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        $callStartTime = microtime(true);

        if ($file_number == 0) {
            $newFileName = "Export_TuhanGate_CSV" . "_" . date('Y-m-d') . '_' . time() . '.csv';
        } else {
            $newFileName = "Export_TuhanGate_CSV" . "_" . date('Y-m-d') . '_' . time() . "(" . $file_number . ")" . '.csv';
        }

        if (!is_dir('uploads/' . 'export/')) {
            mkdir('./uploads/export/', 0777, TRUE);

        }
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save("uploads/export/" . $newFileName);
        //$objWriter->save('php://output');
        return "uploads/export/" . $newFileName;

    }

    public function download_special()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'financial_manager';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("financial_manager_special_download");
        $this->load->view('admin_template', $data);
    }

    public function export_bk()
    {

        if ($this->ion_auth->in_group(array('AA'))) {
            $selected = 'shipment_id, country, account_type, account_name, sale_date, buy_type, item_amount, item_sub_total_amount, import_shipping, import_tax, delivery_amount, discount_ratio, user_discount_amount, coupon_percent_amount, use_credit, discount_amount, coupon_code, sale_total_amount, vat_percent, vat_amount, admin_fee_amount, payment_total_amount, purchaser_type, purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method, shipping_method, shipping_status, aa_name, aa_pay, create_date';
        } else {
            $selected = 'shipment_id, country, account_type, account_name, sale_date, buy_type, item_amount, item_sub_total_amount, import_shipping, import_tax, delivery_amount, discount_ratio, user_discount_amount, coupon_percent_amount, use_credit, discount_amount, coupon_code, sale_total_amount, vat_percent, vat_amount, admin_fee_amount, payment_total_amount, purchaser_type, purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method, shipping_method, shipping_status, sa_name, sa_pay_global, sa_pay_domestic, aa_name, aa_pay, create_date';
        }
        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export'));
        $q = $this->db->query($_set_value_export);
        $financial = $q->result();
        $table_columns = array();
        foreach ($financial[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        // echo date('H:i:s') , " Set document properties" , EOL;
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Financial Manager" . date('Y-m-d'))
            ->setSubject("Financial Manager" . date('Y-m-d'))
            ->setDescription("Export Financial Manager")
            ->setKeywords("Export Financial Manager")
            ->setCategory("Export Financial Manager");

        // Add some data
        // echo date('H:i:s') , " Add some data" , EOL;
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        // print_r($product_management[0]->id);

        for ($i = 0; $i < count($financial); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $financial[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet
        // echo date('H:i:s') , " Rename worksheet" , EOL;
        $this->excel->getActiveSheet()->setTitle('Export Financial Manager' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

        $newFileName = "Export Financial Manager" . "_" . date('Y-m-d') . '_' . time() . '.csv';

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $newFileName . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    public function export_special_bk()
    {
        $selected = 'sp.shipment_id,sp.product_id,sp.blank_a,sp.product_name,sp.item_sub_total_amount, sp.item_amount, sp.sale_date, sp.account_name, sp.email, sp.blank_b, sp.blank_c, sp.blank_d, sp.name, sp.shipping_addr, sp.phone, sp.payment_method, sp.blank_e, sp.blank_f, sp.blank_g, sp.blank_h, sp.blank_i, sp.blank_j, sp.blank_k, sp.blank_l, sp.blank_m, sp.item_sub_total_amount2, sp.item_amount2, sp.sub_total, sp.special_vat, sp.admin_fee_amount, sp.delivery_fee, sp.payment_total_amount, sp.shipping_full_addr, sp.company_name, sp.shipping_full_addr2, sp.phone2, sp.blank_n, sp.blank_o, sp.email2, sp.blank_p, sp.blank_q, sp.item_sub_total_amount3, sp.blank_r, blank_s, blank_t, blank_u, sp.blank_v, sp.delivery_amount, sp.delivery_fee_vat, sp.vat_amount, sp.vat_special, sp.discount_amount, sp.discount_special, sp.blank_w, sp.country, sp.product_id2, sp.item_sub_total_amount4, sp.blank_x, sp.blank_y, blank_z, blank_aa, blank_ab, blank_ac, blank_ad, sp.blank_ae, sp.blank_af, sp.blank_ag, sp.blank_ah, sp.blank_ai, sp.payment_total_amount2, sp.item_sub_total_amount5, sp.blank_aj, sp.blank_ak, sp.blank_al, sp.blank_am, sp.blank_an, discount_special2, sp.blank_ao, sp.blank_ap, sp.create_by, sp.update_by, sp.create_date, sp.update_date,
sales_history_total.`country` as country_seller ';

        $varFrom = " FROM sales_history_special sp RIGHT JOIN sales_history_total ON `sales_history_total`.`shipment_id` = `sp`.`shipment_id`";

        $varSetWhere = "WHERE  (sales_history_total.`country` IN ('Global','Japan','JapanA','JapanB') AND ((sp.payment_method  IN ('amazon','paypal') AND sp.payment_status = 'Completed') OR (sp.payment_method  IN ('Bank Transfer','pay on delivery',Null))))";


        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export'));

        $_set_value_export = str_replace('FROM `sales_history_total`', $varFrom, $_set_value_export);

        $_set_value_export = str_replace('(create_date)', '(sp.create_date)', $_set_value_export);

        $_set_value_export = str_replace('`country` =', 'sp.country =', $_set_value_export);


        $isWhere = strpos($_set_value_export, 'WHERE');

        if ($isWhere) {
            $_set_value_export = str_replace('WHERE', $varSetWhere . ' AND ', $_set_value_export);
        } else {
            $_set_value_export = $_set_value_export . ' WHERE ' . $varSetWhere;
        }

        $q = $this->db->query($_set_value_export);
        $financial = $q->result();

        //$q = $this->db->query($this->session->userdata('admin_sql_export'));
        //$financial = $q->result();


        //$this->db->select('shipment_id, product_id, blank_a, product_name, item_sub_total_amount, item_amount, sale_date, account_name, email, blank_b, blank_c, blank_d, name, shipping_addr, phone, payment_method, blank_e, blank_f, blank_g, blank_h, blank_i, blank_j, blank_k, blank_l, blank_m, item_sub_total_amount2, item_amount2, sub_total, special_vat, admin_fee_amount, delivery_fee, payment_total_amount, shipping_full_addr, company_name, shipping_full_addr2, phone2, blank_n, blank_o, email2, blank_p, blank_q, item_sub_total_amount3, blank_r, blank_s, blank_t, blank_u, blank_v, delivery_amount, delivery_fee_vat, vat_amount, vat_special, discount_amount, discount_special, blank_w, country, product_id2, item_sub_total_amount4, blank_x, blank_y, blank_z, blank_aa, blank_ab, blank_ac, blank_ad, blank_ae, blank_af, blank_ag, blank_ah, blank_ai, payment_total_amount2, item_sub_total_amount5, blank_aj, blank_ak, blank_al, blank_am, blank_an, discount_special2, blank_ao, blank_ap, create_by, update_by, create_date, update_date');
        // if (!(in_array($item->payment_method, array('amazon', 'paypal')) && ($item->payment_status == '-'))) :


        //$financial = $this->datacontrol_model->getAllData('sales_history_special');

        //new


        $table_columns = array();
        foreach ($financial[0] as $key => $value) {
            $table_columns[] = $key;
        }
        unset($table_columns['id']);
        $this->load->library('excel');

        // Set document properties
        // echo date('H:i:s') , " Set document properties" , EOL;
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Financial Manager" . date('Y-m-d'))
            ->setSubject("Financial Manager" . date('Y-m-d'))
            ->setDescription("Export Financial Manager")
            ->setKeywords("Export Financial Manager")
            ->setCategory("Export Financial Manager");

        // Add some data
        // echo date('H:i:s') , " Add some data" , EOL;
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueExplicitByColumnAndRow($column, 1, $field, PHPExcel_Cell_DataType::TYPE_STRING);
            $column++;
        }

        $excel_row = 2;

        // print_r($product_management[0]->id);

        for ($i = 0; $i < count($financial); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueExplicitByColumnAndRow($excel_row_column, $excel_row, $financial[$i]->$value, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet
        // echo date('H:i:s') , " Rename worksheet" , EOL;
        $this->excel->getActiveSheet()->setTitle('Export Financial Manager' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

        $newFileName = "Export_TuhanGate_CSV" . "_" . date('Y-m-d') . '_' . time() . '.csv';

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $newFileName . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    public function pay_credit()
    {
        $this->load->library('encrypt');
        $sa_list = $this->ion_auth->users('SA')->result();
        $sa = '';
        $sale_account_type = '';
        $sale_account_name = '';
        foreach ($sa_list as $item) {
            if ($item->country == $this->input->post('country')) {
                $sale_account_type = 'SA';
                $sale_account_name = $item->email;
            }
        }

        $this->db->order_by('id', 'desc');
        $this->db->where('country', $this->input->post('country'));
        $this->db->limit(1);
        $balance = $this->datacontrol_model->getRowData('sales_history_credit');
        // echo $this->db->last_query();
        // echo $balance->balance;
        // echo $balance->balance - $this->input->post('amount_of_release');

        $otp_code = $this->generateOTP();
        $data = urlencode(base64_encode(json_encode(array(
            'country' => $this->input->post('country'),
            'credit_type' => 'release',
            'release_amount' => $this->input->post('amount_of_release'),
            'seller' => 'Global',
            'purchaser' => 'admin',
            'transaction' => -($this->input->post('amount_of_release')),
            'balance' => $balance->balance - $this->input->post('amount_of_release'),
            'otp' => $otp_code,
        ))));

        // print_r($balance->balance);
        // print_r($this->input->post('amount_of_release'));

        // echo $this->encrypt->encode($data);

        $msg =
            '------------------------<br>
経理担当者へ<br>
------------------------<br>
SA (  ' . $this->input->post('country') . ' )からクレジット解放依頼がありました。<br>
送金処理をおこなってください。<br>
依頼前クレジット残額 : ' . $balance->balance . ' 円<br>
依頼額 : ' . $this->input->post('amount_of_release') . '円 (送金想定額)<br><br>
開放手続きは下記のリンクをクリックして、携帯電話に送信されたパスワードを入力すると完了します。<br>
<a href="' . base_url("sensha-admin/sensha_service/confirm_pay_credit/$data") . '">このリンクをクリック</a><br><br>
経理担当は、以下の手順で作業を行ってください。<br><br>
------------------------<br>
1. 当該SAとの支払いサイクルに関する契約内容を確認してください。<br><br>
2. HQ管理画面より最新のクレジット残額を確認し、解放依頼額を送金可能か確認してください。<br><br>
3. 送金してください。<br>
＊ 万一、クレジット残額が不足していて解放依頼額を送金できない場合には、解放可能額を送金してください。(クレジット残額の全てを送金)<br><br>
4. HQ管理画面より、解放(送金)した額を入力して、当該SAのクレジット残額を減額処理する。<br><br>
5. 解放(送金)額が累計より減額されているか2名以上でダブルチェックをおこなってください。<br><br>
6. 銀行の送金証明書(PDF)を営業担当者へ提供してください。<br>
------------------------<br><br>
間違いの無いよう、確実におこなってください。';

        mail_to(array("info@sensha-world.com" => 'Sensha',"h-aihara@sensha-world.com" => 'Sensha'), "経理担当者へ ［クレジット解放依頼］", $msg);
        //mail_to(array("info@enfete.asia" => 'Sensha'), "[Sensha]Confirm your credit", $msg);

        require_once 'vendor/autoload.php';
        $basic = new \Nexmo\Client\Credentials\Basic('7521ab39', '6Ijk5cPrBVbeG1RW');
        $client = new \Nexmo\Client($basic);

        $message = $client->message()->send([
           	'to' => '+819093667534',
            //'to' => '+66823251212',
            'from' => 'Sensha',
            'text' => 'Your OTP for get credit is : ' . $otp_code,
            'type' => 'unicode'
        ]);

        // $data = array(
        //   'payment_method' => 'release',
        //   'sale_total_amount' => $this->input->post('amount_of_release'),
        //   'country' => $this->input->post('country'),
        //   'account_type' =>$sale_account_type,
        //   'account_name' => $sale_account_name,
        // );
        // $this->datacontrol_model->insert('sales_history', $data);
    }

    public function generateOTP($length = 6)
    {
        $chars = '0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public function add_credit($shipment_id)
    {
        //// credit data prepare
        $this->db->where('shipment_id', $shipment_id);
        $data_sale_total = $this->datacontrol_model->getRowData('sales_history_total');

        $purchaser = $this->ion_auth->user($data_sale_total->create_by)->row();

        $purchaser_type = $data_sale_total->purchaser_type;
        $purchaser_country = $data_sale_total->purchaser_country;
        $purchaser_discount = $purchaser->discount_setting;

        if ($data_sale_total->account_type == 'admin' && $purchaser_type == 'SA') {
            $credit_type = 'BUY';
        } else {
            $credit_type = 'SELL';
        }
        if ($data_sale_total->buy_type == 'domestic') {
            $sa_discount = 100;
            if ($data_sale_total->country == 'Global' && $purchaser_type == 'FC') {
                $sa = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
                $sa_discount = $sa->discount_setting;
            }
        }
        if ($data_sale_total->buy_type == 'oversea') {
            $sa_discount = 0;
            if ($data_sale_total->country == 'Global' && $purchaser_type == 'FC') {
                $sa = $this->ion_auth->user($data_sale_total->purchaser_id)->row();
                $sa_discount = $sa->discount_setting;
            }
        }


        $percent = array($data_sale_total->coupon_percent_discount, $purchaser_discount, $sa_discount);
        arsort($percent);
        $total_percent = 0;
        foreach ($percent as $val) {
            $total_percent = $val - $total_percent;
        }

        $this->db->order_by('id', 'desc');
        $this->db->where('country', $data_sale_total->purchaser_country)->or_where('credit_type', 'BUY');
        $balance = $this->datacontrol_model->getRowData('sales_history_credit');
        $credit_added = 0;
        if ($data_sale_total->sa_pay_global != 0) {
            $credit_added = $data_sale_total->sa_pay_global;
        }
        if ($data_sale_total->sa_pay_domestic != 0) {
            $credit_added = $data_sale_total->sa_pay_domestic;
        }

        //// credit
        $data_credit = array(
            'country' => $data_sale_total->country,
            'credit_type' => $credit_type,
            'shipment_id' => $shipment_id,
            'item_amount' => $data_sale_total->item_amount,
            'sale_amount' => $data_sale_total->item_sub_total_amount,
            'shiping_fee' => $data_sale_total->delivery_amount,
            'discount_total' => $data_sale_total->discount_amount,
            'seller' => $data_sale_total->country,
            'purchaser' => $purchaser_type,
            'purchaser_country' => $purchaser_country,
            'percent' => $total_percent,
            'coupon_percent' => $data_sale_total->coupon_percent_discount,
            'fc_percent' => $purchaser_discount,
            'sa_percent' => $sa_discount,
            'coupon_fix' => $data_sale_total->coupon_fix_amount,
            'credit_added' => $credit_added,
            'use_credit' => $data_sale_total->use_credit,
            'release_amount' => 0,
            'transaction' => $credit_added,
            'balance' => round($balance->balance + $credit_added),
        );

        if ($purchaser_type == 'FC' && $data_sale_total->country == 'Global') {
            $data_credit['country'] = $purchaser_country;
        }
        if ($credit_type == 'BUY') {
            $data_credit['transaction'] = -($data_sale_total->use_credit);
            $data_credit['balance'] = round($balance->balance - $data_sale_total->use_credit);
        }

        if ($data_credit['balance'] < 0) {
            echo json_encode(array('error' => 1));
        } else {
            $this->db->where('shipment_id', $shipment_id);
            $check_credit = $this->datacontrol_model->getRowData('sales_history_credit');
            if (empty($check_credit)) {
                $this->datacontrol_model->insert('sales_history_credit', $data_credit);
                echo json_encode(array('error' => 0));
            }
        }
    }

    public function cancle($shipment_id)
    {
        $this->db->set('cancle', true);
        $this->db->where('shipment_id', $shipment_id);
        $this->db->update('sales_history_total');
        echo json_encode(array('error' => 0));
    }

    public function send_credit_release_request()
    {
        $send_list = [$this->config->item('contact_email') => $this->config->item('contact_email')];

        $bcc = [$this->config->item('credit_email') => $this->config->item('credit_email')];

        $msg = "------------------------";
        $msg .= "経理担当者へ";
        $msg .= "------------------------";
        $msg .= "<br>SA " . $this->input->post('sa_country', TRUE) . " " . $this->input->post('sa_name', TRUE) . " からクレジット解放依頼がありました。";
        $msg .= "<br>送金処理をおこなってください。";
        $msg .= "<br>";
        $msg .= "<br>依頼前クレジット残額(YEN) : " . $this->input->post('sa_current_credit', TRUE);
        $msg .= "<br>依頼額(YEN): " . $this->input->post('amount', TRUE);

        $msg .= "<br>";
        $msg .= "<br>";

        $msg .= "経理担当は、以下の手順で作業を行ってください。";
        $msg .= "<br>";
        $msg .= "<br>------------------------";
        $msg .= "<br>1. 当該SAとの支払いサイクルに関する契約内容を確認してください。";

        $msg .= "<br>2. HQ管理画面より最新のクレジット残額を確認し、解放依頼額を送金可能か確認してください。";

        $msg .= "<br>3. 送金してください。";
        $msg .= "<br>＊ 万一、クレジット残額が不足していて解放依頼額を送金できない場合には、解放可能額を送金してください。(クレジット残額の全てを送金)";

        $msg .= "<br>4. HQ管理画面より、解放(送金)した額を入力して、当該SAのクレジット残額を減額処理する。";

        $msg .= "<br>5. 解放(送金)額が累計より減額されているか2名以上でダブルチェックをおこなってください。";

        $msg .= "<br>6. 銀行の送金証明書(PDF)を営業担当者へ提供してください。";
        $msg .= "<br>------------------------";

        $respone = mail_to($send_list, "経理担当者へ ［クレジット解放依頼］", $msg, $bcc);

        $this->session->set_flashdata('msg_email', 'success');
        redirect('sensha-admin/financial_manager', 'refresh');
    }

}
