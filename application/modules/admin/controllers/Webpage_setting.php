<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Webpage_setting extends CI_Controller
{
    var $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');

        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user_country = $this->ion_auth->user()->row()->country;

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user_country);
        }
        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user_country);
        }
        $data['page_home'] = $this->datacontrol_model->getAllData('page_home');

        $data["content_view"] = 'admin/webpage_setting/home_v';
        $data["menu"] = 'webpage_setting';
        $data["htmlTitle"] = "Webpage Home Setting";

        $this->load->view('admin_template', $data);
    }

    public function add()
    {
        $data = array(
            'name' => $this->input->post('name', TRUE),
            'country' => $this->input->post('country', TRUE),
            'content' => htmlspecialchars($this->input->post('content', TRUE)),
        );
        $country = $this->datacontrol_model->getAllData('nation_lang');
        foreach ($country as $item) {
            $data['country'] = $item->country;
            $affected_rows = $this->datacontrol_model->insert('webpage_setting', $data);
        }
        // $affected_rows = $this->datacontrol_model->insert('webpage_setting', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function add_home()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
        );
        $country = $this->datacontrol_model->getAllData('nation_lang');

        $affected_rows = $this->datacontrol_model->insert('page_home', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit_home()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
        );
        $affected_rows = $this->datacontrol_model->update('page_home', $data, array('id' => $this->input->post('edit_id', TRUE)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete_home($id)
    {
        $affected_rows = $this->datacontrol_model->delete('page_home', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function about()
    {
        $user_country = $this->ion_auth->user()->row()->country;

        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user_country);
        }
        $data['page_about'] = $this->datacontrol_model->getAllData('page_about');

        $data["content_view"] = 'admin/webpage_setting/about_v';
        $data["menu"] = 'webpage_setting';
        $data["htmlTitle"] = "Webpage Home Setting";

        $this->load->view('admin_template', $data);
    }

    public function add_about()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
            'content_d' => htmlspecialchars($this->input->post('content_d', TRUE)),
            'content_e' => htmlspecialchars($this->input->post('content_e', TRUE)),
        );
        $country = $this->datacontrol_model->getAllData('nation_lang');
        // foreach($country as $item){
        //   $data['country'] = $item->country;
        //   $affected_rows = $this->datacontrol_model->insert('page_about', $data);
        // }
        $affected_rows = $this->datacontrol_model->insert('page_about', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit_about()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
            'content_d' => htmlspecialchars($this->input->post('content_d', TRUE)),
            'content_e' => htmlspecialchars($this->input->post('content_e', TRUE)),
        );
        $affected_rows = $this->datacontrol_model->update('page_about', $data, array('id' => $this->input->post('edit_id', TRUE)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete_about($id)
    {
        $affected_rows = $this->datacontrol_model->delete('page_about', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function ppf()
    {
        $user_country = $this->ion_auth->user()->row()->country;

        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user_country);
        }
        $data['page_ppf'] = $this->datacontrol_model->getAllData('page_ppf');

        $data["content_view"] = 'admin/webpage_setting/ppf_v';
        $data["menu"] = 'webpage_setting';
        $data["htmlTitle"] = "Webpage Home Setting";

        $this->load->view('admin_template', $data);
    }

    public function add_ppf()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
        );
        $country = $this->datacontrol_model->getAllData('nation_lang');
        // foreach($country as $item){
        //   $data['country'] = $item->country;
        //   $affected_rows = $this->datacontrol_model->insert('page_ppf', $data);
        // }
        $affected_rows = $this->datacontrol_model->insert('page_ppf', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit_ppf()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
        );
        $affected_rows = $this->datacontrol_model->update('page_ppf', $data, array('id' => $this->input->post('edit_id', TRUE)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete_ppf($id)
    {
        $affected_rows = $this->datacontrol_model->delete('page_ppf', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function agent()
    {
        $user_country = $this->ion_auth->user()->row()->country;

        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('country', $user_country);
        }
        $data['page_agent'] = $this->datacontrol_model->getAllData('page_agent');

        $data["content_view"] = 'admin/webpage_setting/agent_v';
        $data["menu"] = 'webpage_setting';
        $data["htmlTitle"] = "Webpage Home Setting";

        $this->load->view('admin_template', $data);
    }

    public function add_agent()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
            'content_d' => htmlspecialchars($this->input->post('content_d', TRUE)),
        );
        $country = $this->datacontrol_model->getAllData('nation_lang');
        // foreach($country as $item){
        //   $data['country'] = $item->country;
        //   $affected_rows = $this->datacontrol_model->insert('page_agent', $data);
        // }
        $affected_rows = $this->datacontrol_model->insert('page_agent', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit_agent()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'meta_title' => $this->input->post('meta_title', TRUE),
            'meta_description' => $this->input->post('meta_description', TRUE),
            'content_a' => htmlspecialchars($this->input->post('content_a', TRUE)),
            'content_b' => htmlspecialchars($this->input->post('content_b', TRUE)),
            'content_c' => htmlspecialchars($this->input->post('content_c', TRUE)),
            'content_d' => htmlspecialchars($this->input->post('content_d', TRUE)),
        );
        $affected_rows = $this->datacontrol_model->update('page_agent', $data, array('id' => $this->input->post('edit_id', TRUE)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete_agent($id)
    {
        $affected_rows = $this->datacontrol_model->delete('page_agent', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function editorUploadImage()
    {
        if (!file_exists('uploads/webpage_setting/')) {
            mkdir("uploads/webpage_setting/", 0777);
        }

        $tempName = $_FILES['upload']['tmp_name'];
        $fileName = uniqid() . $_FILES['upload']['name'];
        $uploadPath = 'uploads/webpage_setting/' . $fileName;
        $imageUrl = base_url("uploads/webpage_setting/") . $fileName;

        $success = move_uploaded_file($tempName, $uploadPath);
        echo json_encode([
            'uploaded' => $success,
            'fileName' => $fileName,
            'url' => $imageUrl,
        ]);
    }


    // public function userDeactivate(){
    //   $this->ion_auth->deactivate(19);
    // }


}
