<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');

    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index()
  {
    $user_login = $this->ion_auth->user()->row();

    $m = date('m');
    if($this->input->post('month')){
      $m = $this->input->post('month');
    }
    else{
      $this->db->where('month(create_date)', $m);
    }

    $y = date('Y');
    if($this->input->post('year')){
      $y = $this->input->post('year');
    }
    else{
      $this->db->where('year(create_date)', $y);
    }

    $_POST['year'] = $y;
    $_POST['month'] = $m;


    if($this->ion_auth->in_group(array('admin'))){

      //hq
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_global) as sa_pay_global");
      $this->db->like('shipment_id', 'O', 'before');
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      if($this->input->post('country')){
        $this->db->where('purchaser_country', $this->input->post('country'));
      }
      $this->db->group_start();
      $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end();  
      $query = $this->db->get('sales_history_total');
      // $query = $this->db->query("SELECT month(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_global) as sa_pay_global FROM sales_history_total WHERE shipment_id LIKE '%O' AND month(create_date) = '$m'");
       // echo $this->db->last_query();
       // exit();
      $data['hq'] = $query->row();


      //domestic
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic");
      $this->db->like('shipment_id', 'D', 'before');
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      if($this->input->post('country')){
        $this->db->where('purchaser_country', $this->input->post('country'));
      }
      $this->db->group_start();
      $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end();
      $query = $this->db->get('sales_history_total');
      $data['domestic'] = $query->row();


      //release
      $this->db->select("MONTHNAME(create_date) as the_month, sum(release_amount) as release_amount");
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('credit_type', 'release');
      if($this->input->post('country')){
        $this->db->where('purchaser_country', $this->input->post('country'));
      }
      $query = $this->db->get('sales_history_credit');
      // echo $this->db->last_query();
      // $query = $this->db->query("SELECT month(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic FROM sales_history_total WHERE shipment_id LIKE '%D' AND month(create_date) = '$m'");
      $data['release'] = $query->row();

      //buy
      $this->db->select("MONTHNAME(create_date) as the_month, sum(use_credit) as use_credit");
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('credit_type', 'BUY');
      if($this->input->post('country')){
        $this->db->where('purchaser_country', $this->input->post('country'));
      }
      $query = $this->db->get('sales_history_credit');
      //echo $this->db->last_query();
      // $query = $this->db->query("SELECT month(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic FROM sales_history_total WHERE shipment_id LIKE '%D' AND month(create_date) = '$m'");
      $data['buy'] = $query->row();

      $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    }

    if($this->ion_auth->in_group(array('SA'))){

      //hq
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_global) as sa_pay_global, sum(aa_pay) as aa_pay");
      $this->db->like('shipment_id', 'O', 'before');
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);

      $this->db->where('purchaser_country', $user_login->country);

      $this->db->where('account_type', 'admin');

      $this->db->where('purchaser_type', 'FC');
      $this->db->group_start();
      $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end(); 
      $query = $this->db->get('sales_history_total');
       //echo $this->db->last_query();
      $data['hq'] = $query->row();

      //domestic
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic, sum(aa_pay) as aa_pay");
      $this->db->like('shipment_id', 'D', 'before');
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('account_type', 'SA');
        $this->db->where('purchaser_country', $user_login->country);
      $this->db->group_start();
        $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end();
      // $this->db->where('purchaser_type', 'FC');
      $query = $this->db->get('sales_history_total');
      // echo $this->db->last_query();
      $data['domestic'] = $query->row();


      //release
      $this->db->select("MONTHNAME(create_date) as the_month, sum(release_amount) as release_amount");
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('credit_type', 'release');
      $this->db->where('country', $user_login->country);
      $query = $this->db->get('sales_history_credit');
      // echo $this->db->last_query();
      // $query = $this->db->query("SELECT month(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic FROM sales_history_total WHERE shipment_id LIKE '%D' AND month(create_date) = '$m'");
      $data['release'] = $query->row();

      //buy
      $this->db->select("MONTHNAME(create_date) as the_month, sum(use_credit) as use_credit");
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('credit_type', 'BUY');
      $this->db->where('purchaser_country', $user_login->country);
      $query = $this->db->get('sales_history_credit');
      // echo $this->db->last_query();
      // $query = $this->db->query("SELECT month(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic FROM sales_history_total WHERE shipment_id LIKE '%D' AND month(create_date) = '$m'");
      $data['buy'] = $query->row();

      $this->db->where('country', $user_login->country);
      $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    }

    if($this->ion_auth->in_group(array('AA'))){
      //hq
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_global) as sa_pay_global, sum(aa_pay) as aa_pay");
      $this->db->like('shipment_id', 'O', 'before');
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      // $this->db->where('country', 'Global');
      $this->db->where('aa_pay <> ', 0);
      $this->db->group_start();
        $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end();
      $query = $this->db->get('sales_history_total');
      // echo $this->db->last_query();
      $data['hq'] = $query->row();

      //domestic
      $this->db->select("MONTHNAME(create_date) as the_month, sum(item_sub_total_amount) as item_sub_total_amount, sum(discount_amount) as discount_amount, sum(delivery_amount) as delivery_amount, sum(sale_total_amount) as sale_total_amount, sum(sa_pay_domestic) as sa_pay_domestic, sum(aa_pay) as aa_pay");
      $this->db->where('month(create_date)', $m);
      $this->db->where('year(create_date)', $y);
      $this->db->where('country <>', 'Global');
      $this->db->where('aa_name', $user_login->email);
      // $this->db->where('aa_pay <> ', 0);
      $this->db->group_start();
        $this->db->group_start();
            $this->db->where_in('payment_method', ['amazon','paypal']);
            $this->db->where('payment_status', 'Completed');
        $this->db->group_end();
        $this->db->or_group_start();
            $this->db->where_in('payment_method', ['pay on delivery','Bank Transfer']);
            $this->db->or_group_start();
                $this->db->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
       $this->db->group_end();
      $query = $this->db->get('sales_history_total');
      // echo $this->db->last_query();
      $data['domestic'] = $query->row();

      $this->db->where('country', $user_login->country);
      $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
    }

    $data["content_view"] = 'admin/dashboard_v';
    $data["menu"] = 'dashboard';
    $data["htmlTitle"] = "Dashboard";

    $this->load->view('admin_template', $data);
  }
}
