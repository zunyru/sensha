<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_history extends CI_Controller
{
    var $user_lang;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))) {
            redirect('auth/login', 'refresh');
        }
        $this->load->model('database/datacontrol_model');
        $this->load->model('admin/sales_history_model', 'sales_history_m');

        $this->user_lang = 'Global';
        if ($this->ion_auth->logged_in()) {
            $this->user_lang = $this->ion_auth->user()->row()->country;
        }
        if ($this->ion_auth->is_admin()) {
            $this->user_lang = 'Japan';
        }
        if (!file_exists('application/language/' . strtolower($this->user_lang))) {
            $this->user_lang = 'Global';
        }
        $this->lang->load('set', strtolower($this->user_lang));
    }

    public function index()
    {
        $user = $this->ion_auth->user()->row();

        $data['groups_list'] = $this->datacontrol_model->getAllData('groups');
        $data['countries'] = $this->datacontrol_model->getAllData('countries');
        $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

        $country = "";
        $m = "";
        $y = "";


        if ($this->input->post('country')) {
            $country = $this->input->post('country');
        }
        if ($this->input->post('month')) {
            $m = $this->input->post('month');
        } else {
            $m = date('m');
            $_POST['month'] = date('m');
        }
        if ($this->input->post('year')) {
            $y = $this->input->post('year');
        } else {
            $y = date('Y');
            $_POST['year'] = date('Y');
        }
        if ($this->input->post('country')) {
            $this->db->where('country', $country);
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            //$this->db->where('country', $user->country);
            $this->db->where('purchaser_country', $user->country);
            $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
            //$this->db->where('purchaser_type', 'FC');
            $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
        }
        if ($this->ion_auth->in_group(array('AA'))) {
            $this->db->where('aa_name', $user->email);
            //$this->db->where('aa_name <> ', '-');
        }

        $this->db->where("DATE_FORMAT(create_date,'%Y-%m')", "$y-$m");
        $sales_history_total = $this->datacontrol_model->getAllData('sales_history_total');
        // echo $this->db->last_query();
        // echo "<pre>";
        // print_r($sales_history_total);
        // exit();
        $shipment_id = array();

        foreach ($sales_history_total as $item) {
            array_push($shipment_id, $item->shipment_id);
        }

        // print_r($shipment_id);

        // $this->db->select('id, shipment_id, country, account_type, sum(item_sub_total_amount) as item_sub_total_amount, sum(item_amount) as item_amount, sale_date, sum(sale_total_amount) as sale_total_amount, payment_status, shipping_status');


        // if($this->input->post('year')){
        //   $this->db->where('year(create_date)', $this->input->post('year'));
        // }

        // $this->db->group_by('shipment_id');
        // $this->db->select('s.*, i.name, i.company');
        // $this->db->from('sales_history s');
        // $this->db->join('shipping_info i', 's.create_by = i.create_by', 'left');
        if ($this->input->post('country')) {
            $this->db->where('country', $this->input->post('country'));
        }
        if (!empty($shipment_id)) {
            $this->db->where_in('shipment_id', $shipment_id);
            $this->db->where("DATE_FORMAT(create_date,'%Y-%m')", "$y-$m");

            $this->db->order_by('id', 'DESC');
            // $q = $this->db->get();
            // $data['sales_history'] = $q->result();
            $data['sales_history'] = $this->datacontrol_model->getAllData('sales_history');
            // $data['sales_history'] = $sales_history_total;
            //echo $this->db->last_query();exit();
            $this->session->set_userdata('admin_sql_export', $this->db->last_query());
        } else {
            $data['sales_history'] = NULL;
            $this->session->set_userdata('admin_sql_export', NULL);
        }

        $data["content_view"] = 'admin/sales_history/sales_history_v';
        $data["menu"] = 'sales_history';
        $data["htmlTitle"] = "Sales History";
        $data['this_country'] = $user->country;

        $this->load->view('admin_template', $data);
    }

    public function data_index()
    {
        $input = $this->input->post();
        $info = $this->sales_history_m->get_rows($input);
        $this->session->set_userdata('admin_sql_export', $this->db->last_query());
        $infoCount = $this->sales_history_m->get_count($input);

        $user = $this->ion_auth->user()->row();
        $this_country = $user->country;

        foreach ($info->result() as $key => $item) {
            if (($this_country == $item->country && $this->ion_auth->in_group(array('SA'))) || $this->ion_auth->in_group(array('admin'))) {
                $disabled = '';
            } else {
                $disabled = 'disabled';
            }
            $Processing = ($item->shipping_status == 'Processing') ? 'selected' : '';
            $Paid = ($item->shipping_status == 'Paid') ? 'selected' : '';
            $Unpaid = ($item->shipping_status == 'Unpaid') ? 'selected' : '';
            $Credit = ($item->shipping_status == 'Credit sale') ? 'selected' : '';
            $Completed = ($item->shipping_status == 'Completed') ? 'selected' : '';
            $Cancel = ($item->shipping_status == 'Cancel') ? 'selected' : '';

            $column[$key]['DT_RowId'] = $key + 1;
            $column[$key]['shipment_id'] = $item->shipment_id;
            $column[$key]['country'] = ucwords(str_replace("_"," ", $item->country));
            $column[$key]['account_type'] = $item->account_type;
            $column[$key]['purchaser_company'] = ($item->purchaser_company == '') ? $item->purchaser_name : $item->purchaser_company;
            $column[$key]['sale_date'] = $item->sale_date;
            $column[$key]['sale_item_id'] = $item->sale_item_id;
            $column[$key]['product_name'] = $item->product_name;
            $column[$key]['item_amount'] = $item->item_amount;
            $column[$key]['sale_total_amount'] = number_format($item->sale_total_amount);
            $column[$key]['payment_method'] = $item->payment_method;
            $column[$key]['action'] = "<select name=\"shipping_status\" onchange=\"change_shipping_status(this);\" data-shipment_id=\"{$item->shipment_id}\" class=\"form-control\" {$disabled}>" .
                "<option value=\"Processing\" {$Processing}>Processing</option>" .
                "<option value=\"Paid\" {$Paid}>Paid</option>" .
                "<option value=\"Unpaid\" {$Unpaid}>Unpaid</option>" .
                "<option value=\"Credit sale\" {$Credit}>Credit sale</option>" .
                "<option value=\"Completed\" {$Completed}>Completed</option>" .
                "<option value=\"Cancel\" {$Cancel}>Cancel</option>" .
                "</select>";
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }


    public function add()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'account_type' => $this->input->post('account_type', TRUE),
            'account_name' => $this->input->post('account_name', TRUE),
            'sale_date' => $this->input->post('sale_date', TRUE),
            'sale_item_category' => $this->input->post('sale_item_category', TRUE),
            'sale_item_id' => $this->input->post('sale_item_id', TRUE),
            'item_amount' => $this->input->post('item_amount', TRUE),
            'item_sub_total_amount' => $this->input->post('item_sub_total_amount', TRUE),
            'delivery_amount' => $this->input->post('delivery_amount', TRUE),
            'discount_amount' => $this->input->post('discount_amount', TRUE),
            'sale_total_amount' => $this->input->post('sale_total_amount', TRUE),
            'purchaser_type' => $this->input->post('purchaser_type', TRUE),
            'purchaser_id' => $this->input->post('purchaser_id', TRUE),
            'shipping_addr' => $this->input->post('shipping_addr', TRUE),
            'payment_status' => $this->input->post('payment_status', TRUE),
            'payment_method' => $this->input->post('payment_method', TRUE),
            'payment_date' => $this->input->post('payment_date', TRUE),
            'shipping_method' => $this->input->post('shipping_method', TRUE),
            'shipping_status' => $this->input->post('shipping_status', TRUE),
            'shipping_date' => $this->input->post('shipping_date', TRUE),
            'emergency_status' => $this->input->post('emergency_status', TRUE),
            'emergency_date' => $this->input->post('emergency_date', TRUE),
        );
        $affected_rows = $this->datacontrol_model->insert('sales_history', $data);
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function edit()
    {
        $data = array(
            'country' => $this->input->post('country', TRUE),
            'account_type' => $this->input->post('account_type', TRUE),
            'account_name' => $this->input->post('account_name', TRUE),
            'sale_date' => $this->input->post('sale_date', TRUE),
            'sale_item_category' => $this->input->post('sale_item_category', TRUE),
            'sale_item_id' => $this->input->post('sale_item_id', TRUE),
            'item_amount' => $this->input->post('item_amount', TRUE),
            'item_sub_total_amount' => $this->input->post('item_sub_total_amount', TRUE),
            'delivery_amount' => $this->input->post('delivery_amount', TRUE),
            'discount_amount' => $this->input->post('discount_amount', TRUE),
            'sale_total_amount' => $this->input->post('sale_total_amount', TRUE),
            'purchaser_type' => $this->input->post('purchaser_type', TRUE),
            'purchaser_id' => $this->input->post('purchaser_id', TRUE),
            'shipping_addr' => $this->input->post('shipping_addr', TRUE),
            'payment_status' => $this->input->post('payment_status', TRUE),
            'payment_method' => $this->input->post('payment_method', TRUE),
            'payment_date' => $this->input->post('payment_date', TRUE),
            'shipping_method' => $this->input->post('shipping_method', TRUE),
            'shipping_status' => $this->input->post('shipping_status', TRUE),
            'shipping_date' => $this->input->post('shipping_date', TRUE),
            'emergency_status' => $this->input->post('emergency_status', TRUE),
            'emergency_date' => $this->input->post('emergency_date', TRUE),
        );
        $data = array(
            '' => $this->input->post('', TRUE),
        );
        $affected_rows = $this->datacontrol_model->update('sales_history', $data, array('id' => $this->input->post('edit_id', TRUE)));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function delete($id)
    {
        $affected_rows = $this->datacontrol_model->delete('sales_history', array('id' => $id));
        if ($affected_rows > 0) {
            echo json_encode(array('error' => 0));
        } else {
            echo json_encode(array('error' => 1));
        }
    }

    public function changeShippingStatus()
    {
        $data['shipping_status'] = $this->input->post('shipping_status');
        $this->datacontrol_model->update('sales_history', $data, array('shipment_id' => $this->input->post('shipment_id', TRUE)));
        $this->datacontrol_model->update('sales_history_total', $data, array('shipment_id' => $this->input->post('shipment_id', TRUE)));
    }

    public function export()
    {
        // $account_setting = $this->datacontrol_model->getAllData('users');
        $selected = 'shipment_id, country, account_type, account_name, sale_date, sale_item_category, buy_type, 
        product_type, sale_item_id, product_name, item_amount, item_sub_total_amount, sale_total_amount, purchaser_type, 
        purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method,
        payment_data, payment_date, shipping_method, shipping_status, create_date';
        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export'));


        $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
        //$new_query =  $query_limit." LIMIT 15";

        $query = $this->db->query($query_none_limit);
        //echo $query->num_rows(); exit();
        $count_row_limit = 3000;
        $files_csv = [];
        if ($query->num_rows() <= 3000) {
            $export = $query->result();
            array_push($files_csv,$this->new_export($export,0));
        } else {
            $new_query = [];
            $newquery = [];
            $export = [];
            $file_number = 0;
            for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
                $file_number++;
                $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
                $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

                $newquery[$i] = $this->db->query($new_query[$i]);
                $export[$i] = $newquery[$i]->result();

                array_push($files_csv,$this->new_export($export[$i],$file_number));

            }

        }

        $data["files_csv"] = $files_csv;
        $this->session->set_userdata('sales_history_download', $data);

        redirect('admin/sales_history/download');

    }

    public function new_export($data,$file_number = 0)
    {
        $table_columns = array();
        foreach ($data[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Account Setting" . date('Y-m-d'))
            ->setSubject("Account Setting" . date('Y-m-d'))
            ->setDescription("Export Account Setting")
            ->setKeywords("Export Account Setting")
            ->setCategory("Export Account Setting");

        // Add some data
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        for ($i = 0; $i < count($data); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet

        $this->excel->getActiveSheet()->setTitle('Export Sales History' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        $callStartTime = microtime(true);

        if($file_number == 0){
            $newFileName = "Export-Sales-History" . "_" . date('Y-m-d') . '_' . time() . '.csv';
        }else{
            $newFileName = "Export-Sales-History" . "_" . date('Y-m-d') . '_' . time(). "(".$file_number.")" . '.csv';
        }

        if (!is_dir('uploads/'.'export/')) {
            mkdir('./uploads/export/', 0777, TRUE);

        }
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save("uploads/export/".$newFileName);
        //$objWriter->save('php://output');
        return "uploads/export/".$newFileName;

    }

    public function download()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'sales_history';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("sales_history_download");
        $this->load->view('admin_template', $data);
    }

    public function export_bk()
    {
        $selected = 'shipment_id, country, account_type, account_name, sale_date, sale_item_category, buy_type, 
        product_type, sale_item_id, product_name, item_amount, item_sub_total_amount, sale_total_amount, purchaser_type, 
        purchaser_country, purchaser_id, purchaser_name, purchaser_company, shipping_addr, payment_status, payment_method,
        payment_data, payment_date, shipping_method, shipping_status, create_date';
        $_set_value_export = str_replace('*', $selected, $this->session->userdata('admin_sql_export'));
        $q = $this->db->query($_set_value_export);
        $sales_history = $q->result();
        $table_columns = array();
        foreach ($sales_history[0] as $key => $value) {
            $table_columns[] = $key;
        }

        $this->load->library('excel');

        // Set document properties
        // echo date('H:i:s') , " Set document properties" , EOL;
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Sales History" . date('Y-m-d'))
            ->setSubject("Sales History" . date('Y-m-d'))
            ->setDescription("Export Sales History")
            ->setKeywords("Export Sales History")
            ->setCategory("Export Sales History");

        // Add some data
        // echo date('H:i:s') , " Add some data" , EOL;
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        // print_r($product_management[0]->id);

        for ($i = 0; $i < count($sales_history); $i++) {
            $excel_row_column = 0;
            foreach ($table_columns as $value) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $sales_history[$i]->$value);
                $excel_row_column++;
            }
            $excel_row++;
        }

        // Rename worksheet
        // echo date('H:i:s') , " Rename worksheet" , EOL;
        $this->excel->getActiveSheet()->setTitle('Export Sales History' . date('y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        // Save Excel 2007 file
        // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
        $callStartTime = microtime(true);

        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

        $newFileName = "Export Sales History" . "_" . date('Y-m-d') . '_' . time() . '.csv';

        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $newFileName . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }



}
