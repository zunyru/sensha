<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_management extends CI_Controller {
  var $user_lang;
  public function __construct(){
    parent::__construct();
    if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array('admin', 'SA', 'FC', 'AA'))){
      redirect('auth/login', 'refresh');
    }
    $this->load->model('database/datacontrol_model');
      $this->load->model('admin/product_general_model','Product_general_m');
      $this->load->model('admin/product_ppf_model','Product_ppf_m');


    $this->user_lang = 'Global';
    if($this->ion_auth->logged_in()){
      $this->user_lang = $this->ion_auth->user()->row()->country;
    }
    if($this->ion_auth->is_admin()){
      $this->user_lang = 'Japan';
    }
    if(!file_exists('application/language/'.strtolower($this->user_lang))){
      $this->user_lang = 'Global';
    }
    $this->lang->load('set', strtolower($this->user_lang));
  }

  public function index(){

    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    $user_country = 'Global';

    /*if($this->ion_auth->in_group(array('SA'))){
      $user_country = $this->ion_auth->user()->row()->country;
      $this->db->where('product_country', $user_country);
    }
    if($this->input->post('country')){
      $this->db->where('product_country', $this->input->post('country'));
    }
    $this->db->order_by('product_id', 'asc');
    $data['product'] = $this->datacontrol_model->getAllData('product_general');*/
    // echo $this->db->last_query();

    // $this->db->where('country', $user_country);
    $this->db->where('cluster_type', 'general');
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');

    // $this->db->where('country', $user_country);
    $this->db->where('cluster_type', 'general');
    $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');

    // $this->db->where('country', $user_country);
    $this->db->where('cluster_type', 'general');
    $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');

    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    $data["content_view"] = 'admin/product_management/product_general_v';
    $data["menu"] = 'product_management';
    $data["htmlTitle"] = "Product Management";

    $this->load->view('admin_template', $data);
  }

  public function data_index()
  {
      $input = $this->input->post();
      $info = $this->Product_general_m->get_rows($input);
      $this->session->set_userdata('admin_sql_export', $this->db->last_query());
      $infoCount = $this->Product_general_m->get_count($input);

      foreach ($info->result() as $key => $item) {

          $action =  "<button type=\"button\" onclick=\"edit_item(this);\" class=\"btn btn-warning btn-edit\" 
                        data-id=\"{$item->id}\" data-product_id=\"{$item->product_id}\" 
                        data-product_name=\"{$item->product_name}\" 
                        data-cat_1=\"{$item->cat_1}\" 
                        data-cat_2=\"{$item->cat_2}\" 
                        data-cat_3=\"{$item->cat_3}\" 
                        data-product_weight=\"{$item->product_weight}\" 
                        data-product_country=\"{$item->product_country}\" 
                        data-global_price=\"{$item->global_price}\" 
                        data-fixed_price=\"{$item->fixed_price}\" 
                        data-sa_price=\"{$item->sa_price}\" 
                        data-import_shipping=\"{$item->import_shipping}\" 
                        data-import_tax=\"{$item->import_tax}\" 
                        data-no_of_use=\"{$item->no_of_use}\" 
                        data-cost_per_use=\"{$item->cost_per_use}\" 
                        data-fixed_delivery_price=\"{$item->fixed_delivery_price}\"
                        data-no_of_stock=\"{$item->no_of_stock}\" 
                        data-item_description=\"{$item->item_description}\" 
                        data-caution=\"{$item->caution}\" 
                        data-content=\"{$item->content}\"  
                        data-youtube=\"{$item->youtube}\" 
                        data-unpurchasable_user=\"{$item->unpurchasable_user}\"  
                        data-image=\"{$item->image}\" 
                        data-material=\"{$item->material}\" 
                        data-usage=\"{$item->usage}\" 
                        data-item_type=\"{$item->item_type}\" 
                        data-feature=\"{$item->feature}\" 
                        data-is_active=\"{$item->is_active}\" 
                        data-is_show_home=\"{$item->is_show_home}\" 
                        data-create_by=\"{$item->create_by}\" 
                        data-update_by=\"{$item->update_by}\" 
                        data-create_date=\"{$item->create_date}\" 
                        data-update_date=\"{$item->update_date}\">
                        <i class=\"fa fa-pencil-square-o\"></i></button>";
          if($this->ion_auth->in_group(array('admin'))) {
              if($item->product_country == 'Global') {
                  $action .= "<button type=\"button\" onclick=\"delete_item(this);\" class=\"btn btn-danger btn-delete\" 
                        data-id=\"{$item->id}\" data-product_name=\"{$item->product_name}\">
                        <i class=\"fa fa-trash-o\"></i></button>";
              }
          }

          $column[$key]['product_id'] = $item->product_id;
          $column[$key]['product_name'] = $item->product_name;
          $column[$key]['product_country'] = ucwords(str_replace("_"," ", $item->product_country));
          $column[$key]['global_price'] = number_format($item->global_price);
          $column[$key]['sa_price'] = number_format($item->sa_price,0);
          $column[$key]['is_active'] = !empty($item->is_active) ? 'active' : '';
          $column[$key]['is_show_home'] = ($item->is_show_home)? 'feature' : '' ;
          $column[$key]['action'] = $action;
      }

      $data['data'] = $column ?? [];
      $data['recordsTotal'] = $info->num_rows();
      $data['recordsFiltered'] = $infoCount;
      $data['draw'] = $input['draw'];
      $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode($data));
  }

  public function ppf(){
    $data['countries'] = $this->datacontrol_model->getAllData('countries');

    /*if($this->ion_auth->in_group(array('SA'))){
      $user_country = $this->ion_auth->user()->row()->country;
      $this->db->where('product_country', $user_country);
    }
    if($this->input->post('country')){
      $this->db->where('product_country', $this->input->post('country'));
    }
    $data['product'] = $this->datacontrol_model->getAllData('product_ppf');*/

    $this->db->where('cluster_type', 'ppf');
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');

    $this->db->where('cluster_type', 'ppf');
    $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');

    $this->db->where('cluster_type', 'ppf');
    $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');

    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    $data["content_view"] = 'admin/product_management/product_ppf_v';
    $data["menu"] = 'product_management';
    $data["htmlTitle"] = "Product Management";

    $this->load->view('admin_template', $data);
  }

    public function data_index_ppf()
    {
        $input = $this->input->post();
        $info = $this->Product_ppf_m->get_rows($input);
        $this->session->set_userdata('admin_sql_export', $this->db->last_query());
        $infoCount = $this->Product_ppf_m->get_count($input);

        foreach ($info->result() as $key => $item) {

            $action =  "<button type=\"button\" onclick=\"edit_item(this);\" class=\"btn btn-warning btn-edit\" 
                        data-id=\"{$item->id}\" data-product_id=\"{$item->product_id}\" 
                        data-product_name=\"{$item->product_name}\" 
                        data-cat_1=\"{$item->cat_1}\" 
                        data-cat_2=\"{$item->cat_2}\" 
                        data-cat_3=\"{$item->cat_3}\" 
                        data-product_weight=\"{$item->product_weight}\" 
                        data-product_country=\"{$item->product_country}\" 
                        data-global_price=\"{$item->global_price}\" 
                        data-fixed_price=\"{$item->fixed_price}\" 
                        data-sa_price=\"{$item->sa_price}\" 
                        data-import_shipping=\"{$item->import_shipping}\" 
                        data-import_tax=\"{$item->import_tax}\" 
                        data-no_of_use=\"{$item->no_of_use}\" 
                        data-cost_per_use=\"{$item->cost_per_use}\" 
                        data-fixed_delivery_price=\"{$item->fixed_delivery_price}\"
                        
                        data-parts=\"{$item->parts}\" 
                        data-domestic=\"{$item->domestic}\"
                        data-oversea=\"{$item->oversea}\"
                         
                        data-no_of_stock=\"{$item->no_of_stock}\" 
                        data-item_description=\"{$item->item_description}\" 
                        
                        data-car_production_id=\"{$item->car_production_id}\"
                        data-begining_term_to_use=\"{$item->begining_term_to_use}\"
                        data-ending_term_to_use=\"{$item->ending_term_to_use}\"
                        data-html_content=\"{$item->html_content}\"
                        data-registration_date=\"{$item->registration_date}\"
                        data-last_modification_date=\"{$item->last_modification_date}\"
                        
                        data-caution=\"{$item->caution}\" 
                        data-content=\"{$item->content}\"  
                        data-youtube=\"{$item->youtube}\" 
                        data-unpurchasable_user=\"{$item->unpurchasable_user}\" 
                        data-image=\"{$item->image}\"  
                 
                        data-feature=\"{$item->feature}\" 
                        data-is_active=\"{$item->is_active}\" 
                        data-is_show_home=\"{$item->is_show_home}\" 
                        data-create_by=\"{$item->create_by}\" 
                        data-update_by=\"{$item->update_by}\" 
                        data-create_date=\"{$item->create_date}\" 
                        data-update_date=\"{$item->update_date}\">
                        <i class=\"fa fa-pencil-square-o\"></i></button>";
            if($this->ion_auth->in_group(array('admin'))) {
                if($item->product_country == 'Global') {
                    $action .= "<button type=\"button\" onclick=\"delete_item(this);\" class=\"btn btn-danger btn-delete\" 
                        data-id=\"{$item->id}\" data-product_name=\"{$item->product_name}\">
                        <i class=\"fa fa-trash-o\"></i></button>";
                }
            }

            $column[$key]['product_id'] = $item->product_id;
            $column[$key]['product_name'] = $item->product_name;
            $column[$key]['product_country'] = ucwords(str_replace("_"," ", $item->product_country));
            $column[$key]['global_price'] = number_format($item->global_price);
            $column[$key]['sa_price'] = number_format($item->sa_price,0);
            $column[$key]['is_active'] = !empty($item->is_active) ? 'active' : '';
            $column[$key]['action'] = $action;
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

  public function add_general(){
    // foreach ($_POST as $key => $value) {
    //   echo "'$key' => this->input->post('$key', TRUE),<br />";
    // }
    // exit();
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_weight' => $this->input->post('product_weight', TRUE),
      'product_country' => 'Global',
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE) ?? null,
      'import_tax' => $this->input->post('import_tax', TRUE) ?? null,
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'material' => implode(',', $this->input->post('material', TRUE)),
      'usage' => implode(',', $this->input->post('usage', TRUE)),
      'item_type' => $this->input->post('item_type', TRUE),
      'feature' => implode(',', $this->input->post('feature', TRUE)),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      // 'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );

    $country = $this->datacontrol_model->getAllData('nation_lang');
    $this->db->where('id', $this->input->post('cat_1'));
    $cat_1 = $this->datacontrol_model->getRowData('cluster1');
    $this->db->where('id', $this->input->post('cat_2'));
    $cat_2 = $this->datacontrol_model->getRowData('cluster2');
    $this->db->where('id', $this->input->post('cat_3'));
    $cat_3 = $this->datacontrol_model->getRowData('cluster3');
    // echo "<pre>";
    // print_r($cat_1);
    foreach($country as $item){
      $data['product_country'] = $item->country;

      $this->db->where('cluster_url', $cat_1->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_1 = $this->datacontrol_model->getRowData('cluster1');

      $this->db->where('cluster_url', $cat_2->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_2 = $this->datacontrol_model->getRowData('cluster2');

      $this->db->where('cluster_url', $cat_3->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_3 = $this->datacontrol_model->getRowData('cluster3');

      $data['cat_1'] = $data_cat_1->id;
      $data['cat_2'] = $data_cat_2->id;
      $data['cat_3'] = $data_cat_3->id;

      // print_r($data);

      $affected_rows = $this->datacontrol_model->insert('product_general', $data);
    }

    // $affected_rows = $this->datacontrol_model->insert('product_general', $data);

    // echo count($data);

    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit_general(){
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }

    // $data = array(
    //   'product_id' => $this->input->post('product_id', TRUE),
    //   'product_name' => $this->input->post('product_name', TRUE),
    //   'cat_1' => $this->input->post('cat_1', TRUE),
    //   'cat_2' => $this->input->post('cat_2', TRUE),
    //   'cat_3' => $this->input->post('cat_3', TRUE),
    //   'product_weight' => $this->input->post('product_weight', TRUE),
    //   // 'product_country' => $this->input->post('product_country', TRUE),
    //   'global_price' => $this->input->post('global_price', TRUE),
    //   'fixed_price' => $this->input->post('fixed_price', TRUE),
    //   'sa_price' => $this->input->post('sa_price', TRUE),
    //   'import_shipping' => $this->input->post('import_shipping', TRUE),
    //   'import_tax' => $this->input->post('import_tax', TRUE),
    //   'no_of_use' => $this->input->post('no_of_use', TRUE),
    //   'cost_per_use' => $this->input->post('cost_per_use', TRUE),
    //   'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
    //   'material' => implode(',', $this->input->post('material', TRUE)),
    //   'usage' => implode(',', $this->input->post('usage', TRUE)),
    //   'item_type' => $this->input->post('item_type', TRUE),
    //   'feature' => implode(',', $this->input->post('feature', TRUE)),
    //   'no_of_stock' => $this->input->post('no_of_stock', TRUE),
    //   'item_description' => $this->input->post('item_description', TRUE),
    //   'caution' => $this->input->post('caution', TRUE),
    //   'content' => $this->input->post('content', TRUE),
    //   // 'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
    //   'youtube' => $this->input->post('youtube', TRUE),
    //   'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
    //   'is_active' => $this->input->post('is_active', TRUE),
    //   'is_show_home' => $this->input->post('is_show_home', TRUE),
    //   // 'registration_date' => $this->input->post('registration_date', TRUE),
    //   // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
    //   'image' => $this->input->post('set_product_image'),
    // );
    // $this->output->enable_profiler(TRUE); 
    // echo "<pre>";

    $data = array(
      'product_name' => $this->input->post('product_name', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE) ?? null,
      'import_tax' => $this->input->post('import_tax', TRUE),
      'item_description' => $this->input->post('item_description', TRUE) ?? null,
      'content' => $this->input->post('content', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'image' => $this->input->post('set_product_image'),
      'youtube' => $this->input->post('youtube', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
    );
    // print_r($data);

    $affected_rows = $this->datacontrol_model->update('product_general', $data, array('id' => $this->input->post('edit_id', TRUE)));

    if ($this->ion_auth->is_admin()){
      $country = $this->datacontrol_model->getAllData('nation_lang');
      $this->db->where('id', $this->input->post('cat_1'));
      $cat_1 = $this->datacontrol_model->getRowData('cluster1');
      $this->db->where('id', $this->input->post('cat_2'));
      $cat_2 = $this->datacontrol_model->getRowData('cluster2');
      $this->db->where('id', $this->input->post('cat_3'));
      $cat_3 = $this->datacontrol_model->getRowData('cluster3');
      
      // print_r($cat_1);
      foreach ($country as $item) {
        // $data['product_country'] = $item->country;
        // echo $this->input->post('product_id', TRUE);
        // echo $item->country;

        $this->db->where('cluster_url', $cat_1->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_1 = $this->datacontrol_model->getRowData('cluster1');

        $this->db->where('cluster_url', $cat_2->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_2 = $this->datacontrol_model->getRowData('cluster2');

        $this->db->where('cluster_url', $cat_3->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_3 = $this->datacontrol_model->getRowData('cluster3');

        // $data['cat_1'] = $data_cat_1->id;
        // $data['cat_2'] = $data_cat_2->id;
        // $data['cat_3'] = $data_cat_3->id;

        $admin_data = array(
          "cat_1" => $data_cat_1->id,
          "cat_2" => $data_cat_2->id,
          "cat_3" => $data_cat_3->id,
          'product_weight' => $this->input->post('product_weight', TRUE),
          'global_price' => $this->input->post('global_price', TRUE),
          'no_of_use' => $this->input->post('no_of_use', TRUE),
          'material' => implode(',', $this->input->post('material', TRUE)),
          'usage' => implode(',', $this->input->post('usage', TRUE)),
          'item_type' => $this->input->post('item_type', TRUE),
          'feature' => implode(',', $this->input->post('feature', TRUE)),
        );

        // print_r($admin_data);
      

        $affected_rows = $this->datacontrol_model->update('product_general', $admin_data, array('product_id' => $this->input->post('product_id', TRUE), 'product_country' => $item->country));
      }
      
      
    }

    // print_r($data);

    // $affected_rows = $this->datacontrol_model->update('product_general', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    // echo $this->db->last_query();

    // if($this->ion_auth->is_admin()){
    //   unset($data['cat_1'], $data['cat_2'], $data['cat_3'], $data['cat_1'], $data['sa_price'] );
    //   $affected_rows = $this->datacontrol_model->update('product_general', $data, array('product_id'=>$data['product_id']));
    // }



    // 'product_id' => $this->input->post('product_id', TRUE)
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function add_ppf(){
  	// foreach ($_POST as $key => $value) {
  	//   echo "'$key' => this->input->post('$key', TRUE),<br />";
  	// }
  	// exit();
  	if (!file_exists('uploads/product_image/')) {
  		mkdir("uploads/product_image/", 0777);
  	}
  	$product_image = explode(',', $this->input->post('set_product_image'));
  	foreach($product_image as $item) {
  		if(file_exists("uploads/temp/$item")) {
  			@copy("uploads/temp/$item", "uploads/product_image/$item");
  			@unlink("uploads/temp/$item");
  		}
  	}
  	$data = array(
  		'product_id' => $this->input->post('product_id', TRUE),
  		'product_name' => $this->input->post('product_name', TRUE),
  		'cat_1' => $this->input->post('cat_1', TRUE),
  		'cat_2' => $this->input->post('cat_2', TRUE),
  		//'cat_3' => $this->input->post('cat_3', TRUE),
        'product_weight' => $this->input->post('product_weight', TRUE),
  		'product_country' => 'Global',
  		'global_price' => $this->input->post('global_price', TRUE),
  		'fixed_price' => $this->input->post('fixed_price', TRUE),
  		'sa_price' => $this->input->post('sa_price', TRUE),
  		'import_shipping' => $this->input->post('import_shipping', TRUE),
  		'import_tax' => $this->input->post('import_tax', TRUE),
  		'no_of_use' => $this->input->post('no_of_use', TRUE),
  		'cost_per_use' => 0,
  		'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
  		'parts' => $this->input->post('parts', TRUE),
  		'feature' => $this->input->post('feature', TRUE),
  		'no_of_stock' => $this->input->post('no_of_stock', TRUE),
  		'item_description' => $this->input->post('item_description', TRUE),
  		'car_production_id' => 0,
  		'begining_term_to_use' => $this->input->post('begining_term_to_use', TRUE),
  		'ending_term_to_use' => $this->input->post('ending_term_to_use', TRUE),
  		'caution' => $this->input->post('caution', TRUE),
  		'content' => $this->input->post('content', TRUE),
  		'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
  		'youtube' => $this->input->post('youtube', TRUE),
  		'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
  		'is_active' => $this->input->post('is_active', TRUE),
        'is_show_home' => $this->input->post('is_show_home', TRUE),
  		// 'registration_date' => $this->input->post('registration_date', TRUE),
  		// 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
  		'image' => $this->input->post('set_product_image'),
  	);

    $country = $this->datacontrol_model->getAllData('nation_lang');
    $this->db->where('id', $this->input->post('cat_1'));
    $cat_1 = $this->datacontrol_model->getRowData('cluster1');
    $this->db->where('id', $this->input->post('cat_2'));
    $cat_2 = $this->datacontrol_model->getRowData('cluster2');
    $this->db->where('id', $this->input->post('cat_3'));
    $cat_3 = $this->datacontrol_model->getRowData('cluster3');
    foreach($country as $item){
      $data['product_country'] = $item->country;

      $this->db->where('cluster_url', $cat_1->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_1 = $this->datacontrol_model->getRowData('cluster1');

      $this->db->where('cluster_url', $cat_2->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_2 = $this->datacontrol_model->getRowData('cluster2');

      $this->db->where('cluster_url', $cat_3->cluster_url);
      $this->db->where('country', $item->country);
      $data_cat_3 = $this->datacontrol_model->getRowData('cluster3');

      $data['cat_1'] = $data_cat_1->id;
      $data['cat_2'] = $data_cat_2->id;
      $data['cat_3'] = $data_cat_3->id;

      $affected_rows = $this->datacontrol_model->insert('product_ppf', $data);
    }

    // $affected_rows = $this->datacontrol_model->insert('product_ppf', $data);


  	// echo count($data);

  	if($affected_rows > 0){
  		echo json_encode(array('error' => 0));
  	}
  	else{
  		echo json_encode(array('error' => 1));
  	}
  }

  public function edit_ppf(){
  	if (!file_exists('uploads/product_image/')) {
  		mkdir("uploads/product_image/", 0777);
  	}
  	$product_image = explode(',', $this->input->post('set_product_image'));
  	foreach($product_image as $item) {
  		if(file_exists("uploads/temp/$item")) {
  			@copy("uploads/temp/$item", "uploads/product_image/$item");
  			@unlink("uploads/temp/$item");
  		}
  	}

      $data = array(
      'product_name' => $this->input->post('product_name', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'car_production_id' => 0,
      'content' => $this->input->post('content', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'image' => $this->input->post('set_product_image'),
      'youtube' => $this->input->post('youtube', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      );

    $affected_rows = $this->datacontrol_model->update('product_ppf', $data, array('id' => $this->input->post('edit_id', TRUE)));


    if ($this->ion_auth->is_admin()) {
      $country = $this->datacontrol_model->getAllData('nation_lang');
      $this->db->where('id', $this->input->post('cat_1'));
      $cat_1 = $this->datacontrol_model->getRowData('cluster1');
      $this->db->where('id', $this->input->post('cat_2'));
      $cat_2 = $this->datacontrol_model->getRowData('cluster2');
      $this->db->where('id', $this->input->post('cat_3'));
      $cat_3 = $this->datacontrol_model->getRowData('cluster3');

      // print_r($cat_1);

      foreach ($country as $item) {
        // $data['product_country'] = $item->country;
        // echo $this->input->post('product_id', TRUE);
        // echo $item->country;

        $this->db->where('cluster_url', $cat_1->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_1 = $this->datacontrol_model->getRowData('cluster1');

        $this->db->where('cluster_url', $cat_2->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_2 = $this->datacontrol_model->getRowData('cluster2');

        $this->db->where('cluster_url', $cat_3->cluster_url);
        $this->db->where('country', $item->country);
        $data_cat_3 = $this->datacontrol_model->getRowData('cluster3');

        $admin_data = array(
          "cat_1" => $data_cat_1->id,
          "cat_2" => $data_cat_2->id,
          "cat_3" => $data_cat_3->id,
          'product_weight' => $this->input->post('product_weight', TRUE),
          'global_price' => $this->input->post('global_price', TRUE),
          'no_of_use' => $this->input->post('no_of_use', TRUE),
          'cost_per_use' => 0,
          'parts' => $this->input->post('parts', TRUE),
          'feature' => implode(',', $this->input->post('feature', TRUE)),
        );

        //print_r($admin_data);


        $affected_rows = $this->datacontrol_model->update('product_ppf', $admin_data, array('product_id' => $this->input->post('product_id', TRUE), 'product_country' => $item->country));

      }
     
    }

    // $affected_rows = $this->datacontrol_model->update('product_ppf', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    // echo $this->db->last_query();

    // if($this->ion_auth->is_admin()){
    //   unset($data['cat_1'], $data['cat_2'], $data['cat_3'], $data['cat_1'], $data['sa_price'] );
    //   $affected_rows = $this->datacontrol_model->update('product_ppf', $data, array('product_id'=>$data['product_id']));
    // }
    // 'product_id' => $this->input->post('product_id', TRUE)
  	if($affected_rows > 0){
  		echo json_encode(array('error' => 0));
  	}
  	else{
  		echo json_encode(array('error' => 1));
  	}
  }

  /*public function delete_general($id){
  	$affected_rows = $this->datacontrol_model->delete('product_general', array('id'=>$id));
  	if($affected_rows > 0){
  		echo json_encode(array('error' => 0));
  	}
  	else{
  		echo json_encode(array('error' => 1));
  	}
  }*/

  /*public function delete_ppf($id){
    $affected_rows = $this->datacontrol_model->delete('product_ppf', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }*/

  public function delete_general($id){

    $rows =  $this->datacontrol_model->getRowData('product_general', array('id'=>$id));
    $product_id = $rows->product_id;
    
    $affected_rows = $this->datacontrol_model->delete('product_general', array('product_id'=>$product_id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete_ppf($id){
     $rows =  $this->datacontrol_model->getRowData('product_ppf', array('id'=>$id));
    $product_id = $rows->product_id;
    
    $affected_rows = $this->datacontrol_model->delete('product_ppf', array('product_id'=>$product_id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function import_general(){
  	// print_r($_FILES);
  	if(!empty($_FILES['file_import']['name'])){
  		if (!file_exists('uploads/csv_import/')) {
  			mkdir("uploads/csv_import/", 0777);
  		}
  		// Set preference
  		$config['upload_path'] = 'uploads/csv_import/';
  		$config['allowed_types'] = 'csv';
  		// $config['max_size'] = '1000'; // max_size in kb
  		// $config['file_name'] = $_FILES['file_lang']['name'];
  		$config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


  		// Load upload library
  		$this->load->library('upload',$config);
  		$this->upload->initialize($config);


  		// File upload
  		if($this->upload->do_upload('file_import')){
  			// Get data about the file
  			$uploadData = $this->upload->data();
  			$filename = $uploadData['file_name'];

  			// Reading file
  			$file = fopen("uploads/csv_import/".$filename,"r");
  			$i = 1;

  			$importData_arr = array();
  			$text = "";
  			$fieldTitle = array();
  			// $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

  			while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
  				$data = array();
  				if($i == 1){
  					$fieldTitle = $filedata;
  				}
  				if($i>1){

  					$x=0;
  					foreach ($fieldTitle as $key => $item) {
  						if($key != 0){
                if($item != 'create_date' && $item != 'update_date'){
                  $data[$item] = $filedata[$x];
                }
  						}
  						$x++;
  					}
  					// print_r($data);
  					// echo $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

  					$this->db->where('product_id', $data['product_id']);
  					$query = $this->db->get('product_general');
  					if(count($query->row()) > 0){
  						$this->datacontrol_model->update('product_general', $data, array('product_id' => $data['product_id'], 'product_country' => $data['product_country']));
  					}
  					else{
              $country = $this->datacontrol_model->getAllData('nation_lang');
              foreach($country as $item){
                $data['product_country'] = $item->country;
                $this->datacontrol_model->insert('product_general', $data);
              }
  						// $this->datacontrol_model->insert('product_general', $data);
  					}

  				}
  				$i++;
  			}

  			fclose($file);
  			@unlink("uploads/csv_import/".$filename);
        update_cat();
  			echo json_encode(array('error' => 0));
  		}
  		else{
  			// echo $this->upload->display_errors();
  			echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
  		}

  	}
  	else{
  		echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
  	}
  }

  public function import_ppf(){
  	// print_r($_FILES);
  	if(!empty($_FILES['file_import']['name'])){
  		if (!file_exists('uploads/csv_import/')) {
  			mkdir("uploads/csv_import/", 0777);
  		}
  		// Set preference
  		$config['upload_path'] = 'uploads/csv_import/';
  		$config['allowed_types'] = 'csv';
  		// $config['max_size'] = '1000'; // max_size in kb
  		// $config['file_name'] = $_FILES['file_lang']['name'];
  		$config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


  		// Load upload library
  		$this->load->library('upload',$config);
  		$this->upload->initialize($config);


  		// File upload
  		if($this->upload->do_upload('file_import')){
  			// Get data about the file
  			$uploadData = $this->upload->data();
  			$filename = $uploadData['file_name'];

  			// Reading file
  			$file = fopen("uploads/csv_import/".$filename,"r");
  			$i = 1;

  			$importData_arr = array();
  			$fieldTitle = array();
  			// $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

  			while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
  				$data = array();
  				if($i == 1){
  					$fieldTitle = $filedata;
  				}
  				if($i>1){

  					$x=0;
  					foreach ($fieldTitle as $key => $item) {
  						if($key != 0){
                if($item != 'create_date' && $item != 'update_date'){
                  $data[$item] = $filedata[$x];
                }
  						}
  						$x++;
  					}
  					// print_r($data);
  					// $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

  					$this->db->where('product_id', $data['product_id']);
  					$query = $this->db->get('product_ppf');
  					if(count($query->row()) > 0){
  						$this->datacontrol_model->update('product_ppf', $data, array('product_id' => $data['product_id'], 'product_country' => $data['product_country']));
  					}
  					else{
              $country = $this->datacontrol_model->getAllData('nation_lang');
              foreach($country as $item){
                $data['product_country'] = $item->country;
                $this->datacontrol_model->insert('product_ppf', $data);
              }
  						// $this->datacontrol_model->insert('product_ppf', $data);
  					}

  				}
  				$i++;
  			}

  			fclose($file);
  			@unlink("uploads/csv_import/".$filename);
        update_cat();
  			echo json_encode(array('error' => 0));
  		}
  		else{
  			// echo $this->upload->display_errors();
  			echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
  		}

  	}
  	else{
  		echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
  	}
  }

  public function export_general()
  {
      $_set_value_export = $this->session->userdata('admin_sql_export');
      $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
      $query = $this->db->query($query_none_limit);
      $count_row_limit = 3000;
      $files_csv = [];
      if ($query->num_rows() <= 3000) {
          $export = $query->result();
          array_push($files_csv,$this->new_export($export,0,"General"));
      } else {
          $new_query = [];
          $newquery = [];
          $export = [];
          $file_number = 0;
          for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
              $file_number++;
              $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
              $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

              $newquery[$i] = $this->db->query($new_query[$i]);
              $export[$i] = $newquery[$i]->result();

              array_push($files_csv,$this->new_export($export[$i],$file_number,"General"));

          }
      }
      $data["files_csv"] = $files_csv;
      $this->session->set_userdata('product_management_general_download', $data);

      redirect('admin/product_management/download_general');
  }

    public function export_ppf()
    {
        $_set_value_export = $this->session->userdata('admin_sql_export');
        $query_none_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
        $query = $this->db->query($query_none_limit);
        $count_row_limit = 3000;
        $files_csv = [];
        if ($query->num_rows() <= 3000) {
            $export = $query->result();
            array_push($files_csv,$this->new_export($export,0,"PPF"));
        } else {
            $new_query = [];
            $newquery = [];
            $export = [];
            $file_number = 0;
            for ($i = 0; $i < ceil($query->num_rows() / $count_row_limit); $i++) {
                $file_number++;
                $query_limit = substr($_set_value_export, 0, strpos($_set_value_export, "LIMIT"));
                $new_query[$i] = $query_limit . " LIMIT " . $count_row_limit . " OFFSET " . ($i * $count_row_limit);

                $newquery[$i] = $this->db->query($new_query[$i]);
                $export[$i] = $newquery[$i]->result();

                array_push($files_csv,$this->new_export($export[$i],$file_number,"General"));

            }
        }
        $data["files_csv"] = $files_csv;
        $this->session->set_userdata('product_management_ppf_download', $data);

        redirect('admin/product_management/download_ppf');
    }

    public function new_export($data,$file_number = 0,$name)
    {
        $table_columns = array();

        $unuse = array('update_by', 'create_date', 'update_date');
        foreach ($data[0] as $key => $value) {
            if (!in_array($key, $unuse)){
                $table_columns[] = $key;
            }
        }

        $this->load->library('excel');

        // Set document properties
        $this->excel->getProperties()->setCreator("Sensha")
            ->setLastModifiedBy("Sensha")
            ->setTitle("Account Setting" . date('Y-m-d'))
            ->setSubject("Account Setting" . date('Y-m-d'))
            ->setDescription("Export Account Setting")
            ->setKeywords("Export Account Setting")
            ->setCategory("Export Account Setting");

        // Add some data
        $column = 0;
        foreach ($table_columns as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }

        $excel_row = 2;

        if ($this->ion_auth->in_group(array('admin'))) {
            for($i=0; $i < count($data); $i++) {
                $excel_row_column = 0;
                foreach ($table_columns as $value) {
                    $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                    $excel_row_column++;
                }
                $excel_row++;
            }
        }else{
            for($i=0; $i < count($data); $i++) {
                if($data[$i]->product_country==$this->user_lang){
                    $excel_row_column = 0;
                    foreach ($table_columns as $value) {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $data[$i]->$value);
                        $excel_row_column++;
                    }
                    $excel_row++;
                }
            }
        }

        // Rename worksheet
        if($name == "General"){
            $this->excel->getActiveSheet()->setTitle('Export Product General' . date('y'));
        }else{
            $this->excel->getActiveSheet()->setTitle('Export Product PPF' . date('y'));
        }

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excel->setActiveSheetIndex(0);

        $callStartTime = microtime(true);
        if($name == "General") {
            if ($file_number == 0) {
                $newFileName = "Export-Product-General" . "_" . date('Y-m-d') . '_' . time() . '.csv';
            } else {
                $newFileName = "Export-Product-General" . "_" . date('Y-m-d') . '_' . time() . "(" . $file_number . ")" . '.csv';
            }
        }else{
            if ($file_number == 0) {
                $newFileName = "Export-Product-PPF" . "_" . date('Y-m-d') . '_' . time() . '.csv';
            } else {
                $newFileName = "Export-Product-PPF" . "_" . date('Y-m-d') . '_' . time() . "(" . $file_number . ")" . '.csv';
            }
        }

        if (!is_dir('uploads/'.'export/')) {
            mkdir('./uploads/export/', 0777, TRUE);

        }
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        $objWriter->save("uploads/export/".$newFileName);
        //$objWriter->save('php://output');
        return "uploads/export/".$newFileName;

    }

    public function download_general()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'product_management';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("product_management_general_download");
        $this->load->view('admin_template', $data);
    }

    public function download_ppf()
    {
        $data["content_view"] = 'admin/download_v';
        $data["menu"] = 'product_management';
        $data["htmlTitle"] = "Nation & Lang";
        $data["files"] = $this->session->userdata("product_management_ppf_download");
        $this->load->view('admin_template', $data);
    }

  public function export_general_bk(){
    $product_export = $this->datacontrol_model->getAllData('product_general');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }

    $excel_row =2;

    // print_r($product_management[0]->id);
   if ($this->ion_auth->in_group(array('admin'))) {
	   for($i=0; $i < count($product_export); $i++) {
		           $excel_row_column = 0;
			      foreach ($table_columns as $value) {
			        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
			        $excel_row_column++;
			      }
			      $excel_row++;
		   }
	 }else{
		    for($i=0; $i < count($product_export); $i++) {
		      if($product_export[$i]->product_country==$this->user_lang){
			      $excel_row_column = 0;
			      foreach ($table_columns as $value) {
			        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
			        $excel_row_column++;
			      }
			      $excel_row++;
			  }
		    }
    }

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product General'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product General"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }

  public function export_ppf_bk(){
    $product_export = $this->datacontrol_model->getAllData('product_ppf');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }

    $excel_row =2;
  
    // print_r($product_management[0]->id);
	if ($this->ion_auth->in_group(array('admin'))) {
	       
			for($i=0; $i < count($product_export); $i++) {
			      $excel_row_column = 0;
			      foreach ($table_columns as $value) {
			        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
			        $excel_row_column++;
			      }
			      $excel_row++;
		    }
	 }else{
		 for($i=0; $i < count($product_export); $i++) {
		      if($product_export[$i]->product_country==$this->user_lang){
			      $excel_row_column = 0;
			      foreach ($table_columns as $value) {
			        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
			        $excel_row_column++;
			      }
			      $excel_row++;
			  }
		    }
	 }
    

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product PPF'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product PPF"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }




  // public function index(){
  //   if($this->ion_auth->in_group(array('SA'))){
  //     redirect('admin/product_management/sa_general', 'refresh');
  //   }
  //   $data['countries'] = $this->datacontrol_model->getAllData('countries');
  //   $data['product'] = $this->datacontrol_model->getAllData('product_hq_general');
  //   $this->db->where('cluster_type', 'general');
  //   $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');
  //   $this->db->where('cluster_type', 'general');
  //   $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');
  //   $this->db->where('cluster_type', 'general');
  //   $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');
  //   $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
  //
  //   $data["content_view"] = 'admin/product_management/product_hq_general_v';
  //   $data["menu"] = 'product_management';
  //   $data["htmlTitle"] = "Product Management";
  //
  //   $this->load->view('admin_template', $data);
  // }
  //
  // public function hq_ppf(){
  //   $data['countries'] = $this->datacontrol_model->getAllData('countries');
  //   $data['product'] = $this->datacontrol_model->getAllData('product_hq_ppf');
  //   $this->db->where('cluster_type', 'ppf');
  //   $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');
  //   $this->db->where('cluster_type', 'ppf');
  //   $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');
  //   $this->db->where('cluster_type', 'ppf');
  //   $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');
  //   $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');
  //
  //   $data["content_view"] = 'admin/product_management/product_hq_ppf_v';
  //   $data["menu"] = 'product_management';
  //   $data["htmlTitle"] = "Product Management";
  //
  //   $this->load->view('admin_template', $data);
  // }

  public function sa_general(){
    $user_country = $this->ion_auth->user()->row()->country;
    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    if($this->input->post('country')){
      $this->db->where('product_country', $this->input->post('country'));
    }
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('product_country', $user_country);
    }
    $data['product'] = $this->datacontrol_model->getAllData('product_sa_general');
    $this->db->where('cluster_type', 'general');
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');
    $this->db->where('cluster_type', 'general');
    $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');
    $this->db->where('cluster_type', 'general');
    $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    $data["content_view"] = 'admin/product_management/product_sa_general_v';
    $data["menu"] = 'product_management';
    $data["htmlTitle"] = "Product Management";

    $this->load->view('admin_template', $data);
  }

  public function sa_ppf(){
    $user_country = $this->ion_auth->user()->row()->country;
    $data['countries'] = $this->datacontrol_model->getAllData('countries');
    if($this->input->post('country')){
      $this->db->where('product_country', $this->input->post('country'));
    }
    if($this->ion_auth->in_group(array('SA'))){
      $this->db->where('product_country', $user_country);
    }
    $data['product'] = $this->datacontrol_model->getAllData('product_sa_ppf');
    $this->db->where('cluster_type', 'ppf');
    $data['cluster1'] = $this->datacontrol_model->getAllData('cluster1');
    $this->db->where('cluster_type', 'ppf');
    $data['cluster2'] = $this->datacontrol_model->getAllData('cluster2');
    $this->db->where('cluster_type', 'ppf');
    $data['cluster3'] = $this->datacontrol_model->getAllData('cluster3');
    $data['nation_lang'] = $this->datacontrol_model->getAllData('nation_lang');

    $data["content_view"] = 'admin/product_management/product_sa_ppf_v';
    $data["menu"] = 'product_management';
    $data["htmlTitle"] = "Product Management";

    $this->load->view('admin_template', $data);
  }

  public function add_hq_general(){
    // foreach ($_POST as $key => $value) {
    //   echo "'$key' => this->input->post('$key', TRUE),<br />";
    // }
    // exit();
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'material' => $this->input->post('material', TRUE),
      'usage' => $this->input->post('usage', TRUE),
      'item_type' => $this->input->post('item_type', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );

    $users_sa = $this->ion_auth->users('SA')->result();
    foreach ($users_sa as $item) {
      $this->db->set('create_date', 'NOW()', FALSE);
      $this->db->set('create_by', $item->id);
      $this->db->set('update_by', $item->id);
      $this->db->insert('product_sa_general', $data);
    }

    // echo count($data);
    $affected_rows = $this->datacontrol_model->insert('product_hq_general', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit_hq_general(){
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'material' => $this->input->post('material', TRUE),
      'usage' => $this->input->post('usage', TRUE),
      'item_type' => $this->input->post('item_type', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );

    $affected_rows = $this->datacontrol_model->update('product_hq_general', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function add_hq_ppf(){
    // foreach ($_POST as $key => $value) {
    //   echo "'$key' => this->input->post('$key', TRUE),<br />";
    // }
    // exit();
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'parts' => $this->input->post('parts', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'car_production_id' => 0,
      'begining_term_to_use' => $this->input->post('begining_term_to_use', TRUE),
      'ending_term_to_use' => $this->input->post('ending_term_to_use', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );

    $users_sa = $this->ion_auth->users('SA')->result();
    foreach ($users_sa as $item) {
      $this->db->set('create_date', 'NOW()', FALSE);
      $this->db->set('create_by', $item->id);
      $this->db->set('update_by', $item->id);
      $this->db->insert('product_sa_ppf', $data);
    }

    // echo count($data);
    $affected_rows = $this->datacontrol_model->insert('product_hq_ppf', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit_hq_ppf(){
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'parts' => $this->input->post('parts', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'car_production_id' => 0,
      'begining_term_to_use' => $this->input->post('begining_term_to_use', TRUE),
      'ending_term_to_use' => $this->input->post('ending_term_to_use', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );
    $affected_rows = $this->datacontrol_model->update('product_hq_ppf', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function add_sa_general(){
    // foreach ($_POST as $key => $value) {
    //   echo "'$key' => this->input->post('$key', TRUE),<br />";
    // }
    // exit();
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'material' => $this->input->post('material', TRUE),
      'usage' => $this->input->post('usage', TRUE),
      'item_type' => $this->input->post('item_type', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );
    // echo count($data);
    $affected_rows = $this->datacontrol_model->insert('product_sa_general', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit_sa_general(){
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'material' => $this->input->post('material', TRUE),
      'usage' => $this->input->post('usage', TRUE),
      'item_type' => $this->input->post('item_type', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );
    $affected_rows = $this->datacontrol_model->update('product_sa_general', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function add_sa_ppf(){
    // foreach ($_POST as $key => $value) {
    //   echo "'$key' => this->input->post('$key', TRUE),<br />";
    // }
    // exit();
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'parts' => $this->input->post('parts', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'car_production_id' => 0,
      'begining_term_to_use' => $this->input->post('begining_term_to_use', TRUE),
      'ending_term_to_use' => $this->input->post('ending_term_to_use', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );
    // echo count($data);
    $affected_rows = $this->datacontrol_model->insert('product_sa_ppf', $data);
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function edit_sa_ppf(){
    if (!file_exists('uploads/product_image/')) {
      mkdir("uploads/product_image/", 0777);
    }
    $product_image = explode(',', $this->input->post('set_product_image'));
    foreach($product_image as $item) {
      if(file_exists("uploads/temp/$item")) {
        @copy("uploads/temp/$item", "uploads/product_image/$item");
        @unlink("uploads/temp/$item");
      }
    }
    $data = array(
      'product_id' => $this->input->post('product_id', TRUE),
      'product_name' => $this->input->post('product_name', TRUE),
      'cat_1' => $this->input->post('cat_1', TRUE),
      'cat_2' => $this->input->post('cat_2', TRUE),
      //'cat_3' => $this->input->post('cat_3', TRUE),
      'product_country' => $this->input->post('product_country', TRUE),
      'global_price' => $this->input->post('global_price', TRUE),
      'fixed_price' => $this->input->post('fixed_price', TRUE),
      'sa_price' => $this->input->post('sa_price', TRUE),
      'import_shipping' => $this->input->post('import_shipping', TRUE),
      'import_tax' => $this->input->post('import_tax', TRUE),
      'no_of_use' => $this->input->post('no_of_use', TRUE),
      'cost_per_use' => 0,
      'fixed_delivery_price' => $this->input->post('fixed_delivery_price', TRUE),
      'parts' => $this->input->post('parts', TRUE),
      'feature' => $this->input->post('feature', TRUE),
      'no_of_stock' => $this->input->post('no_of_stock', TRUE),
      'item_description' => $this->input->post('item_description', TRUE),
      'car_production_id' => 0,
      'begining_term_to_use' => $this->input->post('begining_term_to_use', TRUE),
      'ending_term_to_use' => $this->input->post('ending_term_to_use', TRUE),
      'caution' => $this->input->post('caution', TRUE),
      'content' => $this->input->post('content', TRUE),
      'html_content' => htmlspecialchars($this->input->post('html_content', TRUE)),
      'youtube' => $this->input->post('youtube', TRUE),
      'unpurchasable_user' => $this->input->post('unpurchasable_user', TRUE),
      'is_active' => $this->input->post('is_active', TRUE),
      'is_show_home' => $this->input->post('is_show_home', TRUE),
      // 'registration_date' => $this->input->post('registration_date', TRUE),
      // 'last_modification_date' => $this->input->post('last_modification_date', TRUE),
      'image' => $this->input->post('set_product_image'),
    );
    $affected_rows = $this->datacontrol_model->update('product_sa_ppf', $data, array('id'=>$this->input->post('edit_id', TRUE)));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete_hq_general($id){
    $affected_rows = $this->datacontrol_model->delete('product_hq_general', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete_hq_ppf($id){
    $affected_rows = $this->datacontrol_model->delete('product_hq_ppf', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete_sa_general($id){
    $affected_rows = $this->datacontrol_model->delete('product_sa_general', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function delete_sa_ppf($id){
    $affected_rows = $this->datacontrol_model->delete('product_sa_ppf', array('id'=>$id));
    if($affected_rows > 0){
      echo json_encode(array('error' => 0));
    }
    else{
      echo json_encode(array('error' => 1));
    }
  }

  public function export_hq_general(){
    $product_export = $this->datacontrol_model->getAllData('product_hq_general');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }

    $excel_row =2;

    // print_r($product_management[0]->id);

    for($i=0; $i < count($product_export); $i++) {
      $excel_row_column = 0;
      foreach ($table_columns as $value) {
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
        $excel_row_column++;
      }
      $excel_row++;
    }

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product HQ General'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product HQ General"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }

  public function export_hq_ppf(){
    $product_export = $this->datacontrol_model->getAllData('product_hq_ppf');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }

    $excel_row =2;

    // print_r($product_management[0]->id);

    for($i=0; $i < count($product_export); $i++) {
      $excel_row_column = 0;
      foreach ($table_columns as $value) {
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
        $excel_row_column++;
      }
      $excel_row++;
    }

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product HQ PPF'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product HQ PPF"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }

  public function export_sa_general(){
    $product_export = $this->datacontrol_model->getAllData('product_sa_general');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }


    $excel_row =2;

    // print_r($product_management[0]->id);

    for($i=0; $i < count($product_export); $i++) {
      $excel_row_column = 0;
      foreach ($table_columns as $value) {
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
        $excel_row_column++;
      }
      $excel_row++;
    }

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product SA General'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product SA General"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }

  public function export_sa_ppf(){
    $product_export = $this->datacontrol_model->getAllData('product_sa_ppf');
    $table_columns = array();
    $unuse = array('update_by', 'create_date', 'update_date');
    foreach ($product_export[0] as $key => $value) {
      if (!in_array($key, $unuse)){
        $table_columns[] = $key;
      }
    }

    $this->load->library('excel');

    // Set document properties
    // echo date('H:i:s') , " Set document properties" , EOL;
    $this->excel->getProperties()->setCreator("Sensha")
    ->setLastModifiedBy("Sensha")
    ->setTitle("Product Management".date('Y-m-d'))
    ->setSubject("Product Management".date('Y-m-d'))
    ->setDescription("Export Product Management")
    ->setKeywords("Export Product Management")
    ->setCategory("Export Product Management");

    // Add some data
    // echo date('H:i:s') , " Add some data" , EOL;
    $column = 0;
    foreach ($table_columns as $field) {
      $this->excel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
      $column++;
    }

    $excel_row =2;

    // print_r($product_management[0]->id);

    for($i=0; $i < count($product_export); $i++) {
      $excel_row_column = 0;
      foreach ($table_columns as $value) {
        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($excel_row_column, $excel_row, $product_export[$i]->$value);
        $excel_row_column++;
      }
      $excel_row++;
    }

    // Rename worksheet
    // echo date('H:i:s') , " Rename worksheet" , EOL;
    $this->excel->getActiveSheet()->setTitle('Export Product SA PPF'.date('y'));

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $this->excel->setActiveSheetIndex(0);

    // Save Excel 2007 file
    // echo date('H:i:s') , " Write to Excel2007 format" , EOL;
    $callStartTime = microtime(true);

    // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'csv');

    $newFileName = "Export Product SA PPF"."_".date('Y-m-d').'_' . time() . '.csv';

    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $newFileName . '"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
    $objWriter->save('php://output');
    exit;
  }

  public function import_hq_general(){
    // print_r($_FILES);
    if(!empty($_FILES['file_import']['name'])){
      if (!file_exists('uploads/csv_import/')) {
        mkdir("uploads/csv_import/", 0777);
      }
      // Set preference
      $config['upload_path'] = 'uploads/csv_import/';
      $config['allowed_types'] = 'csv';
      // $config['max_size'] = '1000'; // max_size in kb
      // $config['file_name'] = $_FILES['file_lang']['name'];
      $config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


      // Load upload library
      $this->load->library('upload',$config);
      $this->upload->initialize($config);


      // File upload
      if($this->upload->do_upload('file_import')){
        // Get data about the file
        $uploadData = $this->upload->data();
        $filename = $uploadData['file_name'];

        // Reading file
        $file = fopen("uploads/csv_import/".$filename,"r");
        $i = 1;

        $importData_arr = array();
        $text = "";
        $fieldTitle = array();
        // $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
          $data = array();
          if($i == 1){
            $fieldTitle = $filedata;
          }
          if($i>1){

            $x=0;
            foreach ($fieldTitle as $key => $item) {
              if($key != 0){
                $data[$item] = $filedata[$x];
              }
              $x++;
            }
            // print_r($data);
            // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

            $this->db->where('product_id', $data['product_id']);
            $query = $this->db->get('product_hq_general');
            if(count($query->row()) > 0){
              $this->datacontrol_model->update('product_hq_general', $data, array('product_id' => $data['product_id']));
            }
            else{
              $this->datacontrol_model->insert('product_hq_general', $data);
            }

          }
          $i++;
        }

        fclose($file);
        @unlink("uploads/csv_import/".$filename);
        echo json_encode(array('error' => 0));
      }
      else{
        // echo $this->upload->display_errors();
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }

    }
    else{
      echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
    }
  }

  public function import_hq_ppf(){
    // print_r($_FILES);
    if(!empty($_FILES['file_import']['name'])){
      if (!file_exists('uploads/csv_import/')) {
        mkdir("uploads/csv_import/", 0777);
      }
      // Set preference
      $config['upload_path'] = 'uploads/csv_import/';
      $config['allowed_types'] = 'csv';
      // $config['max_size'] = '1000'; // max_size in kb
      // $config['file_name'] = $_FILES['file_lang']['name'];
      $config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


      // Load upload library
      $this->load->library('upload',$config);
      $this->upload->initialize($config);


      // File upload
      if($this->upload->do_upload('file_import')){
        // Get data about the file
        $uploadData = $this->upload->data();
        $filename = $uploadData['file_name'];

        // Reading file
        $file = fopen("uploads/csv_import/".$filename,"r");
        $i = 1;

        $importData_arr = array();
        $fieldTitle = array();
        // $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
          $data = array();
          if($i == 1){
            $fieldTitle = $filedata;
          }
          if($i>1){

            $x=0;
            foreach ($fieldTitle as $key => $item) {
              if($key != 0){
                $data[$item] = $filedata[$x];
              }
              $x++;
            }
            // print_r($data);
            // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

            $this->db->where('product_id', $data['product_id']);
            $query = $this->db->get('product_hq_ppf');
            if(count($query->row()) > 0){
              $this->datacontrol_model->update('product_hq_ppf', $data, array('product_id' => $data['product_id']));
            }
            else{
              $this->datacontrol_model->insert('product_hq_ppf', $data);
            }

          }
          $i++;
        }

        fclose($file);
        @unlink("uploads/csv_import/".$filename);
        echo json_encode(array('error' => 0));
      }
      else{
        // echo $this->upload->display_errors();
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }

    }
    else{
      echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
    }
  }

  public function import_sa_general(){
    // print_r($_FILES);
    if(!empty($_FILES['file_import']['name'])){
      if (!file_exists('uploads/csv_import/')) {
        mkdir("uploads/csv_import/", 0777);
      }
      // Set preference
      $config['upload_path'] = 'uploads/csv_import/';
      $config['allowed_types'] = 'csv';
      // $config['max_size'] = '1000'; // max_size in kb
      // $config['file_name'] = $_FILES['file_lang']['name'];
      $config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


      // Load upload library
      $this->load->library('upload',$config);
      $this->upload->initialize($config);


      // File upload
      if($this->upload->do_upload('file_import')){
        // Get data about the file
        $uploadData = $this->upload->data();
        $filename = $uploadData['file_name'];

        // Reading file
        $file = fopen("uploads/csv_import/".$filename,"r");
        $i = 1;

        $importData_arr = array();
        $fieldTitle = array();
        // $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
          $data = array();
          if($i == 1){
            $fieldTitle = $filedata;
          }
          if($i>1){

            $x=0;
            foreach ($fieldTitle as $key => $item) {
              if($key != 0){
                $data[$item] = $filedata[$x];
              }
              $x++;
            }
            // print_r($data);
            // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

            $this->db->where('product_id', $data['product_id']);
            $query = $this->db->get('product_sa_general');
            if(count($query->row()) > 0){
              $this->datacontrol_model->update('product_sa_general', $data, array('product_id' => $data['product_id']));
            }
            else{
              $this->datacontrol_model->insert('product_sa_general', $data);
            }

          }
          $i++;
        }

        fclose($file);
        @unlink("uploads/csv_import/".$filename);
        echo json_encode(array('error' => 0));
      }
      else{
        // echo $this->upload->display_errors();
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }

    }
    else{
      echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
    }
  }

  public function import_sa_ppf(){
    // print_r($_FILES);
    if(!empty($_FILES['file_import']['name'])){
      if (!file_exists('uploads/csv_import/')) {
        mkdir("uploads/csv_import/", 0777);
      }
      // Set preference
      $config['upload_path'] = 'uploads/csv_import/';
      $config['allowed_types'] = 'csv';
      // $config['max_size'] = '1000'; // max_size in kb
      // $config['file_name'] = $_FILES['file_lang']['name'];
      $config['file_name'] = $this->ion_auth->user()->row()->id."_".date("Ymdhis").".csv";


      // Load upload library
      $this->load->library('upload',$config);
      $this->upload->initialize($config);


      // File upload
      if($this->upload->do_upload('file_import')){
        // Get data about the file
        $uploadData = $this->upload->data();
        $filename = $uploadData['file_name'];

        // Reading file
        $file = fopen("uploads/csv_import/".$filename,"r");
        $i = 1;

        $importData_arr = array();
        $fieldTitle = array();
        // $this->datacontrol_model->delete('language', array('language' => $this->input->post('language')));

        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
          $data = array();
          if($i == 1){
            $fieldTitle = $filedata;
          }
          if($i>1){

            $x=0;
            foreach ($fieldTitle as $key => $item) {
              if($key != 0){
                $data[$item] = $filedata[$x];
              }
              $x++;
            }
            // print_r($data);
            // $text .= '$lang["'.$filedata[0].'"] = "'.$filedata[1].'";\n';

            $this->db->where('product_id', $data['product_id']);
            $query = $this->db->get('product_sa_ppf');
            if(count($query->row()) > 0){
              $this->datacontrol_model->update('product_sa_ppf', $data, array('product_id' => $data['product_id']));
            }
            else{
              $this->datacontrol_model->insert('product_sa_ppf', $data);
            }

          }
          $i++;
        }
        fclose($file);
        @unlink("uploads/csv_import/".$filename);
        echo json_encode(array('error' => 0));
      }
      else{
        // echo $this->upload->display_errors();
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }

    }
    else{
      echo json_encode(array('error' => 1, 'msg' => 'Not file.'));
    }
  }

  public function add_product_image_general(){
    if(!empty($_FILES['product_image']['name'])){
      if (!file_exists('uploads/temp/')) {
        mkdir("uploads/temp/", 0777);
      }

      $config['upload_path']          = 'uploads/temp/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('product_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function upload_product_image_general(){
    if(!empty($_FILES['upload_multi_image']['name'])){
      if (!file_exists('uploads/product_image/')) {
        mkdir("uploads/product_image/", 0777);
      }

      $config['upload_path']          = 'uploads/product_image/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = true;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('upload_multi_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function upload_product_image_hq_general(){
    if(!empty($_FILES['upload_multi_image']['name'])){
      if (!file_exists('uploads/product_image/')) {
        mkdir("uploads/product_image/", 0777);
      }

      $config['upload_path']          = 'uploads/product_image/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('upload_multi_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function add_product_image_ppf(){
    if(!empty($_FILES['product_image']['name'])){
      if (!file_exists('uploads/temp/')) {
        mkdir("uploads/temp/", 0777);
      }

      $config['upload_path']          = 'uploads/temp/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('product_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function upload_product_image_hq_ppf(){
    if(!empty($_FILES['upload_multi_image']['name'])){
      if (!file_exists('uploads/product_image/')) {
        mkdir("uploads/product_image/", 0777);
      }

      $config['upload_path']          = 'uploads/product_image/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('upload_multi_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function add_product_image_sa_general(){
    if(!empty($_FILES['product_image']['name'])){
      if (!file_exists('uploads/temp/')) {
        mkdir("uploads/temp/", 0777);
      }

      $config['upload_path']          = 'uploads/temp/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('product_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function upload_product_image_sa_general(){
    if(!empty($_FILES['upload_multi_image']['name'])){
      if (!file_exists('uploads/product_image/')) {
        mkdir("uploads/product_image/", 0777);
      }

      $config['upload_path']          = 'uploads/product_image/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('upload_multi_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function add_product_image_sa_ppf(){
    if(!empty($_FILES['product_image']['name'])){
      if (!file_exists('uploads/temp/')) {
        mkdir("uploads/temp/", 0777);
      }

      $config['upload_path']          = 'uploads/temp/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('product_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }

  public function upload_product_image_sa_ppf(){
    if(!empty($_FILES['upload_multi_image']['name'])){
      if (!file_exists('uploads/product_image/')) {
        mkdir("uploads/product_image/", 0777);
      }

      $config['upload_path']          = 'uploads/product_image/';
      $config['allowed_types']        = 'gif|jpg|png';
      $config['overwrite']            = false;
      // $config['encrypt_name'] = TRUE;
      // $config['max_size']             = 100;
      // $config['max_width']            = 1024;
      // $config['max_height']           = 768;

      $this->load->library('upload', $config);
      $this->upload->initialize($config);


      if ( ! $this->upload->do_upload('upload_multi_image'))
      {
        echo json_encode(array('error' => 1, 'msg' => $this->upload->display_errors()));
      }
      else
      {
        $data = $this->upload->data();
        // echo $data['file_name'];
        echo json_encode(array('error' => 0, 'msg' => '', 'file_name' => $data['file_name']));
      }
    }
  }
}
