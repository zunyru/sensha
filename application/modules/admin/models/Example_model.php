<?php
class Example_model extends CI_Model {
  public function insert($table ,$data){
      $this->db->set('create_date', 'NOW()', FALSE);
      $this->db->insert($table, $data);
      return $this->db->insert_id();
  }

  public function update($table ,$data, $id){
      $this->db->where('id', $id);
      $this->db->update($table, $data);
      return $this->db->affected_rows();
  }

  public function delete($table ,$id){
      $this->db->where('id', $id);
      $this->db->delete($table);
      return $this->db->affected_rows();
  }

  public function getAll($table){
      $query = $this->db->get($table);
      return $query->result();
  }

  public function countAllRows($table){
    // $this->db->count_all_results('my_table');
    // $this->db->count_all_results();
    return $this->db->count_all_results($table, FALSE);
  }

  public function count_all_results(){
    return $this->db->count_all_results();
  }
}

 ?>
