<?php
class Cluster2_model extends CI_Model
{
    public function get_rows($param)
    {
        $user_country = $this->ion_auth->user()->row()->country;

        $this->_condition($param);
        $this->db->select('c2.id, c2.country, c2.cluster_name, c2.level1, c2.cluster_type, c1.cluster_name as c1_name, c2.html_content,c2.cluster_url');
        $this->db->from('cluster2 c2');
        $this->db->join('cluster1 c1', 'c2.level1 = c1.id', 'inner');
        if($this->ion_auth->in_group(array('SA'))){
            $this->db->where('c2.country', $user_country);
        }

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if(!empty($param['filter_country']))
            $this->db->where('c2.country', $this->input->post('filter_country'));

        $query = $this->db->get('cluster2');

        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {

        $user_country = $this->ion_auth->user()->row()->country;

        $this->_condition($param);
        $this->db->select('c2.id, c2.country, c2.cluster_name, c2.level1, c2.cluster_type, c1.cluster_name as c1_name, c2.html_content,c2.cluster_url');
        $this->db->from('cluster2 c2');
        $this->db->join('cluster1 c1', 'c2.level1 = c1.id', 'inner');
        if($this->ion_auth->in_group(array('SA'))){
            $this->db->where('c2.country', $user_country);
        }

        if(!empty($param['filter_country']))
            $this->db->where('c2.country', $this->input->post('filter_country'));

        $query = $this->db->get();
        //echo $this->db->last_query();exit();
        return $query->num_rows();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('c2.cluster_name', $param['search']['value'])
                ->or_like('c2.cluster_url', $param['search']['value'])
                ->or_like('c2.cluster_type', $param['search']['value'])
                ->or_like('c2.country', $param['search']['value'])
                ->group_end();
            $this->db->group_by(id);
        }
    }
}