<?php
class Sales_history_model extends CI_Model
{
    public function get_rows($param)
    {
        $user = $this->ion_auth->user()->row();

        $this->_condition($param);

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('purchaser_country', $user->country);
            $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
            $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
        }
        if ($this->ion_auth->in_group(array('AA'))) {
            $this->db->where('aa_name', $user->email);
        }

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));
        if(!empty($param['filter_month']))
            $this->db->where('DATE_FORMAT(create_date,"%m")',$this->input->post('filter_month'));
        if(!empty($param['filter_year']))
            $this->db->where('DATE_FORMAT(create_date,"%Y")',$this->input->post('filter_year'));

        $query = $this->db
            ->select('*')
            ->from('sales_history')
            ->order_by('update_date', 'desc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {
        $user = $this->ion_auth->user()->row();
        
        $this->_condition($param);

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('purchaser_country', $user->country);
            $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
            $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
        }
        if ($this->ion_auth->in_group(array('AA'))) {
            $this->db->where('aa_name', $user->email);
        }

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));
        if(!empty($param['filter_month']))
            $this->db->where('DATE_FORMAT(sale_date,"%m")',$this->input->post('filter_month'));
        if(!empty($param['filter_year']))
            $this->db->where('DATE_FORMAT(sale_date,"%Y")',$this->input->post('filter_year'));

        $query = $this->db
            ->select('*')
            ->from('sales_history')
            ->get();
        return $query->num_rows();
    }

    private function _condition($param)
    {
        $payment_method = ['amazon', 'paypal'];
        $payment_method_bank = ['pay on delivery', 'Bank Transfer'];
        $this->db->group_start();
        $this->db->group_start()
            ->where_in('payment_method', $payment_method)
            ->where('payment_status','Completed')
            ->group_end();

        $this->db->or_group_start()
            ->where_in('payment_method', $payment_method_bank)
            ->group_end();
            $this->db->or_group_start()
                ->where('payment_method IS NULL');
            $this->db->group_end();
        $this->db->group_end();
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('shipment_id', $param['search']['value'])
                ->or_like('account_type', $param['search']['value'])
                ->or_like('purchaser_name', $param['search']['value'])
                ->or_like('purchaser_company', $param['search']['value'])
                ->or_like('shipping_status', $param['search']['value'])
                ->or_like('sale_date',$param['search']['value'])
                ->or_like('sale_item_id',$param['search']['value'])
                ->or_like('product_name',$param['search']['value'])
                ->or_like('payment_method',$param['search']['value'])
                ->or_like('item_sub_total_amount',str_replace(",","",$param['search']['value']))
                ->group_end();
        }
    }
}