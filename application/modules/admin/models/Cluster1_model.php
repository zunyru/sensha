<?php
class Cluster1_model extends CI_Model
{
    public function get_rows($param)
    {
        $this->_condition($param);

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('a.*')
            ->from('cluster1 a')
            ->order_by('a.update_date', 'desc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {
        $this->_condition($param);

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('a.*')
            ->from('cluster1 a')
            ->get();
        return $query->num_rows();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('a.cluster_name', $param['search']['value'])
                ->or_like('a.cluster_type', $param['search']['value'])
                ->group_end();
        }
    }
}