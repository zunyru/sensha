<?php
class Sales_history_credit_model extends CI_Model
{
    public function get_rows($param)
    {
        $user = $this->ion_auth->user()->row();

        $this->_condition($param);


        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));
        if(!empty($param['filter_month'])) {
            $this->db->where('month(create_date)', $this->input->post('filter_month'));
        }else{
            $this->db->where('month(create_date)', date('m'));
        }
        if(!empty($param['filter_year'])) {
            $this->db->where('year(create_date)', $this->input->post('filter_year'));
        }else{
            $this->db->where('year(create_date)', date('Y'));
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where("(country = '" . $user->country . "' OR purchaser_country = '" . $user->country . "')");
            $this->db->where("(credit_added != 0 OR release_amount != 0 OR use_credit != 0)");
        }

        $query = $this->db
            ->select('*')
            ->from('sales_history_credit')
            ->order_by('update_date', 'desc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {
        $user = $this->ion_auth->user()->row();

        $this->_condition($param);

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));
        if(!empty($param['filter_month'])) {
            $this->db->where('month(create_date)', $this->input->post('filter_month'));
        }else{
            $this->db->where('month(create_date)', date('m'));
        }
        if(!empty($param['filter_year'])) {
            $this->db->where('year(create_date)', $this->input->post('filter_year'));
        }else{
            $this->db->where('year(create_date)', date('Y'));
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where("(country = '" . $user->country . "' OR purchaser_country = '" . $user->country . "')");
            $this->db->where("(credit_added != 0 OR release_amount != 0 OR use_credit != 0)");
        }

        $query = $this->db
            ->select('*')
            ->from('sales_history_credit')
            ->get();
        return $query->num_rows();
    }

    public function get_current_credit()
    {
        $user = $this->ion_auth->user()->row();
        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where("(country = '" . $user->country . "' OR purchaser_country = '" . $user->country . "')");
            $this->db->where("(credit_added != 0 OR release_amount != 0 OR use_credit != 0)");
        }
        $query = $this->db
            ->select('balance')
            ->from('sales_history_credit')
            ->order_by('id', 'desc')
            ->limit(1)
            ->get();
        //echo $this->db->last_query();exit();
        return $query->result();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('shipment_id', $param['search']['value'])
                ->group_end();
        }
    }
}