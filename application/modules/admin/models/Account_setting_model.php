<?php
class Account_setting_model extends CI_Model
{
    public function get_rows($param)
    {
        $user = $this->ion_auth->user()->row();
        if($this->input->post('country')){
            $this->db->where('country', $this->input->post('country'));
        }
        if($this->ion_auth->in_group(array('SA'))){
            $this->db->group_start();
            $this->db->where('create_by', $user->id)->or_where('id', $user->id);
            $this->db->group_end();
        }

        $this->_condition($param);

        if (isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);
        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('*')
            ->from('users')
            ->order_by('country', 'asc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {
        $user = $this->ion_auth->user()->row();
        if($this->input->post('country')){
            $this->db->where('country', $this->input->post('country'));
        }
        if($this->ion_auth->in_group(array('SA'))){
            $this->db->group_start();
            $this->db->where('create_by', $user->id)->or_where('id', $user->id);
            $this->db->group_end();
        }
        $this->_condition($param);

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));

        $query = $this->db
            ->select('*')
            ->from('users')
            ->get();
        return $query->num_rows();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('email', $param['search']['value'])
                ->or_like('company_name', $param['search']['value'])
                ->group_end();
        }
    }
}