<?php
class Nation_lang_model extends CI_Model
{
    public function get_rows($param)
    {
        $this->_condition($param);

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        $query = $this->db
            ->select('a.*,countries.nicename')
            ->from('nation_lang a')
            ->join('countries', 'a.country = REPLACE(countries.name, " ", "_")', 'left')
            ->order_by('a.country', 'asc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
    {
        $this->_condition($param);
        $query = $this->db
            ->select('a.*')
            ->from('nation_lang a')
            ->get();
        return $query->num_rows();
    }

    private function _condition($param)
    {
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('a.country', $param['search']['value'])
                ->or_like('a.language', $param['search']['value'])
                ->or_like('a.currency_name', $param['search']['value'])
                ->group_end();
        }
    }
}