<?php
class Sales_history_total_model extends CI_Model
{
    public function get_rows($param)
    {
        $user = $this->ion_auth->user()->row();

        $this->_condition($param);

        if ( isset($param['length']) )
            $this->db->limit($param['length'], $param['start']);

        if(!empty($param['filter_country']))
            $this->db->where('country', $this->input->post('filter_country'));
        if(!empty($param['filter_month'])) {
            $this->db->where('month(create_date)', $this->input->post('filter_month'));
        }else{
            $this->db->where('month(create_date)', date('m'));
        }
        if(!empty($param['filter_year'])) {
            $this->db->where('year(create_date)', $this->input->post('filter_year'));
        }else{
            $this->db->where('year(create_date)', date('Y'));
        }

        if ($this->ion_auth->in_group(array('SA'))) {
            $this->db->where('purchaser_country', $user->country);
            $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
            $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
        }
        if ($this->ion_auth->in_group(array('AA'))) {
            $this->db->where('aa_name', $user->email);
        }

        $query = $this->db
            ->select('*')
            ->from('sales_history_total')
            ->order_by('update_date', 'desc')
            ->get();
        //echo $this->db->last_query();exit();
        return $query;
    }

    public function get_count($param)
        {
            $user = $this->ion_auth->user()->row();

            $this->_condition($param);

            if(!empty($param['filter_country']))
                $this->db->where('country', $this->input->post('filter_country'));
            if(!empty($param['filter_month'])) {
                $this->db->where('month(create_date)', $this->input->post('filter_month'));
            }else{
                $this->db->where('month(create_date)', date('m'));
            }
            if(!empty($param['filter_year'])) {
                $this->db->where('year(create_date)', $this->input->post('filter_year'));
            }else{
                $this->db->where('year(create_date)', date('Y'));
            }

            if ($this->ion_auth->in_group(array('SA'))) {
                $this->db->where('purchaser_country', $user->country);
                $this->db->where("(account_type = 'SA' OR account_type = 'admin')");
                $this->db->where("((purchaser_type = 'FC') OR (purchaser_type = 'GU' AND buy_type = 'domestic'))");
            }
            if ($this->ion_auth->in_group(array('AA'))) {
                $this->db->where('aa_name', $user->email);
            }

            $query = $this->db
                ->select('*')
                ->from('sales_history_total')
                ->get();
            return $query->num_rows();
        }

    private function _condition($param)
    {
        $payment_method = ['amazon', 'paypal'];
        $payment_method_bank = ['pay on delivery', 'Bank Transfer'];
        $this->db->group_start();
        $this->db->group_start()
            ->where_in('payment_method', $payment_method)
            ->where('payment_status','Completed')
            ->group_end();

        $this->db->or_group_start()
            ->where_in('payment_method', $payment_method_bank)
            ->group_end();
           $this->db->or_group_start()
               ->where('payment_method IS NULL');
           $this->db->group_end();
        $this->db->group_end();

        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                ->group_start()
                ->like('shipment_id', $param['search']['value'])
                ->or_like('account_type', $param['search']['value'])
                ->or_like('purchaser_company', $param['search']['value'])
                ->or_like('purchaser_name', $param['search']['value'])
                ->or_like('purchaser_type', $param['search']['value'])
                ->group_end();
        }
    }
}