<?php
class Datacontrol_model extends CI_Model {
  public function insert($table, $data){
    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->set('create_date', 'NOW()', FALSE);
    $this->db->set('create_by', $user_id);
    $this->db->set('update_by', $user_id);
    $this->db->insert($table, $data);
    $insert_id = $this->db->insert_id();
    return $this->db->insert_id();
    @keep_log("insert $table", $insert_id, json_encode($data));

  }

  public function update($table, $data, $where){
    $user_id = $this->ion_auth->user()->row()->id;
    $this->db->where($where);
    $this->db->set('update_by', $user_id);
    $this->db->update($table, $data);

    //echo $this->db->last_query();
    @keep_log("update $table", json_encode($where), json_encode($data));
    return $this->db->affected_rows();

  }

  public function delete($table, $where){
    $this->db->where($where);
    $this->db->delete($table);
    return $this->db->affected_rows();
    @keep_log("delete $table", json_encode($where));

  }

  public function getAllDataWhere($table,$where=''){
    if($where != ''){
      $this->db->where($where);
    }
    $query = $this->db->get($table);
    return $query->result();
  }

  public function getAllData($table){
    $query = $this->db->get($table);
    return $query->result();
  }

  public function getRowData($table, $where=''){
    if($where != ''){
      $this->db->where($where);
    }
    $query = $this->db->get($table);
    return $query->row();
  }

  public function countTableRows($table){
    // $this->db->count_all_results('my_table');
    // $this->db->count_all_results();
    return $this->db->count_all_results($table, FALSE);
  }

  public function countAllResults(){
    return $this->db->count_all_results();
  }

}

?>
