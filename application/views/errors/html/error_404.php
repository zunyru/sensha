<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<?php
$ci = new CI_Controller();
$ci =& get_instance();
// $ci->load->helper('url');
$ci->load->model('database/datacontrol_model');

$user_lang = 'Global';
$coutry_iso = "";

$ci->db->select('iso');
$r = $ci->datacontrol_model->getAllData('countries');
$iso_list = array();
foreach($r as $item){
  $iso_list[] = strtolower($item->iso);
}


if(!$ci->ion_auth->logged_in()){
  if(in_array($ci->uri->segment(1), $iso_list)){
    $ci->db->where('iso', strtoupper($ci->uri->segment(1)));
    $r = $ci->datacontrol_model->getRowData('countries');
    $user_lang = $r->nicename;
    $coutry_iso = strtolower($r->iso);
  }
}
else{
  if($ci->session->has_userdata('user_language')){
    $user_lang = $ci->session->userdata('user_language');
  }

  $ci->db->where('nicename', $user_lang);
  $r = $ci->datacontrol_model->getRowData('countries');
  $coutry_iso = strtolower($r->iso);

  if($ci->uri->segment(1) != '' && $ci->uri->segment(1) != 'page' && $ci->uri->segment(1) != $coutry_iso){
    $segs = $ci->uri->segment_array();
    $segs[1] = $coutry_iso;
    // echo $new_uri = str_replace("/".$ci->uri->segment(1), $coutry_iso."/", uri_string());
    redirect(base_url($segs), 'refresh');
  }

}

if(!file_exists('application/language/'.strtolower($user_lang))){
  $user_lang = 'Global';
  $coutry_iso = "";
}


$ci->lang->load('set', strtolower($user_lang));

$coutry_iso = $coutry_iso."/";
?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $htmlTitle;?></title>
<link rel="canonical" href="<?php echo base_url(uri_string());?>" />
<meta name="description" content="<?php echo $htmlDes;?>">
<meta name="keywords"    content="">
<?php if($user_lang == 'Global'):?>
  <link rel="alternate" href="<?php echo base_url(uri_string());?>" hreflang="en" />
<?php else:?>
  <link rel="alternate" href="<?php echo base_url(uri_string());?>" hreflang="<?php echo $coutry_iso;?>" />
<?php endif;?>

<?php if($noindex):?>
  <meta name=”robots” content="noindex">
<?php endif;?>

<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/animation.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/home.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/common.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/page.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/menu-mobile.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/jquery.bxslider.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url("assets/sensha-theme/");?>css/owl.theme.default.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:700" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://www.at-aroma.com/js/common/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.mb.YTPlayer.min.js"></script>
<!-- youtube video player -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/packages/icons/css/icons.min.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/css/youtube-video-player.min.css" />
<script type="text/javascript" src="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/js/youtube-video-player.jquery.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.css" />
<script type="text/javascript" src="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/packages/perfect-scrollbar/jquery.mousewheel.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/sensha-theme/");?>youtube-video-player/packages/perfect-scrollbar/perfect-scrollbar.js"></script>
<!-- youtube video player end -->
<?php if(get_cookie('user_language') == 'Japan'):?>
  <link href="<?php echo base_url("assets/sensha-theme/");?>css/japanese.css" rel="stylesheet">
<?php endif;?>
<script>
// YTPlayer実行
$(function(){
	$('#youtube').YTPlayer();
});
</script>
<script>
$(document).ready(function() {
  $('#myList').youtube_video({
    playlist: 'PLZUp1F67_y_cs43qqJc_Y3-c09SWY-ZVe',
    playlist_type: 'horizontal',
  });
});
</script>

</head>
<body <?php echo (get_cookie('user_language') == 'Japan')?'id="japanese"':'';?> class>
  <div id="wrapper">
    <div id="content">
      <header id="header">
        <div class="header-wrap">
          <div class="header-main">
            <div class="inner">
              <a class="header-logo" href="<?php echo base_url("$coutry_iso");?>">
                <img src="<?php echo base_url("assets/sensha-theme/");?>images/logo.png" alt="SENSHA">
              </a>
              <ul class="header-menu">
                <?php if(!$ci->ion_auth->logged_in()):?>
                  <li>
                    <a href="<?php echo base_url("page/register");?>" class="">
                      <span class="icon-person"></span>
                      <?php echo $ci->lang->line('header_menu_sign_up', FALSE); ?>
                    </a>
                  </li>
                  <li>
                    <a href="" data-src="#modal01" class="login">
                      <span class="icon-lock"></span>
                      <?php echo $ci->lang->line('header_menu_login', FALSE); ?>
                    </a>
                  </li>
                <?php else:?>
                  <li>
                    <a href="<?php echo base_url("/page/user/dashboard");?>" class="">
                      <span class="icon-person"></span>
                      <?php echo $ci->ion_auth->user()->row()->email;?>
                    </a>
                  </li>
                  <li>
                    <a href="<?php echo base_url("/page/user/logout");?>" class="">
                      <span class="icon-lock"></span>
                      <?php echo $ci->lang->line('header_menu_logout', FALSE); ?>
                    </a>
                  </li>
                <?php endif;?>
                <li class="">
                  <a href="<?php echo base_url("$coutry_iso"."page/cart");?>" class="">
                    <span class="icon-cart02"></span>
                    <?php echo $ci->lang->line('header_menu_cart', FALSE); ?>
                  </a>
                  <div class="box-cart">
                    <h2><span class="icon-cart02"></span> Current Cart</h2>
                    <div class="clr">
                      <div class="box_domestic">
                        <div class="inner_box_domestic">
                          <span class="c01">Domestic</span>
                          <p class="items">5 items in cart</p>
                          <p class="sub">Subtotal 5,180 Yen</p>
                          <ul class="list-items">
                            <li>
                              <p>Import shipping</p>
                              <span>1,200 Yen</span>
                            </li>
                            <li>
                              <p>Import Tax</p>
                              <span>820  Yen</span>
                            </li>
                            <li>
                              <p>Minimum Shipping</p>
                              <span>900  Yen</span>
                            </li>
                          </ul>
                        </div>
                        <div class="total clr">
                          <p>Total</p>
                          <span>8,180 Yen</span>
                        </div>
                      </div>
                      <div class="box_oversea">
                        <div class="inner_box_domestic">
                          <span class="c01">Oversea</span>
                          <p class="items">5 items in cart</p>
                          <p class="sub">Subtotal 5,180 Yen</p>
                          <ul class="list-items">
                            <li>
                              <p>Import shipping</p>
                              <span>1,200 Yen</span>
                            </li>
                            <li>
                              <p>Import Tax</p>
                              <span>820  Yen</span>
                            </li>
                            <li>
                              <p>Minimum Shipping</p>
                              <span>900  Yen</span>
                            </li>
                          </ul>
                        </div>
                        <div class="total clr">
                          <p>Total</p>
                          <span>8,180 Yen</span>
                        </div>
                      </div>
                    </div>
                    <div class="line-btn">
                      <input type="button" class="buy btn" onclick="location.href='cart.php';" value="ADD TO CART" />
                    </div>
                  </div>
                </li>
                <li class="lang">
                  <a href="" class="">
                    <span class="icon-lang"></span>
                    <?php echo ($ci->session->has_userdata('user_language'))?$user_lang:"Global";?>
                  </a>
                  <div class="language-select">
                    <?php
                    $user_country =  $ci->ion_auth->user()->row()->country;
                    if($ci->ion_auth->logged_in()){
                      $ci->db->where('country', $user_country);
                      $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                      if(empty($nation_lang)){
                        $ci->db->where('country', 'Global');
                        $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                      }
                    }
                    else{
                      $ci->db->where('is_active', 1);
                      $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                    }

                    ?>
                    <ul>
                      <?php foreach($nation_lang as $item):?>
                        <?php
                        $ci->db->where('nicename', $item->country);
                        $c = $ci->datacontrol_model->getRowData('countries');
                        ?>
                        <li><input <?php echo ($user_country == $item->country)?'selected':'';?> type="radio" name="language" data-iso="<?php echo strtolower($c->iso);?>" value="<?php echo $item->country;?>" data-label="language-select" data-value="<?php echo $item->country;?>" ><label for="language01"><?php echo $item->country;?></label></li>
                      <?php endforeach;?>
                      <!-- <li><input type="radio" id="language01" name="language" value="Global" data-label="language-select" data-value="Global" checked="checked"><label for="language01">Global - English</label></li> -->
                      <!-- <li><input type="radio" id="language02" name="language" value="Japan" data-label="language-select" data-value="Japan"><label for="language02">Japan - 日本語</label></li> -->
                      <!-- <li><input type="radio" id="language03" name="language" value="Chinese - Mandalin" data-label="language-select" data-value="Chinese - Mandalin"><label for="language03">Chinese - Mandalin</label></li> -->
                      <!-- <li><input type="radio" id="language04" name="language" value="Chinese - Cantonese" data-label="language-select" data-value="Chinese - Cantonese"><label for="language04">Chinese - Cantonese</label></li> -->
                      <!-- <li><input type="radio" id="language05" name="language" value="English - Us" data-label="language-select" data-value="English - Us"><label for="language05">English - Us</label></li> -->
                      <!-- <li><input type="radio" id="language06" name="language" value="Thai" data-label="language-select" data-value="Thai"><label for="language06">Thai</label></li> -->
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <!-- .header-main -->
          <div id="hamburger">
            <div id="hamburger-menu">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </div>
          </div>
          <?php
          $country = 'Global';
          if($ci->ion_auth->logged_in()){
            $country = $ci->ion_auth->user()->row()->country;
          }
          if($country == 'JapanA' || $country == 'JapanB'){
            $country = 'Japan';
          }
          $ci->db->where('country', $country);
          $ci->db->where('cluster_type', 'general');
          $query = $ci->db->get('cluster1');
          $cluster1 = $query->result();
          ?>
          <div class="header-nav">
            <div class="inner">
              <nav>
                <ul>
      <?php if($ci->ion_auth->logged_in()):?>
                    <li class="welcome">
                    <a href="<?php echo base_url("/page/user/dashboard");?>" class="">
                      Hello
                      <span class="icon-person"></span>
                      <?php echo $ci->ion_auth->user()->row()->email;?>
                    </a>
                    </li>
      <?php endif;?>
                  <?php foreach($cluster1 as $item):?>
                    <li>
                      <a href="<?php echo base_url("$coutry_iso"."page/general/$item->cluster_url");?>"><?php echo $item->cluster_name;?></a>
                    </li>
                  <?php endforeach;?>
                </ul>
              </nav>
              <?php
              $country = 'Global';
              if($ci->ion_auth->logged_in()){
                $country = $ci->ion_auth->user()->row()->country;
              }
              $ci->db->where('country', $country);
              $ci->db->where('cluster_type', 'ppf');
              $query = $ci->db->get('cluster1');
              $cluster1 = $query->result();

              $ci->db->where('country', $country);
              $ci->db->where('cluster_type', 'ppf');
              $query = $ci->db->get('cluster2');
              $cluster2 = $query->result();

              ?>
              <!-- Mobile -->
              <form id="ppf_form_mobile" method="get" action="<?php echo base_url("page/ppf");?>">
                <div class="select-list-block sp_view">
                  <p class="ttl">PPF Search</p>
                  <ul class="select-list">
                    <li class="maker">
                      <span class="icon-maker"></span>
                        <select name="cat_1" required>
                          <option value="Select Maker">Maker</option>
                          <?php foreach($cluster1 as $item):?>
                            <option value="<?php echo $item->id;?>" <?php ($ci->input->post('cat_1') == $item->id)?'selected':'';?>><?php echo $item->cluster_name;?></option>
                          <?php endforeach;?>
                        </select>
                    </li>
                    <li class="model">
                      <span class="icon-model"></span>
                      <select name="cat_2" required>
                        <option value="">Model</option>
                        <?php foreach($cluster2 as $item):?>
                          <option data-level1="<?php echo $item->level1;?>" value="<?php echo $item->id;?>"><?php echo $item->cluster_name;?></option>
                        <?php endforeach;?>
                      </select>
                    </li>
                    <li class="part">
                      <span class="icon-part"></span>
                      <select name="part">
                        <option value="">Part</option>
                        <option value="Headlight">Headlight</option>
                      </select>
                    </li>
                    <!-- <li class="year">
                      <span class="icon-year"></span>
                      <select name="year">
                        <option value="Select Year">Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                      </select>
                    </li> -->
                  </ul>
                  <div class="search-btn">
                    <button type="submit">
                      <span>SEARCH</span>
                      <span class="icon-search02 ico-glass"></span>
                    </button>
                  </div>
                </div>
              </form>


              <form id="ppf_form_desktop" method="get" action="<?php echo base_url("page/ppf");?>">
                <input type="hidden" name="ppf" value="1" />
                <div class="header-search">
                  <a data-src="" class="search-button"><?php echo $ci->lang->line('navi_cut_film_title', FALSE); ?>
                    <span class="icon-search02 ico-glass"></span>
                  </a>
                  <div class="header-search-pop">
                    <div class="search-content">
                      <p class="ttl"><?php echo $ci->lang->line('navi_cut_film_title_search', FALSE); ?></p>
                      <p class="lead"><?php echo $ci->lang->line('navi_cut_film_easy_to', FALSE); ?></p>
                      <ul class="search-list">
                        <li class="maker">
                          <span class="icon-maker"><?php echo $ci->lang->line('navi_cut_film_select_maker', FALSE); ?></span>
                          <select name="cat_1" required>
                            <option value="">Select Maker</option>
                            <?php foreach($cluster1 as $item):?>
                              <option value="<?php echo $item->id;?>" <?php ($ci->input->post('cat_1') == $item->id)?'selected':'';?>><?php echo $item->cluster_name;?></option>
                            <?php endforeach;?>
                          </select>
                        </li>
                        <li class="model">
                          <span class="icon-model"><?php echo $ci->lang->line('navi_cut_film_select_model', FALSE); ?></span>
                          <select name="cat_2" required>
                            <option value="">Select Model</option>
                            <?php foreach($cluster2 as $item):?>
                              <option data-level1="<?php echo $item->level1;?>" value="<?php echo $item->id;?>"><?php echo $item->cluster_name;?></option>
                            <?php endforeach;?>
                          </select>
                        </li>
                        <li class="part">
                          <span class="icon-part"><?php echo $ci->lang->line('navi_cut_film_select_part', FALSE); ?></span>
                          <select name="part">
                            <option value="">Select Part</option>
                            <option value="Headlight">Headlight</option>
                          </select>
                        </li>
                        <!-- <li class="year">
                        <span class="icon-year">Select Year</span>
                        <select name="year">
                        <option value="Select Year">Select Year</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                      </select>
                    </li> -->
                  </ul>
                  <div class="serch-btn">
                    <button type="submit" id="sbtn2">
                      <span><?php echo $ci->lang->line('navi_cut_film_btn_search', FALSE); ?></span>
                      <span class="icon-search02 ico-glass"></span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div style="clear: both"></div>
                <div class="lang mobile">
                  <div class="current_lang">
      	          <span>Select Language</span>
                    <span class="icon-lang" id="lang-btn-sp"></span>
                    <?php echo (get_cookie('user_language') == '')?"Global":get_cookie('user_language');?>
                  </div>
                  <div class="language-select">
                    <?php
                    //$ci->db->where('is_active', 1);
                     // $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                    ?>
                    <?php
                    $user_country =  $ci->ion_auth->user()->row()->country;
                    if($ci->ion_auth->logged_in()){
                      $ci->db->where('country', $user_country);
                      $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                      if(empty($nation_lang)){
                        $ci->db->where('country', 'Global');
                        $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                      }
                    }
                    else{
                      $ci->db->where('is_active', 1);
                      $nation_lang = $ci->datacontrol_model->getAllData('nation_lang');
                    }
                    ?>
                    <ul id="lang-content-sp">
                      <?php foreach($nation_lang as $item):?>
                        <li><input type="radio" name="language" id="lang_<?php echo $item->country;?>" value="<?php echo $item->country;?>" data-label="language-select" data-value="<?php echo $item->country;?>" ><label for="language01"><?php echo $item->country;?></label></li>
                      <?php endforeach;?>
                    </ul>
                  </div>
                </div>
        </div>
      </div>
      <?php if(!$ci->ion_auth->logged_in()):?>
      <div class="sp-login">
        <a href="" data-src="#modal01" class="sp-login-btn">
          <span class="icon-person"></span>
      	Login
        </a>
      </div>
      <?php else:?>
      <div class="sp-login">
        <a href="<?php echo base_url("/page/user/logout");?>">
          <span class="icon-person"></span>
      	Logout
        </a>
      </div>
      <?php endif;?>
      </div>
      </header>
      <script>
      	$("#lang-btn-sp").click(function(event){
      		$("#lang-content-sp").slideToggle();
      		$("html,body").animate({scrollTop:$('#hamburger').offset().top},1000);
      	});

        $(function(){
          $('#ppf_form_desktop select[name="cat_1"]').change(function(){
            // console.log($(this).val());
            $('#ppf_form_desktop select[name="cat_2"]').children('option:gt(0)').hide();
            $('#ppf_form_desktop select[name="cat_2"]').val('');
            $('#ppf_form_desktop select[name="cat_2"]').children("option[data-level1='"+$(this).val()+"']").show()
          });

          $('#ppf_form_mobile select[name="cat_1"]').change(function(){
            // console.log($(this).val());
            $('#ppf_form_mobile select[name="cat_2"]').children('option:gt(0)').hide();
            $('#ppf_form_mobile select[name="cat_2"]').val('');
            $('#ppf_form_mobile select[name="cat_2"]').children("option[data-level1='"+$(this).val()+"']").show()
          });

          $('#ppf_form_home select[name="cat_1"]').change(function(){
            // console.log($(this).val());
            $('#ppf_form_home select[name="cat_2"]').children('option:gt(0)').hide();
            $('#ppf_form_home select[name="cat_2"]').val('');
            $('#ppf_form_home select[name="cat_2"]').children("option[data-level1='"+$(this).val()+"']").show()
          });
        })


      </script>

      <div class="container">
        <div class="clr inner">
          <div id="breadcrumbs">
            <span><a href="index.php">Home</a></span><span>404 PAGE NOT FOUND</span>
          </div>
        </div><!--inner-->
        <div class="clr inner">
          <div class="box-content" style="width: calc(100%);">
            <div class="layout-contain">
              <div class="clr box_form">
                <div class="topic">
                  <p class="title-page">Sorry, this page isn't available.</p>
                </div>
                <div class="clr box-success" style="text-align: center;margin-top:50px;">
                  <p style="font-size: 16px;font-weight: 700;margin-bottom: 40px;">The link you followed may be broken, or the page may have been removed.</p>
                  <h3 style="font-size:98px;font-weight: 700;font-family: 'Oswald', sans-serif;color:#004ea1;">404 </h3>
                  <p style="font-size: 35px;font-weight: 700;margin-bottom:30px;">PAGE NOT FOUND</p>
                </div>
              </div>
            </div><!--layout-contain-->
          </div><!--inner-->
        </div><!--container-->
      </div>

      <footer id="footer">
          <div class="footer-wrap">
              <div class="inner">
                  <nav class="footer-nav">
                      <?php
                      $country = 'Global';
                      if($ci->ion_auth->logged_in()){
                          $country = $ci->ion_auth->user()->row()->country;
                      }
                      $ci->db->where('country', $country);
                      $ci->db->where('cluster_type', 'general');
                      $query = $ci->db->get('cluster1');
                      $cluster1 = $query->result();
                      ?>
                      <section>
                          <h4>SENSHA Products</h4>
                          <ul>
                              <?php foreach($cluster1 as $item):?>
                                  <li>
                                      <a href="<?php echo base_url("$coutry_iso"."page/general/$item->cluster_url");?>"><?php echo $item->cluster_name;?></a>
                                  </li>
                              <?php endforeach;?>
                              <!-- <li><a href="">PPF</a></li>
                              <li><a href="">Body</a></li>
                              <li><a href="">Window</a></li>
                              <li><a href="">Wheel / Tire</a></li>
                              <li><a href="">Mall/Bumper</a></li>
                              <li><a href="">Enine room</a></li>
                              <li><a href="">Interior</a></li>
                              <li><a href="">The other</a></li> -->
                          </ul>
                      </section>
                      <?php
                      $country = 'Global';
                      if($ci->ion_auth->logged_in()){
                          $country = $ci->ion_auth->user()->row()->country;
                      }
                      $ci->db->where('country', $country);
                      $ci->db->where('cluster_type', 'ppf');
                      $query = $ci->db->get('cluster1');
                      $cluster1 = $query->result();
                      ?>
                      <section>
                          <h4>SYNCSHIELD CUT Films</h4>
                          <ul>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>Toyota">TOYOTA</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>HONDA">HONDA</a>
                              </li>

                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MAZDA">MAZDA</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MERCEDES%20BENZ">MERCEDES BENZ</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>MITSUBISHI">MITSUBISHI</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>NISSAN">NISSAN</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>SUZUKI">SUZUKI</a>
                              </li>

                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>BMW">BMW</a>
                              </li>
                              <li>
                                  <a href="<?php echo base_url("$coutry_iso"."page/ppf/");?>ISUZU">ISUZU</a>
                              </li>

                              <!-- <li><a href="">Content 01</a></li>
                              <li><a href="">Content 02</a></li>
                              <li><a href="">Content 03</a></li>
                              <li><a href="">Content 04</a></li>
                              <li><a href="">Content 05</a></li>
                              <li><a href="">Content 06</a></li> -->
                          </ul>
                          <!-- <ul> -->
                          <?php //foreach($cluster1 as $item):?>
                              <!-- <li>
                              <a href="<?php echo base_url("page/product_category/$item->cluster_url");?>"><?php echo $item->cluster_name;?></a>
                          </li> -->
                          <?php //endforeach;?>
                          <!-- <li><a href="">Content 01</a></li>
                          <li><a href="">Content 02</a></li>
                          <li><a href="">Content 03</a></li>
                          <li><a href="">Content 04</a></li>
                          <li><a href="">Content 05</a></li>
                          <li><a href="">Content 06</a></li> -->
                          <!-- </ul> -->
                      </section>
                      <section>
                          <h4>About SENSHA</h4>
                          <ul>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/about");?>">About Sensha</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/about_ppf");?>">About PPF</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/global_network");?>">About Network</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/agent");?>">About Sole Agent</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/terms");?>">Terms of use</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/privacy");?>">Privacy</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/contact");?>">Contact</a></li>
                          </ul>
                      </section>
                      <section>
                          <h4>Q&A</h4>
                          <ul>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/SENSHA");?>">SENSHA</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Products");?>">Products</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Order Payment");?>">Order / Payment</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Delivery");?>">Delivery</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Return product");?>">Return product</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Discount");?>">Discount</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/Account");?>">Account</a></li>
                              <li><a href="<?php echo base_url("$coutry_iso"."page/qa_category/The others");?>">The others</a></li>
                          </ul>
                      </section>
                  </nav>
                  <section class="footer-sns">
                      <ul>
                          <li class="facebook">
                              <a href="">
                                  <span class="icon-facebook"></span>
                                  <span>Facebook</span>
                              </a>
                          </li>
                          <li class="line">
                              <a href="">
                                  <span class="icon-line"></span>
                                  <span>Line id</span>
                              </a>
                          </li>
                          <li class="wechat">
                              <a href="">
                                  <span class="icon-wechat"></span>
                                  <span>WeChat</span>
                              </a>
                          </li>
                          <li class="whatsapp">
                              <a href="">
                                  <span class="icon-whats-app"></span>
                                  <span>What’s App</span>
                              </a>
                          </li>
                      </ul>
                  </section>
              </div>
          </div>
          <div class="copyright">
              <small>Copyright © 2018 Sensha. All Rights Reserved.</small>
          </div>
      </footer>
      <!--------------------------modal--------------------------------------->
      <div class="header-form" style="display: none;"id="modal01">
          <ul class="cl-group">
              <li class="cl active"><span class="icon-lock"></span><span><?php echo $ci->lang->line('login_title', FALSE); ?></span></li>
          </ul>
          <div class="form-content">
              <div id="login" class="form-content-inner">
                  <p class="lead"><?php echo $ci->lang->line('login_text1', FALSE); ?></p>
                  <form id="formLogin" method="post" action="<?php echo base_url("page/user/doLogin");?>">
                      <div class="form-block">
                          <label for="CustomerEmail" class="hidden-label"><?php echo $ci->lang->line('login_text6', FALSE); ?></label>
                          <input type="text" id="CustomerEmail" class="user-input user-email" name="email" placeholder="">
                          <label for="CustomerPassword" class="hidden-label"><?php echo $ci->lang->line('login_text7', FALSE); ?></label>
                          <input type="password" id="CustomerPassword" class="user-input user-password" name="password" placeholder="">
                          <input type="submit" class="sign-in-btn btn" value="LOGIN" />
                      </div>
                  </form>
              </div>

          </div>
          <p class="note"><?php echo $ci->lang->line('login_text2', FALSE); ?> <a href="<?php echo base_url("page/register");?>"><?php echo $ci->lang->line('login_text3', FALSE); ?></a></p>
          <p class="note"><?php echo $ci->lang->line('login_text4', FALSE); ?><a href="<?php echo base_url("page/forgot_password");?>"><?php echo $ci->lang->line('login_text5', FALSE); ?></a></p>
      </div>
      <!----------------------------------------------------------------->
      <!--<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>-->
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/slick.min.js"></script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.fancybox.min.js"></script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/home.js"></script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/action.js"></script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.bxslider.js"></script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/owl.carousel.min.js"></script>

      <!-- sweetalert2 -->
      <script src="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.all.min.js")?>"></script>
      <link rel="stylesheet" href="<?=base_url("node_modules/sweetalert2/dist/sweetalert2.min.css")?>">
      <script>
      	;( function( $, window, document, undefined ){
          'use strict';
          var $list       = $( '.list' ),
              $items      = $list.find( '.list__item' ),
              setHeights  = function()
              {
                  $items.css( 'height', 'auto' );

                  var perRow = Math.floor( $list.width() / $items.width() );
                  if( perRow == null || perRow < 1 ) return true;

                  for( var i = 0, j = $items.length; i < j; i += perRow )
                  {
                      var maxHeight   = 0,
                          $row        = $items.slice( i, i + perRow );

                      $row.each( function()
                      {
                          var itemHeight = parseInt( $( this ).outerHeight() );
                          if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                      });
                      $row.css( 'height', maxHeight );
                  }
              };
          setHeights();
          $( window ).on( 'resize', setHeights );

      	})( jQuery, window, document );
      </script>
      <script>
      	$( ".how-use ul" ).addClass( "list" );
      </script>
      <script>
      	$( ".how-use ul li" ).addClass( "list__item" );
      </script>
      <script type="text/javascript">
      $(document).ready(function() {
          $(".search-button-m").fancybox();
          $(".sign-up").fancybox();
          $(".login").fancybox();
          $(".language").fancybox();
          $(".sp-login-btn").fancybox();

          $('#formLogin').submit(function(){
              // Swal.fire({
              //     title: 'Processing',
              //     allowOutsideClick:false,
              //     onBeforeOpen: () => {
              //         swal.showLoading();
              //     }
              // });

              $.ajax({
                  method: "POST",
                  url: $(this).attr('action'),
                  data: $( this ).serialize()
              })
              .done(function( msg ) {
                  r = JSON.parse(msg);
                  if(r.error == 0){
                      // Swal.fire({
                      //     type: 'success',
                      //     title: 'Login Success',
                      //     showConfirmButton: false,
                      //     allowOutsideClick:false
                      // });
                      setTimeout(function(){ window.location = ''; }, 1500);
                  }
                  else if(r.error == 1){
                      // Swal.fire({
                      //     type: 'error',
                      //     title: 'Please verify your email',
                      // });
                      alert('Please verify your email');
                  }
                  if(r.error == 2){
                      // Swal.fire({
                      //     type: 'error',
                      //     title: r.msg,
                      // });
                      alert(r.msg);
                  }

                  if(r.error == 3){
                      // Swal.fire({
                      //     type: 'error',
                      //     title: 'Not found you account.',
                      // });
                      alert('Not found you account.');
                  }
              })
              .fail(function( msg ) {
                  Swal.fire(
                      'Oops...',
                      msg,
                      'error'
                  );
              });

              return false;
          });
      });
      </script>

      <script>

      if ($('#gallery-thumbs').length > 0) {

          // Cache the thumb selector for speed
          var thumb = $('#gallery-thumbs').find('.thumb');

          // How many thumbs do you want to show & scroll by
          var visibleThumbs = 5;

          // Put slider into variable to use public functions
          var gallerySlider = $('#gallery').bxSlider({
              controls: false,
              pager: false,
              easing: 'easeInOutQuint',
              infiniteLoop: true,
              speed: 500,
              adaptiveHeight:true,
              auto: true,
              onSlideAfter: function (currentSlideNumber) {
                  var currentSlideNumber = gallerySlider.getCurrentSlide();
                  thumb.removeClass('pager-active');
                  thumb.eq(currentSlideNumber).addClass('pager-active');
              },

              onSlideNext: function () {
                  var currentSlideNumber = gallerySlider.getCurrentSlide();
                  slideThumbs(currentSlideNumber, visibleThumbs);
              },

              onSlidePrev: function () {
                  var currentSlideNumber = gallerySlider.getCurrentSlide();
                  slideThumbs(currentSlideNumber, visibleThumbs);
              }
          });

          // When clicking a thumb
          thumb.click(function (e) {

              // -6 as BX slider clones a bunch of elements
              gallerySlider.goToSlide($(this).closest('.thumb-item').index());

              // Prevent default click behaviour
              e.preventDefault();
          });

          // Thumbnail slider
          var thumbsSlider = $('#gallery-thumbs').bxSlider({
              controls:true,
              pager:false,
              adaptiveHeight:true,
              easing: 'easeInOutQuint',
              infiniteLoop: false,
              minSlides: 5,
              maxSlides: 2,
              slideWidth: 360,
              slideMargin: 10
          });

          // Function to calculate which slide to move the thumbs to
          function slideThumbs(currentSlideNumber, visibleThumbs) {

              // Calculate the first number and ignore the remainder
              var m = Math.floor(currentSlideNumber / visibleThumbs);

              // Multiply by the number of visible slides to calculate the exact slide we need to move to
              var slideTo = m * visibleThumbs;

              // Tell the slider to move
              thumbsSlider.goToSlide(m);
          }

          // When you click on a thumb
          $('#gallery-thumbs').find('.thumb').click(function () {

              // Remove the active class from all thumbs
              $('#gallery-thumbs').find('.thumb').removeClass('pager-active');

              // Add the active class to the clicked thumb
              $(this).addClass('pager-active');

          });
      }
      $(function(){
          var lang = '<?php echo (get_cookie('user_language') == '')?"Global":get_cookie('user_language');?>'

          $('input[value="'+lang+'"]').prop('checked', true);
          $('input[name="language"]').change(function(){
              var iso = $(this).data('iso');
              var old_iso = '<?php echo $coutry_iso;?>';
              var my_url = "<?php echo $ci->uri->uri_string();?>";
              $.ajax({
                  method: "POST",
                  url: '<?php echo base_url("page/change_lang");?>',
                  data: {'language': $(this).val()}
              })
              .done(function( msg ) {
                  if(msg == ''){
                      // console.log(iso);
                      // console.log(old_iso);
                      // console.log(my_url);
                      // console.log(msg);
                      var goto = my_url.replace(old_iso+'/', '');
                      if(iso == msg){
                          var goto = '';
                      }
                      if(iso == ""){
                          var goto = my_url.replace(old_iso+'/', '');
                      }
                      // console.log(goto);
                      // console.log();
                      window.location = '<?php echo base_url();?>'+goto;
                  }
                  else{

                      var goto = my_url.replace(old_iso+'/', msg+'/');
                      if(old_iso == ''){
                          var goto = msg+'/'+my_url;
                      }
                      if(my_url == old_iso){
                          var goto = msg;
                      }
                      // console.log(goto);
                      window.location = '<?php echo base_url();?>'+goto;
                  }
                  // window.location = '<?php base_url();?>'+"/"+iso+"/page";
                  // window.location = '';
                  // $('product').html(msg);
                  // r = JSON.parse(msg);
                  // console.log(r);

              })
              .fail(function( msg ) {
                  console.log(msg);
              });
          });
      })
      </script>
      <script src="<?php echo base_url("assets/sensha-theme/");?>js/jquery.showmore.js"></script>
      </body>
      </html>

    </div>
  <!-- .wrapper -->



<!-- <!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 12px 10px;
}

#container {
	margin: 10px;
	border: 1px solid #D0D0D0;
	box-shadow: 0 0 8px #D0D0D0;
}

p {
	margin: 12px 15px 12px 15px;
}
</style>
</head>
<body>
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>
</body>
</html> -->
