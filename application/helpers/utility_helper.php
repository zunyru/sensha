<?php
function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

function arrayToObject($d) {
    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return (object) array_map(__FUNCTION__, $d);
    }
    else {
        // Return object
        return $d;
    }
}

function random_password($len){
    srand((double)microtime()*10000000);
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    //$chars = "0123456789";
    $ret_str = "";
    $num = strlen($chars);
    for($i = 0; $i < $len; $i++)
    {
        $ret_str.= $chars[rand()%$num];
        $ret_str.="";
    }
    return $ret_str;
}


function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);

}

function keep_log($activity, $item='', $detail=''){
  $ci = get_instance();
  $uid = $ci->ion_auth->user()->row()->id;
  if($uid == ''){
    $uid = 0;
  }
  $data = array(
    'uid' => $uid,
    'activity' => $activity,
    'item' => $item,
    'detail' => $detail,
    'ip' => $ci->input->ip_address(),
    'user_agent' => $ci->input->user_agent()
  );
  $ci->db->insert('logs', $data);
}

function getStartAndEndDate($week, $year) {
  $dto = new DateTime();
  $dto->setISODate($year, $week);
  $ret['week_start'] = $dto->format('Y-m-d');
  $dto->modify('+6 days');
  $ret['week_end'] = $dto->format('Y-m-d');
  return $ret;
}

function get_count($cluster){
  $ci = get_instance();
  $ci->db->where('cluster_name', $cluster);
  $ci->db->where('cluster_type', 'general');
  $ci->db->where('country', 'Global');
  $q = $ci->db->get('cluster1');
  $cluster1 = $q->row();

  $ci->db->from('product_general');
  $ci->db->where('product_country', 'Global');
  $ci->db->where('cat_1', $cluster1->id);
  $num_results = $ci->db->count_all_results();
  return $num_results;
}

function update_cat(){
  $ci = get_instance();
  $ci->db->where('country <> "Global"');
  $q = $ci->db->get('cluster2');
  $r = $q->result();
  foreach ($r as $item) {
    if($item->level1!=''){
      $ci->db->where('id', $item->level1);
      $ci->db->where('country', 'Global');
      $q1 = $ci->db->get('cluster1');
      $r1 = $q1->row();

      if($r1){
        $ci->db->where('cluster_name', $r1->cluster_name);
        $ci->db->where('country', $item->country);
        $q11 = $ci->db->get('cluster1');
        $r11 = $q11->row();

        if($r11){
          $data['level1'] = $r11->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('cluster2', $data);
        }
      }
    }
  }

  $ci->db->where('country <> "Global"');
  $q = $ci->db->get('cluster3');
  $r = $q->result();
  foreach ($r as $item) {
    if($item->level1!=''){
      $ci->db->where('id', $item->level1);
      $ci->db->where('country', 'Global');
      $q1 = $ci->db->get('cluster1');
      $r1 = $q1->row();

      if($r1){
        $ci->db->where('cluster_name', $r1->cluster_name);
        $ci->db->where('country', $item->country);
        $q11 = $ci->db->get('cluster1');
        $r11 = $q11->row();

        if($r11){
          $data['level1'] = $r11->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('cluster3', $data);
        }
      }
    }

    if($item->level2!=''){
      $ci->db->where('id', $item->level2);
      $ci->db->where('country', 'Global');
      $q1 = $ci->db->get('cluster2');
      $r1 = $q1->row();

      if($r1){
        $ci->db->where('cluster_name', $r1->cluster_name);
        $ci->db->where('country', $item->country);
        $q11 = $ci->db->get('cluster2');
        $r11 = $q11->row();

        if($r11){
          $data['level2'] = $r11->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('cluster3', $data);
        }
      }
    }
  }


  $ci->db->where('product_country <> "Global"');
  $q = $ci->db->get('product_general');
  $r = $q->result();
  // print_r($r);
  foreach ($r as $item) {

    if($item->cat_1!=''){
      $ci->db->where('id', $item->cat_1);
      $ci->db->where('country', 'Global');
      $q1 = $ci->db->get('cluster1');
      $r1 = $q1->row();
      // echo 5;
      // print_r($r1);
      if($r1){
        $ci->db->where('cluster_name', $r1->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q11 = $ci->db->get('cluster1');
        $r11 = $q11->row();

        if($r11){
          // print_r($r11);

          $data['cat_1'] = $r11->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_general', $data);
        }
      }
    }

    if($item->cat_2!=''){
      $ci->db->where('id', $item->cat_2);
      $ci->db->where('country', 'Global');
      $q2 = $ci->db->get('cluster2');
      $r2 = $q2->row();

      if($r2){
        $ci->db->where('cluster_name', $r2->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q22 = $ci->db->get('cluster2');
        $r22 = $q22->row();

        if($r22){
          $data['cat_2'] = $r22->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_general', $data);
        }
      }
    }

    if($item->cat_3!=''){
      $ci->db->where('id', $item->cat_3);
      $ci->db->where('country', 'Global');
      $q3 = $ci->db->get('cluster3');
      $r3 = $q3->row();

      if($r3){
        $ci->db->where('cluster_name', $r3->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q33 = $ci->db->get('cluster3');
        $r33 = $q33->row();

        if($r33){
          $data['cat_3'] = $r33->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_general', $data);
        }
      }
    }

  }

  $ci->db->where('product_country <> "Global"');
  $q = $ci->db->get('product_ppf');
  $r = $q->result();
  // print_r($r);
  foreach ($r as $item) {

    if($item->cat_1!=''){
      $ci->db->where('id', $item->cat_1);
      $ci->db->where('country', 'Global');
      $q1 = $ci->db->get('cluster1');
      $r1 = $q1->row();
      // echo 5;
      // print_r($r1);
      if($r1){
        $ci->db->where('cluster_name', $r1->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q11 = $ci->db->get('cluster1');
        $r11 = $q11->row();

        if($r11){
          // print_r($r11);

          $data['cat_1'] = $r11->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_ppf', $data);
        }
      }
    }

    if($item->cat_2!=''){
      $ci->db->where('id', $item->cat_2);
      $ci->db->where('country', 'Global');
      $q2 = $ci->db->get('cluster2');
      $r2 = $q2->row();

      if($r2){
        $ci->db->where('cluster_name', $r2->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q22 = $ci->db->get('cluster2');
        $r22 = $q22->row();

        if($r22){
          $data['cat_2'] = $r22->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_ppf', $data);
        }
      }
    }

    if($item->cat_3!=''){
      $ci->db->where('id', $item->cat_3);
      $ci->db->where('country', 'Global');
      $q3 = $ci->db->get('cluster3');
      $r3 = $q3->row();

      if($r3){
        $ci->db->where('cluster_name', $r3->cluster_name);
        $ci->db->where('country', $item->product_country);
        $q33 = $ci->db->get('cluster3');
        $r33 = $q33->row();

        if($r33){
          $data['cat_3'] = $r33->id;
          $ci->db->where('id', $item->id);
          $ci->db->update('product_ppf', $data);
        }
      }
    }

  }
}


?>
