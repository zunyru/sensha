<?php
//Global
$lang['global_home'] = 'Home';
$lang['global_filter'] = 'Filter';
$lang['global_search'] = 'SEARCH';
$lang['global_logout'] = 'Logout';
$lang['global_import'] = 'Import';
$lang['global_export'] = 'Export';
$lang['global_tuhan_gate'] = 'Export Tuhan Gate CSV';
$lang['global_submit'] = 'Submit';
$lang['global_add'] = 'add';
$lang['global_edit'] = 'edit';
$lang['global_delete'] = 'delete';

//Admin Dashboard
$lang['admin_dashboard_title'] = 'Dashboard';
$lang['admin_dashboard_table_title'] = 'Monthly Transaction';
$lang['admin_dashboard_section1_title'] = 'GLOBAL SALES TOTAL';
$lang['admin_dashboard_section2_title'] = 'DOMESTIC SALES TOTAL';
$lang['admin_dashboard_table_basic_info'] = 'Basic info';
$lang['admin_dashboard_table_product'] = 'Product';
$lang['admin_dashboard_table_income'] = 'Income';
$lang['admin_dashboard_table_term'] = 'Term';
$lang['admin_dashboard_table_total'] = 'Total Sales Amount';
$lang['admin_dashboard_table_product_sales'] = 'Product Sales';
$lang['admin_dashboard_table_discount_amount'] = 'Discount Amount';
$lang['admin_dashboard_table_delivery_fee'] = 'Delivery fee';
$lang['admin_dashboard_table_no_of_item'] = 'No. of Item sales';
$lang['admin_dashboard_table_average'] = 'Average sales amount';
$lang['admin_dashboard_table_income2'] = 'Income';
$lang['admin_dashboard_table_pay_to_agent'] = 'Pay to agent';
$lang['admin_dashboard_table_expected_income'] = 'Expected income total';
$lang['admin_dashboard_section3_title'] = 'PAYMENT TOTAL';
$lang['admin_dashboard_table_credit_info'] = 'Credit Information';
$lang['admin_dashboard_table_term2'] = 'Term';
$lang['admin_dashboard_table_total_payment'] = 'Total Payment';
$lang['admin_dashboard_table_release'] = 'Release';
$lang['admin_dashboard_table_buy_products'] = 'Buy products';
$lang['admin_dashboard_section4_title'] = 'GRAND TOTAL';
$lang['admin_dashboard_table_grand_total'] = 'GRAND TOTAL';

//Admin Financial Manager
$lang['admin_financial_title'] = 'Financial Manager';
$lang['admin_financial_pay_credit'] = 'Pay credit';
$lang['admin_financial_country'] = 'Country';
$lang['admin_financial_credit'] = 'Credit';
$lang['admin_financial_amount_to_release'] = 'Amount to release';
$lang['admin_financial_submit'] = 'Submit';
$lang['admin_financial_tab1'] = 'Financial Manager (Income history)';
$lang['admin_financial_tab1_basic_info'] = 'Basic Info';
$lang['admin_financial_tab1_shipment_id'] = 'Shipment ID';
$lang['admin_financial_tab1_date_of_payment'] = 'Date of payment';
$lang['admin_financial_tab1_saller_info'] = 'Saller info';
$lang['admin_financial_tab1_adjusment'] = 'Adjusment';
$lang['admin_financial_tab1_country'] = 'Country';
$lang['admin_financial_tab1_agent_info'] = 'Agent info';
$lang['admin_financial_tab1_sa_name'] = 'SA Name';
$lang['admin_financial_tab1_sa_discount_ratio'] = 'SA discount ratio';
$lang['admin_financial_tab1_aa_name'] = 'AA Name';
$lang['admin_financial_tab1_aa_discount_ratio'] = 'AA discount ratio';
$lang['admin_financial_tab1_purchase_info'] = 'Purchase info';
$lang['admin_financial_tab1_country2'] = 'Country';
$lang['admin_financial_tab1_user_type'] = 'User Type';
$lang['admin_financial_tab1_user_id'] = 'User ID';
$lang['admin_financial_tab1_discount_ratio'] = 'Discount ratio';
$lang['admin_financial_tab1_sales_info'] = 'Sales info';
$lang['admin_financial_tab1_no_of_item'] = 'No. of Item';
$lang['admin_financial_tab1_sales_amount'] = 'Sales amount';
$lang['admin_financial_tab1_shipping_fee'] = 'Shipping fee';
$lang['admin_financial_tab1_discount_total'] = 'Discount total';
$lang['admin_financial_tab1_sales_total'] = 'Sales total';
$lang['admin_financial_tab1_pay_to_agent'] = 'Pay to agent';
$lang['admin_financial_tab1_expected_income'] = 'Expected income total';
$lang['admin_financial_tab2'] = 'Financial Manager (Credit transaction)';
$lang['admin_financial_tab2_basic_info'] = 'Basic Info';
$lang['admin_financial_tab2_date_of_payment'] = 'Date of payment';
$lang['admin_financial_tab2_pay_type'] = 'Pay type';
$lang['admin_financial_tab2_shipment_id'] = 'Shipment ID';
$lang['admin_financial_tab2_payer_info'] = 'Payer info';
$lang['admin_financial_tab2_country'] = 'Country';
$lang['admin_financial_tab2_account_type'] = 'Account Type';
$lang['admin_financial_tab2_detailed_info'] = 'Detailed info';
$lang['admin_financial_tab2_sa_country'] = 'SA Country';
$lang['admin_financial_tab2_sa_name'] = 'Sa Name';
$lang['admin_financial_tab2_credit_before_process'] = 'Credit before process';
$lang['admin_financial_tab2_description'] = 'Description';
$lang['admin_financial_tab2_amount_to_pay'] = 'Amount to pay';
$lang['admin_financial_tab2_credit_after_process'] = 'Credit after process';
$lang['admin_financial_tab2_current_credit'] = 'Current credit';

//Nation&lang
$lang['admin_nation_lang_title'] = 'Nation & Lang';
$lang['admin_nation_lang_country'] = 'Country';
$lang['admin_nation_lang_language'] = 'Language';
$lang['admin_nation_lang_country_flag'] = 'Country Flag';
$lang['admin_nation_lang_font'] = 'Font';
$lang['admin_nation_lang_reading'] = 'Reading';
$lang['admin_nation_lang_currency_name'] = 'Currency Name';
$lang['admin_nation_lang_currency_rate'] = 'Currency Rate Exchange';

//Account Settings
$lang['admin_account_setting_title'] = 'Account Setting';
$lang['admin_account_setting_id'] = 'ID';
$lang['admin_account_setting_email'] = 'Registered Contact (Email)';
$lang['admin_account_setting_password'] = 'Password';
$lang['admin_account_setting_user_id'] = 'User ID';
$lang['admin_account_setting_country'] = 'Country';
$lang['admin_account_setting_account_type'] = 'Account Type';
$lang['admin_account_setting_company_name'] = 'Company Name';
$lang['admin_account_setting_company_phone'] = 'Company Phone';
$lang['admin_account_setting_company_address'] = 'Company Address';
$lang['admin_account_setting_tax_id'] = 'Tax ID';
$lang['admin_account_setting_contact_person'] = 'Contact Person';
$lang['admin_account_setting_shipment_name'] = 'Shipment Name';
$lang['admin_account_setting_shipment_phone'] = 'Shipment Phone';
$lang['admin_account_setting_zipcode'] = 'Zipcode';
$lang['admin_account_setting_shipment_address'] = 'Shipment Address';
$lang['admin_account_setting_discount_setting'] = 'Discount Setting';
$lang['admin_account_setting_comission_setting'] = 'Comission Setting';
$lang['admin_account_setting_exclude_group'] = 'Exclude group';

//Sales History
$lang['admin_sales_history_title'] = 'Sales History';
$lang['admin_sales_history_account_type'] = 'Account Type';
$lang['admin_sales_history_company_name'] = 'Company Name';
$lang['admin_sales_history_item_id'] = 'Product ID';
$lang['admin_sales_history_item_name'] = 'Product Name';
$lang['admin_sales_history_item_amount'] = 'Item Amount';
$lang['admin_sales_history_sales_date'] = 'Sales Date';
$lang['admin_sales_history_sales_total_amount'] = 'Sales Total Amount';
$lang['admin_sales_history_payment_method'] = 'Payment Method';
$lang['admin_sales_history_payment_status'] = 'Payment Status';
$lang['admin_sales_history_shipping_status'] = 'Shipping Status';
$lang['admin_sales_history_create_sales_hitory'] = 'Create Sales History';
$lang['admin_sales_history_account_name'] = 'Account Name';
$lang['admin_sales_history_sale_item_category'] = 'Sale Item Category';

//Product Category
$lang['admin_product_cat_title'] = 'Product Category';
$lang['admin_product_cat_1st_cluster'] = '1st Cluster';
$lang['admin_product_cat_2nd_cluster'] = '2nd Cluster';
$lang['admin_product_cat_3rd_cluster'] = '3rd Cluster';
$lang['admin_product_cat_add_1st_cluster'] = 'Add 1st Cluster';
$lang['admin_product_cat_add_2nd_cluster'] = 'Add 2nd Cluster';
$lang['admin_product_cat_add_3rd_cluster'] = 'Add 3rd Cluster';
$lang['admin_product_cat_create_1st_cluster'] = 'Add 1st Cluster';
$lang['admin_product_cat_create_2nd_cluster'] = 'Add 2nd Cluster';
$lang['admin_product_cat_create_3rd_cluster'] = 'Add 3rd Cluster';
$lang['admin_product_cat_edit_1st_cluster'] = 'Edit 1st Cluster';
$lang['admin_product_cat_edit_2nd_cluster'] = 'Edit 2nd Cluster';
$lang['admin_product_cat_edit_3rd_cluster'] = 'Edit 3rd Cluster';
$lang['admin_product_cat_general'] = 'General';
$lang['admin_product_cat_ppf'] = 'PPF';
$lang['admin_product_cat_edit_product_cat'] = 'Edit Product Category';
$lang['admin_product_cat_name'] = 'Name';
$lang['admin_product_cat_country'] = 'Country';
$lang['admin_product_cat_type'] = 'Type';
$lang['admin_product_cat_cluster_name'] = 'Cluster Name';
$lang['admin_product_cat_cluster_name'] = 'Cluster new name';

//Product Management
$lang['admin_product_management_title'] = 'Product Management';
$lang['admin_product_management_upload'] = 'Upload Product Image';
$lang['admin_product_management_section1_title'] = 'Products General';
$lang['admin_product_management_section2_title'] = 'Products PPF';
$lang['admin_product_management_add_product_management'] = 'Add Product Management';
$lang['admin_product_management_edit_product_management'] = 'Edit Product Management';
$lang['admin_product_management_product_id'] = 'Product ID';
$lang['admin_product_management_product_name'] = 'Product Name';
$lang['admin_product_management_cat_1'] = 'Category 1';
$lang['admin_product_management_cat_2'] = 'Category 2';
$lang['admin_product_management_cat_3'] = 'Category 3';
$lang['admin_product_management_product_weight'] = 'Produc Weight';
$lang['admin_product_management_global_price'] = 'Global Price';
$lang['admin_product_management_domestic_price'] = 'Domestic Price';
$lang['admin_product_management_active'] = 'Status';
$lang['admin_product_management_fixed_price'] = 'Fixed Price';
$lang['admin_product_management_sa_price'] = 'SA Price';
$lang['admin_product_management_import_shipping'] = 'Import Shipping';
$lang['admin_product_management_import_tax'] = 'Import Tax';
$lang['admin_product_management_no_of_use'] = 'No Of Use';
$lang['admin_product_management_cost_per_use'] = 'Cost Per Use';
$lang['admin_product_management_fixed_delivery_price'] = 'Fixed Delivery Price';
$lang['admin_product_management_material'] = 'Material';
$lang['admin_product_management_item_type'] = 'Item Type';
$lang['admin_product_management_usage'] = 'Usage';
$lang['admin_product_management_parts'] = 'Parts';
$lang['admin_product_management_feature'] = 'Feature';
$lang['admin_product_management_no_of_stock'] = 'No. Of Stock';
$lang['admin_product_management_item_description'] = 'Item Description';
$lang['admin_product_management_content'] = 'Content';
$lang['admin_product_management_caution'] = 'Caution';
$lang['admin_product_management_html_content'] = 'HTML Content';
$lang['admin_product_management_image'] = 'Image';
$lang['admin_product_management_add_image'] = 'Add image';
$lang['admin_product_management_youtube_videos'] = 'Youtube Videos';
$lang['admin_product_management_unpurchasable_user'] = 'Unpurchasable User';
$lang['admin_product_management_active'] = 'Active';
$lang['admin_product_management_show_in_home_page'] = 'Show in home page';
$lang['admin_product_management_car_production_id'] = 'Car Production ID';
$lang['admin_product_management_begining_term_to_use'] = 'Begining Term To Use';
$lang['admin_product_management_ending_term_to_use'] = 'Ending Term To Use';
$lang['admin_product_management_create_product_mangement'] = 'Create Product Management';
$lang['admin_product_management_edit_product_mangement'] = 'Edit Product Management';
$lang['admin_product_management_import_product_mangement'] = 'Import Prouct Management';
$lang['admin_product_management_choose_file'] = 'Choose File import';
$lang['admin_product_management_choose_image'] = 'Choose Image';

//Delivery
$lang['admin_delivery_title'] = 'Delivery';
$lang['admin_delivery_country'] = 'Country';
$lang['admin_delivery_fixed_type'] = 'Fixed Type';
$lang['admin_delivery_fixed_amount'] = 'Fixed Amount';
$lang['admin_delivery_create_delivery'] = 'Create Delivery';
$lang['admin_delivery_fixed'] = 'Fixed';
$lang['admin_delivery_percent'] = 'Percent';
$lang['admin_delivery_edit_delivery'] = 'Edit Delivery';

//Exclude Group
$lang['admin_exclude_title'] = 'Exclude Group';
$lang['admin_exclude_add_exclude_group'] = 'Add Exclude Group';
$lang['admin_exclude_create_exclude_group'] = 'Create Exclude Group';
$lang['admin_exclude_edit_exclude_group'] = 'Edit Exclude Group';
$lang['admin_exclude_name'] = 'Name';
$lang['admin_exclude_category'] = 'Category';
$lang['admin_exclude_id'] = 'id';

//Discount Coupon
$lang['admin_discount_coupon_title'] = 'Discount Coupon';
$lang['admin_discount_coupon_add_discount_coupon'] = 'Add Discount Coupon';
$lang['admin_discount_coupon_create_discount_coupon'] = 'Create Discount Coupon';
$lang['admin_discount_coupon_edit_discount_coupon'] = 'Edit Discount Coupon';
$lang['admin_discount_coupon_coupon_name'] = 'Coupon Name';
$lang['admin_discount_coupon_coupon_code'] = 'Coupon Code';
$lang['admin_discount_coupon_discount_type'] = 'Discount Type';
$lang['admin_discount_coupon_discount'] = 'Discount(%)';
$lang['admin_discount_coupon_start_date'] = 'Start Date';
$lang['admin_discount_coupon_end_date'] = 'End Date';

//Webpage setting
$lang['admin_webpage_title'] = 'Webpage Setting';
$lang['admin_webpage_home'] = 'Home';
$lang['admin_webpage_about'] = 'About';
$lang['admin_webpage_ppf'] = 'PPF';
$lang['admin_webpage_agent'] = 'Agent';
$lang['admin_webpage_create_webpage_setting'] = 'Create Webpage Setting';
$lang['admin_webpage_edit_webpage_setting'] = 'Edit Webpage Setting';
$lang['admin_webpage_meta_name'] = 'Meta name';
$lang['admin_webpage_meta_desc'] = 'Meta Description';
$lang['admin_webpage_content'] = 'Content';

//News Post
$lang['admin_post_title'] = 'News Post';
$lang['admin_post_post_title'] = 'Title';
$lang['admin_post_post_content'] = 'Content';
$lang['admin_post_create_date'] = 'Create Date';
$lang['admin_post_create_new_post'] = 'Create New Post';
$lang['admin_post_edit_post'] = 'Edit News Post';

//Q&A Post
$lang['admin_qa_title'] = 'Q&A Post';
$lang['admin_qa_category'] = 'Category';
$lang['admin_qa_post_title'] = 'Title';
$lang['admin_qa_post_content'] = 'Content';
$lang['admin_qa_create_date'] = 'Create Date';
$lang['admin_qa_create_qa_post'] = 'Create Q&A Post';
$lang['admin_qa_edit_qa_post'] = 'Edit Q&A Post';

//Global Network
$lang['admin_gn_title'] = 'Global Network';
$lang['admin_gn_latitude'] = 'Latitude';
$lang['admin_gn_longitude'] = 'Longitude';
$lang['admin_gn_create_global_network'] = 'Create Global Network';
$lang['admin_gn_edit_global_network'] = 'Edit Global Network';


//Header
$lang['header_menu_sign_up'] = 'Sign up';
$lang['header_menu_login'] = 'Login';
$lang['header_menu_logout'] = 'Logout';
$lang['header_menu_cart'] = 'Cart';

//Global Navigation
$lang['navi_cut_film_title'] = 'PPF cut film';
$lang['navi_cut_film_title_search'] = 'Search';
$lang['navi_cut_film_easy_to'] = 'Find Product';
$lang['navi_cut_film_select_maker'] = 'Select Maker';
$lang['navi_cut_film_select_model'] = 'Select Model';
$lang['navi_cut_film_select_part'] = 'Select Part';
$lang['navi_cut_film_btn_search'] = 'Search';

//Login Pop up
$lang['login_title'] = 'LOGIN';
$lang['login_text1'] = 'Login Information';
$lang['login_text2'] = 'Don’t have an account?';
$lang['login_text3'] = 'Click here to sign up';
$lang['login_text4'] = 'Forgot password?';
$lang['login_text5'] = 'Click here to reset your password';
$lang['login_text6'] = 'Registered Contact (Email/Tel)';
$lang['login_text7'] = 'Password';

//Top Page
$lang['page_top_meta_title'] = 'SENSHA WORLD.com | Official Site';
$lang['page_top_meta_desc'] = 'SENSHA Official Website. We offer professional use car washing and coating products. This website gives about our product details and also company details.';
$lang['breadcrumb_home'] = 'Home';
$lang['page_top_section1_title'] = 'Value the car, value the safety';
$lang['page_top_section1_subtitle'] = 'SENSHA makes and sells quality car washing and coating products.';
$lang['page_top_section1_banner1_title'] = 'About SENSHA';
$lang['page_top_section1_banner1_desc'] = 'SENSHA has more than 800 detailing and coating shops and provide quality car washing and coating products in more than 35 countries.';
$lang['page_top_section1_banner2_title'] = 'About Paint Protection Film (PPF)';
$lang['page_top_section1_banner2_desc'] = 'SENSHA collaborately researched and developed with film manufacturers to materialize our Paint Protection Film, Syncshield. This protect your car body from steppingstone and scratches. Syncshield is our new flagship product.';
$lang['page_top_section2_title'] = 'Search SENSHA Products';
$lang['page_top_section2_subtitle'] = 'We offer a range of products that covers all your needs of car washing and coating. Our quality products satisfy not only car owners but also professionals.';
$lang['page_top_section3_title'] = 'Search Syncshield Cut Film';
$lang['page_top_section3_subtitle'] = 'Paint Protection Film that is specially cut and sized for different car models and easy to use.';
$lang['page_top_section4_title'] = 'SENSHA News';
$lang['page_top_section4_subtitle'] = 'New product release and SENSHA Family news.';
$lang['page_top_section4_seeall'] = 'Show all news';
$lang['page_top_section5_banner1_title'] = 'Global Network';
$lang['page_top_section5_banner1_desc'] = 'We, SENSHA, has a global distributor network (SENSHA Family) to deliver our quality products and service in the globe.';
$lang['page_top_section5_banner2_title'] = 'Sole Agent';
$lang['page_top_section5_banner2_desc'] = 'Contact your local SENSHA Family.';
$lang['page_top_section5_banner3_title'] = 'SENSHA Q&A';
$lang['page_top_section5_banner3_desc'] = 'Frequently asked questions.';

//Mail Magazine
$lang['mail_mag_title'] = 'SENSHA Mail Magazine';
$lang['mail_mag_subtitle'] = 'Entre your email address to get mail magazine.';
$lang['mail_mag_btn'] = 'Request';

//Category Page
$lang['page_cat_meta_title'] = 'XXX Product list | SENSHA WORLD.com';
$lang['page_cat_meta_desc'] = 'XXX Product list page. You can purchase SNNSHA car washing and coating products. Also, you can search to find product by purpose or target.';
$lang['page_cat_title'] = ' Product list';
$lang['page_cat_subtitle'] = ' related product page.';

//Search Page
$lang['page_keyword_search_meta_title'] = 'Keyword Search | SENSHA WORLD.com';
$lang['page_keyword_search_meta_desc'] = 'You can purchase SNNSHA car washing and coating products. Also, you can search to find product by purpose or target.';
$lang['page_keyword_search_title'] = 'Search result for: XXX';
$lang['page_keyword_search_subtitle'] = 'XXX related product search result page.';

//Filter Page
$lang['page_filter_search_meta_title'] = 'Detailed Search | SENSHA WORLD.com';
$lang['page_filter_search_meta_desc'] = 'You can purchase SNNSHA car washing and coating products. Also, you can search to find product by purpose or target.';
$lang['page_filter_search_title'] = 'Search dedicated PPF';
$lang['page_filter_search_subtitle'] = 'PPF detailed product search result page.';

//Product Detail
$lang['page_product_detail_meta_title'] = 'XXX Product Name XXX | SENSHA WORLD.com';
$lang['page_product_detail_meta_desc'] = 'XXX Product Description XXX';
$lang['page_product_detail_domestic'] = 'Domestic';
$lang['page_product_detail_international'] = 'International';
$lang['page_product_detail_contents'] = 'Contents';
$lang['page_product_detail_import_shipping'] = 'Import Shipping';
$lang['page_product_detail_import_duty'] = 'Import Duty';
$lang['page_product_detail_shipping'] = 'Shipping';
$lang['page_product_detail_delivery_fee'] = 'Admin Fee';
$lang['page_product_detail_tax'] = 'VAT';
$lang['page_product_detail_coupon_discount'] = 'Coupon Discount';
$lang['page_product_detail_user_discount'] = 'User Discount';
$lang['page_product_detail_use_credit'] = 'Use Credit';
$lang['page_product_detail_add_to_cart'] = 'Add to Cart';
$lang['page_product_detail_added'] = 'Added to Cart';
$lang['page_product_detail_added_caution'] = '* You can order either domestic or international products, but not both.';
$lang['page_product_detail_add_your_produts_to'] = 'Add your products to';
$lang['page_product_detail_wishlist'] = 'wishlist';
$lang['page_product_detail_origin'] = 'Origin';
$lang['page_product_detail_list_price'] = 'List Price';
$lang['page_product_detail_no_of_use'] = 'No of Use';
$lang['page_product_detail_cost_per_car'] = 'Cost per car';
$lang['page_product_detail_stackability'] = 'Stockability';
$lang['page_product_detail_minimum_quantity'] = 'Minimum order quantity';
$lang['page_product_detail_shipping_method'] = 'Shipping method';
$lang['page_product_detail_payment_method'] = 'Payment method';
$lang['page_product_detail_right_side_items'] = 'Items';
$lang['page_product_detail_right_proceed_payment'] = 'Proceed to payment';
$lang['page_product_detail_right_buy_now'] = 'Buy now';
$lang['page_product_detail_right_side_subtotal'] = 'Subtotal';
$lang['page_product_detail_right_side_minimum_cost'] = 'Minimum shipping cost';
$lang['page_product_detail_right_side_total'] = 'Total';
$lang['page_product_detail_right_side_login_to_order'] = 'Login to order';
$lang['page_product_detail_right_side_buy'] = 'Buy';
$lang['page_product_detail_right_side_recently'] = 'Recently viewed items';
$lang['page_product_detail_right_side_confirm_pay'] = 'Confirm and pay';
$lang['page_product_detail_number'] = 'No.';
$lang['mail_payment_to_customer_dear'] = 'Dear';
$lang['mail_payment_to_customer_sama'] = '';
$lang['mail_payment_to_customer'] = '
	Thank you for choosing SENSHA.<br>
	We have received your order as following.<br>
	You will obtain a shipping conformation email once your order is ready to be dispatched. Thank you for your patient.<br>
	<br>
	-----------------------------------------------------------<br>
	【Customer for Bank Transfer (Payment in Advance)】<br>
	※When a shipping cost is marked as "0 Yen", or a shipping method is other than EMS, a shipping cost is quoted separately. Please await updates from us.<br>
	-----------------------------------------------------------<br>
	<br>
	This is an email with your order details. Please keep this email until the transaction is completed.
';
$lang['mail_payment_to_customer_content'] = '**Order Details**';
$lang['mail_payment_to_customer_shipment_id'] = 'Order Number： ';
$lang['mail_payment_to_customer_order_date'] = 'Order Date： ';
$lang['mail_payment_to_customer_payment_method'] = 'Payment Method： ';
$lang['mail_payment_to_customer_shipping_method'] = 'Delivery Method： ';
$lang['mail_payment_to_customer_shipping_place'] = 'Delivery Address： ';
$lang['mail_payment_to_customer_sale_total_amount'] = 'Total Amount： ';
$lang['mail_payment_to_customer_item_amount'] = 'Quantity： ';
$lang['mail_payment_to_customer_item_price'] = 'Price： ';
$lang['mail_payment_to_customer_item_sub_total_amount'] = 'Product Amount Total： ';
$lang['mail_payment_to_customer_item_delivery_amount'] = 'Shipping Cost： ';
$lang['mail_payment_to_customer_item_discount_amount'] = 'Discounts ＆ Points used： ';
$lang['mail_payment_to_customer_item_use_credit'] = 'Points used： ';
$lang['mail_payment_to_customer_item_sale_subtotal_amount'] = 'Subtotal： ';
$lang['mail_payment_to_customer_item_admin_fee_amount'] = 'Fee： ';
$lang['mail_payment_to_customer_item_vat_amount'] = 'Tax： ';
$lang['mail_payment_to_customer_item_sale_total_amount'] = 'Total Amount： ';
//$lang['mail_payment_to_customer_item_total_amount'] = 'ご請求金額： ';
//$lang['mail_payment_to_customer_notice_title'] = '■お振込み先のご案内';
//$lang['mail_payment_to_customer_sale_total_amount_to_transfer'] = 'お振込金額: ';
//$lang['mail_payment_to_customer_transfer_account'] = '<<お振込先>>';
//$lang['mail_payment_to_customer_bank'] = '◆三菱UFJ銀行　池袋支店';
//$lang['mail_payment_to_customer_bank_account'] = '普通　6699778 口座名義 洗車の王国';
//$lang['mail_payment_to_customer_bank2'] = '◆みずほ銀行　池袋支店';
//$lang['mail_payment_to_customer_bank2_account'] = '普通　3357543 口座名義 洗車の王国';
//$lang['mail_payment_to_customer_caution'] = '※お振込完了後、振込先銀行名、お客様のお名前とご注文番号、そして振込み金額を';
//$lang['mail_payment_to_customer_caution2'] = 'info@sensha-world.comまたはinfo@cleanyourcar.jp';
//$lang['mail_payment_to_customer_caution3'] = 'までご一報頂きますようお願いいたします。';
//$lang['mail_payment_to_customer_caution4'] = 'お客様よりお振込み完了のご連絡を頂け次第、ご注文商品発送手配をさせて頂きます。';
$lang['mail_payment_to_customer_confirm_notice'] = '■Order Confirmation';
$lang['mail_payment_to_customer_confirm_notice2'] = 'You can check it from order history';
$lang['mail_payment_to_customer_confirm_notice3'] = '■Invoice';
$lang['mail_payment_to_customer_confirm_notice4'] = 'You can download this from the purchase history page after login.';
$lang['mail_payment_to_customer_confirm_notice5'] = '
	▼Attentions (Please read)▼<br>
	*Please be noted that you will not be able to change or chancel your order after you receiving this email, as we will proceed to arrange a shipment of your order after an inventory check.<br>
	*We dispatch your order as quickly as possible, however there may be delivery delays due to weather, disasters and traffic conditions.<br>
	*If you have any questions regarding products or shipping, please feel free to contact us.<br>
	*This email and any attachments are intended for the addressee(s) only and may be confidential. You should not read, copy, use or disclose the content without authorization. If you have received this email in error, please notify the sender as soon as possible, delete the email and destroy any copies. This notice should not be removed.<br>
	Thank you for your continuous supports and businesses with us.
';
$lang['mail_payment_to_customer_confirm_notice6'] = '▼ SENSHA NEWS ▼<br>Latest News';
$lang['mail_payment_to_customer_confirm_notice7'] = 'We believe that people clean their beloved cars leading to the safe driving society.';
$lang['mail_payment_to_customer_confirm_notice8'] = '
	Clean Your CAR<br>
	SENSHA CO., LTD.<br>
	1007-3, Kami-kasuya, Isehara-shi, Kanagawa 259-1141 Japan<br>
	(Phone) 0463-94-5106<br>
	(Fax) 0463-94-5108<br>
	(E-Mail) info@cleanyourcar.jp<br>
	(URL) <a href="http://sensha-world.com/">http://sensha-world.com/</a>
';
$lang['mail_payment_to_customer_mail_title'] = '[SENSHA] SENSHA Order Confirmation';

//Dashboard
$lang['page_dashboard_meta_title'] = 'Dashboard | SENSHA WORLD.com';
$lang['breadcrumb_dashboard'] = 'Dashboard';
$lang['page_dashboard_credit'] = 'Credit Point';
$lang['page_dashboard_use_credit'] = 'Use Credit Point';
$lang['page_dashboard_use_credit_desc1'] = 'Your current point: ';
$lang['page_dashboard_use_credit_desc2'] = 'JPY<br>You can use points for purchase, also request to get refunds on points in your admin page.';
$lang['page_dashboard_edit_account'] = 'Edit account information';
$lang['page_dashboard_edit_account_desc'] = 'Click here to edit delivery location and other information.';
$lang['page_dashboard_shopping_history'] = 'Shopping history';
$lang['page_dashboard_shopping_history_desc'] = 'Click here for downloading shopping history and invoices.';
$lang['page_dashboard_cart'] = 'Cart';
$lang['page_dashboard_cart_desc'] = 'Click here to check your cart and edit your cart.';
$lang['page_dashboard_wishlist'] = 'Wishlist';
$lang['page_dashboard_wishlist_desc'] = 'Click here to check your Wishlist';
$lang['page_dashboard_faq'] = 'Frequently asked questions';
$lang['page_dashboard_recommend'] = 'Recommended products';
$lang['page_dashboard_recommend_desc'] = 'Recommended products based upon your recent shopping history.';
$lang['page_dashboard_recent_purchase'] = 'Recent Purchased Item';
$lang['page_dashboard_recent_purchase_desc'] = 'Your recent purchased products';
$lang['page_dashboard_recently_viewed'] = 'Recently viewed products.';
$lang['page_dashboard_recently_viewed_desc'] = 'Shown recently viewed products.';

//Product General
$lang['page_product_general_material'] = 'Material';
$lang['page_product_general_painting'] = 'Painting';
$lang['page_product_general_emblem'] = 'Emblem';
$lang['page_product_general_glass'] = 'Glass';
$lang['page_product_general_tire'] = 'Tire';
$lang['page_product_general_tire_house'] = 'Tire house';
$lang['page_product_general_plating'] = 'Plating';
$lang['page_product_general_shaving'] = 'Shaving-polish';
$lang['page_product_general_unpainted_plastic'] = 'Unpainted plastic';
$lang['page_product_general_gum'] = 'Gum';
$lang['page_product_general_engine'] = 'Engine';
$lang['page_product_general_hose'] = 'Hose';
$lang['page_product_general_fabric'] = 'Fabric';
$lang['page_product_general_leather'] = 'Leather';
$lang['page_product_general_meter_panel'] = 'Meter panel';

$lang['page_product_general_usage'] = 'Usage';
$lang['page_product_general_protection'] = 'Protection';
$lang['page_product_general_glazing'] = 'Glazing';
$lang['page_product_general_self_healing'] = 'Self-healing';
$lang['page_product_general_water_repellent'] = 'Water repellent';
$lang['page_product_general_oil_repellent'] = 'Oil repellent';
$lang['page_product_general_yellowing'] = 'Yellowing resistance';
$lang['page_product_general_cracks'] = 'Cracks of the surface';
$lang['page_product_general_rainy'] = 'For rainy day';
$lang['page_product_general_night'] = 'Night, lights glare';
$lang['page_product_general_dust'] = 'Dust, sand';
$lang['page_product_general_dirty_dirt'] = 'Dirty dirt';
$lang['page_product_general_dirt'] = 'Dirt (of scale)';
$lang['page_product_general_oil_film'] = 'Oil film';
$lang['page_product_general_scratches'] = 'Scratches';
$lang['page_product_general_kusumi'] = 'Kusumi';
$lang['page_product_general_ring_stain'] = 'Ring stain';
$lang['page_product_general_stain'] = 'Stain';
$lang['page_product_general_oil'] = 'Oil stains';
$lang['page_product_general_tar'] = 'Tar, Pitch';
$lang['page_product_general_iron_powder'] = 'Iron powder';
$lang['page_product_general_brake_dust'] = 'Brake dust';
$lang['page_product_general_insects'] = 'Insects, bird droppings';
$lang['page_product_general_clouded'] = 'Clouded window';
$lang['page_product_general_washer'] = 'Washer';
$lang['page_product_general_deodorize'] = 'Deodorize';
$lang['page_product_general_sterilization'] = 'Sterilization';
$lang['page_product_general_antibacterial'] = 'Antibacterial';

$lang['page_product_general_item_type'] = 'Item Type';
$lang['page_product_general_basic_wash'] = 'Basic wash';
$lang['page_product_general_coating'] = 'Coating';
$lang['page_product_general_special_wash'] = 'Special wash';
$lang['page_product_general_ppf'] = 'PPF';
$lang['page_product_general_others'] = 'The others';

$lang['page_product_general_feature'] = 'Feature';
$lang['page_product_general_domestic_delivery'] = 'Domestic delivery';
$lang['page_product_general_free_sample'] = 'Free sample';
$lang['page_product_general_bottle'] = 'Bottle';

//Cart
$lang['page_cart_meta_title'] = 'Shopping Cart | SENSHA WORLD.com';
$lang['breadcrumb_cart'] = 'Cart';
$lang['page_cart_quantity'] = 'Quantity';
$lang['page_cart_price'] = 'Price';
$lang['page_cart_available'] = 'Available';
$lang['page_cart_unavailable'] = 'Unavailable';
$lang['page_cart_add_to_wishlist'] = 'Add to Wishlist';
$lang['page_cart_add_to_wishlist_login'] = 'Login to';
$lang['page_cart_delete'] = 'Delete';
$lang['page_cart_delivery'] = 'Delivery';
$lang['page_cart_approximately'] = 'Approximately XXX days';
$lang['page_cart_subtotal'] = 'XXX items subtotal';
$lang['page_cart_proceed'] = 'Proceed to payment';
$lang['page_cart_items_subtotal'] = 'items Subtotal';
$lang['page_cart_fix_price'] = 'Delivery Fee';
$lang['page_cart_estimates'] = 'Estimates Separately';
$lang['page_cart_estimates_notice'] = '';
$lang['page_cart_error'] = 'Please choose delevery method';

//Place delivery
$lang['page_place_delivery_meta_title'] = '';
$lang['page_place_delivery_use_registered'] = 'Use registered shipping address';
$lang['page_place_delivery_use_this'] = 'Use this address';
$lang['page_place_delivery_edit_address'] = 'Edit the address';
$lang['page_place_delivery_please_update_delivery_address'] = 'There is no data of delivery address. Please update your delivery address.';
$lang['page_place_delivery_change_address'] = 'Change registered address and use new address';
$lang['page_place_delivery_over_write'] = 'Over write and use new address';

//Payment
$lang['page_payment_meta_title'] = 'Payment | SENSHA WORLD.com';
$lang['breadcrumb_payment'] = 'Payment';
$lang['page_payment_choose_payment'] = 'Choose payment method';
$lang['you_donot_need_to_pay'] = 'You don\'t need to pay. Please click to "confirmation" to complete the transaction.';
$lang['page_payment_pay_with_amazon'] = 'Pay with your Amazon account';
$lang['page_payment_pay_with_paypal'] = 'Pay with your Paypal account';
$lang['page_payment_pay_with_alipay'] = 'Pay with your Alipay account';
$lang['page_payment_pay_with_bank_tranfer'] = 'Pay via bank transfer';
$lang['page_payment_pay_on_delivery'] = 'Pay on delivery';
$lang['page_payment_no_found_coupon'] = 'not found coupon';
$lang['page_payment_can_not_use'] = 'Can\'t use this coupon';

$lang['page_payment_pay_with_amazon_forsave'] = 'Amazon';
$lang['page_payment_pay_with_paypal_forsave'] = 'Paypal';
$lang['page_payment_pay_with_alipay_forsave'] = 'Alipay';
$lang['page_payment_pay_with_bank_tranfer_forsave'] = 'Bank Transfer';
$lang['page_payment_pay_on_delivery_forsave'] = 'Pay on Delivery';
$lang['page_payment_credit_point'] = 'Discount/Coupon';
$lang['page_payment_without_coupon'] = 'Without coupon';
$lang['page_payment_use_credit'] = ' Use Credit Point';
$lang['page_payment_your_credit1'] = 'You Credit Point Balance:';
$lang['page_payment_your_credit2'] = 'Enter points to be used.';
$lang['page_payment_enter_points'] = 'Enter points to be used.';
$lang['page_payment_use_all'] = 'Use all the points';
$lang['page_payment_you_can_use'] = 'You can use all the points for this payment.';
$lang['page_payment_coupon'] = 'With coupon';
$lang['page_payment_apply_coupon'] = 'Apply Coupon Code';
$lang['page_payment_enter_coupon'] = 'Enter Coupon Code';
$lang['page_payment_pay'] = 'Pay';

//Complete
$lang['page_complete_meta_title'] = 'Order Confirmation | SENSHA WORLD.com';
$lang['breadcrumb_complete'] = 'Order Confirmation';
$lang['page_complete_order_confirmation'] = 'Order Confirmation';
$lang['page_complete_your_order'] = 'Your order has been successfully sent to SENSHA. ';
$lang['page_complete_thank_you'] = '
<p>Thank you for your purchase. You will also receive an order confirmation email.</p>
<p>You can download an invoice on below order history page.</p>
<p>In a case that you do not receive or any emails from SENSHA for next 2 weeks, please do contact us via order history page.</p>';
$lang['page_complete_order_history'] = 'Order History';

$lang['breadcrumb_contact_confirm'] = 'Contact Confirmation';
$lang['page_contact_name'] = 'Name';
$lang['page_contact_email'] = 'Email';
$lang['page_contact_message'] = 'Message';
$lang['page_contact_submit'] = 'Confirm';
$lang['page_contact_tel'] = 'Telephone';
$lang['page_contact_country'] = 'Country';

$lang['breadcrumb_contact_complete'] = 'Contact Completion';
$lang['page_contact_complete_confirmation'] = 'Contact Completion';
$lang['page_contact_complete_your_message'] = 'Your message was sent successfully.';
$lang['page_contact_complete_thank_you'] = '<p>Your message was sent successfully. We will get back to you in the few days from our staff.</p>';

//Cancel
$lang['page_cancel_meta_title'] = 'Cancel Order | SENSHA WORLD.com';
$lang['breadcrumb_cancel'] = 'Cancel Order';
$lang['page_cancel_order_confirmation'] = 'Cancel Order';
$lang['page_cancel_your_order'] = 'Your order was canceled. ';
$lang['page_cancel_thank_you'] = '
<p>Your current order was canceled.</p>
<p>In a case that you do not intend to cancel your order, please try again with another payment method.</p>';

//Account Info
$lang['page_account_info_meta_title'] = 'Account information | SENSHA WORLD.com';
$lang['breadcrumb_account_info'] = 'Personal info';
$lang['page_account_info_account_info'] = 'Account information';
$lang['page_account_info_edit_account_info'] = 'Edit Account Information';
$lang['page_account_info_id'] = 'ID';
$lang['page_account_info_email'] = 'Registered Contact';
$lang['page_account_info_telephone'] = 'Telephone';
$lang['page_account_info_password'] = 'Password';
$lang['page_account_info_country'] = 'Country';
$lang['page_account_info_state'] = 'State';
$lang['page_account_info_edit'] = 'Edit';

$lang['page_shipping_info_meta_title'] = 'Edit Shipping Information｜SENSHA WORLD.com';
$lang['breadcrumb_shipping_info'] = 'Edit Shipping Information';
$lang['page_shipping_info_shipping_info'] = 'Shipping information';
$lang['page_shipping_info_name'] = 'Contact Person';
$lang['page_shipping_info_company'] = 'Company';
$lang['page_shipping_info_zip'] = 'Zipcode';
$lang['page_shipping_info_address'] = 'Address';
$lang['page_shipping_info_state'] = 'State';
$lang['page_shipping_info_country'] = 'Country';
$lang['page_shipping_info_telephone'] = 'Telephone';
$lang['breadcrumb_confirm_shipping_info'] = 'Confirm shipping info';
$lang['page_confirm_shipping_info'] = 'Confirm shipping info';
$lang['breadcrumb_complete_shipping_info'] = 'Complete shipping info';
$lang['page_complete_shipping_info'] = 'Complete shipping info';
$lang['page_complete_shipping_info_caption'] = 'You edit shipping info successfully';
$lang['page_complete_shipping_info_description'] = 'Your shipping information has been updated. Go to Account Information Page if you need to check the updates.';

$lang['page_invoice_info_meta_title'] = 'Edit invoice/receipt information｜SENSHA WORLD.com';
$lang['breadcrumb_invoice_info'] = 'Edit invoice/receipt information';
$lang['page_invoice_info_change_invoice'] = 'Edit invoice/receipt information';
$lang['page_invoice_info_notice'] = '*Please edit only if your invoice address is different from the above shipping address.';
$lang['page_invoice_info_name'] = 'Contact Person';
$lang['page_invoice_info_company'] = 'Company';
$lang['page_invoice_info_zip'] = 'Zipcode';
$lang['page_invoice_info_address'] = 'Address';
$lang['page_invoice_info_state'] = 'State';
$lang['page_invoice_info_country'] = 'Country';
$lang['page_invoice_info_telephone'] = 'Telephone';
$lang['page_invoice_info_tax'] = 'Tax ID';
$lang['breadcrumb_confirm_invoice_info'] = 'Confirm invoice/receipt information';
$lang['page_confirm_invoice_info'] = 'Confirm invoice/receipt information';
$lang['breadcrumb_complete_invoice_info'] = 'Complete invoice/receipt information';
$lang['page_complete_invoice_info'] = 'Complete invoice/receipt information';
$lang['page_complete_invoice_info_caption'] = 'You edit invoice/receipt information successfully';
$lang['page_complete_invoice_info_description'] = 'Your invoice/receipt information has been updated. Go to Account Information Page if you need to check the updates.';

//Edit account info
$lang['page_edit_account_meta_title'] = 'Edit Account Information | SENSHA WORLD.COM';
$lang['breadcrumb_edit_account'] = 'Edit account information';
$lang['page_edit_account_edit'] = 'Enter XXX';
$lang['page_edit_account_update'] = 'Update';

//Check Account
$lang['page_check_account_meta_title'] = 'Check Account Information | SENSHA WORLD.com';
$lang['breadcrumb_check_account'] = 'Check account information';
$lang['page_check_account_confirm_title'] = 'Confirm account information';
$lang['page_check_account_confirm'] = 'Confirm';

//Complete Personal Info
$lang['page_complete_change_account_meta_title'] = 'Account Information Change Confirmation | SENSHA WORLD.com';
$lang['breadcrumb_complete_change_account'] = 'Confirmed';
$lang['page_complete_change_account_confirmed'] = 'Confirmed';
$lang['page_complete_change_account_updated'] = 'Your account information has been updated';
$lang['page_complete_change_account_your_account_informaion'] = 'Your account information has been updated. Go to Account Information Page if you need to check the updates.';

//Order History
$lang['page_order_history_meta_title'] = 'Order History | SENSHA WORLD.com';
$lang['breadcrumb_order_history'] = 'Order history';
$lang['page_order_history_order_history'] = 'Order history';
$lang['page_order_history_shipment_id'] = 'Shipment ID';
$lang['page_order_history_data_of_order'] = 'Data of order';
$lang['page_order_history_delivery_address'] = 'Delivery address';
$lang['page_order_history_total'] = 'Total Amount';
$lang['page_order_history_delivery_mothod'] = 'Delivery method';
$lang['page_order_history_domestic_post'] = 'Domestic post';
$lang['page_order_history_ems'] = 'INTERNATIONAL STANDARD SHIPMENT: EMS';
$lang['page_order_history_exw'] = 'BUYER TRANSPORTATION';
$lang['page_order_history_exw_notice'] = '*All Shipment fees as well as pick up from SELLER warehouse is arranged & paid by BUYER';
$lang['page_order_history_cif'] = 'SELLER TRANSPORTATION';
$lang['page_order_history_cif_notice'] = '*Using SELLER\'s corporate forwarder/transport & estimate quote all total shipping fee to BUYER';
$lang['page_order_history_show_more'] = 'Show more';
$lang['page_order_history_invoice_download'] = 'Invoice download';
$lang['page_order_history_receipt_download'] = 'Receipt download';
$lang['page_order_history_claim_shipment'] = 'Claim this shipment';
$lang['page_order_history_item_sub_total_amount'] = 'Item Sub Total Amount';
$lang['page_order_history_delivery_amount'] = 'Delivery Amount';
$lang['page_order_history_discount_amount'] = 'Discount Amount';
$lang['page_order_history_amount'] = 'Amount';
$lang['page_order_history_sub_total'] = 'Subtotal';
$lang['page_order_history_show_more'] = 'Show more';
$lang['page_order_history_show_less'] = 'Show less';

//Delivery Claim
$lang['page_delivery_claim_meta_title'] = 'Delivery Claim | SENSHA WORLD.com';
$lang['breadcrumb_delivery_claim'] = 'Delivery Claim';
$lang['page_delivery_claim_order_number'] = 'Order Number';
$lang['page_delivery_claim_about_issue'] = 'Claim form';
$lang['page_delivery_claim_issue_details'] = 'Reason for claim';
$lang['page_delivery_claim_not_obtained_products'] = 'The items are not delivered at all';
$lang['page_delivery_claim_not_obtained_any_products'] = 'Currently you have available fo shopping. tou can add the number not over than you total points.';
$lang['page_delivery_claim_missing_products'] = 'The items are not delivered partly';
$lang['page_delivery_claim_obtained_products_but_missing'] = 'Currently you have available fo shopping. tou can add the number not over than you total points.';
$lang['page_delivery_claim_obtained_wrong_products'] = 'The items are not items as you orderd';
$lang['page_delivery_claim_case_obtained_wrong_products'] = 'Currently you have available fo shopping. tou can add the number not over than you total points.';
$lang['page_delivery_claim_others'] = 'Others';
$lang['page_delivery_claim_email'] = 'Email address';
$lang['page_delivery_claim_describe_issues'] = 'Describe the claim in detail';
$lang['page_delivery_claim_comments'] = 'Comments';
$lang['page_delivery_claim_form'] = 'Claim Form';
$lang['page_delivery_claim_send'] = 'Send';

//Confirm Claim
$lang['page_delivery_claim_confirm_meta_title'] = 'Delivery Claim Confirmation | SENSHA WORLD.com';
$lang['breadcrumb_delivery_claim_confirm'] = 'Confirm the claim';
$lang['page_delivery_claim_confirm_order_number'] = 'Confirm the claim for shipment id';
$lang['page_delivery_claim_confirm_order_number'] = 'Ordered items';
$lang['page_delivery_claim_confirm_delivery_type'] = 'Delivery type';
$lang['page_delivery_claim_confirm_reason'] = 'Reason';
$lang['page_delivery_claim_confirm_claim'] = 'Claim';
$lang['page_confirm_claim_confirm'] = 'Send';
$lang['page_confirm_claim_check_box'] = 'Currently you have available for shopping. You can add the number not over than your total points.';

//Complete Claim
$lang['page_complete_claim_meta_title'] = 'Claim Completed | SENSHA WORLD.com';
$lang['breadcrumb_complete_claim'] = 'Claim Completed';
$lang['page_complete_claim_claim_sent'] = 'Claim completed';
$lang['page_complete_claim_your_claim'] = 'Your claim was successfully completed.';
$lang['page_complete_claim_we_check'] = 'We check your email and respond to you as soon as possible. Please accept our deep apologies any inconvenience you have experienced. We are working on this to resolve it quickly.';

//Claim Mail
//Claim Mail
$lang['claim_mail_title'] = 'We have received your inquiry for delivery [SENSHA]';
$lang['claim_mail_cliant_en'] = 'Hi,';
$lang['claim_mail_cliant_jp'] = '';
$lang['claim_mail_paragraph01'] = '  <br><br><br>
  This is SENSHA customer service.<br>
  We have received your inquiry as following.<br><br>
  ============================<br>';
$lang['claim_mail_shipment_id'] = 'Order Number  :';
$lang['claim_mail_order_date'] = 'Order Date  :';
$lang['claim_mail_email'] = 'EMAIL  :';
$lang['claim_mail_reason'] = 'Issue Details  : ';
$lang['claim_mail_comment'] = 'Please describe an issue in details :';
$lang['claim_mail_paragraph02'] = ' ============================<br>
  <br>
  <br>
  -----------------------------------------------------------<br>
  ■Order Confirmation<br>
 Please check this from the purchase history page<br>
  https://sensha-world.com/page/order_history<br>
  -----------------------------------------------------------<br>
  <br>
  <br>
▼Attentions (Please read)▼<br>
  <br>
We will check your inquiry and come back to you shortly. <br>
Thank you for your patient.<br>
  <br>
  <br>
This email and any attachments are intended for the addressee(s) only and may be confidential. You should not read, copy, use or disclose the content without authorization. If you have received this email in error, please notify the sender as soon as possible, delete the email and destroy any copies. This notice should not be removed.<br>
  <br>
  <br>
Thank you for your continuous supports and businesses with us.<br>
  <br>
  <br>
  ▼ SENSHA NEWS ▼<br>
Latest News<br>
  http://sensha.enfete.co/jp/page/news<br>
  <br>
  ======================<br>
We believe that people clean their beloved cars leading to the safe driving society.<br>
  ======================<br>
  <br>
  Clean Your CAR<br>
SENSHA CO., LTD.<br>
1007-3, Kami-kasuya, Isehara-shi, Kanagawa 259-1141 Japan<br>
  (Phone) 0463-94-5106<br>
  (Fax) 0463-94-5108<br>
  (E-Mail) info@cleanyourcar.jp<br>
  (URL) http://www.cleanyourcar.jp<br>
  (World site) http://sensha-world.com/<br>';

//Wishlist
$lang['page_wishlist_meta_title'] = 'Wishlist | SENSHA WORLD.com';
$lang['breadcrumb_wishlist'] = 'Wishlist';
$lang['page_wishlist_wishlist'] = 'Your Wishlist';
$lang['page_wishlist_delete'] = 'Delete';

//About Page
$lang['page_about_meta_title'] = 'About SENSHA | SENSHA';
$lang['page_about_meta_desc'] = '';
$lang['breadcrumb_about_sensha'] = 'About SENSHA';
$lang['page_about_key_visual_title'] = 'CAPTIVATE YOU <br>BY REAL CAR BEAUTY';
$lang['page_about_key_visual_subtitle'] = 'WITH PAINT PROTECTION FILM';
$lang['page_about_key_visual_desc'] = 'Syncshield is a special paint protection film developed to protect vehicle body surface.';
$lang['page_about_sensha_way'] = 'THE SENSHA WAY';
$lang['page_about_section1_title'] = 'We offer high quality products and reliable services';
$lang['page_about_section1_desc'] = 'At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.<br>We can help you manage and advance your car condition that can be measured through visible results';
$lang['page_about_section2_title'] = 'COMMITMENT IN QUALITY AND RELIABILITY';
$lang['page_about_section2_subtitle'] = 'Pride in professional quality products';
$lang['page_about_section2_content'] = '
        <p>Our products are engaged in 800 our directly-managed shops and franchise networks in 18 countries. We continuously satisfy customer requiqmrents by focusing on Quality and Reliability. This is the main reason why we have Research and Development, and Manufacturing in house in Japan.</p>
        <p>Feedback is gathered daily basis and utlized to advance our existing procuts and even for new product developments. In fact, our products are designed to be able to performe their capabilities, although a temperature is extreme: -30oC to + 40oC</p>
        <p>At SENSHA, "Quality and Reliability" are necessary not only for the products and services offered to customers whereas also for SENSHA\'s attitude to society and how each SENSHA staff member carries out their work. SENSHA specifically understands quality as desctibed below:</p>
        <ul>
          <li>-  Quality of products</li>
          <li>-  Quality of sales and service</li>
          <li>-  Quality in terms of variety and delivery</li>
          <li>-  Quality of business</li>
        </ul>
        <p>SENSHA family around the world will support your business. Track record of over 800 stores is the proof.</p>
	';
$lang['page_about_section2_img1_title'] = 'Marketing';
$lang['page_about_section2_img1_txt'] = '
				1. Marketing<br>
				2. Staff Training<br>
				3. Local Shop<br>
				4. Online Shop';
$lang['page_about_section2_img2_title'] = 'Service';
$lang['page_about_section2_img2_txt'] = '
				1. Marketing<br>
				2. Staff Training<br>
				3. Local Shop<br>
				4. Online Shop';
$lang['page_about_section2_img3_title'] = 'Reserch and<br>development';
$lang['page_about_section2_img3_txt'] = '
				1. Marketing Survey<br>
				2. Research &Development<br>
				3. Domestic Experiment<br>
				4. Monitoring<br>
				5. International Experimen';
$lang['page_about_section3_title'] = 'WHAT WE PROVIDE';
$lang['page_about_section3_topic1_title'] = 'Full Product Line Up';
$lang['page_about_section3_topic1_content'] = '<p>SENSHA offers a full product line up to respond all our customers\' needs in the car coating and washing. This avoids customers to use products from multiple suppliers which could have compatibility issues. The compatibility among our products was throughly checked so that you will not experience any compatibility issues</p>';
$lang['page_about_section3_topic2_title'] = 'Part and Problem Specific Approach';
$lang['page_about_section3_topic2_content'] = '<p>We develope and manufactur products by understanding which materials are used to which parts. Therefore, all of our products are "Part and/or Problem Specific". For instance, in terms of coating, you cannot use a coating agent for paint surfaces to wheels. Also, you should not use car shampoo to remove iron powders.</p>
    <p>All our products were designed to undestand which materials and parts the products to be applied in order to maximize their performances and finishing.</p>';
$lang['page_about_section3_cat1_title'] = 'BODY';
$lang['page_about_section3_cat1_desc'] = 'Shiny and paint protection, cover minor scratches and stone chips, slowing down the corrosion';
$lang['page_about_section3_cat2_title'] = 'WINDOW';
$lang['page_about_section3_cat2_desc'] = 'Sparkling clean windows and mirrors – without leaving behind streaks, scratches, residue or lint';
$lang['page_about_section3_cat3_title'] = 'WHEEL/TIRE';
$lang['page_about_section3_cat3_desc'] = 'Stay black and shiniest shine, no more etching or spotting';
$lang['page_about_section3_cat4_title'] = 'MALL/BUMPER';
$lang['page_about_section3_cat4_desc'] = 'Restore and refresh plastic surface with stuck dirt and wax into as new one';
$lang['page_about_section3_cat5_title'] = 'ENGINE ROOM';
$lang['page_about_section3_cat5_desc'] = 'Protect and coat important components of engine room, restore the luster and rust proofing';
$lang['page_about_section3_cat6_title'] = 'INTERIOR';
$lang['page_about_section3_cat6_desc'] = 'Polish, protect the interior work and restore the natural luster with very long-lasting effect';
$lang['page_about_section4_title'] = 'GLOBAL EXPANSION OF SENSHA';
$lang['page_about_section4_desc'] = '"We offer our products and service in 16 countries and regions all over the world! Visit your local web site for further information."';
$lang['page_about_section4_contact'] = 'CONTACT US';
$lang['page_about_section4_head_office'] = 'HEAD OFFICE';
$lang['page_about_section4_office_address'] = 'OFFICE ADDRESS';
$lang['page_about_section4_office_address_content'] = '1007-3, Kami-kasuya, Isehara-shi, Japan';
$lang['page_about_section4_phone'] = 'PHONE';
$lang['page_about_section4_phone_content'] = '(+81) 463-94-5106';
$lang['page_about_section4_link_to_contact'] = 'Contact Us';

//About PPF
$lang['page_about_ppf_meta_title'] = 'About PPF｜SENSHA WORLD.com';
$lang['page_about_ppf_meta_desc'] = '';
$lang['breadcrumb_about_ppf'] = 'About PPF';
$lang['page_about_ppf_key_visual_title'] = 'CAPTIVATE YOU <br>BY REAL CAR BEAUTY';
$lang['page_about-ppf_key_visual_subtitle'] = 'WITH PAINT PROTECTION FILM';
$lang['page_about-ppf_key_visual_desc'] = 'Syncshield is a special paint protection film developed to protect vehicle body surface.';
$lang['page_about_ppf_section1_title'] = 'WHAT IS PAINT PROTECTION FILM?';
$lang['page_about_ppf_section1_desc'] = 'It is popularly called PPF taken from the initial letters of Paint Protection Film. 
The purpose of PPF is to protect the body surface, prevention of fading colors and scratch protection, etc.';
$lang['page_about_ppf_section1_imgtxt1'] = 'Exfoliate Layer';
$lang['page_about_ppf_section1_imgtxt2'] = 'Self Healing Layer';
$lang['page_about_ppf_section1_imgtxt3'] = 'Non-yellowing type<br>polyurethane film';
$lang['page_about_ppf_section1_imgtxt4'] = 'Adhesive Layer';
$lang['page_about_ppf_section1_imgtxt5'] = 'Exfoliate Layer';
$lang['page_about_ppf_section1_desc2'] = '<p>Most PPF products are from the U.S. and the manufacturers are limted. Within such industry, Syncshield was developed in Japan by SENSHA in cooperation with several Japanese film related manufacturers and will be the new flagship product of the SENSHA Brand.</p>
    <p>Compared to other popular products in the market, Syncshield has many unique performance enhanced features added. Its on a performance level of its own.</p>';
$lang['page_about_ppf_section1_feature1'] = 'Self-recovery';
$lang['page_about_ppf_section1_feature2'] = 'Anti-fouling';
$lang['page_about_ppf_section1_feature3'] = 'Water repellent';
$lang['page_about_ppf_section1_feature4'] = 'Oil repellent';
$lang['page_about_ppf_section1_feature5'] = 'Anti-solvent';
$lang['page_about_ppf_section2_title'] = 'SERIAL SEARCH';
$lang['page_about_ppf_section2_input'] = 'Input your serial No.';
$lang['page_about_ppf_section2_search'] = 'Search';
$lang['page_about_ppf_section2_desc'] = 'Each Syncshield has a serial number assigned. Customers may receive the serial number information from the SENSHA family shop that served you. Enter the number in the Serial Search box and it will show results containing the shipped date from Japan, the arrival date in your country, service shop name, your car model and body color. If the serial number you enter shows result of "Not Found", then the PPF you purchaesed is not an authenticated one. By confirming the authenticity through this system, we guarantee it is 100% made by SENSHA.';
$lang['page_about_ppf_section3_title'] = 'QUALITY TEST';
$lang['page_about_ppf_section3_subtitle'] = 'This video shows the performance of our Syncshield compared against other products.';
$lang['page_about_ppf_section4_title'] = 'HOW PPF WORKS';
$lang['page_about_ppf_section4_item1_title'] = 'LUSTROUS';
$lang['page_about_ppf_section4_item1_desc'] = 'This picture shows Syncshield pasted on a dark blue car body panel. As you see, an old and dull looking body panel (right half side of picture) is revived with luster and looking like a high-class brand new body (left half side of picture). By applying Syncshield, you can restore the luster and shine of the body surface on your car just like brand new. You can experience extra-shine if your car is new, a deeper luster if it has condensed color.';
$lang['page_about_ppf_section4_item2_title'] = 'SCRATCH RESISTANCE, THE "SELF HEALING EFFECT"';
$lang['page_about_ppf_section4_item2_desc'] = 'The most unique character of Syncshield is its scratch resistancy. When you get fine scratches, our PPF carries a "Self Healing Effect" to prevent fine scratches from getting on your car. This is called Soft Coating which is a brand new technology born from the completely opposite idea of Hard Coating used for protection films for smart phones and windshields and fixes the scratches immediately (Please see video, the scratches caused by wire brush is fixed immediately). Other brand products require heat to fix the scratches, whereas Syncshield heals under normal temperature. No hot air or hot water necessary!';
$lang['page_about_ppf_section4_item3_title'] = 'WATER REPELLENCY';
$lang['page_about_ppf_section4_item3_desc'] = 'Syncshield has excellent water repellency. The result of water contact angle test was 110℃, which is the level equivalent to water repellent glass coating. No need for glass coating or waxing.';
$lang['page_about_ppf_section4_item4_title'] = 'OIL REPELLENCY';
$lang['page_about_ppf_section4_item4_desc'] = 'Tars and pitches from asphalt roads stick on the vehicle body. Oily stains from exhaust gas also is a reason for the vehicle to get dirty. To prevent these stubborn stains, we strengthened oil repellency also. Contact angle test using hexadecane resulted in 65℃. This video shows what happens when aerozole engine parts cleaner is sprayed on our PPF.';
$lang['page_about_ppf_section4_item5_title'] = 'SOLVENT RESISTANCE';
$lang['page_about_ppf_section4_item5_desc'] = 'Vehicles are exposed not only to tars and pitches, but also to other liquid like maintenance cleaners. Gasoline, etc. Many of these liquid contain organic solvent which cause severe blemishes to PPF. Syncshield strengthened solvent resistancy in addition to the oil repellency. This video features the tests conducted to showcase the different performances level of Syncshield.';
$lang['page_about_ppf_section5_title'] = 'ABOUT PPF';
$lang['page_about_ppf_section5_subtitle'] = '';
$lang['page_about_ppf_section5_topic1_title'] = 'MATERIAL';
$lang['page_about_ppf_section5_topic1_desc'] = 'Generally, PVC or TPU was used as the base material (film) for PPF. The positive for PVC is the low cost of production. On the other hand, the negative is the weak weatherability, not much fit as the material for PPF.';
$lang['page_about_ppf_section5_topic2_title'] = 'TRANSPARANCY';
$lang['page_about_ppf_section5_topic2_desc'] = 'High transparancy is a must for basic feature. We strongly focused on high transparency upon development in order to keep the texture of body colors different by each model. No one will notice that PPF is applied and being unnoticed is a very important basic feature for a PPF.';
$lang['page_about_ppf_section5_topic3_title'] = 'SMOOTHNESS';
$lang['page_about_ppf_section5_topic3_desc'] = 'The ultimate smoothness Syncshield pursued is one of a mirror surface. We realized such smoothness.The ultimate smoothness, so no one will notice that PPF is applied, is a very important feature for a PPF.';
$lang['page_about_ppf_section5_topic4_title'] = 'FLEXIBILITY';
$lang['page_about_ppf_section5_topic4_desc'] = 'Syncshield\'s urethane film was developed focusing on the suppleness to follow the sophisticated tertiary curved surface of a car body. This allowed for application without excessive pulling or additional heat and realized higher durability and anti-chipping performance.';
$lang['page_about_ppf_section5_topic5_title'] = 'NON-YELLOWING';
$lang['page_about_ppf_section5_topic5_desc'] = 'TPU used for Syncshield is a non-yellowing type material with one of the highest quality.';
$lang['page_about_ppf_section5_topic6_title'] = 'ADHESIVE';
$lang['page_about_ppf_section5_topic6_desc'] = 'The adhesive used for Syncshield was selected from 200 different types and adjusted to match a flexible urethane film. The adhesive is made to reduce the adhesion when applying, but keep strong adhesion after application to prevent from peeling off, which is one of the many unique characteristics of Syncshiled.';
$lang['page_about_ppf_section6_title'] = 'CAUTION!';
$lang['page_about_ppf_section6_desc'] = '<li> - Use dedicated mounting liquid upon application of Syncshield.</li>
          <li> - Please use with SENSHA Brand products upon maintenance, daily car care and car wash.</li>
          <li> * We cannot assure quality when used with other Brand products and will not be part of the warranty.</li>';

//Sole Agent
$lang['page_sole_agent_meta_title'] = 'Sole Agent Wanted ｜SENSHA WORLD.com';
$lang['page_sole_agent_meta_desc'] = 'We also welcome you to be a distributor to join our global "SENSHA Family" network connecting 35 countries.';
$lang['breadcrumb_sole_agent'] = 'Sole Agent Wanted';
$lang['page_sole_agent_key_visual_title'] = 'Become a distributor in your country';
$lang['page_sole_agent_key_visual_desc'] = 'We also welcome you to be a distributor to join our global "SENSHA Family" network connecting 35 countries.';
$lang['page_sole_agent_section1_title'] = 'Become a distributor in your country';
$lang['page_sole_agent_section1_desc'] = 'The network of more than 800 car wash and coating shops in 35 countries globally with "SENSHA Family" is the great proof of trust and achievements of our business. SENSHA was founded in 1997, and the company has been growing to be one of the world leading car wash and coating brand and has more than 800 shops in 35 countries. We do not only value our products, as the success of a detailing shop cannot be materialized by products alone. Our goal is the success of every "SENSHA Family" so that we provide everything our "SENSHA Family" required for a successful detailing shop.';
$lang['page_sole_agent_section2_title'] = 'What we can do?';
$lang['page_sole_agent_section2_item1_title'] = 'Start a business at low cost';
$lang['page_sole_agent_section2_item1_desc'] = 'At SENSHA, there is no membership fee or monthly franchise fee. You can start as a SENSHA brand if you have the necessary technical trainings. Our goal is the same as our partners globally, the success of the business.<br /><br />
・ Brand mark usage<br />
・ No membership fee<br />
・ No monthly franchise fee';
$lang['page_sole_agent_section2_item2_title'] = 'Procurement support';
$lang['page_sole_agent_section2_item2_desc'] = 'All products supplied from SENSHA are developed in-house and produced in Japan to distribute to our partners around the world. There could be some bottlenecks of procuring products such as order lot size, purchase price and delivery date. We always work with our partners to reduce and improve these issues of procurement.<br /><br />
・ Purchasing orders made easily through a dedicated system<br />
・ Discounted prices<br />
・ No minimum order quantity<br />
・ Accept multiple payment methods<br />
・ Clear shipping schedule<br />
・ Transport cost quotation
';
$lang['page_sole_agent_section2_item3_title'] = 'Provision of promotional materials';
$lang['page_sole_agent_section2_item3_desc'] = 'By becoming our partner, our images and videos uploaded to SNS by SENSHA Family in 33 countries can be used freely. This helps reduce promotional costs when stating a business.<br /><br />
・ Use of images and videos from SENSHA Family<br />
・ Promotional strategy advice';
$lang['page_sole_agent_section2_item4_title'] = 'Benefit Title';
$lang['page_sole_agent_section2_item4_desc'] = 'The racetrack is where we live. It’s where we prove our commitment to making everbetter cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity';
$lang['page_sole_agent_section2_item5_title'] = 'Benefit Title';
$lang['page_sole_agent_section2_item5_desc'] = 'The racetrack is where we live. It’s where we prove our commitment to making everbetter cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity';
$lang['page_sole_agent_section2_item6_title'] = 'Benefit Title';
$lang['page_sole_agent_section2_item6_desc'] = 'The racetrack is where we live. It’s where we prove our commitment to making everbetter cars. That’s why we’re proud to announce our latest creation for the racetrack: the 2019 Supra Xfinity';
$lang['page_sole_agent_section3_title'] = 'How to become a distributor';
$lang['page_sole_agent_section3_step1_title'] = 'STEP 01';
$lang['page_sole_agent_section3_step1_subtitle'] = 'Contact us via inquiry form';
$lang['page_sole_agent_section3_step1_desc'] = 'Please contact us by entering required information via inquiry form. We support Japanese, English, Chinese, Thai and Indonesian languages.';
$lang['page_sole_agent_section3_step2_title'] = 'STEP 02';
$lang['page_sole_agent_section3_step2_subtitle'] = 'Provision of sample products';
$lang['page_sole_agent_section3_step2_desc'] = 'Let us know your request first. Then we will deliver our sample products so that you will know the quality our produts';
$lang['page_sole_agent_section3_step3_title'] = 'STEP 03';
$lang['page_sole_agent_section3_step3_subtitle'] = 'Discussion';
$lang['page_sole_agent_section3_step3_desc'] = 'Building trust among us our partners and contractors is significantly important. Please do ask any questions or concerns to build a better relationship.';
$lang['page_sole_agent_section3_step4_title'] = 'STEP 04';
$lang['page_sole_agent_section3_step4_subtitle'] = 'Conclusion of a contract';
$lang['page_sole_agent_section3_step4_desc'] = 'We will send you a contract. This contract is an important document to protect your rights. Please make sure to check the contract before signing and sending back to us. The contract will be completed by us signing on and returning it.';
$lang['page_sole_agent_section4_title'] = 'Distributor inquiry';
$lang['page_sole_agent_section4_subtitle'] = 'Please enter required information and click the send button. ';

$lang['breadcrumb_sole_agent_confirm'] = 'Inquiry Confirmation';
$lang['breadcrumb_sole_agent_complete'] = 'Inquiry sent successfully';
$lang['page_sole_agent_complete_your_message'] = 'Your inquiry has been successfully sent to SENSHA.';
$lang['page_sole_agent_complete_thank_you'] = '<p>Your inquiry has been sent successfully. We will get back to you in the few days from our staff.</p>';

//Global Network
$lang['page_global_meta_title'] = 'Global network ｜SENSHA WORLD.com';
$lang['page_global_meta_desc'] = 'We work with our distributors, "SENSHA Family", at each country to provide the high quality products and best service worldwide.';
$lang['breadcrumb_global'] = 'World network';
$lang['page_global_section1_title'] = 'Global network';
$lang['page_global_section1_desc'] = 'We work with our distributors, "SENSHA Family", at each country to provide the high quality products and best service worldwide.';
$lang['page_global_address'] = 'OFFICE ADDRESS';
$lang['page_global_phone'] = 'PHONE';
$lang['page_global_see_google'] = 'See google map';

//Q&A
$lang['page_qa_meta_title'] = 'Q&A｜SENSHA WORLD.com';
$lang['page_qa_meta_desc'] = '';
$lang['breadcrumb_qa'] = 'Q & A';
$lang['page_qa_title'] = 'Q&A: Support for frequent questions';
$lang['page_qa_desc'] = 'We offer high quality products and reliable services<br>At SENSHA, we believe that the superior we do our jobs, the superior you are satisfied.';
$lang['page_qa_sensha'] = 'SENSHA';
$lang['page_qa_qa1'] = 'Pay with your amazon account';
$lang['page_qa_qa2'] = 'Products';
$lang['page_qa_qa3'] = 'Pay with your amazon account';
$lang['page_qa_qa4'] = 'Order / Payment';
$lang['page_qa_qa5'] = 'Pay with your amazon account';
$lang['page_qa_qa6'] = 'Delivery';
$lang['page_qa_qa7'] = 'Pay with your amazon account';
$lang['page_qa_qa8'] = 'Return product';
$lang['page_qa_qa9'] = 'Pay with your amazon account';
$lang['page_qa_qa10'] = 'Discount';
$lang['page_qa_qa11'] = 'Pay with your amazon account';
$lang['page_qa_qa12'] = 'Account';
$lang['page_qa_qa13'] = 'Pay with your amazon account';
$lang['page_qa_qa14'] = 'The others';
$lang['page_qa_qa15'] = 'Pay with your amazon account';
$lang['page_qa_qa16'] = 'Frequent question';

//Q&A Category
$lang['page_qa_cat_meta_title'] = 'Q&A｜SENSHA WORLD.com';
$lang['page_qa_cat_meta_desc'] = '';
$lang['breadcrumb_qa_cat'] = '';
$lang['page_qa_cat_title1'] = 'Question aout';
$lang['page_qa_cat_title2'] = '';
$lang['page_qa_cat_desc1'] = 'For the frequent question regarding';
$lang['page_qa_cat_desc2'] = 'are followed in the below.';
$lang['page_qa_cat_qa_cat'] = 'Q&A category';
$lang['page_qa_cat_see_more'] = 'See more <span>>></span>';

//Terms of use
$lang['page_terms_meta_title'] = 'Terms of use ｜SENSHA WORLD.com';
$lang['breadcrumb_terms'] = 'Terms of use';
$lang['page_terms_title'] = 'Terms of use';
$lang['page_terms_subtitle'] = '<h3 style="font-weight:bold;margin-bottom:20px;">OVERVIEW</h3>';
$lang['page_terms_desc'] = '<p>This website is operated by SENSHA Co., Ltd. Throughout the site, the terms “we”, “us”, “our” and ""SENSHA"" refer to SENSHA Co., Ltd. SENSHA Co., Ltd offers this website, including all information, tools and services available from this site to you, the user, conditioned upon your acceptance of all terms, conditions, policies and notices stated here.<br /><br />
The below is the corporate details:<br /><br />
Company Name: SENSHA Co., Ltd<br />
Corporate Registration Number: 021001024148<br />
Address: 1007-3, Kami-kasuya, Isehara-shi, Kanagawa 259-1141 Japan<br /><br />
SENSHA Co., Ltd that was established and is being run under the Japanese Law.<br /><br />
By visiting our site and/ or purchasing something from us, you engage in our “Service” and/ or ""Services"" as well as ""Product, and/ or ""Products"", and agree to be bound by the following terms and conditions (""Terms and Conditions"", “Terms of Service”, “Terms”), including those additional terms and conditions and policies referenced herein and/or available by hyperlink. These Terms of Service apply to all users of the site, including without limitation users who are browsers, vendors, customers, merchants, and/ or contributors of content.<br /><br />
Please read these Terms of Service carefully before accessing or using our website. By accessing or using any part of the site, you agree to be bound by these Terms of Service. If you do not agree to all the terms and conditions of this agreement, then you may not access the website or use any services. If these Terms of Service are considered an offer, acceptance is expressly limited to these Terms of Service.<br /><br />
These translations are provided for the convenience to all users of the sites, and the original Japanese terms and conditions take over any differences and utilized in priority.</p>';

//Privacy
$lang['page_privacy_meta_title'] = 'Privacy Policy｜SENSHA WORLD.com';
$lang['breadcrumb_privacy'] = 'Privacy Policy';
$lang['page_privacy_title'] = 'Privacy Policy';
$lang['page_privacy_subtitle'] = '<p>This website is operated by SENSHA Co., Ltd. Throughout the site, the terms “we”, “us”, “our” and ""SENSHA"" refer to SENSHA Co., Ltd.  When you purchase or use something from our store, we collect the personal information you give us such as your name, address and email address. We have following policies in handling personal information. Here our Privacy Policy is explained.</p>';
$lang['page_privacy_desc'] = '
<p><em>SECTION 1 - COMPLIANCE WITH LAWS AND REGULATIONS</em>
We comply with the laws including the Japanese law "Act of the Protection of Personal Information", guidelines and this Privacy Policy when handling personal information.</p>
<p><em>SECTION 2 - COLLECTION OF PERSONAL INFORMATION</em>
We may collect your personal information. The range of personal information we collect from users does not exceed the necessary to achieve the purpose of utilization.</p>
<p><em>SECTION 3 - MANAGEMENT AND PROTECTION OF PERSONAL INFORMATION</em>
・When we handle personal information, there is a manager in charge to appropriately manage personal information and endeavour to prevent personal information from flowing externally. Also, an appropriate and reasonable level of precautions and safety measures are taken to protect personal information from a risk of unauthorized access, loss, destruction, falsification and so on.<br />
・Personal information is strictly managed and not used illegally even inside of the company by limiting the persons who have the access to the personal information database.<br />
・We may outsource the handling of personal information. In this circumstance, we select a group company or an outsourcer handling personal information properly (hereinafter, referred as contractor), and let a contractor manage personal information, maintain confidentiality and prohibit a re-provision of personal information under an outsourcing contract. We make a contractor carry out appropriate management with predetermined requirements.</p>
<p><em>SECTION 4 - USE OF PERSONAL INFORMATION</em>
We use personal information collected only for purposes specified in advance at the time of collection or for purposes of use announced, except when the consent of using personal information is obtained from the owner and also when it is permitted by law to handle personal information as an exception.</p>
<p><em>SECTION 5 - PROVISION OF PERSONAL INFORMATION</em>
We do not disclose or provide personal information to a third party company without the consent of the individual. Nevertheless, if we are required or asked to disclose personal information by law, public institutions such as court or police, we may disclose or provide personal information without the consent of the individual. We may also disclose users personal information if the user violates our terms and conditions.</p>
<p><em>SECTION 6 - COMPANY STRUCTURE</em>
Based upon this Privacy Policy, we established the internal rules for the protection of personal information, and set and indicated the clear policy for handling personal information to our all employees in order to protect personal information.</p>
<p><em>SECTION 7 - CHANGE TO THIS PROVACY POLICY</em>
We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it. </p>
<p><em>SECTION 8 - QUESTIONS AND CONTACT INFORMATION</em>
If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact via the inquiry form.</p>
<span>Last updated on May 1, 2019</span>';



//Contact
$lang['page_contact_meta_title'] = 'Contact Us｜SENSHA WORLD.com';
$lang['breadcrumb_contact'] = 'Contact Us';
$lang['page_contact_title'] = 'Contact Us';
$lang['page_contact_subtitle'] = 'Please feel free to contact us. In case that you have a problem regarding item delivery, please report to us via Shopping History in your dashboard.';

//Contact Mail
$lang['page_mail_title'] = 'We have received your inquiry  [SENSHA]';
$lang['page_mail_P01'] = '
<br>
    <br>
    Hi<br>
    This is SENSHA customer service.<br>
    <br>
    <br>
    We have received your inquiry as following.<br>
    <br>
    ============================<br>
    Inquiry Details:';
$lang['page_mail_P02'] = '
    ============================<br>
    <br>
    ▼Attentions (Please read)▼<br>
    <br>
    <br>
    We will check your inquiry and come back to you shortly. Thank you for your patient.<br>
    <br>
    This email and any attachments are intended for the addressee(s) only and may be confidential. You should not read, copy, use or disclose the content without authorization. If you have received this email in error, please notify the sender as soon as possible, delete the email and destroy any copies. This notice should not be removed.<br>
    <br>
    <br>
    Thank you for your continuous supports and businesses with us.<br>
    <br>
    <br>
    ▼ SENSHA NEWS ▼<br>
   Latest News<br>
    http://sensha.enfete.co/jp/page/news<br>
    <br>
    ======================<br>
    We believe that people clean their beloved cars leading to the safe driving society.<br>
    ======================<br>
    <br>
    Clean Your CAR<br>
SENSHA CO., LTD.<br>
1007-3, Kami-kasuya, Isehara-shi, Kanagawa 259-1141 Japan<br>
    (Phone) 0463-94-5106<br>
    (Fax) 0463-94-5108<br>
    (E-Mail) info@cleanyourcar.jp<br>
    (URL) http://www.cleanyourcar.jp<br>
    (World site) http://sensha-world.com/<br>';
    
//News
$lang['page_news_meta_title'] = 'News｜SENSHA WORLD.com';
$lang['page_news_meta_desc'] = '';
$lang['breadcrumb_news'] = 'News';
$lang['page_news_title'] = 'News';
$lang['page_news_subtitle'] = '';
$lang['page_news_see_more'] = 'See more';

//Register
$lang['page_register_meta_title'] = 'Registration form｜SENSHA WORLD.com';
$lang['breadcrumb_register'] = 'Registration form';
$lang['page_register_title'] = 'Registration form';
$lang['page_register_subtitle'] = 'Please choose the authentication method to prove you are not robot. <br>
And fill the below registration form to proceed to shopping with SENSHA';
$lang['page_register_confirmation'] = 'Confirmation';
$lang['page_register_via_email'] = 'Authenticate via email';
$lang['page_register_via_sms'] = 'Authenticate via SMS';
$lang['page_register_email'] = 'Email';
$lang['page_register_input_email'] = 'Please input your Email';
$lang['page_register_tel_code'] = 'Telephone Code';
$lang['page_register_tel'] = 'Telephone';
$lang['page_register_input_tel'] = 'Please input your Telephone';
$lang['page_register_password'] = 'Password';
$lang['page_register_input_password'] = 'Please input your Password';
$lang['page_register_country'] = 'Country';
$lang['page_register_country_select'] = 'Country select';
$lang['page_register_save'] = 'Save';
$lang['page_register_select'] = 'Select your XXXXX';
$lang['mail_register_content'] = 'Thank you for visiting the official website of SENSHA.<br>Click the URL below to complete the registration.';

//Register Confirmation
$lang['page_register_confirm_meta_title'] = 'Registration confirmation｜SENSHA WORLD.com';
$lang['breadcrumb_register_confirm'] = 'Registration confirmation';
$lang['page_register_confirm_title'] = 'Registration confirmation';
$lang['page_register_confirm_subtitle'] = 'If the above information is correct, please click the confirm button in the below. Then, we will send the email or SMS to your registered one to activate your account.';
$lang['page_register_confirm_authenticate'] = 'Authenticate';
$lang['page_register_confirm_authenticate_via'] = 'Authenticate via ';
$lang['page_register_confirm_title'] = 'Registration confirmation';

$lang['page_register_confirm_confirm'] = 'Confirm';

//Register Success
$lang['page_register_success_meta_title'] = 'Authentication email sent｜SENSHA WORLD.com';
$lang['breadcrumb_register_success'] = 'Authentication email sent';
$lang['page_register_success_title'] = 'Authentication email sent';
$lang['page_register_success_message1'] = 'The email for authentication was successfully sent';
$lang['page_register_success_message2'] = 'Please check your email / SMS and click the link to authenticate your account.';
$lang['page_register_success_message3'] = '';

//Register complete
$lang['page_register_complete_meta_title'] = 'Registration completed｜SENSHA WORLD.com';
$lang['breadcrumb_register_complete'] = 'Registration completed';
$lang['page_register_complete_title'] = 'Registration completed';
$lang['page_register_complete_message1'] = 'Your registration was successfully completed';
$lang['page_register_complete_message2'] = 'Your registration was successfully done. To proceed to shopping, please fill additional information such as your personal in formation, delivery address and so on.';
$lang['page_register_complete_message3'] = 'Edit personal info';

//Register OTP
$lang['page_register_otp_meta_title'] = 'Registration completed｜SENSHA WORLD.com';
$lang['breadcrumb_register_otp'] = 'Registration completed';
$lang['page_register_otp_title'] = 'Registration completed';
$lang['page_register_otp_message1'] = 'Please confirm the subscription by entering the OTP number from your phone number.';
$lang['page_register_otp_message2'] = 'OTP';
$lang['page_register_otp_message3'] = 'Please enter your OTP number';
$lang['page_register_otp_send_again'] = 'Send again';
$lang['page_register_otp_confirm'] = 'Confirm';

//Forgot Password
$lang['page_forgot_pass_meta_title'] = 'Password reset｜SENSHA WORLD.com';
$lang['breadcrumb_forgot_pass'] = 'Password reset';
$lang['page_forgot_pass_title'] = 'Password reset form';
$lang['page_forgot_pass_message1'] = 'Forget your password? Please fill the below form, so that we will shortly send the reset link to your registered email or SMS number.';
$lang['page_forgot_pass_message2'] = 'Save';
$lang['page_forgot_pass_message3'] = '';

//New Password
$lang['page_new_pass_meta_title'] = 'Password change｜SENSHA WORLD.com';
$lang['breadcrumb_new_pass'] = 'Password change';
$lang['page_new_pass_title'] = 'Set your new password';
$lang['page_new_pass_message1'] = 'Your password was successfully reset. Please set your new password in the below form.';
$lang['page_new_pass_message2'] = 'Confirm';

//Password Complete
$lang['page_pass_complete_meta_title'] = 'Password completed｜SENSHA WORLD.com';
$lang['breadcrumb_pass_complete'] = 'Password completed';
$lang['page_pass_complete_title'] = 'Password change completed';
$lang['page_pass_complete_message1'] = 'Your password was successfully changed';
$lang['page_pass_complete_message2'] = 'For login, please click the below button to login for further shopping.';
$lang['page_pass_complete_message3'] = 'Login';

//Password OTP
$lang['page_pass_otp_meta_title'] = 'Registration completed｜SENSHA WORLD.com';
$lang['breadcrumb_pass_otp'] = 'Registration completed';
$lang['page_pass_otp_title'] = 'Registration completed';
$lang['page_pass_otp_message1'] = 'Please confirm the subscription by entering the OTP number from your phone number.';
$lang['page_pass_otp_message2'] = 'OTP';
$lang['page_pass_otp_message3'] = 'Please enter your OTP number';

//PDF
$lang['pdf_title'] = 'INVOICE';
$lang['pdf_description'] = 'Contribute to "safe drive and traffic safety "over the world through car care business. It\'s our Dream.';
$lang['pdf_title'] = 'INVOICE';
$lang['pdf_invoice_no'] = 'INVOICE NO. : ';
$lang['pdf_date'] = 'Date';
$lang['pdf_company_name'] = 'SENSHA Co., Ltd.';
$lang['pdf_company_address'] = '1007-3 Kamikasuya, Isehara, Kanagawa 259-1141';
$lang['pdf_company_tel'] = 'Phone 81(463) 94-5106  Fax 81(463) 94-5108';
$lang['pdf_company_mail'] = 'Mail Address: info@cleanyourcar.jp';
$lang['pdf_shipment_terms'] = 'Shipment Terms: ';
$lang['pdf_to'] = 'To: ';
$lang['pdf_customer_name'] = 'Company Name: ';
$lang['pdf_customer_address'] = 'Address: ';
$lang['pdf_customer_zip'] = 'ZIP Code(Postal Code): ';
$lang['pdf_customer_tel'] = 'Phone: ';
$lang['pdf_products_ordered'] = 'Products ordered';
$lang['pdf_qty'] = 'Qty';
$lang['pdf_unit_price'] = 'Unit price (JPY)';
$lang['pdf_amount'] = 'AMOUNT (JPY)';
$lang['pdf_shipping_fee'] = 'SHIPPING FEE';
$lang['pdf_discount'] = 'DISCOUNT';
$lang['pdf_sub_total'] = 'SUB TOTAL';
$lang['pdf_admin_fee'] = 'ADMIN FEE';
$lang['pdf_tax'] = 'TAX';
$lang['pdf_grand_total'] = 'GRAND TOTAL';
$lang['pdf_payment_options'] = 'Payment Options';

$lang['pdf_sensha_bank'] = 'Bank: RESONA BANK, LTD.';
$lang['pdf_sensha_account'] = 'Account Number: 1299037';
$lang['pdf_sensha_swift'] = 'Swift Code: DIWAJPJT';
$lang['pdf_sensha_account_holder'] = 'Account Holder: SENSHA Co., Ltd.';
$lang['pdf_sensha_branch'] = 'Isehara Branch : 646';

$lang['pdf_sensha_bank_oversea'] = 'JAPAN RESONA BANK : 0010';
$lang['pdf_sensha_account_oversea'] = 'Account Number: 1299037';
$lang['pdf_sensha_swift_oversea'] = 'Swift Code: DIWAJPJT';
$lang['pdf_sensha_account_holder_oversea'] = 'Account Name : SENSHA Co., Ltd.';
$lang['pdf_sensha_branch_oversea'] = 'Isehara Branch : 646';

$lang['pdf_sensha_address'] = 'Address of the BANK : 1-3-6 Isehara, Isehara-city, Kanagawa, Japan';
$lang['pdf_sensha_tel'] = 'Phone Number : 81(463)92-1511';

$lang['pdf_sensha_branch_bank'] = 'Zengin Bank Code: 0010';
$lang['pdf_sensha_branch_tel'] = 'Telephone: ＋81-(0)463-92-1511';
$lang['pdf_sensha_branch_bank_code'] = 'Zengin Branch Code: 646';
$lang['pdf_thanks'] = 'THANK YOU FOR YOUR BUSINESS!';
$lang['pdf_sensha_pay_on_delivery'] = 'Pay on Delivery';

$lang['pdf_receipt_title'] = 'CASH RECEIPT';
$lang['pdf_receipt_invoice_no'] = 'Invoice No. :';
$lang['pdf_receipt_company_name'] = 'Company name :';
$lang['pdf_receipt_transaction_no'] = 'Amount Paid for Transaction No. ';
$lang['pdf_receipt_received'] = 'Received & Confirmed';
$lang['pdf_receipt_date_issued'] = 'Date Issued ';
$lang['pdf_receipt_address1'] = 'SENSHA Co.,Ltd.';
$lang['pdf_receipt_address2'] = '1007-3 Kamikasuya, Isehara, ';
$lang['pdf_receipt_address3'] = 'Kanagawa 259-1141';
$lang['pdf_receipt_tel'] = 'Tel.: +81(463) 94-5106';
$lang['pdf_receipt_fax'] = 'Fax: +81(463) 94-5108';
$lang['pdf_receipt_payment_received'] = 'Payment Received in:';
$lang['pdf_receipt_approved'] = 'Approved by:';

//SMS shopping
$lang['shop_sms_greeting'] = 'We have received your order';
$lang['shop_sms_domestic'] = 'Domestic Order [';
$lang['shop_sms_international'] = 'International Order [';

?>
