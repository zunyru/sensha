<?php
//Global
$lang['global_home'] = 'ダッシュボード';
$lang['global_filter'] = 'フィルター';
$lang['global_search'] = '検索';
$lang['global_logout'] = 'ログアウト';
$lang['global_import'] = 'インポート';
$lang['global_export'] = 'エクスポート';
$lang['global_tuhan_gate'] = '通販ゲートCSV';
$lang['global_submit'] = '確定';
$lang['global_add'] = '追加';
$lang['global_edit'] = '編集';
$lang['global_delete'] = '削除';

//Admin Dashboard
$lang['admin_dashboard_title'] = 'ダッシュボード';
$lang['admin_dashboard_table_title'] = '月間の取引概要';
$lang['admin_dashboard_section1_title'] = '国際発送取引合計（HQ）';
$lang['admin_dashboard_sectin2_title'] = '国内発送取引合計（SA)';
$lang['admin_dashboard_table_basic_info'] = '売上関連情報';
$lang['admin_dashboard_table_product'] = '商品情報';
$lang['admin_dashboard_table_income'] = '調整';
$lang['admin_dashboard_table_term'] = '期間';
$lang['admin_dashboard_table_total'] = '売上合計額';
$lang['admin_dashboard_table_product_sales'] = '商品合計';
$lang['admin_dashboard_table_discount_amount'] = '割引合計';
$lang['admin_dashboard_table_delivery_fee'] = '発送料合計';
$lang['admin_dashboard_table_no_of_item'] = '商品販売個数';
$lang['admin_dashboard_table_average'] = '平均販売商品単価';
$lang['admin_dashboard_table_income2'] = '売上合計';
$lang['admin_dashboard_table_pay_to_agent'] = '代理店分配';
$lang['admin_dashboard_table_expected_income'] = '粗利益';
$lang['admin_dashboard_section3_title'] = 'クレジット取引合計';
$lang['admin_dashboard_table_credit_info'] = 'クレジット取引情報';
$lang['admin_dashboard_table_term2'] = '期間';
$lang['admin_dashboard_table_total_payment'] = 'クレジット取引合計';
$lang['admin_dashboard_table_release'] = 'HQからの送金処理';
$lang['admin_dashboard_table_buy_products'] = 'SAによる商品購入';
$lang['admin_dashboard_section4_title'] = '総合計';
$lang['admin_dashboard_table_grand_total'] = '総合計';

//Admin Financial Manager
$lang['admin_financial_title'] = '財務管理';
$lang['admin_financial_pay_credit'] = 'クレジットの解放処理';
$lang['admin_financial_country'] = '国';
$lang['admin_financial_credit'] = 'クレジット';
$lang['admin_financial_amount_to_release'] = 'クレジットの解放';
$lang['admin_financial_submit'] = '支払い処理';
$lang['admin_financial_tab1'] = '売り上げ履歴';
$lang['admin_financial_tab1_basic_info'] = '基本情報';
$lang['admin_financial_tab1_shipment_id'] = '取引 ID';
$lang['admin_financial_tab1_date_of_payment'] = '支払い日';
$lang['admin_financial_tab1_saller_info'] = '販売者';
$lang['admin_financial_tab1_adjusment'] = '調整';
$lang['admin_financial_tab1_country'] = '国';
$lang['admin_financial_tab1_agent_info'] = '関連エージェント';
$lang['admin_financial_tab1_sa_name'] = '関連SA';
$lang['admin_financial_tab1_sa_discount_ratio'] = 'SA 手数料比率';
$lang['admin_financial_tab1_aa_name'] = '関連AA';
$lang['admin_financial_tab1_aa_discount_ratio'] = 'AA 手数料比率';
$lang['admin_financial_tab1_purchase_info'] = '購入者';
$lang['admin_financial_tab1_country2'] = '国';
$lang['admin_financial_tab1_user_type'] = 'タイプ';
$lang['admin_financial_tab1_user_id'] = 'ID';
$lang['admin_financial_tab1_discount_ratio'] = '割引率';
$lang['admin_financial_tab1_sales_info'] = '売り上げ関連情報';
$lang['admin_financial_tab1_no_of_item'] = '商品販売数';
$lang['admin_financial_tab1_sales_amount'] = '商品価格';
$lang['admin_financial_tab1_shipping_fee'] = '発送料';
$lang['admin_financial_tab1_discount_total'] = '割引額';
$lang['admin_financial_tab1_sales_total'] = '売上';
$lang['admin_financial_tab1_pay_to_agent'] = '代理店分配';
$lang['admin_financial_tab1_expected_income'] = '粗利益';
$lang['admin_financial_tab2'] = 'クレジット取引履歴';
$lang['admin_financial_tab2_basic_info'] = '基本情報';
$lang['admin_financial_tab2_date_of_payment'] = '取引日';
$lang['admin_financial_tab2_pay_type'] = '取引タイプ';
$lang['admin_financial_tab2_shipment_id'] = '取引 ID';
$lang['admin_financial_tab2_payer_info'] = '振込元情報';
$lang['admin_financial_tab2_country'] = '国';
$lang['admin_financial_tab2_account_type'] = 'タイプ';
$lang['admin_financial_tab2_detailed_info'] = '取引詳細';
$lang['admin_financial_tab2_sa_country'] = '振込先';
$lang['admin_financial_tab2_sa_name'] = '振込先 SA 名';
$lang['admin_financial_tab2_credit_before_process'] = '取引前残高';
$lang['admin_financial_tab2_description'] = '取引内容';
$lang['admin_financial_tab2_amount_to_pay'] = '振込額';
$lang['admin_financial_tab2_credit_after_process'] = '取引後残高';
$lang['admin_financial_tab2_current_credit'] = '現在のクレジット残高';

//Nation&lang
$lang['admin_nation_lang_title'] = '国と言語の設定';
$lang['admin_nation_lang_country'] = '国';
$lang['admin_nation_lang_language'] = '言語';
$lang['admin_nation_lang_country_flag'] = '国旗';
$lang['admin_nation_lang_font'] = 'フォント';
$lang['admin_nation_lang_reading'] = 'パラグラフ並び';
$lang['admin_nation_lang_currency_name'] = '通貨名称';
$lang['admin_nation_lang_currency_rate'] = '為替レート（対基軸通貨）';

//Account Settings
$lang['admin_account_setting_title'] = 'アカウント設定';
$lang['admin_account_setting_id'] = 'ユーザーID';
$lang['admin_account_setting_email'] = '登録連絡先';
$lang['admin_account_setting_password'] = 'パスワード';
$lang['admin_account_setting_user_id'] = 'ID';
$lang['admin_account_setting_country'] = '国';
$lang['admin_account_setting_account_type'] = 'タイプ';
$lang['admin_account_setting_company_name'] = '会社名';
$lang['admin_account_setting_company_phone'] = '電話番号';
$lang['admin_account_setting_company_address'] = '会社住所';
$lang['admin_account_setting_tax_id'] = '納税者番号';
$lang['admin_account_setting_contact_person'] = '担当者名';
$lang['admin_account_setting_shipment_name'] = '配送先名';
$lang['admin_account_setting_shipment_phone'] = '配送先電話番号';
$lang['admin_account_setting_zipcode'] = '郵便番号';
$lang['admin_account_setting_shipment_address'] = '配送先住所';
$lang['admin_account_setting_discount_setting'] = '割引率';
$lang['admin_account_setting_comission_setting'] = '手数料率（AA）';
$lang['admin_account_setting_exclude_group'] = '除外グループ設定';

//Salese History
$lang['admin_sales_history_title'] = '販売履歴';
$lang['admin_sales_history_account_type'] = 'タイプ';
$lang['admin_sales_history_company_name'] = '会社名';
$lang['admin_sales_history_item_id'] = '商品ID';
$lang['admin_sales_history_item_name'] = '商品名';
$lang['admin_sales_history_item_amount'] = '数量';
$lang['admin_sales_history_sales_date'] = '購入日';
$lang['admin_sales_history_sales_total_amount'] = '小計';
$lang['admin_sales_history_payment_method'] = '支払い方法';
$lang['admin_sales_history_payment_status'] = '購入ステータス';
$lang['admin_sales_history_shipping_status'] = 'ステータス変更';
$lang['admin_sales_history_create_sales_hitory'] = '購入履歴追加';
$lang['admin_sales_history_account_name'] = 'アカウント名';
$lang['admin_sales_history_sale_item_category'] = '商品カテゴリ';

//Product Category
$lang['admin_product_cat_title'] = '商品カテゴリ設定';
$lang['admin_product_cat_1st_cluster'] = '第一階層';
$lang['admin_product_cat_2nd_cluster'] = '第二階層';
$lang['admin_product_cat_3rd_cluster'] = '第三階層';
$lang['admin_product_cat_add_1st_cluster'] = '第一階層を追加';
$lang['admin_product_cat_add_2nd_cluster'] = '第二階層を追加';
$lang['admin_product_cat_add_3rd_cluster'] = '第三階層を追加';
$lang['admin_product_cat_create_1st_cluster'] = '第一階層を追加';
$lang['admin_product_cat_create_2nd_cluster'] = '第二階層を追加';
$lang['admin_product_cat_create_3rd_cluster'] = '第三階層を追加';
$lang['admin_product_cat_edit_1st_cluster'] = '第一階層を編集';
$lang['admin_product_cat_edit_2nd_cluster'] = '第二階層を編集';
$lang['admin_product_cat_edit_3rd_cluster'] = '第三階層を編集';
$lang['admin_product_cat_general'] = '一般商品';
$lang['admin_product_cat_ppf'] = 'PPF';
$lang['admin_product_cat_edit_product_cat'] = '商品カテゴリを編集する';
$lang['admin_product_cat_name'] = 'クラスタ名';
$lang['admin_product_cat_country'] = '国';
$lang['admin_product_cat_type'] = '商品カテゴリ';
$lang['admin_product_cat_cluster_name'] = '階層名';
$lang['admin_product_cat_new_cluster_name'] = '新階層名';

//Product Management
$lang['admin_product_management_title'] = '商品管理';
$lang['admin_product_management_upload'] = '画像の一括アップロード';
$lang['admin_product_management_section1_title'] = '一般商品';
$lang['admin_product_management_section2_title'] = 'カット済みフィルム';
$lang['admin_product_management_add_product_management'] = '商品の追加';
$lang['admin_product_management_edit_product_management'] = '商品の編集';
$lang['admin_product_management_product_id'] = '商品ID';
$lang['admin_product_management_product_name'] = '商品名';
$lang['admin_product_management_cat_1'] = '第一階層';
$lang['admin_product_management_cat_2'] = '第二階層';
$lang['admin_product_management_cat_3'] = '第三階層';
$lang['admin_product_management_product_weight'] = '重量';
$lang['admin_product_management_global_price'] = '国際価格';
$lang['admin_product_management_domestic_price'] = '国内価格';
$lang['admin_product_management_active'] = '状況';
$lang['admin_product_management_sa_price'] = '国内価格（SA）';
$lang['admin_product_management_import_shipping'] = '輸入送料（SA）';
$lang['admin_product_management_import_tax'] = '関税（SA）';
$lang['admin_product_management_no_of_use'] = '想定利用回数';
$lang['admin_product_management_cost_per_use'] = '一回あたりの利用コスト';
$lang['admin_product_management_fixed_delivery_price'] = '固定発送料設定';
$lang['admin_product_management_material'] = '材質';
$lang['admin_product_management_item_type'] = '商品タイプ';
$lang['admin_product_management_usage'] = '用途';
$lang['admin_product_management_parts'] = 'パーツ';
$lang['admin_product_management_feature'] = '表示';
$lang['admin_product_management_no_of_stock'] = '在庫数';
$lang['admin_product_management_item_description'] = '商品説明';
$lang['admin_product_management_content'] = 'コンテンツ';
$lang['admin_product_management_caution'] = '注意書き';
$lang['admin_product_management_html_content'] = 'HTMLコンテンツ';
$lang['admin_product_management_image'] = '画像';
$lang['admin_product_management_add_image'] = '画像のアップロード';
$lang['admin_product_management_youtube_videos'] = 'YouTube コード';
$lang['admin_product_management_unpurchasable_user'] = 'ユーザー利用制限';
$lang['admin_product_management_active'] = '販売状況';
$lang['admin_product_management_show_in_home_page'] = 'トップページに表示';
$lang['admin_product_management_car_production_id'] = '型式';
$lang['admin_product_management_begining_term_to_use'] = '';
$lang['admin_product_management_ending_term_to_use'] = '';
$lang['admin_product_management_create_product_mangement'] = '商品追加';
$lang['admin_product_management_edit_product_mangement'] = '商品編集';
$lang['admin_product_management_import_product_mangement'] = '商品インポート';
$lang['admin_product_management_choose_file'] = 'インポートするファイルを選択してください。';
$lang['admin_product_management_choose_image'] = '画像を選択してください。';

//Delivery
$lang['admin_delivery_title'] = '発送方法';
$lang['admin_delivery_country'] = '国';
$lang['admin_delivery_fixed_type'] = 'タイプ';
$lang['admin_delivery_fixed_amount'] = '発送料金';
$lang['admin_delivery_create_delivery'] = '発送追加';
$lang['admin_delivery_fixed'] = '固定';
$lang['admin_delivery_percent'] = 'パーセント';
$lang['admin_delivery_edit_delivery'] = '発送方法編集';

//Exclude Group
$lang['admin_exclude_title'] = '除外グループ設定';
$lang['admin_exclude_add_exclude_group'] = '除外グループの追加';
$lang['admin_exclude_create_exclude_group'] = '除外グループの追加';
$lang['admin_exclude_edit_exclude_group'] = '除外グループの編集';
$lang['admin_exclude_name'] = '除外グループ名';
$lang['admin_exclude_category'] = '除外対象商品カテゴリ';
$lang['admin_exclude_id'] = '除外対象商品 ID';

//Discount Coupon
$lang['admin_discount_coupon_title'] = '割引クーポン設定';
$lang['admin_discount_coupon_add_discount_coupon'] = '割引クーポンの追加';
$lang['admin_discount_coupon_create_discount_coupon'] = '割引クーポンの追加';
$lang['admin_discount_coupon_edit_discount_coupon'] = '割引クーポンの編集';
$lang['admin_discount_coupon_coupon_name'] = 'クーポン名称';
$lang['admin_discount_coupon_coupon_code'] = 'クーポンコード（半角英数字）';
$lang['admin_discount_coupon_discount_type'] = '割引タイプ';
$lang['admin_discount_coupon_discount'] = '割引額(％)';
$lang['admin_discount_coupon_start_date'] = '利用開始日';
$lang['admin_discount_coupon_end_date'] = '利用終了日';

//Webpage setting
$lang['admin_webpage_title'] = 'ウェブページ設定';
$lang['admin_webpage_home'] = 'トップページ';
$lang['admin_webpage_about'] = 'SENSHAについて';
$lang['admin_webpage_ppf'] = 'PPFについて';
$lang['admin_webpage_agent'] = '代理店募集';
$lang['admin_webpage_create_webpage_setting'] = 'ページ追加';
$lang['admin_webpage_edit_webpage_setting'] = 'ページ編集';
$lang['admin_webpage_meta_name'] = 'メタタイトル';
$lang['admin_webpage_meta_desc'] = 'ディスクリプション';
$lang['admin_webpage_content'] = 'コンテンツ';

//News Post
$lang['admin_post_title'] = 'ニュース投稿';
$lang['admin_post_post_title'] = 'タイトル';
$lang['admin_post_post_content'] = 'コンテンツ';
$lang['admin_post_create_date'] = '公開日';
$lang['admin_post_create_new_post'] = '新規投稿';
$lang['admin_post_edit_post'] = '編集';

//Q&A Post
$lang['admin_qa_title'] = 'Q&A投稿';
$lang['admin_qa_category'] = 'Q&Aカテゴリ';
$lang['admin_qa_post_title'] = 'タイトル';
$lang['admin_qa_post_content'] = 'コンテンツ';
$lang['admin_qa_create_date'] = '追加日';
$lang['admin_qa_create_qa_post'] = 'Q&A追加';
$lang['admin_qa_edit_qa_post'] = 'Q&A  編集';

//Global Network
$lang['admin_gn_title'] = 'グローバルネットワーク';
$lang['admin_gn_latitude'] = '緯度';
$lang['admin_gn_longitude'] = '経度';
$lang['admin_gn_create_global_network'] = 'ネットワークを追加する';
$lang['admin_gn_edit_global_network'] = 'ネットワークを編集する';


//Header
$lang['header_menu_sign_up'] = 'サインアップ';
$lang['header_menu_login'] = 'ログイン';
$lang['header_menu_logout'] = 'ログアウト';
$lang['header_menu_cart'] = 'カート';

//Global Navigation
$lang['navi_cut_film_title'] = 'PPF cut film';
$lang['navi_cut_film_title_search'] = 'Search';
$lang['navi_cut_film_easy_to'] = 'カット済みフィルムを探す';
$lang['navi_cut_film_select_maker'] = 'メーカー';
$lang['navi_cut_film_select_model'] = '車種';
$lang['navi_cut_film_select_part'] = 'パーツ';
$lang['navi_cut_film_btn_search'] = 'Search';

//Login Pop up
$lang['login_title'] = 'ログイン';
$lang['login_text1'] = 'ログイン情報';
$lang['login_text2'] = 'まだ会員登録がお済みでない方は、';
$lang['login_text3'] = 'こちらから新規会員登録をお願いします。';
$lang['login_text4'] = 'パスワードを忘れてしまった方は、';
$lang['login_text5'] = 'こちらから再設定をしてください。';
$lang['login_text6'] = '登録連絡先 (Email/Tel)';
$lang['login_text7'] = 'パスワード';

//Top Page
$lang['page_top_meta_title'] = 'SENSHA WORLD.com｜企業・ショッピングサイト';
$lang['page_top_meta_desc'] = '洗車の王国 ブランド (SENSHA brand)  公式サイトです。プロ仕様の洗車用洗剤・コーティング剤をご提供しています。当サイトでは、オンラインショッピング・製品情報・企業情報をご確認いただけます。';
$lang['breadcrumb_home'] = 'ホーム';
$lang['page_top_section1_title'] = 'Value the car, value the safety';
$lang['page_top_section1_subtitle'] = 'SENSHAは高品質な洗車用品を生産・販売しています。';
$lang['page_top_section1_banner1_title'] = 'About SENSHA';
$lang['page_top_section1_banner1_desc'] = '世界35カ国800店舗以上の洗車コーティングショップがある洗車の王国 ブランド (SENSHA brand)  は、世界各国で高品質な洗車用品を提供しております。';
$lang['page_top_section1_banner2_title'] = 'About PPF';
$lang['page_top_section1_banner2_desc'] = '私たちが日本のフィルム関連製造企業数社と共同で開発・製造するペイントプロテクションフィルム『Syncshield (シンクシールド)』 は、飛び石・擦り傷から自動車のボディを守る、洗車の王国 ブランド (SENSHA brand) の新たなフラッグシップ製品です。';
$lang['page_top_section2_title'] = '商品を検索する';
$lang['page_top_section2_subtitle'] = '様々な用途に合わせた洗剤・コーティング剤・ツール類をラインナップ。個人カーオーナー様からプロショップ様まで満足できる高品質なカーケア製品です。';
$lang['page_top_section3_title'] = 'カット済みフィルムを検索する';
$lang['page_top_section3_subtitle'] = '車種ごとにカットされたプロテクションフィルムをラインナップ。貼りやすさを実現しています。';
$lang['page_top_section4_title'] = 'SENSHA News';
$lang['page_top_section4_subtitle'] = '新製品・各国のニュースなどをリリースしています。';
$lang['page_top_section4_seeall'] = 'もっと見る';
$lang['page_top_section5_banner1_title'] = 'Global Network';
$lang['page_top_section5_banner1_desc'] = '私たちは、各国ディストリビューターである“SENSHA Family”と協力し、世界中に高品質な製品と最高のサービスを提供しています。';
$lang['page_top_section5_banner2_title'] = 'Sole Agent';
$lang['page_top_section5_banner2_desc'] = '"私たちが“SENSHA Family”と呼ぶ世界35カ国のネットワークに、代理店として活躍したいあなたのことをいつでも歓迎します。"';
$lang['page_top_section5_banner3_title'] = 'SENSHA Q&A';
$lang['page_top_section5_banner3_desc'] = 'SENSHAのサービスに関するよくある質問をご確認いただけます。';

//Mail Magazine
$lang['mail_mag_title'] = 'メールマガジン';
$lang['mail_mag_subtitle'] = 'メールマガジンの購読を希望される方は、こちらのフォームにメールアドレスを入力して送信ボタンを押してください。';
$lang['mail_mag_btn'] = '申し込む';

//Category Page
$lang['page_cat_meta_title'] = 'XXXXXの製品一覧｜SENSHA WORLD.com';
$lang['page_cat_meta_desc'] = 'XXXXXの関連商品一覧ページです。洗車の王国が提供する、洗車用洗剤やコーティング剤などの製品を購入できます。また、製品名や用途・目的から適した製品を選ぶこともできます。';
$lang['page_cat_title'] = 'の製品一覧';
$lang['page_cat_subtitle'] = 'の関連商品一覧ページです。';

//Search Page
$lang['page_keyword_search_meta_title'] = 'キーワード検索｜SENSHA WORLD.com';
$lang['page_keyword_search_meta_desc'] = 'SENSHAが提供する、洗車用洗剤やコーティング剤などの製品を購入できます。また、製品名や用途・目的から適した製品を選ぶこともできます。';
$lang['page_keyword_search_title'] = 'XXXXXの製品検索結果';
$lang['page_keyword_search_subtitle'] = 'XXXXXの関連商品の検索結果ページです。';

//Filter Page
$lang['page_filter_search_meta_title'] = '絞り込み検索｜SENSHA WORLD.com';
$lang['page_filter_search_meta_desc'] = 'SENSHAが提供する、洗車用洗剤やコーティング剤などの製品を購入できます。また、製品名や用途・目的から適した製品を選ぶこともできます。';
$lang['page_filter_search_title'] = 'PPF絞り込み検索結果';
$lang['page_filter_search_subtitle'] = 'PPFの絞り込み検索結果のページです。';

//Product Detail
$lang['page_product_detail_meta_title'] = 'XXXX（商品名）XXXXX｜SENSHA WORLD.com';
$lang['page_product_detail_meta_desc'] = 'XXXX（各商品の商品概要をDiscrptionに反映）XXXXX';
$lang['page_product_detail_domestic'] = 'Domestic';
$lang['page_product_detail_international'] = 'International';
$lang['page_product_detail_contents'] = '概要';
$lang['page_product_detail_import_shipping'] = '輸入送料';
$lang['page_product_detail_import_duty'] = '関税';
$lang['page_product_detail_shipping'] = '送料';
$lang['page_product_detail_delivery_fee'] = '手数料';
$lang['page_product_detail_tax'] = '消費税';
$lang['page_product_detail_coupon_discount'] = 'クーポン割引';
$lang['page_product_detail_user_discount'] = 'ユーザー割引';
$lang['page_product_detail_use_credit'] = 'クレジット使用';
$lang['page_product_detail_add_to_cart'] = 'カートに追加する';
$lang['page_product_detail_added'] = 'カートに追加されました。';
$lang['page_product_detail_added_caution'] = '＊国内配送商品および国際配送商品のいずれかのみのご注文となります。';
$lang['page_product_detail_add_your_produts_to'] = '欲しい物リストに';
$lang['page_product_detail_wishlist'] = '追加する';
$lang['page_product_detail_origin'] = '生産国';
$lang['page_product_detail_list_price'] = '定価';
$lang['page_product_detail_no_of_use'] = '使用可能回数';
$lang['page_product_detail_cost_per_car'] = '１台あたりの利用料';
$lang['page_product_detail_stackability'] = '保存の可否';
$lang['page_product_detail_minimum_quantity'] = '最低オーダー数';
$lang['page_product_detail_shipping_method'] = '発送方法';
$lang['page_product_detail_payment_method'] = '支払い方法';
$lang['page_product_detail_right_side_items'] = '個の商品';
$lang['page_product_detail_right_proceed_payment'] = 'お支払い画面へ';
$lang['page_product_detail_right_buy_now'] = 'お支払い';
$lang['page_product_detail_right_side_subtotal'] = '小計';
$lang['page_product_detail_right_side_minimum_cost'] = '最低送料';
$lang['page_product_detail_right_side_total'] = '合計';
$lang['page_product_detail_right_side_login_to_order'] = 'ログインして購入';
$lang['page_product_detail_right_side_buy'] = '購入する';
$lang['page_product_detail_right_side_recently'] = '最近見たアイテム';
$lang['page_product_detail_right_side_confirm_pay'] = '支払確定';
$lang['page_product_detail_number'] = '数量';
$lang['mail_payment_to_customer_dear'] = '';
$lang['mail_payment_to_customer_sama'] = '様';
$lang['mail_payment_to_customer'] = '
	この度は、「洗車の王国」をご利用いただきまして誠にありがとうございます。<br>
	以下の内容にてご注文を承りました。<br>
	発送の手配が整い次第、別途メールにて発送のご連絡をいたしますので、今しばらくお待ちくださいませ。<br>
	<br>
	-----------------------------------------------------------<br>
	【銀行振込（前払い）のお客様へ】<br>
	※送料が"0円"になっている場合に、またEMS以外の配送方法の場合は、別途送料お見積りとなります。<br>
	　弊社からのご案内をお待ちくださいますようお願いいたします。<br>
	-----------------------------------------------------------<br>
	<br>
	このメールはお客様のご注文に関する大切なメールです。<br>
	お取引が完了するまで保存してください。
';
$lang['mail_payment_to_customer_content'] = '**ご注文内容**';
$lang['mail_payment_to_customer_shipment_id'] = '注文番号： ';
$lang['mail_payment_to_customer_order_date'] = '注文日時： ';
$lang['mail_payment_to_customer_payment_method'] = '支払い方法： ';
$lang['mail_payment_to_customer_shipping_method'] = '配送方法： ';
$lang['mail_payment_to_customer_shipping_place'] = '配送先： ';
$lang['mail_payment_to_customer_sale_total_amount'] = 'ご請求金額： ';
$lang['mail_payment_to_customer_item_amount'] = '数量： ';
$lang['mail_payment_to_customer_item_price'] = '金額： ';
$lang['mail_payment_to_customer_item_sub_total_amount'] = '商品合計： ';
$lang['mail_payment_to_customer_item_delivery_amount'] = '送料： ';
$lang['mail_payment_to_customer_item_discount_amount'] = '値引き： ';
$lang['mail_payment_to_customer_item_use_credit'] = '使用ポイント： ';
$lang['mail_payment_to_customer_item_sale_subtotal_amount'] = '小計： ';
$lang['mail_payment_to_customer_item_admin_fee_amount'] = '手数料： ';
$lang['mail_payment_to_customer_item_vat_amount'] = '消費税： ';
$lang['mail_payment_to_customer_item_sale_total_amount'] = 'ご請求金額： ';
$lang['mail_payment_to_customer_item_total_amount'] = 'ご請求金額： ';
$lang['mail_payment_to_customer_notice_title'] = '■お振込み先のご案内';
$lang['mail_payment_to_customer_sale_total_amount_to_transfer'] = 'お振込金額: ';
$lang['mail_payment_to_customer_transfer_account'] = '<<お振込先>>';
$lang['mail_payment_to_customer_bank'] = '◆三菱UFJ銀行　池袋支店';
$lang['mail_payment_to_customer_bank_account'] = '普通　6699778 口座名義 洗車の王国';
$lang['mail_payment_to_customer_bank2'] = '◆みずほ銀行　池袋支店';
$lang['mail_payment_to_customer_bank2_account'] = '普通　3357543 口座名義 洗車の王国';
$lang['mail_payment_to_customer_caution'] = '※お振込完了後、振込先銀行名、お客様のお名前とご注文番号、そして振込み金額を';
$lang['mail_payment_to_customer_caution2'] = 'info@sensha-world.comまたはinfo@cleanyourcar.jp';
$lang['mail_payment_to_customer_caution3'] = 'までご一報頂きますようお願いいたします。';
$lang['mail_payment_to_customer_caution4'] = 'お客様よりお振込み完了のご連絡を頂け次第、ご注文商品発送手配をさせて頂きます。';
$lang['mail_payment_to_customer_confirm_notice'] = '■ご注文内容の確認';
$lang['mail_payment_to_customer_confirm_notice2'] = '購入履歴にてご確認いただけます。';
$lang['mail_payment_to_customer_confirm_notice3'] = '■請求書';
$lang['mail_payment_to_customer_confirm_notice4'] = 'ログイン後、下記よりダウンロードしていただけます。';
$lang['mail_payment_to_customer_confirm_notice5'] = '
	▼ご注意(必ずお読みください)▼<br>
	※在庫照会後、商品発送の手配へ進みますため、本メール受信後はご注文内容の変更・キャンセルはお受けするができませんことをご了承くださいませ。<br>
	※発送はできる限り迅速におこなっておりますが、天候や災害及び交通事情により遅れが生じる場合がございますことを何卒ご了承くださいませ。<br>
	※商品や発送等、ご不明な点がございましたら当店までお気軽にお問い合わせください。<br>
	※本メールはご注文の際にお客様にご指定いただいたメールアドレス宛へ送信しております。万が一、メールの内容にお心当たりがない場合は当店までご連絡をお願いいたします。<br>
	今後とも、洗車の王国をご愛顧いただけますよう宜しくお願い申し上げます。
';
$lang['mail_payment_to_customer_confirm_notice6'] = '▼ SENSHA NEWS ▼<br>こちらで最新情報をご案内しております';
$lang['mail_payment_to_customer_confirm_notice7'] = '洗車をして愛車を綺麗にすること<br>それはきっと安全運転につながるはず。';
$lang['mail_payment_to_customer_confirm_notice8'] = '
	Clean Your CAR<br>
	株式会社 洗車の王国<br>
	〒259-1141<br>
	神奈川県伊勢原市上粕屋1007-3<br>
	(Phone) 0463-94-5106<br>
	(Fax) 0463-94-5108<br>
	(E-Mail) info@cleanyourcar.jp<br>
	(URL) <a href="https://sensha-world.com/">https://sensha-world.com/</a>
';
$lang['mail_payment_to_customer_mail_title'] = 'ご注文承りました【洗車の王国】';



//Dashboard
$lang['page_dashboard_meta_title'] = 'ダッシュボード｜SENSHA WORLD.com';
$lang['breadcrumb_dashboard'] = 'ダッシュボード';
$lang['page_dashboard_credit'] = 'クレジットポイント';
$lang['page_dashboard_use_credit'] = 'クレジットポイントを使用する';
$lang['page_dashboard_use_credit_desc1'] = '現在保有しているポイントは';
$lang['page_dashboard_use_credit_desc2'] = 'JPYです。このポイントは商品の購入時に使用することができます。また、管理者ページからポイントを払い戻すこともできます。';
$lang['page_dashboard_edit_account'] = 'アカウント情報を編集する';
$lang['page_dashboard_edit_account_desc'] = '配達先、その他アカウント情報の編集はこちらから。';
$lang['page_dashboard_shopping_history'] = '購入履歴';
$lang['page_dashboard_shopping_history_desc'] = '購入履歴の確認や領収書のダウンロードはこちらから。';
$lang['page_dashboard_cart'] = 'カート';
$lang['page_dashboard_cart_desc'] = '現在のカートの確認や数量の変更はこちらから。';
$lang['page_dashboard_wishlist'] = 'ほしい物リスト';
$lang['page_dashboard_wishlist_desc'] = 'ほしい物リストの確認はこちらから。';
$lang['page_dashboard_faq'] = 'よくある質問';
$lang['page_dashboard_recommend'] = 'おすすめ商品';
$lang['page_dashboard_recommend_desc'] = '最近購入した商品を基に、おすすめ商品を表示しています。';
$lang['page_dashboard_recent_purchase'] = '最近購入した商品';
$lang['page_dashboard_recent_purchase_desc'] = '最近購入した商品を表示しています。';
$lang['page_dashboard_recently_viewed'] = '最近見た商品';
$lang['page_dashboard_recently_viewed_desc'] = '最近見た商品を表示しています。';

//Product General
$lang['page_product_general_material'] = '素材';
$lang['page_product_general_painting'] = '塗装';
$lang['page_product_general_emblem'] = 'エンブレム';
$lang['page_product_general_glass'] = 'ガラス';
$lang['page_product_general_tire'] = 'タイヤ';
$lang['page_product_general_tire_house'] = 'タイヤハウス';
$lang['page_product_general_plating'] = 'メッキ';
$lang['page_product_general_shaving'] = 'ポリッシュ（削り出し）';
$lang['page_product_general_unpainted_plastic'] = '無塗装プラスチック';
$lang['page_product_general_gum'] = 'ゴム';
$lang['page_product_general_engine'] = 'エンジン';
$lang['page_product_general_hose'] = 'ホース';
$lang['page_product_general_fabric'] = '布地';
$lang['page_product_general_leather'] = 'レザー';
$lang['page_product_general_meter_panel'] = 'ダッシュパネル';

$lang['page_product_general_usage'] = '用途';
$lang['page_product_general_protection'] = '保護';
$lang['page_product_general_glazing'] = '光沢復元';
$lang['page_product_general_self_healing'] = 'キズ自己修復';
$lang['page_product_general_water_repellent'] = '撥水効果';
$lang['page_product_general_oil_repellent'] = '撥油効果';
$lang['page_product_general_yellowing'] = '退色防止';
$lang['page_product_general_cracks'] = '表面のヒビ割れ対策';
$lang['page_product_general_rainy'] = '雨の日の視界改善';
$lang['page_product_general_night'] = '夜間の視界改善';
$lang['page_product_general_dust'] = 'ホコリ・砂の除去';
$lang['page_product_general_dirty_dirt'] = '泥汚れの除去';
$lang['page_product_general_dirt'] = '水垢の除去';
$lang['page_product_general_oil_film'] = '油膜の除去';
$lang['page_product_general_scratches'] = 'キズの除去';
$lang['page_product_general_kusumi'] = 'クスミの除去';
$lang['page_product_general_ring_stain'] = '輪シミの除去';
$lang['page_product_general_stain'] = 'シミの除去';
$lang['page_product_general_oil'] = '油分の除去';
$lang['page_product_general_tar'] = 'タールやピッチの除去';
$lang['page_product_general_iron_powder'] = '鉄粉の除去';
$lang['page_product_general_brake_dust'] = 'ブレーキダストの除去';
$lang['page_product_general_insects'] = '虫・鳥糞の除去';
$lang['page_product_general_clouded'] = '窓の内側のクモリ対策';
$lang['page_product_general_washer'] = 'ウォッシャー液';
$lang['page_product_general_deodorize'] = '消臭';
$lang['page_product_general_sterilization'] = '除菌';
$lang['page_product_general_antibacterial'] = '抗菌';

$lang['page_product_general_item_type'] = '商品タイプ';
$lang['page_product_general_basic_wash'] = '通常シャンプー';
$lang['page_product_general_coating'] = 'コーティング剤';
$lang['page_product_general_special_wash'] = '特殊クリーナー';
$lang['page_product_general_ppf'] = 'プロテクションフィルム';
$lang['page_product_general_others'] = 'その他';

$lang['page_product_general_feature'] = '特徴';
$lang['page_product_general_domestic_delivery'] = '国内発送';
$lang['page_product_general_free_sample'] = '無料サンプル';
$lang['page_product_general_bottle'] = '空ボトル';


//Cart
$lang['page_cart_meta_title'] = 'ショッピングカート｜SENSHA WORLD.com';
$lang['breadcrumb_cart'] = 'カート';
$lang['page_cart_quantity'] = '数量';
$lang['page_cart_price'] = '価格';
$lang['page_cart_available'] = '購入可能';
$lang['page_cart_unavailable'] = '購入不可能';
$lang['page_cart_add_to_wishlist'] = '欲しい物リストに追加';
$lang['page_cart_add_to_wishlist_login'] = ''; //zun lang
$lang['page_cart_delete'] = 'カートから削除';
$lang['page_cart_delivery'] = '配達方法';
$lang['page_cart_approximately'] = 'お届けまでXXXX日';
$lang['page_cart_subtotal'] = 'XXX商品の小計';
$lang['page_cart_proceed'] = '支払い処理へ進む';
$lang['page_cart_items_subtotal'] = 'アイテム　小計';
$lang['page_cart_fix_price'] = '宅配便';
$lang['page_cart_estimates'] = '送料別途見積もり';
$lang['page_cart_estimates_notice'] = '大量のご注文のため送料については後ほどご連絡させていただきます。決済方法は銀行振込をご選択の上、ご注文の確定をお願いいたします。弊社からのご案内をお待ち下さい。';
$lang['page_cart_error'] = '配達方法を選択してください。';

//Place delivery
$lang['page_place_delivery_meta_title'] = 'お届け先｜SENSHA WORLD.com';
$lang['page_place_delivery_use_registered'] = '配送先の住所';
$lang['page_place_delivery_use_this'] = 'この住所を使う';
$lang['page_place_delivery_edit_address'] = '住所を編集する';
$lang['page_place_delivery_please_update_delivery_address'] = '配送先住所が未設定です。';
$lang['page_place_delivery_change_address'] = '新しい住所を設定';
$lang['page_place_delivery_over_write'] = '上書きして新しい住所を使う';

//Payment
$lang['page_payment_meta_title'] = '支払い｜SENSHA WORLD.com';
$lang['breadcrumb_payment'] = '支払い';
$lang['page_payment_choose_payment'] = '支払い方法';
$lang['you_donot_need_to_pay'] = '支払う必要はありません。そのまま「支払確定」をクリックして決済を進めて下さい。';
$lang['page_payment_pay_with_amazon'] = 'Amazonを使って支払う';
$lang['page_payment_pay_with_paypal'] = 'Paypalを使って支払う';
$lang['page_payment_pay_with_alipay'] = 'Alipayを使って支払う';
$lang['page_payment_pay_with_bank_tranfer'] = '銀行振込で支払う';
$lang['page_payment_pay_on_delivery'] = '着払いで支払う';
$lang['page_payment_no_found_coupon'] = 'クーポンコードを入力してください。';
$lang['page_payment_can_not_use'] = 'このクーポンは使用できません。';

$lang['page_payment_pay_with_amazon_forsave'] = 'Amazon';
$lang['page_payment_pay_with_paypal_forsave'] = 'Paypal';
$lang['page_payment_pay_with_alipay_forsave'] = 'Alipay';
$lang['page_payment_pay_with_bank_tranfer_forsave'] = '銀行振込';
$lang['page_payment_pay_on_delivery_forsave'] = '代金引換';
$lang['page_payment_credit_point'] = '割引・クーポン';
$lang['page_payment_without_coupon'] = 'クーポンなし';
$lang['page_payment_use_credit'] = '一部のクレジットポイントを使用する';
$lang['page_payment_your_credit1'] = '現在';
$lang['page_payment_your_credit2'] = 'USDポイントを保有しています。使用するポイントを入力してください。';
$lang['page_payment_enter_points'] = '利用するポイントを入力してください';
$lang['page_payment_use_all'] = 'すべてのポイントを使用する';
$lang['page_payment_you_can_use'] = 'この支払いに利用可能なすべてのポイントを使うことができます。';
$lang['page_payment_coupon'] = 'クーポンあり';
$lang['page_payment_apply_coupon'] = 'クーポンを使用することができます。';
$lang['page_payment_enter_coupon'] = 'クーポンコードを入力してください。';
$lang['page_payment_pay'] = '支払いを完了する';

//Cancel
$lang['page_cancel_meta_title'] = '注文キャンセル｜SENSHA WORLD.com';
$lang['breadcrumb_cancel'] = '注文キャンセル';
$lang['page_cancel_order_confirmation'] = '注文キャンセル';
$lang['page_cancel_your_order'] = '注文をキャンセルしました。';
$lang['page_cancel_thank_you'] = '
<p>ご注文がキャンセルされました。</p>
<p>意図されずにキャンセルされた場合は、お支払い方法に問題が発生しているケースが考えられますので、</p>
<p>異なるお支払い方法をお試しください。</p>';

//Complete
$lang['page_complete_meta_title'] = '注文確定｜SENSHA WORLD.com';
$lang['breadcrumb_complete'] = '注文確定';
$lang['page_complete_order_confirmation'] = '注文確定';
$lang['page_complete_your_order'] = '注文を承りました。';
$lang['page_complete_thank_you'] = '
<p>ご注文ありがとうございます。注文内容は登録済みのEメールでお知らせいたします。</p>
<p>領収書については下記の注文履歴からダウンロードできます。</p>
<p>また、2週間経ってもEメールや商品が届かない場合は注文履歴からご連絡ください。</p>';
$lang['page_complete_order_history'] = '注文履歴';

$lang['breadcrumb_contact_confirm'] = 'お問い合わせ内容確認';
$lang['page_contact_name'] = '名前';
$lang['page_contact_email'] = 'Email';
$lang['page_contact_message'] = '内容';
$lang['page_contact_submit'] = '確認';
$lang['page_contact_tel'] = '電話番号';
$lang['page_contact_country'] = '国';

$lang['breadcrumb_contact_complete'] = 'お問い合わせ完了';
$lang['page_contact_complete_confirmation'] = 'お問い合わせ完了';
$lang['page_contact_complete_your_message'] = 'お問い合わせを承りました。';
$lang['page_contact_complete_thank_you'] = '<p>お問い合わせありがとうございます。お問い合わせ内容に対して近日中に担当者よりご返答させて頂きます。</p>
<p>なお、お問い合わせ内容によってはご返答出来かねるケースもございます。その旨ご了承ください。</p>';

//Account Info
$lang['page_account_info_meta_title'] = 'アカウント情報｜SENSHA WORLD.com';
$lang['breadcrumb_account_info'] = 'アカウント情報';
$lang['page_account_info_account_info'] = 'アカウント情報';
$lang['page_account_info_edit_account_info'] = 'アカウント情報編集';
$lang['page_account_info_id'] = 'ID';
$lang['page_account_info_email'] = '登録連絡先';
$lang['page_account_info_telephone'] = '電話番号';
$lang['page_account_info_password'] = 'パスワード';
$lang['page_account_info_country'] = '国';
$lang['page_account_info_state'] = '県';
$lang['page_account_info_edit'] = '編集';

$lang['page_shipping_info_meta_title'] = '配送情報｜SENSHA WORLD.com';
$lang['breadcrumb_shipping_info'] = '配送情報';
$lang['page_shipping_info_shipping_info'] = '配送情報';
$lang['page_shipping_info_name'] = '担当者名';
$lang['page_shipping_info_company'] = '会社名';
$lang['page_shipping_info_zip'] = '郵便番号';
$lang['page_shipping_info_address'] = '住所';
$lang['page_shipping_info_state'] = '県';
$lang['page_shipping_info_country'] = '国';
$lang['page_shipping_info_telephone'] = '電話番号';
$lang['breadcrumb_confirm_shipping_info'] = '配送先情報確認';
$lang['page_confirm_shipping_info'] = '配送先情報確認';
$lang['breadcrumb_complete_shipping_info'] = '配送先情報変更完了';
$lang['page_complete_shipping_info'] = '配送先情報変更完了';
$lang['page_complete_shipping_info_caption'] = '配送先情報の変更が完了しました。';
$lang['page_complete_shipping_info_description'] = '配送先情報が更新されました。更新した内容を確認したい場合は「アカウント情報を編集する」よりご確認ください。';

$lang['page_invoice_info_meta_title'] = '請求書・領収書情報｜SENSHA WORLD.com';
$lang['breadcrumb_invoice_info'] = '請求書・領収書情報';
$lang['page_invoice_info_change_invoice'] = '請求書・領収書情報';
$lang['page_invoice_info_notice'] = '※配送先情報と異なる場合のみこちらに記載ください。';
$lang['page_invoice_info_name'] = '担当者名';
$lang['page_invoice_info_company'] = '会社名';
$lang['page_invoice_info_zip'] = '郵便番号';
$lang['page_invoice_info_address'] = '住所';
$lang['page_invoice_info_state'] = '県';
$lang['page_invoice_info_country'] = '国';
$lang['page_invoice_info_telephone'] = '電話番号';
$lang['page_invoice_info_tax'] = 'Tax ID';
$lang['breadcrumb_confirm_invoice_info'] = '請求書・領収書情報　確認';
$lang['page_confirm_invoice_info'] = '請求書・領収書情報　確認';
$lang['breadcrumb_complete_invoice_info'] = '請求書・領収書情報変更完了';
$lang['page_complete_invoice_info'] = '請求書・領収書情報変更完了';
$lang['page_complete_invoice_info_caption'] = '請求書・領収書情報の変更が完了しました。';
$lang['page_complete_invoice_info_description'] = '請求書・領収書情報が更新されました。更新した内容を確認したい場合は「アカウント情報を編集する」よりご確認ください。';

//Edit account info
$lang['page_edit_account_meta_title'] = 'アカウント情報の編集｜SENSHA WORLD.com';
$lang['breadcrumb_edit_account'] = 'アカウント情報編集';
$lang['page_edit_account_edit'] = 'XXXXを入力してください';
$lang['page_edit_account_update'] = '保存';

//Check Account
$lang['page_check_account_meta_title'] = 'アカウント情報の確認｜SENSHA WORLD.com';
$lang['breadcrumb_check_account'] = 'アカウント情報の確認';
$lang['page_check_account_confirm_title'] = 'アカウント情報の確認';
$lang['page_check_account_confirm'] = '確認';

//Complete Personal Info
$lang['page_complete_change_account_meta_title'] = '変更完了｜SENSHA WORLD.com';
$lang['breadcrumb_complete_change_account'] = '変更完了';
$lang['page_complete_change_account_confirmed'] = '変更完了';
$lang['page_complete_change_account_updated'] = 'アカウント情報が更新されました。';
$lang['page_complete_change_account_your_account_informaion'] = 'アカウント情報が更新されました。更新した内容を確認したい場合は「アカウント情報」よりご確認ください。';

//Order History
$lang['page_order_history_meta_title'] = '注文履歴｜SENSHA WORLD.com';
$lang['breadcrumb_order_history'] = '注文履歴';
$lang['page_order_history_order_history'] = '注文履歴';
$lang['page_order_history_shipment_id'] = '注文番号';
$lang['page_order_history_data_of_order'] = '注文日';
$lang['page_order_history_delivery_address'] = '配送先住所';
$lang['page_order_history_total'] = '合計金額';
$lang['page_order_history_delivery_mothod'] = '配送方法';
$lang['page_order_history_domestic_post'] = '国内配送';
$lang['page_order_history_ems'] = 'INTERNATIONAL STANDARD SHIPMENT: EMS';
$lang['page_order_history_exw'] = 'BUYER TRANSPORTATION';
$lang['page_order_history_exw_notice'] = '*All Shipment fees as well as pick up from SELLER warehouse is arranged & paid by BUYER';
$lang['page_order_history_cif'] = 'SELLER TRANSPORTATION';
$lang['page_order_history_cif_notice'] = '*Using SELLER\'s corporate forwarder/transport & estimate quote all total shipping fee to BUYER';
$lang['page_order_history_show_more'] = 'もっと見る';
$lang['page_order_history_invoice_download'] = '請求書発行';
$lang['page_order_history_receipt_download'] = '領収書発行';
$lang['page_order_history_claim_shipment'] = '配送に関する問題';
$lang['page_order_history_item_sub_total_amount'] = '商品合計';
$lang['page_order_history_delivery_amount'] = '送料';
$lang['page_order_history_discount_amount'] = '割引';
$lang['page_order_history_amount'] = '数量';
$lang['page_order_history_sub_total'] = '小計';
$lang['page_order_history_show_more'] = 'もっと見る';
$lang['page_order_history_show_less'] = '隠す';

//Delivery Claim
$lang['page_delivery_claim_meta_title'] = '配送に関する問題｜SENSHA WORLD.com';
$lang['breadcrumb_delivery_claim'] = '配送に関する問題';
$lang['page_delivery_claim_order_number'] = '問題が起こった注文番号';
$lang['page_delivery_claim_about_issue'] = '問題について';
$lang['page_delivery_claim_issue_details'] = 'どのような問題ですか？';
$lang['page_delivery_claim_not_obtained_products'] = '商品が届いていない';
$lang['page_delivery_claim_not_obtained_any_products'] = '注文した商品が１点も届いていない場合';
$lang['page_delivery_claim_missing_products'] = '商品が不足している';
$lang['page_delivery_claim_obtained_products_but_missing'] = '注文した商品は届いているが、数量が足りない場合/一部不足がある場合';
$lang['page_delivery_claim_obtained_wrong_products'] = '注文した商品ではない';
$lang['page_delivery_claim_case_obtained_wrong_products'] = '注文した商品とは異なる商品が届いた場合';
$lang['page_delivery_claim_others'] = 'その他';
$lang['page_delivery_claim_email'] = 'Eメール';
$lang['page_delivery_claim_describe_issues'] = '具体的な理由をご記入ください。';
$lang['page_delivery_claim_comments'] = 'コメント';
$lang['page_delivery_claim_send'] = '送信';

//Confirm Claim
$lang['page_delivery_claim_confirm_meta_title'] = '確認画面｜SENSHA WORLD.com';
$lang['breadcrumb_delivery_claim_confirm'] = '問題についての申告確認';
$lang['page_delivery_claim_confirm_order_number'] = '問題が起こった注文番号';
$lang['page_delivery_claim_confirm_order_number'] = '発注商品';
$lang['page_delivery_claim_confirm_delivery_type'] = '発送タイプ';
$lang['page_delivery_claim_confirm_reason'] = '理由';
$lang['page_delivery_claim_confirm_claim'] = '問題';
$lang['page_confirm_claim_confirm'] = '送信';
$lang['page_confirm_claim_check_box'] = 'Currently you have available for shopping. You can add the number not over than your total points.';

//Complete Claim
$lang['page_complete_claim_meta_title'] = '送信完了｜SENSHA WORLD.com';
$lang['breadcrumb_complete_claim'] = '送信完了';
$lang['page_complete_claim_claim_sent'] = '送信完了';
$lang['page_complete_claim_your_claim'] = '問題が報告されました。';
$lang['page_complete_claim_we_check'] = 'この度はご迷惑をおかけして誠に申し訳ございません。いただいた情報を元に早急に問題を解決いたします。';

//Claim Mail
$lang['claim_mail_title'] = '洗車の王国へ配送に関する問題を承りました【洗車の王国】';
$lang['claim_mail_cliant_en'] = '';
$lang['claim_mail_cliant_jp'] = '様';
$lang['claim_mail_paragraph01'] = '  <br><br><br>
  お世話になっております。<br>
  洗車の王国カスタマーセンターでございます。<br><br>
  下記の内容にてご連絡を承りました。<br><br>
  ============================<br>';
$lang['claim_mail_shipment_id'] = '注文番号  :';
$lang['claim_mail_order_date'] = 'ご注文日  :';
$lang['claim_mail_email'] = 'EMAIL  :';
$lang['claim_mail_reason'] = 'どのような問題ですか？  : ';
$lang['claim_mail_comment'] = '具体的な理由をご記入ください。 :';
$lang['claim_mail_paragraph02'] = ' ============================<br>
  <br>
  <br>
  -----------------------------------------------------------<br>
  ■ご注文内容の確認<br>
  こちらの注文履歴にてご確認いただけます<br>
  https://sensha-world.com/page/order_history<br>
  -----------------------------------------------------------<br>
  <br>
  <br>
  ▼ご注意(必ずお読みください)▼<br>
  <br>
  ※内容を確認し、折り返しご連絡をさせていただきますので<br>
  お待ちいただけますようお願いいたします。<br>
  <br>
  <br>
  ※本メールはお問い合わせの際にお客様にご指定いただいたメールアドレス宛へ送信しております。<br>
  万が一、メールの内容にお心当たりがない場合は大変お手数でございますが当店までご連絡をお願いいたします。<br>
  <br>
  <br>
  今後とも洗車の王国をご愛顧いただけますよう宜しくお願い申し上げます。<br>
  <br>
  <br>
  ▼ SENSHA NEWS ▼<br>
  こちらで最新情報をご案内しております<br>
  http://sensha.enfete.co/jp/page/news<br>
  <br>
  ======================<br>
  洗車をして愛車を綺麗にすることそれはきっと安全運転につながるはず。<br>
  ======================<br>
  <br>
  Clean Your CAR<br>
  株式会社 洗車の王国<br>
  〒259-1141<br>
  神奈川県伊勢原市上粕屋1007-3<br>
  (Phone) 0463-94-5106<br>
  (Fax) 0463-94-5108<br>
  (E-Mail) info@cleanyourcar.jp<br>
  (URL) http://www.cleanyourcar.jp<br>
  (World site) http://sensha-world.com/<br>';


//Wishlist
$lang['page_wishlist_meta_title'] = '欲しい物リスト｜SENSHA WORLD.com';
$lang['breadcrumb_wishlist'] = '欲しい物リスト';
$lang['page_wishlist_wishlist'] = 'あなたの欲しい物リスト';
$lang['page_wishlist_delete'] = '削除';

//About Page
$lang['page_about_meta_title'] = 'SENSHAについて ｜ SENSHA';
$lang['page_about_meta_desc'] = '当社の製品は、世界35カ国 800店の洗車コーティングショップで採用されております。世界の自然環境は様々です。各国の異なる自然環境の中から、多くのフィードバックをいただき、その製品性能は磨かれてきました。世界に広がるSENSHAネットワークが信頼の証です。';
$lang['breadcrumb_about_sensha'] = 'SENSHAについて';
$lang['page_about_key_visual_title'] = 'CAPTIVATE YOU <br>BY REAL CAR BEAUTY';
$lang['page_about_key_visual_subtitle'] = 'WITH PAINT PROTECTION FILM';
$lang['page_about_key_visual_desc'] = 'Syncshield (シンクシールド) は、車体の保護を目的に開発された特別なプロテクションフィルムです。開発から生産まで一括して日本国内でおこなうことで、高機能かつリーズナブルな価格での提供を可能としています。';
$lang['page_about_sensha_way'] = 'THE SENSHA WAY';
$lang['page_about_section1_title'] = 'SENSHAは高品質な洗車用品を生産・販売しています。';
$lang['page_about_section1_desc'] = '“SENSHA”ではすべての自動車オーナー様に満足いただけるよう、高品質で信頼性の高い製品を提供いたします。<br>
当社のスタッフは品質・信頼を確保するための業務を怠らず、みなさまの最善のパートナーになるための努力を惜しみません。';
$lang['page_about_section2_title'] = '品質と信頼へのこだわり';
$lang['page_about_section2_subtitle'] = 'プロ品質と製品へのプライド';
$lang['page_about_section2_content'] = '
        <p>当社の製品は、直営店舗とフランチャイズ店を合わせて、世界35カ国 800店舗のネットワークにて採用されております。 私たちは、常に品質と信頼性に重点をおき、お客様のご要望にお応えできる様、最善を尽くしています。 これが、日本で研究開発および、製造を行っている最大の理由です。</p>
        <p>既存製品は常にフィードバックを収集しており、その情報は改善はもちろん新製品開発を行なうために役立てられています。 実際に、当社の製品は -30℃から40℃など極端な温度下でも、十分な機能が発揮できる様に設計されています。</p>
        <p>“SENSHA”では「品質と信頼」が必要不可欠です。それは製品とカスタマーサービスにおけるものだけではなく、“SENSHA”の社会に対する姿勢、そしてスタッフの仕事への取り組み方を考えるにあたり、重要な要素となっています。 “SENSHA”は以下の場面で高い品質を求めなければならないと考えています。</p>
        <ul>
          <li>-  製品の品質</li>
          <li>-  販売、サービスの品質</li>
          <li>-  ラインナップと納期面での品質</li>
          <li>-  取引の品質</li>
        </ul>
        <p>世界中の“SENSHA family”が、あなたのビジネスとカーライフをサポートすることをお約束します。 世界35カ国 800店舗以上の実績がその証拠です。</p>
	';
$lang['page_about_section2_img1_title'] = 'Marketing';
$lang['page_about_section2_img1_txt'] = '
				1. Marketing<br>
				2. Staff Training<br>
				3. Local Shop<br>
				4. Online Shop';
$lang['page_about_section2_img2_title'] = 'Service';
$lang['page_about_section2_img2_txt'] = '
				1. Marketing<br>
				2. Staff Training<br>
				3. Local Shop<br>
				4. Online Shop';
$lang['page_about_section2_img3_title'] = 'Reserch and<br>development';
$lang['page_about_section2_img3_txt'] = '
				1. Marketing Survey<br>
				2. Research &Development<br>
				3. Domestic Experiment<br>
				4. Monitoring<br>
				5. International Experimen';
$lang['page_about_section3_title'] = 'SENSHAブランドの製品';
$lang['page_about_section3_topic1_title'] = '多種多様なカーケア製品をラインナップする理由';
$lang['page_about_section3_topic1_content'] = '<p>“SENSHA”では、全てお車の洗車やコーティングに対応し、全てのお客様のニーズにお応えするため、カーケアに必要な全ての製品ラインナップを提供しております。 これにより、ケミカル製品同士の互換性から発生する問題を未然に防ぐことが出来ます。また、煩雑化する複数の納入業者との取引を避ける事ができます。“SENSHA”の製品同士の適合性は全てテスト済みです。</p>';
$lang['page_about_section3_topic2_title'] = '製品性能を最大限に発揮させる為の取り組み';
$lang['page_about_section3_topic2_content'] = '<p>私たちは、どの様な素材が、車体のどの部分に使用されているか把握した上で製品開発をおこなっています。したがって、当社の製品はすべて『それぞれのパーツ毎の課題を解決するための製品』となっています。コーティングについて例をあげると、ホイールに対しボディー表面用のコーティング剤を使用する事はできません。また、カーシャンプーでは鉄粉除去を行なうことは出来ません。</p>
    <p>当社の全ての製品は、その性能と仕上がりを最大限に発揮するために、それぞれの素材とパーツを理解した上で開発されています。</p>';
$lang['page_about_section3_cat1_title'] = 'BODY';
$lang['page_about_section3_cat1_desc'] = '塗装表面を保護し劣化を遅らせ、光沢を維持します。また、細かい傷や経年劣化も改善できます。';
$lang['page_about_section3_cat2_title'] = 'WINDOW';
$lang['page_about_section3_cat2_desc'] = '撥水コーティングで安全な運転を約束します。油膜や輪ジミも改善できます。';
$lang['page_about_section3_cat3_title'] = 'WHEEL/TIRE';
$lang['page_about_section3_cat3_desc'] = 'ブレーキダストや汚れからホイールを守ります。またタイヤを劣化から防ぎ光沢を維持します。';
$lang['page_about_section3_cat4_title'] = 'MALL/BUMPER';
$lang['page_about_section3_cat4_desc'] = 'プラスチック表面に付着した汚れやワックスを取り除き、新品同様の光沢を復活させます。';
$lang['page_about_section3_cat5_title'] = 'ENGINE ROOM';
$lang['page_about_section3_cat5_desc'] = 'エンジンルーム内の汚れを取り除き、光沢を取り戻します。錆も防ぐことができます。';
$lang['page_about_section3_cat6_title'] = 'INTERIOR';
$lang['page_about_section3_cat6_desc'] = '布地やレザー、プラスチックなど、それぞれ専用のクリーナーやワックスをご用意しています。';
$lang['page_about_section4_title'] = '”SENSHA”のグローバル展開';
$lang['page_about_section4_desc'] = '私たちは、世界35カ国で製品とサービスを提供しています。ご不明な点がございましたら、どんな事でもお問合せください。';
$lang['page_about_section4_contact'] = 'お問い合わせ';
$lang['page_about_section4_head_office'] = '本社';
$lang['page_about_section4_office_address'] = '住所';
$lang['page_about_section4_office_address_content'] = '〒259-1141 神奈川県伊勢原市上粕屋１００７－３';
$lang['page_about_section4_phone'] = '電話番号';
$lang['page_about_section4_phone_content'] = '0463-94-5106';
$lang['page_about_section4_office_address'] = '住所';
$lang['page_about_section4_link_to_contact'] = 'Contact Us';

//About PPF
$lang['page_about_ppf_meta_title'] = 'PPFについて｜SENSHA WORLD.com';
$lang['page_about_ppf_meta_desc'] = 'アメリカ製が主流のPPF業界、洗車の王国 ブランド (SENSHA brand) の新たなフラッグシップ製品として、日本のフィルム関連製造企業数社と洗車の王国が、共同で開発した製品がSyncshieldです。Syncshield は、市場に大きく販売されているアメリカ製 PPF などと比べ、多くの特別な性能が付加されました。他の製品とはレベルが違います。';
$lang['breadcrumb_about_ppf'] = 'PPFについて';
$lang['page_about_ppf_key_visual_title'] = 'CAPTIVATE YOU <br>BY REAL CAR BEAUTY';
$lang['page_about-ppf_key_visual_subtitle'] = 'WITH PAINT PROTECTION FILM';
$lang['page_about-ppf_key_visual_desc'] = '世界35カ国800店舗以上の洗車・コーティングショップがあるSENSHAは、世界中のあらゆる地域で高性能PPF Syncshieldを展開しております。';
$lang['page_about_ppf_section1_title'] = 'ペイントプロテクションフィルムとは';
$lang['page_about_ppf_section1_desc'] = '一般的には、“PPF” と呼ばれますが、これは “Paint Protection Film” の頭文字を取った略称です。PPF (ペイントプロテクションフィルム) の主な施工目的は《車体表面保護》 《退色防止》 《キズ防止》です。';
$lang['page_about_ppf_section1_imgtxt1'] = '離型紙';
$lang['page_about_ppf_section1_imgtxt2'] = '自己修復層';
$lang['page_about_ppf_section1_imgtxt3'] = '無黄変タイプ<br>ポリウレタンフィルム';
$lang['page_about_ppf_section1_imgtxt4'] = '粘着層';
$lang['page_about_ppf_section1_imgtxt5'] = '離型紙';
$lang['page_about_ppf_section1_desc2'] = '<p>PPF は、現在アメリカ製の商品が主流となっており、数社のみから販売されております。 その様な PPF 業界の中、Syncshield は、洗車の王国 ブランド (SENSHA brand) の新たなフラッグシップ製品として、日本のフィルム関連製造企業数社と洗車の王国が、共同で開発した製品になります。もちろん、開発および製造の全てを日本国内で行っております。</p>
    <p>Syncshield は、市場に大きく販売されているアメリカ製 PPF などと比べ、多くの特別な性能が付加されました。他の製品とはレベルが違います。</p>';
$lang['page_about_ppf_section1_feature1'] = '自己修復機能';
$lang['page_about_ppf_section1_feature2'] = '防汚性能';
$lang['page_about_ppf_section1_feature3'] = '撥水性能';
$lang['page_about_ppf_section1_feature4'] = '撥油性能';
$lang['page_about_ppf_section1_feature5'] = '耐溶剤性能';
$lang['page_about_ppf_section2_title'] = 'シリアルナンバー検索';
$lang['page_about_ppf_section2_input'] = 'シリアルナンバーを入力';
$lang['page_about_ppf_section2_search'] = '検索';
$lang['page_about_ppf_section2_desc'] = 'Syncshield製品には固有のシリアルナンバーが発行されております。SENSHA商品お取り扱い店で施行された際に受け取られたシリアルナンバーを入力することで、施行詳細情報をこちらからご確認いただけます。このシリアルナンバーシステムにより、Syncshieldが正規品であることを保証します。';
$lang['page_about_ppf_section3_title'] = '品質テスト';
$lang['page_about_ppf_section3_subtitle'] = '他社製品とSyncshieldを比較した映像です。';
$lang['page_about_ppf_section4_title'] = 'PPFの効果';
$lang['page_about_ppf_section4_item1_title'] = '光沢性';
$lang['page_about_ppf_section4_item1_desc'] = '写真はSyncshield をボディパネルに貼った際の写真です。ご覧の通り、古くなり艶の無くなったボディでも、Syncshield を貼ることにより、新品の高級家具の様な光沢を実現させることができます。自動車のボディー表面も同様です。Syncshield を貼るだけで、艶の無くなった中古車塗装表面であっても、新車同様の光沢を取り戻すことができます。新車への施工では、新車以上の光沢感をご体感いただけるはずです。特に濃縮カラーは非常に深い光沢感を得ることが出来ます。';
$lang['page_about_ppf_section4_item2_title'] = '耐キズ・自己修復機能';
$lang['page_about_ppf_section4_item2_desc'] = 'Syncshield は、フィルム自体が自己修復する機能をもっているため、細かなキズの発生をほぼ防ぐことができます。高速走行中の飛び石、手荷物をぶつけた際の擦り傷など多くのキズから大切な愛車を守ることができます。';
$lang['page_about_ppf_section4_item3_title'] = '撥水性能';
$lang['page_about_ppf_section4_item3_desc'] = 'Syncshield は、非常に優秀な撥水性能を発揮します。水接触角の測定試験では、110℃を記録しています。これは、撥水型ガラスコーティング剤と同等の撥水性能です。したがって、既存のガラスコーティング剤やワックスなどを使用する必要は全くありません。';
$lang['page_about_ppf_section4_item4_title'] = '撥油性能';
$lang['page_about_ppf_section4_item4_desc'] = '自動車には、道路のアスファルトなどから飛んでくる、タールやピッチなどの油汚れが付着します。さらに、排気ガスに含まれる脂汚れも自動車が汚れる要因の一つです。これらの油汚れのこびり付きを防ぐため、撥油性能を強化しました。ヘキサデカンを用いての接触角測定試験では、撥油性能65℃を記録しました。動画は、エアゾール式のエンジンパーツクリーナーを吹き掛けた様子です。';
$lang['page_about_ppf_section4_item5_title'] = '耐溶剤性能';
$lang['page_about_ppf_section4_item5_desc'] = '自動車には、日頃から様々な油汚れが付着します。タールやピッチだけでなく、整備作業中にクリーナーが掛かってしまったり、給油中にガソリンが掛かってしまったりなど様々です。それらの油に含まれる有機溶剤は、PPFに致命的なシミを発生させます。Syncshield は、撥油性能だけでなく、耐溶剤性能も強力にしました。この動画は、これまで紹介した各性能について、テストを実施した様子を撮影したものです。';
$lang['page_about_ppf_section5_title'] = 'PPFの特徴';
$lang['page_about_ppf_section5_subtitle'] = '';
$lang['page_about_ppf_section5_topic1_title'] = '素材　-MATERIAL-';
$lang['page_about_ppf_section5_topic1_desc'] = '<p>一般的に PPF の基材(フィルム)には、PVCもしくは、TPUが使われています。PVCのメリットは安価で製造が可能な点にあります。ただし、PVCの最大のデメリットは、耐候性が著しく劣る事から、PPFには不向きとされています。</p>
<p>さらに、伸縮性能悪さから無理に引っ張ったり、熱を加えたりなどの施工が必要となります。それは、耐久性やチッピング性能の低下をもたらす要因のひとつとなっています。TPUとは“熱可塑性ポリウレタン”の意味です。TPUの最大のメリットは、伸縮性に富み、施工性が非常に良いことです。さらに、近年では無黄変タイプのTPU素材が開発され、耐候性にも優れていることから、現在販売されている多くの PPFも、このTPUを採用しています。Syncshield も同様に、無黄変タイプのTPUを採用しております。</p>';
$lang['page_about_ppf_section5_topic2_title'] = '透明性　-TRANSPARANCY-';
$lang['page_about_ppf_section5_topic2_desc'] = '基本性能として、高い透明性は不可欠です。車種ごとにあるボディーカラーの風合いを損ねない様、高い透明性にこだわり開発いたしました。施工後であっても、フィルムが貼ってある事に気付かれる事は無いでしょう。フィルムが貼ってある事に気付かれる事の無い高い透明性の実現は、PPF の重要な基本性能のひとつです。';
$lang['page_about_ppf_section5_topic3_title'] = '平滑性　-SMOOTHNESS-';
$lang['page_about_ppf_section5_topic3_desc'] = 'Syncshield が求めた究極の平滑性は、鏡の様に平らな表面を実現させることでした。そして、その開発目標を実現させることが出来ました。PPF の基本性能において、ボディーカラーの風合いを損ねず、フィルムが貼ってある事に気付かれる事の無い、高い平滑性は重要なポイントです';
$lang['page_about_ppf_section5_topic4_title'] = '伸縮性能　-FLEXIBILITY-';
$lang['page_about_ppf_section5_topic4_desc'] = 'Syncshield のウレタンフィルムは、車体表面の複雑な三次曲面にも無理なく追従する様、“しなやかさ” にこだわって開発されました。これにより、無理に引っ張ったり、熱をかけたりする極端な施工を軽減させる事に成功いたしました。これは、耐久性と耐チッピング性能に著しい向上をもたらしています。';
$lang['page_about_ppf_section5_topic5_title'] = '無黄変性能　-NON YELLOWING-';
$lang['page_about_ppf_section5_topic5_desc'] = 'Syncshield に採用されたウレタン素材は、無黄変タイプの特別な素材です。その性能は世界最高峰の品質です。';
$lang['page_about_ppf_section5_topic6_title'] = '粘着剤　-ADHESIVE-';
$lang['page_about_ppf_section5_topic6_desc'] = '今回、Syncshield に採用された粘着剤は、約200種類の中から選ばれ、さらに柔軟性のあるウレタンフィルムに合うよう調整されています。施工後の剥がれを防止するため必要な粘着力を確保しながらも、施工性を向上させるため、初期粘着を抑える特殊な加工を施しています。施工の容易さも Syncshield の特徴のひとつとなっています。';
$lang['page_about_ppf_section6_title'] = '注意事項';
$lang['page_about_ppf_section6_desc'] = '<li>・ 施工の際には、専用の施工液をご使用ください。</li>
          <li>・ 日頃の洗車やメンテナンスには、SENSHA brand の製品をご使用ください。</li>
          <li>＊ 他社品をご使用の場合、品質を確保することは困難な為、保証対象外となります。</li>';

//Sole Agent
$lang['page_sole_agent_meta_title'] = '代理店募集｜SENSHA WORLD.com';
$lang['page_sole_agent_meta_desc'] = '私たちは、世界中のパートナーとの協力の元、世界35カ国800店舗以上の洗車・コーティングショップのネットワークを築いてきました。私たちは“SENSHA Family”と呼んでいます。
';
$lang['breadcrumb_sole_agent'] = '代理店募集';
$lang['page_sole_agent_key_visual_title'] = 'Sole Agent Wanted';
$lang['page_sole_agent_key_visual_desc'] = '私たちが“SENSHA Family”と呼ぶ世界35カ国のネットワークに、あなたが加わることを希望するなら、私たちはいつでも歓迎します。';
$lang['page_sole_agent_section1_title'] = '代理店募集';
$lang['page_sole_agent_section1_desc'] = '私たちは“SENSHA Family”と呼ぶ世界35カ国800店舗以上の洗車・コーティングショップのネットワークは、信頼と実績の証です。 1997年に創業した洗車の王国は、現在35ヶ国800店舗以上を抱える世界有数の洗車・コーティングショップ、カーケア用品ブランドとして成長しました。 私たちが大切にしているのは製品だけではありません。ディテイリングショップの成功は製品だけでは実現しないからです。 私たちのゴールは、すべての“SENSHA Family”の成功です。私たちは、ディテイリングショップの成功に必要な全てを提供することが出来ます。';
$lang['page_sole_agent_section2_title'] = '私達にできること';
$lang['page_sole_agent_section2_item1_title'] = '低コストで事業スタート';
$lang['page_sole_agent_section2_item1_desc'] = '<p>SENSHAでは、加盟料や月々のフランチャイズ料はありません。
必要な技術研修を受けていただき、SENSHA製品をご使用いただければ、SENSHA brandとしてスタートすることができます。
世界中のパートナーと我々のゴールは同じです。
皆さまの事業の成功が私達の唯一の願いです。</p>
<ul>
<li>・ ブランドマークが使用可能</li>
<li>・ 加盟料無料</li>
<li>・ 月々のフランチャイズ料不要</li>
</ul>';
$lang['page_sole_agent_section2_item2_title'] = '製品購入時の負担を軽減';
$lang['page_sole_agent_section2_item2_desc'] = '<p>SENSHAで提供する製品は全て日本で生産されています。全ての製品は自社で開発され、世界中のパートナーへご提供しています。
商品の仕入れは「発注ロット」「仕入れ価格」「納期」など悩ましい問題が山積みです。
世界中のパートナー企業が製品を購入する際の負担を軽減することも我々のテーマです。</p>
<ul>
<li>・ パートナー企業専用システムから容易に発注が可能</li>
<li>・ 割引価格での提供</li>
<li>・ 1個からでも発注可能</li>
<li>・ 様々な支払い方法</li>
<li>・ 明確な出荷スケジュールを提示</li>
<li>・ 輸送コストの見積もり</li>
</ul>';
$lang['page_sole_agent_section2_item3_title'] = 'プロモーション素材の提供';
$lang['page_sole_agent_section2_item3_desc'] = '<p>SENSHAパートナーになることで、世界35カ国のSENSHA FamilyがSNSなどにアップしている画像や動画を自由に活用することができます。
あなたが事業をスタートする際に、プロモーション費用を軽減することができます。</p>
<ul>
<li>・ 世界のSENSHA Familyの画像や動画を使用可能</li>
<li>・ プロモーション方法の提案</li>
</ul>';
$lang['page_sole_agent_section2_item4_title'] = '店舗デザイン';
$lang['page_sole_agent_section2_item4_desc'] = '<p>ディテーリングショップを運営にするにあたって店舗の設計は重要です。<br>
私達は世界中のパートナーと協力し、数多くの店舗を作ってきました。そのノウハウから店舗サイズに合わせた最適なレイアウトをご提案できます。<br>
お客様の接客や作業動線を鑑みた店舗内部のレイアウトや外装デザイン、必要機材の数量・手配までお手伝いすることができます。</p>
<ul>
<li>・ レイアウト</li>
<li>・ デザイン</li>
<li>・ 機材の選定</li>
<li>・ 機材の手配</li>
</ul>';
$lang['page_sole_agent_section2_item5_title'] = '人材育成と管理システムの提供';
$lang['page_sole_agent_section2_item5_desc'] = '<p>スタッフ教育は最高のサービスを提供するために最も大切な要素です。<br>
全ての施工メニューの技術研修を行っております。<br>
また、店舗サイズに合わせた適切なスタッフの配置や人事評価制度などもご提案できます。</p>
<ul>
<li>・ 技術研修</li>
<li>・ 適切なスタッフの配置</li>
<li>・ 人事評価制度の提案</li>
</ul>';
$lang['page_sole_agent_section2_item6_title'] = '施工メニューと価格設定の提案';
$lang['page_sole_agent_section2_item6_desc'] = '<p>施工メニューの設計が、店舗の売上・利益に大きく影響します。<br>
私達の製品は世界中で使用されていますが、実際に店舗で使用されている実績から算出された各施工メニューの正確な使用溶剤のコストをご提示できます。<br>
そのコストを元に、利益を生み出しやすい施工メニューの設計をご提案できます。</p>
<ul>
<li>・ 利益を生み出す施工メニューの提案</li>
<li>・ 正確な使用溶剤のコスト</li>
<li>・ 各施工メニューに必要な製品提案</li>
</ul>';
$lang['page_sole_agent_section3_title'] = '代理店になるまで';
$lang['page_sole_agent_section3_step1_title'] = 'STEP 01';
$lang['page_sole_agent_section3_step1_subtitle'] = '問い合わせフォームより連絡';
$lang['page_sole_agent_section3_step1_desc'] = '問い合わせフォームより必要情報を入力して連絡をください。日本語、英語、中国語、タイ語、インドネシア語で対応できます。';
$lang['page_sole_agent_section3_step2_title'] = 'STEP 02';
$lang['page_sole_agent_section3_step2_subtitle'] = '商品サンプルの提供';
$lang['page_sole_agent_section3_step2_desc'] = 'あなたが必要とする製品を教えてください。私たちは製品サンプルをご提供することが出来ます。実際にご使用いただくことで、我々の哲学を知ることが出来るはずです。';
$lang['page_sole_agent_section3_step3_title'] = 'STEP 03';
$lang['page_sole_agent_section3_step3_subtitle'] = 'ディスカッション';
$lang['page_sole_agent_section3_step3_desc'] = '私たちメーカーと代理店様や施工店様とは、信頼関係が重要です。よりよい関係を築くために、どんなに些細な疑問や不安もお聞きください。';
$lang['page_sole_agent_section3_step4_title'] = 'STEP 04';
$lang['page_sole_agent_section3_step4_subtitle'] = '契約締結';
$lang['page_sole_agent_section3_step4_desc'] = '弊社より契約書をお送りします。この契約書はあなたの権利を守るための重要な書類です。必ず内容を確認いただいた上で署名をしご送付ください。私たちは署名を行い返送し、契約は成立いたします。';
$lang['page_sole_agent_section4_title'] = '代理店問い合わせ';
$lang['page_sole_agent_section4_subtitle'] = '必要情報を記入して送信ボタンを押してください。';

$lang['breadcrumb_sole_agent_confirm'] = '代理店応募確認';
$lang['breadcrumb_sole_agent_complete'] = '代理店応募完了';
$lang['page_sole_agent_complete_your_message'] = '送信が完了しました。';
$lang['page_sole_agent_complete_thank_you'] = '<p>お問い合わせありがとうございます。お問い合わせ内容に対して近日中に担当者よりご返答させて頂きます。</p>';

//Global Network
$lang['page_global_meta_title'] = '海外拠点｜SENSHA WORLD.com';
$lang['page_global_meta_desc'] = '私たちは世界中のパートナーとの協力の元、世界35カ国に800店舗以上の洗車・コーティングショップのネットワークを築いてきました。
私たちは、この世界ネットワークを“SENSHA Family”と呼んでいます。';
$lang['breadcrumb_global'] = '海外拠点';
$lang['page_global_section1_title'] = '海外拠点';
$lang['page_global_section1_desc'] = '私たちは、各国ディストリビューターである“SENSHA Family”と協力し、世界中に高品質な製品と最高のサービスを提供しています。';
$lang['page_global_address'] = '所在地';
$lang['page_global_phone'] = '電話';
$lang['page_global_see_google'] = 'Google MAPで見る';

//Q&A
$lang['page_qa_meta_title'] = 'Q&A｜SENSHA WORLD.com';
$lang['page_qa_meta_desc'] = 'SENSHAのサービスに関するよくある質問をご確認いただけます。個人情報の変更、ID、パスワードを忘れた場合、トラブルの際の対応など、みなさまの質問・疑問にお答え致します。';
$lang['breadcrumb_qa'] = 'Q & A';
$lang['page_qa_title'] = 'Q&A　サポート・お問い合わせ';
$lang['page_qa_desc'] = 'SENSHAのサービスに関するよくある質問をご確認いただけます。個人情報の変更、ID、パスワードを忘れた場合、トラブルの際の対応など、みなさまの質問・疑問にお答え致します。';
$lang['page_qa_sensha'] = 'SENSHA';
$lang['page_qa_qa1'] = '私達"SENSHA"についてのご質問';
$lang['page_qa_qa2'] = '製品';
$lang['page_qa_qa3'] = '製品の使用方法などについての質問';
$lang['page_qa_qa4'] = '注文/支払い';
$lang['page_qa_qa5'] = '注文方法、及び、支払い方法について';
$lang['page_qa_qa6'] = '配送';
$lang['page_qa_qa7'] = '配送方法とその詳細について';
$lang['page_qa_qa8'] = '返品';
$lang['page_qa_qa9'] = '返品情報条件とその方法について';
$lang['page_qa_qa10'] = '値引き/クーポン';
$lang['page_qa_qa11'] = '値引き/クーポンの利用について';
$lang['page_qa_qa12'] = 'アカウント';
$lang['page_qa_qa13'] = 'アカウント管理と個人情報について';
$lang['page_qa_qa14'] = 'その他';
$lang['page_qa_qa15'] = 'その他の質問';
$lang['page_qa_qa16'] = 'よくある質問';

//Q&A Category
$lang['page_qa_cat_meta_title'] = 'XXXXXについて（Q&A）｜SENSHA WORLD.com';
$lang['page_qa_cat_meta_desc'] = 'SENSHAのサービスに関するよくある質問をご確認いただけます。個人情報の変更、ID、パスワードを忘れた場合、トラブルの際の対応など、みなさまの質問・疑問にお答え致します。';
$lang['breadcrumb_qa_cat'] = 'XXXXについて';
$lang['page_qa_cat_title1'] = '';
$lang['page_qa_cat_title2'] = 'についての質問';
$lang['page_qa_cat_desc1'] = '';
$lang['page_qa_cat_desc2'] = 'についての質問を下記にまとめておりますので、ご不明点がある場合に参考にしてください。';
$lang['page_qa_cat_qa_cat'] = 'Q＆Aカテゴリー';
$lang['page_qa_cat_see_more'] = 'もっとみる <span>>></span>';

//Terms of use
$lang['page_terms_meta_title'] = '利用規約 ｜SENSHA WORLD.com';
$lang['breadcrumb_terms'] = '利用規約';
$lang['page_terms_title'] = '利用規約';
$lang['page_terms_subtitle'] = '<p>株式会社洗車の王国がお客様に対し、弊社サービス（以下、サービスという）を提供するにあたって、以下の利用規約を制定しております。<br>
お客様がサービスをご利用された場合、本規約に同意したものとみなされますので、よくお読みください。<br>
本規約は、サービスの利用に関するルールや権利関係を明確化したものです。</p>';
$lang['page_terms_desc'] = '<p>本翻訳はお客様の便宜上ご提供しているものであり、いかなる差異に関しても原文である日本語版が優先されます。</p>

<p><em>・初めに</em>
SENSHA の製品およびサービス（以下「本サービス」）をご利用いただきありがとうございます。すべてのユーザーは日本の法律に基づき設立され操業し（会社法人等番号：021001024148）日本国神奈川県伊勢原市上粕屋1007-3に所在する、株式会社洗車の王国（以下「SENSHA」）が本サービスを提供します。</p>

<p>ユーザーは、本サービスをご利用いただく場合、本規約に同意したものとみなします。</p>


<p><em>・本サービスのご利用</em>
本サービス内のすべてのポリシーを遵守してください。</p>


<p><em>・禁止事項</em>
本サービスの利用を妨害する行為や、SENSHA が提供するインターフェースおよび手順以外の方法による本サービスへのアクセス等の不正利用の一切を禁じます。ユーザーは、法律で認められている場合に限り、本サービスを利用することができます。ユーザーが SENSHA の規約やポリシーを遵守しない場合、または SENSHA が不正行為と疑う行為について調査を行う場合に、SENSHA はユーザーに対する本サービスの提供を一時停止または停止することができます。</p>

<p>本サービスに関わる全てのコンテンツは SENSHAもしくは SENSHA が指定する第三者が著作権等の知的財産権、使用権、その他の権利を有しています。ユーザーは、本サービスのコンテンツの所有者から許可を得た場合や、法律によって認められる場合を除き、そのコンテンツを利用することはできません。本規約は、本サービスで使用されている、いかなるブランドまたはロゴを利用する権利もユーザーに与えるものではありません。</p>

<p>本サービス内に表示、または、本サービスに伴って表示されるいかなる法的通知も、削除、隠蔽、改ざんしてはなりません。</p>

<p>本サービスで表示されるコンテンツの一部は、SENSHA の所有物ではありません。こうしたコンテンツについては、そのコンテンツを提供する当事者が単独で責任を負います。SENSHA は、コンテンツが違法か否か、または SENSHA のポリシーに違反しているか否かを判断するために、コンテンツをレビューすることができます。さらに、SENSHA は、そのポリシーまたは法律に違反していると合理的に判断したコンテンツを削除したり、その表示を拒否することができます。ただし、このことは、SENSHA がコンテンツをレビューしていることを必ずしも意味するものではありません。</p>

<p>本サービスの利用に関して、SENSHA はユーザーに対してサービスの告知、管理上のメッセージ、およびその他の情報を送信することができます。</p>


<p><em>・ユーザーの SENSHA アカウント</em>
本サービスを利用するために、SENSHA アカウントが必要になる場合があります。SENSHA アカウントは、ユーザー自身が作成するか、管理者によって割り当てられます。管理者によって割り当てられた SENSHA アカウントを利用する場合には、本規約とは異なるまたは追加の規定が適用されることがあります。また、管理者はユーザーアカウントの管理・削除が可能です。</p>

<p>SENSHA アカウントを保護するため、パスワードは他人に知らせないでください。SENSHA アカウント上や SENSHA アカウントを通じての行動に対する責任はユーザー自身にあります。SENSHA アカウントのパスワードを第三者のアプリケーションで再利用することは避けるようにしてください。ユーザーのパスワードまたはアカウントが不正に利用されていることに気付いた場合には、<a href="forgot_password" style="color:blue;" target="_blank">こちら</a>からパスワードの変更をおこなってください。</p>

<p>ユーザーはサービスの提供を停止したい時、所定の手続きを行なうことでアカウントを削除することができます。ただし、アカウントが削除された時点で、それまでに保有していたポイントやクーポンなどは消滅します。一度削除されたアカウント情報の復旧はできませんので、予めご了承ください。</p>

<p><em>・個人情報の保護</em>
SENSHA のプライバシーポリシーまたは特定の追加規定において、どのようにコンテンツを取り扱うかが記載されています。本サービスに関するフィードバックまたは提案をユーザーが送信した場合、SENSHA は、ユーザーに対する義務を負うことなく、そのフィードバックまたは提案を利用することができます。</p>

<p><em>・本サービスの変更または終了</em>
SENSHA は、常に本サービスの変更および改善を行っています。SENSHA は、機能の追加や変更、削除を行うことができ、本サービス全体を一時停止または終了することができます。</p>

<p>SENSHA はいついかなる場合でも、本サービスの提供を停止、または、本サービスに対する制限を追加または新規に設定することができます。</p>

<p>SENSHA は、ユーザーが自身のデータを所有し、そのデータにユーザーが常にアクセスできるようにすることが重要であると考えています。SENSHA が本サービスを中断する場合、合理的に可能なときにはユーザーに対して、合理的な事前の通知を行い、本サービスから情報を取得する機会を提供します。</p>


<p><em>・保証および免責</em>
SENSHA は、商業上合理的な水準の技術および注意のもとに本サービスを提供し、ユーザーに本サービスの利用を楽しんでいただくことを望んでいますが、約束できないことがあります。</p>

<p>本規約または追加規定に規定されている場合を除き、SENSHA またはサプライヤーもしくはディストリビューターのいずれも、本サービスについて具体的な保証を行いません。たとえば SENSHA は、本サービスについて、機能向上、機能追加、利便性向上、ユーザーニーズへの対応を約束するものではありません。</p>


<p><em>・本サービスに対する責任</em>
SENSHA ならびにサプライヤーおよびディストリビューターは、逸失利益、逸失売上もしくはデータの紛失、金銭的損失、または間接損害、特別損害、結果損害もしくは懲罰的損害について責任を負いません。</p>

<p>法律で許されている範囲内で、黙示的保証を含む、本規約が適用されるいかなる請求についても、SENSHA ならびにサプライヤーおよびディストリビューターが負う責任の総額は、ユーザーが本サービスを利用するために SENSHA に対して支払った金額を上限とし、または、SENSHA が選択した場合には、再度ユーザーに対して本サービスを提供することに限定されるものとします。</p>

<p>いかなる場合においても、SENSHA ならびにそのサプライヤーおよびディストリビューターは、合理的に予測することができない損失または損害については、何らの責任も負いません。</p>

<p>SENSHA は、一部の国において、ユーザーが消費者としての法的権利を有する可能性があることを認識しています。ユーザーが個人的な目的のために本サービスを利用している場合には、本規約または追加規定の中のどの規定も、消費者の法的権利を制約するものではありません。</p>


<p><em>・事業者による本サービスの利用</em>
本サービスを事業のために利用する場合、その事業者は本規約に同意するものとします。事業者は、SENSHA とその関連会社、役員、代理店、従業員を、本サービスの利用または本規約への違反に関連または起因するあらゆる請求申し立て、訴訟、法的措置について、請求申し立て、損失、損害、訴訟、裁判、告訴から生じる法的責任および費用、弁護士費用を含め、免責および補償するものとします。</p>



<p><em>・本規約について</em>
SENSHA は、法律の改正または本サービスの変更を反映するために、本サービスに適用する本規約または特定の本サービスについての追加規定を修正することがあります。ユーザーは定期的に本規約をご確認ください。SENSHA は、本規約の修正に関する通知をこのページに表示します。追加規定の修正については、該当する本サービス内において通知を表示します。変更は、さかのぼって適用されることはなく、その変更が表示されてから 14 日以降に発効します。ただし、本サービスの新機能に対処する変更または法律上の理由に基づく変更は、直ちに発効するものとします。本サービスに関する修正された規定に同意しないユーザーは、本サービスの利用を停止してください。</p>

<p>本規約と追加規定との間に矛盾が存在する場合には、追加規定が本規約に優先します。</p>

<p>本規約は、SENSHA とユーザーとの間の関係を規定するものです。本規約は、第三者の受益権を創設するものではありません。</p>

<p>ユーザーが本規約を遵守しない場合に、SENSHA が直ちに法的措置を講じないことがあったとしても、そのことによって、SENSHA が有している権利（たとえば、将来において、法的措置を講じる権利）を放棄しようとしていることを意味するものではありません。</p>

<p>ある特定の規定が強制執行不可能であることが判明した場合であっても、そのことは他のいずれの規定にも影響を及ぼすものではありません。</p>


<p><em>・準拠法および裁判所</em>
本規約の準拠法は日本法とします。本規約および本サービスに起因または関連してユーザーと SENSHA との間に生じた紛争については、東京地方裁判所を第一審の専属的合意管轄裁判所とします。ただし、国によっては（欧州連合内の国を含む）、契約が消費者の居住国の地域法に準拠することを求める法律があります。その場合は法律が本項よりも優先されます。
</p>';

//Privacy
$lang['page_privacy_meta_title'] = '個人情報保護方針 ｜SENSHA WORLD.com';
$lang['breadcrumb_privacy'] = '個人情報保護方針';
$lang['page_privacy_title'] = '個人情報保護方針';
$lang['page_privacy_subtitle'] = '<p>株式会社洗車の王国は個人情報を取り扱うにあたって、以下の方針を定めております。</p>';
$lang['page_privacy_desc'] = '
<p><em>1. 法令等の遵守</em>
当社は、個人情報を取り扱うにあたっては、日本国の法律「個人情報の保護に関する法律」をはじめとする個人情報の保護に関する法令、ガイドラインおよび本プライバシーポリシーを遵守いたします。</p>
<p><em>2. 個人情報の収集</em>
当社は、必要な範囲で個人情報を収集することがあります。収集する個人情報の範囲は、利用目的を達成するために必要な限度を超えないものとします。</p>
<p><em>3. 個人情報の管理・保護について</em>
・当社が個人情報を取り扱う際には、管理責任者を置き、適切な管理をおこなうとともに、外部への流出防止に努めます。また、外部からの不正アクセスまたは紛失、破壊、改ざん等の危険に対しては、適切かつ合理的なレベルの安全対策を実施し、個人情報の保護に努めます。 <br />
・当社は、個人情報に係るデータベース等へのアクセス権を有する者を限定し、社内においても不正な利用がなされないように厳重に管理いたします。 <br />
・当社は、個人情報の取扱いを外部に委託することがあります。この場合、グループ会社または個人情報を適正に取り扱っていると認められる委託先（以下「業務委託先」といいます）を選定し、委託契約等において、個人情報の管理、秘密保持、再提供の禁止等、個人情報の漏洩等なきよう必要な事項を取り決めるとともに、適切な管理を実施させます。</p>
<p><em>4. 個人情報の利用</em>
当社は、ご本人の同意を得た場合、および法令により例外として取り扱うことが認められている場合を除き、収集した個人情報について、収集の際に予め明示した目的または公表している利用目的においてのみ利用いたします。</p>
<p><em>5. 個人情報の提供</em>
当社は、個人情報をご本人の同意なしに、業務委託先以外の第三者に開示・提供することはありません。ただし、法令により開示を求められた場合、または裁判所、警察等の公的機関から開示を求められた場合には、ご本人の同意なく個人情報を開示・提供することがあります。</p>
<p><em>6. 社内体制の整備</em>
当社は、本プライバシーポリシーに基づき、個人情報の保護に関する社内規定を整備し、当社の役員・従業員等に対し、個人情報の取扱いについて明確な方針を示し、個人情報の保護に努めます。</p>
<p><em>7. 個人情報保護方針の変更</em>
当社は情報の安全性を適切に維持管理するために、個人情報保護方針を定期的に見直す権利を留保します。
改訂事項は当社の公式ウェブサイトに掲示しますので、定期的にご確認ください。ウェブサイトに掲示されると直ちに有効になります。
収集する情報、その使用方法、および開示する状況(ある場合)等の重要な変更を加える場合は、お客様が認識できるよう、更新された内容をここで通知します。</p>
<p><em>8. お問い合わせ</em>
本プライバシーポリシーに関するお問い合わせにつきましては、 お問い合わせフォームで受け付けております。</p>
<span>2019年5月1日 制定</span>';


//Contact
$lang['page_contact_meta_title'] = 'お問い合わせ ｜SENSHA WORLD.com';
$lang['breadcrumb_contact'] = 'お問い合わせ';
$lang['page_contact_title'] = 'お問い合わせ';
$lang['page_contact_subtitle'] = '当サイトに関するお問い合わせはこちらからお願いいたします。なお、商品配送の問題に関してはダッシュボードの「購入履歴」よりご報告ください。';

//Contact Mail
$lang['page_mail_title'] = 'お問い合わせを承りました【洗車の王国】';
$lang['page_mail_P01'] = '
<br>
    <br>
    お世話になっております。<br>
    洗車の王国カスタマーセンターでございます。<br>
    <br>
    <br>
    下記の内容にてお問い合わせを承りました。<br>
    <br>
    ============================<br>
    内容:';
$lang['page_mail_P02'] = '
    ============================<br>
    <br>
    ▼ご注意(必ずお読みください)▼<br>
    <br>
    <br>
    ※内容を確認し、折り返しご連絡をさせていただきますのでお待ちいただけますようお願いいたします。<br>
    <br>
    ※本メールはお問い合わせの際にお客様にご指定いただいたメールアドレス宛へ送信しております。万が一、メールの内容にお心当たりがない場合は大変お手数でございますが当店までご連絡をお願いいたします。<br>
    <br>
    <br>
    今後とも洗車の王国をご愛顧いただけますよう宜しくお願い申し上げます。<br>
    <br>
    <br>
    ▼ SENSHA NEWS ▼<br>
    こちらで最新情報をご案内しております<br>
    http://sensha.enfete.co/jp/page/news<br>
    <br>
    ======================<br>
    洗車をして愛車を綺麗にすることそれはきっと安全運転につながるはず。<br>
    ======================<br>
    <br>
    Clean Your CAR<br>
    株式会社 洗車の王国<br>
    〒259-1141<br>
    神奈川県伊勢原市上粕屋1007-3<br>
    (Phone) 0463-94-5106<br>
    (Fax) 0463-94-5108<br>
    (E-Mail) info@cleanyourcar.jp<br>
    (URL) http://www.cleanyourcar.jp<br>
    (World site) http://sensha-world.com/<br>';
//News
$lang['page_news_meta_title'] = 'ニュース・プレスリリース ｜SENSHA WORLD.com';
$lang['page_news_meta_desc'] = '株式会社洗車の王国のニュース/プレスリリースのページです。洗車の王国 ブランド (SENSHA brand) に関する最新情報を掲載しています。';
$lang['breadcrumb_news'] = 'ニュース・プレスリリース';
$lang['page_news_title'] = 'ニュース・プレスリリース';
$lang['page_news_subtitle'] = '洗車の王国 ブランド (SENSHA brand) の海外展開・新製品の情報等を掲載しています。';
$lang['page_news_see_more'] = 'もっと見る';

//Register
$lang['page_register_meta_title'] = '会員登録 ｜SENSHA WORLD.com';
$lang['breadcrumb_register'] = '会員登録';
$lang['page_register_title'] = '会員登録';
$lang['page_register_subtitle'] = '・ロボットではないことを証明するため、EメールまたはSMS認証を行ってください。<br>
・下記フォームへ必要情報を入力してください。';
$lang['page_register_confirmation'] = '確認';
$lang['page_register_via_email'] = 'Eメール認証';
$lang['page_register_via_sms'] = 'SMS認証';
$lang['page_register_email'] = 'Email';
$lang['page_register_input_email'] = 'Emailを入力して下さい';
$lang['page_register_tel_code'] = '国番号';
$lang['page_register_tel'] = '電話番号';
$lang['page_register_input_tel'] = '電話番号を入力して下さい。';
$lang['page_register_password'] = 'パスワード';
$lang['page_register_input_password'] = 'パスワードを入力してください';
$lang['page_register_country'] = '国';
$lang['page_register_country_select'] = '国を選択して下さい';
$lang['page_register_save'] = '保存';
$lang['page_register_select'] = 'XXXXを選んでください。';
$lang['mail_register_content'] = '洗車の王国　公式ホームページをご利用いただき、誠にありがとうございます。<br>下記のURLをクリックして、登録を完了してください。';


//Register Confirmation
$lang['page_register_confirm_meta_title'] = '登録情報確認 ｜SENSHA WORLD.com';
$lang['breadcrumb_register_confirm'] = '登録情報確認';
$lang['page_register_confirm_title'] = '登録情報確認';
$lang['page_register_confirm_subtitle'] = '上記の情報が正しいか確認してください。確認後、下記の送信ボタンをクリックすると、会員登録を完了するためのEメールまたはSMSを送信します。';
$lang['page_register_confirm_authenticate'] = '認証';
$lang['page_register_confirm_authenticate_via'] = '認証方法 ';
$lang['page_register_confirm_confirm'] = '送信';

//Register Success
$lang['page_register_success_meta_title'] = '認証メール・SMS送信 ｜SENSHA WORLD.com';
$lang['breadcrumb_register_success'] = '認証メール・SMS送信';
$lang['page_register_success_title'] = '認証メール・SMS送信';
$lang['page_register_success_message1'] = '認証メール・SMSの送信が完了しました';
$lang['page_register_success_message2'] = '送信されたメールまたはSMSを確認し会員登録を完了させてください。';
$lang['page_register_success_message3'] = '会員登録を完了していただくためには、下記の手続きをXX時間以内に完了していただく必要があります。下記URLにアクセスして会員登録を完了してください。<br>

<p>URL:XXXXXXXXXXXXXXX</p>

<p>※会員登録をせずXX時間経過した場合、URLが向こうとなります。<br>
　その場合は再度、初めから登録手続きをしてください。<br>
※本メールは自動送信メールとなります。<br>
　本メールにご返信をいただいても回答できませんのでご了承ください。<br>
※このメールにお心当たりがない方は本メールを削除してください。</p>';

//Register complete
$lang['page_register_complete_meta_title'] = '会員登録完了 ｜SENSHA WORLD.com';
$lang['breadcrumb_register_complete'] = '会員登録完了';
$lang['page_register_complete_title'] = '会員登録完了';
$lang['page_register_complete_message1'] = '会員登録が完了しました。';
$lang['page_register_complete_message2'] = '会員登録が完了しました。買い物を続けるためには、あなたの個人情報・配達先などの追加情報を入力してください。';
$lang['page_register_complete_message3'] = '個人情報を編集する';

//Register OTP
$lang['page_register_otp_meta_title'] = 'ワンタイムパスワードの認証 ｜SENSHA WORLD.com';
$lang['breadcrumb_register_otp'] = 'ワンタイムパスワードの認証';
$lang['page_register_otp_title'] = 'ワンタイムパスワードの認証';
$lang['page_register_otp_message1'] = '会員登録が完了するためにSMSに記載されている、ワンタイムパスワードを下記のフィールドに入力ください。';
$lang['page_register_otp_message2'] = 'ワンタイムパスワード';
$lang['page_register_otp_message3'] = 'ワンタイムパスワードを入力ください。';
$lang['page_register_otp_send_again'] = '再送する';
$lang['page_register_otp_confirm'] = '認証する';

//Forgot Password
$lang['page_forgot_pass_meta_title'] = 'パスワードリセット ｜SENSHA WORLD.com';
$lang['breadcrumb_forgot_pass'] = 'パスワードリセット';
$lang['page_forgot_pass_title'] = 'パスワードリセット';
$lang['page_forgot_pass_message1'] = 'パスワードを忘れてしまった場合は下記フォームにEメールアドレスと電話番号を入力し、認証方法を選択後、送信ボタンを押してください。';
$lang['page_forgot_pass_message2'] = '送信';
$lang['page_forgot_pass_message3'] = '<p>件名：パスワードの変更手続き<br>
<br>
本文：ご利用のアカウントのパスワードをリセットするには以下のURLをクリックしてください。パスワードのリセットにお心当たりが無い場合はこのメールを無視してください。<br>
<br>
URL：XXXXXXXXXX<br>
<br>
このリンクは、送信されてから24時間後に使用できなくなります。</p>';

//New Password
$lang['page_new_pass_meta_title'] = 'パスワード変更 ｜SENSHA WORLD.com';
$lang['breadcrumb_new_pass'] = 'パスワード変更';
$lang['page_new_pass_title'] = 'パスワードの変更';
$lang['page_new_pass_message1'] = 'あなたのパスワードはリセットされました。新しいパスワードを入力してください。';
$lang['page_new_pass_message2'] = '確認';

//Password Complete
$lang['page_pass_complete_meta_title'] = 'パスワード変更完了 ｜SENSHA WORLD.com';
$lang['breadcrumb_pass_complete'] = '変更完了';
$lang['page_pass_complete_title'] = 'パスワード変更完了';
$lang['page_pass_complete_message1'] = 'パスワードの変更が完了しました。';
$lang['page_pass_complete_message2'] = 'ショッピングを続けるには下のボタンからログインをしてください。';
$lang['page_pass_complete_message3'] = 'ログイン';

//Password OTP
$lang['page_pass_otp_meta_title'] = 'ワンタイムパスワードの認証 ｜SENSHA WORLD.com';
$lang['breadcrumb_pass_otp'] = 'ワンタイムパスワードの認証';
$lang['page_pass_otp_title'] = 'ワンタイムパスワードの認証';
$lang['page_pass_otp_message1'] = 'パスワードの変更を完了するためにSMSに記載されている、ワンタイムパスワードを下記のフィールドに入力ください。';
$lang['page_pass_otp_message2'] = 'ワンタイムパスワード';
$lang['page_pass_otp_message3'] = 'ワンタイムパスワードを入力ください。';

//PDF
$lang['pdf_title'] = '請求書';
$lang['pdf_description'] = '洗車をして愛車を綺麗にすること それはきっと安全運転につながるはず。';
$lang['pdf_invoice_no'] = '請求書 No : ';
$lang['pdf_date'] = '発行日';
$lang['pdf_company_name'] = '株式会社 洗車の王国';
$lang['pdf_company_address'] = '〒259-1141 神奈川県伊勢原市上粕屋1007-3';
$lang['pdf_company_tel'] = '(Phone)0463-94-5106 (Fax)0463-94-5108';
$lang['pdf_company_mail'] = '(E-Mail): info@cleanyourcar.jp';
$lang['pdf_shipment_terms'] = '配送方法 : ';
$lang['pdf_to_jp'] = '様';
$lang['pdf_customer_tel'] = '(Phone) : ';
$lang['pdf_products_ordered'] = '注文番号';
$lang['pdf_qty'] = '個数';
$lang['pdf_unit_price'] = '単価';
$lang['pdf_amount'] = '金額';
$lang['pdf_shipping_fee'] = '送料';
$lang['pdf_discount'] = '値引き';
$lang['pdf_sub_total'] = '小計';
$lang['pdf_admin_fee'] = '手数料';
$lang['pdf_tax'] = '消費税';
$lang['pdf_grand_total'] = 'ご請求金額';
$lang['pdf_payment_options'] = '支払い方法';
$lang['pdf_sensha_bank'] = '銀行振込 : りそな銀行／伊勢原支店';
$lang['pdf_sensha_account'] = '口座番号 : 1364187';
$lang['pdf_sensha_account_holder'] = '口座名義人 : 株式会社洗車の王国';
$lang['pdf_sensha_branch'] = '伊勢原（イセハラ）支店';

$lang['pdf_sensha_bank_oversea'] = '銀行振込 : JAPAN RESONA BANK : 0010';
$lang['pdf_sensha_account_oversea'] = '口座番号 : 1299037';
$lang['pdf_sensha_account_holder_oversea'] = '口座名義人 : SENSHA Co., Ltd.';
$lang['pdf_sensha_branch_oversea'] = 'Isehara Branch : 646';
$lang['pdf_sensha_address'] = '1-3-6 Isehara, Isehara-city, Kanagawa, Japan';
$lang['pdf_sensha_tel'] = '81(463)92-1511';

$lang['pdf_thanks'] = 'いつもご利用いただき、誠にありがとうございます。';
$lang['pdf_sensha_pay_on_delivery'] = '着払';

$lang['pdf_receipt_title'] = '領収書';
$lang['pdf_receipt_invoice_no'] = '領収書 No. :';
$lang['pdf_receipt_company_name'] = 'Company name :';
$lang['pdf_receipt_transaction_no'] = '注文番号 ';
$lang['pdf_receipt_received'] = '但、洗車用品代として、上記正に領収いたしました。';
$lang['pdf_receipt_date_issued'] = '発行日 ';
$lang['pdf_receipt_address1'] = '株式会社 洗車の王国';
$lang['pdf_receipt_address2'] = '〒259-1141 神奈川県伊勢原市上粕屋1007-3';
$lang['pdf_receipt_address3'] = '';
$lang['pdf_receipt_tel'] = '(Phone)0463-94-5106';
$lang['pdf_receipt_fax'] = '(Fax)0463-94-5108';
$lang['pdf_receipt_payment_received'] = 'お支払い方法 :';
$lang['pdf_receipt_approved'] = '発行元 :';

//SMS shopping
$lang['shop_sms_greeting'] = 'SENSHAサイトでのご注文を承りました。';
$lang['shop_sms_domestic'] = '国内注文 [';
$lang['shop_sms_international'] = '海外注文 [';

?>
